# Contributing

JOGL is **100% open source**, and we fully welcome contributions! You can help us by **contributing to the code**, or translating the platform in your language.

We use the following technologies: [ReactJS](https://reactjs.org/), [NextJS](https://github.com/vercel/next.js/), Typescript, Algolia, Sass, AmazonS3, Ruby, GraphQL, Elastic Search... If you have experience in one or multiple of them, your help will be much appreciated!

When contributing to this repository, please try to focus on already planned issues / known bugs:

- **List view** - View issues in list: [frontend-only issues](https://gitlab.com/JOGL/JOGL/-/issues?label_name[]=frontend) or [all issues](https://gitlab.com/JOGL/JOGL/-/issues).
- **Board view** - View a Trello-like view of the issues listed by priority and state (to do, doing, ready/review): [frontend-only issues](https://gitlab.com/JOGL/JOGL/-/boards/1990876?&label_name[]=frontend) or [all issues](https://gitlab.com/JOGL/JOGL/-/boards/885598).

- API first, head to our [API documentation](https://documenter.getpostman.com/view/8688524/SWE84xMg?version=latest) for a current view of the API.
- If you want to add code, fork from the branch `develop` and merge request to the branch `develop`! Any request to `master` will be automatically rejected. (see below for more detail)
- Check our [Readme](https://gitlab.com/JOGL/frontend-v0.1/-/blob/master/README.md) to know how to launch the stack

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

Please note we have a [code of conduct](https://gitlab.com/JOGL/JOGL/-/blob/master/CONTRIBUTING.md#code-of-conduct), please follow it in all your interactions with the project.

You can contact us at support[at]jogl.io for any question.

## Pull Request Process

1. Ensure any install or build dependencies are removed before the end of the layer when doing a
   build.
2. Update the README.md with details of changes to the interface, this includes new environment
   variables, exposed ports, useful file locations and container parameters.
3. Increase the version numbers in any examples files and the README.md to the new version that this
   Pull Request would represent. The versioning scheme we use is [SemVer](http://semver.org/).
4. You may merge the Pull Request in once you have the sign-off of two other developers, or if you
   do not have permission to do that, you may request the second reviewer to merge it for you.

## More information

For more detailed information on how to contribute, please read our [General contributing guidelines](https://gitlab.com/JOGL/JOGL/-/blob/master/CONTRIBUTING.md).
