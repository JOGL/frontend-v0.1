/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export enum AccessLevel {
  Member = 'member',
  Admin = 'admin',
  Owner = 'owner',
}

export interface AccessOriginModel {
  type: AccessOriginType;
  ecosystem_memberships: MembershipModel[];
}

export enum AccessOriginType {
  Public = 'public',
  Ecosystemmember = 'ecosystemmember',
  Directmember = 'directmember',
}

export interface AccessTokenModel {
  access_token: string;
}

export enum AttendanceAccessLevel {
  Member = 'member',
  Admin = 'admin',
}

export enum AttendanceStatus {
  Pending = 'pending',
  Yes = 'yes',
  No = 'no',
}

export enum AttendanceType {
  User = 'user',
  Email = 'email',
  Communityentity = 'communityentity',
}

export interface AuthResultExtendedModel {
  token: string;
  userId: string;
  created: boolean;
}

export interface AuthResultModel {
  token: string;
  userId: string;
}

export interface AuthorModel {
  id: string;
  name: string;
  orcid_id: string;
  institutions: string[];
  topics: string[];
  newest_work: WorkModel;
}

export interface AuthorizationCodeModel {
  authorization_code: string;
}

export interface BaseModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
}

export interface CallForProposalDetailModel {
  id: string;
  title: string;
  short_title: string;
  description: string;
  short_description: string;
  status: string;
  label: string;
  banner_url_sm: string;
  banner_url: string;
  logo_url_sm: string;
  logo_url: string;
  feed_id: string;
  interests: string[];
  keywords: string[];
  links: LinkModel[];
  tabs: string[];
  listing_privacy: PrivacyLevel;
  content_privacy: PrivacyLevel;
  content_privacy_custom_settings: PrivacyLevelSettingModel[];
  joining_restriction: JoiningRestrictionLevel;
  joining_restriction_custom_settings: JoiningRestrictionLevelSettingModel[];
  management: string[];
  home_channel_id?: string | null;
  user_access_level: string;
  user_access: CommunityEntityPermissionModel;
  user_onboarded: boolean;
  contribution: string;
  listing_origin: AccessOriginModel;
  path: EntityMiniModel[];
  user_invitation: InvitationModelUser;
  community_id: string;
  proposal_participation: PrivacyLevel;
  proposal_privacy: ProposalPrivacyLevel;
  discussion_participation: DiscussionParticipation;
  template: CallForProposalTemplateModel;
  scoring: boolean;
  /** @format int32 */
  max_score: number;
  /** @format date-time */
  submissions_from?: string | null;
  /** @format date-time */
  submissions_to?: string | null;
  rules: string;
  faq: FAQItem[];
  stats: CallForProposalDetailStatModel;
  created_by?: UserMiniModel;
}

export interface CallForProposalDetailStatModel {
  /** @format int32 */
  members_count: number;
  /** @format int32 */
  workspaces_count: number;
  /** @format int32 */
  cfp_count: number;
  /** @format int32 */
  organization_count: number;
  /** @format int32 */
  hubs_count: number;
  /** @format int32 */
  needs_count: number;
  /** @format int32 */
  content_entity_count: number;
  /** @format int32 */
  participant_count: number;
  /** @format int32 */
  needs_count_aggregate: number;
  /** @format int32 */
  documents_count: number;
  /** @format int32 */
  documents_count_aggregate: number;
  /** @format int32 */
  papers_count: number;
  /** @format int32 */
  papers_count_aggregate: number;
  /** @format int32 */
  resources_count: number;
  /** @format int32 */
  resources_count_aggregate: number;
  /** @format int32 */
  proposals_count: number;
  /** @format int32 */
  submitted_proposals_count: number;
}

export interface CallForProposalMiniModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  title: string;
  short_title: string;
  short_description: string;
  description: string;
  status: string;
  label: string;
  type: CommunityEntityType;
  banner_url: string;
  banner_url_sm: string;
  logo_url: string;
  logo_url_sm: string;
  listing_privacy: PrivacyLevel;
  content_privacy: PrivacyLevel;
  joining_restriction: JoiningRestrictionLevel;
  home_channel_id?: string | null;
  user_access_level: string;
  stats: CommunityEntityStatModel;
  user_access: CommunityEntityPermissionModel;
  onboarding: OnboardingConfigurationModel;
  user_onboarded: boolean;
  contribution: string;
  listing_origin: AccessOriginModel;
  content_origin: AccessOriginModel;
  interests: string[];
  keywords: string[];
  links: LinkModel[];
  user_invitation: InvitationModelUser;
  /** @format date-time */
  submissions_from?: string | null;
  /** @format date-time */
  submissions_to?: string | null;
}

export interface CallForProposalModel {
  id: string;
  title: string;
  short_title: string;
  description: string;
  short_description: string;
  status: string;
  label: string;
  banner_url_sm: string;
  banner_url: string;
  logo_url_sm: string;
  logo_url: string;
  feed_id: string;
  interests: string[];
  keywords: string[];
  links: LinkModel[];
  tabs: string[];
  listing_privacy: PrivacyLevel;
  content_privacy: PrivacyLevel;
  content_privacy_custom_settings: PrivacyLevelSettingModel[];
  joining_restriction: JoiningRestrictionLevel;
  joining_restriction_custom_settings: JoiningRestrictionLevelSettingModel[];
  management: string[];
  home_channel_id?: string | null;
  user_access_level: string;
  user_access: CommunityEntityPermissionModel;
  user_onboarded: boolean;
  contribution: string;
  listing_origin: AccessOriginModel;
  path: EntityMiniModel[];
  user_invitation: InvitationModelUser;
  community_id: string;
  proposal_participation: PrivacyLevel;
  proposal_privacy: ProposalPrivacyLevel;
  discussion_participation: DiscussionParticipation;
  template: CallForProposalTemplateModel;
  scoring: boolean;
  /** @format int32 */
  max_score: number;
  /** @format date-time */
  submissions_from?: string | null;
  /** @format date-time */
  submissions_to?: string | null;
  rules: string;
  faq: FAQItem[];
  stats: CallForProposalStatModel;
}

export interface CallForProposalPatchModel {
  title: string;
  short_title: string;
  description?: string | null;
  short_description: string;
  status: string;
  banner_id?: string | null;
  logo_id?: string | null;
  interests?: string[] | null;
  keywords?: string[] | null;
  links?: LinkModel[] | null;
  tabs?: string[] | null;
  listing_privacy?: PrivacyLevel;
  content_privacy?: PrivacyLevel;
  content_privacy_custom_settings?: PrivacyLevelSettingModel[] | null;
  joining_restriction?: JoiningRestrictionLevel;
  joining_restriction_custom_settings?: JoiningRestrictionLevelSettingModel[] | null;
  management?: string[] | null;
  home_channel_id?: string | null;
  community_id: string;
  proposal_participiation: PrivacyLevel;
  proposal_privacy: ProposalPrivacyLevel;
  discussion_participation: DiscussionParticipation;
  template?: CallForProposalTemplateUpsertModel;
  scoring: boolean;
  /** @format int32 */
  max_score?: number | null;
  /** @format date-time */
  submissions_from?: string | null;
  /** @format date-time */
  submissions_to?: string | null;
  rules?: string | null;
  faq?: FAQItem[] | null;
}

export interface CallForProposalStatModel {
  /** @format int32 */
  members_count: number;
  /** @format int32 */
  workspaces_count: number;
  /** @format int32 */
  cfp_count: number;
  /** @format int32 */
  organization_count: number;
  /** @format int32 */
  hubs_count: number;
  /** @format int32 */
  needs_count: number;
  /** @format int32 */
  content_entity_count: number;
  /** @format int32 */
  participant_count: number;
  /** @format int32 */
  proposals_count: number;
  /** @format int32 */
  submitted_proposals_count: number;
}

export interface CallForProposalTemplateModel {
  questions: CallForProposalTemplateQuestionModel[];
}

export interface CallForProposalTemplateQuestionModel {
  key: string;
  type: string;
  /** @format int32 */
  max_length?: number | null;
  /** @format int32 */
  order: number;
  title: string;
  description: string;
  choices: string[];
}

export interface CallForProposalTemplateQuestionUpsertModel {
  key: string;
  type: string;
  /** @format int32 */
  max_length?: number | null;
  /** @format int32 */
  order: number;
  title: string;
  description: string;
  choices?: string[] | null;
}

export interface CallForProposalTemplateUpsertModel {
  questions: CallForProposalTemplateQuestionUpsertModel[];
}

export interface CallForProposalUpsertModel {
  title: string;
  short_title: string;
  description?: string | null;
  short_description: string;
  status: string;
  banner_id?: string | null;
  logo_id?: string | null;
  interests?: string[] | null;
  keywords?: string[] | null;
  links?: LinkModel[] | null;
  tabs?: string[] | null;
  listing_privacy?: PrivacyLevel;
  content_privacy?: PrivacyLevel;
  content_privacy_custom_settings?: PrivacyLevelSettingModel[] | null;
  joining_restriction?: JoiningRestrictionLevel;
  joining_restriction_custom_settings?: JoiningRestrictionLevelSettingModel[] | null;
  management?: string[] | null;
  home_channel_id?: string | null;
  community_id: string;
  proposal_participiation: PrivacyLevel;
  proposal_privacy: ProposalPrivacyLevel;
  discussion_participation: DiscussionParticipation;
  template?: CallForProposalTemplateUpsertModel;
  scoring: boolean;
  /** @format int32 */
  max_score?: number | null;
  /** @format date-time */
  submissions_from?: string | null;
  /** @format date-time */
  submissions_to?: string | null;
  rules?: string | null;
  faq?: FAQItem[] | null;
}

export interface ChannelDetailModel {
  id: string;
  title: string;
  description?: string | null;
  icon_key: string;
  visibility: ChannelVisibility;
  auto_join: boolean;
  management: string[];
  user_access_level?: AccessLevel;
  stats: ChannelStatModel;
  permissions: Permission[];
  created_by?: UserMiniModel;
  /** @format int32 */
  unread_posts: number;
  /** @format int32 */
  unread_mentions: number;
  /** @format int32 */
  unread_threads: number;
  path: EntityMiniModel[];
}

export interface ChannelExtendedModel {
  id: string;
  title: string;
  description?: string | null;
  icon_key: string;
  visibility: ChannelVisibility;
  auto_join: boolean;
  management: string[];
  user_access_level?: AccessLevel;
  permissions: Permission[];
  created_by?: UserMiniModel;
}

export interface ChannelMemberUpsertModel {
  user_id: string;
  access_level: SimpleAccessLevel;
}

export interface ChannelModel {
  id: string;
  title: string;
  description?: string | null;
  icon_key: string;
  visibility: ChannelVisibility;
  auto_join: boolean;
  management: string[];
  user_access_level?: AccessLevel;
  stats: ChannelStatModel;
  permissions: Permission[];
  created_by?: UserMiniModel;
}

export interface ChannelStatModel {
  /** @format int32 */
  members_count: number;
}

export interface ChannelUpsertModel {
  title: string;
  description?: string | null;
  icon_key: string;
  visibility: ChannelVisibility;
  auto_join: boolean;
  management?: string[] | null;
  members?: ChannelMemberUpsertModel[] | null;
}

export enum ChannelVisibility {
  Private = 'private',
  Open = 'open',
}

export interface CommentModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  text: string;
  reply_to_id: string;
  user_id: string;
  created_by?: UserMiniModel;
  overrides?: DiscussionItemOverridesModel;
  /** @format int32 */
  user_mentions: number;
  documents: DocumentModel[];
  is_new: boolean;
  reaction_count: ReactionCountModel[];
  user_reaction: ReactionModel;
}

export interface CommentModelListPage {
  /** @format int32 */
  total: number;
  items: CommentModel[];
}

export interface CommentPatchModel {
  text: string;
  reply_to_id?: string | null;
  documents_to_add?: DocumentInsertModel[] | null;
  documents_to_delete?: string[] | null;
}

export interface CommentUpsertModel {
  text: string;
  reply_to_id?: string | null;
  documents_to_add?: DocumentInsertModel[] | null;
  documents_to_delete?: string[] | null;
}

export interface CommunityEntityChannelModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  title: string;
  short_title: string;
  short_description: string;
  description: string;
  status: string;
  label: string;
  type: CommunityEntityType;
  banner_url: string;
  banner_url_sm: string;
  logo_url: string;
  logo_url_sm: string;
  listing_privacy: PrivacyLevel;
  content_privacy: PrivacyLevel;
  joining_restriction: JoiningRestrictionLevel;
  home_channel_id?: string | null;
  user_access_level: string;
  stats: CommunityEntityStatModel;
  user_access: CommunityEntityPermissionModel;
  onboarding: OnboardingConfigurationModel;
  user_onboarded: boolean;
  contribution: string;
  listing_origin: AccessOriginModel;
  content_origin: AccessOriginModel;
  interests: string[];
  keywords: string[];
  links: LinkModel[];
  user_invitation: InvitationModelUser;
  channels: ChannelExtendedModel[];
  /** @format int32 */
  level: number;
}

export interface CommunityEntityDetailStatModel {
  /** @format int32 */
  members_count: number;
  /** @format int32 */
  workspaces_count: number;
  /** @format int32 */
  cfp_count: number;
  /** @format int32 */
  organization_count: number;
  /** @format int32 */
  hubs_count: number;
  /** @format int32 */
  needs_count: number;
  /** @format int32 */
  content_entity_count: number;
  /** @format int32 */
  participant_count: number;
  /** @format int32 */
  needs_count_aggregate: number;
  /** @format int32 */
  documents_count: number;
  /** @format int32 */
  documents_count_aggregate: number;
  /** @format int32 */
  papers_count: number;
  /** @format int32 */
  papers_count_aggregate: number;
  /** @format int32 */
  resources_count: number;
  /** @format int32 */
  resources_count_aggregate: number;
  /** @format int32 */
  proposal_count: number;
}

export interface CommunityEntityInvitationModelSource {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  title: string;
  short_title: string;
  short_description: string;
  description: string;
  status: string;
  label: string;
  type: CommunityEntityType;
  banner_url: string;
  banner_url_sm: string;
  logo_url: string;
  logo_url_sm: string;
  listing_privacy: PrivacyLevel;
  content_privacy: PrivacyLevel;
  joining_restriction: JoiningRestrictionLevel;
  home_channel_id?: string | null;
  user_access_level: string;
  stats: CommunityEntityStatModel;
  user_access: CommunityEntityPermissionModel;
  onboarding: OnboardingConfigurationModel;
  user_onboarded: boolean;
  contribution: string;
  listing_origin: AccessOriginModel;
  content_origin: AccessOriginModel;
  interests: string[];
  keywords: string[];
  links: LinkModel[];
  user_invitation: InvitationModelUser;
  invitation_id: string;
}

export interface CommunityEntityMiniModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  title: string;
  short_title: string;
  short_description: string;
  description: string;
  status: string;
  label: string;
  type: CommunityEntityType;
  banner_url: string;
  banner_url_sm: string;
  logo_url: string;
  logo_url_sm: string;
  listing_privacy: PrivacyLevel;
  content_privacy: PrivacyLevel;
  joining_restriction: JoiningRestrictionLevel;
  home_channel_id?: string | null;
  user_access_level: string;
  stats: CommunityEntityStatModel;
  user_access: CommunityEntityPermissionModel;
  onboarding: OnboardingConfigurationModel;
  user_onboarded: boolean;
  contribution: string;
  listing_origin: AccessOriginModel;
  content_origin: AccessOriginModel;
  interests: string[];
  keywords: string[];
  links: LinkModel[];
  user_invitation: InvitationModelUser;
}

export interface CommunityEntityMiniModelListPage {
  /** @format int32 */
  total: number;
  items: CommunityEntityMiniModel[];
}

export interface CommunityEntityPermissionModel {
  permissions: Permission[];
}

export interface CommunityEntityStatModel {
  /** @format int32 */
  members_count: number;
  /** @format int32 */
  workspaces_count: number;
  /** @format int32 */
  cfp_count: number;
  /** @format int32 */
  organization_count: number;
  /** @format int32 */
  hubs_count: number;
  /** @format int32 */
  needs_count: number;
  /** @format int32 */
  content_entity_count: number;
  /** @format int32 */
  participant_count: number;
}

export enum CommunityEntityType {
  Project = 'project',
  Workspace = 'workspace',
  Node = 'node',
  Organization = 'organization',
  Callforproposal = 'callforproposal',
  Channel = 'channel',
}

export interface ContactModel {
  subject: string;
  text: string;
}

export enum ContentEntityFilter {
  Posts = 'posts',
  Mentions = 'mentions',
  Threads = 'threads',
}

export interface ContentEntityModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  type: ContentEntityType;
  users: UserMiniModel[];
  text?: string | null;
  status: ContentEntityStatus;
  created_by?: UserMiniModel;
  overrides?: DiscussionItemOverridesModel;
  feed_entity?: EntityMiniModel;
  parent_feed_entity?: EntityMiniModel;
  documents: DocumentModel[];
  reaction_count: ReactionCountModel[];
  /** @format int32 */
  comment_count: number;
  /** @format int32 */
  new_comment_count: number;
  user_reaction: ReactionModel;
  /** @format int32 */
  user_mentions: number;
  /** @format int32 */
  user_mentions_in_comments: number;
  image_url: string;
  image_url_sm: string;
  feed_stats: FeedStatModel;
  is_new: boolean;
}

export interface ContentEntityModelListPage {
  /** @format int32 */
  total: number;
  items: ContentEntityModel[];
}

export interface ContentEntityPatchModel {
  type: ContentEntityType;
  /** @deprecated */
  user_ids?: string[] | null;
  text?: string | null;
  status: ContentEntityStatus;
  visibility: ContentEntityVisibility;
  documents_to_add?: DocumentInsertModel[] | null;
}

export enum ContentEntityStatus {
  Active = 'active',
  Draft = 'draft',
}

export enum ContentEntityType {
  Announcement = 'announcement',
  Jogldoc = 'jogldoc',
  Preprint = 'preprint',
  Article = 'article',
  Protocol = 'protocol',
  Need = 'need',
  Paper = 'paper',
}

export interface ContentEntityUpsertModel {
  type: ContentEntityType;
  /** @deprecated */
  user_ids?: string[] | null;
  text?: string | null;
  status: ContentEntityStatus;
  visibility: ContentEntityVisibility;
  documents_to_add?: DocumentInsertModel[] | null;
}

export enum ContentEntityVisibility {
  Public = 'public',
  Entity = 'entity',
  Event = 'event',
  Ecosystem = 'ecosystem',
  Authors = 'authors',
}

export interface DiscussionItemModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  feed_id: string;
  content_entity_id: string;
  is_reply: boolean;
  text: string;
  reply_to_text: string;
  created_by?: UserMiniModel;
  overrides?: DiscussionItemOverridesModel;
  feed_entity?: EntityMiniModel;
  is_new: boolean;
  source: DiscussionItemSource;
}

export interface DiscussionItemOverridesModel {
  user_image_url: string;
  user_url: string;
  user_name: string;
}

export enum DiscussionItemSource {
  Post = 'post',
  Reply = 'reply',
  Mention = 'mention',
}

export interface DiscussionModel {
  /** @format int32 */
  total: number;
  items: ContentEntityModel[];
  /** @format int32 */
  unread_posts: number;
  /** @format int32 */
  unread_mentions: number;
  /** @format int32 */
  unread_threads: number;
  permissions: Permission[];
  feed_entity?: EntityMiniModel;
  parent_feed_entity?: EntityMiniModel;
}

export enum DiscussionParticipation {
  Public = 'public',
  Ecosystem = 'ecosystem',
  Private = 'private',
  Participants = 'participants',
  Adminonly = 'adminonly',
}

export interface DocumentConversionUpsertModel {
  data: string;
}

export enum DocumentFilter {
  Document = 'document',
  File = 'file',
  Media = 'media',
  Link = 'link',
  Jogldoc = 'jogldoc',
}

export interface DocumentInsertModel {
  title: string;
  file_name?: string | null;
  url?: string | null;
  type: DocumentType;
  description?: string | null;
  data?: string | null;
  user_ids?: string[] | null;
  status?: ContentEntityStatus;
  default_visibility?: FeedEntityVisibility;
  user_visibility?: FeedEntityUserVisibilityUpsertModel[] | null;
  communityentity_visibility?: FeedEntityCommunityEntityVisibilityUpsertModel[] | null;
  image_id?: string | null;
  folder_id?: string | null;
  keywords?: string[] | null;
}

export interface DocumentModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  /** @format date-time */
  opened?: string | null;
  id: string;
  title: string;
  file_name: string;
  file_type: string;
  /** @format int32 */
  file_size_bytes: number;
  url: string;
  type: DocumentType;
  description: string;
  data: string;
  image_url: string;
  document_url: string;
  image_url_sm: string;
  created_by?: UserMiniModel;
  feed_entity?: EntityMiniModel;
  users: UserMiniModel[];
  feed_stats: FeedStatModel;
  status: ContentEntityStatus;
  visibility: ContentEntityVisibility;
  default_visibility?: FeedEntityVisibility;
  user_visibility?: FeedEntityUserVisibilityModel[] | null;
  communityentity_visibility?: FeedEntityCommunityEntityVisibilityModel[] | null;
  image_id: string;
  folder_id: string;
  keywords?: string[] | null;
  is_folder: boolean;
  is_new: boolean;
  is_media: boolean;
  permissions: Permission[];
  path: EntityMiniModel[];
}

export interface DocumentModelListPage {
  /** @format int32 */
  total: number;
  items: DocumentModel[];
}

export interface DocumentOrFolderModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  title: string;
  url?: string | null;
  type?: DocumentType;
  description?: string | null;
  created_by?: UserMiniModel;
  feed_stats?: FeedStatModel;
  feed_entity?: EntityMiniModel;
  image_id?: string | null;
  folder_id?: string | null;
  is_folder: boolean;
  is_new: boolean;
  parent_folder_id?: string | null;
}

export enum DocumentType {
  Document = 'document',
  Link = 'link',
  Jogldoc = 'jogldoc',
}

export interface DocumentUpdateModel {
  title: string;
  url?: string | null;
  type: DocumentType;
  description?: string | null;
  user_ids?: string[] | null;
  status?: ContentEntityStatus;
  default_visibility?: FeedEntityVisibility;
  user_visibility?: FeedEntityUserVisibilityUpsertModel[] | null;
  communityentity_visibility?: FeedEntityCommunityEntityVisibilityUpsertModel[] | null;
  image_id?: string | null;
  folder_id?: string | null;
  keywords?: string[] | null;
}

export interface Education {
  school: string;
  department_name: string;
  degree_name: string;
  dateFrom: string;
  dateTo: string;
}

export interface EmailModel {
  email: string;
}

export interface Employment {
  company: string;
  department_name: string;
  position: string;
  date_from: string;
  date_to: string;
}

export interface EntityMiniModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  full_name: string;
  short_title: string;
  title: string;
  short_description: string;
  label?: string | null;
  banner_url: string;
  banner_url_sm: string;
  logo_url: string;
  logo_url_sm: string;
  entity_type: FeedType;
}

export interface ErrorModel {
  error: string;
}

export interface EventAttendanceAccessLevelModel {
  access_level: AttendanceAccessLevel;
}

export interface EventAttendanceLabelBatchModel {
  attendance_ids: string[];
  labels?: string[] | null;
}

export interface EventAttendanceLevelBatchModel {
  attendance_ids: string[];
  access_level: AttendanceAccessLevel;
}

export interface EventAttendanceModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  event_id: string;
  user: UserMiniModel;
  user_email: string;
  community_entity: CommunityEntityMiniModel;
  origin_community_entity: CommunityEntityMiniModel;
  labels?: string[] | null;
  status: AttendanceStatus;
  access_level: AttendanceAccessLevel;
  created_by?: UserMiniModel;
}

export interface EventAttendanceUpsertModel {
  user_id?: string | null;
  user_email?: string | null;
  community_entity_id?: string | null;
  origin_community_entity_id?: string | null;
  labels?: string[] | null;
  access_level: AttendanceAccessLevel;
}

export interface EventModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  title: string;
  description: string;
  generate_meet_link: boolean;
  meeting_url: string;
  generated_meeting_url: string;
  banner_id: string;
  banner_url: string;
  banner_url_sm: string;
  visibility: EventVisibility;
  /** @format date-time */
  start: string;
  /** @format date-time */
  end: string;
  timezone: TimezoneModel;
  feed_stats: FeedStatModel;
  permissions: Permission[];
  keywords: string[];
  location: GeolocationModel;
  feed_entity?: EntityMiniModel;
  user_attendance: EventAttendanceModel;
  /** @format int32 */
  attendee_count: number;
  /** @format int32 */
  invitee_count: number;
  is_new: boolean;
  path: EntityMiniModel[];
}

export interface EventModelListPage {
  /** @format int32 */
  total: number;
  items: EventModel[];
}

export enum EventTag {
  Invited = 'invited',
  Attending = 'attending',
  Rejected = 'rejected',
  Organizer = 'organizer',
  Speaker = 'speaker',
  Attendee = 'attendee',
  Online = 'online',
  Physical = 'physical',
}

export interface EventUpsertModel {
  title: string;
  description?: string | null;
  generate_meet_link: boolean;
  meeting_url?: string | null;
  banner_id?: string | null;
  visibility: EventVisibility;
  /** @format date-time */
  start: string;
  /** @format date-time */
  end: string;
  timezone: TimezoneModel;
  keywords?: string[] | null;
  location?: GeolocationModel;
  attendances_to_upsert?: EventAttendanceUpsertModel[] | null;
}

export enum EventVisibility {
  Private = 'private',
  Container = 'container',
  Public = 'public',
}

export enum ExternalSystem {
  Orcid = 'orcid',
  Semanticscholar = 'semanticscholar',
  Openalex = 'openalex',
  Pubmed = 'pubmed',
  None = 'none',
}

export interface FAQItem {
  question: string;
  answer: string;
}

export interface FeedEntityCommunityEntityVisibilityModel {
  visibility: FeedEntityVisibility;
  community_entity: CommunityEntityMiniModel;
}

export interface FeedEntityCommunityEntityVisibilityUpsertModel {
  visibility: FeedEntityVisibility;
  community_entity_id: string;
}

export enum FeedEntityFilter {
  Createdbyuser = 'createdbyuser',
  Sharedwithuser = 'sharedwithuser',
  Openedbyuser = 'openedbyuser',
}

export interface FeedEntityUserVisibilityModel {
  visibility: FeedEntityVisibility;
  user: UserMiniModel;
}

export interface FeedEntityUserVisibilityUpsertModel {
  visibility: FeedEntityVisibility;
  user_id: string;
}

export enum FeedEntityVisibility {
  View = 'view',
  Comment = 'comment',
  Edit = 'edit',
}

export interface FeedIntegrationModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  type: FeedIntegrationType;
  source_id?: string | null;
  source_url?: string | null;
}

export interface FeedIntegrationTokenModel {
  type: FeedIntegrationType;
  authorization_code?: string | null;
}

export enum FeedIntegrationType {
  Github = 'github',
  Huggingface = 'huggingface',
  Arxiv = 'arxiv',
  Joglagentpublication = 'joglagentpublication',
  Pubmed = 'pubmed',
}

export interface FeedIntegrationUpsertModel {
  type: FeedIntegrationType;
  source_id?: string | null;
  source_url?: string | null;
  access_token?: string | null;
}

export interface FeedModel {
  id: string;
  type: FeedType;
}

export interface FeedStatModel {
  /** @format int32 */
  post_count: number;
  /** @format int32 */
  new_post_count: number;
  /** @format int32 */
  new_mention_count: number;
  /** @format int32 */
  new_thread_activity_count: number;
  /**
   * @deprecated
   * @format int32
   */
  comment_count: number;
}

export enum FeedType {
  Project = 'project',
  Workspace = 'workspace',
  Node = 'node',
  Need = 'need',
  User = 'user',
  Organization = 'organization',
  Paper = 'paper',
  Document = 'document',
  Callforproposal = 'callforproposal',
  Event = 'event',
  Channel = 'channel',
  Resource = 'resource',
}

export interface FolderModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  name: string;
  parent_folder_id: string;
  is_folder: boolean;
}

export interface FolderUpsertModel {
  name: string;
  parent_folder_id?: string | null;
}

export interface GeolocationModel {
  name: string;
  url: string;
}

export interface ImageConversionUpsertModel {
  to_format: string;
  data: string;
}

export interface ImageInsertModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  file_name: string;
  data: string;
}

export interface ImageInsertResultModel {
  id: string;
  url: string;
}

export interface ImageModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  file_name: string;
  file_type: string;
  data: string;
}

export interface InvitationMassUpsertModel {
  invitees: InvitationUpsertModel[];
  community_entity_ids: string[];
}

export interface InvitationModelEntity {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  title: string;
  short_title: string;
  short_description: string;
  description: string;
  status: string;
  label: string;
  type: CommunityEntityType;
  banner_url: string;
  banner_url_sm: string;
  logo_url: string;
  logo_url_sm: string;
  listing_privacy: PrivacyLevel;
  content_privacy: PrivacyLevel;
  joining_restriction: JoiningRestrictionLevel;
  home_channel_id?: string | null;
  user_access_level: string;
  stats: CommunityEntityStatModel;
  user_access: CommunityEntityPermissionModel;
  onboarding: OnboardingConfigurationModel;
  user_onboarded: boolean;
  contribution: string;
  listing_origin: AccessOriginModel;
  content_origin: AccessOriginModel;
  interests: string[];
  keywords: string[];
  links: LinkModel[];
  user_invitation: InvitationModelUser;
  invitation_id: string;
  entity_type: CommunityEntityType;
  invitation_type: InvitationType;
}

export interface InvitationModelUser {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  first_name: string;
  last_name: string;
  username: string;
  short_bio: string;
  banner_url: string;
  banner_url_sm: string;
  logo_url: string;
  logo_url_sm: string;
  status: string;
  country: string;
  city: string;
  /** @format int32 */
  follower_count: number;
  user_follows: boolean;
  stats: CommunityEntityStatModel;
  experience: UserExperienceModel[];
  education: UserEducationModel[];
  spaces: CommunityEntityMiniModel[];
  invitation_id: string;
  email: string;
  invitation_type: InvitationType;
}

export enum InvitationType {
  Invitation = 'invitation',
  Request = 'request',
}

export interface InvitationUpsertModel {
  user_email?: string | null;
  user_id?: string | null;
  access_level: AccessLevel;
}

export enum JoiningRestrictionLevel {
  Invite = 'invite',
  Request = 'request',
  Open = 'open',
  Custom = 'custom',
}

export interface JoiningRestrictionLevelSettingModel {
  communityEntityId?: string;
  level: JoiningRestrictionLevel;
}

export interface JsonNode {
  options?: JsonNodeOptions;
  parent?: JsonNode;
  root?: JsonNode;
}

export interface JsonNodeOptions {
  propertyNameCaseInsensitive?: boolean;
}

export interface Link {
  type?: string | null;
  title: string;
  url: string;
}

export interface LinkModel {
  type?: string | null;
  title?: string | null;
  url: string;
}

export interface MemberModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  first_name: string;
  last_name: string;
  username: string;
  short_bio: string;
  banner_url: string;
  banner_url_sm: string;
  logo_url: string;
  logo_url_sm: string;
  status: string;
  country: string;
  city: string;
  /** @format int32 */
  follower_count: number;
  user_follows: boolean;
  stats: CommunityEntityStatModel;
  experience: UserExperienceModel[];
  education: UserEducationModel[];
  spaces: CommunityEntityMiniModel[];
  access_level: AccessLevel;
  membership_id: string;
  contribution: string;
  labels: string[];
}

export interface MembershipModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  access_level: AccessLevel;
  community_entity_id: string;
  contribution: string;
  community_entity_type: CommunityEntityType;
}

export interface MessageModel {
  user_ids: string[];
  subject: string;
  message: string;
}

export interface NeedModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  /** @format date-time */
  opened?: string | null;
  id: string;
  title: string;
  description: string;
  /** @format date-time */
  end_date?: string | null;
  keywords: string[];
  interests: string[];
  type: NeedType;
  default_visibility?: FeedEntityVisibility;
  user_visibility?: FeedEntityUserVisibilityModel[] | null;
  communityentity_visibility?: FeedEntityCommunityEntityVisibilityModel[] | null;
  created_by?: UserMiniModel;
  entity: CommunityEntityMiniModel;
  feed_stats: FeedStatModel;
  is_new: boolean;
  permissions: Permission[];
  path: EntityMiniModel[];
}

export interface NeedModelListPage {
  /** @format int32 */
  total: number;
  items: NeedModel[];
}

export enum NeedType {
  Funding = 'funding',
  Equipment = 'equipment',
  Expertise = 'expertise',
  Softwarelicense = 'softwarelicense',
  Otherlicense = 'otherlicense',
  Tasks = 'tasks',
}

export interface NeedUpsertModel {
  title: string;
  description?: string | null;
  /** @format date-time */
  end_date?: string | null;
  keywords?: string[] | null;
  interests?: string[] | null;
  type?: NeedType;
  default_visibility?: FeedEntityVisibility;
  user_visibility?: FeedEntityUserVisibilityUpsertModel[] | null;
  communityentity_visibility?: FeedEntityCommunityEntityVisibilityUpsertModel[] | null;
}

export interface NodeDetailModel {
  id: string;
  title: string;
  short_title: string;
  description: string;
  short_description: string;
  status: string;
  label: string;
  banner_url_sm: string;
  banner_url: string;
  logo_url_sm: string;
  logo_url: string;
  feed_id: string;
  interests: string[];
  keywords: string[];
  links: LinkModel[];
  tabs: string[];
  listing_privacy: PrivacyLevel;
  content_privacy: PrivacyLevel;
  content_privacy_custom_settings: PrivacyLevelSettingModel[];
  joining_restriction: JoiningRestrictionLevel;
  joining_restriction_custom_settings: JoiningRestrictionLevelSettingModel[];
  management: string[];
  home_channel_id?: string | null;
  user_access_level: string;
  user_access: CommunityEntityPermissionModel;
  user_onboarded: boolean;
  contribution: string;
  listing_origin: AccessOriginModel;
  path: EntityMiniModel[];
  user_invitation: InvitationModelUser;
  onboarding: OnboardingConfigurationUpsertModel;
  faq: FAQItem[];
  external_website: string;
  stats: CommunityEntityDetailStatModel;
  created_by?: UserMiniModel;
}

export interface NodeFeedDataModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  title: string;
  short_title: string;
  short_description: string;
  description: string;
  status: string;
  label: string;
  type: CommunityEntityType;
  banner_url: string;
  banner_url_sm: string;
  logo_url: string;
  logo_url_sm: string;
  listing_privacy: PrivacyLevel;
  content_privacy: PrivacyLevel;
  joining_restriction: JoiningRestrictionLevel;
  home_channel_id?: string | null;
  user_access_level: string;
  stats: CommunityEntityStatModel;
  user_access: CommunityEntityPermissionModel;
  onboarding: OnboardingConfigurationModel;
  user_onboarded: boolean;
  contribution: string;
  listing_origin: AccessOriginModel;
  content_origin: AccessOriginModel;
  interests: string[];
  keywords: string[];
  links: LinkModel[];
  user_invitation: InvitationModelUser;
  entities: CommunityEntityChannelModel[];
  new_events: boolean;
  new_needs: boolean;
  new_documents: boolean;
  new_papers: boolean;
  /** @format int32 */
  unread_posts_total: number;
  /** @format int32 */
  unread_mentions_total: number;
  /** @format int32 */
  unread_threads_total: number;
  /** @format int32 */
  unread_posts_events: number;
  /** @format int32 */
  unread_mentions_events: number;
  /** @format int32 */
  unread_threads_events: number;
  /** @format int32 */
  unread_posts_needs: number;
  /** @format int32 */
  unread_mentions_needs: number;
  /** @format int32 */
  unread_threads_needs: number;
  /** @format int32 */
  unread_posts_documents: number;
  /** @format int32 */
  unread_mentions_documents: number;
  /** @format int32 */
  unread_threads_documents: number;
  /** @format int32 */
  unread_posts_papers: number;
  /** @format int32 */
  unread_mentions_papers: number;
  /** @format int32 */
  unread_threads_papers: number;
}

export interface NodeModel {
  id: string;
  title: string;
  short_title: string;
  description: string;
  short_description: string;
  status: string;
  label: string;
  banner_url_sm: string;
  banner_url: string;
  logo_url_sm: string;
  logo_url: string;
  feed_id: string;
  interests: string[];
  keywords: string[];
  links: LinkModel[];
  tabs: string[];
  listing_privacy: PrivacyLevel;
  content_privacy: PrivacyLevel;
  content_privacy_custom_settings: PrivacyLevelSettingModel[];
  joining_restriction: JoiningRestrictionLevel;
  joining_restriction_custom_settings: JoiningRestrictionLevelSettingModel[];
  management: string[];
  home_channel_id?: string | null;
  user_access_level: string;
  stats: CommunityEntityStatModel;
  user_access: CommunityEntityPermissionModel;
  user_onboarded: boolean;
  contribution: string;
  listing_origin: AccessOriginModel;
  path: EntityMiniModel[];
  user_invitation: InvitationModelUser;
  onboarding: OnboardingConfigurationUpsertModel;
  faq: FAQItem[];
  external_website: string;
}

export interface NodePatchModel {
  title?: string | null;
  short_title?: string | null;
  description?: string | null;
  short_description?: string | null;
  status?: string | null;
  banner_id?: string | null;
  logo_id?: string | null;
  interests?: string[] | null;
  keywords?: string[] | null;
  links?: LinkModel[] | null;
  tabs?: string[] | null;
  listing_privacy?: PrivacyLevel;
  content_privacy?: PrivacyLevel;
  content_privacy_custom_settings?: PrivacyLevelSettingModel[] | null;
  joining_restriction?: JoiningRestrictionLevel;
  joining_restriction_custom_settings?: JoiningRestrictionLevelSettingModel[] | null;
  management?: string[] | null;
  onboarding?: OnboardingConfigurationUpsertModel;
  faq?: FAQItem[] | null;
  external_website?: string | null;
}

export interface NodeUpsertModel {
  title: string;
  short_title: string;
  description?: string | null;
  short_description: string;
  status: string;
  banner_id?: string | null;
  logo_id?: string | null;
  interests?: string[] | null;
  keywords?: string[] | null;
  links?: LinkModel[] | null;
  tabs?: string[] | null;
  listing_privacy?: PrivacyLevel;
  content_privacy?: PrivacyLevel;
  content_privacy_custom_settings?: PrivacyLevelSettingModel[] | null;
  joining_restriction?: JoiningRestrictionLevel;
  joining_restriction_custom_settings?: JoiningRestrictionLevelSettingModel[] | null;
  management?: string[] | null;
  home_channel_id?: string | null;
  onboarding?: OnboardingConfigurationUpsertModel;
  faq?: FAQItem[] | null;
  external_website?: string | null;
}

export enum NotificationDataKey {
  Communityentity = 'communityentity',
  Contententity = 'contententity',
  User = 'user',
  Role = 'role',
  Invitation = 'invitation',
  Feedentity = 'feedentity',
}

export interface NotificationDataModel {
  key: NotificationDataKey;
  entity_id: string;
  community_entity_type?: CommunityEntityType;
  community_entity_onboarding?: OnboardingConfigurationModel;
  content_entity_type?: ContentEntityType;
  entity_title: string;
  entity_subtype: string;
  entity_logo_url?: string | null;
  entity_logo_url_sm?: string | null;
  entity_banner_url?: string | null;
  entity_banner_url_sm?: string | null;
  entity_onboarding_enabled: boolean;
  entity_home_channel_id?: string | null;
}

export interface NotificationModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  type: NotificationType;
  actioned: boolean;
  data: NotificationDataModel[];
  /** @format int32 */
  count: number;
}

export interface NotificationModelListPage {
  /** @format int32 */
  total: number;
  items: NotificationModel[];
}

export enum NotificationType {
  Invite = 'invite',
  Adminrequest = 'adminrequest',
  Admincommunityentityinvite = 'admincommunityentityinvite',
  Acceptation = 'acceptation',
  Adminaccesslevel = 'adminaccesslevel',
  Follower = 'follower',
  Need = 'need',
  Resource = 'resource',
  Note = 'note',
  Noteauthor = 'noteauthor',
  Paper = 'paper',
  Relation = 'relation',
  Member = 'member',
  Comment = 'comment',
  Noteedit = 'noteedit',
  Adminrelation = 'adminrelation',
  Adminmember = 'adminmember',
  Mention = 'mention',
  Eventinvite = 'eventinvite',
  Eventinviteupdate = 'eventinviteupdate',
}

export interface OnboardingConfigurationModel {
  enabled: boolean;
  presentation: OnboardingPresentationModel;
  questionnaire: OnboardingQuestionnaireModel;
  rules: OnboardingRulesModel;
}

export interface OnboardingConfigurationUpsertModel {
  enabled: boolean;
  presentation: OnboardingPresentationUpsertModel;
  questionnaire: OnboardingQuestionnaireModel;
  rules: OnboardingRulesModel;
}

export interface OnboardingPresentationItemModel {
  image_id: string;
  image_url: string;
  title: string;
  text: string;
}

export interface OnboardingPresentationItemUpsertModel {
  image_id: string;
  title: string;
  text: string;
}

export interface OnboardingPresentationModel {
  enabled: boolean;
  items: OnboardingPresentationItemModel[];
}

export interface OnboardingPresentationUpsertModel {
  enabled: boolean;
  items: OnboardingPresentationItemUpsertModel[];
}

export interface OnboardingQuestionnaireInstanceItemModel {
  question: string;
  answer: string;
}

export interface OnboardingQuestionnaireInstanceModel {
  items: OnboardingQuestionnaireInstanceItemModel[];
  /** @format date-time */
  completed_at: string;
}

export interface OnboardingQuestionnaireInstanceUpsertModel {
  items: OnboardingQuestionnaireInstanceItemModel[];
}

export interface OnboardingQuestionnaireItemModel {
  question: string;
}

export interface OnboardingQuestionnaireModel {
  enabled: boolean;
  items: OnboardingQuestionnaireItemModel[];
}

export interface OnboardingRulesModel {
  enabled: boolean;
  text: string;
}

export interface OnboardingUpsertModel {
  paperIds: string[];
  repos: string[];
  github_access_token?: string | null;
  huggingface_access_token?: string | null;
  experience?: UserExperienceModel[] | null;
  education?: UserEducationModel[] | null;
}

export interface OrcidExperienceModel {
  educations: Education[];
  employments: Employment[];
}

export interface OrcidLoadModel {
  authorization_code: string;
}

export interface OrcidRegistrationModel {
  authorization_code: string;
  screen: string;
}

export interface OrganizationDetailModel {
  id: string;
  title: string;
  short_title: string;
  description: string;
  short_description: string;
  status: string;
  label: string;
  banner_url_sm: string;
  banner_url: string;
  logo_url_sm: string;
  logo_url: string;
  feed_id: string;
  interests: string[];
  keywords: string[];
  links: LinkModel[];
  tabs: string[];
  listing_privacy: PrivacyLevel;
  content_privacy: PrivacyLevel;
  content_privacy_custom_settings: PrivacyLevelSettingModel[];
  joining_restriction: JoiningRestrictionLevel;
  joining_restriction_custom_settings: JoiningRestrictionLevelSettingModel[];
  management: string[];
  home_channel_id?: string | null;
  user_access_level: string;
  user_access: CommunityEntityPermissionModel;
  user_onboarded: boolean;
  contribution: string;
  listing_origin: AccessOriginModel;
  path: EntityMiniModel[];
  user_invitation: InvitationModelUser;
  address: string;
  stats: CommunityEntityDetailStatModel;
  created_by?: UserMiniModel;
}

export interface OrganizationModel {
  id: string;
  title: string;
  short_title: string;
  description: string;
  short_description: string;
  status: string;
  label: string;
  banner_url_sm: string;
  banner_url: string;
  logo_url_sm: string;
  logo_url: string;
  feed_id: string;
  interests: string[];
  keywords: string[];
  links: LinkModel[];
  tabs: string[];
  listing_privacy: PrivacyLevel;
  content_privacy: PrivacyLevel;
  content_privacy_custom_settings: PrivacyLevelSettingModel[];
  joining_restriction: JoiningRestrictionLevel;
  joining_restriction_custom_settings: JoiningRestrictionLevelSettingModel[];
  management: string[];
  home_channel_id?: string | null;
  user_access_level: string;
  stats: CommunityEntityStatModel;
  user_access: CommunityEntityPermissionModel;
  user_onboarded: boolean;
  contribution: string;
  listing_origin: AccessOriginModel;
  path: EntityMiniModel[];
  user_invitation: InvitationModelUser;
  address: string;
}

export interface OrganizationPatchModel {
  title: string;
  short_title: string;
  description?: string | null;
  short_description: string;
  status: string;
  banner_id?: string | null;
  logo_id?: string | null;
  interests?: string[] | null;
  keywords?: string[] | null;
  links?: LinkModel[] | null;
  tabs?: string[] | null;
  listing_privacy?: PrivacyLevel;
  content_privacy?: PrivacyLevel;
  content_privacy_custom_settings?: PrivacyLevelSettingModel[] | null;
  joining_restriction?: JoiningRestrictionLevel;
  joining_restriction_custom_settings?: JoiningRestrictionLevelSettingModel[] | null;
  management?: string[] | null;
  home_channel_id?: string | null;
  address?: string | null;
}

export interface OrganizationUpsertModel {
  title: string;
  short_title: string;
  description?: string | null;
  short_description: string;
  status: string;
  banner_id?: string | null;
  logo_id?: string | null;
  interests?: string[] | null;
  keywords?: string[] | null;
  links?: LinkModel[] | null;
  tabs?: string[] | null;
  listing_privacy?: PrivacyLevel;
  content_privacy?: PrivacyLevel;
  content_privacy_custom_settings?: PrivacyLevelSettingModel[] | null;
  joining_restriction?: JoiningRestrictionLevel;
  joining_restriction_custom_settings?: JoiningRestrictionLevelSettingModel[] | null;
  management?: string[] | null;
  home_channel_id?: string | null;
  address?: string | null;
}

export interface PaperModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  /** @format date-time */
  opened?: string | null;
  id: string;
  title: string;
  summary: string;
  authors: string;
  journal: string;
  external_id: string;
  tags: string[];
  type: PaperType;
  external_system: ExternalSystem;
  publication_date: string;
  created_by?: UserMiniModel;
  feed_entity?: EntityMiniModel;
  user_ids: string[];
  default_visibility?: FeedEntityVisibility;
  user_visibility?: FeedEntityUserVisibilityModel[] | null;
  communityentity_visibility?: FeedEntityCommunityEntityVisibilityModel[] | null;
  open_access_pdf: string;
  /**
   * @deprecated
   * @format int32
   */
  feed_count: number;
  feed_stats: FeedStatModel;
  is_new: boolean;
  permissions: Permission[];
  path: EntityMiniModel[];
}

export interface PaperModelListPage {
  /** @format int32 */
  total: number;
  items: PaperModel[];
}

export interface PaperModelOA {
  title: string;
  abstract: string;
  journal: string;
  publication_date: string;
  authors: string;
  external_id: string;
  jogl_id: string;
  open_access_pdf: string;
  external_id_url: string;
  tags: string[];
}

export interface PaperModelOAListPage {
  /** @format int32 */
  total: number;
  items: PaperModelOA[];
}

export interface PaperModelOrcid {
  title: string;
  journal_title: string;
  abstract: string;
  publication_date: string;
  authors: string;
  external_id: string;
  external_url: string;
  type: PaperType;
  source_name: string;
  tags: string[];
}

export interface PaperModelPM {
  title: string;
  abstract: string;
  journal: string;
  publication_date: string;
  authors: string;
  external_id: string;
  jogl_id: string;
}

export interface PaperModelPMListPage {
  /** @format int32 */
  total: number;
  items: PaperModelPM[];
}

export interface PaperModelS2 {
  title: string;
  abstract: string;
  journal: string;
  publication_date: string;
  authors: string;
  external_id: string;
  jogl_id: string;
  open_access_pdf: string;
}

export interface PaperModelS2ListPage {
  /** @format int32 */
  total: number;
  items: PaperModelS2[];
}

export enum PaperTag {
  Reference = 'reference',
  Aggregated = 'aggregated',
  Library = 'library',
  Authoredbyme = 'authoredbyme',
  Authorof = 'authorof',
  Workspace = 'workspace',
  Node = 'node',
}

export enum PaperType {
  Article = 'article',
  Preprint = 'preprint',
  Note = 'note',
}

export interface PaperUpsertModel {
  title: string;
  summary?: string | null;
  authors?: string | null;
  journal?: string | null;
  external_id?: string | null;
  tags?: string[] | null;
  type: PaperType;
  external_system: ExternalSystem;
  publication_date?: string | null;
  user_ids?: string[] | null;
  default_visibility?: FeedEntityVisibility;
  user_visibility?: FeedEntityUserVisibilityUpsertModel[] | null;
  communityentity_visibility?: FeedEntityCommunityEntityVisibilityUpsertModel[] | null;
  open_access_pdf?: string | null;
}

export enum Permission {
  Read = 'read',
  Manage = 'manage',
  Manageowners = 'manageowners',
  Delete = 'delete',
  Postneed = 'postneed',
  Postcontententity = 'postcontententity',
  Postcomment = 'postcomment',
  Managelibrary = 'managelibrary',
  Managedocuments = 'managedocuments',
  Postresources = 'postresources',
  Deletecontententity = 'deletecontententity',
  Deletecomment = 'deletecomment',
  Createworkspaces = 'createworkspaces',
  Createproposals = 'createproposals',
  Listproposals = 'listproposals',
  Scoreproposals = 'scoreproposals',
  Mentioneveryone = 'mentioneveryone',
  Createevents = 'createevents',
  Createchannels = 'createchannels',
  Invitemembers = 'invitemembers',
  Join = 'join',
  Membership = 'membership',
  Request = 'request',
}

export interface PortfolioItemModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  title: string;
  summary: string;
  authors: string;
  type: PortfolioItemType;
  feed_stats: FeedStatModel;
  permissions: Permission[];
  publication_date: string;
  keywords?: string[] | null;
  external_id: string;
  journal: string;
  url: string;
  source: string;
}

export enum PortfolioItemType {
  Paper = 'paper',
  Jogldoc = 'jogldoc',
  Repository = 'repository',
}

export enum PrivacyLevel {
  Public = 'public',
  Ecosystem = 'ecosystem',
  Private = 'private',
  Custom = 'custom',
}

export interface PrivacyLevelSettingModel {
  communityEntityId?: string;
  allowed: boolean;
}

export interface ProfileModel {
  education: UserEducationModel[];
  experience: UserExperienceModel[];
}

export interface ProposalAnswerModel {
  question_key: string;
  answer: string[];
  answer_document: DocumentModel;
}

export interface ProposalAnswerUpsertModel {
  question_key: string;
  answer: string[];
  answer_document_id: string;
}

export interface ProposalModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  project: EntityMiniModel;
  call_for_proposal: CallForProposalMiniModel;
  community: EntityMiniModel;
  title: string;
  description: string;
  status: ProposalStatus;
  users: UserMiniModel[];
  answers: ProposalAnswerModel[];
  /** @format double */
  score: number;
  /** @format date-time */
  submitted_at?: string | null;
}

export interface ProposalPatchModel {
  id?: string | null;
  project_id: string;
  call_for_proposal_id: string;
  title?: string | null;
  description?: string | null;
  answers?: ProposalAnswerUpsertModel[] | null;
}

export enum ProposalPrivacyLevel {
  Public = 'public',
  Ecosystem = 'ecosystem',
  Private = 'private',
  Adminandreviewers = 'adminandreviewers',
  Admin = 'admin',
}

export enum ProposalStatus {
  Draft = 'draft',
  Submitted = 'submitted',
  Rejected = 'rejected',
  Accepted = 'accepted',
}

export interface ProposalUpsertModel {
  id?: string | null;
  project_id: string;
  call_for_proposal_id: string;
  title?: string | null;
  description?: string | null;
  answers?: ProposalAnswerUpsertModel[] | null;
}

export interface ReactionCountModel {
  key: string;
  /** @format int32 */
  count: number;
}

export interface ReactionModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  key: string;
  user_id: string;
  created_by?: UserMiniModel;
}

export interface ReactionUpsertModel {
  key: string;
}

export interface RepositoryModel {
  name: string;
  url: string;
}

export interface ResourceModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  /** @format date-time */
  opened?: string | null;
  id: string;
  title: string;
  description: string;
  data?: Record<string, JsonNode>;
  type: ResourceType;
  default_visibility?: FeedEntityVisibility;
  user_visibility?: FeedEntityUserVisibilityModel[] | null;
  communityentity_visibility?: FeedEntityCommunityEntityVisibilityModel[] | null;
  permissions: Permission[];
}

export interface ResourceModelListPage {
  /** @format int32 */
  total: number;
  items: ResourceModel[];
}

export interface ResourceRepositoryUpsertModel {
  repo_url: string;
  access_token?: string | null;
}

export enum ResourceType {
  Repository = 'repository',
}

export interface ResourceUpsertModel {
  title: string;
  description?: string | null;
  data?: Record<string, JsonNode>;
  default_visibility?: FeedEntityVisibility;
  user_visibility?: FeedEntityUserVisibilityUpsertModel[] | null;
  communityentity_visibility?: FeedEntityCommunityEntityVisibilityUpsertModel[] | null;
}

export interface SearchResultGlobalModel {
  /** @format int64 */
  user_count: number;
  /** @format int64 */
  node_count: number;
  /** @format int64 */
  event_count: number;
  /** @format int64 */
  need_count: number;
  /** @format int64 */
  org_count: number;
}

export interface SearchResultNodeModel {
  /** @format int64 */
  user_count: number;
  /** @format int64 */
  workspace_count: number;
  /** @format int64 */
  event_count: number;
  /** @format int64 */
  need_count: number;
  /** @format int64 */
  doc_count: number;
  /** @format int64 */
  paper_count: number;
}

export enum SimpleAccessLevel {
  Member = 'member',
  Admin = 'admin',
}

export enum SortKey {
  Createddate = 'createddate',
  Updateddate = 'updateddate',
  Lastactivity = 'lastactivity',
  Recentlyopened = 'recentlyopened',
  Date = 'date',
  Alphabetical = 'alphabetical',
  Relevance = 'relevance',
  Invitationdate = 'invitationdate',
}

export interface TextValueModel {
  value: string;
}

export interface TimezoneModel {
  value: string;
  label: string;
}

export interface UserContactModel {
  FirstName: string;
  LastName: string;
  /** @format email */
  EmailAddress: string;
  Phone: string;
  Organization: string;
  OrganizationSize: string;
  Message: string;
  Reason: string;
  Country: string;
}

export interface UserCreateModel {
  first_name: string;
  last_name: string;
  /** @format email */
  email: string;
  password: string;
  newsletter: boolean;
  terms_confirmation: boolean;
  age_confirmation: boolean;
  verification_code?: string | null;
  redirect_url?: string | null;
  captcha_verification_token?: string | null;
}

export interface UserCreateWalletModel {
  first_name: string;
  last_name: string;
  /** @format email */
  email: string;
  wallet: string;
  signature: string;
  terms_confirmation: boolean;
  age_confirmation: boolean;
}

export interface UserEducationModel {
  school: string;
  program?: string | null;
  description?: string | null;
  date_from?: string | null;
  date_to?: string | null;
  current: boolean;
}

export interface UserExperienceModel {
  company: string;
  position: string;
  description?: string | null;
  date_from?: string | null;
  date_to?: string | null;
  current: boolean;
}

export interface UserExternalAuthModel {
  orcid_access_token: string;
  is_orcid_user: boolean;
  is_google_user: boolean;
}

export interface UserForgotPasswordModel {
  email: string;
  code: string;
  new_password: string;
}

export interface UserLoginCodeModel {
  email: string;
  code: string;
}

export interface UserLoginPasswordModel {
  email: string;
  password: string;
}

export interface UserLoginSignatureModel {
  wallet: string;
  signature: string;
}

export interface UserMiniModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  first_name: string;
  last_name: string;
  username: string;
  short_bio: string;
  banner_url: string;
  banner_url_sm: string;
  logo_url: string;
  logo_url_sm: string;
  status: string;
  country: string;
  city: string;
  /** @format int32 */
  follower_count: number;
  user_follows: boolean;
  stats: CommunityEntityStatModel;
  experience: UserExperienceModel[];
  education: UserEducationModel[];
  spaces: CommunityEntityMiniModel[];
}

export interface UserMiniModelListPage {
  /** @format int32 */
  total: number;
  items: UserMiniModel[];
}

export interface UserModel {
  /** @format date-time */
  created: string;
  created_by_user_id: string;
  /** @format date-time */
  updated?: string | null;
  /** @format date-time */
  last_activity?: string | null;
  id: string;
  first_name: string;
  last_name: string;
  username: string;
  email: string;
  short_bio: string;
  /** @format date-time */
  date_of_birth: string;
  gender: string;
  banner_url: string;
  banner_url_sm: string;
  logo_url: string;
  logo_url_sm: string;
  status: string;
  user_status: UserStatus;
  bio: string;
  country: string;
  city: string;
  links: Link[];
  skills: string[];
  interests: string[];
  assets: string[];
  orcid_id: string;
  newsletter: boolean;
  contact_me: boolean;
  feed_id: string;
  notification_settings: UserNotificationSettingsModel;
  experience: UserExperienceModel[];
  education: UserEducationModel[];
  /** @format int32 */
  followed_count: number;
  /** @format int32 */
  follower_count: number;
  user_follows: boolean;
  stats: CommunityEntityStatModel;
  external_auth: UserExternalAuthModel;
  organizations: EntityMiniModel[];
  language: string;
}

export interface UserNotificationSettingsModel {
  mentionJogl?: boolean;
  mentionEmail?: boolean;
  postMemberContainerJogl?: boolean;
  postMemberContainerEmail?: boolean;
  threadActivityJogl?: boolean;
  threadActivityEmail?: boolean;
  postAuthoredObjectJogl?: boolean;
  postAuthoredObjectEmail?: boolean;
  documentMemberContainerJogl?: boolean;
  documentMemberContainerEmail?: boolean;
  needMemberContainerJogl?: boolean;
  needMemberContainerEmail?: boolean;
  paperMemberContainerJogl?: boolean;
  paperMemberContainerEmail?: boolean;
  eventMemberContainerJogl?: boolean;
  eventMemberContainerEmail?: boolean;
  containerInvitationJogl?: boolean;
  containerInvitationEmail?: boolean;
  eventInvitationJogl?: boolean;
  postAttendingEventJogl?: boolean;
  postAttendingEventEmail?: boolean;
  postAuthoredEventJogl?: boolean;
  postAuthoredEventEmail?: boolean;
}

export interface UserPasswordChangeModel {
  old_password: string;
  new_password: string;
}

export interface UserPatchModel {
  first_name?: string | null;
  last_name?: string | null;
  username?: string | null;
  /** @format email */
  email?: string | null;
  /** @format date-time */
  date_of_birth?: string | null;
  gender?: string | null;
  banner_id?: string | null;
  logo_id?: string | null;
  short_bio?: string | null;
  bio?: string | null;
  country?: string | null;
  city?: string | null;
  links?: LinkModel[] | null;
  skills?: string[] | null;
  interests?: string[] | null;
  assets?: string[] | null;
  orcid_id?: string | null;
  status?: string | null;
  newsletter?: boolean | null;
  contact_me?: boolean | null;
  notification_settings?: UserNotificationSettingsModel;
  experience?: UserExperienceModel[] | null;
  education?: UserEducationModel[] | null;
  external_auth?: UserExternalAuthModel;
  language?: string | null;
}

export enum UserStatus {
  Pending = 'pending',
  Verified = 'verified',
  Archived = 'archived',
}

export interface UserUpdateModel {
  first_name: string;
  last_name: string;
  username: string;
  /** @format email */
  email: string;
  /** @format date-time */
  date_of_birth: string;
  gender: string;
  banner_id: string;
  logo_id: string;
  short_bio: string;
  bio: string;
  country: string;
  city: string;
  links?: LinkModel[] | null;
  skills?: string[] | null;
  interests?: string[] | null;
  assets?: string[] | null;
  orcid_id: string;
  status?: string | null;
  newsletter: boolean;
  contact_me: boolean;
  notification_settings?: UserNotificationSettingsModel;
  experience?: UserExperienceModel[] | null;
  education?: UserEducationModel[] | null;
  external_auth: UserExternalAuthModel;
  language: string;
}

export interface VerificationConfirmationModel {
  /** @format email */
  email: string;
  code: string;
}

export interface VerificationStartModel {
  /** @format email */
  email: string;
}

export enum VerificationStatus {
  Ok = 'ok',
  Expired = 'expired',
  Invalid = 'invalid',
}

export interface WaitlistRecordModel {
  FirstName: string;
  LastName: string;
  /** @format email */
  EmailAddress: string;
  Organization: string;
  Message: string;
}

export enum WalletType {
  Evm = 'evm',
  Near = 'near',
}

export interface WorkModel {
  id: string;
  title: string;
  authors: string[];
  publication: string;
  date: string;
}

export interface WorkspaceDetailModel {
  id: string;
  title: string;
  short_title: string;
  description: string;
  short_description: string;
  status: string;
  label: string;
  banner_url_sm: string;
  banner_url: string;
  logo_url_sm: string;
  logo_url: string;
  feed_id: string;
  interests: string[];
  keywords: string[];
  links: LinkModel[];
  tabs: string[];
  listing_privacy: PrivacyLevel;
  content_privacy: PrivacyLevel;
  content_privacy_custom_settings: PrivacyLevelSettingModel[];
  joining_restriction: JoiningRestrictionLevel;
  joining_restriction_custom_settings: JoiningRestrictionLevelSettingModel[];
  management: string[];
  home_channel_id?: string | null;
  user_access_level: string;
  user_access: CommunityEntityPermissionModel;
  user_onboarded: boolean;
  contribution: string;
  listing_origin: AccessOriginModel;
  path: EntityMiniModel[];
  user_invitation: InvitationModelUser;
  onboarding: OnboardingConfigurationModel;
  faq: FAQItem[];
  locations: string[];
  stats: CommunityEntityDetailStatModel;
  created_by?: UserMiniModel;
}

export interface WorkspaceModel {
  id: string;
  title: string;
  short_title: string;
  description: string;
  short_description: string;
  status: string;
  label: string;
  banner_url_sm: string;
  banner_url: string;
  logo_url_sm: string;
  logo_url: string;
  feed_id: string;
  interests: string[];
  keywords: string[];
  links: LinkModel[];
  tabs: string[];
  listing_privacy: PrivacyLevel;
  content_privacy: PrivacyLevel;
  content_privacy_custom_settings: PrivacyLevelSettingModel[];
  joining_restriction: JoiningRestrictionLevel;
  joining_restriction_custom_settings: JoiningRestrictionLevelSettingModel[];
  management: string[];
  home_channel_id?: string | null;
  user_access_level: string;
  stats: CommunityEntityStatModel;
  user_access: CommunityEntityPermissionModel;
  user_onboarded: boolean;
  contribution: string;
  listing_origin: AccessOriginModel;
  path: EntityMiniModel[];
  user_invitation: InvitationModelUser;
  onboarding: OnboardingConfigurationModel;
  faq: FAQItem[];
  locations: string[];
}

export interface WorkspacePatchModel {
  title?: string | null;
  short_title?: string | null;
  description?: string | null;
  short_description?: string | null;
  status?: string | null;
  banner_id?: string | null;
  logo_id?: string | null;
  interests?: string[] | null;
  keywords?: string[] | null;
  links?: LinkModel[] | null;
  tabs?: string[] | null;
  listing_privacy?: PrivacyLevel;
  content_privacy?: PrivacyLevel;
  content_privacy_custom_settings?: PrivacyLevelSettingModel[] | null;
  joining_restriction?: JoiningRestrictionLevel;
  joining_restriction_custom_settings?: JoiningRestrictionLevelSettingModel[] | null;
  management?: string[] | null;
  feed_open?: boolean | null;
  onboarding?: OnboardingConfigurationUpsertModel;
  faq?: FAQItem[] | null;
  label?: string | null;
  locations?: string[] | null;
}

export interface WorkspaceUpsertModel {
  title: string;
  short_title: string;
  description?: string | null;
  short_description: string;
  status: string;
  banner_id?: string | null;
  logo_id?: string | null;
  interests?: string[] | null;
  keywords?: string[] | null;
  links?: LinkModel[] | null;
  tabs?: string[] | null;
  listing_privacy?: PrivacyLevel;
  content_privacy?: PrivacyLevel;
  content_privacy_custom_settings?: PrivacyLevelSettingModel[] | null;
  joining_restriction?: JoiningRestrictionLevel;
  joining_restriction_custom_settings?: JoiningRestrictionLevelSettingModel[] | null;
  management?: string[] | null;
  home_channel_id?: string | null;
  onboarding?: OnboardingConfigurationUpsertModel;
  faq?: FAQItem[] | null;
  locations?: string[] | null;
  label?: string | null;
  parent_id: string;
}

export namespace Auth {
  /**
   * No description
   * @tags Auth
   * @name LoginCreate
   * @summary Logs a user in using an email and password
   * @request POST:/auth/login
   * @response `200` `AuthResultModel` User login successful
   * @response `401` `void` Invalid user credentials
   * @response `403` `void` User not yet verified
   */
  export namespace LoginCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = UserLoginPasswordModel;
    export type RequestHeaders = {};
    export type ResponseBody = AuthResultModel;
  }

  /**
   * No description
   * @tags Auth
   * @name LoginPasswordCreate
   * @summary Logs a user in using an email and password
   * @request POST:/auth/login/password
   * @response `200` `AuthResultModel` User login successful
   * @response `401` `void` Invalid user credentials
   * @response `403` `void` User not yet verified
   */
  export namespace LoginPasswordCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = UserLoginPasswordModel;
    export type RequestHeaders = {};
    export type ResponseBody = AuthResultModel;
  }

  /**
   * No description
   * @tags Auth
   * @name VerificationCreate
   * @summary Creates a new email verification for a given email
   * @request POST:/auth/verification
   * @response `200` `void` The verification was created
   */
  export namespace VerificationCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = VerificationStartModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Auth
   * @name WalletChallengeDetailDetail
   * @summary Retrieves a user challenge for a wallet signature login
   * @request GET:/auth/wallet/challenge/{wallet}
   * @response `200` `string` User challenge
   */
  export namespace WalletChallengeDetailDetail {
    export type RequestParams = {
      wallet: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Auth
   * @name WalletCreateDetail
   * @summary Logs a user in using a web3 wallet signature
   * @request POST:/auth/wallet/{walletType}
   * @response `200` `AuthResultModel` User login successful
   * @response `401` `void` Invalid signature
   * @response `403` `void` User not yet verified
   */
  export namespace WalletCreateDetail {
    export type RequestParams = {
      walletType: WalletType;
    };
    export type RequestQuery = {};
    export type RequestBody = UserLoginSignatureModel;
    export type RequestHeaders = {};
    export type ResponseBody = AuthResultModel;
  }

  /**
   * No description
   * @tags Auth
   * @name VerificationCheckList
   * @summary Returns the status of a user email verification for a given email and code
   * @request GET:/auth/verification/check
   * @response `200` `VerificationStatus` The verification status
   */
  export namespace VerificationCheckList {
    export type RequestParams = {};
    export type RequestQuery = {
      email?: string;
      code?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = VerificationStatus;
  }

  /**
   * No description
   * @tags Auth
   * @name VerificationConfirmCreate
   * @summary Confirms the verification for a given email and code
   * @request POST:/auth/verification/confirm
   * @response `200` `void` The verification was completed
   * @response `400` `void` The supplied verification code is invalid
   */
  export namespace VerificationConfirmCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = VerificationConfirmationModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Auth
   * @name ResetList
   * @summary Initiates the password reset process using the supplied email address
   * @request GET:/auth/reset
   * @response `200` `void`
   */
  export namespace ResetList {
    export type RequestParams = {};
    export type RequestQuery = {
      email?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Auth
   * @name ResetConfirmCreate
   * @summary Resets a user's password using a code delivered to their email address
   * @request POST:/auth/resetConfirm
   * @response `200` `void` The user's password was reset successfully
   * @response `403` `void` The user does not exist, the code is invalid or has expired
   */
  export namespace ResetConfirmCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = UserForgotPasswordModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Auth
   * @name PasswordCreate
   * @summary Updates a user's password
   * @request POST:/auth/password
   * @response `200` `void` The user's password was reset successfully
   * @response `403` `void` The old password is invalid
   */
  export namespace PasswordCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = UserPasswordChangeModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Auth
   * @name CodeTriggerList
   * @summary Initiates the one-time login process using the supplied email address
   * @request GET:/auth/code/trigger
   * @response `200` `void` Success
   */
  export namespace CodeTriggerList {
    export type RequestParams = {};
    export type RequestQuery = {
      email?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Auth
   * @name CodeLoginCreate
   * @summary Verifies the one-time login code and completes the one-time login processs. If a user doesn't exist with the supplied email, it will be created. A user created in this manner has no password and can only log in via further one-time codes until they link another authentication method to their account.
   * @request POST:/auth/code/login
   * @response `200` `void` User login successful
   * @response `401` `void` Invalid user credentials
   */
  export namespace CodeLoginCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = UserLoginCodeModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Auth
   * @name OrcidCreate
   * @summary register or sign-in the user's unique ORCID id from ORCID and stores it in the database
   * @request POST:/auth/orcid
   * @response `200` `AuthResultExtendedModel` Login or registration successful
   * @response `401` `void` No ORCID record found
   * @response `403` `void` Login or registration forbidden
   */
  export namespace OrcidCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = OrcidRegistrationModel;
    export type RequestHeaders = {};
    export type ResponseBody = AuthResultExtendedModel;
  }

  /**
   * No description
   * @tags Auth
   * @name GoogleCreate
   * @summary Register or sign-in the user with a google access token
   * @request POST:/auth/google
   * @response `200` `AuthResultExtendedModel` Login or registration successful
   * @response `401` `void` No google record found
   * @response `403` `void` Login or registration forbidden
   */
  export namespace GoogleCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = AccessTokenModel;
    export type RequestHeaders = {};
    export type ResponseBody = AuthResultExtendedModel;
  }

  /**
   * No description
   * @tags Auth
   * @name LinkedinCreate
   * @summary Register or sign-in the user with a linkedin access token
   * @request POST:/auth/linkedin
   * @response `200` `AuthResultExtendedModel` Login or registration successful
   * @response `401` `void` No linkedin record found
   * @response `403` `void` Login or registration forbidden
   */
  export namespace LinkedinCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = AuthorizationCodeModel;
    export type RequestHeaders = {};
    export type ResponseBody = AuthResultExtendedModel;
  }
}

export namespace CfPs {
  /**
   * No description
   * @tags CallForProposal
   * @name CfPsCreate
   * @summary Create a new call for proposals. The current user becomes a member of the call for proposals with the Owner role
   * @request POST:/CFPs
   * @response `200` `void` Success
   */
  export namespace CfPsCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = CallForProposalUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name CfPsList
   * @summary List all accessible calls for proposals for a given search query. Only calls for proposals accessible to the currently logged in user will be returned
   * @request GET:/CFPs
   * @response `200` `CommunityEntityMiniModelListPage`
   */
  export namespace CfPsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModelListPage;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name CfPsDetailDetail
   * @summary Returns a single call for proposals
   * @request GET:/CFPs/{id}
   * @response `200` `CallForProposalModel` The call for proposals data
   * @response `404` `void` No call for proposals was found for that id
   */
  export namespace CfPsDetailDetail {
    export type RequestParams = {
      /** ID of the call for proposal */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CallForProposalModel;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name CfPsPartialUpdateDetail
   * @summary Patches the specified call for proposal
   * @request PATCH:/CFPs/{id}
   * @response `200` `string` The ID of the entity
   * @response `403` `void` The current user does not have rights to edit this call for proposal
   * @response `404` `void` No call for proposal was found for that id
   */
  export namespace CfPsPartialUpdateDetail {
    export type RequestParams = {
      /** ID of the call for proposals */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = CallForProposalPatchModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name CfPsUpdateDetail
   * @summary Updates the specified call for proposal
   * @request PUT:/CFPs/{id}
   * @response `200` `string` The ID of the entity
   * @response `403` `void` The current user does not have rights to edit this call for proposal
   * @response `404` `void` No call for proposal was found for that id
   */
  export namespace CfPsUpdateDetail {
    export type RequestParams = {
      /** ID of the call for proposals */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = CallForProposalUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name CfPsDeleteDetail
   * @summary Deletes the specified call for proposal and removes all of the call for proposal's associations from projects and communities
   * @request DELETE:/CFPs/{id}
   * @response `403` `void` The current user does not have rights to delete this call for proposal
   * @response `404` `void` No call for proposal was found for that id
   */
  export namespace CfPsDeleteDetail {
    export type RequestParams = {
      /** ID of the call for proposals */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name DetailDetail
   * @summary Returns a single call for proposals, including detailed stats
   * @request GET:/CFPs/{id}/detail
   * @response `200` `CallForProposalDetailModel` The call for proposals data including detailed stats
   * @response `404` `void` No call for proposals was found for that id
   */
  export namespace DetailDetail {
    export type RequestParams = {
      /** ID of the call for proposal */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CallForProposalDetailModel;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name AutocompleteList
   * @summary List the basic information of calls for proposals for a given search query. Only calls for proposals accessible to the currently logged in user will be returned
   * @request GET:/CFPs/autocomplete
   * @response `200` `(CommunityEntityMiniModel)[]`
   */
  export namespace AutocompleteList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModel[];
  }

  /**
   * No description
   * @tags CallForProposal
   * @name ProposalsDetail
   * @summary Lists all proposals submitted to the specified call for proposals
   * @request GET:/CFPs/{id}/proposals
   * @response `200` `(ProposalModel)[]`
   * @response `403` `void` The current user does not have the rights to view proposals
   * @response `404` `void` No call for proposal was found for that id
   */
  export namespace ProposalsDetail {
    export type RequestParams = {
      /** ID of the call for proposals */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ProposalModel[];
  }

  /**
   * No description
   * @tags CallForProposal
   * @name MessageCreate
   * @summary Sends a message to selected cfp members
   * @request POST:/CFPs/{id}/message
   * @response `200` `(ProposalModel)[]`
   * @response `403` `void` The current user does not have the rights to send messages to cfp members
   * @response `404` `void` No call for proposal was found for that id
   */
  export namespace MessageCreate {
    export type RequestParams = {
      /** ID of the call for proposals */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = MessageModel;
    export type RequestHeaders = {};
    export type ResponseBody = ProposalModel[];
  }

  /**
   * No description
   * @tags CallForProposal
   * @name ChangedCreate
   * @summary Determines whether the template for a CFP has changed or not
   * @request POST:/CFPs/{id}/changed
   * @response `200` `(ProposalModel)[]`
   * @response `404` `void` No call for proposal was found for that id
   */
  export namespace ChangedCreate {
    export type RequestParams = {
      /** ID of the call for proposals */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = CallForProposalUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = ProposalModel[];
  }

  /**
   * No description
   * @tags CallForProposal
   * @name DocumentsCreate
   * @summary Adds a new document for the specified entity.
   * @request POST:/CFPs/{id}/documents
   * @deprecated
   * @response `200` `string` The document was created
   * @response `400` `void` Note-typed documents have to be created and updated through the feed endpoints
   * @response `403` `void` The current user doesn't have sufficient rights to add documents for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace DocumentsCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = DocumentInsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name DocumentsDetail
   * @summary Lists all documents for the specified entity, not including file data
   * @request GET:/CFPs/{id}/documents
   * @deprecated
   * @response `200` `string` The document was created
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace DocumentsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      folderId?: string;
      type?: DocumentFilter;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name DocumentsDetailDetail
   * @summary Returns a single document, including the file represented as base64
   * @request GET:/CFPs/{id}/documents/{documentId}
   * @deprecated
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
   * @response `404` `void` No entity was found for that id or the document does not exist
   */
  export namespace DocumentsDetailDetail {
    export type RequestParams = {
      id: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name DocumentsUpdateDetail
   * @summary Updates the title and description for the document
   * @request PUT:/CFPs/{id}/documents/{documentId}
   * @deprecated
   * @response `200` `void` The document was updated
   * @response `403` `void` The current user doesn't have sufficient rights to edit documents for the entity
   * @response `404` `void` No entity was found for that id or the document does not exist
   */
  export namespace DocumentsUpdateDetail {
    export type RequestParams = {
      id: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = DocumentUpdateModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name DocumentsDeleteDetail
   * @summary Deletes the specified document
   * @request DELETE:/CFPs/{id}/documents/{documentId}
   * @deprecated
   * @response `200` `void` The document was deleted
   * @response `403` `void` The current user doesn't have sufficient rights to delete the document
   * @response `404` `void` No entity was found for that id or the document does not exist
   */
  export namespace DocumentsDeleteDetail {
    export type RequestParams = {
      id: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name DocumentsDraftDetail
   * @summary Returns a draft document for the specified container
   * @request GET:/CFPs/{id}/documents/draft
   * @deprecated
   * @response `204` `void` No draft document was found for the community entity
   * @response `404` `void` No community entity was found for the specified id
   */
  export namespace DocumentsDraftDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name DocumentsAllDetail
   * @summary Lists all documents and folders for the specified entity, not including file data
   * @request GET:/CFPs/{id}/documents/all
   * @deprecated
   * @response `200` `(BaseModel)[]` The document was created
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace DocumentsAllDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = BaseModel[];
  }

  /**
   * No description
   * @tags CallForProposal
   * @name JoinCreate
   * @summary Joins an entity on behalf of the currently logged in user. Only works for community entities with the Open membership access level. Creates a membership record immediately, no invitation is created.
   * @request POST:/CFPs/{id}/join
   * @deprecated
   * @response `200` `string` ID of the new membership object
   * @response `403` `void` The entity is not open to members joining
   * @response `404` `void` The entity does not exist
   * @response `409` `void` A membership record already exists for the entity and user
   */
  export namespace JoinCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name RequestCreate
   * @summary Creates a request to join an entity on behalf of the currently logged in user
   * @request POST:/CFPs/{id}/request
   * @deprecated
   * @response `200` `string` ID of the new invitation object
   * @response `403` `void` The entity is not open to members requesting to join
   * @response `404` `void` The entity does not exist
   * @response `409` `void` An invitation already exists for the entity and user
   */
  export namespace RequestCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name InviteIdCreateDetail
   * @summary Invite a user to an entity using their ID
   * @request POST:/CFPs/{id}/invite/id/{userId}
   * @response `200` `string` ID of the new invitation object
   * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity, or the user is inviting an owner without having the rights to manage owners
   * @response `404` `void` The entity or user do not exist
   * @response `409` `void` An invitation already exists for the entity and user
   */
  export namespace InviteIdCreateDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the user to invite */
      userId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = AccessLevel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name InviteEmailCreateDetail
   * @summary Invite a user to an entity using their email
   * @request POST:/CFPs/{id}/invite/email/{userEmail}
   * @response `200` `string` ID of the new invitation object
   * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace InviteEmailCreateDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the user to invite */
      userEmail: string;
    };
    export type RequestQuery = {};
    export type RequestBody = AccessLevel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name InviteEmailBatchCreate
   * @summary Invite a user to an entity using their email
   * @request POST:/CFPs/{id}/invite/email/batch
   * @deprecated
   * @response `200` `string` ID of the new invitation object
   * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace InviteEmailBatchCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = string[];
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name InvitesDetail
   * @summary List all pending invitations for a given entity
   * @request GET:/CFPs/{id}/invites
   * @response `200` `(InvitationModelUser)[]` A list of invitations
   * @response `404` `void` No entity was found for that id
   */
  export namespace InvitesDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = InvitationModelUser[];
  }

  /**
   * No description
   * @tags CallForProposal
   * @name InvitesAcceptCreate
   * @summary Accept the invite on behalf of the currently logged-in user
   * @request POST:/CFPs/{id}/invites/{invitationId}/accept
   * @response `200` `void` The invitation has been accepted. The user is now a member of the project.
   * @response `403` `void` The current user does not have the right to accept the invitation
   * @response `404` `void` No entity was found for that id or the invitation does not exist
   */
  export namespace InvitesAcceptCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name InvitesRejectCreate
   * @summary Reject the invite on behalf of the currently logged-in user
   * @request POST:/CFPs/{id}/invites/{invitationId}/reject
   * @response `200` `void` The invitation has been rejected
   * @response `403` `void` The current user does not have the right to reject the invitation
   * @response `404` `void` No entity was found for that id or the invitation does not exist
   */
  export namespace InvitesRejectCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name InvitesCancelCreate
   * @summary Cancels an invite
   * @request POST:/CFPs/{id}/invites/{invitationId}/cancel
   * @response `200` `void` The invitation has been cancelled
   * @response `403` `void` The current user does not have the right to cancel the invitation
   * @response `404` `void` No entity was found for that id or the invitation does not exist
   */
  export namespace InvitesCancelCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name InvitesResendCreate
   * @summary Resends an invite
   * @request POST:/CFPs/invites/{invitationId}/resend
   * @response `200` `void` The invitation has been resent
   * @response `400` `void` The invitation is a request and cannot be resent
   * @response `403` `void` The user does not have the right to resend the invitation
   * @response `404` `void` No entity was found for that id or the invitation does not exist
   */
  export namespace InvitesResendCreate {
    export type RequestParams = {
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name LeaveCreate
   * @summary Leaves an entity on behalf of the currently logged in user
   * @request POST:/CFPs/{id}/leave
   * @deprecated
   * @response `200` `void` The user has successfully left the entity
   * @response `404` `void` The entity does not exist or the user is not a member
   */
  export namespace LeaveCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name OnboardingResponsesDetailDetail
   * @summary List onboarding responses for a community entity and a user
   * @request GET:/CFPs/{id}/onboardingResponses/{userId}
   * @deprecated
   * @response `200` `OnboardingQuestionnaireInstanceModel` onboarding questionnaire responses
   * @response `404` `void` No entity was found for that id or no user was found
   */
  export namespace OnboardingResponsesDetailDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      userId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = OnboardingQuestionnaireInstanceModel;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name OnboardingResponsesCreate
   * @summary Posts onboarding responses for a community entity for the current user
   * @request POST:/CFPs/{id}/onboardingResponses
   * @deprecated
   * @response `200` `void` The onboarding questionnaire responses were saved successfully
   * @response `404` `void` No entity was found for that id
   */
  export namespace OnboardingResponsesCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = OnboardingQuestionnaireInstanceUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name OnboardingCompletionCreate
   * @summary Records the completion of the onboarding workflow for a community entity for the current user
   * @request POST:/CFPs/{id}/onboardingCompletion
   * @deprecated
   * @response `200` `void` The onboarding completion was recorded successfully
   * @response `404` `void` No entity was found for that id
   */
  export namespace OnboardingCompletionCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name MembersDetail
   * @summary List all members for an entity
   * @request GET:/CFPs/{id}/members
   * @response `200` `(MemberModel)[]` A list of members
   * @response `403` `void` The current user doesn't have sufficient rights to see the entity's members
   * @response `404` `void` No entity was found for that id
   */
  export namespace MembersDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = MemberModel[];
  }

  /**
   * No description
   * @tags CallForProposal
   * @name InvitationDetail
   * @summary Returns the pending invitation for an object for the current user
   * @request GET:/CFPs/{id}/invitation
   * @response `200` `InvitationModelEntity` A pending invitation
   * @response `404` `void` No invitation is pending or no entity was found for that id
   */
  export namespace InvitationDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = InvitationModelEntity;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name MembersUpdateDetail
   * @summary Updates a member's access level for an entity
   * @request PUT:/CFPs/{id}/members/{memberId}
   * @response `200` `void` The access level has been updated
   * @response `403` `void` The user does not have the right to set this access level for this member
   * @response `404` `void` No entity was found for that id or the user is not a member of that entity
   */
  export namespace MembersUpdateDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the entity */
      memberId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = AccessLevel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name MembersDeleteDetail
   * @summary Removes a member's access from an entity
   * @request DELETE:/CFPs/{id}/members/{memberId}
   * @response `200` `void` The member has been removed
   * @response `403` `void` The current user does not have the right to remove members from this entity
   * @response `404` `void` No entity was found for that id or the user is not a member of that entity
   */
  export namespace MembersDeleteDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the entity */
      memberId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name MembersContributionUpdate
   * @summary Updates a member's contribution
   * @request PUT:/CFPs/{id}/members/{memberId}/contribution
   * @response `200` `void` The contribution has been updated
   * @response `403` `void` The user does not have the right to set the contribution for other members
   * @response `404` `void` No entity was found for that id or the user is not a member of that entity
   */
  export namespace MembersContributionUpdate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the user */
      memberId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = string;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name MembersLabelsUpdate
   * @summary Updates a member's labels
   * @request PUT:/CFPs/{id}/members/{memberId}/labels
   * @response `200` `void` The labels have been updated
   * @response `403` `void` The current user does not have the right to set labels for members
   * @response `404` `void` No entity was found for that id or the user is not a member of that entity
   */
  export namespace MembersLabelsUpdate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the user */
      memberId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = string[];
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name CommunityEntityInviteCreateDetail
   * @summary Invite a community entity to become affiliated to another entity
   * @request POST:/CFPs/{id}/communityEntityInvite/{targetEntityId}
   * @response `200` `string` ID of the new invitation object
   * @response `400` `void` An invitation cannot be extended to a community entity of the same type
   * @response `403` `void` The current user does not have the rights to invite entities to the specified entity
   * @response `404` `void` One of the entities does not exist
   * @response `409` `void` An invitation or affiliation already exists for the entities
   */
  export namespace CommunityEntityInviteCreateDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the target entity */
      targetEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name CommunityEntityInvitesIncomingDetail
   * @summary List all pending invitations for a given entity
   * @request GET:/CFPs/{id}/communityEntityInvites/incoming
   * @response `200` `(CommunityEntityInvitationModelSource)[]` A list of invitations
   * @response `404` `void` No entity was found for that id
   */
  export namespace CommunityEntityInvitesIncomingDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityInvitationModelSource[];
  }

  /**
   * No description
   * @tags CallForProposal
   * @name CommunityEntityInvitesOutgoingDetail
   * @summary List all pending invitations for a given entity
   * @request GET:/CFPs/{id}/communityEntityInvites/outgoing
   * @response `200` `(CommunityEntityInvitationModelSource)[]` A list of invitations
   * @response `404` `void` No entity was found for that id
   */
  export namespace CommunityEntityInvitesOutgoingDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityInvitationModelSource[];
  }

  /**
   * No description
   * @tags CallForProposal
   * @name CommunityEntityInvitesAcceptCreate
   * @summary Accept an invite
   * @request POST:/CFPs/{id}/communityEntityInvites/{invitationId}/accept
   * @response `200` `void` The invitation has been accepted
   * @response `403` `void` The current user does not have the right to accept the invitation
   * @response `404` `void` One of the entities does not exist or the invitation does not exist
   */
  export namespace CommunityEntityInvitesAcceptCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name CommunityEntityInvitesRejectCreate
   * @summary Rejects an invite
   * @request POST:/CFPs/{id}/communityEntityInvites/{invitationId}/reject
   * @response `200` `void` The invitation has been rejected
   * @response `403` `void` The current user does not have the right to reject the invitation
   * @response `404` `void` One of the entities does not exist or the invitation does not exist
   */
  export namespace CommunityEntityInvitesRejectCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name CommunityEntityInvitesCancelCreate
   * @summary Cancels an invite
   * @request POST:/CFPs/{id}/communityEntityInvites/{invitationId}/cancel
   * @response `200` `void` The invitation has been cancelled
   * @response `403` `void` The current user does not have the right to cancel the invitation
   * @response `404` `void` One of the entities does not exist or the invitation does not exist
   */
  export namespace CommunityEntityInvitesCancelCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name CommunityEntityRemoveCreate
   * @summary Removes an affiliation of an entity to another entity
   * @request POST:/CFPs/{id}/communityEntity/{targetEntityId}/remove
   * @response `200` `void` The entity affiliation has been removed successfully
   * @response `403` `void` The current user does not have the permissions to manage the community entity's affiliations
   * @response `404` `void` One of the entities does not exist or the entities are not affiliated
   */
  export namespace CommunityEntityRemoveCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the target entity */
      targetEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name CommunityEntityLinkCreate
   * @summary Creates an affiliation of an entity to another entity. On the entity, the user must be admin or owner, or the toggle for members to allow creating community entities of the respective type must be on. The user must be admin or owner on the target entity.
   * @request POST:/CFPs/{id}/communityEntity/{targetEntityId}/link
   * @response `200` `void` The entity affiliation has been created successfully
   * @response `409` `void` One of the entities does not exist or the entities are already affiliated
   */
  export namespace CommunityEntityLinkCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the target entity */
      targetEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name EcosystemCommunityEntitiesDetail
   * @summary Lists all ecosystem containers (projects, communities and nodes) for the given community entity
   * @request GET:/CFPs/{id}/ecosystem/communityEntities
   * @deprecated
   * @response `200` `(EntityMiniModel)[]`
   * @response `403` `string` The current user does not have rights to view this community entity's contents
   * @response `404` `void` No community entity was found for that id
   */
  export namespace EcosystemCommunityEntitiesDetail {
    export type RequestParams = {
      /** ID of the community entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EntityMiniModel[];
  }

  /**
   * No description
   * @tags CallForProposal
   * @name EcosystemUsersDetail
   * @summary Lists all ecosystem members for the given community entity
   * @request GET:/CFPs/{id}/ecosystem/users
   * @response `200` `(EntityMiniModel)[]`
   * @response `403` `string` The current user does not have rights to view this community entity's contents
   * @response `404` `void` No community entity was found for that id
   */
  export namespace EcosystemUsersDetail {
    export type RequestParams = {
      /** ID of the community entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EntityMiniModel[];
  }

  /**
   * No description
   * @tags CallForProposal
   * @name NeedsCreate
   * @summary Adds a new need for the specified project
   * @request POST:/CFPs/{id}/needs
   * @deprecated
   * @response `200` `string` The ID of the new need
   * @response `403` `void` The current user doesn't have sufficient rights to add needs for the entity
   */
  export namespace NeedsCreate {
    export type RequestParams = {
      /** ID of the project */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = NeedUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name NeedsDetail
   * @summary Lists all needs for the specified community entity
   * @request GET:/CFPs/{id}/needs
   * @deprecated
   * @response `200` `(NeedModel)[]`
   * @response `403` `string` The current user does not have rights to view this community entity's contents
   * @response `404` `void` No community entity was found for that id
   */
  export namespace NeedsDetail {
    export type RequestParams = {
      /** ID of the community entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = NeedModel[];
  }

  /**
   * No description
   * @tags CallForProposal
   * @name NeedsDetailDetail
   * @summary Returns a single need
   * @request GET:/CFPs/needs/{needId}
   * @deprecated
   * @response `403` `string` The current user does not have rights to view this community entity's contents
   * @response `404` `void` No need was found for the need id
   */
  export namespace NeedsDetailDetail {
    export type RequestParams = {
      needId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name NeedsUpdateDetail
   * @summary Updates the need
   * @request PUT:/CFPs/needs/{needId}
   * @deprecated
   * @response `200` `void` The need was updated
   * @response `403` `void` The current user does not have rights to update needs on this community entity
   * @response `404` `void` No need was found for the need id
   */
  export namespace NeedsUpdateDetail {
    export type RequestParams = {
      needId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = NeedUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name NeedsDeleteDetail
   * @summary Deletes the specified need
   * @request DELETE:/CFPs/needs/{needId}
   * @deprecated
   * @response `200` `void` The need was deleted
   * @response `403` `void` The current user does not have rights to delete needs on this community entity
   * @response `404` `void` No need was found for the need id
   */
  export namespace NeedsDeleteDetail {
    export type RequestParams = {
      needId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name EventsCreate
   * @summary Adds a new event for the specified entity.
   * @request POST:/CFPs/{id}/events
   * @deprecated
   * @response `200` `string` The event was created
   * @response `403` `void` The current user doesn't have sufficient rights to add events for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace EventsCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = EventUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name EventsDetail
   * @summary Lists events for the specified entity.
   * @request GET:/CFPs/{id}/events
   * @deprecated
   * @response `200` `(EventModel)[]` Event data
   * @response `403` `void` The current user doesn't have sufficient rights to view events for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace EventsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /** @format date-time */
      from?: string;
      /** @format date-time */
      to?: string;
      tags?: EventTag[];
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EventModel[];
  }

  /**
   * No description
   * @tags CallForProposal
   * @name ChannelsCreate
   * @summary Adds a new channel for the specified entity.
   * @request POST:/CFPs/{id}/channels
   * @deprecated
   * @response `200` `string` The channel was created
   * @response `403` `void` The current user doesn't have sufficient rights to add discussion channels for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace ChannelsCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ChannelUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags CallForProposal
   * @name ChannelsDetail
   * @summary Lists channels for the specified entity.
   * @request GET:/CFPs/{id}/channels
   * @deprecated
   * @response `200` `(ChannelModel)[]` channel data
   * @response `403` `void` The current user doesn't have sufficient rights to view channels for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace ChannelsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ChannelModel[];
  }
}

export namespace Channels {
  /**
   * No description
   * @tags Channel
   * @name ChannelsCreate
   * @summary Adds a new channel for the specified entity.
   * @request POST:/channels/{entityId}/channels
   * @response `200` `string` The channel was created
   * @response `403` `void` The current user doesn't have sufficient rights to add discussion channels for the entity
   */
  export namespace ChannelsCreate {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ChannelUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Channel
   * @name ChannelsDetail
   * @summary Lists channels for the specified entity.
   * @request GET:/channels/{entityId}/channels
   * @response `200` `(ChannelModel)[]` channel data
   * @response `403` `void` The current user doesn't have sufficient rights to view channels for the entity
   */
  export namespace ChannelsDetail {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ChannelModel[];
  }

  /**
   * No description
   * @tags Channel
   * @name ChannelsDetailDetail
   * @summary Returns a single channel
   * @request GET:/channels/{id}
   * @response `200` `ChannelModel` The channel data
   * @response `403` `void` The current user doesn't have sufficient rights to see the channel
   * @response `404` `void` No channel was found for that id
   */
  export namespace ChannelsDetailDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ChannelModel;
  }

  /**
   * No description
   * @tags Channel
   * @name ChannelsUpdateDetail
   * @summary Updates the channel
   * @request PUT:/channels/{id}
   * @response `200` `ChannelModel` The channel was updated
   * @response `403` `void` The current user doesn't have sufficient rights to edit the channel
   * @response `404` `void` No channel was found for that id
   */
  export namespace ChannelsUpdateDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ChannelUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = ChannelModel;
  }

  /**
   * No description
   * @tags Channel
   * @name ChannelsDeleteDetail
   * @summary Deletes a channel
   * @request DELETE:/channels/{id}
   * @response `200` `void` The channel was deleted
   * @response `403` `void` The current user doesn't have sufficient rights to delete this channel
   * @response `404` `void` No channel was found for that id
   */
  export namespace ChannelsDeleteDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Channel
   * @name DetailDetail
   * @summary Returns a single channel
   * @request GET:/channels/{id}/detail
   * @response `200` `ChannelDetailModel` The channel data including detailed path
   * @response `403` `void` The current user doesn't have sufficient rights to see the channel
   * @response `404` `void` No channel was found for that id
   */
  export namespace DetailDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ChannelDetailModel;
  }

  /**
   * No description
   * @tags Channel
   * @name MembersDetail
   * @summary Returns members for the specified channel
   * @request GET:/channels/{id}/members
   * @response `200` `(MemberModel)[]` The channel members
   * @response `403` `void` The current user doesn't have sufficient rights to see members for the channel
   * @response `404` `void` No channel was found for that id
   */
  export namespace MembersDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = MemberModel[];
  }

  /**
   * No description
   * @tags Channel
   * @name MembersCreate
   * @summary Adds a batch of users for the specified channel
   * @request POST:/channels/{id}/members
   * @response `200` `void` The membership data was created
   * @response `403` `void` The current user doesn't have sufficient rights to add members to the channel or the user isn't allowed to invite admins
   * @response `404` `void` No channel was found for that id
   */
  export namespace MembersCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ChannelMemberUpsertModel[];
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Channel
   * @name MembersUpdate
   * @summary Adds or updates a batch of users for the specified channel
   * @request PUT:/channels/{id}/members
   * @response `200` `void` The membership data was updated
   * @response `403` `void` The current user doesn't have sufficient rights to update roles for channel members
   * @response `404` `void` No channel was found for that id
   * @response `424` `void` The operation cannot be performed, since it downgrades the only remaining admin or owner of the channel to member
   */
  export namespace MembersUpdate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ChannelMemberUpsertModel[];
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Channel
   * @name MembersDelete
   * @summary Removes a batch of users from the specified channel
   * @request DELETE:/channels/{id}/members
   * @response `200` `void` The membership data was updated
   * @response `403` `void` The current user doesn't have sufficient rights to remove members from the channel
   * @response `404` `void` No channel was found for that id
   */
  export namespace MembersDelete {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = string[];
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Channel
   * @name UsersDetail
   * @summary Lists all eligible users to channel for a given channel, excluding users that have already joined the channel
   * @request GET:/channels/{id}/users
   * @response `200` `(UserMiniModel)[]` The eligible users
   * @response `403` `void` The current user does not have rights to view this channel's contents
   * @response `404` `void` No channel was found for that id
   */
  export namespace UsersDetail {
    export type RequestParams = {
      /** ID of the channel */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = UserMiniModel[];
  }

  /**
   * No description
   * @tags Channel
   * @name MembersJoinCreate
   * @summary Adds the current user to the specified channel
   * @request POST:/channels/{id}/members/join
   * @response `200` `void` The membership record was created
   * @response `403` `void` The current user doesn't have sufficient rights to see the channel
   * @response `404` `void` No channel was found for that id
   */
  export namespace MembersJoinCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Channel
   * @name MembersLeaveCreate
   * @summary Removes the current user from the specified channel
   * @request POST:/channels/{id}/members/leave
   * @response `200` `void` The membership record was deleted
   * @response `404` `void` No channel was found for that id
   * @response `409` `void` The current user isn't a member of this channel
   */
  export namespace MembersLeaveCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Channel
   * @name DocumentsDetail
   * @summary Returns documents for the channel
   * @request GET:/channels/{id}/documents
   * @response `200` `DocumentModelListPage` The document data
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the channel
   * @response `404` `void` No channel was found for that id
   */
  export namespace DocumentsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      type?: DocumentFilter;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = DocumentModelListPage;
  }
}

export namespace CommunityEntities {
  /**
   * No description
   * @tags CommunityEntity
   * @name CommunityEntitiesDetailDetail
   * @summary Gets community entity
   * @request GET:/communityEntities/{id}
   * @response `200` `CommunityEntityMiniModel` The community entity data
   * @response `403` `void` The current user doesn't have sufficient rights to see the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace CommunityEntitiesDetailDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModel;
  }

  /**
   * No description
   * @tags CommunityEntity
   * @name MembersDetail
   * @summary List all members for an entity
   * @request GET:/communityEntities/{id}/members
   * @response `200` `(MemberModel)[]` A list of members
   * @response `403` `void` The current user doesn't have sufficient rights to see the entity's members
   * @response `404` `void` No entity was found for that id
   */
  export namespace MembersDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = MemberModel[];
  }

  /**
   * No description
   * @tags CommunityEntity
   * @name InviteBatchCreate
   * @summary Invite a batch of users to an entity
   * @request POST:/communityEntities/{id}/invite/batch
   * @response `200` `void` The users were successfully invited
   * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace InviteBatchCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = InvitationUpsertModel[];
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CommunityEntity
   * @name JoinKeyDetail
   * @summary Gets an invitation key for a community entity
   * @request GET:/communityEntities/{id}/join/key
   * @response `200` `string` The invitation key
   * @response `404` `void` No entity was found for that id
   */
  export namespace JoinKeyDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags CommunityEntity
   * @name JoinKeyCreateDetail
   * @summary Joins a community entity using an invitation key
   * @request POST:/communityEntities/{id}/join/key/{key}
   * @response `200` `FeedModel` The corresponding feed data
   * @response `404` `void` No matching invitation key found
   */
  export namespace JoinKeyCreateDetail {
    export type RequestParams = {
      id: string;
      key: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = FeedModel;
  }

  /**
   * No description
   * @tags CommunityEntity
   * @name OnboardingDetailDetail
   * @summary List onboarding responses for a community entity and a user
   * @request GET:/communityEntities/{id}/onboarding/{userId}
   * @response `200` `OnboardingQuestionnaireInstanceModel` onboarding questionnaire responses
   * @response `404` `void` No entity was found for that id or no user was found
   */
  export namespace OnboardingDetailDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      userId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = OnboardingQuestionnaireInstanceModel;
  }

  /**
   * No description
   * @tags CommunityEntity
   * @name OnboardingCreate
   * @summary Posts onboarding responses for a community entity for the current user
   * @request POST:/communityEntities/{id}/onboarding
   * @response `200` `void` The onboarding questionnaire responses were saved successfully
   * @response `404` `void` No entity was found for that id
   */
  export namespace OnboardingCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = OnboardingQuestionnaireInstanceUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CommunityEntity
   * @name OnboardingCompleteCreate
   * @summary Records the completion of the onboarding workflow for a community entity for the current user
   * @request POST:/communityEntities/{id}/onboarding/complete
   * @response `200` `void` The onboarding completion was recorded successfully
   * @response `404` `void` No entity was found for that id
   */
  export namespace OnboardingCompleteCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CommunityEntity
   * @name LeaveCreate
   * @summary Leaves an entity on behalf of the currently logged in user
   * @request POST:/communityEntities/{id}/leave
   * @response `200` `void` The user has successfully left the entity
   * @response `404` `void` The entity does not exist or the user is not a member
   */
  export namespace LeaveCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags CommunityEntity
   * @name RequestCreate
   * @summary Creates a request to join an entity on behalf of the currently logged in user
   * @request POST:/communityEntities/{id}/request
   * @response `200` `string` ID of the new invitation object
   * @response `403` `void` The current user does not have the right to request to join the community entity
   * @response `404` `void` The entity does not exist
   * @response `409` `void` A request or invitation already exists for the entity and user
   */
  export namespace RequestCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags CommunityEntity
   * @name JoinCreate
   * @summary Joins an entity on behalf of the currently logged in user. Only works for community entities with the Open membership access level. Creates a membership record immediately, no invitation is created.
   * @request POST:/communityEntities/{id}/join
   * @response `200` `string` ID of the new membership object
   * @response `403` `void` The current user does not have the right to join the community entity
   * @response `404` `void` The entity does not exist
   * @response `409` `void` A request or invitation already exists for the entity and user
   */
  export namespace JoinCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags CommunityEntity
   * @name InviteMassCreate
   * @summary Mass invite a batch of users to a set of community entities
   * @request POST:/communityEntities/invite/mass
   * @response `200` `void` The users were successfully invited
   * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace InviteMassCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = InvitationMassUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
}

export namespace Documents {
  /**
   * No description
   * @tags Document
   * @name DocumentsCreate
   * @summary Adds a new document for the specified feed.
   * @request POST:/documents/{entityId}/documents
   * @response `200` `string` The document was created
   * @response `403` `void` The current user doesn't have sufficient rights to add documents for the entity
   */
  export namespace DocumentsCreate {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = DocumentInsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Document
   * @name DocumentsDetail
   * @summary Lists all documents for the specified entity
   * @request GET:/documents/{entityId}/documents
   * @response `200` `(DocumentModel)[]` The document data
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
   */
  export namespace DocumentsDetail {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {
      folderId?: string;
      type?: DocumentFilter;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = DocumentModel[];
  }

  /**
   * No description
   * @tags Document
   * @name DocumentsNewDetail
   * @summary Returns a value indicating whether or not there are new documents for a particular entity
   * @request GET:/documents/{entityId}/documents/new
   * @response `200` `boolean` True or false
   */
  export namespace DocumentsNewDetail {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = boolean;
  }

  /**
   * No description
   * @tags Document
   * @name DocumentsAllDetail
   * @summary Lists all documents and folders for the specified entity, not including file data
   * @request GET:/documents/{entityId}/documents/all
   * @response `200` `(DocumentOrFolderModel)[]` The document and folder data
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace DocumentsAllDetail {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = DocumentOrFolderModel[];
  }

  /**
   * No description
   * @tags Document
   * @name DocumentsAllNewDetail
   * @summary Returns a value indicating whether or not there are new documents for a particular entity
   * @request GET:/documents/{entityId}/documents/all/new
   * @response `200` `boolean` True or false
   */
  export namespace DocumentsAllNewDetail {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = boolean;
  }

  /**
   * No description
   * @tags Document
   * @name DocumentsDetailDetail
   * @summary Returns a single document
   * @request GET:/documents/{documentId}
   * @response `200` `DocumentModel` The document
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the parent entity
   * @response `404` `void` No document was found for that id
   */
  export namespace DocumentsDetailDetail {
    export type RequestParams = {
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = DocumentModel;
  }

  /**
   * No description
   * @tags Document
   * @name DocumentsUpdateDetail
   * @summary Updates the document
   * @request PUT:/documents/{documentId}
   * @response `200` `void` The document was updated
   * @response `403` `void` The current user doesn't have sufficient rights to edit documents for the entity
   * @response `404` `void` No document was found for that id
   */
  export namespace DocumentsUpdateDetail {
    export type RequestParams = {
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = DocumentUpdateModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Document
   * @name DocumentsDeleteDetail
   * @summary Deletes the specified document
   * @request DELETE:/documents/{documentId}
   * @response `200` `void` The document was deleted
   * @response `403` `void` The current user doesn't have sufficient rights to delete the document
   * @response `404` `void` No document was found for that id
   */
  export namespace DocumentsDeleteDetail {
    export type RequestParams = {
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Document
   * @name DownloadDetail
   * @summary Returns document data
   * @request GET:/documents/{documentId}/download
   * @response `200` `void` The document data as a file
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the parent entity
   * @response `404` `void` No document was found for that id
   */
  export namespace DownloadDetail {
    export type RequestParams = {
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Document
   * @name FoldersCreateDetail
   * @summary Adds a new folder for the specified feed.
   * @request POST:/documents/folders/{feedId}
   * @response `200` `string` The folder was created
   * @response `403` `void` The current user doesn't have sufficient rights to add folders for the feed
   */
  export namespace FoldersCreateDetail {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = FolderUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Document
   * @name FoldersDetailDetail
   * @summary Lists all folders for the specified feed
   * @request GET:/documents/folders/{feedId}
   * @response `200` `(FolderModel)[]` The folder data
   * @response `403` `void` The current user doesn't have sufficient rights to view folders for the feed
   */
  export namespace FoldersDetailDetail {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = FolderModel[];
  }

  /**
   * No description
   * @tags Document
   * @name FoldersDetailDetail2
   * @summary Lists all folders in a specified parent folder, in a specified feed
   * @request GET:/documents/folders/{feedId}/{parentFolderId}
   * @originalName foldersDetailDetail
   * @duplicate
   * @response `200` `(FolderModel)[]` The folder data
   * @response `403` `void` The current user doesn't have sufficient rights to view folders for the feed
   */
  export namespace FoldersDetailDetail2 {
    export type RequestParams = {
      feedId: string;
      parentFolderId: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = FolderModel[];
  }

  /**
   * No description
   * @tags Document
   * @name FoldersUpdateDetail
   * @summary Updates the folder
   * @request PUT:/documents/folders/{folderId}
   * @response `200` `void` The folder was updated
   * @response `400` `void` Invalid folder setup specified
   * @response `403` `void` The current user doesn't have sufficient rights to edit folders for the feed
   * @response `404` `void` No folder was found for that id
   */
  export namespace FoldersUpdateDetail {
    export type RequestParams = {
      folderId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = FolderUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Document
   * @name FoldersDeleteDetail
   * @summary Deletes the specified folder
   * @request DELETE:/documents/folders/{folderId}
   * @response `200` `void` The folder was deleted
   * @response `403` `void` The current user doesn't have sufficient rights to delete folders for the feed
   * @response `404` `void` No folder was found for that id
   */
  export namespace FoldersDeleteDetail {
    export type RequestParams = {
      folderId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Document
   * @name ConvertPdfCreate
   * @request POST:/documents/convert/pdf
   * @response `200` `void` Success
   */
  export namespace ConvertPdfCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = DocumentConversionUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Document
   * @name ConvertPngCreate
   * @request POST:/documents/convert/png
   * @response `200` `void` Success
   */
  export namespace ConvertPngCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = DocumentConversionUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
}

export namespace Entities {
  /**
   * No description
   * @tags Entity
   * @name PermissionDetailDetail
   * @summary Determines whether the current user has a given permission on a specified object
   * @request GET:/entities/permission/{id}/{permission}
   * @deprecated
   * @response `200` `boolean` True or false
   * @response `404` `void` The given object was not found
   */
  export namespace PermissionDetailDetail {
    export type RequestParams = {
      id: string;
      permission: Permission;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = boolean;
  }

  /**
   * No description
   * @tags Entity
   * @name EntitiesDetailDetail
   * @summary Returns a single entity
   * @request GET:/entities/{id}
   * @response `200` `EntityMiniModel` Entity data
   * @response `404` `void` Not entity was found for given id
   */
  export namespace EntitiesDetailDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EntityMiniModel;
  }

  /**
   * No description
   * @tags Entity
   * @name PermissionsDetail
   * @summary Loads permissions on an object for the current user
   * @request GET:/entities/{id}/permissions
   * @response `200` `(Permission)[]` Permission data
   */
  export namespace PermissionsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = Permission[];
  }

  /**
   * No description
   * @tags Entity
   * @name CommunityEntitiesList
   * @summary Lists all ecosystem containers (projects, communities and nodes) with a given permissioni
   * @request GET:/entities/communityEntities
   * @response `200` `(CommunityEntityMiniModel)[]`
   */
  export namespace CommunityEntitiesList {
    export type RequestParams = {};
    export type RequestQuery = {
      /** ID of a given community entity */
      id?: string;
      /** Target permission */
      permission?: Permission;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModel[];
  }

  /**
   * No description
   * @tags Entity
   * @name SearchTotalsList
   * @summary Lists the totals for a global search query
   * @request GET:/entities/search/totals
   * @response `200` `SearchResultGlobalModel` Search result totals
   */
  export namespace SearchTotalsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = SearchResultGlobalModel;
  }

  /**
   * No description
   * @tags Entity
   * @name PathDetail
   * @summary Gets a path for an entity
   * @request GET:/entities/{id}/path
   * @response `200` `(EntityMiniModel)[]` Path data
   */
  export namespace PathDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EntityMiniModel[];
  }
}

export namespace Events {
  /**
   * No description
   * @tags Event
   * @name EventsCreate
   * @summary Adds a new event for the specified community entity.
   * @request POST:/events/{entityId}/events
   * @response `200` `string` The event was created
   * @response `403` `void` The current user doesn't have sufficient rights to add events for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace EventsCreate {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = EventUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Event
   * @name EventsDetail
   * @summary Lists all events for the specified entity
   * @request GET:/events/{entityId}/events
   * @response `200` `(EventModel)[]` The event data
   * @response `403` `void` The current user doesn't have sufficient rights to view events for the entity
   */
  export namespace EventsDetail {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {
      status?: AttendanceStatus;
      /** @format date-time */
      from?: string;
      /** @format date-time */
      to?: string;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EventModel[];
  }

  /**
   * No description
   * @tags Event
   * @name EventsList
   * @summary List all accessible events for a given search query. Only events accessible to the currently logged in user will be returned
   * @request GET:/events
   * @response `200` `EventModelListPage` A list of user events
   */
  export namespace EventsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EventModelListPage;
  }

  /**
   * No description
   * @tags Event
   * @name EventsDetailDetail
   * @summary Returns a single event
   * @request GET:/events/{id}
   * @response `403` `void` The current user doesn't have sufficient rights to see the event
   * @response `404` `void` No event was found for that id
   */
  export namespace EventsDetailDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Event
   * @name EventsUpdateDetail
   * @summary Updates the event
   * @request PUT:/events/{id}
   * @response `200` `void` The event was updated
   * @response `403` `void` The current user doesn't have sufficient rights to edit the event
   * @response `404` `void` No event was found for that id
   */
  export namespace EventsUpdateDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = EventUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name EventsDeleteDetail
   * @summary Deletes an event
   * @request DELETE:/events/{id}
   * @response `200` `void` The event was deleted
   * @response `403` `void` The current user doesn't have sufficient rights to delete this event
   * @response `404` `void` No event was found for that id
   */
  export namespace EventsDeleteDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name EventsNewDetail
   * @summary Returns a value indicating whether or not there are new events for a particular entity
   * @request GET:/events/{entityId}/events/new
   * @response `200` `boolean` True or false
   */
  export namespace EventsNewDetail {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = boolean;
  }

  /**
   * No description
   * @tags Event
   * @name AttendancesDetail
   * @summary Returns attendances for the specified event
   * @request GET:/events/{id}/attendances
   * @response `200` `(EventAttendanceModel)[]` The event attendances
   * @response `403` `void` The current user doesn't have sufficient rights to see attendees for the event
   * @response `404` `void` No event was found for that id
   */
  export namespace AttendancesDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      level?: AttendanceAccessLevel;
      status?: AttendanceStatus;
      type?: AttendanceType;
      labels?: string[];
      communityEntityTypes?: CommunityEntityType[];
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EventAttendanceModel[];
  }

  /**
   * No description
   * @tags Event
   * @name EcosystemCommunityEntitiesDetail
   * @summary Lists all ecosystem containers (projects, communities and nodes) for the given event
   * @request GET:/events/{id}/ecosystem/communityEntities
   * @response `200` `(EntityMiniModel)[]`
   * @response `403` `string` The current user does not have rights to view this event's contents
   * @response `404` `void` No event was found for that id
   */
  export namespace EcosystemCommunityEntitiesDetail {
    export type RequestParams = {
      /** ID of the event */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EntityMiniModel[];
  }

  /**
   * No description
   * @tags Event
   * @name EcosystemUsersDetail
   * @summary Lists all ecosystem members for the given event
   * @request GET:/events/{id}/ecosystem/users
   * @response `200` `(EntityMiniModel)[]`
   * @response `403` `string` The current user does not have rights to view this event's contents
   * @response `404` `void` No event was found for that id
   */
  export namespace EcosystemUsersDetail {
    export type RequestParams = {
      /** ID of the event */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EntityMiniModel[];
  }

  /**
   * No description
   * @tags Event
   * @name InviteCreate
   * @summary Invites the user, email address or community entity to the specified event
   * @request POST:/events/{id}/invite
   * @response `200` `void` The attendance was created
   * @response `400` `void` Either the invite is empty (neither user id, community entity id nor email is populated) or the event is private and the community entity id is popuated
   * @response `403` `void` The current user doesn't have sufficient rights to invite to the event
   * @response `404` `void` No event was found for that id
   * @response `409` `void` The user, email or community entity is already invited
   */
  export namespace InviteCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = EventAttendanceUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name InviteBatchCreate
   * @summary Invites a batch of users, email addresses or community entities to the specified event
   * @request POST:/events/{id}/invite/batch
   * @response `200` `void` The attendances were created
   * @response `403` `void` The current user doesn't have sufficient rights to invite to the event
   * @response `404` `void` No event was found for that id
   */
  export namespace InviteBatchCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = EventAttendanceUpsertModel[];
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name InviteBatchCommunityEntityCreate
   * @summary Invites all members of a specific community entity or entities to an event
   * @request POST:/events/{id}/invite/batch/communityEntity
   * @deprecated
   * @response `200` `void` The attendances were created
   * @response `403` `void` The current user doesn't have sufficient rights to invite to the event or to manage one of the community entities
   * @response `404` `void` No event was found for that id
   */
  export namespace InviteBatchCommunityEntityCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = string[];
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name AttendCreate
   * @summary Invites the current user to the specified event
   * @request POST:/events/{id}/attend
   * @response `200` `void` The attendance was created
   * @response `403` `void` The current user doesn't have sufficient rights to see the event
   * @response `404` `void` No event was found for that id
   * @response `409` `void` The user is already invited
   */
  export namespace AttendCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      originCommunityEntityId?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name AttendEmailCreate
   * @summary Invites the current user to the specified event (via email)
   * @request POST:/events/{id}/attend/email
   * @response `200` `void` The attendance was created
   * @response `403` `void` The current user doesn't have sufficient rights to see the event
   * @response `404` `void` No event was found for that id
   * @response `409` `void` The email is already invited
   */
  export namespace AttendEmailCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = EmailModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name AttendancesAcceptCreate
   * @summary Accepts the event invitation on behalf of a user or community entity
   * @request POST:/events/attendances/{attendanceId}/accept
   * @response `200` `void` The invite was accepted
   * @response `403` `void` The current user doesn't have sufficient rights to accept the invite
   * @response `404` `void` No pending invitation was found for that id
   */
  export namespace AttendancesAcceptCreate {
    export type RequestParams = {
      attendanceId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name AttendancesRejectCreate
   * @summary Rejects the event invitation on behalf of a user or community entity
   * @request POST:/events/attendances/{attendanceId}/reject
   * @response `200` `void` The invite was rejected
   * @response `403` `void` The current user doesn't have sufficient rights to reject the invite
   * @response `404` `void` No pending invitation was found for that id
   */
  export namespace AttendancesRejectCreate {
    export type RequestParams = {
      attendanceId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name AttendancesDeleteDetail
   * @summary Removes an attendance
   * @request DELETE:/events/attendances/{attendanceId}
   * @response `200` `void` The attendance was deleted
   * @response `400` `void` You are trying to remove the last organizer
   * @response `403` `void` The current user doesn't have sufficient rights to delete attendees from the event
   * @response `404` `void` No attendance was found for that id
   */
  export namespace AttendancesDeleteDetail {
    export type RequestParams = {
      attendanceId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name AttendancesBatchDelete
   * @summary Removes attendances in batch
   * @request DELETE:/events/attendances/batch
   * @response `200` `void` The attendances were deleted
   * @response `400` `void` You can only update attendee access level within one event
   * @response `403` `void` The current user doesn't have sufficient rights to delete attendees from the event
   */
  export namespace AttendancesBatchDelete {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = string[];
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name AttendancesAccessLevelCreate
   * @summary Updates the access level of an attendance
   * @request POST:/events/attendances/{attendanceId}/accessLevel
   * @response `200` `void` The access level was set
   * @response `403` `void` The current user doesn't have sufficient rights to manage access level for the event
   * @response `404` `void` No attendance was found for that id
   */
  export namespace AttendancesAccessLevelCreate {
    export type RequestParams = {
      attendanceId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = EventAttendanceAccessLevelModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name AttendancesAccessLevelBatchCreate
   * @summary Updates the access level of a batch of attendees
   * @request POST:/events/attendances/{attendanceId}/accessLevel/batch
   * @response `200` `void` The access level was set
   * @response `400` `void` You can only update attendee access level within one event
   * @response `403` `void` The current user doesn't have sufficient rights to manage attendee access level for the event
   */
  export namespace AttendancesAccessLevelBatchCreate {
    export type RequestParams = {
      attendanceId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = EventAttendanceLevelBatchModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name AttendancesLabelsCreate
   * @summary Updates the labels of an attendee
   * @request POST:/events/attendances/{attendanceId}/labels
   * @response `200` `void` The labels were set
   * @response `403` `void` The current user doesn't have sufficient rights to manage attendee labels for the event
   * @response `404` `void` No attendance was found for that id
   */
  export namespace AttendancesLabelsCreate {
    export type RequestParams = {
      attendanceId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = string[];
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name AttendancesLabelsBatchCreate
   * @summary Updates the labels of a batch of attendees
   * @request POST:/events/attendances/{attendanceId}/labels/batch
   * @response `200` `void` The labels were set
   * @response `400` `void` You can only update attendee labels within one event
   * @response `403` `void` The current user doesn't have sufficient rights to manage attendee labels for the event
   */
  export namespace AttendancesLabelsBatchCreate {
    export type RequestParams = {
      attendanceId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = EventAttendanceLabelBatchModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name DocumentsCreate
   * @summary Adds a new document for the specified event.
   * @request POST:/events/{id}/documents
   * @deprecated
   * @response `200` `string` The document was created
   * @response `403` `void` The current user doesn't have sufficient rights to add documents for the event
   * @response `404` `void` No event was found for that id
   */
  export namespace DocumentsCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = DocumentInsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Event
   * @name DocumentsDetail
   * @summary Lists all documents for the specified event, not including file data
   * @request GET:/events/{id}/documents
   * @deprecated
   * @response `200` `string` Documents
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the event
   * @response `404` `void` No event was found for that id
   */
  export namespace DocumentsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      folderId?: string;
      type?: DocumentFilter;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Event
   * @name DocumentsDetailDetail
   * @summary Returns a single document, including the file represented as base64
   * @request GET:/events/{id}/documents/{documentId}
   * @deprecated
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the event
   * @response `404` `void` No event was found for that id or the document does not exist
   */
  export namespace DocumentsDetailDetail {
    export type RequestParams = {
      id: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Event
   * @name DocumentsUpdateDetail
   * @summary Updates the title and description for the document
   * @request PUT:/events/{id}/documents/{documentId}
   * @deprecated
   * @response `200` `void` The document was updated
   * @response `403` `void` The current user doesn't have sufficient rights to edit documents for the event
   * @response `404` `void` No event was found for that id or the document does not exist
   */
  export namespace DocumentsUpdateDetail {
    export type RequestParams = {
      id: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = DocumentUpdateModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name DocumentsDeleteDetail
   * @summary Deletes the specified document
   * @request DELETE:/events/{id}/documents/{documentId}
   * @deprecated
   * @response `200` `void` The document was deleted
   * @response `403` `void` The current user doesn't have sufficient rights to delete the document
   * @response `404` `void` No event was found for that id or the document does not exist
   */
  export namespace DocumentsDeleteDetail {
    export type RequestParams = {
      id: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name DocumentsDraftDetail
   * @summary Returns a draft document for the specified event
   * @request GET:/events/{id}/documents/draft
   * @deprecated
   * @response `204` `void` No draft document was found for the event
   * @response `404` `void` No event was found for the specified id
   */
  export namespace DocumentsDraftDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Event
   * @name MessageCreate
   * @summary Sends a message to selected attendees
   * @request POST:/events/{id}/message
   * @response `200` `void` The message was successfully sent
   * @response `403` `void` The current user does not have the rights to send messages to event attendees
   * @response `404` `void` No event was found for that id
   */
  export namespace MessageCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = MessageModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
}

export namespace Feed {
  /**
   * No description
   * @tags Feed
   * @name FeedCreateDetail
   * @request POST:/feed/{feedId}
   * @response `200` `void` Success
   */
  export namespace FeedCreateDetail {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ContentEntityUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name FeedDetailDetail
   * @request GET:/feed/{feedId}
   * @deprecated
   * @response `200` `DiscussionModel` Discussion data, including stats for the currently logged in user
   * @response `403` `void` The current user doesn't have sufficient rights to view the feed
   */
  export namespace FeedDetailDetail {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {
      type?: ContentEntityType;
      filter?: ContentEntityFilter;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = DiscussionModel;
  }

  /**
   * No description
   * @tags Feed
   * @name PermissionsDetail
   * @request GET:/feed/{feedId}/permissions
   * @response `200` `(Permission)[]` Discussion permissions for the currently logged in user
   * @response `403` `void` The current user doesn't have sufficient rights to view the feed
   */
  export namespace PermissionsDetail {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = Permission[];
  }

  /**
   * No description
   * @tags Feed
   * @name MentionsDetail
   * @request GET:/feed/{feedId}/mentions
   * @deprecated
   * @response `200` `(ContentEntityModel)[]` Discussion mentions for the currently logged in user
   * @response `403` `void` The current user doesn't have sufficient rights to view the feed
   */
  export namespace MentionsDetail {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {
      type?: ContentEntityType;
      /** @format int32 */
      offset?: number;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ContentEntityModel[];
  }

  /**
   * No description
   * @tags Feed
   * @name ThreadsDetail
   * @request GET:/feed/{feedId}/threads
   * @deprecated
   * @response `200` `(ContentEntityModel)[]` Discussion threads for the currently logged in user
   * @response `403` `void` The current user doesn't have sufficient rights to view the feed
   */
  export namespace ThreadsDetail {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {
      type?: ContentEntityType;
      /** @format int32 */
      offset?: number;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ContentEntityModel[];
  }

  /**
   * No description
   * @tags Feed
   * @name PostsListDetail
   * @request GET:/feed/{feedId}/posts/list
   * @response `200` `ContentEntityModelListPage` Posts
   * @response `403` `void` The current user doesn't have sufficient rights to view the feed
   */
  export namespace PostsListDetail {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {
      type?: ContentEntityType;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ContentEntityModelListPage;
  }

  /**
   * No description
   * @tags Feed
   * @name MentionsListDetail
   * @request GET:/feed/{feedId}/mentions/list
   * @response `200` `ContentEntityModelListPage` Posts with mentions
   * @response `403` `void` The current user doesn't have sufficient rights to view the feed
   */
  export namespace MentionsListDetail {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {
      type?: ContentEntityType;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ContentEntityModelListPage;
  }

  /**
   * No description
   * @tags Feed
   * @name ThreadsListDetail
   * @request GET:/feed/{feedId}/threads/list
   * @response `200` `ContentEntityModelListPage` Posts with unread threads
   * @response `403` `void` The current user doesn't have sufficient rights to view the feed
   */
  export namespace ThreadsListDetail {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {
      type?: ContentEntityType;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ContentEntityModelListPage;
  }

  /**
   * No description
   * @tags Feed
   * @name NodePostsListDetail
   * @request GET:/feed/node/{nodeId}/posts/list
   * @response `200` `(DiscussionItemModel)[]` Posts
   */
  export namespace NodePostsListDetail {
    export type RequestParams = {
      nodeId: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = DiscussionItemModel[];
  }

  /**
   * No description
   * @tags Feed
   * @name NodeMentionsListDetail
   * @request GET:/feed/node/{nodeId}/mentions/list
   * @response `200` `(DiscussionItemModel)[]` Posts with mentions
   */
  export namespace NodeMentionsListDetail {
    export type RequestParams = {
      nodeId: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = DiscussionItemModel[];
  }

  /**
   * No description
   * @tags Feed
   * @name NodeThreadsListDetail
   * @request GET:/feed/node/{nodeId}/threads/list
   * @response `200` `(DiscussionItemModel)[]` Posts with unread threads
   */
  export namespace NodeThreadsListDetail {
    export type RequestParams = {
      nodeId: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = DiscussionItemModel[];
  }

  /**
   * No description
   * @tags Feed
   * @name GetFeed
   * @summary Returns information on whether there is a new post, thread or mention on a feed
   * @request GET:/feed/{feedId}/new
   * @response `200` `boolean` True or false
   */
  export namespace GetFeed {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = boolean;
  }

  /**
   * No description
   * @tags Feed
   * @name OpenedCreate
   * @summary Records the user having opened the specified feed
   * @request POST:/feed/{feedId}/opened
   * @response `200` `void` The feed was marked as opened
   */
  export namespace OpenedCreate {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name SeenCreate
   * @summary Records the user having read the specified feed
   * @request POST:/feed/{feedId}/seen
   * @response `200` `void` All new posts and mentions were marked as seen
   * @response `204` `void` There was nothing to mark as seen
   */
  export namespace SeenCreate {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name StarCreate
   * @summary Makes the feed starred for a user
   * @request POST:/feed/{feedId}/star
   * @response `404` `void` The feed has not yet been read by the user or is not accessible to them
   */
  export namespace StarCreate {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Feed
   * @name StarDelete
   * @summary Makes the feed unstarred for a user
   * @request DELETE:/feed/{feedId}/star
   * @response `404` `void` The feed has not yet been read by the user or is not accessible to them
   */
  export namespace StarDelete {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Feed
   * @name ActiveCreate
   * @summary Makes the feed unarchived for a user
   * @request POST:/feed/{feedId}/active
   * @response `404` `void` The feed has not yet been read by the user or is not accessible to them
   */
  export namespace ActiveCreate {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Feed
   * @name ActiveDelete
   * @summary Makes the feed archived for a user
   * @request DELETE:/feed/{feedId}/active
   * @response `404` `void` The feed has not yet been read by the user or is not accessible to them
   */
  export namespace ActiveDelete {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesFeedDetail
   * @request GET:/feed/contentEntities/{contentEntityId}/feed
   * @response `200` `FeedModel` The feed data
   * @response `403` `void` The current user does not have the rights to view the content entity
   * @response `404` `void` No content entity was found for that id
   */
  export namespace ContentEntitiesFeedDetail {
    export type RequestParams = {
      contentEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = FeedModel;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesDetailDetail
   * @request GET:/feed/contentEntities/{contentEntityId}
   * @response `200` `ContentEntityModel` The content entity data
   * @response `403` `void` The current user does not have the rights to view the content entity
   * @response `404` `void` No content entity was found for that id
   */
  export namespace ContentEntitiesDetailDetail {
    export type RequestParams = {
      contentEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ContentEntityModel;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesUpdateDetail
   * @request PUT:/feed/contentEntities/{contentEntityId}
   * @response `200` `void` Success
   */
  export namespace ContentEntitiesUpdateDetail {
    export type RequestParams = {
      contentEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ContentEntityUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesPartialUpdateDetail
   * @request PATCH:/feed/contentEntities/{contentEntityId}
   * @deprecated
   * @response `200` `void` Success
   */
  export namespace ContentEntitiesPartialUpdateDetail {
    export type RequestParams = {
      contentEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ContentEntityPatchModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesDeleteDetail
   * @request DELETE:/feed/contentEntities/{contentEntityId}
   * @response `200` `void` Success
   */
  export namespace ContentEntitiesDeleteDetail {
    export type RequestParams = {
      contentEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesSeenCreate
   * @summary Records the user having read a post
   * @request POST:/feed/contentEntities/{contentEntityId}/seen
   * @response `200` `void` All new replies and mentions were marked as seen
   * @response `204` `void` There was nothing to mark as seen
   * @response `404` `void` No content entity was found for that id
   */
  export namespace ContentEntitiesSeenCreate {
    export type RequestParams = {
      contentEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesSeenCreate2
   * @summary Records the user having read the specific content entities
   * @request POST:/feed/contentEntities/seen
   * @deprecated
   * @originalName contentEntitiesSeenCreate
   * @duplicate
   * @response `200` `void` The content entities were marked as seen
   * @response `404` `void` No content entity was found for one of the ids
   */
  export namespace ContentEntitiesSeenCreate2 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = string[];
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesReactionsCreate
   * @summary Creates or replaces a user's reaction to a content entity
   * @request POST:/feed/contentEntities/{contentEntityId}/reactions
   * @response `200` `string` Reaction created or replaced
   * @response `403` `void` The current user does not have the rights to view the feed
   * @response `404` `void` Content entity not found
   */
  export namespace ContentEntitiesReactionsCreate {
    export type RequestParams = {
      contentEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ReactionUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesReactionsDetail
   * @summary Lists all reactions to a content entity
   * @request GET:/feed/contentEntities/{contentEntityId}/reactions
   * @response `200` `(ReactionModel)[]` Reaction data
   * @response `403` `void` The current user does not have the rights to view the feed
   * @response `404` `void` Content entity not found
   */
  export namespace ContentEntitiesReactionsDetail {
    export type RequestParams = {
      contentEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ReactionModel[];
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesReactionsDeleteDetail
   * @summary Removes a user's reaction to a content entity
   * @request DELETE:/feed/contentEntities/{contentEntityId}/reactions/{id}
   * @response `200` `void` The reaction was deleted
   * @response `403` `void` The current user does not have the rights to delete the reaction
   * @response `404` `void` Reaction not found
   */
  export namespace ContentEntitiesReactionsDeleteDetail {
    export type RequestParams = {
      contentEntityId: string;
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesCommentsCreate
   * @request POST:/feed/contentEntities/{contentEntityId}/comments
   * @response `200` `void` Success
   */
  export namespace ContentEntitiesCommentsCreate {
    export type RequestParams = {
      contentEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = CommentUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesCommentsDetail
   * @request GET:/feed/contentEntities/{contentEntityId}/comments
   * @deprecated
   * @response `200` `(CommentModel)[]` The comment data
   * @response `403` `void` The current user doesn't have sufficient rights to view discussions for the parent entity
   * @response `404` `void` The feed, entity or content entity does not exist
   */
  export namespace ContentEntitiesCommentsDetail {
    export type RequestParams = {
      contentEntityId: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommentModel[];
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesCommentsListDetail
   * @request GET:/feed/contentEntities/{contentEntityId}/comments/list
   * @response `200` `CommentModelListPage` The comment data
   * @response `403` `void` The current user doesn't have sufficient rights to view discussions for the parent entity
   * @response `404` `void` The content entity does not exist
   */
  export namespace ContentEntitiesCommentsListDetail {
    export type RequestParams = {
      contentEntityId: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommentModelListPage;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesCommentsUpdateDetail
   * @request PUT:/feed/contentEntities/{contentEntityId}/comments/{id}
   * @response `200` `void` Success
   */
  export namespace ContentEntitiesCommentsUpdateDetail {
    export type RequestParams = {
      contentEntityId: string;
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = CommentUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesCommentsPartialUpdateDetail
   * @request PATCH:/feed/contentEntities/{contentEntityId}/comments/{id}
   * @deprecated
   * @response `200` `void` Success
   */
  export namespace ContentEntitiesCommentsPartialUpdateDetail {
    export type RequestParams = {
      contentEntityId: string;
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = CommentPatchModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesCommentsDeleteDetail
   * @request DELETE:/feed/contentEntities/{contentEntityId}/comments/{id}
   * @response `200` `void` Success
   */
  export namespace ContentEntitiesCommentsDeleteDetail {
    export type RequestParams = {
      contentEntityId: string;
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesCommentsSeenCreate
   * @summary Records the user having read the specific comments under a post
   * @request POST:/feed/contentEntities/comments/seen
   * @deprecated
   * @response `200` `void` All comments were marked as seen
   */
  export namespace ContentEntitiesCommentsSeenCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = string[];
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name CommentsSeenCreate
   * @summary Records the user having read the specific comments under a post
   * @request POST:/feed/comments/seen
   * @deprecated
   * @response `200` `void` All comments were marked as seen
   */
  export namespace CommentsSeenCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = string[];
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name CommentsReactionsCreate
   * @summary Creates or replaces a user's reaction to a comment
   * @request POST:/feed/comments/{commentId}/reactions
   * @response `200` `string` Reaction created or replaced
   * @response `403` `void` The current user does not have the rights to view the feed
   * @response `404` `void` Comment not found
   */
  export namespace CommentsReactionsCreate {
    export type RequestParams = {
      commentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ReactionUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Feed
   * @name CommentsReactionsDetail
   * @summary Lists all reactions to a comment
   * @request GET:/feed/comments/{commentId}/reactions
   * @response `200` `(ReactionModel)[]` Reaction data
   * @response `403` `void` The current user does not have the rights to view the feed
   * @response `404` `void` Comment not found
   */
  export namespace CommentsReactionsDetail {
    export type RequestParams = {
      commentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ReactionModel[];
  }

  /**
   * No description
   * @tags Feed
   * @name CommentsReactionsDeleteDetail
   * @summary Removes a user's reaction to a comment
   * @request DELETE:/feed/comments/{commentId}/reactions/{id}
   * @response `200` `void` The reaction was deleted
   * @response `403` `void` The current user does not have the rights to delete the reaction
   * @response `404` `void` Reaction not found
   */
  export namespace CommentsReactionsDeleteDetail {
    export type RequestParams = {
      commentId: string;
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesDocumentsCreate
   * @summary Adds a new document for the specified content entity.
   * @request POST:/feed/contentEntities/{contentEntityId}/documents
   * @response `200` `string` The document was created
   * @response `403` `void` The current user doesn't have sufficient rights to add documents for the content entity
   * @response `404` `void` No content entity was found for that id or the feed id is incorrect
   */
  export namespace ContentEntitiesDocumentsCreate {
    export type RequestParams = {
      contentEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = DocumentInsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesDocumentsUpdateDetail
   * @summary Updates the title and description for the document
   * @request PUT:/feed/contentEntities/{contentEntityId}/documents/{documentId}
   * @deprecated
   * @response `200` `void` The document was updated
   * @response `403` `void` The current user doesn't have sufficient rights to edit documents for the entity
   * @response `404` `void` No content entity was found for that id, the feed id is incorrect or the document does not exist
   */
  export namespace ContentEntitiesDocumentsUpdateDetail {
    export type RequestParams = {
      contentEntityId: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = DocumentUpdateModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesDocumentsDeleteDetail
   * @summary Deletes the specified document
   * @request DELETE:/feed/contentEntities/{contentEntityId}/documents/{documentId}
   * @response `200` `void` The document was deleted
   * @response `403` `void` The current user doesn't have sufficient rights to delete the document
   * @response `404` `void` No content entity was found for that id or the document does not exist
   */
  export namespace ContentEntitiesDocumentsDeleteDetail {
    export type RequestParams = {
      contentEntityId: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name ContentEntitiesCommentsDocumentsDeleteDetail
   * @summary Deletes the specified document
   * @request DELETE:/feed/contentEntities/{contentEntityId}/comments/{commentId}/documents/{documentId}
   * @deprecated
   * @response `200` `void` The document was deleted
   * @response `403` `void` The current user doesn't have sufficient rights to delete the document
   * @response `404` `void` No content entity was found for that id, no comment was found for that id or the document does not exist
   */
  export namespace ContentEntitiesCommentsDocumentsDeleteDetail {
    export type RequestParams = {
      contentEntityId: string;
      commentId: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name DraftDetailDetail
   * @summary Fetches a draft for a channel, feed or post id
   * @request GET:/feed/draft/{entityId}
   * @response `200` `string` The draft text
   */
  export namespace DraftDetailDetail {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Feed
   * @name DraftCreateDetail
   * @summary Creates or updates a draft for a channel, feed or post id
   * @request POST:/feed/draft/{entityId}
   * @response `200` `void` The draft was created or updated
   */
  export namespace DraftCreateDetail {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = string;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name NodesList
   * @summary Returns a list of nodes with metadata
   * @request GET:/feed/nodes
   * @response `200` `(NodeFeedDataModel)[]` Node metadata
   */
  export namespace NodesList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = NodeFeedDataModel[];
  }

  /**
   * No description
   * @tags Feed
   * @name NodesDetailDetail
   * @summary Returns a node with metadata
   * @request GET:/feed/nodes/{id}
   * @response `200` `NodeFeedDataModel` Node metadata
   */
  export namespace NodesDetailDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = NodeFeedDataModel;
  }

  /**
   * No description
   * @tags Feed
   * @name UsersDetail
   * @summary List all users for a given feed
   * @request GET:/feed/{feedId}/users
   * @response `200` `(UserMiniModel)[]` A list of user models
   * @response `403` `void` The current user doesn't have sufficient rights to list the feed's users
   */
  export namespace UsersDetail {
    export type RequestParams = {
      /** the id of the community entity, event, need, paper, document */
      feedId: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = UserMiniModel[];
  }

  /**
   * No description
   * @tags Feed
   * @name UsersAutocompleteDetail
   * @summary List all users for a given feed
   * @request GET:/feed/{feedId}/users/autocomplete
   * @response `200` `(UserMiniModel)[]` A list of user models
   * @response `403` `void` The current user doesn't have sufficient rights to list the feed's users
   */
  export namespace UsersAutocompleteDetail {
    export type RequestParams = {
      /** the id of the community entity, event, need, paper, document */
      feedId: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = UserMiniModel[];
  }

  /**
   * No description
   * @tags Feed
   * @name IntegrationsCreate
   * @summary Adds a feed integration to the specified feed
   * @request POST:/feed/{feedId}/integrations
   * @response `200` `string` The feed integration id
   * @response `403` `void` The current user doesn't have sufficient rights to add integrations to the feed
   * @response `404` `void` The operation failed validation; this may be because the source id does not exist, or it's private and the access token needs to be supplied
   * @response `409` `void` The integration already exists for the feed
   */
  export namespace IntegrationsCreate {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = FeedIntegrationUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Feed
   * @name IntegrationsDetail
   * @summary Lists feed integrations for the specified feed
   * @request GET:/feed/{feedId}/integrations
   * @response `200` `(FeedIntegrationModel)[]` The feed integration data
   * @response `403` `void` The current user doesn't have sufficient rights to view the feed
   */
  export namespace IntegrationsDetail {
    export type RequestParams = {
      feedId: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = FeedIntegrationModel[];
  }

  /**
   * No description
   * @tags Feed
   * @name IntegrationsTokenCreate
   * @summary Exchanges an authorization code for an access token
   * @request POST:/feed/integrations/token
   * @response `200` `string` The access token
   * @response `403` `void` The operation failed
   */
  export namespace IntegrationsTokenCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = FeedIntegrationTokenModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Feed
   * @name IntegrationsOptionsDetailDetail
   * @summary Lists feed integration options for a specific feed integration type
   * @request GET:/feed/integrations/options/{feedIntegrationType}
   * @response `200` `(string)[]` The options
   */
  export namespace IntegrationsOptionsDetailDetail {
    export type RequestParams = {
      feedIntegrationType: FeedIntegrationType;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string[];
  }

  /**
   * No description
   * @tags Feed
   * @name IntegrationsDeleteDetail
   * @summary Removes a feed integration
   * @request DELETE:/feed/integrations/{id}
   * @response `200` `void` The feed integration was removed
   * @response `403` `void` The current user doesn't have sufficient rights to remove integrations from the feed
   * @response `404` `void` No integration was found for that id
   */
  export namespace IntegrationsDeleteDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name ReportContentEntityCreateDetail
   * @summary Reports a content entity
   * @request POST:/feed/report/contentEntity/{id}
   * @response `200` `void` Successfully reported a post
   */
  export namespace ReportContentEntityCreateDetail {
    export type RequestParams = {
      /** The id of the content entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Feed
   * @name ReportCommentCreateDetail
   * @summary Reports a content entity
   * @request POST:/feed/report/comment/{id}
   * @response `200` `void` Successfully reported a comment
   */
  export namespace ReportCommentCreateDetail {
    export type RequestParams = {
      /** The id of the comment */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
}

export namespace Images {
  /**
   * No description
   * @tags Image
   * @name ImagesCreate
   * @request POST:/images
   * @response `200` `ImageInsertResultModel` Image data
   */
  export namespace ImagesCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ImageInsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = ImageInsertResultModel;
  }

  /**
   * No description
   * @tags Image
   * @name FullDetail
   * @request GET:/images/{id}/full
   * @response `200` `void` Success
   */
  export namespace FullDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Image
   * @name FullModelDetail
   * @request GET:/images/{id}/full/model
   * @response `200` `ImageModel` Image data
   */
  export namespace FullModelDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ImageModel;
  }

  /**
   * No description
   * @tags Image
   * @name GetImages
   * @request GET:/images/{id}/tn
   * @response `200` `void` Success
   */
  export namespace GetImages {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Image
   * @name TnModelDetail
   * @request GET:/images/{id}/tn/model
   * @response `200` `ImageModel` Image data
   */
  export namespace TnModelDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ImageModel;
  }

  /**
   * No description
   * @tags Image
   * @name ConvertCreate
   * @request POST:/images/convert
   * @response `200` `void` Success
   */
  export namespace ConvertCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ImageConversionUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
}

export namespace Needs {
  /**
   * No description
   * @tags Need
   * @name NeedsCreate
   * @summary Adds a new need for the specified feed.
   * @request POST:/needs/{entityId}/needs
   * @response `200` `string` The need was created
   * @response `403` `void` The current user doesn't have sufficient rights to add need for the entity
   */
  export namespace NeedsCreate {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = NeedUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Need
   * @name NeedsDetail
   * @summary Lists all needs for the specified entity
   * @request GET:/needs/{entityId}/needs
   * @response `200` `(NeedModel)[]` The need data
   * @response `403` `void` The current user doesn't have sufficient rights to view needs for the entity
   */
  export namespace NeedsDetail {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = NeedModel[];
  }

  /**
   * No description
   * @tags Need
   * @name NeedsList
   * @summary Lists all needs
   * @request GET:/needs
   * @response `200` `NeedModelListPage` A list of needs matching the search query visible to the current user
   */
  export namespace NeedsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = NeedModelListPage;
  }

  /**
   * No description
   * @tags Need
   * @name NeedsDetailDetail
   * @summary Returns a single need
   * @request GET:/needs/{id}
   * @response `200` `NeedModel`
   * @response `403` `void` The current user doesn't have sufficient rights to see the need
   * @response `404` `void` No need was found for that id
   */
  export namespace NeedsDetailDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = NeedModel;
  }

  /**
   * No description
   * @tags Need
   * @name NeedsUpdateDetail
   * @summary Updates the need
   * @request PUT:/needs/{id}
   * @response `200` `void` The need was updated
   * @response `403` `void` The current user doesn't have sufficient rights to edit the need
   * @response `404` `void` No need was found for that id
   */
  export namespace NeedsUpdateDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = NeedUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Need
   * @name NeedsDeleteDetail
   * @summary Deletes a need
   * @request DELETE:/needs/{id}
   * @response `200` `void` The need was deleted
   * @response `403` `void` The current user doesn't have sufficient rights to delete this need
   * @response `404` `void` No need was found for that id
   */
  export namespace NeedsDeleteDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Need
   * @name NeedsNewDetail
   * @summary Returns a value indicating whether or not there are new needs for a particular entity
   * @request GET:/needs/{entityId}/needs/new
   * @response `200` `boolean` True or false
   */
  export namespace NeedsNewDetail {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = boolean;
  }
}

export namespace Nodes {
  /**
   * No description
   * @tags Node
   * @name NodesCreate
   * @summary Create a new node. The current user becomes a member of the node with the Owner role
   * @request POST:/nodes
   * @response `200` `void` Success
   */
  export namespace NodesCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = NodeUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name NodesList
   * @summary List all accessible nodes for a given search query. Only nodes accessible to the currently logged in user will be returned
   * @request GET:/nodes
   * @response `200` `CommunityEntityMiniModelListPage`
   */
  export namespace NodesList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModelListPage;
  }

  /**
   * No description
   * @tags Node
   * @name NodesDetailDetail
   * @summary Returns a node
   * @request GET:/nodes/{id}
   * @response `200` `NodeModel` The node data
   * @response `404` `void` No node was found for that id
   */
  export namespace NodesDetailDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = NodeModel;
  }

  /**
   * No description
   * @tags Node
   * @name NodesPartialUpdateDetail
   * @summary Patches the specified node
   * @request PATCH:/nodes/{id}
   * @response `200` `string` The ID of the entity
   * @response `403` `void` The current user does not have rights to edit this node
   * @response `404` `void` No node was found for that id
   */
  export namespace NodesPartialUpdateDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = NodePatchModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Node
   * @name NodesUpdateDetail
   * @summary Updates the specified node
   * @request PUT:/nodes/{id}
   * @response `200` `string` The ID of the entity
   * @response `403` `void` The current user does not have rights to edit this node
   * @response `404` `void` No node was found for that id
   */
  export namespace NodesUpdateDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = NodeUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Node
   * @name NodesDeleteDetail
   * @summary Deletes the specified node and removes all of the node's associations from communities and nodes
   * @request DELETE:/nodes/{id}
   * @response `403` `void` The current user does not have rights to delete this node
   * @response `404` `void` No node was found for that id
   */
  export namespace NodesDeleteDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Node
   * @name DetailDetail
   * @summary Returns a node including detailed stats
   * @request GET:/nodes/{id}/detail
   * @response `200` `NodeDetailModel` The node data including detailed stats
   * @response `404` `void` No node was found for that id
   */
  export namespace DetailDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = NodeDetailModel;
  }

  /**
   * No description
   * @tags Node
   * @name AutocompleteList
   * @summary List the basic information of nodes for a given search query. Only nodes accessible to the currently logged in user will be returned
   * @request GET:/nodes/autocomplete
   * @response `200` `(CommunityEntityMiniModel)[]`
   */
  export namespace AutocompleteList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModel[];
  }

  /**
   * No description
   * @tags Node
   * @name WorkspacesDetail
   * @request GET:/nodes/{id}/workspaces
   * @response `200` `(WorkspaceModel)[]` Workspace data
   * @response `403` `string` The current user does not have rights to view this node's content
   * @response `404` `void` No node was found for that id
   */
  export namespace WorkspacesDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = WorkspaceModel[];
  }

  /**
   * No description
   * @tags Node
   * @name OrganizationsDetail
   * @request GET:/nodes/{id}/organizations
   * @response `403` `string` The current user does not have rights to view this node's content
   * @response `404` `void` No node was found for that id
   */
  export namespace OrganizationsDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Node
   * @name CfpsDetail
   * @summary Lists all cfps for the specified node
   * @request GET:/nodes/{id}/cfps
   * @response `200` `(CallForProposalMiniModel)[]`
   * @response `403` `string` The current user does not have rights to view this node's contents
   * @response `404` `void` No node was found for that id
   */
  export namespace CfpsDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CallForProposalMiniModel[];
  }

  /**
   * No description
   * @tags Node
   * @name NeedsAggregateDetail
   * @summary Lists all needs for the specified node and its ecosystem
   * @request GET:/nodes/{id}/needs/aggregate
   * @response `200` `NeedModelListPage` A list of needs in the specified node matching the search query visible to the current user
   * @response `404` `void` No node was found for that id
   */
  export namespace NeedsAggregateDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {
      communityEntityIds?: string;
      filter?: FeedEntityFilter;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = NeedModelListPage;
  }

  /**
   * No description
   * @tags Node
   * @name NeedsAggregateNewDetail
   * @summary Returns a value indicating whether or not there are new needs for a given node
   * @request GET:/nodes/{id}/needs/aggregate/new
   * @response `200` `boolean` True or false
   */
  export namespace NeedsAggregateNewDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {
      filter?: FeedEntityFilter;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = boolean;
  }

  /**
   * No description
   * @tags Node
   * @name NeedsAggregateCommunityEntitiesDetail
   * @summary Lists all community entities for needs for the specified node and its ecosystem
   * @request GET:/nodes/{id}/needs/aggregate/communityEntities
   * @response `200` `(CommunityEntityMiniModel)[]`
   * @response `403` `void` The current user does not have rights to view this node's content
   * @response `404` `void` No node was found for that id
   */
  export namespace NeedsAggregateCommunityEntitiesDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {
      types?: CommunityEntityType[];
      currentUser?: boolean;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModel[];
  }

  /**
   * No description
   * @tags Node
   * @name EventsAggregateDetail
   * @summary Lists all events for the specified node and its ecosystem
   * @request GET:/nodes/{id}/events/aggregate
   * @response `200` `EventModelListPage` Event data
   * @response `404` `void` No node was found for that id
   */
  export namespace EventsAggregateDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {
      communityEntityIds?: string;
      filter?: FeedEntityFilter;
      status?: AttendanceStatus;
      /** @format date-time */
      from?: string;
      /** @format date-time */
      to?: string;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EventModelListPage;
  }

  /**
   * No description
   * @tags Node
   * @name EventsAggregateNewDetail
   * @summary Returns a value indicating whether or not there are new events for a given node
   * @request GET:/nodes/{id}/events/aggregate/new
   * @response `200` `boolean` True or false
   */
  export namespace EventsAggregateNewDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {
      filter?: FeedEntityFilter;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = boolean;
  }

  /**
   * No description
   * @tags Node
   * @name EventsAggregateCommunityEntitiesDetail
   * @summary Lists all community entities for events for the specified node and its ecosystem
   * @request GET:/nodes/{id}/events/aggregate/communityEntities
   * @response `200` `(CommunityEntityMiniModel)[]`
   * @response `403` `void` The current user does not have rights to view this node's content
   * @response `404` `void` No node was found for that id
   */
  export namespace EventsAggregateCommunityEntitiesDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {
      types?: CommunityEntityType[];
      currentUser?: boolean;
      tags?: EventTag[];
      /** @format date-time */
      from?: string;
      /** @format date-time */
      to?: string;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModel[];
  }

  /**
   * No description
   * @tags Node
   * @name DocumentsAggregateDetail
   * @summary Lists all documents for the specified node and its ecosystem
   * @request GET:/nodes/{id}/documents/aggregate
   * @response `200` `DocumentModelListPage`
   * @response `404` `void` No node was found for that id
   */
  export namespace DocumentsAggregateDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {
      communityEntityIds?: string;
      type?: DocumentFilter;
      filter?: FeedEntityFilter;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = DocumentModelListPage;
  }

  /**
   * No description
   * @tags Node
   * @name DocumentsAggregateNewDetail
   * @summary Returns a value indicating whether or not there are new documents for a given node
   * @request GET:/nodes/{id}/documents/aggregate/new
   * @response `200` `boolean` True or false
   */
  export namespace DocumentsAggregateNewDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {
      filter?: FeedEntityFilter;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = boolean;
  }

  /**
   * No description
   * @tags Node
   * @name DocumentsAggregateCommunityEntitiesDetail
   * @summary Lists all community entities for documents for the specified node and its ecosystem
   * @request GET:/nodes/{id}/documents/aggregate/communityEntities
   * @response `200` `(CommunityEntityMiniModel)[]`
   * @response `403` `void` The current user does not have rights to view this node's content
   * @response `404` `void` No node was found for that id
   */
  export namespace DocumentsAggregateCommunityEntitiesDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {
      types?: CommunityEntityType[];
      type?: DocumentFilter;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModel[];
  }

  /**
   * No description
   * @tags Node
   * @name PapersAggregateDetail
   * @summary Lists all papers for the specified node and its ecosystem
   * @request GET:/nodes/{id}/papers/aggregate
   * @response `200` `PaperModelListPage`
   * @response `404` `void` No node was found for that id
   */
  export namespace PapersAggregateDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {
      communityEntityIds?: string;
      type?: PaperType;
      filter?: FeedEntityFilter;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PaperModelListPage;
  }

  /**
   * No description
   * @tags Node
   * @name PapersAggregateNewDetail
   * @summary Returns a value indicating whether or not there are new papers for a given node
   * @request GET:/nodes/{id}/papers/aggregate/new
   * @response `200` `boolean` True or false
   */
  export namespace PapersAggregateNewDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {
      filter?: FeedEntityFilter;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = boolean;
  }

  /**
   * No description
   * @tags Node
   * @name PapersAggregateCommunityEntitiesDetail
   * @summary Lists all community entities for papers for the specified node and its ecosystem
   * @request GET:/nodes/{id}/papers/aggregate/communityEntities
   * @response `200` `(CommunityEntityMiniModel)[]`
   * @response `403` `void` The current user does not have rights to view this node's content
   * @response `404` `void` No node was found for that id
   */
  export namespace PapersAggregateCommunityEntitiesDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {
      types?: CommunityEntityType[];
      type?: PaperType;
      tags?: PaperTag[];
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModel[];
  }

  /**
   * No description
   * @tags Node
   * @name FeedAggregateNewDetail
   * @summary Returns a value indicating whether or not there are new posts in all feeds for a given node
   * @request GET:/nodes/{id}/feed/aggregate/new
   * @response `200` `boolean` True or false
   */
  export namespace FeedAggregateNewDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = boolean;
  }

  /**
   * No description
   * @tags Node
   * @name ChannelAggregateNewDetail
   * @summary Returns a value indicating whether or not there are new posts in channels for a given node
   * @request GET:/nodes/{id}/channel/aggregate/new
   * @response `200` `boolean` True or false
   */
  export namespace ChannelAggregateNewDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = boolean;
  }

  /**
   * No description
   * @tags Node
   * @name UsersAggregateDetail
   * @summary Lists all needs for the specified node and its ecosystem
   * @request GET:/nodes/{id}/users/aggregate
   * @response `200` `UserMiniModelListPage` A list of needs in the specified node matching the search query
   * @response `404` `void` No node was found for that id
   */
  export namespace UsersAggregateDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {
      communityEntityIds?: string;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = UserMiniModelListPage;
  }

  /**
   * No description
   * @tags Node
   * @name UsersAggregateCommunityEntitiesDetail
   * @summary Lists all community entities for users for the specified node and its ecosystem
   * @request GET:/nodes/{id}/users/aggregate/communityEntities
   * @response `200` `(CommunityEntityMiniModel)[]`
   * @response `403` `void` The current user does not have rights to view this node's content
   * @response `404` `void` No node was found for that id
   */
  export namespace UsersAggregateCommunityEntitiesDetail {
    export type RequestParams = {
      /** ID of the node */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModel[];
  }

  /**
   * No description
   * @tags Node
   * @name SearchTotalsDetail
   * @summary Lists the totals for a global search query
   * @request GET:/nodes/{id}/search/totals
   * @response `200` `SearchResultNodeModel` Search result totals
   */
  export namespace SearchTotalsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = SearchResultNodeModel;
  }

  /**
   * No description
   * @tags Node
   * @name DocumentsCreate
   * @summary Adds a new document for the specified entity.
   * @request POST:/nodes/{id}/documents
   * @deprecated
   * @response `200` `string` The document was created
   * @response `400` `void` Note-typed documents have to be created and updated through the feed endpoints
   * @response `403` `void` The current user doesn't have sufficient rights to add documents for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace DocumentsCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = DocumentInsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Node
   * @name DocumentsDetail
   * @summary Lists all documents for the specified entity, not including file data
   * @request GET:/nodes/{id}/documents
   * @deprecated
   * @response `200` `string` The document was created
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace DocumentsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      folderId?: string;
      type?: DocumentFilter;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Node
   * @name DocumentsDetailDetail
   * @summary Returns a single document, including the file represented as base64
   * @request GET:/nodes/{id}/documents/{documentId}
   * @deprecated
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
   * @response `404` `void` No entity was found for that id or the document does not exist
   */
  export namespace DocumentsDetailDetail {
    export type RequestParams = {
      id: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Node
   * @name DocumentsUpdateDetail
   * @summary Updates the title and description for the document
   * @request PUT:/nodes/{id}/documents/{documentId}
   * @deprecated
   * @response `200` `void` The document was updated
   * @response `403` `void` The current user doesn't have sufficient rights to edit documents for the entity
   * @response `404` `void` No entity was found for that id or the document does not exist
   */
  export namespace DocumentsUpdateDetail {
    export type RequestParams = {
      id: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = DocumentUpdateModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name DocumentsDeleteDetail
   * @summary Deletes the specified document
   * @request DELETE:/nodes/{id}/documents/{documentId}
   * @deprecated
   * @response `200` `void` The document was deleted
   * @response `403` `void` The current user doesn't have sufficient rights to delete the document
   * @response `404` `void` No entity was found for that id or the document does not exist
   */
  export namespace DocumentsDeleteDetail {
    export type RequestParams = {
      id: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name DocumentsDraftDetail
   * @summary Returns a draft document for the specified container
   * @request GET:/nodes/{id}/documents/draft
   * @deprecated
   * @response `204` `void` No draft document was found for the community entity
   * @response `404` `void` No community entity was found for the specified id
   */
  export namespace DocumentsDraftDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name DocumentsAllDetail
   * @summary Lists all documents and folders for the specified entity, not including file data
   * @request GET:/nodes/{id}/documents/all
   * @deprecated
   * @response `200` `(BaseModel)[]` The document was created
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace DocumentsAllDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = BaseModel[];
  }

  /**
   * No description
   * @tags Node
   * @name JoinCreate
   * @summary Joins an entity on behalf of the currently logged in user. Only works for community entities with the Open membership access level. Creates a membership record immediately, no invitation is created.
   * @request POST:/nodes/{id}/join
   * @deprecated
   * @response `200` `string` ID of the new membership object
   * @response `403` `void` The entity is not open to members joining
   * @response `404` `void` The entity does not exist
   * @response `409` `void` A membership record already exists for the entity and user
   */
  export namespace JoinCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Node
   * @name RequestCreate
   * @summary Creates a request to join an entity on behalf of the currently logged in user
   * @request POST:/nodes/{id}/request
   * @deprecated
   * @response `200` `string` ID of the new invitation object
   * @response `403` `void` The entity is not open to members requesting to join
   * @response `404` `void` The entity does not exist
   * @response `409` `void` An invitation already exists for the entity and user
   */
  export namespace RequestCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Node
   * @name InviteIdCreateDetail
   * @summary Invite a user to an entity using their ID
   * @request POST:/nodes/{id}/invite/id/{userId}
   * @response `200` `string` ID of the new invitation object
   * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity, or the user is inviting an owner without having the rights to manage owners
   * @response `404` `void` The entity or user do not exist
   * @response `409` `void` An invitation already exists for the entity and user
   */
  export namespace InviteIdCreateDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the user to invite */
      userId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = AccessLevel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Node
   * @name InviteEmailCreateDetail
   * @summary Invite a user to an entity using their email
   * @request POST:/nodes/{id}/invite/email/{userEmail}
   * @response `200` `string` ID of the new invitation object
   * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace InviteEmailCreateDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the user to invite */
      userEmail: string;
    };
    export type RequestQuery = {};
    export type RequestBody = AccessLevel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Node
   * @name InviteEmailBatchCreate
   * @summary Invite a user to an entity using their email
   * @request POST:/nodes/{id}/invite/email/batch
   * @deprecated
   * @response `200` `string` ID of the new invitation object
   * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace InviteEmailBatchCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = string[];
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Node
   * @name InvitesDetail
   * @summary List all pending invitations for a given entity
   * @request GET:/nodes/{id}/invites
   * @response `200` `(InvitationModelUser)[]` A list of invitations
   * @response `404` `void` No entity was found for that id
   */
  export namespace InvitesDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = InvitationModelUser[];
  }

  /**
   * No description
   * @tags Node
   * @name InvitesAcceptCreate
   * @summary Accept the invite on behalf of the currently logged-in user
   * @request POST:/nodes/{id}/invites/{invitationId}/accept
   * @response `200` `void` The invitation has been accepted. The user is now a member of the project.
   * @response `403` `void` The current user does not have the right to accept the invitation
   * @response `404` `void` No entity was found for that id or the invitation does not exist
   */
  export namespace InvitesAcceptCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name InvitesRejectCreate
   * @summary Reject the invite on behalf of the currently logged-in user
   * @request POST:/nodes/{id}/invites/{invitationId}/reject
   * @response `200` `void` The invitation has been rejected
   * @response `403` `void` The current user does not have the right to reject the invitation
   * @response `404` `void` No entity was found for that id or the invitation does not exist
   */
  export namespace InvitesRejectCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name InvitesCancelCreate
   * @summary Cancels an invite
   * @request POST:/nodes/{id}/invites/{invitationId}/cancel
   * @response `200` `void` The invitation has been cancelled
   * @response `403` `void` The current user does not have the right to cancel the invitation
   * @response `404` `void` No entity was found for that id or the invitation does not exist
   */
  export namespace InvitesCancelCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name InvitesResendCreate
   * @summary Resends an invite
   * @request POST:/nodes/invites/{invitationId}/resend
   * @response `200` `void` The invitation has been resent
   * @response `400` `void` The invitation is a request and cannot be resent
   * @response `403` `void` The user does not have the right to resend the invitation
   * @response `404` `void` No entity was found for that id or the invitation does not exist
   */
  export namespace InvitesResendCreate {
    export type RequestParams = {
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name LeaveCreate
   * @summary Leaves an entity on behalf of the currently logged in user
   * @request POST:/nodes/{id}/leave
   * @deprecated
   * @response `200` `void` The user has successfully left the entity
   * @response `404` `void` The entity does not exist or the user is not a member
   */
  export namespace LeaveCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name OnboardingResponsesDetailDetail
   * @summary List onboarding responses for a community entity and a user
   * @request GET:/nodes/{id}/onboardingResponses/{userId}
   * @deprecated
   * @response `200` `OnboardingQuestionnaireInstanceModel` onboarding questionnaire responses
   * @response `404` `void` No entity was found for that id or no user was found
   */
  export namespace OnboardingResponsesDetailDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      userId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = OnboardingQuestionnaireInstanceModel;
  }

  /**
   * No description
   * @tags Node
   * @name OnboardingResponsesCreate
   * @summary Posts onboarding responses for a community entity for the current user
   * @request POST:/nodes/{id}/onboardingResponses
   * @deprecated
   * @response `200` `void` The onboarding questionnaire responses were saved successfully
   * @response `404` `void` No entity was found for that id
   */
  export namespace OnboardingResponsesCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = OnboardingQuestionnaireInstanceUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name OnboardingCompletionCreate
   * @summary Records the completion of the onboarding workflow for a community entity for the current user
   * @request POST:/nodes/{id}/onboardingCompletion
   * @deprecated
   * @response `200` `void` The onboarding completion was recorded successfully
   * @response `404` `void` No entity was found for that id
   */
  export namespace OnboardingCompletionCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name MembersDetail
   * @summary List all members for an entity
   * @request GET:/nodes/{id}/members
   * @response `200` `(MemberModel)[]` A list of members
   * @response `403` `void` The current user doesn't have sufficient rights to see the entity's members
   * @response `404` `void` No entity was found for that id
   */
  export namespace MembersDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = MemberModel[];
  }

  /**
   * No description
   * @tags Node
   * @name InvitationDetail
   * @summary Returns the pending invitation for an object for the current user
   * @request GET:/nodes/{id}/invitation
   * @response `200` `InvitationModelEntity` A pending invitation
   * @response `404` `void` No invitation is pending or no entity was found for that id
   */
  export namespace InvitationDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = InvitationModelEntity;
  }

  /**
   * No description
   * @tags Node
   * @name MembersUpdateDetail
   * @summary Updates a member's access level for an entity
   * @request PUT:/nodes/{id}/members/{memberId}
   * @response `200` `void` The access level has been updated
   * @response `403` `void` The user does not have the right to set this access level for this member
   * @response `404` `void` No entity was found for that id or the user is not a member of that entity
   */
  export namespace MembersUpdateDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the entity */
      memberId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = AccessLevel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name MembersDeleteDetail
   * @summary Removes a member's access from an entity
   * @request DELETE:/nodes/{id}/members/{memberId}
   * @response `200` `void` The member has been removed
   * @response `403` `void` The current user does not have the right to remove members from this entity
   * @response `404` `void` No entity was found for that id or the user is not a member of that entity
   */
  export namespace MembersDeleteDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the entity */
      memberId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name MembersContributionUpdate
   * @summary Updates a member's contribution
   * @request PUT:/nodes/{id}/members/{memberId}/contribution
   * @response `200` `void` The contribution has been updated
   * @response `403` `void` The user does not have the right to set the contribution for other members
   * @response `404` `void` No entity was found for that id or the user is not a member of that entity
   */
  export namespace MembersContributionUpdate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the user */
      memberId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = string;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name MembersLabelsUpdate
   * @summary Updates a member's labels
   * @request PUT:/nodes/{id}/members/{memberId}/labels
   * @response `200` `void` The labels have been updated
   * @response `403` `void` The current user does not have the right to set labels for members
   * @response `404` `void` No entity was found for that id or the user is not a member of that entity
   */
  export namespace MembersLabelsUpdate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the user */
      memberId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = string[];
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name CommunityEntityInviteCreateDetail
   * @summary Invite a community entity to become affiliated to another entity
   * @request POST:/nodes/{id}/communityEntityInvite/{targetEntityId}
   * @response `200` `string` ID of the new invitation object
   * @response `400` `void` An invitation cannot be extended to a community entity of the same type
   * @response `403` `void` The current user does not have the rights to invite entities to the specified entity
   * @response `404` `void` One of the entities does not exist
   * @response `409` `void` An invitation or affiliation already exists for the entities
   */
  export namespace CommunityEntityInviteCreateDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the target entity */
      targetEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Node
   * @name CommunityEntityInvitesIncomingDetail
   * @summary List all pending invitations for a given entity
   * @request GET:/nodes/{id}/communityEntityInvites/incoming
   * @response `200` `(CommunityEntityInvitationModelSource)[]` A list of invitations
   * @response `404` `void` No entity was found for that id
   */
  export namespace CommunityEntityInvitesIncomingDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityInvitationModelSource[];
  }

  /**
   * No description
   * @tags Node
   * @name CommunityEntityInvitesOutgoingDetail
   * @summary List all pending invitations for a given entity
   * @request GET:/nodes/{id}/communityEntityInvites/outgoing
   * @response `200` `(CommunityEntityInvitationModelSource)[]` A list of invitations
   * @response `404` `void` No entity was found for that id
   */
  export namespace CommunityEntityInvitesOutgoingDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityInvitationModelSource[];
  }

  /**
   * No description
   * @tags Node
   * @name CommunityEntityInvitesAcceptCreate
   * @summary Accept an invite
   * @request POST:/nodes/{id}/communityEntityInvites/{invitationId}/accept
   * @response `200` `void` The invitation has been accepted
   * @response `403` `void` The current user does not have the right to accept the invitation
   * @response `404` `void` One of the entities does not exist or the invitation does not exist
   */
  export namespace CommunityEntityInvitesAcceptCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name CommunityEntityInvitesRejectCreate
   * @summary Rejects an invite
   * @request POST:/nodes/{id}/communityEntityInvites/{invitationId}/reject
   * @response `200` `void` The invitation has been rejected
   * @response `403` `void` The current user does not have the right to reject the invitation
   * @response `404` `void` One of the entities does not exist or the invitation does not exist
   */
  export namespace CommunityEntityInvitesRejectCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name CommunityEntityInvitesCancelCreate
   * @summary Cancels an invite
   * @request POST:/nodes/{id}/communityEntityInvites/{invitationId}/cancel
   * @response `200` `void` The invitation has been cancelled
   * @response `403` `void` The current user does not have the right to cancel the invitation
   * @response `404` `void` One of the entities does not exist or the invitation does not exist
   */
  export namespace CommunityEntityInvitesCancelCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name CommunityEntityRemoveCreate
   * @summary Removes an affiliation of an entity to another entity
   * @request POST:/nodes/{id}/communityEntity/{targetEntityId}/remove
   * @response `200` `void` The entity affiliation has been removed successfully
   * @response `403` `void` The current user does not have the permissions to manage the community entity's affiliations
   * @response `404` `void` One of the entities does not exist or the entities are not affiliated
   */
  export namespace CommunityEntityRemoveCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the target entity */
      targetEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name CommunityEntityLinkCreate
   * @summary Creates an affiliation of an entity to another entity. On the entity, the user must be admin or owner, or the toggle for members to allow creating community entities of the respective type must be on. The user must be admin or owner on the target entity.
   * @request POST:/nodes/{id}/communityEntity/{targetEntityId}/link
   * @response `200` `void` The entity affiliation has been created successfully
   * @response `409` `void` One of the entities does not exist or the entities are already affiliated
   */
  export namespace CommunityEntityLinkCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the target entity */
      targetEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name EcosystemCommunityEntitiesDetail
   * @summary Lists all ecosystem containers (projects, communities and nodes) for the given community entity
   * @request GET:/nodes/{id}/ecosystem/communityEntities
   * @deprecated
   * @response `200` `(EntityMiniModel)[]`
   * @response `403` `string` The current user does not have rights to view this community entity's contents
   * @response `404` `void` No community entity was found for that id
   */
  export namespace EcosystemCommunityEntitiesDetail {
    export type RequestParams = {
      /** ID of the community entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EntityMiniModel[];
  }

  /**
   * No description
   * @tags Node
   * @name EcosystemUsersDetail
   * @summary Lists all ecosystem members for the given community entity
   * @request GET:/nodes/{id}/ecosystem/users
   * @response `200` `(EntityMiniModel)[]`
   * @response `403` `string` The current user does not have rights to view this community entity's contents
   * @response `404` `void` No community entity was found for that id
   */
  export namespace EcosystemUsersDetail {
    export type RequestParams = {
      /** ID of the community entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EntityMiniModel[];
  }

  /**
   * No description
   * @tags Node
   * @name NeedsCreate
   * @summary Adds a new need for the specified project
   * @request POST:/nodes/{id}/needs
   * @deprecated
   * @response `200` `string` The ID of the new need
   * @response `403` `void` The current user doesn't have sufficient rights to add needs for the entity
   */
  export namespace NeedsCreate {
    export type RequestParams = {
      /** ID of the project */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = NeedUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Node
   * @name NeedsDetail
   * @summary Lists all needs for the specified community entity
   * @request GET:/nodes/{id}/needs
   * @deprecated
   * @response `200` `(NeedModel)[]`
   * @response `403` `string` The current user does not have rights to view this community entity's contents
   * @response `404` `void` No community entity was found for that id
   */
  export namespace NeedsDetail {
    export type RequestParams = {
      /** ID of the community entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = NeedModel[];
  }

  /**
   * No description
   * @tags Node
   * @name NeedsDetailDetail
   * @summary Returns a single need
   * @request GET:/nodes/needs/{needId}
   * @deprecated
   * @response `403` `string` The current user does not have rights to view this community entity's contents
   * @response `404` `void` No need was found for the need id
   */
  export namespace NeedsDetailDetail {
    export type RequestParams = {
      needId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Node
   * @name NeedsUpdateDetail
   * @summary Updates the need
   * @request PUT:/nodes/needs/{needId}
   * @deprecated
   * @response `200` `void` The need was updated
   * @response `403` `void` The current user does not have rights to update needs on this community entity
   * @response `404` `void` No need was found for the need id
   */
  export namespace NeedsUpdateDetail {
    export type RequestParams = {
      needId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = NeedUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name NeedsDeleteDetail
   * @summary Deletes the specified need
   * @request DELETE:/nodes/needs/{needId}
   * @deprecated
   * @response `200` `void` The need was deleted
   * @response `403` `void` The current user does not have rights to delete needs on this community entity
   * @response `404` `void` No need was found for the need id
   */
  export namespace NeedsDeleteDetail {
    export type RequestParams = {
      needId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Node
   * @name EventsCreate
   * @summary Adds a new event for the specified entity.
   * @request POST:/nodes/{id}/events
   * @deprecated
   * @response `200` `string` The event was created
   * @response `403` `void` The current user doesn't have sufficient rights to add events for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace EventsCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = EventUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Node
   * @name EventsDetail
   * @summary Lists events for the specified entity.
   * @request GET:/nodes/{id}/events
   * @deprecated
   * @response `200` `(EventModel)[]` Event data
   * @response `403` `void` The current user doesn't have sufficient rights to view events for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace EventsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /** @format date-time */
      from?: string;
      /** @format date-time */
      to?: string;
      tags?: EventTag[];
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EventModel[];
  }

  /**
   * No description
   * @tags Node
   * @name ChannelsCreate
   * @summary Adds a new channel for the specified entity.
   * @request POST:/nodes/{id}/channels
   * @deprecated
   * @response `200` `string` The channel was created
   * @response `403` `void` The current user doesn't have sufficient rights to add discussion channels for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace ChannelsCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ChannelUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Node
   * @name ChannelsDetail
   * @summary Lists channels for the specified entity.
   * @request GET:/nodes/{id}/channels
   * @deprecated
   * @response `200` `(ChannelModel)[]` channel data
   * @response `403` `void` The current user doesn't have sufficient rights to view channels for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace ChannelsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ChannelModel[];
  }
}

export namespace Organizations {
  /**
   * No description
   * @tags Organization
   * @name OrganizationsCreate
   * @summary Create a new organization. The current user becomes a member of the organization with the Owner role
   * @request POST:/organizations
   * @response `200` `void` Success
   */
  export namespace OrganizationsCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = OrganizationUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name OrganizationsList
   * @summary List all accessible organizations for a given search query. Only organizations accessible to the currently logged in user will be returned
   * @request GET:/organizations
   * @response `200` `CommunityEntityMiniModelListPage`
   */
  export namespace OrganizationsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModelListPage;
  }

  /**
   * No description
   * @tags Organization
   * @name OrganizationsDetailDetail
   * @summary Returns an organization
   * @request GET:/organizations/{id}
   * @response `200` `OrganizationModel` The organization data
   * @response `404` `void` No organization was found for that id
   */
  export namespace OrganizationsDetailDetail {
    export type RequestParams = {
      /** ID of the organization */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = OrganizationModel;
  }

  /**
   * No description
   * @tags Organization
   * @name OrganizationsPartialUpdateDetail
   * @summary Patches the specified organization
   * @request PATCH:/organizations/{id}
   * @response `200` `string` The ID of the entity
   * @response `403` `void` The current user does not have rights to edit this organization
   * @response `404` `void` No organization was found for that id
   */
  export namespace OrganizationsPartialUpdateDetail {
    export type RequestParams = {
      /** ID of the organization */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = OrganizationPatchModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Organization
   * @name OrganizationsUpdateDetail
   * @summary Updates the specified organization
   * @request PUT:/organizations/{id}
   * @response `200` `string` The ID of the entity
   * @response `403` `void` The current user does not have rights to edit this organization
   * @response `404` `void` No organization was found for that id
   */
  export namespace OrganizationsUpdateDetail {
    export type RequestParams = {
      /** ID of the organization */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = OrganizationUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Organization
   * @name OrganizationsDeleteDetail
   * @summary Deletes the specified organization and removes all of the organization's associations from communities and nodes
   * @request DELETE:/organizations/{id}
   * @response `403` `void` The current user does not have rights to delete this organization
   * @response `404` `void` No organization was found for that id
   */
  export namespace OrganizationsDeleteDetail {
    export type RequestParams = {
      /** ID of the organization */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Organization
   * @name DetailDetail
   * @summary Returns an organization including detailed stats
   * @request GET:/organizations/{id}/detail
   * @response `200` `OrganizationDetailModel` The organization data including detailed stats
   * @response `404` `void` No organization was found for that id
   */
  export namespace DetailDetail {
    export type RequestParams = {
      /** ID of the organization */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = OrganizationDetailModel;
  }

  /**
   * No description
   * @tags Organization
   * @name AutocompleteList
   * @summary List the basic information of organizations for a given search query. Only organizations accessible to the currently logged in user will be returned
   * @request GET:/organizations/autocomplete
   * @response `200` `(CommunityEntityMiniModel)[]`
   */
  export namespace AutocompleteList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModel[];
  }

  /**
   * No description
   * @tags Organization
   * @name WorkspacesDetail
   * @request GET:/organizations/{id}/workspaces
   * @response `200` `(WorkspaceModel)[]` Workspace data
   * @response `403` `string` The current user does not have rights to view this organization's content
   * @response `404` `void` No organization was found for that id
   */
  export namespace WorkspacesDetail {
    export type RequestParams = {
      /** ID of the organization */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = WorkspaceModel[];
  }

  /**
   * No description
   * @tags Organization
   * @name NodesDetail
   * @summary Lists all events for the specified node and its ecosystem
   * @request GET:/organizations/{id}/nodes
   * @response `403` `string` The current user does not have rights to view this organization's content
   * @response `404` `void` No organization was found for that id
   */
  export namespace NodesDetail {
    export type RequestParams = {
      /** ID of the organization */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Organization
   * @name EventsAggregateDetail
   * @summary Lists all events for the specified organization and its ecosystem
   * @request GET:/organizations/{id}/events/aggregate
   * @response `200` `EventModelListPage` Event data
   * @response `403` `string` The current user does not have rights to view this organization's content
   * @response `404` `void` No organization was found for that id
   */
  export namespace EventsAggregateDetail {
    export type RequestParams = {
      /** ID of the organization */
      id: string;
    };
    export type RequestQuery = {
      types?: CommunityEntityType[];
      communityEntityIds?: string[];
      tags?: EventTag[];
      /** @format date-time */
      from?: string;
      /** @format date-time */
      to?: string;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EventModelListPage;
  }

  /**
   * No description
   * @tags Organization
   * @name EventsAggregateCommunityEntitiesDetail
   * @summary Lists all community entities for events for the specified org and its ecosystem
   * @request GET:/organizations/{id}/events/aggregate/communityEntities
   * @response `200` `(CommunityEntityMiniModel)[]`
   * @response `403` `void` The current user does not have rights to view this org's content
   * @response `404` `void` No org was found for that id
   */
  export namespace EventsAggregateCommunityEntitiesDetail {
    export type RequestParams = {
      /** ID of the org */
      id: string;
    };
    export type RequestQuery = {
      types?: CommunityEntityType[];
      currentUser?: boolean;
      tags?: EventTag[];
      /** @format date-time */
      from?: string;
      /** @format date-time */
      to?: string;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModel[];
  }

  /**
   * No description
   * @tags Organization
   * @name DocumentsCreate
   * @summary Adds a new document for the specified entity.
   * @request POST:/organizations/{id}/documents
   * @deprecated
   * @response `200` `string` The document was created
   * @response `400` `void` Note-typed documents have to be created and updated through the feed endpoints
   * @response `403` `void` The current user doesn't have sufficient rights to add documents for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace DocumentsCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = DocumentInsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Organization
   * @name DocumentsDetail
   * @summary Lists all documents for the specified entity, not including file data
   * @request GET:/organizations/{id}/documents
   * @deprecated
   * @response `200` `string` The document was created
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace DocumentsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      folderId?: string;
      type?: DocumentFilter;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Organization
   * @name DocumentsDetailDetail
   * @summary Returns a single document, including the file represented as base64
   * @request GET:/organizations/{id}/documents/{documentId}
   * @deprecated
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
   * @response `404` `void` No entity was found for that id or the document does not exist
   */
  export namespace DocumentsDetailDetail {
    export type RequestParams = {
      id: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Organization
   * @name DocumentsUpdateDetail
   * @summary Updates the title and description for the document
   * @request PUT:/organizations/{id}/documents/{documentId}
   * @deprecated
   * @response `200` `void` The document was updated
   * @response `403` `void` The current user doesn't have sufficient rights to edit documents for the entity
   * @response `404` `void` No entity was found for that id or the document does not exist
   */
  export namespace DocumentsUpdateDetail {
    export type RequestParams = {
      id: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = DocumentUpdateModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name DocumentsDeleteDetail
   * @summary Deletes the specified document
   * @request DELETE:/organizations/{id}/documents/{documentId}
   * @deprecated
   * @response `200` `void` The document was deleted
   * @response `403` `void` The current user doesn't have sufficient rights to delete the document
   * @response `404` `void` No entity was found for that id or the document does not exist
   */
  export namespace DocumentsDeleteDetail {
    export type RequestParams = {
      id: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name DocumentsDraftDetail
   * @summary Returns a draft document for the specified container
   * @request GET:/organizations/{id}/documents/draft
   * @deprecated
   * @response `204` `void` No draft document was found for the community entity
   * @response `404` `void` No community entity was found for the specified id
   */
  export namespace DocumentsDraftDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name DocumentsAllDetail
   * @summary Lists all documents and folders for the specified entity, not including file data
   * @request GET:/organizations/{id}/documents/all
   * @deprecated
   * @response `200` `(BaseModel)[]` The document was created
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace DocumentsAllDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = BaseModel[];
  }

  /**
   * No description
   * @tags Organization
   * @name JoinCreate
   * @summary Joins an entity on behalf of the currently logged in user. Only works for community entities with the Open membership access level. Creates a membership record immediately, no invitation is created.
   * @request POST:/organizations/{id}/join
   * @deprecated
   * @response `200` `string` ID of the new membership object
   * @response `403` `void` The entity is not open to members joining
   * @response `404` `void` The entity does not exist
   * @response `409` `void` A membership record already exists for the entity and user
   */
  export namespace JoinCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Organization
   * @name RequestCreate
   * @summary Creates a request to join an entity on behalf of the currently logged in user
   * @request POST:/organizations/{id}/request
   * @deprecated
   * @response `200` `string` ID of the new invitation object
   * @response `403` `void` The entity is not open to members requesting to join
   * @response `404` `void` The entity does not exist
   * @response `409` `void` An invitation already exists for the entity and user
   */
  export namespace RequestCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Organization
   * @name InviteIdCreateDetail
   * @summary Invite a user to an entity using their ID
   * @request POST:/organizations/{id}/invite/id/{userId}
   * @response `200` `string` ID of the new invitation object
   * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity, or the user is inviting an owner without having the rights to manage owners
   * @response `404` `void` The entity or user do not exist
   * @response `409` `void` An invitation already exists for the entity and user
   */
  export namespace InviteIdCreateDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the user to invite */
      userId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = AccessLevel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Organization
   * @name InviteEmailCreateDetail
   * @summary Invite a user to an entity using their email
   * @request POST:/organizations/{id}/invite/email/{userEmail}
   * @response `200` `string` ID of the new invitation object
   * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace InviteEmailCreateDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the user to invite */
      userEmail: string;
    };
    export type RequestQuery = {};
    export type RequestBody = AccessLevel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Organization
   * @name InviteEmailBatchCreate
   * @summary Invite a user to an entity using their email
   * @request POST:/organizations/{id}/invite/email/batch
   * @deprecated
   * @response `200` `string` ID of the new invitation object
   * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace InviteEmailBatchCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = string[];
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Organization
   * @name InvitesDetail
   * @summary List all pending invitations for a given entity
   * @request GET:/organizations/{id}/invites
   * @response `200` `(InvitationModelUser)[]` A list of invitations
   * @response `404` `void` No entity was found for that id
   */
  export namespace InvitesDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = InvitationModelUser[];
  }

  /**
   * No description
   * @tags Organization
   * @name InvitesAcceptCreate
   * @summary Accept the invite on behalf of the currently logged-in user
   * @request POST:/organizations/{id}/invites/{invitationId}/accept
   * @response `200` `void` The invitation has been accepted. The user is now a member of the project.
   * @response `403` `void` The current user does not have the right to accept the invitation
   * @response `404` `void` No entity was found for that id or the invitation does not exist
   */
  export namespace InvitesAcceptCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name InvitesRejectCreate
   * @summary Reject the invite on behalf of the currently logged-in user
   * @request POST:/organizations/{id}/invites/{invitationId}/reject
   * @response `200` `void` The invitation has been rejected
   * @response `403` `void` The current user does not have the right to reject the invitation
   * @response `404` `void` No entity was found for that id or the invitation does not exist
   */
  export namespace InvitesRejectCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name InvitesCancelCreate
   * @summary Cancels an invite
   * @request POST:/organizations/{id}/invites/{invitationId}/cancel
   * @response `200` `void` The invitation has been cancelled
   * @response `403` `void` The current user does not have the right to cancel the invitation
   * @response `404` `void` No entity was found for that id or the invitation does not exist
   */
  export namespace InvitesCancelCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name InvitesResendCreate
   * @summary Resends an invite
   * @request POST:/organizations/invites/{invitationId}/resend
   * @response `200` `void` The invitation has been resent
   * @response `400` `void` The invitation is a request and cannot be resent
   * @response `403` `void` The user does not have the right to resend the invitation
   * @response `404` `void` No entity was found for that id or the invitation does not exist
   */
  export namespace InvitesResendCreate {
    export type RequestParams = {
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name LeaveCreate
   * @summary Leaves an entity on behalf of the currently logged in user
   * @request POST:/organizations/{id}/leave
   * @deprecated
   * @response `200` `void` The user has successfully left the entity
   * @response `404` `void` The entity does not exist or the user is not a member
   */
  export namespace LeaveCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name OnboardingResponsesDetailDetail
   * @summary List onboarding responses for a community entity and a user
   * @request GET:/organizations/{id}/onboardingResponses/{userId}
   * @deprecated
   * @response `200` `OnboardingQuestionnaireInstanceModel` onboarding questionnaire responses
   * @response `404` `void` No entity was found for that id or no user was found
   */
  export namespace OnboardingResponsesDetailDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      userId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = OnboardingQuestionnaireInstanceModel;
  }

  /**
   * No description
   * @tags Organization
   * @name OnboardingResponsesCreate
   * @summary Posts onboarding responses for a community entity for the current user
   * @request POST:/organizations/{id}/onboardingResponses
   * @deprecated
   * @response `200` `void` The onboarding questionnaire responses were saved successfully
   * @response `404` `void` No entity was found for that id
   */
  export namespace OnboardingResponsesCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = OnboardingQuestionnaireInstanceUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name OnboardingCompletionCreate
   * @summary Records the completion of the onboarding workflow for a community entity for the current user
   * @request POST:/organizations/{id}/onboardingCompletion
   * @deprecated
   * @response `200` `void` The onboarding completion was recorded successfully
   * @response `404` `void` No entity was found for that id
   */
  export namespace OnboardingCompletionCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name MembersDetail
   * @summary List all members for an entity
   * @request GET:/organizations/{id}/members
   * @response `200` `(MemberModel)[]` A list of members
   * @response `403` `void` The current user doesn't have sufficient rights to see the entity's members
   * @response `404` `void` No entity was found for that id
   */
  export namespace MembersDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = MemberModel[];
  }

  /**
   * No description
   * @tags Organization
   * @name InvitationDetail
   * @summary Returns the pending invitation for an object for the current user
   * @request GET:/organizations/{id}/invitation
   * @response `200` `InvitationModelEntity` A pending invitation
   * @response `404` `void` No invitation is pending or no entity was found for that id
   */
  export namespace InvitationDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = InvitationModelEntity;
  }

  /**
   * No description
   * @tags Organization
   * @name MembersUpdateDetail
   * @summary Updates a member's access level for an entity
   * @request PUT:/organizations/{id}/members/{memberId}
   * @response `200` `void` The access level has been updated
   * @response `403` `void` The user does not have the right to set this access level for this member
   * @response `404` `void` No entity was found for that id or the user is not a member of that entity
   */
  export namespace MembersUpdateDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the entity */
      memberId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = AccessLevel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name MembersDeleteDetail
   * @summary Removes a member's access from an entity
   * @request DELETE:/organizations/{id}/members/{memberId}
   * @response `200` `void` The member has been removed
   * @response `403` `void` The current user does not have the right to remove members from this entity
   * @response `404` `void` No entity was found for that id or the user is not a member of that entity
   */
  export namespace MembersDeleteDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the entity */
      memberId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name MembersContributionUpdate
   * @summary Updates a member's contribution
   * @request PUT:/organizations/{id}/members/{memberId}/contribution
   * @response `200` `void` The contribution has been updated
   * @response `403` `void` The user does not have the right to set the contribution for other members
   * @response `404` `void` No entity was found for that id or the user is not a member of that entity
   */
  export namespace MembersContributionUpdate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the user */
      memberId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = string;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name MembersLabelsUpdate
   * @summary Updates a member's labels
   * @request PUT:/organizations/{id}/members/{memberId}/labels
   * @response `200` `void` The labels have been updated
   * @response `403` `void` The current user does not have the right to set labels for members
   * @response `404` `void` No entity was found for that id or the user is not a member of that entity
   */
  export namespace MembersLabelsUpdate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the user */
      memberId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = string[];
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name CommunityEntityInviteCreateDetail
   * @summary Invite a community entity to become affiliated to another entity
   * @request POST:/organizations/{id}/communityEntityInvite/{targetEntityId}
   * @response `200` `string` ID of the new invitation object
   * @response `400` `void` An invitation cannot be extended to a community entity of the same type
   * @response `403` `void` The current user does not have the rights to invite entities to the specified entity
   * @response `404` `void` One of the entities does not exist
   * @response `409` `void` An invitation or affiliation already exists for the entities
   */
  export namespace CommunityEntityInviteCreateDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the target entity */
      targetEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Organization
   * @name CommunityEntityInvitesIncomingDetail
   * @summary List all pending invitations for a given entity
   * @request GET:/organizations/{id}/communityEntityInvites/incoming
   * @response `200` `(CommunityEntityInvitationModelSource)[]` A list of invitations
   * @response `404` `void` No entity was found for that id
   */
  export namespace CommunityEntityInvitesIncomingDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityInvitationModelSource[];
  }

  /**
   * No description
   * @tags Organization
   * @name CommunityEntityInvitesOutgoingDetail
   * @summary List all pending invitations for a given entity
   * @request GET:/organizations/{id}/communityEntityInvites/outgoing
   * @response `200` `(CommunityEntityInvitationModelSource)[]` A list of invitations
   * @response `404` `void` No entity was found for that id
   */
  export namespace CommunityEntityInvitesOutgoingDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityInvitationModelSource[];
  }

  /**
   * No description
   * @tags Organization
   * @name CommunityEntityInvitesAcceptCreate
   * @summary Accept an invite
   * @request POST:/organizations/{id}/communityEntityInvites/{invitationId}/accept
   * @response `200` `void` The invitation has been accepted
   * @response `403` `void` The current user does not have the right to accept the invitation
   * @response `404` `void` One of the entities does not exist or the invitation does not exist
   */
  export namespace CommunityEntityInvitesAcceptCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name CommunityEntityInvitesRejectCreate
   * @summary Rejects an invite
   * @request POST:/organizations/{id}/communityEntityInvites/{invitationId}/reject
   * @response `200` `void` The invitation has been rejected
   * @response `403` `void` The current user does not have the right to reject the invitation
   * @response `404` `void` One of the entities does not exist or the invitation does not exist
   */
  export namespace CommunityEntityInvitesRejectCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name CommunityEntityInvitesCancelCreate
   * @summary Cancels an invite
   * @request POST:/organizations/{id}/communityEntityInvites/{invitationId}/cancel
   * @response `200` `void` The invitation has been cancelled
   * @response `403` `void` The current user does not have the right to cancel the invitation
   * @response `404` `void` One of the entities does not exist or the invitation does not exist
   */
  export namespace CommunityEntityInvitesCancelCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name CommunityEntityRemoveCreate
   * @summary Removes an affiliation of an entity to another entity
   * @request POST:/organizations/{id}/communityEntity/{targetEntityId}/remove
   * @response `200` `void` The entity affiliation has been removed successfully
   * @response `403` `void` The current user does not have the permissions to manage the community entity's affiliations
   * @response `404` `void` One of the entities does not exist or the entities are not affiliated
   */
  export namespace CommunityEntityRemoveCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the target entity */
      targetEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name CommunityEntityLinkCreate
   * @summary Creates an affiliation of an entity to another entity. On the entity, the user must be admin or owner, or the toggle for members to allow creating community entities of the respective type must be on. The user must be admin or owner on the target entity.
   * @request POST:/organizations/{id}/communityEntity/{targetEntityId}/link
   * @response `200` `void` The entity affiliation has been created successfully
   * @response `409` `void` One of the entities does not exist or the entities are already affiliated
   */
  export namespace CommunityEntityLinkCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the target entity */
      targetEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name EcosystemCommunityEntitiesDetail
   * @summary Lists all ecosystem containers (projects, communities and nodes) for the given community entity
   * @request GET:/organizations/{id}/ecosystem/communityEntities
   * @deprecated
   * @response `200` `(EntityMiniModel)[]`
   * @response `403` `string` The current user does not have rights to view this community entity's contents
   * @response `404` `void` No community entity was found for that id
   */
  export namespace EcosystemCommunityEntitiesDetail {
    export type RequestParams = {
      /** ID of the community entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EntityMiniModel[];
  }

  /**
   * No description
   * @tags Organization
   * @name EcosystemUsersDetail
   * @summary Lists all ecosystem members for the given community entity
   * @request GET:/organizations/{id}/ecosystem/users
   * @response `200` `(EntityMiniModel)[]`
   * @response `403` `string` The current user does not have rights to view this community entity's contents
   * @response `404` `void` No community entity was found for that id
   */
  export namespace EcosystemUsersDetail {
    export type RequestParams = {
      /** ID of the community entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EntityMiniModel[];
  }

  /**
   * No description
   * @tags Organization
   * @name NeedsCreate
   * @summary Adds a new need for the specified project
   * @request POST:/organizations/{id}/needs
   * @deprecated
   * @response `200` `string` The ID of the new need
   * @response `403` `void` The current user doesn't have sufficient rights to add needs for the entity
   */
  export namespace NeedsCreate {
    export type RequestParams = {
      /** ID of the project */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = NeedUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Organization
   * @name NeedsDetail
   * @summary Lists all needs for the specified community entity
   * @request GET:/organizations/{id}/needs
   * @deprecated
   * @response `200` `(NeedModel)[]`
   * @response `403` `string` The current user does not have rights to view this community entity's contents
   * @response `404` `void` No community entity was found for that id
   */
  export namespace NeedsDetail {
    export type RequestParams = {
      /** ID of the community entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = NeedModel[];
  }

  /**
   * No description
   * @tags Organization
   * @name NeedsDetailDetail
   * @summary Returns a single need
   * @request GET:/organizations/needs/{needId}
   * @deprecated
   * @response `403` `string` The current user does not have rights to view this community entity's contents
   * @response `404` `void` No need was found for the need id
   */
  export namespace NeedsDetailDetail {
    export type RequestParams = {
      needId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Organization
   * @name NeedsUpdateDetail
   * @summary Updates the need
   * @request PUT:/organizations/needs/{needId}
   * @deprecated
   * @response `200` `void` The need was updated
   * @response `403` `void` The current user does not have rights to update needs on this community entity
   * @response `404` `void` No need was found for the need id
   */
  export namespace NeedsUpdateDetail {
    export type RequestParams = {
      needId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = NeedUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name NeedsDeleteDetail
   * @summary Deletes the specified need
   * @request DELETE:/organizations/needs/{needId}
   * @deprecated
   * @response `200` `void` The need was deleted
   * @response `403` `void` The current user does not have rights to delete needs on this community entity
   * @response `404` `void` No need was found for the need id
   */
  export namespace NeedsDeleteDetail {
    export type RequestParams = {
      needId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Organization
   * @name EventsCreate
   * @summary Adds a new event for the specified entity.
   * @request POST:/organizations/{id}/events
   * @deprecated
   * @response `200` `string` The event was created
   * @response `403` `void` The current user doesn't have sufficient rights to add events for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace EventsCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = EventUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Organization
   * @name EventsDetail
   * @summary Lists events for the specified entity.
   * @request GET:/organizations/{id}/events
   * @deprecated
   * @response `200` `(EventModel)[]` Event data
   * @response `403` `void` The current user doesn't have sufficient rights to view events for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace EventsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /** @format date-time */
      from?: string;
      /** @format date-time */
      to?: string;
      tags?: EventTag[];
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EventModel[];
  }

  /**
   * No description
   * @tags Organization
   * @name ChannelsCreate
   * @summary Adds a new channel for the specified entity.
   * @request POST:/organizations/{id}/channels
   * @deprecated
   * @response `200` `string` The channel was created
   * @response `403` `void` The current user doesn't have sufficient rights to add discussion channels for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace ChannelsCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ChannelUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Organization
   * @name ChannelsDetail
   * @summary Lists channels for the specified entity.
   * @request GET:/organizations/{id}/channels
   * @deprecated
   * @response `200` `(ChannelModel)[]` channel data
   * @response `403` `void` The current user doesn't have sufficient rights to view channels for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace ChannelsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ChannelModel[];
  }
}

export namespace Papers {
  /**
   * No description
   * @tags Paper
   * @name PapersCreate
   * @summary Adds a new paper
   * @request POST:/papers/{entityId}/papers
   * @response `200` `string` The ID of the new paper
   * @response `403` `void` The current user doesn't have sufficient rights to add papers for the entity
   * @response `404` `void` The entity could not be found
   */
  export namespace PapersCreate {
    export type RequestParams = {
      /** ID of the entity */
      entityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = PaperUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Paper
   * @name PapersDetail
   * @summary Lists all papers for the specified entity
   * @request GET:/papers/{entityId}/papers
   * @response `200` `(PaperModel)[]` The paper data
   * @response `403` `void` The current user doesn't have sufficient rights to list papers for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace PapersDetail {
    export type RequestParams = {
      /** ID of the entity */
      entityId: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PaperModel[];
  }

  /**
   * No description
   * @tags Paper
   * @name PapersNewDetail
   * @summary Returns a value indicating whether or not there are new papers for a particular entity
   * @request GET:/papers/{entityId}/papers/new
   * @response `200` `boolean` True or false
   */
  export namespace PapersNewDetail {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = boolean;
  }

  /**
   * No description
   * @tags Paper
   * @name PapersDetailDetail
   * @summary Returns a single paper
   * @request GET:/papers/{id}
   * @response `200` `PaperModel` The paper data
   * @response `403` `void` The current user doesn't have sufficient rights to view the paper
   * @response `404` `void` No paper was found for that id
   */
  export namespace PapersDetailDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PaperModel;
  }

  /**
   * No description
   * @tags Paper
   * @name PapersUpdateDetail
   * @summary Updates the paper
   * @request PUT:/papers/{id}
   * @response `200` `void` The paper was updated
   * @response `403` `void` The current user doesn't have sufficient rights to edit the paper
   * @response `404` `void` No paper was found for that id
   */
  export namespace PapersUpdateDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = PaperUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Paper
   * @name PapersDeleteDetail
   * @summary Deletes the specified paper
   * @request DELETE:/papers/{id}
   * @response `200` `void` The paper was deleted
   * @response `403` `void` The current user doesn't have sufficient rights to delete the paper
   * @response `404` `void` No paper was found for that id
   */
  export namespace PapersDeleteDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Paper
   * @name OpenalexAuthorsList
   * @summary Returns a list of scientific authors matching a search term
   * @request GET:/papers/openalex/authors
   * @response `200` `(AuthorModel)[]` The author data
   */
  export namespace OpenalexAuthorsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = AuthorModel[];
  }

  /**
   * No description
   * @tags Paper
   * @name OpenalexWorksByAuthorIdsList
   * @summary Returns a list of scientific papers by a specific author or authors
   * @request GET:/papers/openalex/works/byAuthorIds
   * @response `200` `(WorkModel)[]` The paper data
   */
  export namespace OpenalexWorksByAuthorIdsList {
    export type RequestParams = {};
    export type RequestQuery = {
      authorIds?: string;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = WorkModel[];
  }

  /**
   * No description
   * @tags Paper
   * @name OpenalexWorksByAuthorNameList
   * @summary Returns a list of scientific papers for an author name
   * @request GET:/papers/openalex/works/byAuthorName
   * @response `200` `(WorkModel)[]` The paper data
   */
  export namespace OpenalexWorksByAuthorNameList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = WorkModel[];
  }
}

export namespace Proposals {
  /**
   * No description
   * @tags Proposal
   * @name ProposalsCreate
   * @summary Creates a new proposal on behalf of a project
   * @request POST:/proposals
   * @response `200` `string` The ID of the new proposal
   * @response `403` `void` The call for proposal is not yet open for submissions or the current user does not have permissions
   * @response `404` `void` Either the call for proposal or the project was not found
   * @response `409` `string` A proposal already exists for this project and call for proposals
   */
  export namespace ProposalsCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ProposalUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Proposal
   * @name ProposalsDetailDetail
   * @summary Gets a proposal
   * @request GET:/proposals/{proposalId}
   * @response `200` `ProposalModel`
   * @response `403` `void` The current user is not an author on the proposal
   * @response `404` `void` No proposal was found for the id
   */
  export namespace ProposalsDetailDetail {
    export type RequestParams = {
      /** ID of the proposal */
      proposalId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ProposalModel;
  }

  /**
   * No description
   * @tags Proposal
   * @name ProposalsPartialUpdateDetail
   * @summary Patches a proposal
   * @request PATCH:/proposals/{proposalId}
   * @response `200` `ProposalModel`
   * @response `403` `void` The current user is not an author on the proposal
   * @response `404` `void` No proposal was found for the id
   */
  export namespace ProposalsPartialUpdateDetail {
    export type RequestParams = {
      proposalId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ProposalPatchModel;
    export type RequestHeaders = {};
    export type ResponseBody = ProposalModel;
  }

  /**
   * No description
   * @tags Proposal
   * @name ProposalsDeleteDetail
   * @summary Deletes the specified proposal
   * @request DELETE:/proposals/{proposalId}
   * @response `200` `void` The proposal was deleted
   * @response `403` `void` The proposal wasn't created by the current user
   * @response `404` `void` No proposal was found for the id
   */
  export namespace ProposalsDeleteDetail {
    export type RequestParams = {
      proposalId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Proposal
   * @name StatusCreate
   * @summary Updates the status of a proposal
   * @request POST:/proposals/{proposalId}/status
   * @response `400` `void` This status cannot be set
   * @response `403` `void` The current user does not have the rights to set this status
   * @response `404` `void` No proposal was found for the id
   * @response `409` `void` The proposal has already been submitted
   */
  export namespace StatusCreate {
    export type RequestParams = {
      proposalId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ProposalStatus;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Proposal
   * @name ScoreCreate
   * @summary Scores a proposal
   * @request POST:/proposals/{proposalId}/score
   * @response `200` `ProposalModel`
   * @response `403` `void` The current user is not a reviewer on the cfp
   * @response `404` `void` No proposal was found for the id
   */
  export namespace ScoreCreate {
    export type RequestParams = {
      proposalId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = number;
    export type RequestHeaders = {};
    export type ResponseBody = ProposalModel;
  }

  /**
   * No description
   * @tags Proposal
   * @name DocumentsCreate
   * @summary Adds a new document for the specified proposal.
   * @request POST:/proposals/{proposalId}/documents
   * @response `200` `string` The document was created
   * @response `403` `void` The current user doesn't have sufficient rights to add documents for the proposal
   * @response `404` `void` No proposal was found for that id
   */
  export namespace DocumentsCreate {
    export type RequestParams = {
      proposalId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = DocumentInsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Proposal
   * @name DocumentsDetailDetail
   * @summary Returns a single document, including the file represented as base64
   * @request GET:/proposals/{proposalId}/documents/{documentId}
   * @deprecated
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the proposal
   * @response `404` `void` No proposal was found for that id, the document id is incorrect or the document does not exist
   */
  export namespace DocumentsDetailDetail {
    export type RequestParams = {
      proposalId: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }
}

export namespace Public {
  /**
   * No description
   * @tags Public
   * @name JoinCreate
   * @request POST:/public/join
   * @response `200` `void` User successful added to the waitlist
   */
  export namespace JoinCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = WaitlistRecordModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Public
   * @name ContactCreate
   * @request POST:/public/contact
   * @response `200` `void` User contact successful
   */
  export namespace ContactCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = UserContactModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
}

export namespace Resources {
  /**
   * No description
   * @tags Resource
   * @name ResourcesCreate
   * @summary Adds a new resource for the specified feed.
   * @request POST:/resources/{entityId}/resources
   * @response `200` `string` The resource was created
   * @response `403` `void` The current user doesn't have sufficient rights to add resource for the entity
   */
  export namespace ResourcesCreate {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ResourceUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Resource
   * @name ResourcesDetail
   * @summary Lists all resources for the specified entity
   * @request GET:/resources/{entityId}/resources
   * @response `200` `(ResourceModel)[]` The resource data
   * @response `403` `void` The current user doesn't have sufficient rights to view resources for the entity
   */
  export namespace ResourcesDetail {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ResourceModel[];
  }

  /**
   * No description
   * @tags Resource
   * @name ResourcesRepoCreate
   * @summary Adds a new repository resource for the specified feed.
   * @request POST:/resources/{entityId}/resources/repo
   * @response `200` `string` The resource was created
   * @response `403` `void` The current user doesn't have sufficient rights to add resource for the entity
   * @response `422` `void` The url could not be resolved to a valid repository
   */
  export namespace ResourcesRepoCreate {
    export type RequestParams = {
      entityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ResourceRepositoryUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Resource
   * @name ResourcesList
   * @summary Lists all resources
   * @request GET:/resources
   * @response `200` `ResourceModelListPage` A list of resources matching the search query visible to the current user
   */
  export namespace ResourcesList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ResourceModelListPage;
  }

  /**
   * No description
   * @tags Resource
   * @name ResourcesDetailDetail
   * @summary Returns a single resource
   * @request GET:/resources/{id}
   * @response `200` `ResourceModel`
   * @response `403` `void` The current user doesn't have sufficient rights to see the resource
   * @response `404` `void` No resource was found for that id
   */
  export namespace ResourcesDetailDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ResourceModel;
  }

  /**
   * No description
   * @tags Resource
   * @name ResourcesUpdateDetail
   * @summary Updates the resource
   * @request PUT:/resources/{id}
   * @response `200` `void` The resource was updated
   * @response `403` `void` The current user doesn't have sufficient rights to edit the resource
   * @response `404` `void` No resource was found for that id
   */
  export namespace ResourcesUpdateDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ResourceUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Resource
   * @name ResourcesDeleteDetail
   * @summary Deletes a resource
   * @request DELETE:/resources/{id}
   * @response `200` `void` The resource was deleted
   * @response `403` `void` The current user doesn't have sufficient rights to delete this resource
   * @response `404` `void` No resource was found for that id
   */
  export namespace ResourcesDeleteDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
}

export namespace Users {
  /**
   * No description
   * @tags User
   * @name UsersCreate
   * @summary Create a new user
   * @request POST:/users
   * @response `200` `string` ID of the new user record
   * @response `400` `void` The supplied verification code is invalid
   * @response `403` `void` The captcha code is invalid
   * @response `409` `ErrorModel` A user with this email or nickname already exists
   */
  export namespace UsersCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = UserCreateModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags User
   * @name UsersList
   * @summary List all users for a given query
   * @request GET:/users
   * @response `200` `UserMiniModelListPage` A list of user models
   */
  export namespace UsersList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = UserMiniModelListPage;
  }

  /**
   * No description
   * @tags User
   * @name UsersDelete
   * @summary Deletes the current user
   * @request DELETE:/users
   * @response `200` `void` The user was deleted successfully
   * @response `400` `void` The user cannot be deleted
   * @response `409` `void` The user is already archived
   */
  export namespace UsersDelete {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags User
   * @name WalletCreateDetail
   * @summary Register a user with a wallet signature
   * @request POST:/users/wallet/{walletType}
   * @response `200` `string` Registration successful
   * @response `401` `void` Signature invalid
   * @response `409` `ErrorModel` A user with this wallet, email or nickname already exists
   */
  export namespace WalletCreateDetail {
    export type RequestParams = {
      walletType: WalletType;
    };
    export type RequestQuery = {};
    export type RequestBody = UserCreateWalletModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags User
   * @name CheckCreate
   * @summary Verifies whether an email address is eligible to join JOGL
   * @request POST:/users/check
   * @deprecated
   * @response `200` `void` The email address can be used to sign up with a new user
   * @response `409` `void` A user with this email already exists
   */
  export namespace CheckCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = EmailModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags User
   * @name UsersDetailDetail
   * @request GET:/users/{id}
   * @response `200` `UserModel` The user data
   * @response `404` `void` No user was found for that id
   */
  export namespace UsersDetailDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = UserModel;
  }

  /**
   * No description
   * @tags User
   * @name UsersPartialUpdateDetail
   * @summary Patches the specified user.
   * @request PATCH:/users/{id}
   * @response `403` `void` The current user can only edit its own user record
   * @response `404` `void` No user was found for that id
   * @response `409` `void` The username or email is already taken
   */
  export namespace UsersPartialUpdateDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = UserPatchModel;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags User
   * @name UsersUpdateDetail
   * @summary Updates the specified user record
   * @request PUT:/users/{id}
   * @response `403` `void` The current user can only edit its own user record
   * @response `404` `void` No user was found for that id
   * @response `409` `void` The username or email is already taken
   */
  export namespace UsersUpdateDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = UserUpdateModel;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags User
   * @name PortfolioDetail
   * @request GET:/users/{id}/portfolio
   * @response `200` `(PortfolioItemModel)[]` Portfolio data
   * @response `404` `void` No user was found for that id
   */
  export namespace PortfolioDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PortfolioItemModel[];
  }

  /**
   * No description
   * @tags User
   * @name NeedsDetail
   * @request GET:/users/{id}/needs
   * @response `404` `void` No user was found for that id
   */
  export namespace NeedsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags User
   * @name EcosystemDetail
   * @request GET:/users/{id}/ecosystem
   * @deprecated
   * @response `404` `void` No user was found for that id
   */
  export namespace EcosystemDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags User
   * @name WorkspacesDetail
   * @request GET:/users/{id}/workspaces
   * @response `200` `(CommunityEntityMiniModel)[]` The user's workspaces
   * @response `404` `void` No user was found for that id
   */
  export namespace WorkspacesDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      permission?: Permission;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModel[];
  }

  /**
   * No description
   * @tags User
   * @name NodesDetail
   * @request GET:/users/{id}/nodes
   * @response `200` `(CommunityEntityMiniModel)[]` The user's nodes
   * @response `404` `void` No user was found for that id
   */
  export namespace NodesDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      permission?: Permission;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModel[];
  }

  /**
   * No description
   * @tags User
   * @name OrganizationsDetail
   * @request GET:/users/{id}/organizations
   * @response `404` `void` No user was found for that id
   */
  export namespace OrganizationsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      permission?: Permission;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags User
   * @name ArchiveCreate
   * @summary Archives the current user
   * @request POST:/users/archive
   * @response `200` `void` The user was archived successfully
   * @response `400` `void` The user cannot be archived
   * @response `409` `void` The user is already archived
   */
  export namespace ArchiveCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags User
   * @name UnarchiveCreate
   * @summary Unarchives the current user
   * @request POST:/users/unarchive
   * @response `200` `void` The user was unarchived successfully
   * @response `400` `void` The user isn't archived
   */
  export namespace UnarchiveCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags User
   * @name ExistsUsernameDetailDetail
   * @summary Checks whether a username is already taken or not
   * @request GET:/users/exists/username/{username}
   * @response `200` `void` Username is available
   * @response `409` `void` Username is already taken
   */
  export namespace ExistsUsernameDetailDetail {
    export type RequestParams = {
      username: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags User
   * @name ExistsWalletDetailDetail
   * @summary Checks whether a wallet address exists on JOGL or not
   * @request GET:/users/exists/wallet/{wallet}
   * @response `200` `boolean` Whether or not the specified wallet exits
   */
  export namespace ExistsWalletDetailDetail {
    export type RequestParams = {
      wallet: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = boolean;
  }

  /**
   * No description
   * @tags User
   * @name InvitesDetail
   * @summary List all pending community entity invitations for a given user
   * @request GET:/users/{id}/invites
   * @deprecated
   * @response `200` `(InvitationModelEntity)[]` A list of community entity invitations
   */
  export namespace InvitesDetail {
    export type RequestParams = {
      /** ID of the user */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = InvitationModelEntity[];
  }

  /**
   * No description
   * @tags User
   * @name InvitesCommunityEntityList
   * @summary List all pending community entity invitations for the current user
   * @request GET:/users/invites/communityEntity
   * @response `200` `(InvitationModelEntity)[]` A list of community entity invitations
   */
  export namespace InvitesCommunityEntityList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = InvitationModelEntity[];
  }

  /**
   * No description
   * @tags User
   * @name InvitesEventList
   * @summary List all pending event invitations for the current user
   * @request GET:/users/invites/event
   * @response `200` `(EventAttendanceModel)[]` A list of event invitations
   */
  export namespace InvitesEventList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EventAttendanceModel[];
  }

  /**
   * No description
   * @tags User
   * @name ContactCreate
   * @request POST:/users/{id}/contact
   * @response `403` `void` The user has not enabled the Contact Me function
   * @response `404` `void` No user was found for that id
   */
  export namespace ContactCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ContactModel;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags User
   * @name FollowCreate
   * @request POST:/users/{id}/follow
   * @response `400` `void` The user cannot follow themselves
   * @response `404` `void` No user was found for that id
   * @response `409` `void` The current user is already following this user
   */
  export namespace FollowCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags User
   * @name FollowDelete
   * @request DELETE:/users/{id}/follow
   * @response `404` `void` No user was found for that id or the current user isn't following this user
   */
  export namespace FollowDelete {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags User
   * @name FollowedDetail
   * @request GET:/users/{id}/followed
   * @response `404` `void` No user was found for that id
   */
  export namespace FollowedDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags User
   * @name FollowersDetail
   * @request GET:/users/{id}/followers
   * @response `404` `void` No user was found for that id
   */
  export namespace FollowersDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags User
   * @name SkillsCreate
   * @request POST:/users/skills
   * @response `200` `void` Success
   */
  export namespace SkillsCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = TextValueModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags User
   * @name SkillsList
   * @request GET:/users/skills
   * @response `200` `void` Success
   */
  export namespace SkillsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags User
   * @name InterestsCreate
   * @request POST:/users/interests
   * @response `200` `void` Success
   */
  export namespace InterestsCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = TextValueModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags User
   * @name InterestsList
   * @request GET:/users/interests
   * @response `200` `(TextValueModel)[]` The interest data
   */
  export namespace InterestsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = TextValueModel[];
  }

  /**
   * No description
   * @tags User
   * @name PapersCreate
   * @summary Adds a new paper
   * @request POST:/users/papers
   * @deprecated
   * @response `200` `string` The ID of the new paper
   * @response `400` `void` Note-typed papers have to be created through the feed controller
   */
  export namespace PapersCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = PaperUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags User
   * @name OrcidCreate
   * @summary Loads the user's unique ORCID id from ORCID and stores it in the database
   * @request POST:/users/orcid
   * @response `404` `void` No ORCID record found or user not found
   */
  export namespace OrcidCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = OrcidLoadModel;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags User
   * @name OrcidDelete
   * @summary Removes the user's ORCID id and all orcid-loaded papers from the database
   * @request DELETE:/users/orcid
   * @response `404` `void` No ORCID record found or user not found
   */
  export namespace OrcidDelete {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags User
   * @name GoogleDelete
   * @summary Removes the Google account link
   * @request DELETE:/users/google
   * @response `404` `void` No Google record found or user not found
   */
  export namespace GoogleDelete {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags User
   * @name PapersOrcidList
   * @summary Gets papers for the current user's ORCID
   * @request GET:/users/papers/orcid
   * @response `200` `(PaperModelOrcid)[]` The papers from ORCID
   * @response `404` `void` No ORCID set on the current user
   */
  export namespace PapersOrcidList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PaperModelOrcid[];
  }

  /**
   * No description
   * @tags User
   * @name PapersS2PaperList
   * @summary Gets papers from Semantic Scholar - search papers
   * @request GET:/users/papers/s2/paper
   * @response `200` `PaperModelS2ListPage` The papers from S2
   */
  export namespace PapersS2PaperList {
    export type RequestParams = {};
    export type RequestQuery = {
      /** The search query to filter projects by */
      Search: string;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 100
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 100
       * @format int32
       * @min 1
       * @max 100
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PaperModelS2ListPage;
  }

  /**
   * No description
   * @tags User
   * @name PapersOaList
   * @summary Gets works from OpenAlex - search works
   * @request GET:/users/papers/oa
   * @response `200` `PaperModelOAListPage` The works from OpenAlex
   */
  export namespace PapersOaList {
    export type RequestParams = {};
    export type RequestQuery = {
      /** The search query to filter projects by */
      Search: string;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 100
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 100
       * @format int32
       * @min 1
       * @max 100
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PaperModelOAListPage;
  }

  /**
   * No description
   * @tags User
   * @name PapersPmList
   * @summary Gets articles from PubMed - search articles
   * @request GET:/users/papers/pm
   * @response `200` `PaperModelPMListPage` The articles from PubMed
   */
  export namespace PapersPmList {
    export type RequestParams = {};
    export type RequestQuery = {
      /** The search query to filter projects by */
      Search: string;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 100
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 100
       * @format int32
       * @min 1
       * @max 100
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PaperModelPMListPage;
  }

  /**
   * No description
   * @tags User
   * @name PapersDoiList
   * @summary Gets paper for DOI
   * @request GET:/users/papers/doi
   * @response `200` `PaperModelOrcid` The paper
   * @response `404` `void` No paper found
   */
  export namespace PapersDoiList {
    export type RequestParams = {};
    export type RequestQuery = {
      id?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PaperModelOrcid;
  }

  /**
   * No description
   * @tags User
   * @name ProposalsList
   * @summary Lists all proposals for current user
   * @request GET:/users/proposals
   * @response `200` `(ProposalModel)[]`
   */
  export namespace ProposalsList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ProposalModel[];
  }

  /**
   * No description
   * @tags User
   * @name NotificationsLastReadList
   * @summary Returns the date time and when the user last read their notifications
   * @request GET:/users/notifications/lastRead
   * @response `200` `void` Success
   */
  export namespace NotificationsLastReadList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags User
   * @name NotificationsLastReadCreate
   * @summary Records the date time and when the user last read their notifications
   * @request POST:/users/notifications/lastRead
   * @response `200` `void` Success
   */
  export namespace NotificationsLastReadCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags User
   * @name NotificationsList
   * @summary Lists all notifications for current user
   * @request GET:/users/notifications
   * @deprecated
   * @response `200` `(NotificationModel)[]`
   */
  export namespace NotificationsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = NotificationModel[];
  }

  /**
   * No description
   * @tags User
   * @name CurrentNotificationsList
   * @summary Lists all notifications for current user
   * @request GET:/users/current/notifications
   * @response `200` `NotificationModelListPage`
   */
  export namespace CurrentNotificationsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = NotificationModelListPage;
  }

  /**
   * No description
   * @tags User
   * @name CurrentNotificationsPendingList
   * @summary Returns a boolean value indicating whether or not the current user has any pending (un-actioned) notifications
   * @request GET:/users/current/notifications/pending
   * @response `200` `boolean` A boolean flag
   */
  export namespace CurrentNotificationsPendingList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = boolean;
  }

  /**
   * No description
   * @tags User
   * @name NotificationsActionedCreate
   * @summary Records the date time and when the user last read their notifications
   * @request POST:/users/notifications/{id}/actioned
   * @response `403` `void` The notification belongs to a different user
   * @response `404` `void` The notification does not exist
   */
  export namespace NotificationsActionedCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags User
   * @name EventsDetail
   * @request GET:/users/{id}/events
   * @response `404` `void` No user was found for that id
   */
  export namespace EventsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
      /** @format date-time */
      from?: string;
      /** @format date-time */
      to?: string;
      tags?: EventTag[];
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags User
   * @name OrcidUserdataList
   * @summary Gets data for the current user's ORCID
   * @request GET:/users/orcid/userdata
   * @response `200` `OrcidExperienceModel` The data from ORCID
   * @response `404` `void` No ORCID set on the current user
   */
  export namespace OrcidUserdataList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = OrcidExperienceModel;
  }

  /**
   * No description
   * @tags User
   * @name GithubReposList
   * @summary Gets GitHub repositories for the current user
   * @request GET:/users/github/repos
   * @response `200` `(RepositoryModel)[]` The repository data
   */
  export namespace GithubReposList {
    export type RequestParams = {};
    export type RequestQuery = {
      accessToken?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = RepositoryModel[];
  }

  /**
   * No description
   * @tags User
   * @name HuggingfaceReposList
   * @summary Gets Hugging Face repositories for the current user
   * @request GET:/users/huggingface/repos
   * @response `200` `(RepositoryModel)[]` The repository data
   */
  export namespace HuggingfaceReposList {
    export type RequestParams = {};
    export type RequestQuery = {
      accessToken?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = RepositoryModel[];
  }

  /**
   * No description
   * @tags User
   * @name LinkedinProfileList
   * @summary Gets LinkedIn data for the current user
   * @request GET:/users/linkedin/profile
   * @response `200` `ProfileModel` The LinkedIn data
   */
  export namespace LinkedinProfileList {
    export type RequestParams = {};
    export type RequestQuery = {
      linkedInUrl?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ProfileModel;
  }

  /**
   * No description
   * @tags User
   * @name PushTokenCreate
   * @summary Upserts a push notification token for the current user
   * @request POST:/users/push/token
   * @response `200` `void` Push notification token stored
   */
  export namespace PushTokenCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = string;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags User
   * @name AutocompleteList
   * @summary Autocomplete search for all users
   * @request GET:/users/autocomplete
   * @response `200` `(UserMiniModel)[]` A list of user models
   */
  export namespace AutocompleteList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = UserMiniModel[];
  }

  /**
   * No description
   * @tags User
   * @name OnboardingCreate
   * @summary Stores onboarding data for the current user
   * @request POST:/users/onboarding
   * @response `200` `void` The onboarding data was stored in the user profile
   */
  export namespace OnboardingCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = OnboardingUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags User
   * @name OnboardingStatusCreate
   * @summary Stores onboarding status for the current user
   * @request POST:/users/onboarding/status
   * @response `200` `void` The onboarding status was stored in the user profile
   */
  export namespace OnboardingStatusCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags User
   * @name OnboardingStatusList
   * @summary Gets onboarding status for the current user
   * @request GET:/users/onboarding/status
   * @response `200` `boolean` The onboarding status
   */
  export namespace OnboardingStatusList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = boolean;
  }
}

export namespace Workspaces {
  /**
   * No description
   * @tags Workspace
   * @name WorkspacesCreate
   * @summary Create a new workspace. The current user becomes a member of the workspace with the Owner role
   * @request POST:/workspaces
   * @response `200` `string` The workspace id
   * @response `403` `void` The user does not have the permission to create workspaces in the target community entity
   */
  export namespace WorkspacesCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = WorkspaceUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Workspace
   * @name WorkspacesList
   * @summary List all accessible workspaces for a given search query. Only workspaces accessible to the currently logged in user will be returned
   * @request GET:/workspaces
   * @response `200` `CommunityEntityMiniModelListPage`
   */
  export namespace WorkspacesList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModelListPage;
  }

  /**
   * No description
   * @tags Workspace
   * @name WorkspacesDetailDetail
   * @summary Returns a single workspace
   * @request GET:/workspaces/{id}
   * @response `200` `WorkspaceModel` The workspace data
   * @response `404` `void` No workspace was found for that id
   */
  export namespace WorkspacesDetailDetail {
    export type RequestParams = {
      /** ID of the workspace */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = WorkspaceModel;
  }

  /**
   * No description
   * @tags Workspace
   * @name WorkspacesPartialUpdateDetail
   * @summary Patches the specified workspace
   * @request PATCH:/workspaces/{id}
   * @response `200` `string` The ID of the entity
   * @response `403` `void` The current user does not have rights to edit this workspace
   * @response `404` `void` No workspace was found for that id
   */
  export namespace WorkspacesPartialUpdateDetail {
    export type RequestParams = {
      /** ID of the workspace */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = WorkspacePatchModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Workspace
   * @name WorkspacesUpdateDetail
   * @summary Updates the specified workspace
   * @request PUT:/workspaces/{id}
   * @response `200` `string` The ID of the entity
   * @response `403` `void` The current user does not have rights to edit this workspace
   * @response `404` `void` No workspace was found for that id
   */
  export namespace WorkspacesUpdateDetail {
    export type RequestParams = {
      /** ID of the workspace */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = WorkspaceUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Workspace
   * @name WorkspacesDeleteDetail
   * @summary Deletes the specified workspace and removes all of the community's associations from workspaces and nodes
   * @request DELETE:/workspaces/{id}
   * @response `403` `void` The current user does not have rights to delete this workspace
   * @response `404` `void` No workspace was found for that id
   */
  export namespace WorkspacesDeleteDetail {
    export type RequestParams = {
      /** ID of the workspace */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Workspace
   * @name DetailDetail
   * @summary Returns a single workspace including detailed stats
   * @request GET:/workspaces/{id}/detail
   * @response `200` `WorkspaceDetailModel` The workspace data including detailed stats
   * @response `404` `void` No workspace was found for that id
   */
  export namespace DetailDetail {
    export type RequestParams = {
      /** ID of the workspace */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = WorkspaceDetailModel;
  }

  /**
   * No description
   * @tags Workspace
   * @name AutocompleteList
   * @summary List the basic information of workspaces for a given search query. Only workspaces accessible to the currently logged in user will be returned
   * @request GET:/workspaces/autocomplete
   * @response `200` `(CommunityEntityMiniModel)[]`
   */
  export namespace AutocompleteList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModel[];
  }

  /**
   * No description
   * @tags Workspace
   * @name NodesDetail
   * @request GET:/workspaces/{id}/nodes
   * @response `403` `string` The current user does not have rights to view this workspace's content
   * @response `404` `void` No workspace was found for that id
   */
  export namespace NodesDetail {
    export type RequestParams = {
      /** ID of the workspace */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Workspace
   * @name OrganizationsDetail
   * @request GET:/workspaces/{id}/organizations
   * @response `403` `string` The current user does not have rights to view this workspace's content
   * @response `404` `void` No workspace was found for that id
   */
  export namespace OrganizationsDetail {
    export type RequestParams = {
      /** ID of the workspace */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Workspace
   * @name CfpsDetail
   * @summary Lists all calls for proposals for the workspace
   * @request GET:/workspaces/{id}/cfps
   * @response `200` `(CallForProposalModel)[]`
   * @response `403` `string` The current user does not have rights to view this workspace's contents
   * @response `404` `void` No workspace was found for that id
   */
  export namespace CfpsDetail {
    export type RequestParams = {
      /** ID of the workspace */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CallForProposalModel[];
  }

  /**
   * No description
   * @tags Workspace
   * @name PapersAggregateDetail
   * @summary Lists all papers for the specified workspace and its ecosystem
   * @request GET:/workspaces/{id}/papers/aggregate
   * @deprecated
   * @response `200` `(PaperModel)[]`
   * @response `403` `string` The current user does not have rights to view this workspace's content
   * @response `404` `void` No workspace was found for that id
   */
  export namespace PapersAggregateDetail {
    export type RequestParams = {
      /** ID of the workspace */
      id: string;
    };
    export type RequestQuery = {
      types?: CommunityEntityType[];
      communityEntityIds?: string[];
      type?: PaperType;
      tags?: PaperTag[];
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PaperModel[];
  }

  /**
   * No description
   * @tags Workspace
   * @name PapersAggregateCommunityEntitiesDetail
   * @summary Lists all community entities for papers for the specified workspace and its ecosystem
   * @request GET:/workspaces/{id}/papers/aggregate/communityEntities
   * @deprecated
   * @response `200` `(CommunityEntityMiniModel)[]`
   * @response `403` `void` The current user does not have rights to view this workspace's content
   * @response `404` `void` No workspace was found for that id
   */
  export namespace PapersAggregateCommunityEntitiesDetail {
    export type RequestParams = {
      /** ID of the workspace */
      id: string;
    };
    export type RequestQuery = {
      types?: CommunityEntityType[];
      type?: PaperType;
      tags?: PaperTag[];
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModel[];
  }

  /**
   * No description
   * @tags Workspace
   * @name NeedsAggregateDetail
   * @request GET:/workspaces/{id}/needs/aggregate
   * @deprecated
   * @response `200` `(NeedModel)[]`
   * @response `403` `string` The current user does not have rights to view this workspace's content
   * @response `404` `void` No workspace was found for that id
   */
  export namespace NeedsAggregateDetail {
    export type RequestParams = {
      /** ID of the workspace */
      id: string;
    };
    export type RequestQuery = {
      communityEntityIds?: string[];
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = NeedModel[];
  }

  /**
   * No description
   * @tags Workspace
   * @name NeedsAggregateCommunityEntitiesDetail
   * @summary Lists all community entities for needs for the specified workspace and its ecosystem
   * @request GET:/workspaces/{id}/needs/aggregate/communityEntities
   * @deprecated
   * @response `200` `(CommunityEntityMiniModel)[]`
   * @response `403` `void` The current user does not have rights to view this workspace's content
   * @response `404` `void` No workspace was found for that id
   */
  export namespace NeedsAggregateCommunityEntitiesDetail {
    export type RequestParams = {
      /** ID of the workspace */
      id: string;
    };
    export type RequestQuery = {
      types?: CommunityEntityType[];
      currentUser?: boolean;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityMiniModel[];
  }

  /**
   * No description
   * @tags Workspace
   * @name WorkspacesDetail
   * @request GET:/workspaces/{id}/workspaces
   * @response `200` `(WorkspaceModel)[]` Workspace data
   * @response `403` `string` The current user does not have rights to view this workspace's content
   * @response `404` `void` No workspace was found for that id
   */
  export namespace WorkspacesDetail {
    export type RequestParams = {
      /** ID of the workspace */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = WorkspaceModel[];
  }

  /**
   * No description
   * @tags Workspace
   * @name PaperList
   * @request GET:/workspaces/paper
   * @deprecated
   * @response `404` `void` No project was found for that id
   */
  export namespace PaperList {
    export type RequestParams = {};
    export type RequestQuery = {
      /** External ID */
      externalID?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Workspace
   * @name DocumentsCreate
   * @summary Adds a new document for the specified entity.
   * @request POST:/workspaces/{id}/documents
   * @deprecated
   * @response `200` `string` The document was created
   * @response `400` `void` Note-typed documents have to be created and updated through the feed endpoints
   * @response `403` `void` The current user doesn't have sufficient rights to add documents for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace DocumentsCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = DocumentInsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Workspace
   * @name DocumentsDetail
   * @summary Lists all documents for the specified entity, not including file data
   * @request GET:/workspaces/{id}/documents
   * @deprecated
   * @response `200` `string` The document was created
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace DocumentsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      folderId?: string;
      type?: DocumentFilter;
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Workspace
   * @name DocumentsDetailDetail
   * @summary Returns a single document, including the file represented as base64
   * @request GET:/workspaces/{id}/documents/{documentId}
   * @deprecated
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
   * @response `404` `void` No entity was found for that id or the document does not exist
   */
  export namespace DocumentsDetailDetail {
    export type RequestParams = {
      id: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Workspace
   * @name DocumentsUpdateDetail
   * @summary Updates the title and description for the document
   * @request PUT:/workspaces/{id}/documents/{documentId}
   * @deprecated
   * @response `200` `void` The document was updated
   * @response `403` `void` The current user doesn't have sufficient rights to edit documents for the entity
   * @response `404` `void` No entity was found for that id or the document does not exist
   */
  export namespace DocumentsUpdateDetail {
    export type RequestParams = {
      id: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = DocumentUpdateModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name DocumentsDeleteDetail
   * @summary Deletes the specified document
   * @request DELETE:/workspaces/{id}/documents/{documentId}
   * @deprecated
   * @response `200` `void` The document was deleted
   * @response `403` `void` The current user doesn't have sufficient rights to delete the document
   * @response `404` `void` No entity was found for that id or the document does not exist
   */
  export namespace DocumentsDeleteDetail {
    export type RequestParams = {
      id: string;
      documentId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name DocumentsDraftDetail
   * @summary Returns a draft document for the specified container
   * @request GET:/workspaces/{id}/documents/draft
   * @deprecated
   * @response `204` `void` No draft document was found for the community entity
   * @response `404` `void` No community entity was found for the specified id
   */
  export namespace DocumentsDraftDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name DocumentsAllDetail
   * @summary Lists all documents and folders for the specified entity, not including file data
   * @request GET:/workspaces/{id}/documents/all
   * @deprecated
   * @response `200` `(BaseModel)[]` The document was created
   * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace DocumentsAllDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = BaseModel[];
  }

  /**
   * No description
   * @tags Workspace
   * @name JoinCreate
   * @summary Joins an entity on behalf of the currently logged in user. Only works for community entities with the Open membership access level. Creates a membership record immediately, no invitation is created.
   * @request POST:/workspaces/{id}/join
   * @deprecated
   * @response `200` `string` ID of the new membership object
   * @response `403` `void` The entity is not open to members joining
   * @response `404` `void` The entity does not exist
   * @response `409` `void` A membership record already exists for the entity and user
   */
  export namespace JoinCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Workspace
   * @name RequestCreate
   * @summary Creates a request to join an entity on behalf of the currently logged in user
   * @request POST:/workspaces/{id}/request
   * @deprecated
   * @response `200` `string` ID of the new invitation object
   * @response `403` `void` The entity is not open to members requesting to join
   * @response `404` `void` The entity does not exist
   * @response `409` `void` An invitation already exists for the entity and user
   */
  export namespace RequestCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Workspace
   * @name InviteIdCreateDetail
   * @summary Invite a user to an entity using their ID
   * @request POST:/workspaces/{id}/invite/id/{userId}
   * @response `200` `string` ID of the new invitation object
   * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity, or the user is inviting an owner without having the rights to manage owners
   * @response `404` `void` The entity or user do not exist
   * @response `409` `void` An invitation already exists for the entity and user
   */
  export namespace InviteIdCreateDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the user to invite */
      userId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = AccessLevel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Workspace
   * @name InviteEmailCreateDetail
   * @summary Invite a user to an entity using their email
   * @request POST:/workspaces/{id}/invite/email/{userEmail}
   * @response `200` `string` ID of the new invitation object
   * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace InviteEmailCreateDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the user to invite */
      userEmail: string;
    };
    export type RequestQuery = {};
    export type RequestBody = AccessLevel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Workspace
   * @name InviteEmailBatchCreate
   * @summary Invite a user to an entity using their email
   * @request POST:/workspaces/{id}/invite/email/batch
   * @deprecated
   * @response `200` `string` ID of the new invitation object
   * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace InviteEmailBatchCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = string[];
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Workspace
   * @name InvitesDetail
   * @summary List all pending invitations for a given entity
   * @request GET:/workspaces/{id}/invites
   * @response `200` `(InvitationModelUser)[]` A list of invitations
   * @response `404` `void` No entity was found for that id
   */
  export namespace InvitesDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = InvitationModelUser[];
  }

  /**
   * No description
   * @tags Workspace
   * @name InvitesAcceptCreate
   * @summary Accept the invite on behalf of the currently logged-in user
   * @request POST:/workspaces/{id}/invites/{invitationId}/accept
   * @response `200` `void` The invitation has been accepted. The user is now a member of the project.
   * @response `403` `void` The current user does not have the right to accept the invitation
   * @response `404` `void` No entity was found for that id or the invitation does not exist
   */
  export namespace InvitesAcceptCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name InvitesRejectCreate
   * @summary Reject the invite on behalf of the currently logged-in user
   * @request POST:/workspaces/{id}/invites/{invitationId}/reject
   * @response `200` `void` The invitation has been rejected
   * @response `403` `void` The current user does not have the right to reject the invitation
   * @response `404` `void` No entity was found for that id or the invitation does not exist
   */
  export namespace InvitesRejectCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name InvitesCancelCreate
   * @summary Cancels an invite
   * @request POST:/workspaces/{id}/invites/{invitationId}/cancel
   * @response `200` `void` The invitation has been cancelled
   * @response `403` `void` The current user does not have the right to cancel the invitation
   * @response `404` `void` No entity was found for that id or the invitation does not exist
   */
  export namespace InvitesCancelCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name InvitesResendCreate
   * @summary Resends an invite
   * @request POST:/workspaces/invites/{invitationId}/resend
   * @response `200` `void` The invitation has been resent
   * @response `400` `void` The invitation is a request and cannot be resent
   * @response `403` `void` The user does not have the right to resend the invitation
   * @response `404` `void` No entity was found for that id or the invitation does not exist
   */
  export namespace InvitesResendCreate {
    export type RequestParams = {
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name LeaveCreate
   * @summary Leaves an entity on behalf of the currently logged in user
   * @request POST:/workspaces/{id}/leave
   * @deprecated
   * @response `200` `void` The user has successfully left the entity
   * @response `404` `void` The entity does not exist or the user is not a member
   */
  export namespace LeaveCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name OnboardingResponsesDetailDetail
   * @summary List onboarding responses for a community entity and a user
   * @request GET:/workspaces/{id}/onboardingResponses/{userId}
   * @deprecated
   * @response `200` `OnboardingQuestionnaireInstanceModel` onboarding questionnaire responses
   * @response `404` `void` No entity was found for that id or no user was found
   */
  export namespace OnboardingResponsesDetailDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      userId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = OnboardingQuestionnaireInstanceModel;
  }

  /**
   * No description
   * @tags Workspace
   * @name OnboardingResponsesCreate
   * @summary Posts onboarding responses for a community entity for the current user
   * @request POST:/workspaces/{id}/onboardingResponses
   * @deprecated
   * @response `200` `void` The onboarding questionnaire responses were saved successfully
   * @response `404` `void` No entity was found for that id
   */
  export namespace OnboardingResponsesCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = OnboardingQuestionnaireInstanceUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name OnboardingCompletionCreate
   * @summary Records the completion of the onboarding workflow for a community entity for the current user
   * @request POST:/workspaces/{id}/onboardingCompletion
   * @deprecated
   * @response `200` `void` The onboarding completion was recorded successfully
   * @response `404` `void` No entity was found for that id
   */
  export namespace OnboardingCompletionCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name MembersDetail
   * @summary List all members for an entity
   * @request GET:/workspaces/{id}/members
   * @response `200` `(MemberModel)[]` A list of members
   * @response `403` `void` The current user doesn't have sufficient rights to see the entity's members
   * @response `404` `void` No entity was found for that id
   */
  export namespace MembersDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = MemberModel[];
  }

  /**
   * No description
   * @tags Workspace
   * @name InvitationDetail
   * @summary Returns the pending invitation for an object for the current user
   * @request GET:/workspaces/{id}/invitation
   * @response `200` `InvitationModelEntity` A pending invitation
   * @response `404` `void` No invitation is pending or no entity was found for that id
   */
  export namespace InvitationDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = InvitationModelEntity;
  }

  /**
   * No description
   * @tags Workspace
   * @name MembersUpdateDetail
   * @summary Updates a member's access level for an entity
   * @request PUT:/workspaces/{id}/members/{memberId}
   * @response `200` `void` The access level has been updated
   * @response `403` `void` The user does not have the right to set this access level for this member
   * @response `404` `void` No entity was found for that id or the user is not a member of that entity
   */
  export namespace MembersUpdateDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the entity */
      memberId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = AccessLevel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name MembersDeleteDetail
   * @summary Removes a member's access from an entity
   * @request DELETE:/workspaces/{id}/members/{memberId}
   * @response `200` `void` The member has been removed
   * @response `403` `void` The current user does not have the right to remove members from this entity
   * @response `404` `void` No entity was found for that id or the user is not a member of that entity
   */
  export namespace MembersDeleteDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the entity */
      memberId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name MembersContributionUpdate
   * @summary Updates a member's contribution
   * @request PUT:/workspaces/{id}/members/{memberId}/contribution
   * @response `200` `void` The contribution has been updated
   * @response `403` `void` The user does not have the right to set the contribution for other members
   * @response `404` `void` No entity was found for that id or the user is not a member of that entity
   */
  export namespace MembersContributionUpdate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the user */
      memberId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = string;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name MembersLabelsUpdate
   * @summary Updates a member's labels
   * @request PUT:/workspaces/{id}/members/{memberId}/labels
   * @response `200` `void` The labels have been updated
   * @response `403` `void` The current user does not have the right to set labels for members
   * @response `404` `void` No entity was found for that id or the user is not a member of that entity
   */
  export namespace MembersLabelsUpdate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the user */
      memberId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = string[];
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name CommunityEntityInviteCreateDetail
   * @summary Invite a community entity to become affiliated to another entity
   * @request POST:/workspaces/{id}/communityEntityInvite/{targetEntityId}
   * @response `200` `string` ID of the new invitation object
   * @response `400` `void` An invitation cannot be extended to a community entity of the same type
   * @response `403` `void` The current user does not have the rights to invite entities to the specified entity
   * @response `404` `void` One of the entities does not exist
   * @response `409` `void` An invitation or affiliation already exists for the entities
   */
  export namespace CommunityEntityInviteCreateDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the target entity */
      targetEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Workspace
   * @name CommunityEntityInvitesIncomingDetail
   * @summary List all pending invitations for a given entity
   * @request GET:/workspaces/{id}/communityEntityInvites/incoming
   * @response `200` `(CommunityEntityInvitationModelSource)[]` A list of invitations
   * @response `404` `void` No entity was found for that id
   */
  export namespace CommunityEntityInvitesIncomingDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityInvitationModelSource[];
  }

  /**
   * No description
   * @tags Workspace
   * @name CommunityEntityInvitesOutgoingDetail
   * @summary List all pending invitations for a given entity
   * @request GET:/workspaces/{id}/communityEntityInvites/outgoing
   * @response `200` `(CommunityEntityInvitationModelSource)[]` A list of invitations
   * @response `404` `void` No entity was found for that id
   */
  export namespace CommunityEntityInvitesOutgoingDetail {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CommunityEntityInvitationModelSource[];
  }

  /**
   * No description
   * @tags Workspace
   * @name CommunityEntityInvitesAcceptCreate
   * @summary Accept an invite
   * @request POST:/workspaces/{id}/communityEntityInvites/{invitationId}/accept
   * @response `200` `void` The invitation has been accepted
   * @response `403` `void` The current user does not have the right to accept the invitation
   * @response `404` `void` One of the entities does not exist or the invitation does not exist
   */
  export namespace CommunityEntityInvitesAcceptCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name CommunityEntityInvitesRejectCreate
   * @summary Rejects an invite
   * @request POST:/workspaces/{id}/communityEntityInvites/{invitationId}/reject
   * @response `200` `void` The invitation has been rejected
   * @response `403` `void` The current user does not have the right to reject the invitation
   * @response `404` `void` One of the entities does not exist or the invitation does not exist
   */
  export namespace CommunityEntityInvitesRejectCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name CommunityEntityInvitesCancelCreate
   * @summary Cancels an invite
   * @request POST:/workspaces/{id}/communityEntityInvites/{invitationId}/cancel
   * @response `200` `void` The invitation has been cancelled
   * @response `403` `void` The current user does not have the right to cancel the invitation
   * @response `404` `void` One of the entities does not exist or the invitation does not exist
   */
  export namespace CommunityEntityInvitesCancelCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the invitation */
      invitationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name CommunityEntityRemoveCreate
   * @summary Removes an affiliation of an entity to another entity
   * @request POST:/workspaces/{id}/communityEntity/{targetEntityId}/remove
   * @response `200` `void` The entity affiliation has been removed successfully
   * @response `403` `void` The current user does not have the permissions to manage the community entity's affiliations
   * @response `404` `void` One of the entities does not exist or the entities are not affiliated
   */
  export namespace CommunityEntityRemoveCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the target entity */
      targetEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name CommunityEntityLinkCreate
   * @summary Creates an affiliation of an entity to another entity. On the entity, the user must be admin or owner, or the toggle for members to allow creating community entities of the respective type must be on. The user must be admin or owner on the target entity.
   * @request POST:/workspaces/{id}/communityEntity/{targetEntityId}/link
   * @response `200` `void` The entity affiliation has been created successfully
   * @response `409` `void` One of the entities does not exist or the entities are already affiliated
   */
  export namespace CommunityEntityLinkCreate {
    export type RequestParams = {
      /** ID of the entity */
      id: string;
      /** ID of the target entity */
      targetEntityId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name EcosystemCommunityEntitiesDetail
   * @summary Lists all ecosystem containers (projects, communities and nodes) for the given community entity
   * @request GET:/workspaces/{id}/ecosystem/communityEntities
   * @deprecated
   * @response `200` `(EntityMiniModel)[]`
   * @response `403` `string` The current user does not have rights to view this community entity's contents
   * @response `404` `void` No community entity was found for that id
   */
  export namespace EcosystemCommunityEntitiesDetail {
    export type RequestParams = {
      /** ID of the community entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EntityMiniModel[];
  }

  /**
   * No description
   * @tags Workspace
   * @name EcosystemUsersDetail
   * @summary Lists all ecosystem members for the given community entity
   * @request GET:/workspaces/{id}/ecosystem/users
   * @response `200` `(EntityMiniModel)[]`
   * @response `403` `string` The current user does not have rights to view this community entity's contents
   * @response `404` `void` No community entity was found for that id
   */
  export namespace EcosystemUsersDetail {
    export type RequestParams = {
      /** ID of the community entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EntityMiniModel[];
  }

  /**
   * No description
   * @tags Workspace
   * @name NeedsCreate
   * @summary Adds a new need for the specified project
   * @request POST:/workspaces/{id}/needs
   * @deprecated
   * @response `200` `string` The ID of the new need
   * @response `403` `void` The current user doesn't have sufficient rights to add needs for the entity
   */
  export namespace NeedsCreate {
    export type RequestParams = {
      /** ID of the project */
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = NeedUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Workspace
   * @name NeedsDetail
   * @summary Lists all needs for the specified community entity
   * @request GET:/workspaces/{id}/needs
   * @deprecated
   * @response `200` `(NeedModel)[]`
   * @response `403` `string` The current user does not have rights to view this community entity's contents
   * @response `404` `void` No community entity was found for that id
   */
  export namespace NeedsDetail {
    export type RequestParams = {
      /** ID of the community entity */
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = NeedModel[];
  }

  /**
   * No description
   * @tags Workspace
   * @name NeedsDetailDetail
   * @summary Returns a single need
   * @request GET:/workspaces/needs/{needId}
   * @deprecated
   * @response `403` `string` The current user does not have rights to view this community entity's contents
   * @response `404` `void` No need was found for the need id
   */
  export namespace NeedsDetailDetail {
    export type RequestParams = {
      needId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = any;
  }

  /**
   * No description
   * @tags Workspace
   * @name NeedsUpdateDetail
   * @summary Updates the need
   * @request PUT:/workspaces/needs/{needId}
   * @deprecated
   * @response `200` `void` The need was updated
   * @response `403` `void` The current user does not have rights to update needs on this community entity
   * @response `404` `void` No need was found for the need id
   */
  export namespace NeedsUpdateDetail {
    export type RequestParams = {
      needId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = NeedUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name NeedsDeleteDetail
   * @summary Deletes the specified need
   * @request DELETE:/workspaces/needs/{needId}
   * @deprecated
   * @response `200` `void` The need was deleted
   * @response `403` `void` The current user does not have rights to delete needs on this community entity
   * @response `404` `void` No need was found for the need id
   */
  export namespace NeedsDeleteDetail {
    export type RequestParams = {
      needId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }

  /**
   * No description
   * @tags Workspace
   * @name EventsCreate
   * @summary Adds a new event for the specified entity.
   * @request POST:/workspaces/{id}/events
   * @deprecated
   * @response `200` `string` The event was created
   * @response `403` `void` The current user doesn't have sufficient rights to add events for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace EventsCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = EventUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Workspace
   * @name EventsDetail
   * @summary Lists events for the specified entity.
   * @request GET:/workspaces/{id}/events
   * @deprecated
   * @response `200` `(EventModel)[]` Event data
   * @response `403` `void` The current user doesn't have sufficient rights to view events for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace EventsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /** @format date-time */
      from?: string;
      /** @format date-time */
      to?: string;
      tags?: EventTag[];
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EventModel[];
  }

  /**
   * No description
   * @tags Workspace
   * @name ChannelsCreate
   * @summary Adds a new channel for the specified entity.
   * @request POST:/workspaces/{id}/channels
   * @deprecated
   * @response `200` `string` The channel was created
   * @response `403` `void` The current user doesn't have sufficient rights to add discussion channels for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace ChannelsCreate {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ChannelUpsertModel;
    export type RequestHeaders = {};
    export type ResponseBody = string;
  }

  /**
   * No description
   * @tags Workspace
   * @name ChannelsDetail
   * @summary Lists channels for the specified entity.
   * @request GET:/workspaces/{id}/channels
   * @deprecated
   * @response `200` `(ChannelModel)[]` channel data
   * @response `403` `void` The current user doesn't have sufficient rights to view channels for the entity
   * @response `404` `void` No entity was found for that id
   */
  export namespace ChannelsDetail {
    export type RequestParams = {
      id: string;
    };
    export type RequestQuery = {
      /**
       * The page to fetch. Pages start at 1.
       * @format int32
       * @min 1
       * @max 1000
       */
      Page?: number;
      /**
       * Size of one page. The default is 50. Maximum value is 1000
       * @format int32
       * @min 1
       * @max 1000
       */
      PageSize?: number;
      SortKey?: SortKey;
      SortAscending?: boolean;
      /** The search query to filter projects by */
      Search?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ChannelModel[];
  }
}

import type { AxiosInstance, AxiosRequestConfig, AxiosResponse, HeadersDefaults, ResponseType } from 'axios';
import axios from 'axios';

export type QueryParamsType = Record<string | number, any>;

export interface FullRequestParams extends Omit<AxiosRequestConfig, 'data' | 'params' | 'url' | 'responseType'> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseType;
  /** request body */
  body?: unknown;
}

export type RequestParams = Omit<FullRequestParams, 'body' | 'method' | 'query' | 'path'>;

export interface ApiConfig<SecurityDataType = unknown> extends Omit<AxiosRequestConfig, 'data' | 'cancelToken'> {
  securityWorker?: (
    securityData: SecurityDataType | null,
  ) => Promise<AxiosRequestConfig | void> | AxiosRequestConfig | void;
  secure?: boolean;
  format?: ResponseType;
}

export enum ContentType {
  Json = 'application/json',
  FormData = 'multipart/form-data',
  UrlEncoded = 'application/x-www-form-urlencoded',
  Text = 'text/plain',
}

export class HttpClient<SecurityDataType = unknown> {
  public instance: AxiosInstance;
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>['securityWorker'];
  private secure?: boolean;
  private format?: ResponseType;

  constructor({ securityWorker, secure, format, ...axiosConfig }: ApiConfig<SecurityDataType> = {}) {
    this.instance = axios.create({ ...axiosConfig, baseURL: axiosConfig.baseURL || '' });
    this.secure = secure;
    this.format = format;
    this.securityWorker = securityWorker;
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  protected mergeRequestParams(params1: AxiosRequestConfig, params2?: AxiosRequestConfig): AxiosRequestConfig {
    const method = params1.method || (params2 && params2.method);

    return {
      ...this.instance.defaults,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...((method && this.instance.defaults.headers[method.toLowerCase() as keyof HeadersDefaults]) || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  protected stringifyFormItem(formItem: unknown) {
    if (typeof formItem === 'object' && formItem !== null) {
      return JSON.stringify(formItem);
    } else {
      return `${formItem}`;
    }
  }

  protected createFormData(input: Record<string, unknown>): FormData {
    if (input instanceof FormData) {
      return input;
    }
    return Object.keys(input || {}).reduce((formData, key) => {
      const property = input[key];
      const propertyContent: any[] = property instanceof Array ? property : [property];

      for (const formItem of propertyContent) {
        const isFileType = formItem instanceof Blob || formItem instanceof File;
        formData.append(key, isFileType ? formItem : this.stringifyFormItem(formItem));
      }

      return formData;
    }, new FormData());
  }

  public request = async <T = any, _E = any>({
    secure,
    path,
    type,
    query,
    format,
    body,
    ...params
  }: FullRequestParams): Promise<AxiosResponse<T>> => {
    const secureParams =
      ((typeof secure === 'boolean' ? secure : this.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const responseFormat = format || this.format || undefined;

    if (type === ContentType.FormData && body && body !== null && typeof body === 'object') {
      body = this.createFormData(body as Record<string, unknown>);
    }

    if (type === ContentType.Text && body && body !== null && typeof body !== 'string') {
      body = JSON.stringify(body);
    }

    return this.instance.request({
      ...requestParams,
      headers: {
        ...(requestParams.headers || {}),
        ...(type && type !== ContentType.FormData ? { 'Content-Type': type } : {}),
      },
      params: query,
      responseType: responseFormat,
      data: body,
      url: path,
    });
  };
}

/**
 * @title JOGL API
 * @version v1
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
  auth = {
    /**
     * No description
     *
     * @tags Auth
     * @name LoginCreate
     * @summary Logs a user in using an email and password
     * @request POST:/auth/login
     * @response `200` `AuthResultModel` User login successful
     * @response `401` `void` Invalid user credentials
     * @response `403` `void` User not yet verified
     */
    loginCreate: (data: UserLoginPasswordModel, params: RequestParams = {}) =>
      this.request<AuthResultModel, void>({
        path: `/auth/login`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Auth
     * @name LoginPasswordCreate
     * @summary Logs a user in using an email and password
     * @request POST:/auth/login/password
     * @response `200` `AuthResultModel` User login successful
     * @response `401` `void` Invalid user credentials
     * @response `403` `void` User not yet verified
     */
    loginPasswordCreate: (data: UserLoginPasswordModel, params: RequestParams = {}) =>
      this.request<AuthResultModel, void>({
        path: `/auth/login/password`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Auth
     * @name VerificationCreate
     * @summary Creates a new email verification for a given email
     * @request POST:/auth/verification
     * @response `200` `void` The verification was created
     */
    verificationCreate: (data: VerificationStartModel, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/auth/verification`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Auth
     * @name WalletChallengeDetailDetail
     * @summary Retrieves a user challenge for a wallet signature login
     * @request GET:/auth/wallet/challenge/{wallet}
     * @response `200` `string` User challenge
     */
    walletChallengeDetailDetail: (wallet: string, params: RequestParams = {}) =>
      this.request<string, any>({
        path: `/auth/wallet/challenge/${wallet}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Auth
     * @name WalletCreateDetail
     * @summary Logs a user in using a web3 wallet signature
     * @request POST:/auth/wallet/{walletType}
     * @response `200` `AuthResultModel` User login successful
     * @response `401` `void` Invalid signature
     * @response `403` `void` User not yet verified
     */
    walletCreateDetail: (walletType: WalletType, data: UserLoginSignatureModel, params: RequestParams = {}) =>
      this.request<AuthResultModel, void>({
        path: `/auth/wallet/${walletType}`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Auth
     * @name VerificationCheckList
     * @summary Returns the status of a user email verification for a given email and code
     * @request GET:/auth/verification/check
     * @response `200` `VerificationStatus` The verification status
     */
    verificationCheckList: (
      query?: {
        email?: string;
        code?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<VerificationStatus, any>({
        path: `/auth/verification/check`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Auth
     * @name VerificationConfirmCreate
     * @summary Confirms the verification for a given email and code
     * @request POST:/auth/verification/confirm
     * @response `200` `void` The verification was completed
     * @response `400` `void` The supplied verification code is invalid
     */
    verificationConfirmCreate: (data: VerificationConfirmationModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/auth/verification/confirm`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Auth
     * @name ResetList
     * @summary Initiates the password reset process using the supplied email address
     * @request GET:/auth/reset
     * @response `200` `void`
     */
    resetList: (
      query?: {
        email?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<void, any>({
        path: `/auth/reset`,
        method: 'GET',
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Auth
     * @name ResetConfirmCreate
     * @summary Resets a user's password using a code delivered to their email address
     * @request POST:/auth/resetConfirm
     * @response `200` `void` The user's password was reset successfully
     * @response `403` `void` The user does not exist, the code is invalid or has expired
     */
    resetConfirmCreate: (data: UserForgotPasswordModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/auth/resetConfirm`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Auth
     * @name PasswordCreate
     * @summary Updates a user's password
     * @request POST:/auth/password
     * @response `200` `void` The user's password was reset successfully
     * @response `403` `void` The old password is invalid
     */
    passwordCreate: (data: UserPasswordChangeModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/auth/password`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Auth
     * @name CodeTriggerList
     * @summary Initiates the one-time login process using the supplied email address
     * @request GET:/auth/code/trigger
     * @response `200` `void` Success
     */
    codeTriggerList: (
      query?: {
        email?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<void, any>({
        path: `/auth/code/trigger`,
        method: 'GET',
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Auth
     * @name CodeLoginCreate
     * @summary Verifies the one-time login code and completes the one-time login processs. If a user doesn't exist with the supplied email, it will be created. A user created in this manner has no password and can only log in via further one-time codes until they link another authentication method to their account.
     * @request POST:/auth/code/login
     * @response `200` `void` User login successful
     * @response `401` `void` Invalid user credentials
     */
    codeLoginCreate: (data: UserLoginCodeModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/auth/code/login`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Auth
     * @name OrcidCreate
     * @summary register or sign-in the user's unique ORCID id from ORCID and stores it in the database
     * @request POST:/auth/orcid
     * @response `200` `AuthResultExtendedModel` Login or registration successful
     * @response `401` `void` No ORCID record found
     * @response `403` `void` Login or registration forbidden
     */
    orcidCreate: (data: OrcidRegistrationModel, params: RequestParams = {}) =>
      this.request<AuthResultExtendedModel, void>({
        path: `/auth/orcid`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Auth
     * @name GoogleCreate
     * @summary Register or sign-in the user with a google access token
     * @request POST:/auth/google
     * @response `200` `AuthResultExtendedModel` Login or registration successful
     * @response `401` `void` No google record found
     * @response `403` `void` Login or registration forbidden
     */
    googleCreate: (data: AccessTokenModel, params: RequestParams = {}) =>
      this.request<AuthResultExtendedModel, void>({
        path: `/auth/google`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Auth
     * @name LinkedinCreate
     * @summary Register or sign-in the user with a linkedin access token
     * @request POST:/auth/linkedin
     * @response `200` `AuthResultExtendedModel` Login or registration successful
     * @response `401` `void` No linkedin record found
     * @response `403` `void` Login or registration forbidden
     */
    linkedinCreate: (data: AuthorizationCodeModel, params: RequestParams = {}) =>
      this.request<AuthResultExtendedModel, void>({
        path: `/auth/linkedin`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
  cfPs = {
    /**
     * No description
     *
     * @tags CallForProposal
     * @name CfPsCreate
     * @summary Create a new call for proposals. The current user becomes a member of the call for proposals with the Owner role
     * @request POST:/CFPs
     * @response `200` `void` Success
     */
    cfPsCreate: (data: CallForProposalUpsertModel, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/CFPs`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name CfPsList
     * @summary List all accessible calls for proposals for a given search query. Only calls for proposals accessible to the currently logged in user will be returned
     * @request GET:/CFPs
     * @response `200` `CommunityEntityMiniModelListPage`
     */
    cfPsList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModelListPage, any>({
        path: `/CFPs`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name CfPsDetailDetail
     * @summary Returns a single call for proposals
     * @request GET:/CFPs/{id}
     * @response `200` `CallForProposalModel` The call for proposals data
     * @response `404` `void` No call for proposals was found for that id
     */
    cfPsDetailDetail: (id: string, params: RequestParams = {}) =>
      this.request<CallForProposalModel, void>({
        path: `/CFPs/${id}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name CfPsPartialUpdateDetail
     * @summary Patches the specified call for proposal
     * @request PATCH:/CFPs/{id}
     * @response `200` `string` The ID of the entity
     * @response `403` `void` The current user does not have rights to edit this call for proposal
     * @response `404` `void` No call for proposal was found for that id
     */
    cfPsPartialUpdateDetail: (id: string, data: CallForProposalPatchModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/CFPs/${id}`,
        method: 'PATCH',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name CfPsUpdateDetail
     * @summary Updates the specified call for proposal
     * @request PUT:/CFPs/{id}
     * @response `200` `string` The ID of the entity
     * @response `403` `void` The current user does not have rights to edit this call for proposal
     * @response `404` `void` No call for proposal was found for that id
     */
    cfPsUpdateDetail: (id: string, data: CallForProposalUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/CFPs/${id}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name CfPsDeleteDetail
     * @summary Deletes the specified call for proposal and removes all of the call for proposal's associations from projects and communities
     * @request DELETE:/CFPs/{id}
     * @response `403` `void` The current user does not have rights to delete this call for proposal
     * @response `404` `void` No call for proposal was found for that id
     */
    cfPsDeleteDetail: (id: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/CFPs/${id}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name DetailDetail
     * @summary Returns a single call for proposals, including detailed stats
     * @request GET:/CFPs/{id}/detail
     * @response `200` `CallForProposalDetailModel` The call for proposals data including detailed stats
     * @response `404` `void` No call for proposals was found for that id
     */
    detailDetail: (id: string, params: RequestParams = {}) =>
      this.request<CallForProposalDetailModel, void>({
        path: `/CFPs/${id}/detail`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name AutocompleteList
     * @summary List the basic information of calls for proposals for a given search query. Only calls for proposals accessible to the currently logged in user will be returned
     * @request GET:/CFPs/autocomplete
     * @response `200` `(CommunityEntityMiniModel)[]`
     */
    autocompleteList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModel[], any>({
        path: `/CFPs/autocomplete`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name ProposalsDetail
     * @summary Lists all proposals submitted to the specified call for proposals
     * @request GET:/CFPs/{id}/proposals
     * @response `200` `(ProposalModel)[]`
     * @response `403` `void` The current user does not have the rights to view proposals
     * @response `404` `void` No call for proposal was found for that id
     */
    proposalsDetail: (id: string, params: RequestParams = {}) =>
      this.request<ProposalModel[], void>({
        path: `/CFPs/${id}/proposals`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name MessageCreate
     * @summary Sends a message to selected cfp members
     * @request POST:/CFPs/{id}/message
     * @response `200` `(ProposalModel)[]`
     * @response `403` `void` The current user does not have the rights to send messages to cfp members
     * @response `404` `void` No call for proposal was found for that id
     */
    messageCreate: (id: string, data: MessageModel, params: RequestParams = {}) =>
      this.request<ProposalModel[], void>({
        path: `/CFPs/${id}/message`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name ChangedCreate
     * @summary Determines whether the template for a CFP has changed or not
     * @request POST:/CFPs/{id}/changed
     * @response `200` `(ProposalModel)[]`
     * @response `404` `void` No call for proposal was found for that id
     */
    changedCreate: (id: string, data: CallForProposalUpsertModel, params: RequestParams = {}) =>
      this.request<ProposalModel[], void>({
        path: `/CFPs/${id}/changed`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name DocumentsCreate
     * @summary Adds a new document for the specified entity.
     * @request POST:/CFPs/{id}/documents
     * @deprecated
     * @response `200` `string` The document was created
     * @response `400` `void` Note-typed documents have to be created and updated through the feed endpoints
     * @response `403` `void` The current user doesn't have sufficient rights to add documents for the entity
     * @response `404` `void` No entity was found for that id
     */
    documentsCreate: (id: string, data: DocumentInsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/CFPs/${id}/documents`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name DocumentsDetail
     * @summary Lists all documents for the specified entity, not including file data
     * @request GET:/CFPs/{id}/documents
     * @deprecated
     * @response `200` `string` The document was created
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
     * @response `404` `void` No entity was found for that id
     */
    documentsDetail: (
      id: string,
      query?: {
        folderId?: string;
        type?: DocumentFilter;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string, void>({
        path: `/CFPs/${id}/documents`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name DocumentsDetailDetail
     * @summary Returns a single document, including the file represented as base64
     * @request GET:/CFPs/{id}/documents/{documentId}
     * @deprecated
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
     * @response `404` `void` No entity was found for that id or the document does not exist
     */
    documentsDetailDetail: (id: string, documentId: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/CFPs/${id}/documents/${documentId}`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name DocumentsUpdateDetail
     * @summary Updates the title and description for the document
     * @request PUT:/CFPs/{id}/documents/{documentId}
     * @deprecated
     * @response `200` `void` The document was updated
     * @response `403` `void` The current user doesn't have sufficient rights to edit documents for the entity
     * @response `404` `void` No entity was found for that id or the document does not exist
     */
    documentsUpdateDetail: (id: string, documentId: string, data: DocumentUpdateModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/${id}/documents/${documentId}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name DocumentsDeleteDetail
     * @summary Deletes the specified document
     * @request DELETE:/CFPs/{id}/documents/{documentId}
     * @deprecated
     * @response `200` `void` The document was deleted
     * @response `403` `void` The current user doesn't have sufficient rights to delete the document
     * @response `404` `void` No entity was found for that id or the document does not exist
     */
    documentsDeleteDetail: (id: string, documentId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/${id}/documents/${documentId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name DocumentsDraftDetail
     * @summary Returns a draft document for the specified container
     * @request GET:/CFPs/{id}/documents/draft
     * @deprecated
     * @response `204` `void` No draft document was found for the community entity
     * @response `404` `void` No community entity was found for the specified id
     */
    documentsDraftDetail: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/${id}/documents/draft`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name DocumentsAllDetail
     * @summary Lists all documents and folders for the specified entity, not including file data
     * @request GET:/CFPs/{id}/documents/all
     * @deprecated
     * @response `200` `(BaseModel)[]` The document was created
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
     * @response `404` `void` No entity was found for that id
     */
    documentsAllDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<BaseModel[], void>({
        path: `/CFPs/${id}/documents/all`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name JoinCreate
     * @summary Joins an entity on behalf of the currently logged in user. Only works for community entities with the Open membership access level. Creates a membership record immediately, no invitation is created.
     * @request POST:/CFPs/{id}/join
     * @deprecated
     * @response `200` `string` ID of the new membership object
     * @response `403` `void` The entity is not open to members joining
     * @response `404` `void` The entity does not exist
     * @response `409` `void` A membership record already exists for the entity and user
     */
    joinCreate: (id: string, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/CFPs/${id}/join`,
        method: 'POST',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name RequestCreate
     * @summary Creates a request to join an entity on behalf of the currently logged in user
     * @request POST:/CFPs/{id}/request
     * @deprecated
     * @response `200` `string` ID of the new invitation object
     * @response `403` `void` The entity is not open to members requesting to join
     * @response `404` `void` The entity does not exist
     * @response `409` `void` An invitation already exists for the entity and user
     */
    requestCreate: (id: string, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/CFPs/${id}/request`,
        method: 'POST',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name InviteIdCreateDetail
     * @summary Invite a user to an entity using their ID
     * @request POST:/CFPs/{id}/invite/id/{userId}
     * @response `200` `string` ID of the new invitation object
     * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity, or the user is inviting an owner without having the rights to manage owners
     * @response `404` `void` The entity or user do not exist
     * @response `409` `void` An invitation already exists for the entity and user
     */
    inviteIdCreateDetail: (id: string, userId: string, data: AccessLevel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/CFPs/${id}/invite/id/${userId}`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name InviteEmailCreateDetail
     * @summary Invite a user to an entity using their email
     * @request POST:/CFPs/{id}/invite/email/{userEmail}
     * @response `200` `string` ID of the new invitation object
     * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
     * @response `404` `void` No entity was found for that id
     */
    inviteEmailCreateDetail: (id: string, userEmail: string, data: AccessLevel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/CFPs/${id}/invite/email/${userEmail}`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name InviteEmailBatchCreate
     * @summary Invite a user to an entity using their email
     * @request POST:/CFPs/{id}/invite/email/batch
     * @deprecated
     * @response `200` `string` ID of the new invitation object
     * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
     * @response `404` `void` No entity was found for that id
     */
    inviteEmailBatchCreate: (id: string, data: string[], params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/CFPs/${id}/invite/email/batch`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name InvitesDetail
     * @summary List all pending invitations for a given entity
     * @request GET:/CFPs/{id}/invites
     * @response `200` `(InvitationModelUser)[]` A list of invitations
     * @response `404` `void` No entity was found for that id
     */
    invitesDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<InvitationModelUser[], void>({
        path: `/CFPs/${id}/invites`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name InvitesAcceptCreate
     * @summary Accept the invite on behalf of the currently logged-in user
     * @request POST:/CFPs/{id}/invites/{invitationId}/accept
     * @response `200` `void` The invitation has been accepted. The user is now a member of the project.
     * @response `403` `void` The current user does not have the right to accept the invitation
     * @response `404` `void` No entity was found for that id or the invitation does not exist
     */
    invitesAcceptCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/${id}/invites/${invitationId}/accept`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name InvitesRejectCreate
     * @summary Reject the invite on behalf of the currently logged-in user
     * @request POST:/CFPs/{id}/invites/{invitationId}/reject
     * @response `200` `void` The invitation has been rejected
     * @response `403` `void` The current user does not have the right to reject the invitation
     * @response `404` `void` No entity was found for that id or the invitation does not exist
     */
    invitesRejectCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/${id}/invites/${invitationId}/reject`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name InvitesCancelCreate
     * @summary Cancels an invite
     * @request POST:/CFPs/{id}/invites/{invitationId}/cancel
     * @response `200` `void` The invitation has been cancelled
     * @response `403` `void` The current user does not have the right to cancel the invitation
     * @response `404` `void` No entity was found for that id or the invitation does not exist
     */
    invitesCancelCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/${id}/invites/${invitationId}/cancel`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name InvitesResendCreate
     * @summary Resends an invite
     * @request POST:/CFPs/invites/{invitationId}/resend
     * @response `200` `void` The invitation has been resent
     * @response `400` `void` The invitation is a request and cannot be resent
     * @response `403` `void` The user does not have the right to resend the invitation
     * @response `404` `void` No entity was found for that id or the invitation does not exist
     */
    invitesResendCreate: (invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/invites/${invitationId}/resend`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name LeaveCreate
     * @summary Leaves an entity on behalf of the currently logged in user
     * @request POST:/CFPs/{id}/leave
     * @deprecated
     * @response `200` `void` The user has successfully left the entity
     * @response `404` `void` The entity does not exist or the user is not a member
     */
    leaveCreate: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/${id}/leave`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name OnboardingResponsesDetailDetail
     * @summary List onboarding responses for a community entity and a user
     * @request GET:/CFPs/{id}/onboardingResponses/{userId}
     * @deprecated
     * @response `200` `OnboardingQuestionnaireInstanceModel` onboarding questionnaire responses
     * @response `404` `void` No entity was found for that id or no user was found
     */
    onboardingResponsesDetailDetail: (id: string, userId: string, params: RequestParams = {}) =>
      this.request<OnboardingQuestionnaireInstanceModel, void>({
        path: `/CFPs/${id}/onboardingResponses/${userId}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name OnboardingResponsesCreate
     * @summary Posts onboarding responses for a community entity for the current user
     * @request POST:/CFPs/{id}/onboardingResponses
     * @deprecated
     * @response `200` `void` The onboarding questionnaire responses were saved successfully
     * @response `404` `void` No entity was found for that id
     */
    onboardingResponsesCreate: (
      id: string,
      data: OnboardingQuestionnaireInstanceUpsertModel,
      params: RequestParams = {},
    ) =>
      this.request<void, void>({
        path: `/CFPs/${id}/onboardingResponses`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name OnboardingCompletionCreate
     * @summary Records the completion of the onboarding workflow for a community entity for the current user
     * @request POST:/CFPs/{id}/onboardingCompletion
     * @deprecated
     * @response `200` `void` The onboarding completion was recorded successfully
     * @response `404` `void` No entity was found for that id
     */
    onboardingCompletionCreate: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/${id}/onboardingCompletion`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name MembersDetail
     * @summary List all members for an entity
     * @request GET:/CFPs/{id}/members
     * @response `200` `(MemberModel)[]` A list of members
     * @response `403` `void` The current user doesn't have sufficient rights to see the entity's members
     * @response `404` `void` No entity was found for that id
     */
    membersDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<MemberModel[], void>({
        path: `/CFPs/${id}/members`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name InvitationDetail
     * @summary Returns the pending invitation for an object for the current user
     * @request GET:/CFPs/{id}/invitation
     * @response `200` `InvitationModelEntity` A pending invitation
     * @response `404` `void` No invitation is pending or no entity was found for that id
     */
    invitationDetail: (id: string, params: RequestParams = {}) =>
      this.request<InvitationModelEntity, void>({
        path: `/CFPs/${id}/invitation`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name MembersUpdateDetail
     * @summary Updates a member's access level for an entity
     * @request PUT:/CFPs/{id}/members/{memberId}
     * @response `200` `void` The access level has been updated
     * @response `403` `void` The user does not have the right to set this access level for this member
     * @response `404` `void` No entity was found for that id or the user is not a member of that entity
     */
    membersUpdateDetail: (id: string, memberId: string, data: AccessLevel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/${id}/members/${memberId}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name MembersDeleteDetail
     * @summary Removes a member's access from an entity
     * @request DELETE:/CFPs/{id}/members/{memberId}
     * @response `200` `void` The member has been removed
     * @response `403` `void` The current user does not have the right to remove members from this entity
     * @response `404` `void` No entity was found for that id or the user is not a member of that entity
     */
    membersDeleteDetail: (id: string, memberId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/${id}/members/${memberId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name MembersContributionUpdate
     * @summary Updates a member's contribution
     * @request PUT:/CFPs/{id}/members/{memberId}/contribution
     * @response `200` `void` The contribution has been updated
     * @response `403` `void` The user does not have the right to set the contribution for other members
     * @response `404` `void` No entity was found for that id or the user is not a member of that entity
     */
    membersContributionUpdate: (id: string, memberId: string, data: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/${id}/members/${memberId}/contribution`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name MembersLabelsUpdate
     * @summary Updates a member's labels
     * @request PUT:/CFPs/{id}/members/{memberId}/labels
     * @response `200` `void` The labels have been updated
     * @response `403` `void` The current user does not have the right to set labels for members
     * @response `404` `void` No entity was found for that id or the user is not a member of that entity
     */
    membersLabelsUpdate: (id: string, memberId: string, data: string[], params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/${id}/members/${memberId}/labels`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name CommunityEntityInviteCreateDetail
     * @summary Invite a community entity to become affiliated to another entity
     * @request POST:/CFPs/{id}/communityEntityInvite/{targetEntityId}
     * @response `200` `string` ID of the new invitation object
     * @response `400` `void` An invitation cannot be extended to a community entity of the same type
     * @response `403` `void` The current user does not have the rights to invite entities to the specified entity
     * @response `404` `void` One of the entities does not exist
     * @response `409` `void` An invitation or affiliation already exists for the entities
     */
    communityEntityInviteCreateDetail: (id: string, targetEntityId: string, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/CFPs/${id}/communityEntityInvite/${targetEntityId}`,
        method: 'POST',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name CommunityEntityInvitesIncomingDetail
     * @summary List all pending invitations for a given entity
     * @request GET:/CFPs/{id}/communityEntityInvites/incoming
     * @response `200` `(CommunityEntityInvitationModelSource)[]` A list of invitations
     * @response `404` `void` No entity was found for that id
     */
    communityEntityInvitesIncomingDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityInvitationModelSource[], void>({
        path: `/CFPs/${id}/communityEntityInvites/incoming`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name CommunityEntityInvitesOutgoingDetail
     * @summary List all pending invitations for a given entity
     * @request GET:/CFPs/{id}/communityEntityInvites/outgoing
     * @response `200` `(CommunityEntityInvitationModelSource)[]` A list of invitations
     * @response `404` `void` No entity was found for that id
     */
    communityEntityInvitesOutgoingDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityInvitationModelSource[], void>({
        path: `/CFPs/${id}/communityEntityInvites/outgoing`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name CommunityEntityInvitesAcceptCreate
     * @summary Accept an invite
     * @request POST:/CFPs/{id}/communityEntityInvites/{invitationId}/accept
     * @response `200` `void` The invitation has been accepted
     * @response `403` `void` The current user does not have the right to accept the invitation
     * @response `404` `void` One of the entities does not exist or the invitation does not exist
     */
    communityEntityInvitesAcceptCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/${id}/communityEntityInvites/${invitationId}/accept`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name CommunityEntityInvitesRejectCreate
     * @summary Rejects an invite
     * @request POST:/CFPs/{id}/communityEntityInvites/{invitationId}/reject
     * @response `200` `void` The invitation has been rejected
     * @response `403` `void` The current user does not have the right to reject the invitation
     * @response `404` `void` One of the entities does not exist or the invitation does not exist
     */
    communityEntityInvitesRejectCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/${id}/communityEntityInvites/${invitationId}/reject`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name CommunityEntityInvitesCancelCreate
     * @summary Cancels an invite
     * @request POST:/CFPs/{id}/communityEntityInvites/{invitationId}/cancel
     * @response `200` `void` The invitation has been cancelled
     * @response `403` `void` The current user does not have the right to cancel the invitation
     * @response `404` `void` One of the entities does not exist or the invitation does not exist
     */
    communityEntityInvitesCancelCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/${id}/communityEntityInvites/${invitationId}/cancel`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name CommunityEntityRemoveCreate
     * @summary Removes an affiliation of an entity to another entity
     * @request POST:/CFPs/{id}/communityEntity/{targetEntityId}/remove
     * @response `200` `void` The entity affiliation has been removed successfully
     * @response `403` `void` The current user does not have the permissions to manage the community entity's affiliations
     * @response `404` `void` One of the entities does not exist or the entities are not affiliated
     */
    communityEntityRemoveCreate: (id: string, targetEntityId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/${id}/communityEntity/${targetEntityId}/remove`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name CommunityEntityLinkCreate
     * @summary Creates an affiliation of an entity to another entity. On the entity, the user must be admin or owner, or the toggle for members to allow creating community entities of the respective type must be on. The user must be admin or owner on the target entity.
     * @request POST:/CFPs/{id}/communityEntity/{targetEntityId}/link
     * @response `200` `void` The entity affiliation has been created successfully
     * @response `409` `void` One of the entities does not exist or the entities are already affiliated
     */
    communityEntityLinkCreate: (id: string, targetEntityId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/${id}/communityEntity/${targetEntityId}/link`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name EcosystemCommunityEntitiesDetail
     * @summary Lists all ecosystem containers (projects, communities and nodes) for the given community entity
     * @request GET:/CFPs/{id}/ecosystem/communityEntities
     * @deprecated
     * @response `200` `(EntityMiniModel)[]`
     * @response `403` `string` The current user does not have rights to view this community entity's contents
     * @response `404` `void` No community entity was found for that id
     */
    ecosystemCommunityEntitiesDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EntityMiniModel[], string | void>({
        path: `/CFPs/${id}/ecosystem/communityEntities`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name EcosystemUsersDetail
     * @summary Lists all ecosystem members for the given community entity
     * @request GET:/CFPs/{id}/ecosystem/users
     * @response `200` `(EntityMiniModel)[]`
     * @response `403` `string` The current user does not have rights to view this community entity's contents
     * @response `404` `void` No community entity was found for that id
     */
    ecosystemUsersDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EntityMiniModel[], string | void>({
        path: `/CFPs/${id}/ecosystem/users`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name NeedsCreate
     * @summary Adds a new need for the specified project
     * @request POST:/CFPs/{id}/needs
     * @deprecated
     * @response `200` `string` The ID of the new need
     * @response `403` `void` The current user doesn't have sufficient rights to add needs for the entity
     */
    needsCreate: (id: string, data: NeedUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/CFPs/${id}/needs`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name NeedsDetail
     * @summary Lists all needs for the specified community entity
     * @request GET:/CFPs/{id}/needs
     * @deprecated
     * @response `200` `(NeedModel)[]`
     * @response `403` `string` The current user does not have rights to view this community entity's contents
     * @response `404` `void` No community entity was found for that id
     */
    needsDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<NeedModel[], string | void>({
        path: `/CFPs/${id}/needs`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name NeedsDetailDetail
     * @summary Returns a single need
     * @request GET:/CFPs/needs/{needId}
     * @deprecated
     * @response `403` `string` The current user does not have rights to view this community entity's contents
     * @response `404` `void` No need was found for the need id
     */
    needsDetailDetail: (needId: string, params: RequestParams = {}) =>
      this.request<any, string | void>({
        path: `/CFPs/needs/${needId}`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name NeedsUpdateDetail
     * @summary Updates the need
     * @request PUT:/CFPs/needs/{needId}
     * @deprecated
     * @response `200` `void` The need was updated
     * @response `403` `void` The current user does not have rights to update needs on this community entity
     * @response `404` `void` No need was found for the need id
     */
    needsUpdateDetail: (needId: string, data: NeedUpsertModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/needs/${needId}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name NeedsDeleteDetail
     * @summary Deletes the specified need
     * @request DELETE:/CFPs/needs/{needId}
     * @deprecated
     * @response `200` `void` The need was deleted
     * @response `403` `void` The current user does not have rights to delete needs on this community entity
     * @response `404` `void` No need was found for the need id
     */
    needsDeleteDetail: (needId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/CFPs/needs/${needId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name EventsCreate
     * @summary Adds a new event for the specified entity.
     * @request POST:/CFPs/{id}/events
     * @deprecated
     * @response `200` `string` The event was created
     * @response `403` `void` The current user doesn't have sufficient rights to add events for the entity
     * @response `404` `void` No entity was found for that id
     */
    eventsCreate: (id: string, data: EventUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/CFPs/${id}/events`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name EventsDetail
     * @summary Lists events for the specified entity.
     * @request GET:/CFPs/{id}/events
     * @deprecated
     * @response `200` `(EventModel)[]` Event data
     * @response `403` `void` The current user doesn't have sufficient rights to view events for the entity
     * @response `404` `void` No entity was found for that id
     */
    eventsDetail: (
      id: string,
      query?: {
        /** @format date-time */
        from?: string;
        /** @format date-time */
        to?: string;
        tags?: EventTag[];
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EventModel[], void>({
        path: `/CFPs/${id}/events`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name ChannelsCreate
     * @summary Adds a new channel for the specified entity.
     * @request POST:/CFPs/{id}/channels
     * @deprecated
     * @response `200` `string` The channel was created
     * @response `403` `void` The current user doesn't have sufficient rights to add discussion channels for the entity
     * @response `404` `void` No entity was found for that id
     */
    channelsCreate: (id: string, data: ChannelUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/CFPs/${id}/channels`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CallForProposal
     * @name ChannelsDetail
     * @summary Lists channels for the specified entity.
     * @request GET:/CFPs/{id}/channels
     * @deprecated
     * @response `200` `(ChannelModel)[]` channel data
     * @response `403` `void` The current user doesn't have sufficient rights to view channels for the entity
     * @response `404` `void` No entity was found for that id
     */
    channelsDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ChannelModel[], void>({
        path: `/CFPs/${id}/channels`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),
  };
  channels = {
    /**
     * No description
     *
     * @tags Channel
     * @name ChannelsCreate
     * @summary Adds a new channel for the specified entity.
     * @request POST:/channels/{entityId}/channels
     * @response `200` `string` The channel was created
     * @response `403` `void` The current user doesn't have sufficient rights to add discussion channels for the entity
     */
    channelsCreate: (entityId: string, data: ChannelUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/channels/${entityId}/channels`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Channel
     * @name ChannelsDetail
     * @summary Lists channels for the specified entity.
     * @request GET:/channels/{entityId}/channels
     * @response `200` `(ChannelModel)[]` channel data
     * @response `403` `void` The current user doesn't have sufficient rights to view channels for the entity
     */
    channelsDetail: (
      entityId: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ChannelModel[], void>({
        path: `/channels/${entityId}/channels`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Channel
     * @name ChannelsDetailDetail
     * @summary Returns a single channel
     * @request GET:/channels/{id}
     * @response `200` `ChannelModel` The channel data
     * @response `403` `void` The current user doesn't have sufficient rights to see the channel
     * @response `404` `void` No channel was found for that id
     */
    channelsDetailDetail: (id: string, params: RequestParams = {}) =>
      this.request<ChannelModel, void>({
        path: `/channels/${id}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Channel
     * @name ChannelsUpdateDetail
     * @summary Updates the channel
     * @request PUT:/channels/{id}
     * @response `200` `ChannelModel` The channel was updated
     * @response `403` `void` The current user doesn't have sufficient rights to edit the channel
     * @response `404` `void` No channel was found for that id
     */
    channelsUpdateDetail: (id: string, data: ChannelUpsertModel, params: RequestParams = {}) =>
      this.request<ChannelModel, void>({
        path: `/channels/${id}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Channel
     * @name ChannelsDeleteDetail
     * @summary Deletes a channel
     * @request DELETE:/channels/{id}
     * @response `200` `void` The channel was deleted
     * @response `403` `void` The current user doesn't have sufficient rights to delete this channel
     * @response `404` `void` No channel was found for that id
     */
    channelsDeleteDetail: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/channels/${id}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Channel
     * @name DetailDetail
     * @summary Returns a single channel
     * @request GET:/channels/{id}/detail
     * @response `200` `ChannelDetailModel` The channel data including detailed path
     * @response `403` `void` The current user doesn't have sufficient rights to see the channel
     * @response `404` `void` No channel was found for that id
     */
    detailDetail: (id: string, params: RequestParams = {}) =>
      this.request<ChannelDetailModel, void>({
        path: `/channels/${id}/detail`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Channel
     * @name MembersDetail
     * @summary Returns members for the specified channel
     * @request GET:/channels/{id}/members
     * @response `200` `(MemberModel)[]` The channel members
     * @response `403` `void` The current user doesn't have sufficient rights to see members for the channel
     * @response `404` `void` No channel was found for that id
     */
    membersDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<MemberModel[], void>({
        path: `/channels/${id}/members`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Channel
     * @name MembersCreate
     * @summary Adds a batch of users for the specified channel
     * @request POST:/channels/{id}/members
     * @response `200` `void` The membership data was created
     * @response `403` `void` The current user doesn't have sufficient rights to add members to the channel or the user isn't allowed to invite admins
     * @response `404` `void` No channel was found for that id
     */
    membersCreate: (id: string, data: ChannelMemberUpsertModel[], params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/channels/${id}/members`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Channel
     * @name MembersUpdate
     * @summary Adds or updates a batch of users for the specified channel
     * @request PUT:/channels/{id}/members
     * @response `200` `void` The membership data was updated
     * @response `403` `void` The current user doesn't have sufficient rights to update roles for channel members
     * @response `404` `void` No channel was found for that id
     * @response `424` `void` The operation cannot be performed, since it downgrades the only remaining admin or owner of the channel to member
     */
    membersUpdate: (id: string, data: ChannelMemberUpsertModel[], params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/channels/${id}/members`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Channel
     * @name MembersDelete
     * @summary Removes a batch of users from the specified channel
     * @request DELETE:/channels/{id}/members
     * @response `200` `void` The membership data was updated
     * @response `403` `void` The current user doesn't have sufficient rights to remove members from the channel
     * @response `404` `void` No channel was found for that id
     */
    membersDelete: (id: string, data: string[], params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/channels/${id}/members`,
        method: 'DELETE',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Channel
     * @name UsersDetail
     * @summary Lists all eligible users to channel for a given channel, excluding users that have already joined the channel
     * @request GET:/channels/{id}/users
     * @response `200` `(UserMiniModel)[]` The eligible users
     * @response `403` `void` The current user does not have rights to view this channel's contents
     * @response `404` `void` No channel was found for that id
     */
    usersDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<UserMiniModel[], void>({
        path: `/channels/${id}/users`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Channel
     * @name MembersJoinCreate
     * @summary Adds the current user to the specified channel
     * @request POST:/channels/{id}/members/join
     * @response `200` `void` The membership record was created
     * @response `403` `void` The current user doesn't have sufficient rights to see the channel
     * @response `404` `void` No channel was found for that id
     */
    membersJoinCreate: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/channels/${id}/members/join`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Channel
     * @name MembersLeaveCreate
     * @summary Removes the current user from the specified channel
     * @request POST:/channels/{id}/members/leave
     * @response `200` `void` The membership record was deleted
     * @response `404` `void` No channel was found for that id
     * @response `409` `void` The current user isn't a member of this channel
     */
    membersLeaveCreate: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/channels/${id}/members/leave`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Channel
     * @name DocumentsDetail
     * @summary Returns documents for the channel
     * @request GET:/channels/{id}/documents
     * @response `200` `DocumentModelListPage` The document data
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the channel
     * @response `404` `void` No channel was found for that id
     */
    documentsDetail: (
      id: string,
      query?: {
        type?: DocumentFilter;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<DocumentModelListPage, void>({
        path: `/channels/${id}/documents`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),
  };
  communityEntities = {
    /**
     * No description
     *
     * @tags CommunityEntity
     * @name CommunityEntitiesDetailDetail
     * @summary Gets community entity
     * @request GET:/communityEntities/{id}
     * @response `200` `CommunityEntityMiniModel` The community entity data
     * @response `403` `void` The current user doesn't have sufficient rights to see the entity
     * @response `404` `void` No entity was found for that id
     */
    communityEntitiesDetailDetail: (id: string, params: RequestParams = {}) =>
      this.request<CommunityEntityMiniModel, void>({
        path: `/communityEntities/${id}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CommunityEntity
     * @name MembersDetail
     * @summary List all members for an entity
     * @request GET:/communityEntities/{id}/members
     * @response `200` `(MemberModel)[]` A list of members
     * @response `403` `void` The current user doesn't have sufficient rights to see the entity's members
     * @response `404` `void` No entity was found for that id
     */
    membersDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<MemberModel[], void>({
        path: `/communityEntities/${id}/members`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CommunityEntity
     * @name InviteBatchCreate
     * @summary Invite a batch of users to an entity
     * @request POST:/communityEntities/{id}/invite/batch
     * @response `200` `void` The users were successfully invited
     * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
     * @response `404` `void` No entity was found for that id
     */
    inviteBatchCreate: (id: string, data: InvitationUpsertModel[], params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/communityEntities/${id}/invite/batch`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags CommunityEntity
     * @name JoinKeyDetail
     * @summary Gets an invitation key for a community entity
     * @request GET:/communityEntities/{id}/join/key
     * @response `200` `string` The invitation key
     * @response `404` `void` No entity was found for that id
     */
    joinKeyDetail: (id: string, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/communityEntities/${id}/join/key`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CommunityEntity
     * @name JoinKeyCreateDetail
     * @summary Joins a community entity using an invitation key
     * @request POST:/communityEntities/{id}/join/key/{key}
     * @response `200` `FeedModel` The corresponding feed data
     * @response `404` `void` No matching invitation key found
     */
    joinKeyCreateDetail: (id: string, key: string, params: RequestParams = {}) =>
      this.request<FeedModel, void>({
        path: `/communityEntities/${id}/join/key/${key}`,
        method: 'POST',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CommunityEntity
     * @name OnboardingDetailDetail
     * @summary List onboarding responses for a community entity and a user
     * @request GET:/communityEntities/{id}/onboarding/{userId}
     * @response `200` `OnboardingQuestionnaireInstanceModel` onboarding questionnaire responses
     * @response `404` `void` No entity was found for that id or no user was found
     */
    onboardingDetailDetail: (id: string, userId: string, params: RequestParams = {}) =>
      this.request<OnboardingQuestionnaireInstanceModel, void>({
        path: `/communityEntities/${id}/onboarding/${userId}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CommunityEntity
     * @name OnboardingCreate
     * @summary Posts onboarding responses for a community entity for the current user
     * @request POST:/communityEntities/{id}/onboarding
     * @response `200` `void` The onboarding questionnaire responses were saved successfully
     * @response `404` `void` No entity was found for that id
     */
    onboardingCreate: (id: string, data: OnboardingQuestionnaireInstanceUpsertModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/communityEntities/${id}/onboarding`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags CommunityEntity
     * @name OnboardingCompleteCreate
     * @summary Records the completion of the onboarding workflow for a community entity for the current user
     * @request POST:/communityEntities/{id}/onboarding/complete
     * @response `200` `void` The onboarding completion was recorded successfully
     * @response `404` `void` No entity was found for that id
     */
    onboardingCompleteCreate: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/communityEntities/${id}/onboarding/complete`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CommunityEntity
     * @name LeaveCreate
     * @summary Leaves an entity on behalf of the currently logged in user
     * @request POST:/communityEntities/{id}/leave
     * @response `200` `void` The user has successfully left the entity
     * @response `404` `void` The entity does not exist or the user is not a member
     */
    leaveCreate: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/communityEntities/${id}/leave`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CommunityEntity
     * @name RequestCreate
     * @summary Creates a request to join an entity on behalf of the currently logged in user
     * @request POST:/communityEntities/{id}/request
     * @response `200` `string` ID of the new invitation object
     * @response `403` `void` The current user does not have the right to request to join the community entity
     * @response `404` `void` The entity does not exist
     * @response `409` `void` A request or invitation already exists for the entity and user
     */
    requestCreate: (id: string, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/communityEntities/${id}/request`,
        method: 'POST',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CommunityEntity
     * @name JoinCreate
     * @summary Joins an entity on behalf of the currently logged in user. Only works for community entities with the Open membership access level. Creates a membership record immediately, no invitation is created.
     * @request POST:/communityEntities/{id}/join
     * @response `200` `string` ID of the new membership object
     * @response `403` `void` The current user does not have the right to join the community entity
     * @response `404` `void` The entity does not exist
     * @response `409` `void` A request or invitation already exists for the entity and user
     */
    joinCreate: (id: string, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/communityEntities/${id}/join`,
        method: 'POST',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags CommunityEntity
     * @name InviteMassCreate
     * @summary Mass invite a batch of users to a set of community entities
     * @request POST:/communityEntities/invite/mass
     * @response `200` `void` The users were successfully invited
     * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
     * @response `404` `void` No entity was found for that id
     */
    inviteMassCreate: (data: InvitationMassUpsertModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/communityEntities/invite/mass`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),
  };
  documents = {
    /**
     * No description
     *
     * @tags Document
     * @name DocumentsCreate
     * @summary Adds a new document for the specified feed.
     * @request POST:/documents/{entityId}/documents
     * @response `200` `string` The document was created
     * @response `403` `void` The current user doesn't have sufficient rights to add documents for the entity
     */
    documentsCreate: (entityId: string, data: DocumentInsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/documents/${entityId}/documents`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Document
     * @name DocumentsDetail
     * @summary Lists all documents for the specified entity
     * @request GET:/documents/{entityId}/documents
     * @response `200` `(DocumentModel)[]` The document data
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
     */
    documentsDetail: (
      entityId: string,
      query?: {
        folderId?: string;
        type?: DocumentFilter;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<DocumentModel[], void>({
        path: `/documents/${entityId}/documents`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Document
     * @name DocumentsNewDetail
     * @summary Returns a value indicating whether or not there are new documents for a particular entity
     * @request GET:/documents/{entityId}/documents/new
     * @response `200` `boolean` True or false
     */
    documentsNewDetail: (entityId: string, params: RequestParams = {}) =>
      this.request<boolean, any>({
        path: `/documents/${entityId}/documents/new`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Document
     * @name DocumentsAllDetail
     * @summary Lists all documents and folders for the specified entity, not including file data
     * @request GET:/documents/{entityId}/documents/all
     * @response `200` `(DocumentOrFolderModel)[]` The document and folder data
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
     * @response `404` `void` No entity was found for that id
     */
    documentsAllDetail: (
      entityId: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<DocumentOrFolderModel[], void>({
        path: `/documents/${entityId}/documents/all`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Document
     * @name DocumentsAllNewDetail
     * @summary Returns a value indicating whether or not there are new documents for a particular entity
     * @request GET:/documents/{entityId}/documents/all/new
     * @response `200` `boolean` True or false
     */
    documentsAllNewDetail: (entityId: string, params: RequestParams = {}) =>
      this.request<boolean, any>({
        path: `/documents/${entityId}/documents/all/new`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Document
     * @name DocumentsDetailDetail
     * @summary Returns a single document
     * @request GET:/documents/{documentId}
     * @response `200` `DocumentModel` The document
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the parent entity
     * @response `404` `void` No document was found for that id
     */
    documentsDetailDetail: (documentId: string, params: RequestParams = {}) =>
      this.request<DocumentModel, void>({
        path: `/documents/${documentId}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Document
     * @name DocumentsUpdateDetail
     * @summary Updates the document
     * @request PUT:/documents/{documentId}
     * @response `200` `void` The document was updated
     * @response `403` `void` The current user doesn't have sufficient rights to edit documents for the entity
     * @response `404` `void` No document was found for that id
     */
    documentsUpdateDetail: (documentId: string, data: DocumentUpdateModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/documents/${documentId}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Document
     * @name DocumentsDeleteDetail
     * @summary Deletes the specified document
     * @request DELETE:/documents/{documentId}
     * @response `200` `void` The document was deleted
     * @response `403` `void` The current user doesn't have sufficient rights to delete the document
     * @response `404` `void` No document was found for that id
     */
    documentsDeleteDetail: (documentId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/documents/${documentId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Document
     * @name DownloadDetail
     * @summary Returns document data
     * @request GET:/documents/{documentId}/download
     * @response `200` `void` The document data as a file
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the parent entity
     * @response `404` `void` No document was found for that id
     */
    downloadDetail: (documentId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/documents/${documentId}/download`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Document
     * @name FoldersCreateDetail
     * @summary Adds a new folder for the specified feed.
     * @request POST:/documents/folders/{feedId}
     * @response `200` `string` The folder was created
     * @response `403` `void` The current user doesn't have sufficient rights to add folders for the feed
     */
    foldersCreateDetail: (feedId: string, data: FolderUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/documents/folders/${feedId}`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Document
     * @name FoldersDetailDetail
     * @summary Lists all folders for the specified feed
     * @request GET:/documents/folders/{feedId}
     * @response `200` `(FolderModel)[]` The folder data
     * @response `403` `void` The current user doesn't have sufficient rights to view folders for the feed
     */
    foldersDetailDetail: (
      feedId: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<FolderModel[], void>({
        path: `/documents/folders/${feedId}`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Document
     * @name FoldersDetailDetail2
     * @summary Lists all folders in a specified parent folder, in a specified feed
     * @request GET:/documents/folders/{feedId}/{parentFolderId}
     * @originalName foldersDetailDetail
     * @duplicate
     * @response `200` `(FolderModel)[]` The folder data
     * @response `403` `void` The current user doesn't have sufficient rights to view folders for the feed
     */
    foldersDetailDetail2: (
      feedId: string,
      parentFolderId: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<FolderModel[], void>({
        path: `/documents/folders/${feedId}/${parentFolderId}`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Document
     * @name FoldersUpdateDetail
     * @summary Updates the folder
     * @request PUT:/documents/folders/{folderId}
     * @response `200` `void` The folder was updated
     * @response `400` `void` Invalid folder setup specified
     * @response `403` `void` The current user doesn't have sufficient rights to edit folders for the feed
     * @response `404` `void` No folder was found for that id
     */
    foldersUpdateDetail: (folderId: string, data: FolderUpsertModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/documents/folders/${folderId}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Document
     * @name FoldersDeleteDetail
     * @summary Deletes the specified folder
     * @request DELETE:/documents/folders/{folderId}
     * @response `200` `void` The folder was deleted
     * @response `403` `void` The current user doesn't have sufficient rights to delete folders for the feed
     * @response `404` `void` No folder was found for that id
     */
    foldersDeleteDetail: (folderId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/documents/folders/${folderId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Document
     * @name ConvertPdfCreate
     * @request POST:/documents/convert/pdf
     * @response `200` `void` Success
     */
    convertPdfCreate: (data: DocumentConversionUpsertModel, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/documents/convert/pdf`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Document
     * @name ConvertPngCreate
     * @request POST:/documents/convert/png
     * @response `200` `void` Success
     */
    convertPngCreate: (data: DocumentConversionUpsertModel, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/documents/convert/png`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),
  };
  entities = {
    /**
     * No description
     *
     * @tags Entity
     * @name PermissionDetailDetail
     * @summary Determines whether the current user has a given permission on a specified object
     * @request GET:/entities/permission/{id}/{permission}
     * @deprecated
     * @response `200` `boolean` True or false
     * @response `404` `void` The given object was not found
     */
    permissionDetailDetail: (id: string, permission: Permission, params: RequestParams = {}) =>
      this.request<boolean, void>({
        path: `/entities/permission/${id}/${permission}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Entity
     * @name EntitiesDetailDetail
     * @summary Returns a single entity
     * @request GET:/entities/{id}
     * @response `200` `EntityMiniModel` Entity data
     * @response `404` `void` Not entity was found for given id
     */
    entitiesDetailDetail: (id: string, params: RequestParams = {}) =>
      this.request<EntityMiniModel, void>({
        path: `/entities/${id}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Entity
     * @name PermissionsDetail
     * @summary Loads permissions on an object for the current user
     * @request GET:/entities/{id}/permissions
     * @response `200` `(Permission)[]` Permission data
     */
    permissionsDetail: (id: string, params: RequestParams = {}) =>
      this.request<Permission[], any>({
        path: `/entities/${id}/permissions`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Entity
     * @name CommunityEntitiesList
     * @summary Lists all ecosystem containers (projects, communities and nodes) with a given permissioni
     * @request GET:/entities/communityEntities
     * @response `200` `(CommunityEntityMiniModel)[]`
     */
    communityEntitiesList: (
      query?: {
        /** ID of a given community entity */
        id?: string;
        /** Target permission */
        permission?: Permission;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModel[], any>({
        path: `/entities/communityEntities`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Entity
     * @name SearchTotalsList
     * @summary Lists the totals for a global search query
     * @request GET:/entities/search/totals
     * @response `200` `SearchResultGlobalModel` Search result totals
     */
    searchTotalsList: (
      query?: {
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<SearchResultGlobalModel, any>({
        path: `/entities/search/totals`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Entity
     * @name PathDetail
     * @summary Gets a path for an entity
     * @request GET:/entities/{id}/path
     * @response `200` `(EntityMiniModel)[]` Path data
     */
    pathDetail: (id: string, params: RequestParams = {}) =>
      this.request<EntityMiniModel[], any>({
        path: `/entities/${id}/path`,
        method: 'GET',
        format: 'json',
        ...params,
      }),
  };
  events = {
    /**
     * No description
     *
     * @tags Event
     * @name EventsCreate
     * @summary Adds a new event for the specified community entity.
     * @request POST:/events/{entityId}/events
     * @response `200` `string` The event was created
     * @response `403` `void` The current user doesn't have sufficient rights to add events for the entity
     * @response `404` `void` No entity was found for that id
     */
    eventsCreate: (entityId: string, data: EventUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/events/${entityId}/events`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name EventsDetail
     * @summary Lists all events for the specified entity
     * @request GET:/events/{entityId}/events
     * @response `200` `(EventModel)[]` The event data
     * @response `403` `void` The current user doesn't have sufficient rights to view events for the entity
     */
    eventsDetail: (
      entityId: string,
      query?: {
        status?: AttendanceStatus;
        /** @format date-time */
        from?: string;
        /** @format date-time */
        to?: string;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EventModel[], void>({
        path: `/events/${entityId}/events`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name EventsList
     * @summary List all accessible events for a given search query. Only events accessible to the currently logged in user will be returned
     * @request GET:/events
     * @response `200` `EventModelListPage` A list of user events
     */
    eventsList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EventModelListPage, any>({
        path: `/events`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name EventsDetailDetail
     * @summary Returns a single event
     * @request GET:/events/{id}
     * @response `403` `void` The current user doesn't have sufficient rights to see the event
     * @response `404` `void` No event was found for that id
     */
    eventsDetailDetail: (id: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/events/${id}`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name EventsUpdateDetail
     * @summary Updates the event
     * @request PUT:/events/{id}
     * @response `200` `void` The event was updated
     * @response `403` `void` The current user doesn't have sufficient rights to edit the event
     * @response `404` `void` No event was found for that id
     */
    eventsUpdateDetail: (id: string, data: EventUpsertModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/events/${id}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name EventsDeleteDetail
     * @summary Deletes an event
     * @request DELETE:/events/{id}
     * @response `200` `void` The event was deleted
     * @response `403` `void` The current user doesn't have sufficient rights to delete this event
     * @response `404` `void` No event was found for that id
     */
    eventsDeleteDetail: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/events/${id}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name EventsNewDetail
     * @summary Returns a value indicating whether or not there are new events for a particular entity
     * @request GET:/events/{entityId}/events/new
     * @response `200` `boolean` True or false
     */
    eventsNewDetail: (entityId: string, params: RequestParams = {}) =>
      this.request<boolean, any>({
        path: `/events/${entityId}/events/new`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name AttendancesDetail
     * @summary Returns attendances for the specified event
     * @request GET:/events/{id}/attendances
     * @response `200` `(EventAttendanceModel)[]` The event attendances
     * @response `403` `void` The current user doesn't have sufficient rights to see attendees for the event
     * @response `404` `void` No event was found for that id
     */
    attendancesDetail: (
      id: string,
      query?: {
        level?: AttendanceAccessLevel;
        status?: AttendanceStatus;
        type?: AttendanceType;
        labels?: string[];
        communityEntityTypes?: CommunityEntityType[];
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EventAttendanceModel[], void>({
        path: `/events/${id}/attendances`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name EcosystemCommunityEntitiesDetail
     * @summary Lists all ecosystem containers (projects, communities and nodes) for the given event
     * @request GET:/events/{id}/ecosystem/communityEntities
     * @response `200` `(EntityMiniModel)[]`
     * @response `403` `string` The current user does not have rights to view this event's contents
     * @response `404` `void` No event was found for that id
     */
    ecosystemCommunityEntitiesDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EntityMiniModel[], string | void>({
        path: `/events/${id}/ecosystem/communityEntities`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name EcosystemUsersDetail
     * @summary Lists all ecosystem members for the given event
     * @request GET:/events/{id}/ecosystem/users
     * @response `200` `(EntityMiniModel)[]`
     * @response `403` `string` The current user does not have rights to view this event's contents
     * @response `404` `void` No event was found for that id
     */
    ecosystemUsersDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EntityMiniModel[], string | void>({
        path: `/events/${id}/ecosystem/users`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name InviteCreate
     * @summary Invites the user, email address or community entity to the specified event
     * @request POST:/events/{id}/invite
     * @response `200` `void` The attendance was created
     * @response `400` `void` Either the invite is empty (neither user id, community entity id nor email is populated) or the event is private and the community entity id is popuated
     * @response `403` `void` The current user doesn't have sufficient rights to invite to the event
     * @response `404` `void` No event was found for that id
     * @response `409` `void` The user, email or community entity is already invited
     */
    inviteCreate: (id: string, data: EventAttendanceUpsertModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/events/${id}/invite`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name InviteBatchCreate
     * @summary Invites a batch of users, email addresses or community entities to the specified event
     * @request POST:/events/{id}/invite/batch
     * @response `200` `void` The attendances were created
     * @response `403` `void` The current user doesn't have sufficient rights to invite to the event
     * @response `404` `void` No event was found for that id
     */
    inviteBatchCreate: (id: string, data: EventAttendanceUpsertModel[], params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/events/${id}/invite/batch`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name InviteBatchCommunityEntityCreate
     * @summary Invites all members of a specific community entity or entities to an event
     * @request POST:/events/{id}/invite/batch/communityEntity
     * @deprecated
     * @response `200` `void` The attendances were created
     * @response `403` `void` The current user doesn't have sufficient rights to invite to the event or to manage one of the community entities
     * @response `404` `void` No event was found for that id
     */
    inviteBatchCommunityEntityCreate: (id: string, data: string[], params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/events/${id}/invite/batch/communityEntity`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name AttendCreate
     * @summary Invites the current user to the specified event
     * @request POST:/events/{id}/attend
     * @response `200` `void` The attendance was created
     * @response `403` `void` The current user doesn't have sufficient rights to see the event
     * @response `404` `void` No event was found for that id
     * @response `409` `void` The user is already invited
     */
    attendCreate: (
      id: string,
      query?: {
        originCommunityEntityId?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<void, void>({
        path: `/events/${id}/attend`,
        method: 'POST',
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name AttendEmailCreate
     * @summary Invites the current user to the specified event (via email)
     * @request POST:/events/{id}/attend/email
     * @response `200` `void` The attendance was created
     * @response `403` `void` The current user doesn't have sufficient rights to see the event
     * @response `404` `void` No event was found for that id
     * @response `409` `void` The email is already invited
     */
    attendEmailCreate: (id: string, data: EmailModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/events/${id}/attend/email`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name AttendancesAcceptCreate
     * @summary Accepts the event invitation on behalf of a user or community entity
     * @request POST:/events/attendances/{attendanceId}/accept
     * @response `200` `void` The invite was accepted
     * @response `403` `void` The current user doesn't have sufficient rights to accept the invite
     * @response `404` `void` No pending invitation was found for that id
     */
    attendancesAcceptCreate: (attendanceId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/events/attendances/${attendanceId}/accept`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name AttendancesRejectCreate
     * @summary Rejects the event invitation on behalf of a user or community entity
     * @request POST:/events/attendances/{attendanceId}/reject
     * @response `200` `void` The invite was rejected
     * @response `403` `void` The current user doesn't have sufficient rights to reject the invite
     * @response `404` `void` No pending invitation was found for that id
     */
    attendancesRejectCreate: (attendanceId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/events/attendances/${attendanceId}/reject`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name AttendancesDeleteDetail
     * @summary Removes an attendance
     * @request DELETE:/events/attendances/{attendanceId}
     * @response `200` `void` The attendance was deleted
     * @response `400` `void` You are trying to remove the last organizer
     * @response `403` `void` The current user doesn't have sufficient rights to delete attendees from the event
     * @response `404` `void` No attendance was found for that id
     */
    attendancesDeleteDetail: (attendanceId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/events/attendances/${attendanceId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name AttendancesBatchDelete
     * @summary Removes attendances in batch
     * @request DELETE:/events/attendances/batch
     * @response `200` `void` The attendances were deleted
     * @response `400` `void` You can only update attendee access level within one event
     * @response `403` `void` The current user doesn't have sufficient rights to delete attendees from the event
     */
    attendancesBatchDelete: (data: string[], params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/events/attendances/batch`,
        method: 'DELETE',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name AttendancesAccessLevelCreate
     * @summary Updates the access level of an attendance
     * @request POST:/events/attendances/{attendanceId}/accessLevel
     * @response `200` `void` The access level was set
     * @response `403` `void` The current user doesn't have sufficient rights to manage access level for the event
     * @response `404` `void` No attendance was found for that id
     */
    attendancesAccessLevelCreate: (
      attendanceId: string,
      data: EventAttendanceAccessLevelModel,
      params: RequestParams = {},
    ) =>
      this.request<void, void>({
        path: `/events/attendances/${attendanceId}/accessLevel`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name AttendancesAccessLevelBatchCreate
     * @summary Updates the access level of a batch of attendees
     * @request POST:/events/attendances/{attendanceId}/accessLevel/batch
     * @response `200` `void` The access level was set
     * @response `400` `void` You can only update attendee access level within one event
     * @response `403` `void` The current user doesn't have sufficient rights to manage attendee access level for the event
     */
    attendancesAccessLevelBatchCreate: (
      attendanceId: string,
      data: EventAttendanceLevelBatchModel,
      params: RequestParams = {},
    ) =>
      this.request<void, void>({
        path: `/events/attendances/${attendanceId}/accessLevel/batch`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name AttendancesLabelsCreate
     * @summary Updates the labels of an attendee
     * @request POST:/events/attendances/{attendanceId}/labels
     * @response `200` `void` The labels were set
     * @response `403` `void` The current user doesn't have sufficient rights to manage attendee labels for the event
     * @response `404` `void` No attendance was found for that id
     */
    attendancesLabelsCreate: (attendanceId: string, data: string[], params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/events/attendances/${attendanceId}/labels`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name AttendancesLabelsBatchCreate
     * @summary Updates the labels of a batch of attendees
     * @request POST:/events/attendances/{attendanceId}/labels/batch
     * @response `200` `void` The labels were set
     * @response `400` `void` You can only update attendee labels within one event
     * @response `403` `void` The current user doesn't have sufficient rights to manage attendee labels for the event
     */
    attendancesLabelsBatchCreate: (
      attendanceId: string,
      data: EventAttendanceLabelBatchModel,
      params: RequestParams = {},
    ) =>
      this.request<void, void>({
        path: `/events/attendances/${attendanceId}/labels/batch`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name DocumentsCreate
     * @summary Adds a new document for the specified event.
     * @request POST:/events/{id}/documents
     * @deprecated
     * @response `200` `string` The document was created
     * @response `403` `void` The current user doesn't have sufficient rights to add documents for the event
     * @response `404` `void` No event was found for that id
     */
    documentsCreate: (id: string, data: DocumentInsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/events/${id}/documents`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name DocumentsDetail
     * @summary Lists all documents for the specified event, not including file data
     * @request GET:/events/{id}/documents
     * @deprecated
     * @response `200` `string` Documents
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the event
     * @response `404` `void` No event was found for that id
     */
    documentsDetail: (
      id: string,
      query?: {
        folderId?: string;
        type?: DocumentFilter;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string, void>({
        path: `/events/${id}/documents`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name DocumentsDetailDetail
     * @summary Returns a single document, including the file represented as base64
     * @request GET:/events/{id}/documents/{documentId}
     * @deprecated
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the event
     * @response `404` `void` No event was found for that id or the document does not exist
     */
    documentsDetailDetail: (id: string, documentId: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/events/${id}/documents/${documentId}`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name DocumentsUpdateDetail
     * @summary Updates the title and description for the document
     * @request PUT:/events/{id}/documents/{documentId}
     * @deprecated
     * @response `200` `void` The document was updated
     * @response `403` `void` The current user doesn't have sufficient rights to edit documents for the event
     * @response `404` `void` No event was found for that id or the document does not exist
     */
    documentsUpdateDetail: (id: string, documentId: string, data: DocumentUpdateModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/events/${id}/documents/${documentId}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name DocumentsDeleteDetail
     * @summary Deletes the specified document
     * @request DELETE:/events/{id}/documents/{documentId}
     * @deprecated
     * @response `200` `void` The document was deleted
     * @response `403` `void` The current user doesn't have sufficient rights to delete the document
     * @response `404` `void` No event was found for that id or the document does not exist
     */
    documentsDeleteDetail: (id: string, documentId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/events/${id}/documents/${documentId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name DocumentsDraftDetail
     * @summary Returns a draft document for the specified event
     * @request GET:/events/{id}/documents/draft
     * @deprecated
     * @response `204` `void` No draft document was found for the event
     * @response `404` `void` No event was found for the specified id
     */
    documentsDraftDetail: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/events/${id}/documents/draft`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Event
     * @name MessageCreate
     * @summary Sends a message to selected attendees
     * @request POST:/events/{id}/message
     * @response `200` `void` The message was successfully sent
     * @response `403` `void` The current user does not have the rights to send messages to event attendees
     * @response `404` `void` No event was found for that id
     */
    messageCreate: (id: string, data: MessageModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/events/${id}/message`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),
  };
  feed = {
    /**
     * No description
     *
     * @tags Feed
     * @name FeedCreateDetail
     * @request POST:/feed/{feedId}
     * @response `200` `void` Success
     */
    feedCreateDetail: (feedId: string, data: ContentEntityUpsertModel, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/feed/${feedId}`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name FeedDetailDetail
     * @request GET:/feed/{feedId}
     * @deprecated
     * @response `200` `DiscussionModel` Discussion data, including stats for the currently logged in user
     * @response `403` `void` The current user doesn't have sufficient rights to view the feed
     */
    feedDetailDetail: (
      feedId: string,
      query?: {
        type?: ContentEntityType;
        filter?: ContentEntityFilter;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<DiscussionModel, void>({
        path: `/feed/${feedId}`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name PermissionsDetail
     * @request GET:/feed/{feedId}/permissions
     * @response `200` `(Permission)[]` Discussion permissions for the currently logged in user
     * @response `403` `void` The current user doesn't have sufficient rights to view the feed
     */
    permissionsDetail: (feedId: string, params: RequestParams = {}) =>
      this.request<Permission[], void>({
        path: `/feed/${feedId}/permissions`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name MentionsDetail
     * @request GET:/feed/{feedId}/mentions
     * @deprecated
     * @response `200` `(ContentEntityModel)[]` Discussion mentions for the currently logged in user
     * @response `403` `void` The current user doesn't have sufficient rights to view the feed
     */
    mentionsDetail: (
      feedId: string,
      query?: {
        type?: ContentEntityType;
        /** @format int32 */
        offset?: number;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ContentEntityModel[], void>({
        path: `/feed/${feedId}/mentions`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ThreadsDetail
     * @request GET:/feed/{feedId}/threads
     * @deprecated
     * @response `200` `(ContentEntityModel)[]` Discussion threads for the currently logged in user
     * @response `403` `void` The current user doesn't have sufficient rights to view the feed
     */
    threadsDetail: (
      feedId: string,
      query?: {
        type?: ContentEntityType;
        /** @format int32 */
        offset?: number;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ContentEntityModel[], void>({
        path: `/feed/${feedId}/threads`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name PostsListDetail
     * @request GET:/feed/{feedId}/posts/list
     * @response `200` `ContentEntityModelListPage` Posts
     * @response `403` `void` The current user doesn't have sufficient rights to view the feed
     */
    postsListDetail: (
      feedId: string,
      query?: {
        type?: ContentEntityType;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ContentEntityModelListPage, void>({
        path: `/feed/${feedId}/posts/list`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name MentionsListDetail
     * @request GET:/feed/{feedId}/mentions/list
     * @response `200` `ContentEntityModelListPage` Posts with mentions
     * @response `403` `void` The current user doesn't have sufficient rights to view the feed
     */
    mentionsListDetail: (
      feedId: string,
      query?: {
        type?: ContentEntityType;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ContentEntityModelListPage, void>({
        path: `/feed/${feedId}/mentions/list`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ThreadsListDetail
     * @request GET:/feed/{feedId}/threads/list
     * @response `200` `ContentEntityModelListPage` Posts with unread threads
     * @response `403` `void` The current user doesn't have sufficient rights to view the feed
     */
    threadsListDetail: (
      feedId: string,
      query?: {
        type?: ContentEntityType;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ContentEntityModelListPage, void>({
        path: `/feed/${feedId}/threads/list`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name NodePostsListDetail
     * @request GET:/feed/node/{nodeId}/posts/list
     * @response `200` `(DiscussionItemModel)[]` Posts
     */
    nodePostsListDetail: (
      nodeId: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<DiscussionItemModel[], any>({
        path: `/feed/node/${nodeId}/posts/list`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name NodeMentionsListDetail
     * @request GET:/feed/node/{nodeId}/mentions/list
     * @response `200` `(DiscussionItemModel)[]` Posts with mentions
     */
    nodeMentionsListDetail: (
      nodeId: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<DiscussionItemModel[], any>({
        path: `/feed/node/${nodeId}/mentions/list`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name NodeThreadsListDetail
     * @request GET:/feed/node/{nodeId}/threads/list
     * @response `200` `(DiscussionItemModel)[]` Posts with unread threads
     */
    nodeThreadsListDetail: (
      nodeId: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<DiscussionItemModel[], any>({
        path: `/feed/node/${nodeId}/threads/list`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name GetFeed
     * @summary Returns information on whether there is a new post, thread or mention on a feed
     * @request GET:/feed/{feedId}/new
     * @response `200` `boolean` True or false
     */
    getFeed: (feedId: string, params: RequestParams = {}) =>
      this.request<boolean, any>({
        path: `/feed/${feedId}/new`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name OpenedCreate
     * @summary Records the user having opened the specified feed
     * @request POST:/feed/{feedId}/opened
     * @response `200` `void` The feed was marked as opened
     */
    openedCreate: (feedId: string, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/feed/${feedId}/opened`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name SeenCreate
     * @summary Records the user having read the specified feed
     * @request POST:/feed/{feedId}/seen
     * @response `200` `void` All new posts and mentions were marked as seen
     * @response `204` `void` There was nothing to mark as seen
     */
    seenCreate: (feedId: string, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/feed/${feedId}/seen`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name StarCreate
     * @summary Makes the feed starred for a user
     * @request POST:/feed/{feedId}/star
     * @response `404` `void` The feed has not yet been read by the user or is not accessible to them
     */
    starCreate: (feedId: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/feed/${feedId}/star`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name StarDelete
     * @summary Makes the feed unstarred for a user
     * @request DELETE:/feed/{feedId}/star
     * @response `404` `void` The feed has not yet been read by the user or is not accessible to them
     */
    starDelete: (feedId: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/feed/${feedId}/star`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ActiveCreate
     * @summary Makes the feed unarchived for a user
     * @request POST:/feed/{feedId}/active
     * @response `404` `void` The feed has not yet been read by the user or is not accessible to them
     */
    activeCreate: (feedId: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/feed/${feedId}/active`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ActiveDelete
     * @summary Makes the feed archived for a user
     * @request DELETE:/feed/{feedId}/active
     * @response `404` `void` The feed has not yet been read by the user or is not accessible to them
     */
    activeDelete: (feedId: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/feed/${feedId}/active`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesFeedDetail
     * @request GET:/feed/contentEntities/{contentEntityId}/feed
     * @response `200` `FeedModel` The feed data
     * @response `403` `void` The current user does not have the rights to view the content entity
     * @response `404` `void` No content entity was found for that id
     */
    contentEntitiesFeedDetail: (contentEntityId: string, params: RequestParams = {}) =>
      this.request<FeedModel, void>({
        path: `/feed/contentEntities/${contentEntityId}/feed`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesDetailDetail
     * @request GET:/feed/contentEntities/{contentEntityId}
     * @response `200` `ContentEntityModel` The content entity data
     * @response `403` `void` The current user does not have the rights to view the content entity
     * @response `404` `void` No content entity was found for that id
     */
    contentEntitiesDetailDetail: (contentEntityId: string, params: RequestParams = {}) =>
      this.request<ContentEntityModel, void>({
        path: `/feed/contentEntities/${contentEntityId}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesUpdateDetail
     * @request PUT:/feed/contentEntities/{contentEntityId}
     * @response `200` `void` Success
     */
    contentEntitiesUpdateDetail: (
      contentEntityId: string,
      data: ContentEntityUpsertModel,
      params: RequestParams = {},
    ) =>
      this.request<void, any>({
        path: `/feed/contentEntities/${contentEntityId}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesPartialUpdateDetail
     * @request PATCH:/feed/contentEntities/{contentEntityId}
     * @deprecated
     * @response `200` `void` Success
     */
    contentEntitiesPartialUpdateDetail: (
      contentEntityId: string,
      data: ContentEntityPatchModel,
      params: RequestParams = {},
    ) =>
      this.request<void, any>({
        path: `/feed/contentEntities/${contentEntityId}`,
        method: 'PATCH',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesDeleteDetail
     * @request DELETE:/feed/contentEntities/{contentEntityId}
     * @response `200` `void` Success
     */
    contentEntitiesDeleteDetail: (contentEntityId: string, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/feed/contentEntities/${contentEntityId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesSeenCreate
     * @summary Records the user having read a post
     * @request POST:/feed/contentEntities/{contentEntityId}/seen
     * @response `200` `void` All new replies and mentions were marked as seen
     * @response `204` `void` There was nothing to mark as seen
     * @response `404` `void` No content entity was found for that id
     */
    contentEntitiesSeenCreate: (contentEntityId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/feed/contentEntities/${contentEntityId}/seen`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesSeenCreate2
     * @summary Records the user having read the specific content entities
     * @request POST:/feed/contentEntities/seen
     * @deprecated
     * @originalName contentEntitiesSeenCreate
     * @duplicate
     * @response `200` `void` The content entities were marked as seen
     * @response `404` `void` No content entity was found for one of the ids
     */
    contentEntitiesSeenCreate2: (data: string[], params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/feed/contentEntities/seen`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesReactionsCreate
     * @summary Creates or replaces a user's reaction to a content entity
     * @request POST:/feed/contentEntities/{contentEntityId}/reactions
     * @response `200` `string` Reaction created or replaced
     * @response `403` `void` The current user does not have the rights to view the feed
     * @response `404` `void` Content entity not found
     */
    contentEntitiesReactionsCreate: (contentEntityId: string, data: ReactionUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/feed/contentEntities/${contentEntityId}/reactions`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesReactionsDetail
     * @summary Lists all reactions to a content entity
     * @request GET:/feed/contentEntities/{contentEntityId}/reactions
     * @response `200` `(ReactionModel)[]` Reaction data
     * @response `403` `void` The current user does not have the rights to view the feed
     * @response `404` `void` Content entity not found
     */
    contentEntitiesReactionsDetail: (contentEntityId: string, params: RequestParams = {}) =>
      this.request<ReactionModel[], void>({
        path: `/feed/contentEntities/${contentEntityId}/reactions`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesReactionsDeleteDetail
     * @summary Removes a user's reaction to a content entity
     * @request DELETE:/feed/contentEntities/{contentEntityId}/reactions/{id}
     * @response `200` `void` The reaction was deleted
     * @response `403` `void` The current user does not have the rights to delete the reaction
     * @response `404` `void` Reaction not found
     */
    contentEntitiesReactionsDeleteDetail: (contentEntityId: string, id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/feed/contentEntities/${contentEntityId}/reactions/${id}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesCommentsCreate
     * @request POST:/feed/contentEntities/{contentEntityId}/comments
     * @response `200` `void` Success
     */
    contentEntitiesCommentsCreate: (contentEntityId: string, data: CommentUpsertModel, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/feed/contentEntities/${contentEntityId}/comments`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesCommentsDetail
     * @request GET:/feed/contentEntities/{contentEntityId}/comments
     * @deprecated
     * @response `200` `(CommentModel)[]` The comment data
     * @response `403` `void` The current user doesn't have sufficient rights to view discussions for the parent entity
     * @response `404` `void` The feed, entity or content entity does not exist
     */
    contentEntitiesCommentsDetail: (
      contentEntityId: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommentModel[], void>({
        path: `/feed/contentEntities/${contentEntityId}/comments`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesCommentsListDetail
     * @request GET:/feed/contentEntities/{contentEntityId}/comments/list
     * @response `200` `CommentModelListPage` The comment data
     * @response `403` `void` The current user doesn't have sufficient rights to view discussions for the parent entity
     * @response `404` `void` The content entity does not exist
     */
    contentEntitiesCommentsListDetail: (
      contentEntityId: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommentModelListPage, void>({
        path: `/feed/contentEntities/${contentEntityId}/comments/list`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesCommentsUpdateDetail
     * @request PUT:/feed/contentEntities/{contentEntityId}/comments/{id}
     * @response `200` `void` Success
     */
    contentEntitiesCommentsUpdateDetail: (
      contentEntityId: string,
      id: string,
      data: CommentUpsertModel,
      params: RequestParams = {},
    ) =>
      this.request<void, any>({
        path: `/feed/contentEntities/${contentEntityId}/comments/${id}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesCommentsPartialUpdateDetail
     * @request PATCH:/feed/contentEntities/{contentEntityId}/comments/{id}
     * @deprecated
     * @response `200` `void` Success
     */
    contentEntitiesCommentsPartialUpdateDetail: (
      contentEntityId: string,
      id: string,
      data: CommentPatchModel,
      params: RequestParams = {},
    ) =>
      this.request<void, any>({
        path: `/feed/contentEntities/${contentEntityId}/comments/${id}`,
        method: 'PATCH',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesCommentsDeleteDetail
     * @request DELETE:/feed/contentEntities/{contentEntityId}/comments/{id}
     * @response `200` `void` Success
     */
    contentEntitiesCommentsDeleteDetail: (contentEntityId: string, id: string, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/feed/contentEntities/${contentEntityId}/comments/${id}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesCommentsSeenCreate
     * @summary Records the user having read the specific comments under a post
     * @request POST:/feed/contentEntities/comments/seen
     * @deprecated
     * @response `200` `void` All comments were marked as seen
     */
    contentEntitiesCommentsSeenCreate: (data: string[], params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/feed/contentEntities/comments/seen`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name CommentsSeenCreate
     * @summary Records the user having read the specific comments under a post
     * @request POST:/feed/comments/seen
     * @deprecated
     * @response `200` `void` All comments were marked as seen
     */
    commentsSeenCreate: (data: string[], params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/feed/comments/seen`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name CommentsReactionsCreate
     * @summary Creates or replaces a user's reaction to a comment
     * @request POST:/feed/comments/{commentId}/reactions
     * @response `200` `string` Reaction created or replaced
     * @response `403` `void` The current user does not have the rights to view the feed
     * @response `404` `void` Comment not found
     */
    commentsReactionsCreate: (commentId: string, data: ReactionUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/feed/comments/${commentId}/reactions`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name CommentsReactionsDetail
     * @summary Lists all reactions to a comment
     * @request GET:/feed/comments/{commentId}/reactions
     * @response `200` `(ReactionModel)[]` Reaction data
     * @response `403` `void` The current user does not have the rights to view the feed
     * @response `404` `void` Comment not found
     */
    commentsReactionsDetail: (commentId: string, params: RequestParams = {}) =>
      this.request<ReactionModel[], void>({
        path: `/feed/comments/${commentId}/reactions`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name CommentsReactionsDeleteDetail
     * @summary Removes a user's reaction to a comment
     * @request DELETE:/feed/comments/{commentId}/reactions/{id}
     * @response `200` `void` The reaction was deleted
     * @response `403` `void` The current user does not have the rights to delete the reaction
     * @response `404` `void` Reaction not found
     */
    commentsReactionsDeleteDetail: (commentId: string, id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/feed/comments/${commentId}/reactions/${id}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesDocumentsCreate
     * @summary Adds a new document for the specified content entity.
     * @request POST:/feed/contentEntities/{contentEntityId}/documents
     * @response `200` `string` The document was created
     * @response `403` `void` The current user doesn't have sufficient rights to add documents for the content entity
     * @response `404` `void` No content entity was found for that id or the feed id is incorrect
     */
    contentEntitiesDocumentsCreate: (contentEntityId: string, data: DocumentInsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/feed/contentEntities/${contentEntityId}/documents`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesDocumentsUpdateDetail
     * @summary Updates the title and description for the document
     * @request PUT:/feed/contentEntities/{contentEntityId}/documents/{documentId}
     * @deprecated
     * @response `200` `void` The document was updated
     * @response `403` `void` The current user doesn't have sufficient rights to edit documents for the entity
     * @response `404` `void` No content entity was found for that id, the feed id is incorrect or the document does not exist
     */
    contentEntitiesDocumentsUpdateDetail: (
      contentEntityId: string,
      documentId: string,
      data: DocumentUpdateModel,
      params: RequestParams = {},
    ) =>
      this.request<void, void>({
        path: `/feed/contentEntities/${contentEntityId}/documents/${documentId}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesDocumentsDeleteDetail
     * @summary Deletes the specified document
     * @request DELETE:/feed/contentEntities/{contentEntityId}/documents/{documentId}
     * @response `200` `void` The document was deleted
     * @response `403` `void` The current user doesn't have sufficient rights to delete the document
     * @response `404` `void` No content entity was found for that id or the document does not exist
     */
    contentEntitiesDocumentsDeleteDetail: (contentEntityId: string, documentId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/feed/contentEntities/${contentEntityId}/documents/${documentId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ContentEntitiesCommentsDocumentsDeleteDetail
     * @summary Deletes the specified document
     * @request DELETE:/feed/contentEntities/{contentEntityId}/comments/{commentId}/documents/{documentId}
     * @deprecated
     * @response `200` `void` The document was deleted
     * @response `403` `void` The current user doesn't have sufficient rights to delete the document
     * @response `404` `void` No content entity was found for that id, no comment was found for that id or the document does not exist
     */
    contentEntitiesCommentsDocumentsDeleteDetail: (
      contentEntityId: string,
      commentId: string,
      documentId: string,
      params: RequestParams = {},
    ) =>
      this.request<void, void>({
        path: `/feed/contentEntities/${contentEntityId}/comments/${commentId}/documents/${documentId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name DraftDetailDetail
     * @summary Fetches a draft for a channel, feed or post id
     * @request GET:/feed/draft/{entityId}
     * @response `200` `string` The draft text
     */
    draftDetailDetail: (entityId: string, params: RequestParams = {}) =>
      this.request<string, any>({
        path: `/feed/draft/${entityId}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name DraftCreateDetail
     * @summary Creates or updates a draft for a channel, feed or post id
     * @request POST:/feed/draft/{entityId}
     * @response `200` `void` The draft was created or updated
     */
    draftCreateDetail: (entityId: string, data: string, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/feed/draft/${entityId}`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name NodesList
     * @summary Returns a list of nodes with metadata
     * @request GET:/feed/nodes
     * @response `200` `(NodeFeedDataModel)[]` Node metadata
     */
    nodesList: (params: RequestParams = {}) =>
      this.request<NodeFeedDataModel[], any>({
        path: `/feed/nodes`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name NodesDetailDetail
     * @summary Returns a node with metadata
     * @request GET:/feed/nodes/{id}
     * @response `200` `NodeFeedDataModel` Node metadata
     */
    nodesDetailDetail: (id: string, params: RequestParams = {}) =>
      this.request<NodeFeedDataModel, any>({
        path: `/feed/nodes/${id}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name UsersDetail
     * @summary List all users for a given feed
     * @request GET:/feed/{feedId}/users
     * @response `200` `(UserMiniModel)[]` A list of user models
     * @response `403` `void` The current user doesn't have sufficient rights to list the feed's users
     */
    usersDetail: (
      feedId: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<UserMiniModel[], void>({
        path: `/feed/${feedId}/users`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name UsersAutocompleteDetail
     * @summary List all users for a given feed
     * @request GET:/feed/{feedId}/users/autocomplete
     * @response `200` `(UserMiniModel)[]` A list of user models
     * @response `403` `void` The current user doesn't have sufficient rights to list the feed's users
     */
    usersAutocompleteDetail: (
      feedId: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<UserMiniModel[], void>({
        path: `/feed/${feedId}/users/autocomplete`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name IntegrationsCreate
     * @summary Adds a feed integration to the specified feed
     * @request POST:/feed/{feedId}/integrations
     * @response `200` `string` The feed integration id
     * @response `403` `void` The current user doesn't have sufficient rights to add integrations to the feed
     * @response `404` `void` The operation failed validation; this may be because the source id does not exist, or it's private and the access token needs to be supplied
     * @response `409` `void` The integration already exists for the feed
     */
    integrationsCreate: (feedId: string, data: FeedIntegrationUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/feed/${feedId}/integrations`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name IntegrationsDetail
     * @summary Lists feed integrations for the specified feed
     * @request GET:/feed/{feedId}/integrations
     * @response `200` `(FeedIntegrationModel)[]` The feed integration data
     * @response `403` `void` The current user doesn't have sufficient rights to view the feed
     */
    integrationsDetail: (
      feedId: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<FeedIntegrationModel[], void>({
        path: `/feed/${feedId}/integrations`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name IntegrationsTokenCreate
     * @summary Exchanges an authorization code for an access token
     * @request POST:/feed/integrations/token
     * @response `200` `string` The access token
     * @response `403` `void` The operation failed
     */
    integrationsTokenCreate: (data: FeedIntegrationTokenModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/feed/integrations/token`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name IntegrationsOptionsDetailDetail
     * @summary Lists feed integration options for a specific feed integration type
     * @request GET:/feed/integrations/options/{feedIntegrationType}
     * @response `200` `(string)[]` The options
     */
    integrationsOptionsDetailDetail: (feedIntegrationType: FeedIntegrationType, params: RequestParams = {}) =>
      this.request<string[], any>({
        path: `/feed/integrations/options/${feedIntegrationType}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name IntegrationsDeleteDetail
     * @summary Removes a feed integration
     * @request DELETE:/feed/integrations/{id}
     * @response `200` `void` The feed integration was removed
     * @response `403` `void` The current user doesn't have sufficient rights to remove integrations from the feed
     * @response `404` `void` No integration was found for that id
     */
    integrationsDeleteDetail: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/feed/integrations/${id}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ReportContentEntityCreateDetail
     * @summary Reports a content entity
     * @request POST:/feed/report/contentEntity/{id}
     * @response `200` `void` Successfully reported a post
     */
    reportContentEntityCreateDetail: (id: string, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/feed/report/contentEntity/${id}`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Feed
     * @name ReportCommentCreateDetail
     * @summary Reports a content entity
     * @request POST:/feed/report/comment/{id}
     * @response `200` `void` Successfully reported a comment
     */
    reportCommentCreateDetail: (id: string, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/feed/report/comment/${id}`,
        method: 'POST',
        ...params,
      }),
  };
  images = {
    /**
     * No description
     *
     * @tags Image
     * @name ImagesCreate
     * @request POST:/images
     * @response `200` `ImageInsertResultModel` Image data
     */
    imagesCreate: (data: ImageInsertModel, params: RequestParams = {}) =>
      this.request<ImageInsertResultModel, any>({
        path: `/images`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Image
     * @name FullDetail
     * @request GET:/images/{id}/full
     * @response `200` `void` Success
     */
    fullDetail: (id: string, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/images/${id}/full`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Image
     * @name FullModelDetail
     * @request GET:/images/{id}/full/model
     * @response `200` `ImageModel` Image data
     */
    fullModelDetail: (id: string, params: RequestParams = {}) =>
      this.request<ImageModel, any>({
        path: `/images/${id}/full/model`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Image
     * @name GetImages
     * @request GET:/images/{id}/tn
     * @response `200` `void` Success
     */
    getImages: (id: string, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/images/${id}/tn`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Image
     * @name TnModelDetail
     * @request GET:/images/{id}/tn/model
     * @response `200` `ImageModel` Image data
     */
    tnModelDetail: (id: string, params: RequestParams = {}) =>
      this.request<ImageModel, any>({
        path: `/images/${id}/tn/model`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Image
     * @name ConvertCreate
     * @request POST:/images/convert
     * @response `200` `void` Success
     */
    convertCreate: (data: ImageConversionUpsertModel, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/images/convert`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),
  };
  needs = {
    /**
     * No description
     *
     * @tags Need
     * @name NeedsCreate
     * @summary Adds a new need for the specified feed.
     * @request POST:/needs/{entityId}/needs
     * @response `200` `string` The need was created
     * @response `403` `void` The current user doesn't have sufficient rights to add need for the entity
     */
    needsCreate: (entityId: string, data: NeedUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/needs/${entityId}/needs`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Need
     * @name NeedsDetail
     * @summary Lists all needs for the specified entity
     * @request GET:/needs/{entityId}/needs
     * @response `200` `(NeedModel)[]` The need data
     * @response `403` `void` The current user doesn't have sufficient rights to view needs for the entity
     */
    needsDetail: (
      entityId: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<NeedModel[], void>({
        path: `/needs/${entityId}/needs`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Need
     * @name NeedsList
     * @summary Lists all needs
     * @request GET:/needs
     * @response `200` `NeedModelListPage` A list of needs matching the search query visible to the current user
     */
    needsList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<NeedModelListPage, any>({
        path: `/needs`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Need
     * @name NeedsDetailDetail
     * @summary Returns a single need
     * @request GET:/needs/{id}
     * @response `200` `NeedModel`
     * @response `403` `void` The current user doesn't have sufficient rights to see the need
     * @response `404` `void` No need was found for that id
     */
    needsDetailDetail: (id: string, params: RequestParams = {}) =>
      this.request<NeedModel, void>({
        path: `/needs/${id}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Need
     * @name NeedsUpdateDetail
     * @summary Updates the need
     * @request PUT:/needs/{id}
     * @response `200` `void` The need was updated
     * @response `403` `void` The current user doesn't have sufficient rights to edit the need
     * @response `404` `void` No need was found for that id
     */
    needsUpdateDetail: (id: string, data: NeedUpsertModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/needs/${id}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Need
     * @name NeedsDeleteDetail
     * @summary Deletes a need
     * @request DELETE:/needs/{id}
     * @response `200` `void` The need was deleted
     * @response `403` `void` The current user doesn't have sufficient rights to delete this need
     * @response `404` `void` No need was found for that id
     */
    needsDeleteDetail: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/needs/${id}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Need
     * @name NeedsNewDetail
     * @summary Returns a value indicating whether or not there are new needs for a particular entity
     * @request GET:/needs/{entityId}/needs/new
     * @response `200` `boolean` True or false
     */
    needsNewDetail: (entityId: string, params: RequestParams = {}) =>
      this.request<boolean, any>({
        path: `/needs/${entityId}/needs/new`,
        method: 'GET',
        format: 'json',
        ...params,
      }),
  };
  nodes = {
    /**
     * No description
     *
     * @tags Node
     * @name NodesCreate
     * @summary Create a new node. The current user becomes a member of the node with the Owner role
     * @request POST:/nodes
     * @response `200` `void` Success
     */
    nodesCreate: (data: NodeUpsertModel, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/nodes`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name NodesList
     * @summary List all accessible nodes for a given search query. Only nodes accessible to the currently logged in user will be returned
     * @request GET:/nodes
     * @response `200` `CommunityEntityMiniModelListPage`
     */
    nodesList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModelListPage, any>({
        path: `/nodes`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name NodesDetailDetail
     * @summary Returns a node
     * @request GET:/nodes/{id}
     * @response `200` `NodeModel` The node data
     * @response `404` `void` No node was found for that id
     */
    nodesDetailDetail: (id: string, params: RequestParams = {}) =>
      this.request<NodeModel, void>({
        path: `/nodes/${id}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name NodesPartialUpdateDetail
     * @summary Patches the specified node
     * @request PATCH:/nodes/{id}
     * @response `200` `string` The ID of the entity
     * @response `403` `void` The current user does not have rights to edit this node
     * @response `404` `void` No node was found for that id
     */
    nodesPartialUpdateDetail: (id: string, data: NodePatchModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/nodes/${id}`,
        method: 'PATCH',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name NodesUpdateDetail
     * @summary Updates the specified node
     * @request PUT:/nodes/{id}
     * @response `200` `string` The ID of the entity
     * @response `403` `void` The current user does not have rights to edit this node
     * @response `404` `void` No node was found for that id
     */
    nodesUpdateDetail: (id: string, data: NodeUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/nodes/${id}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name NodesDeleteDetail
     * @summary Deletes the specified node and removes all of the node's associations from communities and nodes
     * @request DELETE:/nodes/{id}
     * @response `403` `void` The current user does not have rights to delete this node
     * @response `404` `void` No node was found for that id
     */
    nodesDeleteDetail: (id: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/nodes/${id}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name DetailDetail
     * @summary Returns a node including detailed stats
     * @request GET:/nodes/{id}/detail
     * @response `200` `NodeDetailModel` The node data including detailed stats
     * @response `404` `void` No node was found for that id
     */
    detailDetail: (id: string, params: RequestParams = {}) =>
      this.request<NodeDetailModel, void>({
        path: `/nodes/${id}/detail`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name AutocompleteList
     * @summary List the basic information of nodes for a given search query. Only nodes accessible to the currently logged in user will be returned
     * @request GET:/nodes/autocomplete
     * @response `200` `(CommunityEntityMiniModel)[]`
     */
    autocompleteList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModel[], any>({
        path: `/nodes/autocomplete`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name WorkspacesDetail
     * @request GET:/nodes/{id}/workspaces
     * @response `200` `(WorkspaceModel)[]` Workspace data
     * @response `403` `string` The current user does not have rights to view this node's content
     * @response `404` `void` No node was found for that id
     */
    workspacesDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<WorkspaceModel[], string | void>({
        path: `/nodes/${id}/workspaces`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name OrganizationsDetail
     * @request GET:/nodes/{id}/organizations
     * @response `403` `string` The current user does not have rights to view this node's content
     * @response `404` `void` No node was found for that id
     */
    organizationsDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<any, string | void>({
        path: `/nodes/${id}/organizations`,
        method: 'GET',
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name CfpsDetail
     * @summary Lists all cfps for the specified node
     * @request GET:/nodes/{id}/cfps
     * @response `200` `(CallForProposalMiniModel)[]`
     * @response `403` `string` The current user does not have rights to view this node's contents
     * @response `404` `void` No node was found for that id
     */
    cfpsDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CallForProposalMiniModel[], string | void>({
        path: `/nodes/${id}/cfps`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name NeedsAggregateDetail
     * @summary Lists all needs for the specified node and its ecosystem
     * @request GET:/nodes/{id}/needs/aggregate
     * @response `200` `NeedModelListPage` A list of needs in the specified node matching the search query visible to the current user
     * @response `404` `void` No node was found for that id
     */
    needsAggregateDetail: (
      id: string,
      query?: {
        communityEntityIds?: string;
        filter?: FeedEntityFilter;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<NeedModelListPage, void>({
        path: `/nodes/${id}/needs/aggregate`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name NeedsAggregateNewDetail
     * @summary Returns a value indicating whether or not there are new needs for a given node
     * @request GET:/nodes/{id}/needs/aggregate/new
     * @response `200` `boolean` True or false
     */
    needsAggregateNewDetail: (
      id: string,
      query?: {
        filter?: FeedEntityFilter;
      },
      params: RequestParams = {},
    ) =>
      this.request<boolean, any>({
        path: `/nodes/${id}/needs/aggregate/new`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name NeedsAggregateCommunityEntitiesDetail
     * @summary Lists all community entities for needs for the specified node and its ecosystem
     * @request GET:/nodes/{id}/needs/aggregate/communityEntities
     * @response `200` `(CommunityEntityMiniModel)[]`
     * @response `403` `void` The current user does not have rights to view this node's content
     * @response `404` `void` No node was found for that id
     */
    needsAggregateCommunityEntitiesDetail: (
      id: string,
      query?: {
        types?: CommunityEntityType[];
        currentUser?: boolean;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModel[], void>({
        path: `/nodes/${id}/needs/aggregate/communityEntities`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name EventsAggregateDetail
     * @summary Lists all events for the specified node and its ecosystem
     * @request GET:/nodes/{id}/events/aggregate
     * @response `200` `EventModelListPage` Event data
     * @response `404` `void` No node was found for that id
     */
    eventsAggregateDetail: (
      id: string,
      query?: {
        communityEntityIds?: string;
        filter?: FeedEntityFilter;
        status?: AttendanceStatus;
        /** @format date-time */
        from?: string;
        /** @format date-time */
        to?: string;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EventModelListPage, void>({
        path: `/nodes/${id}/events/aggregate`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name EventsAggregateNewDetail
     * @summary Returns a value indicating whether or not there are new events for a given node
     * @request GET:/nodes/{id}/events/aggregate/new
     * @response `200` `boolean` True or false
     */
    eventsAggregateNewDetail: (
      id: string,
      query?: {
        filter?: FeedEntityFilter;
      },
      params: RequestParams = {},
    ) =>
      this.request<boolean, any>({
        path: `/nodes/${id}/events/aggregate/new`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name EventsAggregateCommunityEntitiesDetail
     * @summary Lists all community entities for events for the specified node and its ecosystem
     * @request GET:/nodes/{id}/events/aggregate/communityEntities
     * @response `200` `(CommunityEntityMiniModel)[]`
     * @response `403` `void` The current user does not have rights to view this node's content
     * @response `404` `void` No node was found for that id
     */
    eventsAggregateCommunityEntitiesDetail: (
      id: string,
      query?: {
        types?: CommunityEntityType[];
        currentUser?: boolean;
        tags?: EventTag[];
        /** @format date-time */
        from?: string;
        /** @format date-time */
        to?: string;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModel[], void>({
        path: `/nodes/${id}/events/aggregate/communityEntities`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name DocumentsAggregateDetail
     * @summary Lists all documents for the specified node and its ecosystem
     * @request GET:/nodes/{id}/documents/aggregate
     * @response `200` `DocumentModelListPage`
     * @response `404` `void` No node was found for that id
     */
    documentsAggregateDetail: (
      id: string,
      query?: {
        communityEntityIds?: string;
        type?: DocumentFilter;
        filter?: FeedEntityFilter;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<DocumentModelListPage, void>({
        path: `/nodes/${id}/documents/aggregate`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name DocumentsAggregateNewDetail
     * @summary Returns a value indicating whether or not there are new documents for a given node
     * @request GET:/nodes/{id}/documents/aggregate/new
     * @response `200` `boolean` True or false
     */
    documentsAggregateNewDetail: (
      id: string,
      query?: {
        filter?: FeedEntityFilter;
      },
      params: RequestParams = {},
    ) =>
      this.request<boolean, any>({
        path: `/nodes/${id}/documents/aggregate/new`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name DocumentsAggregateCommunityEntitiesDetail
     * @summary Lists all community entities for documents for the specified node and its ecosystem
     * @request GET:/nodes/{id}/documents/aggregate/communityEntities
     * @response `200` `(CommunityEntityMiniModel)[]`
     * @response `403` `void` The current user does not have rights to view this node's content
     * @response `404` `void` No node was found for that id
     */
    documentsAggregateCommunityEntitiesDetail: (
      id: string,
      query?: {
        types?: CommunityEntityType[];
        type?: DocumentFilter;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModel[], void>({
        path: `/nodes/${id}/documents/aggregate/communityEntities`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name PapersAggregateDetail
     * @summary Lists all papers for the specified node and its ecosystem
     * @request GET:/nodes/{id}/papers/aggregate
     * @response `200` `PaperModelListPage`
     * @response `404` `void` No node was found for that id
     */
    papersAggregateDetail: (
      id: string,
      query?: {
        communityEntityIds?: string;
        type?: PaperType;
        filter?: FeedEntityFilter;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<PaperModelListPage, void>({
        path: `/nodes/${id}/papers/aggregate`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name PapersAggregateNewDetail
     * @summary Returns a value indicating whether or not there are new papers for a given node
     * @request GET:/nodes/{id}/papers/aggregate/new
     * @response `200` `boolean` True or false
     */
    papersAggregateNewDetail: (
      id: string,
      query?: {
        filter?: FeedEntityFilter;
      },
      params: RequestParams = {},
    ) =>
      this.request<boolean, any>({
        path: `/nodes/${id}/papers/aggregate/new`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name PapersAggregateCommunityEntitiesDetail
     * @summary Lists all community entities for papers for the specified node and its ecosystem
     * @request GET:/nodes/{id}/papers/aggregate/communityEntities
     * @response `200` `(CommunityEntityMiniModel)[]`
     * @response `403` `void` The current user does not have rights to view this node's content
     * @response `404` `void` No node was found for that id
     */
    papersAggregateCommunityEntitiesDetail: (
      id: string,
      query?: {
        types?: CommunityEntityType[];
        type?: PaperType;
        tags?: PaperTag[];
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModel[], void>({
        path: `/nodes/${id}/papers/aggregate/communityEntities`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name FeedAggregateNewDetail
     * @summary Returns a value indicating whether or not there are new posts in all feeds for a given node
     * @request GET:/nodes/{id}/feed/aggregate/new
     * @response `200` `boolean` True or false
     */
    feedAggregateNewDetail: (id: string, params: RequestParams = {}) =>
      this.request<boolean, any>({
        path: `/nodes/${id}/feed/aggregate/new`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name ChannelAggregateNewDetail
     * @summary Returns a value indicating whether or not there are new posts in channels for a given node
     * @request GET:/nodes/{id}/channel/aggregate/new
     * @response `200` `boolean` True or false
     */
    channelAggregateNewDetail: (id: string, params: RequestParams = {}) =>
      this.request<boolean, any>({
        path: `/nodes/${id}/channel/aggregate/new`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name UsersAggregateDetail
     * @summary Lists all needs for the specified node and its ecosystem
     * @request GET:/nodes/{id}/users/aggregate
     * @response `200` `UserMiniModelListPage` A list of needs in the specified node matching the search query
     * @response `404` `void` No node was found for that id
     */
    usersAggregateDetail: (
      id: string,
      query?: {
        communityEntityIds?: string;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<UserMiniModelListPage, void>({
        path: `/nodes/${id}/users/aggregate`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name UsersAggregateCommunityEntitiesDetail
     * @summary Lists all community entities for users for the specified node and its ecosystem
     * @request GET:/nodes/{id}/users/aggregate/communityEntities
     * @response `200` `(CommunityEntityMiniModel)[]`
     * @response `403` `void` The current user does not have rights to view this node's content
     * @response `404` `void` No node was found for that id
     */
    usersAggregateCommunityEntitiesDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModel[], void>({
        path: `/nodes/${id}/users/aggregate/communityEntities`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name SearchTotalsDetail
     * @summary Lists the totals for a global search query
     * @request GET:/nodes/{id}/search/totals
     * @response `200` `SearchResultNodeModel` Search result totals
     */
    searchTotalsDetail: (
      id: string,
      query?: {
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<SearchResultNodeModel, any>({
        path: `/nodes/${id}/search/totals`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name DocumentsCreate
     * @summary Adds a new document for the specified entity.
     * @request POST:/nodes/{id}/documents
     * @deprecated
     * @response `200` `string` The document was created
     * @response `400` `void` Note-typed documents have to be created and updated through the feed endpoints
     * @response `403` `void` The current user doesn't have sufficient rights to add documents for the entity
     * @response `404` `void` No entity was found for that id
     */
    documentsCreate: (id: string, data: DocumentInsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/nodes/${id}/documents`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name DocumentsDetail
     * @summary Lists all documents for the specified entity, not including file data
     * @request GET:/nodes/{id}/documents
     * @deprecated
     * @response `200` `string` The document was created
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
     * @response `404` `void` No entity was found for that id
     */
    documentsDetail: (
      id: string,
      query?: {
        folderId?: string;
        type?: DocumentFilter;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string, void>({
        path: `/nodes/${id}/documents`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name DocumentsDetailDetail
     * @summary Returns a single document, including the file represented as base64
     * @request GET:/nodes/{id}/documents/{documentId}
     * @deprecated
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
     * @response `404` `void` No entity was found for that id or the document does not exist
     */
    documentsDetailDetail: (id: string, documentId: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/nodes/${id}/documents/${documentId}`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name DocumentsUpdateDetail
     * @summary Updates the title and description for the document
     * @request PUT:/nodes/{id}/documents/{documentId}
     * @deprecated
     * @response `200` `void` The document was updated
     * @response `403` `void` The current user doesn't have sufficient rights to edit documents for the entity
     * @response `404` `void` No entity was found for that id or the document does not exist
     */
    documentsUpdateDetail: (id: string, documentId: string, data: DocumentUpdateModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/${id}/documents/${documentId}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name DocumentsDeleteDetail
     * @summary Deletes the specified document
     * @request DELETE:/nodes/{id}/documents/{documentId}
     * @deprecated
     * @response `200` `void` The document was deleted
     * @response `403` `void` The current user doesn't have sufficient rights to delete the document
     * @response `404` `void` No entity was found for that id or the document does not exist
     */
    documentsDeleteDetail: (id: string, documentId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/${id}/documents/${documentId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name DocumentsDraftDetail
     * @summary Returns a draft document for the specified container
     * @request GET:/nodes/{id}/documents/draft
     * @deprecated
     * @response `204` `void` No draft document was found for the community entity
     * @response `404` `void` No community entity was found for the specified id
     */
    documentsDraftDetail: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/${id}/documents/draft`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name DocumentsAllDetail
     * @summary Lists all documents and folders for the specified entity, not including file data
     * @request GET:/nodes/{id}/documents/all
     * @deprecated
     * @response `200` `(BaseModel)[]` The document was created
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
     * @response `404` `void` No entity was found for that id
     */
    documentsAllDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<BaseModel[], void>({
        path: `/nodes/${id}/documents/all`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name JoinCreate
     * @summary Joins an entity on behalf of the currently logged in user. Only works for community entities with the Open membership access level. Creates a membership record immediately, no invitation is created.
     * @request POST:/nodes/{id}/join
     * @deprecated
     * @response `200` `string` ID of the new membership object
     * @response `403` `void` The entity is not open to members joining
     * @response `404` `void` The entity does not exist
     * @response `409` `void` A membership record already exists for the entity and user
     */
    joinCreate: (id: string, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/nodes/${id}/join`,
        method: 'POST',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name RequestCreate
     * @summary Creates a request to join an entity on behalf of the currently logged in user
     * @request POST:/nodes/{id}/request
     * @deprecated
     * @response `200` `string` ID of the new invitation object
     * @response `403` `void` The entity is not open to members requesting to join
     * @response `404` `void` The entity does not exist
     * @response `409` `void` An invitation already exists for the entity and user
     */
    requestCreate: (id: string, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/nodes/${id}/request`,
        method: 'POST',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name InviteIdCreateDetail
     * @summary Invite a user to an entity using their ID
     * @request POST:/nodes/{id}/invite/id/{userId}
     * @response `200` `string` ID of the new invitation object
     * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity, or the user is inviting an owner without having the rights to manage owners
     * @response `404` `void` The entity or user do not exist
     * @response `409` `void` An invitation already exists for the entity and user
     */
    inviteIdCreateDetail: (id: string, userId: string, data: AccessLevel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/nodes/${id}/invite/id/${userId}`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name InviteEmailCreateDetail
     * @summary Invite a user to an entity using their email
     * @request POST:/nodes/{id}/invite/email/{userEmail}
     * @response `200` `string` ID of the new invitation object
     * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
     * @response `404` `void` No entity was found for that id
     */
    inviteEmailCreateDetail: (id: string, userEmail: string, data: AccessLevel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/nodes/${id}/invite/email/${userEmail}`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name InviteEmailBatchCreate
     * @summary Invite a user to an entity using their email
     * @request POST:/nodes/{id}/invite/email/batch
     * @deprecated
     * @response `200` `string` ID of the new invitation object
     * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
     * @response `404` `void` No entity was found for that id
     */
    inviteEmailBatchCreate: (id: string, data: string[], params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/nodes/${id}/invite/email/batch`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name InvitesDetail
     * @summary List all pending invitations for a given entity
     * @request GET:/nodes/{id}/invites
     * @response `200` `(InvitationModelUser)[]` A list of invitations
     * @response `404` `void` No entity was found for that id
     */
    invitesDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<InvitationModelUser[], void>({
        path: `/nodes/${id}/invites`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name InvitesAcceptCreate
     * @summary Accept the invite on behalf of the currently logged-in user
     * @request POST:/nodes/{id}/invites/{invitationId}/accept
     * @response `200` `void` The invitation has been accepted. The user is now a member of the project.
     * @response `403` `void` The current user does not have the right to accept the invitation
     * @response `404` `void` No entity was found for that id or the invitation does not exist
     */
    invitesAcceptCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/${id}/invites/${invitationId}/accept`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name InvitesRejectCreate
     * @summary Reject the invite on behalf of the currently logged-in user
     * @request POST:/nodes/{id}/invites/{invitationId}/reject
     * @response `200` `void` The invitation has been rejected
     * @response `403` `void` The current user does not have the right to reject the invitation
     * @response `404` `void` No entity was found for that id or the invitation does not exist
     */
    invitesRejectCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/${id}/invites/${invitationId}/reject`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name InvitesCancelCreate
     * @summary Cancels an invite
     * @request POST:/nodes/{id}/invites/{invitationId}/cancel
     * @response `200` `void` The invitation has been cancelled
     * @response `403` `void` The current user does not have the right to cancel the invitation
     * @response `404` `void` No entity was found for that id or the invitation does not exist
     */
    invitesCancelCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/${id}/invites/${invitationId}/cancel`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name InvitesResendCreate
     * @summary Resends an invite
     * @request POST:/nodes/invites/{invitationId}/resend
     * @response `200` `void` The invitation has been resent
     * @response `400` `void` The invitation is a request and cannot be resent
     * @response `403` `void` The user does not have the right to resend the invitation
     * @response `404` `void` No entity was found for that id or the invitation does not exist
     */
    invitesResendCreate: (invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/invites/${invitationId}/resend`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name LeaveCreate
     * @summary Leaves an entity on behalf of the currently logged in user
     * @request POST:/nodes/{id}/leave
     * @deprecated
     * @response `200` `void` The user has successfully left the entity
     * @response `404` `void` The entity does not exist or the user is not a member
     */
    leaveCreate: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/${id}/leave`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name OnboardingResponsesDetailDetail
     * @summary List onboarding responses for a community entity and a user
     * @request GET:/nodes/{id}/onboardingResponses/{userId}
     * @deprecated
     * @response `200` `OnboardingQuestionnaireInstanceModel` onboarding questionnaire responses
     * @response `404` `void` No entity was found for that id or no user was found
     */
    onboardingResponsesDetailDetail: (id: string, userId: string, params: RequestParams = {}) =>
      this.request<OnboardingQuestionnaireInstanceModel, void>({
        path: `/nodes/${id}/onboardingResponses/${userId}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name OnboardingResponsesCreate
     * @summary Posts onboarding responses for a community entity for the current user
     * @request POST:/nodes/{id}/onboardingResponses
     * @deprecated
     * @response `200` `void` The onboarding questionnaire responses were saved successfully
     * @response `404` `void` No entity was found for that id
     */
    onboardingResponsesCreate: (
      id: string,
      data: OnboardingQuestionnaireInstanceUpsertModel,
      params: RequestParams = {},
    ) =>
      this.request<void, void>({
        path: `/nodes/${id}/onboardingResponses`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name OnboardingCompletionCreate
     * @summary Records the completion of the onboarding workflow for a community entity for the current user
     * @request POST:/nodes/{id}/onboardingCompletion
     * @deprecated
     * @response `200` `void` The onboarding completion was recorded successfully
     * @response `404` `void` No entity was found for that id
     */
    onboardingCompletionCreate: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/${id}/onboardingCompletion`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name MembersDetail
     * @summary List all members for an entity
     * @request GET:/nodes/{id}/members
     * @response `200` `(MemberModel)[]` A list of members
     * @response `403` `void` The current user doesn't have sufficient rights to see the entity's members
     * @response `404` `void` No entity was found for that id
     */
    membersDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<MemberModel[], void>({
        path: `/nodes/${id}/members`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name InvitationDetail
     * @summary Returns the pending invitation for an object for the current user
     * @request GET:/nodes/{id}/invitation
     * @response `200` `InvitationModelEntity` A pending invitation
     * @response `404` `void` No invitation is pending or no entity was found for that id
     */
    invitationDetail: (id: string, params: RequestParams = {}) =>
      this.request<InvitationModelEntity, void>({
        path: `/nodes/${id}/invitation`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name MembersUpdateDetail
     * @summary Updates a member's access level for an entity
     * @request PUT:/nodes/{id}/members/{memberId}
     * @response `200` `void` The access level has been updated
     * @response `403` `void` The user does not have the right to set this access level for this member
     * @response `404` `void` No entity was found for that id or the user is not a member of that entity
     */
    membersUpdateDetail: (id: string, memberId: string, data: AccessLevel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/${id}/members/${memberId}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name MembersDeleteDetail
     * @summary Removes a member's access from an entity
     * @request DELETE:/nodes/{id}/members/{memberId}
     * @response `200` `void` The member has been removed
     * @response `403` `void` The current user does not have the right to remove members from this entity
     * @response `404` `void` No entity was found for that id or the user is not a member of that entity
     */
    membersDeleteDetail: (id: string, memberId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/${id}/members/${memberId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name MembersContributionUpdate
     * @summary Updates a member's contribution
     * @request PUT:/nodes/{id}/members/{memberId}/contribution
     * @response `200` `void` The contribution has been updated
     * @response `403` `void` The user does not have the right to set the contribution for other members
     * @response `404` `void` No entity was found for that id or the user is not a member of that entity
     */
    membersContributionUpdate: (id: string, memberId: string, data: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/${id}/members/${memberId}/contribution`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name MembersLabelsUpdate
     * @summary Updates a member's labels
     * @request PUT:/nodes/{id}/members/{memberId}/labels
     * @response `200` `void` The labels have been updated
     * @response `403` `void` The current user does not have the right to set labels for members
     * @response `404` `void` No entity was found for that id or the user is not a member of that entity
     */
    membersLabelsUpdate: (id: string, memberId: string, data: string[], params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/${id}/members/${memberId}/labels`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name CommunityEntityInviteCreateDetail
     * @summary Invite a community entity to become affiliated to another entity
     * @request POST:/nodes/{id}/communityEntityInvite/{targetEntityId}
     * @response `200` `string` ID of the new invitation object
     * @response `400` `void` An invitation cannot be extended to a community entity of the same type
     * @response `403` `void` The current user does not have the rights to invite entities to the specified entity
     * @response `404` `void` One of the entities does not exist
     * @response `409` `void` An invitation or affiliation already exists for the entities
     */
    communityEntityInviteCreateDetail: (id: string, targetEntityId: string, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/nodes/${id}/communityEntityInvite/${targetEntityId}`,
        method: 'POST',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name CommunityEntityInvitesIncomingDetail
     * @summary List all pending invitations for a given entity
     * @request GET:/nodes/{id}/communityEntityInvites/incoming
     * @response `200` `(CommunityEntityInvitationModelSource)[]` A list of invitations
     * @response `404` `void` No entity was found for that id
     */
    communityEntityInvitesIncomingDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityInvitationModelSource[], void>({
        path: `/nodes/${id}/communityEntityInvites/incoming`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name CommunityEntityInvitesOutgoingDetail
     * @summary List all pending invitations for a given entity
     * @request GET:/nodes/{id}/communityEntityInvites/outgoing
     * @response `200` `(CommunityEntityInvitationModelSource)[]` A list of invitations
     * @response `404` `void` No entity was found for that id
     */
    communityEntityInvitesOutgoingDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityInvitationModelSource[], void>({
        path: `/nodes/${id}/communityEntityInvites/outgoing`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name CommunityEntityInvitesAcceptCreate
     * @summary Accept an invite
     * @request POST:/nodes/{id}/communityEntityInvites/{invitationId}/accept
     * @response `200` `void` The invitation has been accepted
     * @response `403` `void` The current user does not have the right to accept the invitation
     * @response `404` `void` One of the entities does not exist or the invitation does not exist
     */
    communityEntityInvitesAcceptCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/${id}/communityEntityInvites/${invitationId}/accept`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name CommunityEntityInvitesRejectCreate
     * @summary Rejects an invite
     * @request POST:/nodes/{id}/communityEntityInvites/{invitationId}/reject
     * @response `200` `void` The invitation has been rejected
     * @response `403` `void` The current user does not have the right to reject the invitation
     * @response `404` `void` One of the entities does not exist or the invitation does not exist
     */
    communityEntityInvitesRejectCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/${id}/communityEntityInvites/${invitationId}/reject`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name CommunityEntityInvitesCancelCreate
     * @summary Cancels an invite
     * @request POST:/nodes/{id}/communityEntityInvites/{invitationId}/cancel
     * @response `200` `void` The invitation has been cancelled
     * @response `403` `void` The current user does not have the right to cancel the invitation
     * @response `404` `void` One of the entities does not exist or the invitation does not exist
     */
    communityEntityInvitesCancelCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/${id}/communityEntityInvites/${invitationId}/cancel`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name CommunityEntityRemoveCreate
     * @summary Removes an affiliation of an entity to another entity
     * @request POST:/nodes/{id}/communityEntity/{targetEntityId}/remove
     * @response `200` `void` The entity affiliation has been removed successfully
     * @response `403` `void` The current user does not have the permissions to manage the community entity's affiliations
     * @response `404` `void` One of the entities does not exist or the entities are not affiliated
     */
    communityEntityRemoveCreate: (id: string, targetEntityId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/${id}/communityEntity/${targetEntityId}/remove`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name CommunityEntityLinkCreate
     * @summary Creates an affiliation of an entity to another entity. On the entity, the user must be admin or owner, or the toggle for members to allow creating community entities of the respective type must be on. The user must be admin or owner on the target entity.
     * @request POST:/nodes/{id}/communityEntity/{targetEntityId}/link
     * @response `200` `void` The entity affiliation has been created successfully
     * @response `409` `void` One of the entities does not exist or the entities are already affiliated
     */
    communityEntityLinkCreate: (id: string, targetEntityId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/${id}/communityEntity/${targetEntityId}/link`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name EcosystemCommunityEntitiesDetail
     * @summary Lists all ecosystem containers (projects, communities and nodes) for the given community entity
     * @request GET:/nodes/{id}/ecosystem/communityEntities
     * @deprecated
     * @response `200` `(EntityMiniModel)[]`
     * @response `403` `string` The current user does not have rights to view this community entity's contents
     * @response `404` `void` No community entity was found for that id
     */
    ecosystemCommunityEntitiesDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EntityMiniModel[], string | void>({
        path: `/nodes/${id}/ecosystem/communityEntities`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name EcosystemUsersDetail
     * @summary Lists all ecosystem members for the given community entity
     * @request GET:/nodes/{id}/ecosystem/users
     * @response `200` `(EntityMiniModel)[]`
     * @response `403` `string` The current user does not have rights to view this community entity's contents
     * @response `404` `void` No community entity was found for that id
     */
    ecosystemUsersDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EntityMiniModel[], string | void>({
        path: `/nodes/${id}/ecosystem/users`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name NeedsCreate
     * @summary Adds a new need for the specified project
     * @request POST:/nodes/{id}/needs
     * @deprecated
     * @response `200` `string` The ID of the new need
     * @response `403` `void` The current user doesn't have sufficient rights to add needs for the entity
     */
    needsCreate: (id: string, data: NeedUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/nodes/${id}/needs`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name NeedsDetail
     * @summary Lists all needs for the specified community entity
     * @request GET:/nodes/{id}/needs
     * @deprecated
     * @response `200` `(NeedModel)[]`
     * @response `403` `string` The current user does not have rights to view this community entity's contents
     * @response `404` `void` No community entity was found for that id
     */
    needsDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<NeedModel[], string | void>({
        path: `/nodes/${id}/needs`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name NeedsDetailDetail
     * @summary Returns a single need
     * @request GET:/nodes/needs/{needId}
     * @deprecated
     * @response `403` `string` The current user does not have rights to view this community entity's contents
     * @response `404` `void` No need was found for the need id
     */
    needsDetailDetail: (needId: string, params: RequestParams = {}) =>
      this.request<any, string | void>({
        path: `/nodes/needs/${needId}`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name NeedsUpdateDetail
     * @summary Updates the need
     * @request PUT:/nodes/needs/{needId}
     * @deprecated
     * @response `200` `void` The need was updated
     * @response `403` `void` The current user does not have rights to update needs on this community entity
     * @response `404` `void` No need was found for the need id
     */
    needsUpdateDetail: (needId: string, data: NeedUpsertModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/needs/${needId}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name NeedsDeleteDetail
     * @summary Deletes the specified need
     * @request DELETE:/nodes/needs/{needId}
     * @deprecated
     * @response `200` `void` The need was deleted
     * @response `403` `void` The current user does not have rights to delete needs on this community entity
     * @response `404` `void` No need was found for the need id
     */
    needsDeleteDetail: (needId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/nodes/needs/${needId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name EventsCreate
     * @summary Adds a new event for the specified entity.
     * @request POST:/nodes/{id}/events
     * @deprecated
     * @response `200` `string` The event was created
     * @response `403` `void` The current user doesn't have sufficient rights to add events for the entity
     * @response `404` `void` No entity was found for that id
     */
    eventsCreate: (id: string, data: EventUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/nodes/${id}/events`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name EventsDetail
     * @summary Lists events for the specified entity.
     * @request GET:/nodes/{id}/events
     * @deprecated
     * @response `200` `(EventModel)[]` Event data
     * @response `403` `void` The current user doesn't have sufficient rights to view events for the entity
     * @response `404` `void` No entity was found for that id
     */
    eventsDetail: (
      id: string,
      query?: {
        /** @format date-time */
        from?: string;
        /** @format date-time */
        to?: string;
        tags?: EventTag[];
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EventModel[], void>({
        path: `/nodes/${id}/events`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name ChannelsCreate
     * @summary Adds a new channel for the specified entity.
     * @request POST:/nodes/{id}/channels
     * @deprecated
     * @response `200` `string` The channel was created
     * @response `403` `void` The current user doesn't have sufficient rights to add discussion channels for the entity
     * @response `404` `void` No entity was found for that id
     */
    channelsCreate: (id: string, data: ChannelUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/nodes/${id}/channels`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Node
     * @name ChannelsDetail
     * @summary Lists channels for the specified entity.
     * @request GET:/nodes/{id}/channels
     * @deprecated
     * @response `200` `(ChannelModel)[]` channel data
     * @response `403` `void` The current user doesn't have sufficient rights to view channels for the entity
     * @response `404` `void` No entity was found for that id
     */
    channelsDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ChannelModel[], void>({
        path: `/nodes/${id}/channels`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),
  };
  organizations = {
    /**
     * No description
     *
     * @tags Organization
     * @name OrganizationsCreate
     * @summary Create a new organization. The current user becomes a member of the organization with the Owner role
     * @request POST:/organizations
     * @response `200` `void` Success
     */
    organizationsCreate: (data: OrganizationUpsertModel, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/organizations`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name OrganizationsList
     * @summary List all accessible organizations for a given search query. Only organizations accessible to the currently logged in user will be returned
     * @request GET:/organizations
     * @response `200` `CommunityEntityMiniModelListPage`
     */
    organizationsList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModelListPage, any>({
        path: `/organizations`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name OrganizationsDetailDetail
     * @summary Returns an organization
     * @request GET:/organizations/{id}
     * @response `200` `OrganizationModel` The organization data
     * @response `404` `void` No organization was found for that id
     */
    organizationsDetailDetail: (id: string, params: RequestParams = {}) =>
      this.request<OrganizationModel, void>({
        path: `/organizations/${id}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name OrganizationsPartialUpdateDetail
     * @summary Patches the specified organization
     * @request PATCH:/organizations/{id}
     * @response `200` `string` The ID of the entity
     * @response `403` `void` The current user does not have rights to edit this organization
     * @response `404` `void` No organization was found for that id
     */
    organizationsPartialUpdateDetail: (id: string, data: OrganizationPatchModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/organizations/${id}`,
        method: 'PATCH',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name OrganizationsUpdateDetail
     * @summary Updates the specified organization
     * @request PUT:/organizations/{id}
     * @response `200` `string` The ID of the entity
     * @response `403` `void` The current user does not have rights to edit this organization
     * @response `404` `void` No organization was found for that id
     */
    organizationsUpdateDetail: (id: string, data: OrganizationUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/organizations/${id}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name OrganizationsDeleteDetail
     * @summary Deletes the specified organization and removes all of the organization's associations from communities and nodes
     * @request DELETE:/organizations/{id}
     * @response `403` `void` The current user does not have rights to delete this organization
     * @response `404` `void` No organization was found for that id
     */
    organizationsDeleteDetail: (id: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/organizations/${id}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name DetailDetail
     * @summary Returns an organization including detailed stats
     * @request GET:/organizations/{id}/detail
     * @response `200` `OrganizationDetailModel` The organization data including detailed stats
     * @response `404` `void` No organization was found for that id
     */
    detailDetail: (id: string, params: RequestParams = {}) =>
      this.request<OrganizationDetailModel, void>({
        path: `/organizations/${id}/detail`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name AutocompleteList
     * @summary List the basic information of organizations for a given search query. Only organizations accessible to the currently logged in user will be returned
     * @request GET:/organizations/autocomplete
     * @response `200` `(CommunityEntityMiniModel)[]`
     */
    autocompleteList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModel[], any>({
        path: `/organizations/autocomplete`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name WorkspacesDetail
     * @request GET:/organizations/{id}/workspaces
     * @response `200` `(WorkspaceModel)[]` Workspace data
     * @response `403` `string` The current user does not have rights to view this organization's content
     * @response `404` `void` No organization was found for that id
     */
    workspacesDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<WorkspaceModel[], string | void>({
        path: `/organizations/${id}/workspaces`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name NodesDetail
     * @summary Lists all events for the specified node and its ecosystem
     * @request GET:/organizations/{id}/nodes
     * @response `403` `string` The current user does not have rights to view this organization's content
     * @response `404` `void` No organization was found for that id
     */
    nodesDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<any, string | void>({
        path: `/organizations/${id}/nodes`,
        method: 'GET',
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name EventsAggregateDetail
     * @summary Lists all events for the specified organization and its ecosystem
     * @request GET:/organizations/{id}/events/aggregate
     * @response `200` `EventModelListPage` Event data
     * @response `403` `string` The current user does not have rights to view this organization's content
     * @response `404` `void` No organization was found for that id
     */
    eventsAggregateDetail: (
      id: string,
      query?: {
        types?: CommunityEntityType[];
        communityEntityIds?: string[];
        tags?: EventTag[];
        /** @format date-time */
        from?: string;
        /** @format date-time */
        to?: string;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EventModelListPage, string | void>({
        path: `/organizations/${id}/events/aggregate`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name EventsAggregateCommunityEntitiesDetail
     * @summary Lists all community entities for events for the specified org and its ecosystem
     * @request GET:/organizations/{id}/events/aggregate/communityEntities
     * @response `200` `(CommunityEntityMiniModel)[]`
     * @response `403` `void` The current user does not have rights to view this org's content
     * @response `404` `void` No org was found for that id
     */
    eventsAggregateCommunityEntitiesDetail: (
      id: string,
      query?: {
        types?: CommunityEntityType[];
        currentUser?: boolean;
        tags?: EventTag[];
        /** @format date-time */
        from?: string;
        /** @format date-time */
        to?: string;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModel[], void>({
        path: `/organizations/${id}/events/aggregate/communityEntities`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name DocumentsCreate
     * @summary Adds a new document for the specified entity.
     * @request POST:/organizations/{id}/documents
     * @deprecated
     * @response `200` `string` The document was created
     * @response `400` `void` Note-typed documents have to be created and updated through the feed endpoints
     * @response `403` `void` The current user doesn't have sufficient rights to add documents for the entity
     * @response `404` `void` No entity was found for that id
     */
    documentsCreate: (id: string, data: DocumentInsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/organizations/${id}/documents`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name DocumentsDetail
     * @summary Lists all documents for the specified entity, not including file data
     * @request GET:/organizations/{id}/documents
     * @deprecated
     * @response `200` `string` The document was created
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
     * @response `404` `void` No entity was found for that id
     */
    documentsDetail: (
      id: string,
      query?: {
        folderId?: string;
        type?: DocumentFilter;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string, void>({
        path: `/organizations/${id}/documents`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name DocumentsDetailDetail
     * @summary Returns a single document, including the file represented as base64
     * @request GET:/organizations/{id}/documents/{documentId}
     * @deprecated
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
     * @response `404` `void` No entity was found for that id or the document does not exist
     */
    documentsDetailDetail: (id: string, documentId: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/organizations/${id}/documents/${documentId}`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name DocumentsUpdateDetail
     * @summary Updates the title and description for the document
     * @request PUT:/organizations/{id}/documents/{documentId}
     * @deprecated
     * @response `200` `void` The document was updated
     * @response `403` `void` The current user doesn't have sufficient rights to edit documents for the entity
     * @response `404` `void` No entity was found for that id or the document does not exist
     */
    documentsUpdateDetail: (id: string, documentId: string, data: DocumentUpdateModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/${id}/documents/${documentId}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name DocumentsDeleteDetail
     * @summary Deletes the specified document
     * @request DELETE:/organizations/{id}/documents/{documentId}
     * @deprecated
     * @response `200` `void` The document was deleted
     * @response `403` `void` The current user doesn't have sufficient rights to delete the document
     * @response `404` `void` No entity was found for that id or the document does not exist
     */
    documentsDeleteDetail: (id: string, documentId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/${id}/documents/${documentId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name DocumentsDraftDetail
     * @summary Returns a draft document for the specified container
     * @request GET:/organizations/{id}/documents/draft
     * @deprecated
     * @response `204` `void` No draft document was found for the community entity
     * @response `404` `void` No community entity was found for the specified id
     */
    documentsDraftDetail: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/${id}/documents/draft`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name DocumentsAllDetail
     * @summary Lists all documents and folders for the specified entity, not including file data
     * @request GET:/organizations/{id}/documents/all
     * @deprecated
     * @response `200` `(BaseModel)[]` The document was created
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
     * @response `404` `void` No entity was found for that id
     */
    documentsAllDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<BaseModel[], void>({
        path: `/organizations/${id}/documents/all`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name JoinCreate
     * @summary Joins an entity on behalf of the currently logged in user. Only works for community entities with the Open membership access level. Creates a membership record immediately, no invitation is created.
     * @request POST:/organizations/{id}/join
     * @deprecated
     * @response `200` `string` ID of the new membership object
     * @response `403` `void` The entity is not open to members joining
     * @response `404` `void` The entity does not exist
     * @response `409` `void` A membership record already exists for the entity and user
     */
    joinCreate: (id: string, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/organizations/${id}/join`,
        method: 'POST',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name RequestCreate
     * @summary Creates a request to join an entity on behalf of the currently logged in user
     * @request POST:/organizations/{id}/request
     * @deprecated
     * @response `200` `string` ID of the new invitation object
     * @response `403` `void` The entity is not open to members requesting to join
     * @response `404` `void` The entity does not exist
     * @response `409` `void` An invitation already exists for the entity and user
     */
    requestCreate: (id: string, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/organizations/${id}/request`,
        method: 'POST',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name InviteIdCreateDetail
     * @summary Invite a user to an entity using their ID
     * @request POST:/organizations/{id}/invite/id/{userId}
     * @response `200` `string` ID of the new invitation object
     * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity, or the user is inviting an owner without having the rights to manage owners
     * @response `404` `void` The entity or user do not exist
     * @response `409` `void` An invitation already exists for the entity and user
     */
    inviteIdCreateDetail: (id: string, userId: string, data: AccessLevel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/organizations/${id}/invite/id/${userId}`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name InviteEmailCreateDetail
     * @summary Invite a user to an entity using their email
     * @request POST:/organizations/{id}/invite/email/{userEmail}
     * @response `200` `string` ID of the new invitation object
     * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
     * @response `404` `void` No entity was found for that id
     */
    inviteEmailCreateDetail: (id: string, userEmail: string, data: AccessLevel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/organizations/${id}/invite/email/${userEmail}`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name InviteEmailBatchCreate
     * @summary Invite a user to an entity using their email
     * @request POST:/organizations/{id}/invite/email/batch
     * @deprecated
     * @response `200` `string` ID of the new invitation object
     * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
     * @response `404` `void` No entity was found for that id
     */
    inviteEmailBatchCreate: (id: string, data: string[], params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/organizations/${id}/invite/email/batch`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name InvitesDetail
     * @summary List all pending invitations for a given entity
     * @request GET:/organizations/{id}/invites
     * @response `200` `(InvitationModelUser)[]` A list of invitations
     * @response `404` `void` No entity was found for that id
     */
    invitesDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<InvitationModelUser[], void>({
        path: `/organizations/${id}/invites`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name InvitesAcceptCreate
     * @summary Accept the invite on behalf of the currently logged-in user
     * @request POST:/organizations/{id}/invites/{invitationId}/accept
     * @response `200` `void` The invitation has been accepted. The user is now a member of the project.
     * @response `403` `void` The current user does not have the right to accept the invitation
     * @response `404` `void` No entity was found for that id or the invitation does not exist
     */
    invitesAcceptCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/${id}/invites/${invitationId}/accept`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name InvitesRejectCreate
     * @summary Reject the invite on behalf of the currently logged-in user
     * @request POST:/organizations/{id}/invites/{invitationId}/reject
     * @response `200` `void` The invitation has been rejected
     * @response `403` `void` The current user does not have the right to reject the invitation
     * @response `404` `void` No entity was found for that id or the invitation does not exist
     */
    invitesRejectCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/${id}/invites/${invitationId}/reject`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name InvitesCancelCreate
     * @summary Cancels an invite
     * @request POST:/organizations/{id}/invites/{invitationId}/cancel
     * @response `200` `void` The invitation has been cancelled
     * @response `403` `void` The current user does not have the right to cancel the invitation
     * @response `404` `void` No entity was found for that id or the invitation does not exist
     */
    invitesCancelCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/${id}/invites/${invitationId}/cancel`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name InvitesResendCreate
     * @summary Resends an invite
     * @request POST:/organizations/invites/{invitationId}/resend
     * @response `200` `void` The invitation has been resent
     * @response `400` `void` The invitation is a request and cannot be resent
     * @response `403` `void` The user does not have the right to resend the invitation
     * @response `404` `void` No entity was found for that id or the invitation does not exist
     */
    invitesResendCreate: (invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/invites/${invitationId}/resend`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name LeaveCreate
     * @summary Leaves an entity on behalf of the currently logged in user
     * @request POST:/organizations/{id}/leave
     * @deprecated
     * @response `200` `void` The user has successfully left the entity
     * @response `404` `void` The entity does not exist or the user is not a member
     */
    leaveCreate: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/${id}/leave`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name OnboardingResponsesDetailDetail
     * @summary List onboarding responses for a community entity and a user
     * @request GET:/organizations/{id}/onboardingResponses/{userId}
     * @deprecated
     * @response `200` `OnboardingQuestionnaireInstanceModel` onboarding questionnaire responses
     * @response `404` `void` No entity was found for that id or no user was found
     */
    onboardingResponsesDetailDetail: (id: string, userId: string, params: RequestParams = {}) =>
      this.request<OnboardingQuestionnaireInstanceModel, void>({
        path: `/organizations/${id}/onboardingResponses/${userId}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name OnboardingResponsesCreate
     * @summary Posts onboarding responses for a community entity for the current user
     * @request POST:/organizations/{id}/onboardingResponses
     * @deprecated
     * @response `200` `void` The onboarding questionnaire responses were saved successfully
     * @response `404` `void` No entity was found for that id
     */
    onboardingResponsesCreate: (
      id: string,
      data: OnboardingQuestionnaireInstanceUpsertModel,
      params: RequestParams = {},
    ) =>
      this.request<void, void>({
        path: `/organizations/${id}/onboardingResponses`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name OnboardingCompletionCreate
     * @summary Records the completion of the onboarding workflow for a community entity for the current user
     * @request POST:/organizations/{id}/onboardingCompletion
     * @deprecated
     * @response `200` `void` The onboarding completion was recorded successfully
     * @response `404` `void` No entity was found for that id
     */
    onboardingCompletionCreate: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/${id}/onboardingCompletion`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name MembersDetail
     * @summary List all members for an entity
     * @request GET:/organizations/{id}/members
     * @response `200` `(MemberModel)[]` A list of members
     * @response `403` `void` The current user doesn't have sufficient rights to see the entity's members
     * @response `404` `void` No entity was found for that id
     */
    membersDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<MemberModel[], void>({
        path: `/organizations/${id}/members`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name InvitationDetail
     * @summary Returns the pending invitation for an object for the current user
     * @request GET:/organizations/{id}/invitation
     * @response `200` `InvitationModelEntity` A pending invitation
     * @response `404` `void` No invitation is pending or no entity was found for that id
     */
    invitationDetail: (id: string, params: RequestParams = {}) =>
      this.request<InvitationModelEntity, void>({
        path: `/organizations/${id}/invitation`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name MembersUpdateDetail
     * @summary Updates a member's access level for an entity
     * @request PUT:/organizations/{id}/members/{memberId}
     * @response `200` `void` The access level has been updated
     * @response `403` `void` The user does not have the right to set this access level for this member
     * @response `404` `void` No entity was found for that id or the user is not a member of that entity
     */
    membersUpdateDetail: (id: string, memberId: string, data: AccessLevel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/${id}/members/${memberId}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name MembersDeleteDetail
     * @summary Removes a member's access from an entity
     * @request DELETE:/organizations/{id}/members/{memberId}
     * @response `200` `void` The member has been removed
     * @response `403` `void` The current user does not have the right to remove members from this entity
     * @response `404` `void` No entity was found for that id or the user is not a member of that entity
     */
    membersDeleteDetail: (id: string, memberId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/${id}/members/${memberId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name MembersContributionUpdate
     * @summary Updates a member's contribution
     * @request PUT:/organizations/{id}/members/{memberId}/contribution
     * @response `200` `void` The contribution has been updated
     * @response `403` `void` The user does not have the right to set the contribution for other members
     * @response `404` `void` No entity was found for that id or the user is not a member of that entity
     */
    membersContributionUpdate: (id: string, memberId: string, data: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/${id}/members/${memberId}/contribution`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name MembersLabelsUpdate
     * @summary Updates a member's labels
     * @request PUT:/organizations/{id}/members/{memberId}/labels
     * @response `200` `void` The labels have been updated
     * @response `403` `void` The current user does not have the right to set labels for members
     * @response `404` `void` No entity was found for that id or the user is not a member of that entity
     */
    membersLabelsUpdate: (id: string, memberId: string, data: string[], params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/${id}/members/${memberId}/labels`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name CommunityEntityInviteCreateDetail
     * @summary Invite a community entity to become affiliated to another entity
     * @request POST:/organizations/{id}/communityEntityInvite/{targetEntityId}
     * @response `200` `string` ID of the new invitation object
     * @response `400` `void` An invitation cannot be extended to a community entity of the same type
     * @response `403` `void` The current user does not have the rights to invite entities to the specified entity
     * @response `404` `void` One of the entities does not exist
     * @response `409` `void` An invitation or affiliation already exists for the entities
     */
    communityEntityInviteCreateDetail: (id: string, targetEntityId: string, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/organizations/${id}/communityEntityInvite/${targetEntityId}`,
        method: 'POST',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name CommunityEntityInvitesIncomingDetail
     * @summary List all pending invitations for a given entity
     * @request GET:/organizations/{id}/communityEntityInvites/incoming
     * @response `200` `(CommunityEntityInvitationModelSource)[]` A list of invitations
     * @response `404` `void` No entity was found for that id
     */
    communityEntityInvitesIncomingDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityInvitationModelSource[], void>({
        path: `/organizations/${id}/communityEntityInvites/incoming`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name CommunityEntityInvitesOutgoingDetail
     * @summary List all pending invitations for a given entity
     * @request GET:/organizations/{id}/communityEntityInvites/outgoing
     * @response `200` `(CommunityEntityInvitationModelSource)[]` A list of invitations
     * @response `404` `void` No entity was found for that id
     */
    communityEntityInvitesOutgoingDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityInvitationModelSource[], void>({
        path: `/organizations/${id}/communityEntityInvites/outgoing`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name CommunityEntityInvitesAcceptCreate
     * @summary Accept an invite
     * @request POST:/organizations/{id}/communityEntityInvites/{invitationId}/accept
     * @response `200` `void` The invitation has been accepted
     * @response `403` `void` The current user does not have the right to accept the invitation
     * @response `404` `void` One of the entities does not exist or the invitation does not exist
     */
    communityEntityInvitesAcceptCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/${id}/communityEntityInvites/${invitationId}/accept`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name CommunityEntityInvitesRejectCreate
     * @summary Rejects an invite
     * @request POST:/organizations/{id}/communityEntityInvites/{invitationId}/reject
     * @response `200` `void` The invitation has been rejected
     * @response `403` `void` The current user does not have the right to reject the invitation
     * @response `404` `void` One of the entities does not exist or the invitation does not exist
     */
    communityEntityInvitesRejectCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/${id}/communityEntityInvites/${invitationId}/reject`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name CommunityEntityInvitesCancelCreate
     * @summary Cancels an invite
     * @request POST:/organizations/{id}/communityEntityInvites/{invitationId}/cancel
     * @response `200` `void` The invitation has been cancelled
     * @response `403` `void` The current user does not have the right to cancel the invitation
     * @response `404` `void` One of the entities does not exist or the invitation does not exist
     */
    communityEntityInvitesCancelCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/${id}/communityEntityInvites/${invitationId}/cancel`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name CommunityEntityRemoveCreate
     * @summary Removes an affiliation of an entity to another entity
     * @request POST:/organizations/{id}/communityEntity/{targetEntityId}/remove
     * @response `200` `void` The entity affiliation has been removed successfully
     * @response `403` `void` The current user does not have the permissions to manage the community entity's affiliations
     * @response `404` `void` One of the entities does not exist or the entities are not affiliated
     */
    communityEntityRemoveCreate: (id: string, targetEntityId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/${id}/communityEntity/${targetEntityId}/remove`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name CommunityEntityLinkCreate
     * @summary Creates an affiliation of an entity to another entity. On the entity, the user must be admin or owner, or the toggle for members to allow creating community entities of the respective type must be on. The user must be admin or owner on the target entity.
     * @request POST:/organizations/{id}/communityEntity/{targetEntityId}/link
     * @response `200` `void` The entity affiliation has been created successfully
     * @response `409` `void` One of the entities does not exist or the entities are already affiliated
     */
    communityEntityLinkCreate: (id: string, targetEntityId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/${id}/communityEntity/${targetEntityId}/link`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name EcosystemCommunityEntitiesDetail
     * @summary Lists all ecosystem containers (projects, communities and nodes) for the given community entity
     * @request GET:/organizations/{id}/ecosystem/communityEntities
     * @deprecated
     * @response `200` `(EntityMiniModel)[]`
     * @response `403` `string` The current user does not have rights to view this community entity's contents
     * @response `404` `void` No community entity was found for that id
     */
    ecosystemCommunityEntitiesDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EntityMiniModel[], string | void>({
        path: `/organizations/${id}/ecosystem/communityEntities`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name EcosystemUsersDetail
     * @summary Lists all ecosystem members for the given community entity
     * @request GET:/organizations/{id}/ecosystem/users
     * @response `200` `(EntityMiniModel)[]`
     * @response `403` `string` The current user does not have rights to view this community entity's contents
     * @response `404` `void` No community entity was found for that id
     */
    ecosystemUsersDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EntityMiniModel[], string | void>({
        path: `/organizations/${id}/ecosystem/users`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name NeedsCreate
     * @summary Adds a new need for the specified project
     * @request POST:/organizations/{id}/needs
     * @deprecated
     * @response `200` `string` The ID of the new need
     * @response `403` `void` The current user doesn't have sufficient rights to add needs for the entity
     */
    needsCreate: (id: string, data: NeedUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/organizations/${id}/needs`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name NeedsDetail
     * @summary Lists all needs for the specified community entity
     * @request GET:/organizations/{id}/needs
     * @deprecated
     * @response `200` `(NeedModel)[]`
     * @response `403` `string` The current user does not have rights to view this community entity's contents
     * @response `404` `void` No community entity was found for that id
     */
    needsDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<NeedModel[], string | void>({
        path: `/organizations/${id}/needs`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name NeedsDetailDetail
     * @summary Returns a single need
     * @request GET:/organizations/needs/{needId}
     * @deprecated
     * @response `403` `string` The current user does not have rights to view this community entity's contents
     * @response `404` `void` No need was found for the need id
     */
    needsDetailDetail: (needId: string, params: RequestParams = {}) =>
      this.request<any, string | void>({
        path: `/organizations/needs/${needId}`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name NeedsUpdateDetail
     * @summary Updates the need
     * @request PUT:/organizations/needs/{needId}
     * @deprecated
     * @response `200` `void` The need was updated
     * @response `403` `void` The current user does not have rights to update needs on this community entity
     * @response `404` `void` No need was found for the need id
     */
    needsUpdateDetail: (needId: string, data: NeedUpsertModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/needs/${needId}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name NeedsDeleteDetail
     * @summary Deletes the specified need
     * @request DELETE:/organizations/needs/{needId}
     * @deprecated
     * @response `200` `void` The need was deleted
     * @response `403` `void` The current user does not have rights to delete needs on this community entity
     * @response `404` `void` No need was found for the need id
     */
    needsDeleteDetail: (needId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/organizations/needs/${needId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name EventsCreate
     * @summary Adds a new event for the specified entity.
     * @request POST:/organizations/{id}/events
     * @deprecated
     * @response `200` `string` The event was created
     * @response `403` `void` The current user doesn't have sufficient rights to add events for the entity
     * @response `404` `void` No entity was found for that id
     */
    eventsCreate: (id: string, data: EventUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/organizations/${id}/events`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name EventsDetail
     * @summary Lists events for the specified entity.
     * @request GET:/organizations/{id}/events
     * @deprecated
     * @response `200` `(EventModel)[]` Event data
     * @response `403` `void` The current user doesn't have sufficient rights to view events for the entity
     * @response `404` `void` No entity was found for that id
     */
    eventsDetail: (
      id: string,
      query?: {
        /** @format date-time */
        from?: string;
        /** @format date-time */
        to?: string;
        tags?: EventTag[];
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EventModel[], void>({
        path: `/organizations/${id}/events`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name ChannelsCreate
     * @summary Adds a new channel for the specified entity.
     * @request POST:/organizations/{id}/channels
     * @deprecated
     * @response `200` `string` The channel was created
     * @response `403` `void` The current user doesn't have sufficient rights to add discussion channels for the entity
     * @response `404` `void` No entity was found for that id
     */
    channelsCreate: (id: string, data: ChannelUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/organizations/${id}/channels`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Organization
     * @name ChannelsDetail
     * @summary Lists channels for the specified entity.
     * @request GET:/organizations/{id}/channels
     * @deprecated
     * @response `200` `(ChannelModel)[]` channel data
     * @response `403` `void` The current user doesn't have sufficient rights to view channels for the entity
     * @response `404` `void` No entity was found for that id
     */
    channelsDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ChannelModel[], void>({
        path: `/organizations/${id}/channels`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),
  };
  papers = {
    /**
     * No description
     *
     * @tags Paper
     * @name PapersCreate
     * @summary Adds a new paper
     * @request POST:/papers/{entityId}/papers
     * @response `200` `string` The ID of the new paper
     * @response `403` `void` The current user doesn't have sufficient rights to add papers for the entity
     * @response `404` `void` The entity could not be found
     */
    papersCreate: (entityId: string, data: PaperUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/papers/${entityId}/papers`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Paper
     * @name PapersDetail
     * @summary Lists all papers for the specified entity
     * @request GET:/papers/{entityId}/papers
     * @response `200` `(PaperModel)[]` The paper data
     * @response `403` `void` The current user doesn't have sufficient rights to list papers for the entity
     * @response `404` `void` No entity was found for that id
     */
    papersDetail: (
      entityId: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<PaperModel[], void>({
        path: `/papers/${entityId}/papers`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Paper
     * @name PapersNewDetail
     * @summary Returns a value indicating whether or not there are new papers for a particular entity
     * @request GET:/papers/{entityId}/papers/new
     * @response `200` `boolean` True or false
     */
    papersNewDetail: (entityId: string, params: RequestParams = {}) =>
      this.request<boolean, any>({
        path: `/papers/${entityId}/papers/new`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Paper
     * @name PapersDetailDetail
     * @summary Returns a single paper
     * @request GET:/papers/{id}
     * @response `200` `PaperModel` The paper data
     * @response `403` `void` The current user doesn't have sufficient rights to view the paper
     * @response `404` `void` No paper was found for that id
     */
    papersDetailDetail: (id: string, params: RequestParams = {}) =>
      this.request<PaperModel, void>({
        path: `/papers/${id}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Paper
     * @name PapersUpdateDetail
     * @summary Updates the paper
     * @request PUT:/papers/{id}
     * @response `200` `void` The paper was updated
     * @response `403` `void` The current user doesn't have sufficient rights to edit the paper
     * @response `404` `void` No paper was found for that id
     */
    papersUpdateDetail: (id: string, data: PaperUpsertModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/papers/${id}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Paper
     * @name PapersDeleteDetail
     * @summary Deletes the specified paper
     * @request DELETE:/papers/{id}
     * @response `200` `void` The paper was deleted
     * @response `403` `void` The current user doesn't have sufficient rights to delete the paper
     * @response `404` `void` No paper was found for that id
     */
    papersDeleteDetail: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/papers/${id}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Paper
     * @name OpenalexAuthorsList
     * @summary Returns a list of scientific authors matching a search term
     * @request GET:/papers/openalex/authors
     * @response `200` `(AuthorModel)[]` The author data
     */
    openalexAuthorsList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<AuthorModel[], any>({
        path: `/papers/openalex/authors`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Paper
     * @name OpenalexWorksByAuthorIdsList
     * @summary Returns a list of scientific papers by a specific author or authors
     * @request GET:/papers/openalex/works/byAuthorIds
     * @response `200` `(WorkModel)[]` The paper data
     */
    openalexWorksByAuthorIdsList: (
      query?: {
        authorIds?: string;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<WorkModel[], any>({
        path: `/papers/openalex/works/byAuthorIds`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Paper
     * @name OpenalexWorksByAuthorNameList
     * @summary Returns a list of scientific papers for an author name
     * @request GET:/papers/openalex/works/byAuthorName
     * @response `200` `(WorkModel)[]` The paper data
     */
    openalexWorksByAuthorNameList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<WorkModel[], any>({
        path: `/papers/openalex/works/byAuthorName`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),
  };
  proposals = {
    /**
     * No description
     *
     * @tags Proposal
     * @name ProposalsCreate
     * @summary Creates a new proposal on behalf of a project
     * @request POST:/proposals
     * @response `200` `string` The ID of the new proposal
     * @response `403` `void` The call for proposal is not yet open for submissions or the current user does not have permissions
     * @response `404` `void` Either the call for proposal or the project was not found
     * @response `409` `string` A proposal already exists for this project and call for proposals
     */
    proposalsCreate: (data: ProposalUpsertModel, params: RequestParams = {}) =>
      this.request<string, void | string>({
        path: `/proposals`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Proposal
     * @name ProposalsDetailDetail
     * @summary Gets a proposal
     * @request GET:/proposals/{proposalId}
     * @response `200` `ProposalModel`
     * @response `403` `void` The current user is not an author on the proposal
     * @response `404` `void` No proposal was found for the id
     */
    proposalsDetailDetail: (proposalId: string, params: RequestParams = {}) =>
      this.request<ProposalModel, void>({
        path: `/proposals/${proposalId}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Proposal
     * @name ProposalsPartialUpdateDetail
     * @summary Patches a proposal
     * @request PATCH:/proposals/{proposalId}
     * @response `200` `ProposalModel`
     * @response `403` `void` The current user is not an author on the proposal
     * @response `404` `void` No proposal was found for the id
     */
    proposalsPartialUpdateDetail: (proposalId: string, data: ProposalPatchModel, params: RequestParams = {}) =>
      this.request<ProposalModel, void>({
        path: `/proposals/${proposalId}`,
        method: 'PATCH',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Proposal
     * @name ProposalsDeleteDetail
     * @summary Deletes the specified proposal
     * @request DELETE:/proposals/{proposalId}
     * @response `200` `void` The proposal was deleted
     * @response `403` `void` The proposal wasn't created by the current user
     * @response `404` `void` No proposal was found for the id
     */
    proposalsDeleteDetail: (proposalId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/proposals/${proposalId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Proposal
     * @name StatusCreate
     * @summary Updates the status of a proposal
     * @request POST:/proposals/{proposalId}/status
     * @response `400` `void` This status cannot be set
     * @response `403` `void` The current user does not have the rights to set this status
     * @response `404` `void` No proposal was found for the id
     * @response `409` `void` The proposal has already been submitted
     */
    statusCreate: (proposalId: string, data: ProposalStatus, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/proposals/${proposalId}/status`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Proposal
     * @name ScoreCreate
     * @summary Scores a proposal
     * @request POST:/proposals/{proposalId}/score
     * @response `200` `ProposalModel`
     * @response `403` `void` The current user is not a reviewer on the cfp
     * @response `404` `void` No proposal was found for the id
     */
    scoreCreate: (proposalId: string, data: number, params: RequestParams = {}) =>
      this.request<ProposalModel, void>({
        path: `/proposals/${proposalId}/score`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Proposal
     * @name DocumentsCreate
     * @summary Adds a new document for the specified proposal.
     * @request POST:/proposals/{proposalId}/documents
     * @response `200` `string` The document was created
     * @response `403` `void` The current user doesn't have sufficient rights to add documents for the proposal
     * @response `404` `void` No proposal was found for that id
     */
    documentsCreate: (proposalId: string, data: DocumentInsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/proposals/${proposalId}/documents`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Proposal
     * @name DocumentsDetailDetail
     * @summary Returns a single document, including the file represented as base64
     * @request GET:/proposals/{proposalId}/documents/{documentId}
     * @deprecated
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the proposal
     * @response `404` `void` No proposal was found for that id, the document id is incorrect or the document does not exist
     */
    documentsDetailDetail: (proposalId: string, documentId: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/proposals/${proposalId}/documents/${documentId}`,
        method: 'GET',
        ...params,
      }),
  };
  public = {
    /**
     * No description
     *
     * @tags Public
     * @name JoinCreate
     * @request POST:/public/join
     * @response `200` `void` User successful added to the waitlist
     */
    joinCreate: (data: WaitlistRecordModel, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/public/join`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Public
     * @name ContactCreate
     * @request POST:/public/contact
     * @response `200` `void` User contact successful
     */
    contactCreate: (data: UserContactModel, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/public/contact`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),
  };
  resources = {
    /**
     * No description
     *
     * @tags Resource
     * @name ResourcesCreate
     * @summary Adds a new resource for the specified feed.
     * @request POST:/resources/{entityId}/resources
     * @response `200` `string` The resource was created
     * @response `403` `void` The current user doesn't have sufficient rights to add resource for the entity
     */
    resourcesCreate: (entityId: string, data: ResourceUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/resources/${entityId}/resources`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Resource
     * @name ResourcesDetail
     * @summary Lists all resources for the specified entity
     * @request GET:/resources/{entityId}/resources
     * @response `200` `(ResourceModel)[]` The resource data
     * @response `403` `void` The current user doesn't have sufficient rights to view resources for the entity
     */
    resourcesDetail: (
      entityId: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ResourceModel[], void>({
        path: `/resources/${entityId}/resources`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Resource
     * @name ResourcesRepoCreate
     * @summary Adds a new repository resource for the specified feed.
     * @request POST:/resources/{entityId}/resources/repo
     * @response `200` `string` The resource was created
     * @response `403` `void` The current user doesn't have sufficient rights to add resource for the entity
     * @response `422` `void` The url could not be resolved to a valid repository
     */
    resourcesRepoCreate: (entityId: string, data: ResourceRepositoryUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/resources/${entityId}/resources/repo`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Resource
     * @name ResourcesList
     * @summary Lists all resources
     * @request GET:/resources
     * @response `200` `ResourceModelListPage` A list of resources matching the search query visible to the current user
     */
    resourcesList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ResourceModelListPage, any>({
        path: `/resources`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Resource
     * @name ResourcesDetailDetail
     * @summary Returns a single resource
     * @request GET:/resources/{id}
     * @response `200` `ResourceModel`
     * @response `403` `void` The current user doesn't have sufficient rights to see the resource
     * @response `404` `void` No resource was found for that id
     */
    resourcesDetailDetail: (id: string, params: RequestParams = {}) =>
      this.request<ResourceModel, void>({
        path: `/resources/${id}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Resource
     * @name ResourcesUpdateDetail
     * @summary Updates the resource
     * @request PUT:/resources/{id}
     * @response `200` `void` The resource was updated
     * @response `403` `void` The current user doesn't have sufficient rights to edit the resource
     * @response `404` `void` No resource was found for that id
     */
    resourcesUpdateDetail: (id: string, data: ResourceUpsertModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/resources/${id}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Resource
     * @name ResourcesDeleteDetail
     * @summary Deletes a resource
     * @request DELETE:/resources/{id}
     * @response `200` `void` The resource was deleted
     * @response `403` `void` The current user doesn't have sufficient rights to delete this resource
     * @response `404` `void` No resource was found for that id
     */
    resourcesDeleteDetail: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/resources/${id}`,
        method: 'DELETE',
        ...params,
      }),
  };
  users = {
    /**
     * No description
     *
     * @tags User
     * @name UsersCreate
     * @summary Create a new user
     * @request POST:/users
     * @response `200` `string` ID of the new user record
     * @response `400` `void` The supplied verification code is invalid
     * @response `403` `void` The captcha code is invalid
     * @response `409` `ErrorModel` A user with this email or nickname already exists
     */
    usersCreate: (data: UserCreateModel, params: RequestParams = {}) =>
      this.request<string, void | ErrorModel>({
        path: `/users`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name UsersList
     * @summary List all users for a given query
     * @request GET:/users
     * @response `200` `UserMiniModelListPage` A list of user models
     */
    usersList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<UserMiniModelListPage, any>({
        path: `/users`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name UsersDelete
     * @summary Deletes the current user
     * @request DELETE:/users
     * @response `200` `void` The user was deleted successfully
     * @response `400` `void` The user cannot be deleted
     * @response `409` `void` The user is already archived
     */
    usersDelete: (params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/users`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name WalletCreateDetail
     * @summary Register a user with a wallet signature
     * @request POST:/users/wallet/{walletType}
     * @response `200` `string` Registration successful
     * @response `401` `void` Signature invalid
     * @response `409` `ErrorModel` A user with this wallet, email or nickname already exists
     */
    walletCreateDetail: (walletType: WalletType, data: UserCreateWalletModel, params: RequestParams = {}) =>
      this.request<string, void | ErrorModel>({
        path: `/users/wallet/${walletType}`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name CheckCreate
     * @summary Verifies whether an email address is eligible to join JOGL
     * @request POST:/users/check
     * @deprecated
     * @response `200` `void` The email address can be used to sign up with a new user
     * @response `409` `void` A user with this email already exists
     */
    checkCreate: (data: EmailModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/users/check`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name UsersDetailDetail
     * @request GET:/users/{id}
     * @response `200` `UserModel` The user data
     * @response `404` `void` No user was found for that id
     */
    usersDetailDetail: (id: string, params: RequestParams = {}) =>
      this.request<UserModel, void>({
        path: `/users/${id}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name UsersPartialUpdateDetail
     * @summary Patches the specified user.
     * @request PATCH:/users/{id}
     * @response `403` `void` The current user can only edit its own user record
     * @response `404` `void` No user was found for that id
     * @response `409` `void` The username or email is already taken
     */
    usersPartialUpdateDetail: (id: string, data: UserPatchModel, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/users/${id}`,
        method: 'PATCH',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name UsersUpdateDetail
     * @summary Updates the specified user record
     * @request PUT:/users/{id}
     * @response `403` `void` The current user can only edit its own user record
     * @response `404` `void` No user was found for that id
     * @response `409` `void` The username or email is already taken
     */
    usersUpdateDetail: (id: string, data: UserUpdateModel, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/users/${id}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name PortfolioDetail
     * @request GET:/users/{id}/portfolio
     * @response `200` `(PortfolioItemModel)[]` Portfolio data
     * @response `404` `void` No user was found for that id
     */
    portfolioDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<PortfolioItemModel[], void>({
        path: `/users/${id}/portfolio`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name NeedsDetail
     * @request GET:/users/{id}/needs
     * @response `404` `void` No user was found for that id
     */
    needsDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<any, void>({
        path: `/users/${id}/needs`,
        method: 'GET',
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name EcosystemDetail
     * @request GET:/users/{id}/ecosystem
     * @deprecated
     * @response `404` `void` No user was found for that id
     */
    ecosystemDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<any, void>({
        path: `/users/${id}/ecosystem`,
        method: 'GET',
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name WorkspacesDetail
     * @request GET:/users/{id}/workspaces
     * @response `200` `(CommunityEntityMiniModel)[]` The user's workspaces
     * @response `404` `void` No user was found for that id
     */
    workspacesDetail: (
      id: string,
      query?: {
        permission?: Permission;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModel[], void>({
        path: `/users/${id}/workspaces`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name NodesDetail
     * @request GET:/users/{id}/nodes
     * @response `200` `(CommunityEntityMiniModel)[]` The user's nodes
     * @response `404` `void` No user was found for that id
     */
    nodesDetail: (
      id: string,
      query?: {
        permission?: Permission;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModel[], void>({
        path: `/users/${id}/nodes`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name OrganizationsDetail
     * @request GET:/users/{id}/organizations
     * @response `404` `void` No user was found for that id
     */
    organizationsDetail: (
      id: string,
      query?: {
        permission?: Permission;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<any, void>({
        path: `/users/${id}/organizations`,
        method: 'GET',
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name ArchiveCreate
     * @summary Archives the current user
     * @request POST:/users/archive
     * @response `200` `void` The user was archived successfully
     * @response `400` `void` The user cannot be archived
     * @response `409` `void` The user is already archived
     */
    archiveCreate: (params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/users/archive`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name UnarchiveCreate
     * @summary Unarchives the current user
     * @request POST:/users/unarchive
     * @response `200` `void` The user was unarchived successfully
     * @response `400` `void` The user isn't archived
     */
    unarchiveCreate: (params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/users/unarchive`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name ExistsUsernameDetailDetail
     * @summary Checks whether a username is already taken or not
     * @request GET:/users/exists/username/{username}
     * @response `200` `void` Username is available
     * @response `409` `void` Username is already taken
     */
    existsUsernameDetailDetail: (username: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/users/exists/username/${username}`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name ExistsWalletDetailDetail
     * @summary Checks whether a wallet address exists on JOGL or not
     * @request GET:/users/exists/wallet/{wallet}
     * @response `200` `boolean` Whether or not the specified wallet exits
     */
    existsWalletDetailDetail: (wallet: string, params: RequestParams = {}) =>
      this.request<boolean, any>({
        path: `/users/exists/wallet/${wallet}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name InvitesDetail
     * @summary List all pending community entity invitations for a given user
     * @request GET:/users/{id}/invites
     * @deprecated
     * @response `200` `(InvitationModelEntity)[]` A list of community entity invitations
     */
    invitesDetail: (id: string, params: RequestParams = {}) =>
      this.request<InvitationModelEntity[], any>({
        path: `/users/${id}/invites`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name InvitesCommunityEntityList
     * @summary List all pending community entity invitations for the current user
     * @request GET:/users/invites/communityEntity
     * @response `200` `(InvitationModelEntity)[]` A list of community entity invitations
     */
    invitesCommunityEntityList: (params: RequestParams = {}) =>
      this.request<InvitationModelEntity[], any>({
        path: `/users/invites/communityEntity`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name InvitesEventList
     * @summary List all pending event invitations for the current user
     * @request GET:/users/invites/event
     * @response `200` `(EventAttendanceModel)[]` A list of event invitations
     */
    invitesEventList: (params: RequestParams = {}) =>
      this.request<EventAttendanceModel[], any>({
        path: `/users/invites/event`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name ContactCreate
     * @request POST:/users/{id}/contact
     * @response `403` `void` The user has not enabled the Contact Me function
     * @response `404` `void` No user was found for that id
     */
    contactCreate: (id: string, data: ContactModel, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/users/${id}/contact`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name FollowCreate
     * @request POST:/users/{id}/follow
     * @response `400` `void` The user cannot follow themselves
     * @response `404` `void` No user was found for that id
     * @response `409` `void` The current user is already following this user
     */
    followCreate: (id: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/users/${id}/follow`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name FollowDelete
     * @request DELETE:/users/{id}/follow
     * @response `404` `void` No user was found for that id or the current user isn't following this user
     */
    followDelete: (id: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/users/${id}/follow`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name FollowedDetail
     * @request GET:/users/{id}/followed
     * @response `404` `void` No user was found for that id
     */
    followedDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<any, void>({
        path: `/users/${id}/followed`,
        method: 'GET',
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name FollowersDetail
     * @request GET:/users/{id}/followers
     * @response `404` `void` No user was found for that id
     */
    followersDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<any, void>({
        path: `/users/${id}/followers`,
        method: 'GET',
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name SkillsCreate
     * @request POST:/users/skills
     * @response `200` `void` Success
     */
    skillsCreate: (data: TextValueModel, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/users/skills`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name SkillsList
     * @request GET:/users/skills
     * @response `200` `void` Success
     */
    skillsList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<void, any>({
        path: `/users/skills`,
        method: 'GET',
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name InterestsCreate
     * @request POST:/users/interests
     * @response `200` `void` Success
     */
    interestsCreate: (data: TextValueModel, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/users/interests`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name InterestsList
     * @request GET:/users/interests
     * @response `200` `(TextValueModel)[]` The interest data
     */
    interestsList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<TextValueModel[], any>({
        path: `/users/interests`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name PapersCreate
     * @summary Adds a new paper
     * @request POST:/users/papers
     * @deprecated
     * @response `200` `string` The ID of the new paper
     * @response `400` `void` Note-typed papers have to be created through the feed controller
     */
    papersCreate: (data: PaperUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/users/papers`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name OrcidCreate
     * @summary Loads the user's unique ORCID id from ORCID and stores it in the database
     * @request POST:/users/orcid
     * @response `404` `void` No ORCID record found or user not found
     */
    orcidCreate: (data: OrcidLoadModel, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/users/orcid`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name OrcidDelete
     * @summary Removes the user's ORCID id and all orcid-loaded papers from the database
     * @request DELETE:/users/orcid
     * @response `404` `void` No ORCID record found or user not found
     */
    orcidDelete: (params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/users/orcid`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name GoogleDelete
     * @summary Removes the Google account link
     * @request DELETE:/users/google
     * @response `404` `void` No Google record found or user not found
     */
    googleDelete: (params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/users/google`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name PapersOrcidList
     * @summary Gets papers for the current user's ORCID
     * @request GET:/users/papers/orcid
     * @response `200` `(PaperModelOrcid)[]` The papers from ORCID
     * @response `404` `void` No ORCID set on the current user
     */
    papersOrcidList: (params: RequestParams = {}) =>
      this.request<PaperModelOrcid[], void>({
        path: `/users/papers/orcid`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name PapersS2PaperList
     * @summary Gets papers from Semantic Scholar - search papers
     * @request GET:/users/papers/s2/paper
     * @response `200` `PaperModelS2ListPage` The papers from S2
     */
    papersS2PaperList: (
      query: {
        /** The search query to filter projects by */
        Search: string;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 100
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 100
         * @format int32
         * @min 1
         * @max 100
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
      },
      params: RequestParams = {},
    ) =>
      this.request<PaperModelS2ListPage, any>({
        path: `/users/papers/s2/paper`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name PapersOaList
     * @summary Gets works from OpenAlex - search works
     * @request GET:/users/papers/oa
     * @response `200` `PaperModelOAListPage` The works from OpenAlex
     */
    papersOaList: (
      query: {
        /** The search query to filter projects by */
        Search: string;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 100
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 100
         * @format int32
         * @min 1
         * @max 100
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
      },
      params: RequestParams = {},
    ) =>
      this.request<PaperModelOAListPage, any>({
        path: `/users/papers/oa`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name PapersPmList
     * @summary Gets articles from PubMed - search articles
     * @request GET:/users/papers/pm
     * @response `200` `PaperModelPMListPage` The articles from PubMed
     */
    papersPmList: (
      query: {
        /** The search query to filter projects by */
        Search: string;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 100
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 100
         * @format int32
         * @min 1
         * @max 100
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
      },
      params: RequestParams = {},
    ) =>
      this.request<PaperModelPMListPage, any>({
        path: `/users/papers/pm`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name PapersDoiList
     * @summary Gets paper for DOI
     * @request GET:/users/papers/doi
     * @response `200` `PaperModelOrcid` The paper
     * @response `404` `void` No paper found
     */
    papersDoiList: (
      query?: {
        id?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<PaperModelOrcid, void>({
        path: `/users/papers/doi`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name ProposalsList
     * @summary Lists all proposals for current user
     * @request GET:/users/proposals
     * @response `200` `(ProposalModel)[]`
     */
    proposalsList: (params: RequestParams = {}) =>
      this.request<ProposalModel[], any>({
        path: `/users/proposals`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name NotificationsLastReadList
     * @summary Returns the date time and when the user last read their notifications
     * @request GET:/users/notifications/lastRead
     * @response `200` `void` Success
     */
    notificationsLastReadList: (params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/users/notifications/lastRead`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name NotificationsLastReadCreate
     * @summary Records the date time and when the user last read their notifications
     * @request POST:/users/notifications/lastRead
     * @response `200` `void` Success
     */
    notificationsLastReadCreate: (params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/users/notifications/lastRead`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name NotificationsList
     * @summary Lists all notifications for current user
     * @request GET:/users/notifications
     * @deprecated
     * @response `200` `(NotificationModel)[]`
     */
    notificationsList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<NotificationModel[], any>({
        path: `/users/notifications`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name CurrentNotificationsList
     * @summary Lists all notifications for current user
     * @request GET:/users/current/notifications
     * @response `200` `NotificationModelListPage`
     */
    currentNotificationsList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<NotificationModelListPage, any>({
        path: `/users/current/notifications`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name CurrentNotificationsPendingList
     * @summary Returns a boolean value indicating whether or not the current user has any pending (un-actioned) notifications
     * @request GET:/users/current/notifications/pending
     * @response `200` `boolean` A boolean flag
     */
    currentNotificationsPendingList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<boolean, any>({
        path: `/users/current/notifications/pending`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name NotificationsActionedCreate
     * @summary Records the date time and when the user last read their notifications
     * @request POST:/users/notifications/{id}/actioned
     * @response `403` `void` The notification belongs to a different user
     * @response `404` `void` The notification does not exist
     */
    notificationsActionedCreate: (id: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/users/notifications/${id}/actioned`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name EventsDetail
     * @request GET:/users/{id}/events
     * @response `404` `void` No user was found for that id
     */
    eventsDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
        /** @format date-time */
        from?: string;
        /** @format date-time */
        to?: string;
        tags?: EventTag[];
      },
      params: RequestParams = {},
    ) =>
      this.request<any, void>({
        path: `/users/${id}/events`,
        method: 'GET',
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name OrcidUserdataList
     * @summary Gets data for the current user's ORCID
     * @request GET:/users/orcid/userdata
     * @response `200` `OrcidExperienceModel` The data from ORCID
     * @response `404` `void` No ORCID set on the current user
     */
    orcidUserdataList: (params: RequestParams = {}) =>
      this.request<OrcidExperienceModel, void>({
        path: `/users/orcid/userdata`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name GithubReposList
     * @summary Gets GitHub repositories for the current user
     * @request GET:/users/github/repos
     * @response `200` `(RepositoryModel)[]` The repository data
     */
    githubReposList: (
      query?: {
        accessToken?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<RepositoryModel[], any>({
        path: `/users/github/repos`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name HuggingfaceReposList
     * @summary Gets Hugging Face repositories for the current user
     * @request GET:/users/huggingface/repos
     * @response `200` `(RepositoryModel)[]` The repository data
     */
    huggingfaceReposList: (
      query?: {
        accessToken?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<RepositoryModel[], any>({
        path: `/users/huggingface/repos`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name LinkedinProfileList
     * @summary Gets LinkedIn data for the current user
     * @request GET:/users/linkedin/profile
     * @response `200` `ProfileModel` The LinkedIn data
     */
    linkedinProfileList: (
      query?: {
        linkedInUrl?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ProfileModel, any>({
        path: `/users/linkedin/profile`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name PushTokenCreate
     * @summary Upserts a push notification token for the current user
     * @request POST:/users/push/token
     * @response `200` `void` Push notification token stored
     */
    pushTokenCreate: (data: string, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/users/push/token`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name AutocompleteList
     * @summary Autocomplete search for all users
     * @request GET:/users/autocomplete
     * @response `200` `(UserMiniModel)[]` A list of user models
     */
    autocompleteList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<UserMiniModel[], any>({
        path: `/users/autocomplete`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name OnboardingCreate
     * @summary Stores onboarding data for the current user
     * @request POST:/users/onboarding
     * @response `200` `void` The onboarding data was stored in the user profile
     */
    onboardingCreate: (data: OnboardingUpsertModel, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/users/onboarding`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name OnboardingStatusCreate
     * @summary Stores onboarding status for the current user
     * @request POST:/users/onboarding/status
     * @response `200` `void` The onboarding status was stored in the user profile
     */
    onboardingStatusCreate: (params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/users/onboarding/status`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name OnboardingStatusList
     * @summary Gets onboarding status for the current user
     * @request GET:/users/onboarding/status
     * @response `200` `boolean` The onboarding status
     */
    onboardingStatusList: (params: RequestParams = {}) =>
      this.request<boolean, any>({
        path: `/users/onboarding/status`,
        method: 'GET',
        format: 'json',
        ...params,
      }),
  };
  workspaces = {
    /**
     * No description
     *
     * @tags Workspace
     * @name WorkspacesCreate
     * @summary Create a new workspace. The current user becomes a member of the workspace with the Owner role
     * @request POST:/workspaces
     * @response `200` `string` The workspace id
     * @response `403` `void` The user does not have the permission to create workspaces in the target community entity
     */
    workspacesCreate: (data: WorkspaceUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/workspaces`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name WorkspacesList
     * @summary List all accessible workspaces for a given search query. Only workspaces accessible to the currently logged in user will be returned
     * @request GET:/workspaces
     * @response `200` `CommunityEntityMiniModelListPage`
     */
    workspacesList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModelListPage, any>({
        path: `/workspaces`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name WorkspacesDetailDetail
     * @summary Returns a single workspace
     * @request GET:/workspaces/{id}
     * @response `200` `WorkspaceModel` The workspace data
     * @response `404` `void` No workspace was found for that id
     */
    workspacesDetailDetail: (id: string, params: RequestParams = {}) =>
      this.request<WorkspaceModel, void>({
        path: `/workspaces/${id}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name WorkspacesPartialUpdateDetail
     * @summary Patches the specified workspace
     * @request PATCH:/workspaces/{id}
     * @response `200` `string` The ID of the entity
     * @response `403` `void` The current user does not have rights to edit this workspace
     * @response `404` `void` No workspace was found for that id
     */
    workspacesPartialUpdateDetail: (id: string, data: WorkspacePatchModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/workspaces/${id}`,
        method: 'PATCH',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name WorkspacesUpdateDetail
     * @summary Updates the specified workspace
     * @request PUT:/workspaces/{id}
     * @response `200` `string` The ID of the entity
     * @response `403` `void` The current user does not have rights to edit this workspace
     * @response `404` `void` No workspace was found for that id
     */
    workspacesUpdateDetail: (id: string, data: WorkspaceUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/workspaces/${id}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name WorkspacesDeleteDetail
     * @summary Deletes the specified workspace and removes all of the community's associations from workspaces and nodes
     * @request DELETE:/workspaces/{id}
     * @response `403` `void` The current user does not have rights to delete this workspace
     * @response `404` `void` No workspace was found for that id
     */
    workspacesDeleteDetail: (id: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/workspaces/${id}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name DetailDetail
     * @summary Returns a single workspace including detailed stats
     * @request GET:/workspaces/{id}/detail
     * @response `200` `WorkspaceDetailModel` The workspace data including detailed stats
     * @response `404` `void` No workspace was found for that id
     */
    detailDetail: (id: string, params: RequestParams = {}) =>
      this.request<WorkspaceDetailModel, void>({
        path: `/workspaces/${id}/detail`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name AutocompleteList
     * @summary List the basic information of workspaces for a given search query. Only workspaces accessible to the currently logged in user will be returned
     * @request GET:/workspaces/autocomplete
     * @response `200` `(CommunityEntityMiniModel)[]`
     */
    autocompleteList: (
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModel[], any>({
        path: `/workspaces/autocomplete`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name NodesDetail
     * @request GET:/workspaces/{id}/nodes
     * @response `403` `string` The current user does not have rights to view this workspace's content
     * @response `404` `void` No workspace was found for that id
     */
    nodesDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<any, string | void>({
        path: `/workspaces/${id}/nodes`,
        method: 'GET',
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name OrganizationsDetail
     * @request GET:/workspaces/{id}/organizations
     * @response `403` `string` The current user does not have rights to view this workspace's content
     * @response `404` `void` No workspace was found for that id
     */
    organizationsDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<any, string | void>({
        path: `/workspaces/${id}/organizations`,
        method: 'GET',
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name CfpsDetail
     * @summary Lists all calls for proposals for the workspace
     * @request GET:/workspaces/{id}/cfps
     * @response `200` `(CallForProposalModel)[]`
     * @response `403` `string` The current user does not have rights to view this workspace's contents
     * @response `404` `void` No workspace was found for that id
     */
    cfpsDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CallForProposalModel[], string | void>({
        path: `/workspaces/${id}/cfps`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name PapersAggregateDetail
     * @summary Lists all papers for the specified workspace and its ecosystem
     * @request GET:/workspaces/{id}/papers/aggregate
     * @deprecated
     * @response `200` `(PaperModel)[]`
     * @response `403` `string` The current user does not have rights to view this workspace's content
     * @response `404` `void` No workspace was found for that id
     */
    papersAggregateDetail: (
      id: string,
      query?: {
        types?: CommunityEntityType[];
        communityEntityIds?: string[];
        type?: PaperType;
        tags?: PaperTag[];
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<PaperModel[], string | void>({
        path: `/workspaces/${id}/papers/aggregate`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name PapersAggregateCommunityEntitiesDetail
     * @summary Lists all community entities for papers for the specified workspace and its ecosystem
     * @request GET:/workspaces/{id}/papers/aggregate/communityEntities
     * @deprecated
     * @response `200` `(CommunityEntityMiniModel)[]`
     * @response `403` `void` The current user does not have rights to view this workspace's content
     * @response `404` `void` No workspace was found for that id
     */
    papersAggregateCommunityEntitiesDetail: (
      id: string,
      query?: {
        types?: CommunityEntityType[];
        type?: PaperType;
        tags?: PaperTag[];
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModel[], void>({
        path: `/workspaces/${id}/papers/aggregate/communityEntities`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name NeedsAggregateDetail
     * @request GET:/workspaces/{id}/needs/aggregate
     * @deprecated
     * @response `200` `(NeedModel)[]`
     * @response `403` `string` The current user does not have rights to view this workspace's content
     * @response `404` `void` No workspace was found for that id
     */
    needsAggregateDetail: (
      id: string,
      query?: {
        communityEntityIds?: string[];
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<NeedModel[], string | void>({
        path: `/workspaces/${id}/needs/aggregate`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name NeedsAggregateCommunityEntitiesDetail
     * @summary Lists all community entities for needs for the specified workspace and its ecosystem
     * @request GET:/workspaces/{id}/needs/aggregate/communityEntities
     * @deprecated
     * @response `200` `(CommunityEntityMiniModel)[]`
     * @response `403` `void` The current user does not have rights to view this workspace's content
     * @response `404` `void` No workspace was found for that id
     */
    needsAggregateCommunityEntitiesDetail: (
      id: string,
      query?: {
        types?: CommunityEntityType[];
        currentUser?: boolean;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityMiniModel[], void>({
        path: `/workspaces/${id}/needs/aggregate/communityEntities`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name WorkspacesDetail
     * @request GET:/workspaces/{id}/workspaces
     * @response `200` `(WorkspaceModel)[]` Workspace data
     * @response `403` `string` The current user does not have rights to view this workspace's content
     * @response `404` `void` No workspace was found for that id
     */
    workspacesDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<WorkspaceModel[], string | void>({
        path: `/workspaces/${id}/workspaces`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name PaperList
     * @request GET:/workspaces/paper
     * @deprecated
     * @response `404` `void` No project was found for that id
     */
    paperList: (
      query?: {
        /** External ID */
        externalID?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<any, void>({
        path: `/workspaces/paper`,
        method: 'GET',
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name DocumentsCreate
     * @summary Adds a new document for the specified entity.
     * @request POST:/workspaces/{id}/documents
     * @deprecated
     * @response `200` `string` The document was created
     * @response `400` `void` Note-typed documents have to be created and updated through the feed endpoints
     * @response `403` `void` The current user doesn't have sufficient rights to add documents for the entity
     * @response `404` `void` No entity was found for that id
     */
    documentsCreate: (id: string, data: DocumentInsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/workspaces/${id}/documents`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name DocumentsDetail
     * @summary Lists all documents for the specified entity, not including file data
     * @request GET:/workspaces/{id}/documents
     * @deprecated
     * @response `200` `string` The document was created
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
     * @response `404` `void` No entity was found for that id
     */
    documentsDetail: (
      id: string,
      query?: {
        folderId?: string;
        type?: DocumentFilter;
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string, void>({
        path: `/workspaces/${id}/documents`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name DocumentsDetailDetail
     * @summary Returns a single document, including the file represented as base64
     * @request GET:/workspaces/{id}/documents/{documentId}
     * @deprecated
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
     * @response `404` `void` No entity was found for that id or the document does not exist
     */
    documentsDetailDetail: (id: string, documentId: string, params: RequestParams = {}) =>
      this.request<any, void>({
        path: `/workspaces/${id}/documents/${documentId}`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name DocumentsUpdateDetail
     * @summary Updates the title and description for the document
     * @request PUT:/workspaces/{id}/documents/{documentId}
     * @deprecated
     * @response `200` `void` The document was updated
     * @response `403` `void` The current user doesn't have sufficient rights to edit documents for the entity
     * @response `404` `void` No entity was found for that id or the document does not exist
     */
    documentsUpdateDetail: (id: string, documentId: string, data: DocumentUpdateModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/${id}/documents/${documentId}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name DocumentsDeleteDetail
     * @summary Deletes the specified document
     * @request DELETE:/workspaces/{id}/documents/{documentId}
     * @deprecated
     * @response `200` `void` The document was deleted
     * @response `403` `void` The current user doesn't have sufficient rights to delete the document
     * @response `404` `void` No entity was found for that id or the document does not exist
     */
    documentsDeleteDetail: (id: string, documentId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/${id}/documents/${documentId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name DocumentsDraftDetail
     * @summary Returns a draft document for the specified container
     * @request GET:/workspaces/{id}/documents/draft
     * @deprecated
     * @response `204` `void` No draft document was found for the community entity
     * @response `404` `void` No community entity was found for the specified id
     */
    documentsDraftDetail: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/${id}/documents/draft`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name DocumentsAllDetail
     * @summary Lists all documents and folders for the specified entity, not including file data
     * @request GET:/workspaces/{id}/documents/all
     * @deprecated
     * @response `200` `(BaseModel)[]` The document was created
     * @response `403` `void` The current user doesn't have sufficient rights to view documents for the entity
     * @response `404` `void` No entity was found for that id
     */
    documentsAllDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<BaseModel[], void>({
        path: `/workspaces/${id}/documents/all`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name JoinCreate
     * @summary Joins an entity on behalf of the currently logged in user. Only works for community entities with the Open membership access level. Creates a membership record immediately, no invitation is created.
     * @request POST:/workspaces/{id}/join
     * @deprecated
     * @response `200` `string` ID of the new membership object
     * @response `403` `void` The entity is not open to members joining
     * @response `404` `void` The entity does not exist
     * @response `409` `void` A membership record already exists for the entity and user
     */
    joinCreate: (id: string, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/workspaces/${id}/join`,
        method: 'POST',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name RequestCreate
     * @summary Creates a request to join an entity on behalf of the currently logged in user
     * @request POST:/workspaces/{id}/request
     * @deprecated
     * @response `200` `string` ID of the new invitation object
     * @response `403` `void` The entity is not open to members requesting to join
     * @response `404` `void` The entity does not exist
     * @response `409` `void` An invitation already exists for the entity and user
     */
    requestCreate: (id: string, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/workspaces/${id}/request`,
        method: 'POST',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name InviteIdCreateDetail
     * @summary Invite a user to an entity using their ID
     * @request POST:/workspaces/{id}/invite/id/{userId}
     * @response `200` `string` ID of the new invitation object
     * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity, or the user is inviting an owner without having the rights to manage owners
     * @response `404` `void` The entity or user do not exist
     * @response `409` `void` An invitation already exists for the entity and user
     */
    inviteIdCreateDetail: (id: string, userId: string, data: AccessLevel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/workspaces/${id}/invite/id/${userId}`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name InviteEmailCreateDetail
     * @summary Invite a user to an entity using their email
     * @request POST:/workspaces/{id}/invite/email/{userEmail}
     * @response `200` `string` ID of the new invitation object
     * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
     * @response `404` `void` No entity was found for that id
     */
    inviteEmailCreateDetail: (id: string, userEmail: string, data: AccessLevel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/workspaces/${id}/invite/email/${userEmail}`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name InviteEmailBatchCreate
     * @summary Invite a user to an entity using their email
     * @request POST:/workspaces/{id}/invite/email/batch
     * @deprecated
     * @response `200` `string` ID of the new invitation object
     * @response `403` `void` The currently logged-in user does not have the rights to invite people to the specified entity
     * @response `404` `void` No entity was found for that id
     */
    inviteEmailBatchCreate: (id: string, data: string[], params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/workspaces/${id}/invite/email/batch`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name InvitesDetail
     * @summary List all pending invitations for a given entity
     * @request GET:/workspaces/{id}/invites
     * @response `200` `(InvitationModelUser)[]` A list of invitations
     * @response `404` `void` No entity was found for that id
     */
    invitesDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<InvitationModelUser[], void>({
        path: `/workspaces/${id}/invites`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name InvitesAcceptCreate
     * @summary Accept the invite on behalf of the currently logged-in user
     * @request POST:/workspaces/{id}/invites/{invitationId}/accept
     * @response `200` `void` The invitation has been accepted. The user is now a member of the project.
     * @response `403` `void` The current user does not have the right to accept the invitation
     * @response `404` `void` No entity was found for that id or the invitation does not exist
     */
    invitesAcceptCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/${id}/invites/${invitationId}/accept`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name InvitesRejectCreate
     * @summary Reject the invite on behalf of the currently logged-in user
     * @request POST:/workspaces/{id}/invites/{invitationId}/reject
     * @response `200` `void` The invitation has been rejected
     * @response `403` `void` The current user does not have the right to reject the invitation
     * @response `404` `void` No entity was found for that id or the invitation does not exist
     */
    invitesRejectCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/${id}/invites/${invitationId}/reject`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name InvitesCancelCreate
     * @summary Cancels an invite
     * @request POST:/workspaces/{id}/invites/{invitationId}/cancel
     * @response `200` `void` The invitation has been cancelled
     * @response `403` `void` The current user does not have the right to cancel the invitation
     * @response `404` `void` No entity was found for that id or the invitation does not exist
     */
    invitesCancelCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/${id}/invites/${invitationId}/cancel`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name InvitesResendCreate
     * @summary Resends an invite
     * @request POST:/workspaces/invites/{invitationId}/resend
     * @response `200` `void` The invitation has been resent
     * @response `400` `void` The invitation is a request and cannot be resent
     * @response `403` `void` The user does not have the right to resend the invitation
     * @response `404` `void` No entity was found for that id or the invitation does not exist
     */
    invitesResendCreate: (invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/invites/${invitationId}/resend`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name LeaveCreate
     * @summary Leaves an entity on behalf of the currently logged in user
     * @request POST:/workspaces/{id}/leave
     * @deprecated
     * @response `200` `void` The user has successfully left the entity
     * @response `404` `void` The entity does not exist or the user is not a member
     */
    leaveCreate: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/${id}/leave`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name OnboardingResponsesDetailDetail
     * @summary List onboarding responses for a community entity and a user
     * @request GET:/workspaces/{id}/onboardingResponses/{userId}
     * @deprecated
     * @response `200` `OnboardingQuestionnaireInstanceModel` onboarding questionnaire responses
     * @response `404` `void` No entity was found for that id or no user was found
     */
    onboardingResponsesDetailDetail: (id: string, userId: string, params: RequestParams = {}) =>
      this.request<OnboardingQuestionnaireInstanceModel, void>({
        path: `/workspaces/${id}/onboardingResponses/${userId}`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name OnboardingResponsesCreate
     * @summary Posts onboarding responses for a community entity for the current user
     * @request POST:/workspaces/{id}/onboardingResponses
     * @deprecated
     * @response `200` `void` The onboarding questionnaire responses were saved successfully
     * @response `404` `void` No entity was found for that id
     */
    onboardingResponsesCreate: (
      id: string,
      data: OnboardingQuestionnaireInstanceUpsertModel,
      params: RequestParams = {},
    ) =>
      this.request<void, void>({
        path: `/workspaces/${id}/onboardingResponses`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name OnboardingCompletionCreate
     * @summary Records the completion of the onboarding workflow for a community entity for the current user
     * @request POST:/workspaces/{id}/onboardingCompletion
     * @deprecated
     * @response `200` `void` The onboarding completion was recorded successfully
     * @response `404` `void` No entity was found for that id
     */
    onboardingCompletionCreate: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/${id}/onboardingCompletion`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name MembersDetail
     * @summary List all members for an entity
     * @request GET:/workspaces/{id}/members
     * @response `200` `(MemberModel)[]` A list of members
     * @response `403` `void` The current user doesn't have sufficient rights to see the entity's members
     * @response `404` `void` No entity was found for that id
     */
    membersDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<MemberModel[], void>({
        path: `/workspaces/${id}/members`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name InvitationDetail
     * @summary Returns the pending invitation for an object for the current user
     * @request GET:/workspaces/{id}/invitation
     * @response `200` `InvitationModelEntity` A pending invitation
     * @response `404` `void` No invitation is pending or no entity was found for that id
     */
    invitationDetail: (id: string, params: RequestParams = {}) =>
      this.request<InvitationModelEntity, void>({
        path: `/workspaces/${id}/invitation`,
        method: 'GET',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name MembersUpdateDetail
     * @summary Updates a member's access level for an entity
     * @request PUT:/workspaces/{id}/members/{memberId}
     * @response `200` `void` The access level has been updated
     * @response `403` `void` The user does not have the right to set this access level for this member
     * @response `404` `void` No entity was found for that id or the user is not a member of that entity
     */
    membersUpdateDetail: (id: string, memberId: string, data: AccessLevel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/${id}/members/${memberId}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name MembersDeleteDetail
     * @summary Removes a member's access from an entity
     * @request DELETE:/workspaces/{id}/members/{memberId}
     * @response `200` `void` The member has been removed
     * @response `403` `void` The current user does not have the right to remove members from this entity
     * @response `404` `void` No entity was found for that id or the user is not a member of that entity
     */
    membersDeleteDetail: (id: string, memberId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/${id}/members/${memberId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name MembersContributionUpdate
     * @summary Updates a member's contribution
     * @request PUT:/workspaces/{id}/members/{memberId}/contribution
     * @response `200` `void` The contribution has been updated
     * @response `403` `void` The user does not have the right to set the contribution for other members
     * @response `404` `void` No entity was found for that id or the user is not a member of that entity
     */
    membersContributionUpdate: (id: string, memberId: string, data: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/${id}/members/${memberId}/contribution`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name MembersLabelsUpdate
     * @summary Updates a member's labels
     * @request PUT:/workspaces/{id}/members/{memberId}/labels
     * @response `200` `void` The labels have been updated
     * @response `403` `void` The current user does not have the right to set labels for members
     * @response `404` `void` No entity was found for that id or the user is not a member of that entity
     */
    membersLabelsUpdate: (id: string, memberId: string, data: string[], params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/${id}/members/${memberId}/labels`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name CommunityEntityInviteCreateDetail
     * @summary Invite a community entity to become affiliated to another entity
     * @request POST:/workspaces/{id}/communityEntityInvite/{targetEntityId}
     * @response `200` `string` ID of the new invitation object
     * @response `400` `void` An invitation cannot be extended to a community entity of the same type
     * @response `403` `void` The current user does not have the rights to invite entities to the specified entity
     * @response `404` `void` One of the entities does not exist
     * @response `409` `void` An invitation or affiliation already exists for the entities
     */
    communityEntityInviteCreateDetail: (id: string, targetEntityId: string, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/workspaces/${id}/communityEntityInvite/${targetEntityId}`,
        method: 'POST',
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name CommunityEntityInvitesIncomingDetail
     * @summary List all pending invitations for a given entity
     * @request GET:/workspaces/{id}/communityEntityInvites/incoming
     * @response `200` `(CommunityEntityInvitationModelSource)[]` A list of invitations
     * @response `404` `void` No entity was found for that id
     */
    communityEntityInvitesIncomingDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityInvitationModelSource[], void>({
        path: `/workspaces/${id}/communityEntityInvites/incoming`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name CommunityEntityInvitesOutgoingDetail
     * @summary List all pending invitations for a given entity
     * @request GET:/workspaces/{id}/communityEntityInvites/outgoing
     * @response `200` `(CommunityEntityInvitationModelSource)[]` A list of invitations
     * @response `404` `void` No entity was found for that id
     */
    communityEntityInvitesOutgoingDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommunityEntityInvitationModelSource[], void>({
        path: `/workspaces/${id}/communityEntityInvites/outgoing`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name CommunityEntityInvitesAcceptCreate
     * @summary Accept an invite
     * @request POST:/workspaces/{id}/communityEntityInvites/{invitationId}/accept
     * @response `200` `void` The invitation has been accepted
     * @response `403` `void` The current user does not have the right to accept the invitation
     * @response `404` `void` One of the entities does not exist or the invitation does not exist
     */
    communityEntityInvitesAcceptCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/${id}/communityEntityInvites/${invitationId}/accept`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name CommunityEntityInvitesRejectCreate
     * @summary Rejects an invite
     * @request POST:/workspaces/{id}/communityEntityInvites/{invitationId}/reject
     * @response `200` `void` The invitation has been rejected
     * @response `403` `void` The current user does not have the right to reject the invitation
     * @response `404` `void` One of the entities does not exist or the invitation does not exist
     */
    communityEntityInvitesRejectCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/${id}/communityEntityInvites/${invitationId}/reject`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name CommunityEntityInvitesCancelCreate
     * @summary Cancels an invite
     * @request POST:/workspaces/{id}/communityEntityInvites/{invitationId}/cancel
     * @response `200` `void` The invitation has been cancelled
     * @response `403` `void` The current user does not have the right to cancel the invitation
     * @response `404` `void` One of the entities does not exist or the invitation does not exist
     */
    communityEntityInvitesCancelCreate: (id: string, invitationId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/${id}/communityEntityInvites/${invitationId}/cancel`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name CommunityEntityRemoveCreate
     * @summary Removes an affiliation of an entity to another entity
     * @request POST:/workspaces/{id}/communityEntity/{targetEntityId}/remove
     * @response `200` `void` The entity affiliation has been removed successfully
     * @response `403` `void` The current user does not have the permissions to manage the community entity's affiliations
     * @response `404` `void` One of the entities does not exist or the entities are not affiliated
     */
    communityEntityRemoveCreate: (id: string, targetEntityId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/${id}/communityEntity/${targetEntityId}/remove`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name CommunityEntityLinkCreate
     * @summary Creates an affiliation of an entity to another entity. On the entity, the user must be admin or owner, or the toggle for members to allow creating community entities of the respective type must be on. The user must be admin or owner on the target entity.
     * @request POST:/workspaces/{id}/communityEntity/{targetEntityId}/link
     * @response `200` `void` The entity affiliation has been created successfully
     * @response `409` `void` One of the entities does not exist or the entities are already affiliated
     */
    communityEntityLinkCreate: (id: string, targetEntityId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/${id}/communityEntity/${targetEntityId}/link`,
        method: 'POST',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name EcosystemCommunityEntitiesDetail
     * @summary Lists all ecosystem containers (projects, communities and nodes) for the given community entity
     * @request GET:/workspaces/{id}/ecosystem/communityEntities
     * @deprecated
     * @response `200` `(EntityMiniModel)[]`
     * @response `403` `string` The current user does not have rights to view this community entity's contents
     * @response `404` `void` No community entity was found for that id
     */
    ecosystemCommunityEntitiesDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EntityMiniModel[], string | void>({
        path: `/workspaces/${id}/ecosystem/communityEntities`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name EcosystemUsersDetail
     * @summary Lists all ecosystem members for the given community entity
     * @request GET:/workspaces/{id}/ecosystem/users
     * @response `200` `(EntityMiniModel)[]`
     * @response `403` `string` The current user does not have rights to view this community entity's contents
     * @response `404` `void` No community entity was found for that id
     */
    ecosystemUsersDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EntityMiniModel[], string | void>({
        path: `/workspaces/${id}/ecosystem/users`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name NeedsCreate
     * @summary Adds a new need for the specified project
     * @request POST:/workspaces/{id}/needs
     * @deprecated
     * @response `200` `string` The ID of the new need
     * @response `403` `void` The current user doesn't have sufficient rights to add needs for the entity
     */
    needsCreate: (id: string, data: NeedUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/workspaces/${id}/needs`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name NeedsDetail
     * @summary Lists all needs for the specified community entity
     * @request GET:/workspaces/{id}/needs
     * @deprecated
     * @response `200` `(NeedModel)[]`
     * @response `403` `string` The current user does not have rights to view this community entity's contents
     * @response `404` `void` No community entity was found for that id
     */
    needsDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<NeedModel[], string | void>({
        path: `/workspaces/${id}/needs`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name NeedsDetailDetail
     * @summary Returns a single need
     * @request GET:/workspaces/needs/{needId}
     * @deprecated
     * @response `403` `string` The current user does not have rights to view this community entity's contents
     * @response `404` `void` No need was found for the need id
     */
    needsDetailDetail: (needId: string, params: RequestParams = {}) =>
      this.request<any, string | void>({
        path: `/workspaces/needs/${needId}`,
        method: 'GET',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name NeedsUpdateDetail
     * @summary Updates the need
     * @request PUT:/workspaces/needs/{needId}
     * @deprecated
     * @response `200` `void` The need was updated
     * @response `403` `void` The current user does not have rights to update needs on this community entity
     * @response `404` `void` No need was found for the need id
     */
    needsUpdateDetail: (needId: string, data: NeedUpsertModel, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/needs/${needId}`,
        method: 'PUT',
        body: data,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name NeedsDeleteDetail
     * @summary Deletes the specified need
     * @request DELETE:/workspaces/needs/{needId}
     * @deprecated
     * @response `200` `void` The need was deleted
     * @response `403` `void` The current user does not have rights to delete needs on this community entity
     * @response `404` `void` No need was found for the need id
     */
    needsDeleteDetail: (needId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/workspaces/needs/${needId}`,
        method: 'DELETE',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name EventsCreate
     * @summary Adds a new event for the specified entity.
     * @request POST:/workspaces/{id}/events
     * @deprecated
     * @response `200` `string` The event was created
     * @response `403` `void` The current user doesn't have sufficient rights to add events for the entity
     * @response `404` `void` No entity was found for that id
     */
    eventsCreate: (id: string, data: EventUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/workspaces/${id}/events`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name EventsDetail
     * @summary Lists events for the specified entity.
     * @request GET:/workspaces/{id}/events
     * @deprecated
     * @response `200` `(EventModel)[]` Event data
     * @response `403` `void` The current user doesn't have sufficient rights to view events for the entity
     * @response `404` `void` No entity was found for that id
     */
    eventsDetail: (
      id: string,
      query?: {
        /** @format date-time */
        from?: string;
        /** @format date-time */
        to?: string;
        tags?: EventTag[];
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EventModel[], void>({
        path: `/workspaces/${id}/events`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name ChannelsCreate
     * @summary Adds a new channel for the specified entity.
     * @request POST:/workspaces/{id}/channels
     * @deprecated
     * @response `200` `string` The channel was created
     * @response `403` `void` The current user doesn't have sufficient rights to add discussion channels for the entity
     * @response `404` `void` No entity was found for that id
     */
    channelsCreate: (id: string, data: ChannelUpsertModel, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/workspaces/${id}/channels`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Workspace
     * @name ChannelsDetail
     * @summary Lists channels for the specified entity.
     * @request GET:/workspaces/{id}/channels
     * @deprecated
     * @response `200` `(ChannelModel)[]` channel data
     * @response `403` `void` The current user doesn't have sufficient rights to view channels for the entity
     * @response `404` `void` No entity was found for that id
     */
    channelsDetail: (
      id: string,
      query?: {
        /**
         * The page to fetch. Pages start at 1.
         * @format int32
         * @min 1
         * @max 1000
         */
        Page?: number;
        /**
         * Size of one page. The default is 50. Maximum value is 1000
         * @format int32
         * @min 1
         * @max 1000
         */
        PageSize?: number;
        SortKey?: SortKey;
        SortAscending?: boolean;
        /** The search query to filter projects by */
        Search?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ChannelModel[], void>({
        path: `/workspaces/${id}/channels`,
        method: 'GET',
        query: query,
        format: 'json',
        ...params,
      }),
  };
}
