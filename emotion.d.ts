import '@emotion/react';
import { Theme as CustomTheme } from '~/src/utils/theme/theme';
import { AppAliasToken } from '~/src/utils/theme/index.type';

type EmotionCustomTheme = CustomTheme & { token: AppAliasToken };
declare module '@emotion/react' {
  export interface Theme extends EmotionCustomTheme {}
}
