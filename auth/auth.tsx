import { ReactNode, createContext, useContext, useState } from 'react';
import cookie from 'js-cookie';
import useSyncLogout from '~/src/hooks/useSyncLogout';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';

export interface AuthData {
  userId: string;
  token: string;
}

export interface AuthContext {
  isLoggedIn: boolean;
  userId: string | null;
  setUserId: (id: string | null) => void;
  accessToken: string | null;
  setAccessToken: (newToken: string) => void;
  onLoginUser: (data: AuthData) => void;
}

const Auth = createContext<AuthContext>({
  isLoggedIn: false,
  userId: null,
  setUserId: () => {},
  accessToken: null,
  setAccessToken: () => {},
  onLoginUser: () => {},
});

const COOKIE_EXPIRATION = 30; // In days

interface AuthProviderProps {
  children: ReactNode;
}

export const AuthProvider = ({ children }: AuthProviderProps) => {
  const [token, setToken] = useState<string | null>(cookie.get('authorization'));
  const [id, setId] = useState<string | null>(cookie.get('userId'));

  const { initializeHubs } = useHubDiscussionsStore((store) => ({
    initializeHubs: store.initialize,
  }));

  const setAccessToken = (newToken: string | null) => {
    setToken(newToken);
  };

  const setUserId = (id: string | null) => {
    setId(id);
  };
  const isLoggedIn = !!token;

  const clearCredentials = () => {
    setAccessToken('');
    setUserId('');
  };

  const onLoginUser = async ({ userId, token }) => {
    const authorization = `Bearer ${token}`;
    cookie.set('authorization', authorization, { expires: COOKIE_EXPIRATION });
    cookie.set('userId', userId, { expires: COOKIE_EXPIRATION });

    //initialize hubs
    initializeHubs();

    // Sync login between pages
    typeof window !== 'undefined' && window.localStorage.setItem('login', Date.now().toString());
    setAccessToken(authorization);
    setUserId(userId);
  };

  useSyncLogout(clearCredentials);

  return (
    <Auth.Provider value={{ isLoggedIn, userId: id, setUserId, accessToken: token, setAccessToken, onLoginUser }}>
      {children}
    </Auth.Provider>
  );
};

export function useAuth() {
  return useContext(Auth);
}
