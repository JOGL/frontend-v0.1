#!/bin/sh

# Adapted from https://toedter.com/2018/06/02/heroku-docker-deployment-update/
# Modified by Leo Blondel
# Last edit 03/24/2020

# Modified by Luis Arias and Isabelle Lafont
# Last edit 02/15/2021

# Variables that are set in settings are:
# - APP_ID=YOUR_APP_ID
# - APP_TOKEN=YOUR_APP_TOKEN
# - BACKEND_URL=https://url.of.the./backend
# - FRONTEND_URL=https://url.of.the./frontend

# First we build the docker image, using the ENV var set by the environment variables
apk add py-pip python3-dev libffi-dev openssl-dev gcc libc-dev rust cargo make docker-compose
docker-compose build

#Then we login to Azure
docker login -u $AZURE_CR_USERNAME -p $AZURE_CR_PASSWORD joglr.azurecr.io

docker tag web joglr.azurecr.io/$APP/web

# And push the new docker image to the Azure registry
docker push joglr.azurecr.io/$APP/web
