const nextTranslate = require('next-translate');

/* eslint-disable no-param-reassign */
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

// const withCSS = require('@zeit/next-css');

const nextConfig = {
  typescript: {
    // !! WARNING !!
    // Dangerously allow production builds to successfully complete even if your project has type errors.
    ignoreBuildErrors: true,
  },
  // manage env variables
  env: {
    ADDRESS_BACK: process.env.ADDRESS_BACK,
    ADDRESS_WSS: process.env.ADDRESS_WSS,
    ADDRESS_FRONT: process.env.ADDRESS_FRONT,
    GOOGLE_ANALYTICS_ID: process.env.GOOGLE_ANALYTICS_ID,
    GOOGLE_ANALYTICS4_ID: process.env.GOOGLE_ANALYTICS4_ID,
    TAGMANAGER_ARGS_ID: process.env.TAGMANAGER_ARGS_ID,
    HOTJAR_ID: process.env.HOTJAR_ID,
    ORCID_URL: process.env.ORCID_URL,
    ORCID_CLIENT_ID: process.env.ORCID_CLIENT_ID,
    ORCID_REDIRECT_URL: process.env.ORCID_REDIRECT_URL,
    GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
    GOOGLE_MAPS_API_KEY: process.env.GOOGLE_MAPS_API_KEY,
    LINKEDIN_CLIENT_ID: process.env.LINKEDIN_CLIENT_ID,
    NEXT_PUBLIC_ENVIRONMENT: process.env.NEXT_PUBLIC_ENVIRONMENT,
    NEXT_PUBLIC_RECAPTCHA_KEY: process.env.NEXT_PUBLIC_RECAPTCHA_KEY,
    GITHUB_CLIENT_ID: process.env.GITHUB_CLIENT_ID,
    GITHUB_REDIRECT_URL: process.env.GITHUB_REDIRECT_URL,
    HUGGINGFACE_CLIENT_ID: process.env.HUGGINGFACE_CLIENT_ID,
    HUGGINGFACE_REDIRECT_URL: process.env.HUGGINGFACE_REDIRECT_URL,
  },
  // image optimization config (https://nextjs.org/docs/basic-features/image-optimization)
  images: {
    deviceSizes: [480, 768, 1280],
    domains: [
      'jogl-dev-api.azurewebsites.net',
      'jogl-staging-api.azurewebsites.net',
      'jogl-prod-api.azurewebsites.net',
      'backendjogl.azurewebsites.net',
      'localhost',
      'img.evbuc.com',
    ],
    disableStaticImages: true,
    unoptimized: true, //Azure can't deal with next/image optimizations
  },
  eslint: {
    // Warning: This allows production builds to successfully complete even if your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
  webpack: (config, { isServer }) => {
    // Fixes npm packages that depend on `fs` and `module` modules
    if (!isServer) {
      config.resolve.fallback.fs = false;
      config.resolve.fallback.module = false;
    }
    return config;
  },
  cleanDistDir: false,
  //This works, our project is just full of misconfigurations that result in warnings like this one
  async redirects() {
    return [
      {
        source: '/project/:path*',
        destination: '/workspace/:path*',
        permanent: true,
      },
      {
        source: '/community/:path*',
        destination: '/workspace/:path*',
        permanent: true,
      },
    ];
  },
};

module.exports = nextTranslate(withBundleAnalyzer(nextConfig));
