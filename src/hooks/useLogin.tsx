import { useRouter } from 'next/router';
import { useAuth } from '~/auth/auth';

const useLogin = () => {
  const router = useRouter();
  const { isLoggedIn } = useAuth();

  const signin = () => {
    if (router.pathname === '/') router.push({ pathname: '/signin' });
    else router.push({ pathname: '/signin', query: { redirectUrl: router.asPath } });
  };

  const signout = () => {
    router.push('/signout');
  };

  const handle403 = () => {
    if (isLoggedIn === false) signin();
    else router.push('/403');
  };

  return [signin, signout, handle403];
};

export default useLogin;
