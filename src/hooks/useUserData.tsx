import { useAuth } from '~/auth/auth';
import useGet from '~/src/hooks/useGet';
import { User } from '~/src/types';

export default function useUserData() {
  const { userId } = useAuth();
  const { data, error: userDataError, ...rest } = useGet<User>(userId && `/users/${userId}`);
  return { userData: data, userDataError, ...rest };
}
