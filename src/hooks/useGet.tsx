import useSWR, { SWRConfiguration, SWRResponse } from 'swr';
import { AxiosRequestConfig, AxiosResponse, AxiosError } from 'axios';
import { useApi } from '~/src/contexts/apiContext';

export type GetRequest = AxiosRequestConfig | null | string;

interface Return<Data, Error>
  extends Pick<SWRResponse<AxiosResponse<Data>, AxiosError<Error>>, 'isValidating' | 'isLoading' | 'error' | 'mutate'> {
  data: Data | undefined;
  response: AxiosResponse<Data> | undefined;
}

export interface Config<Data = unknown, Error = unknown>
  extends Omit<SWRConfiguration<AxiosResponse<Data>, AxiosError<Error>>, 'fallbackData'> {
  fallbackData?: Data;
}

export default function useGet<Data = unknown, Error = unknown>(
  request: GetRequest,
  { fallbackData, ...config }: Config<Data, Error> = {}
): Return<Data, Error> {
  const api = useApi();
  const { data: response, error, isLoading, isValidating, mutate } = useSWR<AxiosResponse<Data>, AxiosError<Error>>(
    request,
    // () => axios.request<Data>(request!),
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    () => api.get(request),
    {
      ...config,
      fallbackData:
        fallbackData &&
        ({
          status: 200,
          statusText: 'InitialData',
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          config: request!,
          headers: {},
          data: fallbackData,
        } as AxiosResponse<Data>),
      keepPreviousData: true,
      revalidateOnFocus: false,
    }
  );

  return {
    data: response && response.data,
    response,
    isLoading,
    error,
    isValidating,
    mutate,
  };
}
