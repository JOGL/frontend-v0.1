import { Grid } from 'antd';

const { useBreakpoint: useAntBreakpoint } = Grid;

const useMobile = () => {
  const screens = useAntBreakpoint();
  const isMobile = screens.xs;
  return isMobile;
};

export default useMobile;
