import { useContext } from 'react';
import { UserContext } from '~/src/contexts/UserProvider';

const useUser = () => useContext(UserContext);
export default useUser;
