import { useEffect, useState } from 'react';
import { Grid } from 'antd';

const { useBreakpoint: useAntBreakpoint } = Grid;

// This hook captures the user device width and returns corresponding breakpoint name
const useBreakpoint = () => {
  const screens = useAntBreakpoint();
  const [breakpoint, setBreakpoint] = useState('desktop');

  useEffect(() => {
    if (screens.md) setBreakpoint('desktop');
    // else if (screens.md) setBreakpoint('laptop');
    else if (screens.sm) setBreakpoint('tablet');
    else if (screens.xs) setBreakpoint('mobile');
  }, [screens]);

  return breakpoint;
};

export default useBreakpoint;
