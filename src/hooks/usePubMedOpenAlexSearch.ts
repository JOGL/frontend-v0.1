import { useState, useEffect, useMemo } from 'react';
import { useApi } from '~/src/contexts/apiContext';
import { ExternalSearchResult, Publication } from '~/src/types';
import { confAlert } from '~/src/utils/utils';

export default function usePubMedOpenAlexSearch(type: string, searchText: string) {
  const [loading, setLoading] = useState(false);
  const [loadingMore, setLoadingMore] = useState(false);
  const [showLoadMoreBtn, setShowLoadMoreBtn] = useState(false);
  const [pageOffset, setPageOffset] = useState(1);
  const [results, setResults] = useState<Publication[]>([]);
  const [total, setTotal] = useState<number>(0);
  const api = useApi();

  const debounce = (func: (...args: unknown[]) => void, timeout = 300) => {
    let timer: NodeJS.Timeout;
    return (...args: unknown[]) => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        func.apply(this, args);
      }, timeout);
    };
  };

  const getData = async (params) => {
    try {
      const url = `/users/papers/${type}/`;
      const pageSize = 50;

      const { data } = await api.get<ExternalSearchResult<Publication>>(url, {
        params: { ...params, pageSize },
      });

      return data;
    } catch (err) {
      console.warn(err);
      setShowLoadMoreBtn(false);
      confAlert.fire({ icon: 'error', title: 'Unable to fetch data' });
    }
  };

  const loadMore = async () => {
    setLoadingMore(true);
    try {
      if (searchText.length > 0) {
        const data = await getData({ Search: searchText, Page: pageOffset + 1 });

        setPageOffset((prevState) => prevState + 1);
        setShowLoadMoreBtn(results.length + data.items.length < data.total);

        if (data.items.length > 0) {
          setResults((prevState) => [...prevState, ...data.items]);
        }
      } else {
        setResults([]);
        setShowLoadMoreBtn(false);
      }
    } catch (e) {
      console.warn('Err While Searching With API:: ', e);
    }
    setLoadingMore(false);
  };

  const fetchData = async (searchText: string) => {
    setLoading(true);
    try {
      if (searchText.length > 0) {
        const data = await getData({ Search: searchText, Page: 1 });

        if (data.items.length < data.total) {
          setPageOffset(1);
          setShowLoadMoreBtn(true);
        }

        setTotal(data.total);
        setResults(data.items);
      } else {
        setTotal(0);
        setResults([]);
        setShowLoadMoreBtn(false);
      }
    } catch (e) {
      setTotal(0);
      setResults([]);
      setShowLoadMoreBtn(false);
      console.warn('Err While Searching With API:: ', e);
    }
    setLoading(false);
  };

  const searchApi = useMemo(() => debounce(fetchData, 1200), []);

  useEffect(() => {
    searchApi(searchText);
    return () => {};
  }, [searchText]);

  return { loading, loadingMore, showLoadMoreBtn, results, total, loadMore };
}
