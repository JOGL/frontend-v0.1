import useTranslation from 'next-translate/useTranslation';

const useTitleHelper = () => {
  const { t } = useTranslation('common');

  const getTitle = (title: string) => {
    if (!title) return t('untitled');
    if (title === 'General') return t('general');

    return title;
  };

  return [getTitle];
};

export default useTitleHelper;
