import { useEffect } from 'react';

// It allows all tabs to be in sync at logout
const syncLogout = (event, clearCredentials) => {
  if (event.key === 'logout') {
    // Remove all credentials
    clearCredentials();
    window.location.href=('/signin');
  } else if (event.key === 'login') {
    window.location.reload();
  }
};
export default function useSyncLogout(clearCredentials) {
  useEffect(() => {
    // sync logout across tabs
    window.addEventListener('storage', (e) => syncLogout(e, clearCredentials));
    return () => {
      window.removeEventListener('storage', (e) => syncLogout(e, clearCredentials));
      window.localStorage.removeItem('logout');
      window.localStorage.removeItem('login');
    };
  }, [clearCredentials]);
}
