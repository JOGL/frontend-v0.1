import { SWRInfiniteConfigInterface, SWRInfiniteResponseInterface } from 'swr';
import useSWRInfinite from 'swr/infinite';
import { AxiosRequestConfig, AxiosResponse, AxiosError } from 'axios';
import { useApi } from '~/src/contexts/apiContext';

export type GetRequest = AxiosRequestConfig | string | null;

interface InfiniteReturn<Data, Error>
  extends Pick<
    SWRInfiniteResponseInterface<AxiosResponse<Data>, AxiosError<Error>>,
    'isValidating' | 'isLoading' | 'error' | 'mutate' | 'size' | 'setSize'
  > {
  data: Data[] | undefined;
  response: AxiosResponse<Data>[] | undefined;
}

export interface InfiniteConfig<Data = unknown, Error = unknown>
  extends Omit<SWRInfiniteConfigInterface<AxiosResponse<Data>, AxiosError<Error>>, 'fallbackData'> {
  fallbackData?: Data[];
}

export default function useInfiniteLoading<Data = unknown, Error = unknown>(
  getRequest: (index: number, previousPageData: AxiosResponse<Data> | null) => GetRequest,
  { fallbackData, ...config }: InfiniteConfig<Data, Error> = {}
): InfiniteReturn<Data, Error> {
  const api = useApi();
  const { data: response, error, isValidating, isLoading, mutate, size, setSize } = useSWRInfinite<
    AxiosResponse<Data>,
    AxiosError<Error>
  >(
    (index, previousPageData) => {
      const key = getRequest(index, previousPageData);
      return key ? JSON.stringify(key) : null;
    },
    (request) => api.get(JSON.parse(request)),
    {
      ...config,
      fallbackData:
        fallbackData &&
        fallbackData.map((i) => ({
          status: 200,
          statusText: 'fallbackData',
          config: {},
          headers: {},
          data: i,
        })),
    }
  );

  return {
    data: response && response.map((r) => r.data),
    response,
    error,
    isValidating,
    isLoading,
    mutate,
    size,
    setSize,
  };
}
