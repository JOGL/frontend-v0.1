import { useState, useEffect } from 'react';

export default function usePagedTable(data: any[], page: number, rowsPerPage: number) {
  const [slice, setSlice] = useState<any[]>();
  const [totalPages, setTotalPages] = useState<number>(0);

  useEffect(() => {
    if (data) {
      const totalPages = Math.ceil(data.length / rowsPerPage);
      setTotalPages(totalPages);

      const slice = sliceData(data, page, rowsPerPage);
      setSlice([...slice]);
    }
  }, [data, page, rowsPerPage, setSlice]);

  return { slice, totalPages };
}

const sliceData = (data: any[], page: number, rowsPerPage: number) => {
  return data?.slice((page - 1) * rowsPerPage, page * rowsPerPage);
};
