import { FeedIntegrationType } from '~/__generated__/types';

export const useValidateIntegrationUrl = () => {
  const parseFeedIntegration = (url: string) => {
    const githubRegex = 'github.com/([^/]+/[^/]+)';
    const huggingfaceRegex = 'huggingface.co(/spaces)?(/datasets)?/([^/]+/[^/]+)';

    const githubMatch = url.match(githubRegex);
    if (githubMatch)
      return {
        type: FeedIntegrationType.Github,
        source_id: githubMatch[1],
        source_url: `https://github.com/${githubMatch[1]}`,
      };

    const huggingFaceMatch = url.match(huggingfaceRegex);
    if (huggingFaceMatch) {
      //for urls pointing to spaces
      if (huggingFaceMatch[1])
        return {
          type: FeedIntegrationType.Huggingface,
          source_id: 'spaces/' + huggingFaceMatch[3],
          source_url: `https://huggingface.co/spaces/${huggingFaceMatch[3]}`,
        };

      //for urls pointing to datasets
      if (huggingFaceMatch[2])
        return {
          type: FeedIntegrationType.Huggingface,
          source_id: 'datasets/' + huggingFaceMatch[3],
          source_url: `https://huggingface.co/datasets/${huggingFaceMatch[3]}`,
        };

      //for urls pointing to models
      return {
        type: FeedIntegrationType.Huggingface,
        source_id: 'models/' + huggingFaceMatch[3],
        source_url: `https://huggingface.co/${huggingFaceMatch[3]}`,
      };
    }
    return null;
  };

  return { parseFeedIntegration };
};
