import { useEffect } from 'react';

// Function to save the user-defined language: https://github.com/vinissimus/next-translate#10-how-to-save-the-user-defined-language
export default function usePersistLocaleCookie(locale, defaultLocale) {
  function persistLocaleCookie() {
    const date = new Date();
    const expireMs = 100 * 365 * 24 * 60 * 60 * 1000; // 100 days
    date.setTime(date.getTime() + expireMs);
    document.cookie = `NEXT_LOCALE=${locale};expires=${date.toUTCString()};path=/`;
  }

  useEffect(persistLocaleCookie, [locale, defaultLocale]);
}
