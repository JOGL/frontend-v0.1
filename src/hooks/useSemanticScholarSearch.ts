import { useState, useEffect, useMemo } from 'react';
import { useApi } from '~/src/contexts/apiContext';
import { Author, ExternalSearchResult, Publication } from '~/src/types';
import { confAlert } from '~/src/utils/utils';

export default function useSemantiScholarSearch(/*type: string, */ searchText: string) {
  const [loading, setLoading] = useState(false);
  const [loadingMore, setLoadingMore] = useState(false);
  const [showLoadMoreBtn, setShowLoadMoreBtn] = useState(false);
  const [pageOffset, setPageOffset] = useState(1);
  const [results, setResults] = useState<Publication[]>([]);
  const [total, setTotal] = useState<number>(0);
  const api = useApi();

  const debounce = (func: (...args: unknown[]) => void, timeout = 300) => {
    let timer: NodeJS.Timeout;
    return (...args: unknown[]) => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        func.apply(this, args);
      }, timeout);
    };
  };

  /*const getAuthorData = async (params) => {
    try {
      const url = '/users/papers/s2/author';

      const pageSize = 10;

      const { data } = await api.get<ExternalSearchResult<Author>>(url, {
        params: {
          ...params,
          pageSize,
        },
      });

      return data;
    } catch (err) {
      console.warn(err);
      setShowLoadMoreBtn(false);
      confAlert.fire({ icon: 'error', title: 'Unable to fetch data' });
    }
  };*/

  const getPublicationData = async (params) => {
    try {
      const url = '/users/papers/s2/paper';
      const pageSize = 50;

      const { data } = await api.get<ExternalSearchResult<Publication>>(url, {
        params: {
          ...params,
          pageSize,
        },
      });

      return data;
    } catch (err) {
      console.warn(err);
      setShowLoadMoreBtn(false);
      confAlert.fire({ icon: 'error', title: 'Unable to fetch data' });
    }
  };

  const loadMore = async () => {
    setLoadingMore(true);
    try {
      if (searchText.length > 0) {
        let data: ExternalSearchResult<any>;
        //This endpoint doesn't exist yet
        /* if (type === 'user') {
          data = await getAuthorData({ Search: searchText, Page: pageOffset + 1 });

          setShowLoadMoreBtn(results.length + data.items.length < data.total);

          if (data.items.length > 0) {
            setResults((prevState) => [...prevState, ...data.items.flatMap((a) => a.papers)]);
          }
        } else {*/
        data = await getPublicationData({ Search: searchText, Page: pageOffset + 1 });

        setShowLoadMoreBtn(results.length + data.items.length < data.total);

        if (data.items.length > 0) {
          setResults((prevState) => [...prevState, ...data.items]);
        }

        setPageOffset((prevState) => prevState + 1);
      } else {
        setResults([]);
        setShowLoadMoreBtn(false);
      }
    } catch (e) {
      console.warn('Err While Searching With API:: ', e);
    }
    setLoadingMore(false);
  };

  const fetchData = async (searchText: string) => {
    setLoading(true);
    try {
      if (searchText.length > 0) {
        let data: ExternalSearchResult<any>;
        //This endpoint doesn't exist yet
        /* if (type === 'user') {
          data = await getAuthorData({ Search: searchText, Page: 1 });

          if (data.items.length) {
            setResults(data.items.flatMap((a) => a.papers));
          }
       } else {*/
        data = await getPublicationData({ Search: searchText, Page: 1 });

        if (data.items.length < data.total) {
          setPageOffset(1);
          setShowLoadMoreBtn(true);
        }

        setResults(data?.items);
        setTotal(data?.total);
      } else {
        setResults([]);
        setTotal(0);
        setShowLoadMoreBtn(false);
      }
    } catch (e) {
      setTotal(0);
      setResults([]);
      setShowLoadMoreBtn(false);
      console.warn('Err While Searching With API:: ', e);
    }
    setLoading(false);
  };

  const searchApi = useMemo(() => debounce(fetchData, 1200), []);

  useEffect(() => {
    searchApi(searchText);
    return () => {};
  }, [searchText]);

  return { loading, loadingMore, showLoadMoreBtn, results, total, loadMore };
}
