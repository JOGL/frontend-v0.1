import styled from '@emotion/styled';

export const WrapperStyled = styled.div`
  padding: ${({ theme }) => theme.space[4]};
  background-color: ${({ theme }) => theme.token.neutral3};
  border-radius: 8px;
  white-space: pre-wrap;
  overflow-wrap: break-word;
  width: 100%;

  & pre {
    margin-bottom: 0;
  }

  & code {
    white-space: pre-wrap;
    overflow-wrap: break-word;
  }
`;
