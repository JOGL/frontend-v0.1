import { ReactNode } from 'react';
import { WrapperStyled } from './code.styles.';

interface Props {
  children: ReactNode;
}

const Code = ({ children }: Props) => {
  return (
    <WrapperStyled>
      <pre>
        <code>{children}</code>
      </pre>
    </WrapperStyled>
  );
};

export default Code;
