import { useEffect } from 'react';
import { Router, useRouter } from 'next/router';
import nProgress from 'nprogress';
import { ConfigProvider, theme as antdTheme } from 'antd';
import { ThemeProvider as ThemeProviderEmotion } from '@emotion/react';
import GlobalStyles from '../GlobalStyles';
import ErrorBoundary from '../Tools/ErrorBoundary';
import { NotLoggedInModalProvider } from '~/src/contexts/notLoggedInModalContext';
import { ApiProvider } from '~/src/contexts/apiContext';
import UserProvider from '~/src/contexts/UserProvider';
import { ModalProvider } from '~/src/contexts/modalContext';
import { GoogleOAuthProvider } from '@react-oauth/google';
import usePersistLocaleCookie from '~/src/hooks/usePersistLocaleCookie';
import useIsMounted from '~/src/hooks/useIsMounted';
import { ImageCropperModalProvider } from '~/src/contexts/imageCropperContext';
import { PushNotificationStoreProvider } from '~/src/store/push-notifications/PushNotificationsStoreProvider';
import { PushNotificationsHandler } from '../Notifications/PushNotificationsHandler';
import { StonePathStoreProvider } from '~/src/contexts/stonepath/StonePathStoreProvider';
import { AuthProvider } from '../../../auth/auth';
import { QueryClientProvider } from '@tanstack/react-query';
import { useQueryClient } from '~/src/utils/useQueryClient';
import customAntdTheme from '~/src/utils/theme';
import theme from '~/src/utils/theme/theme';
import { HubDiscussionsStoreProvider } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import { WebSocketProvider } from '~/src/contexts/websocket/websocketProvider';
import { UnreadStoreInitializer } from '~/src/store/unread/unreadStoreInitializer';
import OnboardingContext from '~/src/contexts/onboardingContext';
// prettier-ignore
// This code comes from /with-loading NextJS example (allows loading indicator at the top of the page)
// Current link : https://github.com/zeit/next.js/blob/canary/examples/with-loading/pages/_app.js
Router.events.on('routeChangeStart', () => nProgress.start());
Router.events.on('routeChangeComplete', () => nProgress.done());
Router.events.on('routeChangeError', () => nProgress.done());

export default function AppLayout({ children }) {
  const { mounted } = useIsMounted();

  if (!mounted) {
    return <></>;
  }

  return <DefaultLayout>{children}</DefaultLayout>;
}

function DefaultLayout({ children }) {
  const { locale, defaultLocale } = useRouter();

  const { token } = antdTheme.useToken();
  const queryClient = useQueryClient();

  useEffect(() => {
    document.documentElement.lang = locale;
  }, [locale]);
  // add class "using-mouse" to body if user is using mouse.
  // We do this so we can remove style from all elements on focus, but add a special focus style if user uses the tab key to navigate
  // so all the elements he navigates to will be focused (accessibility purpose)
  const addBodyClass = () => document.body.classList.add('using-mouse');
  const removeBodyClass = (e) => e.keyCode === 9 && document.body.classList.remove('using-mouse');

  useEffect(() => {
    window.addEventListener('mousedown', addBodyClass);
    window.addEventListener('keydown', removeBodyClass);
    return () => {
      window.removeEventListener('mousedown', addBodyClass);
      window.removeEventListener('keydown', removeBodyClass);
    };
  });

  usePersistLocaleCookie(locale, defaultLocale);

  return (
    <ErrorBoundary>
      <QueryClientProvider client={queryClient}>
        <HubDiscussionsStoreProvider>
          <UnreadStoreInitializer />
          <AuthProvider>
            <ThemeProviderEmotion theme={{ ...theme, token: { ...token, ...customAntdTheme.token } }}>
              <ConfigProvider theme={customAntdTheme}>
                <NotLoggedInModalProvider>
                  <ApiProvider>
                    <PushNotificationStoreProvider>
                      <UserProvider>
                        <OnboardingContext>
                          <WebSocketProvider>
                            <ImageCropperModalProvider>
                              <ModalProvider>
                                <StonePathStoreProvider>
                                  <PushNotificationsHandler />
                                  <GoogleOAuthProvider clientId={process.env.GOOGLE_CLIENT_ID}>
                                    {children}
                                  </GoogleOAuthProvider>
                                </StonePathStoreProvider>
                              </ModalProvider>
                            </ImageCropperModalProvider>
                          </WebSocketProvider>
                        </OnboardingContext>
                      </UserProvider>
                    </PushNotificationStoreProvider>
                  </ApiProvider>
                </NotLoggedInModalProvider>
              </ConfigProvider>
            </ThemeProviderEmotion>
          </AuthProvider>
        </HubDiscussionsStoreProvider>
      </QueryClientProvider>
      <GlobalStyles />
    </ErrorBoundary>
  );
}
