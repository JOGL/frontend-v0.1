import ChannelMenu from '../channelMenu/channelMenu';
import InboxMenu from '../inboxMenu/inboxMenu';
import { MailOutlinedIcon } from '~/src/assets/icons/mailOutlinedIcon';
import { CarryOutOutlinedIcon } from '~/src/assets/icons/carryOutOutlinedIcon';
import { SoundOutlinedIcon } from '~/src/assets/icons/soundOutlinedIcon';
import { FileOutlinedIcon } from '~/src/assets/icons/fileOutlinedIcon';
import { LaptopOutlinedIcon } from '~/src/assets/icons/laptopOutlinedIcon';
import { BookOutlinedIcon } from '~/src/assets/icons/bookOutlinedIcon';
import { UserOutlinedIcon } from '~/src/assets/icons/userOutlinedIcon';
import { HomeOutlinedIcon } from '~/src/assets/icons/homeOutlinedIcon';

export enum FrontPath {
  Need = 'need',
  Paper = 'paper',
  Event = 'event',
  Document = 'doc',
  Inbox = 'inbox',
  Channel = 'channel',
  User = 'user',
  CommunityEntity = 'entity',
  Home = 'home',
}

export const menuItems = {
  inbox: {
    translationId: 'inbox',
    add_modal_key: null,
    front_path_generic: FrontPath.Inbox,
    front_path_desktop: 'dashboard/inbox/:id',
    front_path_mobile: 'menu/inbox',
    icon: <MailOutlinedIcon />,
    secondaryMenu: <InboxMenu />,
    secondaryMenuItems: null,
  },
  channels: {
    translationId: 'channel.other',
    add_modal_key: null,
    front_path_generic: FrontPath.Channel,
    front_path_desktop: 'channel',
    front_path_mobile: 'menu/channels',
    icon: <SoundOutlinedIcon />,
    secondaryMenu: <ChannelMenu />,
    secondaryMenuItems: null,
  },
  events: {
    translationId: 'event.other',
    add_modal_key: FrontPath.Event,
    front_path_generic: FrontPath.Event + '/list',
    front_path_desktop: 'dashboard/events/:id?key=all',
    front_path_mobile: 'menu/events',
    icon: <CarryOutOutlinedIcon />,
    secondaryMenuItems: {
      calendar: {
        translationId: 'hubCalendar',
        key: 'calendar',
      },
      new: {
        translationId: 'event.new',
        key: 'new',
      },
      created: {
        translationId: 'createdByYou',
        key: 'created',
      },
      invitations: {
        translationId: 'invitations',
        key: 'invitations',
      },
    },
  },
  documents: {
    translationId: 'document.other',
    add_modal_key: FrontPath.Document,
    front_path_generic: FrontPath.Document + '/list',
    front_path_desktop: 'dashboard/documents/:id?key=all',
    front_path_mobile: 'menu/documents',
    icon: <FileOutlinedIcon />,
    secondaryMenuItems: {
      all: {
        translationId: 'document.all',
        key: 'all',
      },
      recent: {
        translationId: 'recentlyViewed',
        key: 'recent',
      },
      shared: {
        translationId: 'sharedWithYou',
        key: 'shared',
      },
      created: {
        translationId: 'createdByYou',
        key: 'created',
      },
    },
  },
  needs: {
    translationId: 'need.other',
    add_modal_key: FrontPath.Need,
    front_path_generic: FrontPath.Need + '/list',
    front_path_desktop: 'dashboard/needs/:id?key=all',
    front_path_mobile: 'menu/needs',
    icon: <LaptopOutlinedIcon />,
    secondaryMenuItems: {
      all: {
        translationId: 'need.all',
        key: 'all',
      },
      recent: {
        translationId: 'recentlyViewed',
        key: 'recent',
      },
      shared: {
        translationId: 'sharedWithYou',
        key: 'shared',
      },
      created: {
        translationId: 'createdByYou',
        key: 'created',
      },
    },
  },
  papers: {
    translationId: 'paper.other',
    add_modal_key: FrontPath.Paper,
    front_path_generic: FrontPath.Paper + '/list',
    front_path_desktop: 'dashboard/papers/:id?key=all',
    front_path_mobile: 'menu/papers',
    icon: <BookOutlinedIcon />,
    secondaryMenuItems: {
      all: {
        translationId: 'paper.all',
        key: 'all',
      },
      recent: {
        translationId: 'recentlyViewed',
        key: 'recent',
      },
      shared: {
        translationId: 'sharedWithYou',
        key: 'shared',
      },
      created: {
        translationId: 'createdByYou',
        key: 'created',
      },
    },
  },
  users: {
    translationId: 'person.other',
    add_modal_key: FrontPath.User,
    front_path_generic: FrontPath.User + '/list',
    front_path_desktop: 'dashboard/users/:id?key=all',
    front_path_mobile: 'menu/users',
    icon: <UserOutlinedIcon />,
    secondaryMenuItems: {
      all: {
        translationId: 'user.all',
        key: 'all',
      },
      recent: {
        translationId: 'user.joined',
        key: 'joined',
      },
    },
  },
  home: {
    translationId: 'home',
    add_modal_key: FrontPath.Home,
    front_path_generic: FrontPath.CommunityEntity,
    front_path_desktop: 'dashboard/home/:id?key=all',
    front_path_mobile: 'menu/home',
    icon: <HomeOutlinedIcon />,
    secondaryMenuItems: {
      all: {
        translationId: 'workspace.all',
        key: 'all',
      },
      recent: {
        translationId: 'recentlyCreated',
        key: 'recent',
      },
      my: {
        translationId: 'workspace.my',
        key: 'created',
      },
    },
  },
};
