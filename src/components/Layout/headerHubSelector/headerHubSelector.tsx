import { Flex, Typography, theme, Badge, Dropdown } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import Image from 'next/image';
import {
  DropdownMenuStyled,
  FlexRowStyled,
  ImageStyled,
  TextStyled,
  TitleStyled,
  WrapperStyled,
} from './headerHubSelector.styles';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import { NodeFeedDataModel } from '~/__generated__/types';
import { FlexFullWidthStyled } from '../../Common/common.styles';
import { useRouter } from 'next/router';

const HeaderHubSelector = () => {
  const { t } = useTranslation('common');
  const { token } = theme.useToken();
  const router = useRouter();

  const { selectedHub, setSelectedHub, hubs, setSelectedMenuItem } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
    setSelectedHub: store?.setSelectedHubDiscussion,
    hubs: store?.hubDiscussions,
    setSelectedMenuItem: store.setSelectedMenuItem,
  }));

  const getIndicatorInfo = (hubDiscussion: NodeFeedDataModel | undefined) => {
    if (!hubDiscussion) return { showActivityIndicator: false, numberOfUnreadActivity: 0 };

    const numberOfUnreadActivity = hubDiscussion?.unread_mentions_total + hubDiscussion?.unread_threads_total;

    const showActivityIndicator =
      hubDiscussion?.new_events ||
      hubDiscussion?.new_needs ||
      hubDiscussion?.new_documents ||
      hubDiscussion?.new_papers ||
      hubDiscussion?.unread_posts_total > 0;

    return { showActivityIndicator, numberOfUnreadActivity };
  };

  const { showActivityIndicator, numberOfUnreadActivity } = getIndicatorInfo(selectedHub);
  const getTriggerContent = () => {
    return (
      <WrapperStyled align="center" gap="small">
        <Badge
          offset={[0, 5]}
          color={token.colorPrimary}
          dot={showActivityIndicator && numberOfUnreadActivity === 0}
          count={numberOfUnreadActivity > 0 ? numberOfUnreadActivity : ''}
        >
          <ImageStyled
            src={selectedHub?.logo_url_sm ?? '/images/logo_single.svg'}
            alt={t(`hubIcon`)}
            width={40}
            height={40}
          />
        </Badge>

        <FlexFullWidthStyled vertical justify="left">
          {/* <TextStyled>{selectedHub ? t('hub.one') : ''}</TextStyled> */}
          <TitleStyled>{selectedHub?.title ?? t('action.selectHub')}</TitleStyled>
        </FlexFullWidthStyled>
      </WrapperStyled>
    );
  };

  if (!hubs) return <>{getTriggerContent()}</>;

  const items = hubs.map((hub) => {
    const { showActivityIndicator, numberOfUnreadActivity } = getIndicatorInfo(hub);
    return {
      key: hub.id,
      label: (
        <FlexRowStyled
          align="center"
          justify="space-between"
          selected={selectedHub?.id === hub.id}
          onClick={() => {
            setSelectedHub(hub);
            setSelectedMenuItem('inbox');
            router.push(`/dashboard/inbox/${selectedHub?.id}?key=all`);
          }}
        >
          <Flex align="center" justify="flex-start" gap="small">
            <Image src={hub?.logo_url_sm ?? '/images/logo_single.svg'} alt={t(`hubIcon`)} width={40} height={40} />
            <Typography>{hub.title}</Typography>
          </Flex>

          <Flex align="center">
            <Badge
              offset={[-10, 0]}
              color={token.colorPrimary}
              key={hub.id}
              dot={showActivityIndicator && numberOfUnreadActivity === 0}
              count={numberOfUnreadActivity > 0 ? numberOfUnreadActivity : ''}
            >
              <></>
            </Badge>
          </Flex>
        </FlexRowStyled>
      ),
    };
  });

  return (
    <Dropdown
      menu={{ items }}
      trigger={['click']}
      placement="bottomLeft"
      dropdownRender={(menu) => <DropdownMenuStyled>{menu}</DropdownMenuStyled>}
    >
      {getTriggerContent()}
    </Dropdown>
  );
};

export default HeaderHubSelector;
