import styled from '@emotion/styled';
import { Flex, Typography } from 'antd';
import Image from 'next/image';
import { FlexFullWidthStyled } from '../../Common/common.styles';

export const ImageStyled = styled(Image)`
  border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;
  border: 2px solid transparent;
  &:hover {
    border-color: ${(p) => p.theme.colors.carouselBg};
  }
`;

export const DropdownMenuStyled = styled.div`
  min-width: 400px;

  .ant-dropdown-menu {
    max-height: calc(100vh - 60px);
  }
`;

export const FlexRowStyled = styled(Flex)<{ selected: boolean }>`
  width: 100%;
  cursor: pointer;
  padding: ${({ theme }) => theme.space[2]};
  background: ${(p) => (p.selected ? p.theme.colors.panelListBG : p.theme.token.neutral1)};
  &:hover {
    background: ${(p) => p.theme.colors.panelListBG};
  }
`;

export const TextStyled = styled(Typography.Text)`
  color: ${({ theme }) => theme.colors.greys['1000']};
`;

export const TitleStyled = styled(Typography.Text)`
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
  font-size: ${({ theme }) => theme.fontSizes['lg']};
  font-weight: ${({ theme }) => theme.fontWeight['600']};
  color: ${({ theme }) => theme.colors.greys['1000']};
  width: 100%;
  display: block;
`;

export const WrapperStyled = styled(FlexFullWidthStyled)`
  cursor: pointer;
  padding: 0 ${({ theme }) => theme.space[4]} 0 0;
`;
