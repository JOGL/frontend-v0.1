import styled from '@emotion/styled';
import Image from 'next/image';

export const MenuStyled = styled.div`
  cursor: pointer;
`;

export const ImageStyled = styled(Image)`
  border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;
  border: 2px solid transparent;
  &:hover {
    border-color: ${(p) => p.theme.colors.carouselBg};
  }
`;
