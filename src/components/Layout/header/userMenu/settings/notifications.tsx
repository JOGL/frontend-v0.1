import { Button, Col, Row, Typography, Switch, message, Divider } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { isPushNotificationsStoreSupported } from '~/src/store/push-notifications/pushNotificationsStore';
import { LOCAL_STORAGE_PUSH_NOTIFICATIONS_DISABLED } from '~/src/components/Notifications/PushNotificationsHandler';
import { usePushNotificationStore } from '~/src/store/push-notifications/PushNotificationsStoreProvider';
import { useRouter } from 'next/router';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import useUser from '~/src/hooks/useUser';
import { UserNotificationSettingsModel } from '~/__generated__/types';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';

export const Notifications = () => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const { user } = useUser();
  const queryClient = useQueryClient();
  const { showPushNotificationModal } = usePushNotificationStore((s) => ({
    showPushNotificationModal: s.showPushNotificationModal,
  }));

  const updateSettings = useMutation({
    mutationFn: async (notification_settings: UserNotificationSettingsModel) => {
      await api.users.usersPartialUpdateDetail(user.id, { notification_settings });
    },
    onSuccess: () => {
      message.success(t('settingsUpdated'));
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.userData],
      });
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });
  const onUpdate = (setting: UserNotificationSettingsModel) => {
    updateSettings.mutate({ ...user.notification_settings, ...setting });
  };

  return (
    <>
      {isPushNotificationsStoreSupported() &&
        Notification.permission === 'default' &&
        !localStorage.getItem(LOCAL_STORAGE_PUSH_NOTIFICATIONS_DISABLED) && (
          <Button type="primary" onClick={() => showPushNotificationModal(router)}>
            {t('enablePushNotifications')}
          </Button>
        )}
      <Row gutter={[16, 16]}>
        <Col span={24}>
          <Row gutter={[16, 16]}>
            <Col span={12} />
            <Col span={6}>
              <Typography.Text strong>{t('notificationSettings.pushNotification')}</Typography.Text>
            </Col>
            <Col span={6}>
              <Typography.Text strong>{t('notificationSettings.directEmail')}</Typography.Text>
            </Col>
            <Col span={24}>
              <Typography.Text strong>{t('notificationSettings.generalDiscussion')}</Typography.Text>
            </Col>
            <Col span={12}>
              <Typography.Text>{t('mention.one')}</Typography.Text>
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.mentionJogl}
                onChange={(checked) => onUpdate({ mentionJogl: checked })}
              />
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.mentionEmail}
                onChange={(checked) => onUpdate({ mentionEmail: checked })}
              />
            </Col>
            <Col span={12}>
              <Typography.Text>{t('notificationSettings.postsInContainerDiscussionsYouAreMemberOf')}</Typography.Text>
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.postMemberContainerJogl}
                onChange={(checked) => onUpdate({ postMemberContainerJogl: checked })}
              />
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.postMemberContainerEmail}
                onChange={(checked) => onUpdate({ postMemberContainerEmail: checked })}
              />
            </Col>

            <Col span={12}>
              <Typography.Text>
                {t('notificationSettings.repliesInThreadsYouCreatedParticipatedOrYouAreMentionedIn')}
              </Typography.Text>
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.threadActivityJogl}
                onChange={(checked) => onUpdate({ threadActivityJogl: checked })}
              />
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.threadActivityEmail}
                onChange={(checked) => onUpdate({ threadActivityEmail: checked })}
              />
            </Col>

            <Col span={12}>
              <Typography.Text>{t('notificationSettings.postsOnContentObjectsYouCreated')}</Typography.Text>
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.postAuthoredObjectJogl}
                onChange={(checked) => onUpdate({ postAuthoredObjectJogl: checked })}
              />
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.postAuthoredObjectEmail}
                onChange={(checked) => onUpdate({ postAuthoredObjectEmail: checked })}
              />
            </Col>

            <Divider />

            <Col span={24}>
              <Typography.Text strong>{t('notificationSettings.contentObjectsCreation')}</Typography.Text>
            </Col>
            <Col span={12}>
              <Typography.Text>{t('notificationSettings.documentAddedToContainerYouAreMemberOf')}</Typography.Text>
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.documentMemberContainerJogl}
                onChange={(checked) => onUpdate({ documentMemberContainerJogl: checked })}
              />
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.documentMemberContainerEmail}
                onChange={(checked) => onUpdate({ documentMemberContainerEmail: checked })}
              />
            </Col>
            <Col span={12}>
              <Typography.Text>{t('notificationSettings.needAddedToContainerYouAreMemberOf')}</Typography.Text>
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.needMemberContainerJogl}
                onChange={(checked) => onUpdate({ needMemberContainerJogl: checked })}
              />
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.needMemberContainerEmail}
                onChange={(checked) => onUpdate({ needMemberContainerEmail: checked })}
              />
            </Col>
            <Col span={12}>
              <Typography.Text>{t('notificationSettings.paperAddedToContainerYouAreMemberOf')}</Typography.Text>
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.paperMemberContainerJogl}
                onChange={(checked) => onUpdate({ paperMemberContainerJogl: checked })}
              />
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.paperMemberContainerEmail}
                onChange={(checked) => onUpdate({ paperMemberContainerEmail: checked })}
              />
            </Col>

            <Divider />

            <Col span={24}>
              <Typography.Text strong>{t('notificationSettings.containerInvites')}</Typography.Text>
            </Col>
            <Col span={12}>
              <Typography.Text>{t('notificationSettings.youAreInvitedToContainer')}</Typography.Text>
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.containerInvitationJogl}
                onChange={(checked) => onUpdate({ containerInvitationJogl: checked })}
              />
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.containerInvitationEmail}
                onChange={(checked) => onUpdate({ containerInvitationEmail: checked })}
              />
            </Col>

            <Divider />

            <Col span={24}>
              <Typography.Text strong>{t('event.other')}</Typography.Text>
            </Col>
            <Col span={12}>
              <Typography.Text>{t('notificationSettings.youAreDirectlyInvitedToEvent')}</Typography.Text>
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.eventInvitationJogl}
                onChange={(checked) => onUpdate({ eventInvitationJogl: checked })}
              />
            </Col>
            <Col span={6} />
            <Col span={12}>
              <Typography.Text>{t('notificationSettings.newEventInContainerYouAreMemberOf')}</Typography.Text>
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.eventMemberContainerJogl}
                onChange={(checked) => onUpdate({ eventMemberContainerJogl: checked })}
              />
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.eventMemberContainerEmail}
                onChange={(checked) => onUpdate({ eventMemberContainerEmail: checked })}
              />
            </Col>
            <Col span={12}>
              <Typography.Text>{t('notificationSettings.postsInEventsYouAreAttending')}</Typography.Text>
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.postAttendingEventJogl}
                onChange={(checked) => onUpdate({ postAttendingEventJogl: checked })}
              />
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.postAttendingEventEmail}
                onChange={(checked) => onUpdate({ postAttendingEventEmail: checked })}
              />
            </Col>
            <Col span={12}>
              <Typography.Text>{t('notificationSettings.postsInEventsYouCreated')}</Typography.Text>
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.postAuthoredEventJogl}
                onChange={(checked) => onUpdate({ postAuthoredEventJogl: checked })}
              />
            </Col>
            <Col span={6}>
              <Switch
                checked={user.notification_settings.postAuthoredEventEmail}
                onChange={(checked) => onUpdate({ postAuthoredEventEmail: checked })}
              />
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  );
};
