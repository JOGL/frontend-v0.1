import { Button, Typography, message, Flex, Space } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import useUser from '~/src/hooks/useUser';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { DeleteOutlined } from '@ant-design/icons';

export const LinkedAccount = () => {
  const { t } = useTranslation('common');
  const { user } = useUser();
  const queryClient = useQueryClient();

  const removeOrcid = useMutation({
    mutationFn: async () => {
      await api.users.orcidDelete(user.id);
    },
    onSuccess: () => {
      message.success(t('removeOrcidSuccess'));
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.userData],
      });
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const removeGoogle = useMutation({
    mutationFn: async () => {
      await api.users.googleDelete(user.id);
    },
    onSuccess: () => {
      message.success(t('removeGoogleSuccess'));
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.userData],
      });
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const openLinkMyOrcidModal = () => {
    window.location.href = `${process.env.ORCID_URL}/oauth/authorize?client_id=${process.env.ORCID_CLIENT_ID}&response_type=code&scope=/read-limited&redirect_uri=${process.env.ORCID_REDIRECT_URL}`;
  };

  return (
    <Flex vertical align="start" gap="middle">
      <Typography.Text>{t('manageAccountsLinkedToYourProfile')}</Typography.Text>
      {!user.orcid_id && (
        <Button onClick={() => openLinkMyOrcidModal()}>
          <img src={`/images/icons/links-orcid.png`} style={{ width: 24, height: 24 }} />
          {t('action.linkMyOrcid')}
        </Button>
      )}
      {user.orcid_id && (
        <Flex gap="middle" align="center">
          <Button
            icon={<DeleteOutlined />}
            onClick={() => {
              removeOrcid.mutate();
            }}
          />
          <Flex gap="small">
            <img src={`/images/icons/links-orcid.png`} style={{ width: 24, height: 24 }} />
            <Space>
              <Typography.Text strong>ORCID</Typography.Text>
              <Typography.Text>{t('accountLinked')}</Typography.Text>
            </Space>
          </Flex>
        </Flex>
      )}
      {user?.external_auth?.is_google_user && (
        <Flex gap="middle" align="center">
          <Button
            icon={<DeleteOutlined />}
            onClick={() => {
              removeGoogle.mutate();
            }}
          />
          <Flex gap="small">
            <img src={`/images/icons/sign-in-google.png`} style={{ width: 24, height: 24 }} />
            <Space>
              <Typography.Text strong>
                {user?.first_name} {user?.last_name}
              </Typography.Text>
              <Typography.Text>{t('accountLinked')}</Typography.Text>
            </Space>
          </Flex>
        </Flex>
      )}
    </Flex>
  );
};
