import { Modal, Tabs } from 'antd';
import type { TabsProps } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { Notifications } from './notifications';
import { LinkedAccount } from './linked-account';
import { More } from './more';

interface Props {
  onClose: () => void;
}

export const Settings = ({ onClose }: Props) => {
  const { t } = useTranslation('common');

  const tabs: TabsProps['items'] = [
    {
      key: 'notifications',
      label: t('notification.other'),

      children: <Notifications />,
    },
    {
      key: 'account',
      label: t('linkedAccount'),
      children: <LinkedAccount />,
    },
    {
      key: 'more',
      label: t('more'),
      children: <More />,
    },
  ];

  return (
    <Modal open title={t('setting.other')} onClose={onClose} onCancel={onClose} footer={null}>
      <Tabs defaultActiveKey="1" items={tabs} />
    </Modal>
  );
};
