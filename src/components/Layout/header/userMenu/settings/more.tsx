import { Button, Typography, Flex, Popconfirm } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useMutation } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import useUser from '~/src/hooks/useUser';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import { UserStatus } from '~/__generated__/types';
import useLogin from '~/src/hooks/useLogin';

export const More = () => {
  const { t } = useTranslation('common');
  const { user } = useUser();
  const [, signout] = useLogin();

  const archiveSelf = useMutation({
    mutationKey: [MUTATION_KEYS.userArchive],
    mutationFn: async () => {
      const response = await api.users.archiveCreate();
      user.user_status = UserStatus.Archived;
      return { status: response.status, data: response.data };
    },
  });

  const unarchiveSelf = useMutation({
    mutationKey: [MUTATION_KEYS.userArchive],
    mutationFn: async () => {
      const response = await api.users.unarchiveCreate();
      user.user_status = UserStatus.Verified;
      return { status: response.status, data: response.data };
    },
  });

  const deleteSelf = useMutation({
    mutationKey: [MUTATION_KEYS.userDelete],
    mutationFn: async () => {
      const response = await api.users.usersDelete();
      return { status: response.status, data: response.data };
    },
  });

  return (
    <Flex vertical align="start" gap="middle">
      <Typography.Text>{t('archiveOrDeleteYourProfile')}</Typography.Text>
      {user.user_status === UserStatus.Verified && (
        <Popconfirm
          title={t('action.archive')}
          description={t('areYouSureYouWantToArchiveYourself')}
          onConfirm={() => {
            archiveSelf.mutate();
          }}
          okText={t('yes')}
          cancelText={t('no')}
        >
          <Button> {t('action.archive')}</Button>
        </Popconfirm>
      )}
      {user.user_status === UserStatus.Archived && (
        <Flex gap="small">
          <Popconfirm
            title={t('action.unarchive')}
            description={t('areYouSureYouWantToUnarchiveYourself')}
            onConfirm={() => {
              unarchiveSelf.mutate();
            }}
            okText={t('yes')}
            cancelText={t('no')}
          >
            <Button> {t('action.unarchive')}</Button>
          </Popconfirm>
          <Popconfirm
            title={t('action.delete')}
            description={t('areYouSureYouWantToDeleteYourself')}
            onConfirm={() => {
              deleteSelf.mutate();
              signout();
            }}
            okText={t('yes')}
            cancelText={t('no')}
          >
            <Button danger> {t('action.delete')}</Button>
          </Popconfirm>
        </Flex>
      )}
    </Flex>
  );
};
