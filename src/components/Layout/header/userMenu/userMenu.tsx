import React, { FC, useState } from 'react';
import Image from 'next/image';
import setLanguage from 'next-translate/setLanguage';
import { Avatar, Button, Divider, Dropdown, Flex, message, Popover, Space } from 'antd';
import { ImageStyled, MenuStyled } from './userMenu.styles';
import { NEXT_PUBLIC_ENVIRONMENT } from '~/src/types/env';
import {
  CheckOutlined,
  FlagOutlined,
  LogoutOutlined,
  SettingOutlined,
  SwitcherOutlined,
  TranslationOutlined,
  UserOutlined,
} from '@ant-design/icons';
import useUser from '~/src/hooks/useUser';
import useTranslation from 'next-translate/useTranslation';
import { Router, useRouter } from 'next/router';
import { useMutation } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { Settings } from './settings/settings';

const languages = [
  { id: 'en', title: 'English' },
  { id: 'fr', title: 'Français' },
  { id: 'nl', title: 'Dutch' },
];

const UserMenu: FC<{ logoSrc: string }> = ({ logoSrc }) => {
  const [isCrowdinLoaded, setIsCrowdinLoaded] = useState(!!window._jipt);
  const [open, setOpen] = useState(false);
  const [openSettings, setOpenSettings] = useState(false);
  const { locale, push } = useRouter();
  const { user, logout } = useUser();
  const { t } = useTranslation('common');

  const changeLanguage = useMutation({
    mutationFn: async (language: string) => {
      await api.users.usersPartialUpdateDetail(user.id, { language });
      return language;
    },
    onSuccess: (language) => {
      setLanguage(language);
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const language = languages.find((l) => l.id === locale)?.title || 'English';
  const languageMenuItems = languages.map((lang, i) => {
    return {
      label: (
        <Space>
          {lang.title} {locale === lang.id && <CheckOutlined />}
        </Space>
      ),
      key: lang.id,
      onClick: () => changeLanguage.mutate(lang.id),
    };
  });
  const handleOpenChange = (newOpen: boolean) => {
    setOpen(newOpen);
  };
  return (
    <>
      <MenuStyled>
        <Popover
          open={open}
          onOpenChange={handleOpenChange}
          content={
            <>
              <Flex vertical align="start" gap="small">
                <Button type="link" icon={<UserOutlined />} onClick={() => push(`/user/${user.id}`)}>
                  {t('myProfile')}
                </Button>
                <Button
                  type="link"
                  icon={<FlagOutlined />}
                  onClick={() =>
                    window.open(
                      'https://jogldoc.notion.site/Your-first-steps-on-JOGL-9ed4463c6dd04176a56f994a6b19c3c4',
                      '_blank'
                    )
                  }
                >
                  {t('help')}
                </Button>
                <Button
                  type="link"
                  icon={<SettingOutlined />}
                  onClick={() => {
                    setOpen(false);
                    setOpenSettings(true);
                  }}
                >
                  {t('setting.other')}
                </Button>
                <Dropdown
                  placement="bottomLeft"
                  trigger={['click']}
                  menu={{
                    items: languageMenuItems,
                  }}
                >
                  <Button type="link" icon={<TranslationOutlined />}>
                    {t('selectedLanguage', { language })}
                  </Button>
                </Dropdown>
                {process.env.NEXT_PUBLIC_ENVIRONMENT === NEXT_PUBLIC_ENVIRONMENT.develop && (
                  <Button
                    type="primary"
                    icon={<TranslationOutlined />}
                    onClick={() => {
                      if (isCrowdinLoaded) {
                        changeLanguage.mutate('lol');
                        return;
                      }

                      window._jipt = [['project', 'jogl']];

                      const scriptEle = document.createElement('script');
                      scriptEle.setAttribute('src', '//cdn.crowdin.com/jipt/jipt.js');
                      scriptEle.setAttribute('type', 'text/javascript');

                      document.body.appendChild(scriptEle);

                      scriptEle.addEventListener('load', () => setIsCrowdinLoaded(true));
                      scriptEle.addEventListener('error', (ev) => console.log('Error loading Crowdin ', ev));
                    }}
                    disabled={isCrowdinLoaded && locale === 'lol'}
                  >
                    {isCrowdinLoaded ? t('action.identifyTranslatableText') : t('action.loadTranslations')}
                  </Button>
                )}
              </Flex>
              <Divider style={{ margin: 12 }} />
              <Button type="text" icon={<LogoutOutlined />} onClick={() => logout('/signin')}>
                {t('action.logout')}
              </Button>
            </>
          }
          trigger="click"
          placement="bottomRight"
        >
          <Avatar
            shape="square"
            size="large"
            src={
              <ImageStyled
                alt="user-avatar"
                src={logoSrc || '/images/default/default-user.png'}
                width={40}
                height={40}
              />
            }
          />
        </Popover>
      </MenuStyled>
      {openSettings && <Settings onClose={() => setOpenSettings(false)} />}
    </>
  );
};

export default UserMenu;
