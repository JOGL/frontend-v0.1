import styled from '@emotion/styled';
import { Col, Row } from 'antd';

export const RowStyled = styled(Row)`
  width: 100%;
  display: flex;
`;

export const ColStyled = styled(Col)`
  display: flex;
`;
