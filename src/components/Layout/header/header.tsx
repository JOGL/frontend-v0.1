import useTranslation from 'next-translate/useTranslation';
import { Button, Flex, Row } from 'antd';
import { ColStyled, RowStyled } from './header.styles';
import { useAuth } from '~/auth/auth';
import HeaderHubSelector from '../headerHubSelector/headerHubSelector';
import { FlexFullWidthStyled } from '../../Common/common.styles';
import useLogin from '~/src/hooks/useLogin';
import ActionButton from './actionButton/actionButton';
import UserMenuButton from './userMenuButton/userMenuButton';

const HeaderNew = () => {
  const { t } = useTranslation('common');
  const { isLoggedIn } = useAuth();
  const [signin] = useLogin();

  return (
    <RowStyled>
      <ColStyled span={6}>
        {isLoggedIn === true && (
          <FlexFullWidthStyled align="center" flex="auto">
            <HeaderHubSelector />
          </FlexFullWidthStyled>
        )}
      </ColStyled>
      <ColStyled span={14}>
        {/* <Flex align="center" flex="auto">
          <HeaderSearch />
        </Flex> */}
      </ColStyled>
      <ColStyled span={4}>
        <Flex align="center" justify="flex-end" flex="auto">
          {isLoggedIn !== true && (
            <Button onClick={signin} type="primary" size="large">
              {t('action.signInOrSignUp')}
            </Button>
          )}
          {isLoggedIn === true && (
            <Flex gap="small" align="center">
              <ActionButton />
              <UserMenuButton />
            </Flex>
          )}
        </Flex>
      </ColStyled>
    </RowStyled>
  );
};

export default HeaderNew;
