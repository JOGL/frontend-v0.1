import styled from '@emotion/styled';
import { Flex, Input } from 'antd';

export const SearchStyled = styled(Input.Search)`
  padding-top: ${({ theme }) => theme.space[1]};
`;

export const SearchWrapperStyled = styled(Flex)`
  padding: ${({ theme }) => theme.token.sizeLG}px;
  text-align: center;
  & svg {
    font-size: 56px;
    color: ${({ theme }) => theme.token.colorTextSecondary};
  }
`;
