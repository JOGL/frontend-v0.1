import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { SearchStyled } from './headerSearch.styles';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';

const HeaderSearch = () => {
  const { t } = useTranslation('common');
  //const debouncedSearch = useDebounce(search, 300);
  const router = useRouter();

  const { selectedHub } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
  }));

  return (
    <>
      <SearchStyled
        size="large"
        placeholder={`${t('action.search')} ${selectedHub?.title}`}
        onSearch={(value) => {
          router.push(`/hub/${selectedHub?.id}/search?initial=${encodeURIComponent(value)}`);
        }}
      />
    </>
  );
};

export default HeaderSearch;
