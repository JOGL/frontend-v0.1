import useTranslation from 'next-translate/useTranslation';
import { useState } from 'react';
import { NotificationModel } from '~/__generated__/types';
import { filterNCleanActions } from '~/src/components/Header/NotificationUtils';
import useGet from '~/src/hooks/useGet';

const useAction = () => {
  const { t } = useTranslation('common');
  const [unreadNotifications, setUnreadNotifications] = useState(0);
  const { data: notifications, mutate: revalidateNotification } = useGet('/users/notifications');
  //TODO refactor with action list
  globalThis.revalidateNotificationInfo = revalidateNotification.bind(this);

  const fetchNotification = async () => {
    const notificationsInfo = filterNCleanActions(notifications as NotificationModel[], t);
    const notificationsCount = notificationsInfo?.adminCount + notificationsInfo?.personalCount;
    if (notificationsCount === unreadNotifications) {
      return;
    }
    setUnreadNotifications(notificationsCount);
  };

  notifications && fetchNotification();

  return {
    unreadNotifications,
  };
};

export default useAction;