import styled from '@emotion/styled';
import { Badge } from 'antd';

export const ActionsStyled = styled.div`
  min-width: 50px;
`;

export const BadgeStyled = styled(Badge)`
  svg {
    font-size: 30px;
    color: ${({ theme }) => theme.token.neutral8};
  }
`;
