import Link from 'next/link';
import React from 'react';
import { Flex, theme } from 'antd';

import { ActionsStyled, BadgeStyled } from './actionButton.styles';

import { BellOutlined } from '@ant-design/icons';
import useAction from './actionButton.hooks';

const ActionButton = () => {
  const { token } = theme.useToken();
  const { unreadNotifications } = useAction();
  return (
    <Link href="/actions" passHref>
      <ActionsStyled>
        <Flex vertical align="center">
          <BadgeStyled count={unreadNotifications} size="small" offset={[-6, 4]} color={token.colorPrimary}>
            <BellOutlined />
          </BadgeStyled>
        </Flex>
      </ActionsStyled>
    </Link>
  );
};

export default ActionButton;
