import { FC } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Flex } from 'antd';
import { EntityMiniModel } from '~/__generated__/types';
import { entityItem } from '~/src/utils/utils';
import StonePathItem from './stonePathItem/stonePathItem';

const StonePath: FC<{ paths: EntityMiniModel[] }> = ({ paths }) => {
  const router = useRouter();

  return (
    <div>
      <Flex gap="small" align="center">
        {paths.map((path) => {
          const href = `/${entityItem[path.entity_type].front_path}/${path.id}`;
          const isActivePath = router.asPath === href;

          return isActivePath ? (
            <div key={path.id}>
              <StonePathItem path={path} />
            </div>
          ) : (
            <Link href={href} key={path.id}>
              <StonePathItem path={path} />
            </Link>
          );
        })}
      </Flex>
    </div>
  );
};

export default StonePath;
