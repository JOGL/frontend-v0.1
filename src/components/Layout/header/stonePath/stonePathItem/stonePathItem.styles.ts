import styled from '@emotion/styled';
import { Typography } from 'antd';

export const StonePathStyled = styled.div`
  color: ${({ theme }) => theme.token.neutral8};
  h5 {
    margin-bottom: 0;
  }
`;

export const EllipsisStyled = styled.div`
  h5 {
    max-width: 184px;
    display: inline-block;
  }
`;

export const TextStyled = styled(Typography.Text)`
  max-width: 184px !important;
`;
