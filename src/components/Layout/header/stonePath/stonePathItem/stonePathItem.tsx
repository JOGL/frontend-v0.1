import { FC } from 'react';
import { RightOutlined } from '@ant-design/icons';
import { Flex, Typography } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { EntityMiniModel } from '~/__generated__/types';
import HubLogo from '~/src/components/Common/hubLogo/hubLogo';
import { entityItem } from '~/src/utils/utils';
import { EllipsisStyled, StonePathStyled, TextStyled } from './stonePathItem.styles';

const StonePathItem: FC<{ path: EntityMiniModel }> = ({ path }) => {
  const { t } = useTranslation('common');

  const item = {
    title: path.entity_type === 'user' ? path.full_name : path.title,
    src: path.logo_url_sm || path.logo_url,
    id: path.id,
    entity_type: path.entity_type,
    label: path.entity_type === 'workspace' && path?.label ? path.label : t(entityItem[path.entity_type].translationId),
  };
  return (
    <StonePathStyled>
      <Flex gap="small" align="center">
        <RightOutlined />
        {item.src && <HubLogo src={item.src} alt={item.title} />}
        <Flex vertical>
          <TextStyled ellipsis={{ tooltip: item.label }}>{item.label}</TextStyled>
          <EllipsisStyled>
            <Typography.Title level={5} ellipsis={{ tooltip: item.title }}>
              {item.title || t('untitled')}
            </Typography.Title>
          </EllipsisStyled>
        </Flex>
      </Flex>
    </StonePathStyled>
  );
};

export default StonePathItem;
