import UserMenu from '../userMenu/userMenu';
import useUser from '~/src/hooks/useUser';
import { WrapperStyled } from './userMenuButton.styles';

const UserMenuButton = () => {
  const { user } = useUser();

  return (
    <WrapperStyled align="center" gap="small">
      <UserMenu logoSrc={user?.logo_url_sm} />
    </WrapperStyled>
  );
};

export default UserMenuButton;
