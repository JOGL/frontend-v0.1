import styled from '@emotion/styled';
import { Flex, Typography } from 'antd';

export const WrapperStyled = styled(Flex)`
  cursor: pointer;
  font-size: ${({ theme }) => theme.fontSizes['2xl']};
  margin: ${({ theme }) => theme.space[2]} 0;
`;

export const TextStyled = styled(Typography.Text)`
  color: ${({ theme }) => theme.colors.greys['1000']};
`;

export const TitleStyled = styled(Typography.Text)`
  font-size: ${({ theme }) => theme.fontSizes.lg};
  font-weight: ${({ theme }) => theme.fontWeight['600']};
  color: ${({ theme }) => theme.colors.greys['1000']};
`;
