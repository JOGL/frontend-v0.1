import styled from '@emotion/styled';
import { Alert, Button } from 'antd';

export const AlertStyled = styled(Alert)`
  position: fixed;
  top: 0;
  border-radius: 0;
  border: 0;
  height: 60px;
  z-index: 1000;
`;

export const ButtonStyled = styled(Button)`
  margin-left: ${({ theme }) => `${theme.token.sizeMS}px`};
`;
