import Trans from 'next-translate/Trans';
import { AlertStyled, ButtonStyled } from './pwaBanner.styles';
import useTranslation from 'next-translate/useTranslation';

import { usePwa } from '~/src/pwa/pwa';

interface Props {
  handleInstall: () => void;
}

const PwaBanner = ({ handleInstall }: Props) => {
  const { t } = useTranslation('common');
  return (
    <AlertStyled
      message={t('pwaBannerText')}
      type="warning"
      action={
        <ButtonStyled size="small" type="primary" onClick={handleInstall}>
          {t('install')}
        </ButtonStyled>
      }
    />
  );
};

const PwaBannerIos = () => {
  return (
    <AlertStyled
      message={<Trans i18nKey={`common:pwaBannerTextIos`} components={[<b key={'pwaBanner'} />]} />}
      type="warning"
    />
  );
};

const PwaBannerManual = () => {
  return (
    <AlertStyled
      message={<Trans i18nKey={`common:pwaBannerTextManual`} components={[<b key={'pwaBanner'} />]} />}
      type="warning"
    />
  );
};

export default function PwaAlert() {
  const { showPwaAlert, handleInstallPwa, showIosPwaAlert, manualInstallInfo } = usePwa();

  if (showPwaAlert) return <PwaBanner handleInstall={handleInstallPwa} />;
  if (showIosPwaAlert || true) return <PwaBannerIos />;
  if (manualInstallInfo) return <PwaBannerManual />;
}
