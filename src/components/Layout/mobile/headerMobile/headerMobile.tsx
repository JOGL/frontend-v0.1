import useTranslation from 'next-translate/useTranslation';
import { Button, Flex } from 'antd';
import { ColStyled, RowStyled } from './headerMobile.styles';
import { useAuth } from '~/auth/auth';
import HeaderHubSelector from '../../headerHubSelector/headerHubSelector';
import useLogin from '~/src/hooks/useLogin';
import ActionButton from '../../header/actionButton/actionButton';
import UserMenuButton from '../../header/userMenuButton/userMenuButton';
import { FlexFullWidthStyled } from '~/src/components/Common/common.styles';

const HeaderMobile = () => {
  const { t } = useTranslation('common');
  const { isLoggedIn } = useAuth();
  const [signin] = useLogin();

  return (
    <RowStyled>
      <ColStyled span={16}>
        {isLoggedIn === true && (
          <FlexFullWidthStyled align="center" flex="auto">
            <HeaderHubSelector />
          </FlexFullWidthStyled>
        )}
      </ColStyled>
      <ColStyled span={8}>
        <Flex align="center" justify="flex-end" flex="auto">
          {isLoggedIn !== true && (
            <Button onClick={signin} type="primary" size="large">
              {t('action.signInOrSignUp')}
            </Button>
          )}
          {isLoggedIn === true && <ActionButton />}
          {isLoggedIn === true && <UserMenuButton />}
        </Flex>
      </ColStyled>
    </RowStyled>
  );
};

export default HeaderMobile;
