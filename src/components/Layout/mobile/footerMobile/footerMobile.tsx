import { useRouter } from 'next/router';
import { menuItems } from '../../menuItems/menuItems';
import { MenuStyled, MoreStyled } from './footerMobile.styles';
import useTranslation from 'next-translate/useTranslation';
import { BadgeStyled, FlexFullHeightStyled } from '~/src/components/Common/common.styles';
import { useUnreadStore } from '~/src/store/unread/unreadStore';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import { Flex, Typography } from 'antd';
import { EllipsisOutlined, MoreOutlined } from '@ant-design/icons';

const FooterMobile = () => {
  const router = useRouter();
  const { t } = useTranslation('common');

  const { selectedHub } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
  }));

  const { hasNew } = useUnreadStore((store) => ({
    hasNew: store?.hasNewItemsHub,
  }));

  return (
    <MenuStyled
      mode="horizontal"
      overflowedIndicator={
        <MoreStyled vertical align="center">
          <EllipsisOutlined />
          <Typography.Text>{t('more')}</Typography.Text>
        </MoreStyled>
      }
      items={Object.entries(menuItems).map(([key, item]) => ({
        key: key,
        icon: (
          <Flex vertical>
            {item.icon}
            <BadgeStyled dot={hasNew(key, selectedHub?.id) === true} offset={[4, -25]} />
          </Flex>
        ),
        label: t(item.translationId),
        onClick: () => router.push(`/${item.front_path_mobile}`),
      }))}
    />
  );
};

export default FooterMobile;
