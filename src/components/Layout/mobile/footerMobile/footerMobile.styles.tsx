import styled from '@emotion/styled';
import { Flex, Menu } from 'antd';

export const MenuStyled = styled(Menu)`
  border: none;
  && .ant-menu-item {
    display: flex;
    flex-direction: column;
    align-items: center;
    max-width: 80px;
    span {
      line-height: 18px;
      margin-inline-start: 0;
    }
  }

  && .ant-menu-item .anticon {
    font-size: ${({ theme }) => theme.fontSizes['4xl']};
    padding: ${({ theme }) => `0 ${theme.space[3]}`};
  }

  width: 100%;
  && .ant-menu-item-selected::after {
    border-bottom: 0;
  }
  .ant-menu-submenu-title .anticon + span {
    margin-inline-start: 0;
  }
`;

export const MoreStyled = styled(Flex)`
  svg {
    width: 26px;
    height: 26px;
  }
  span {
    line-height: 18px;
    margin-inline-start: 0;
  }
`;
