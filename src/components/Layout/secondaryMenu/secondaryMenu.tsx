import { Divider, Flex, Tree, TreeDataNode } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import {
  ButtonStyled,
  FlexStyled,
  MenuStyled,
  MenuTitleStyled,
  TagStyled,
  TypographyStyled,
} from './secondaryMenu.styles';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import { useRouter } from 'next/router';
import { useMemo, useState } from 'react';
import { FrontPath, menuItems } from '../menuItems/menuItems';
import useTitleHelper from '~/src/hooks/useTitleHelper';
import { DownOutlined, PlusOutlined } from '@ant-design/icons';
import CreateModal from '../../Common/createModal/createModal';
import ContainerLogo from '../../Common/container/containerLogo/containerLogo';
import UserInvite from '../../users/userInviteModal/userInviteModal';
import { BadgeStyled } from '../../Common/common.styles';
import { useUnreadStore } from '~/src/store/unread/unreadStore';

const SecondaryMenu = ({ desiredMenuItem = undefined }: { desiredMenuItem?: string }) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const [getTitle] = useTitleHelper();
  const [createModalType, setCreateModalType] = useState<FrontPath | null>(null);
  const [selectedSecondaryMenuItem, setSelectedSecondaryMenuItem] = useState((router.query.key as string) ?? '');

  const { selectedHub, selectedMenuItem } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
    selectedMenuItem: store?.selectedMenuItem,
  }));

  const { hasNewItemsHub } = useUnreadStore((store) => ({
    hasNewItemsHub: store?.hasNewItemsHub,
  }));

  const selectedSpace = router.query.id ? selectedHub?.entities.find((item) => item.id === router.query.id) : null;

  const menuItem: string = useMemo(() => {
    return desiredMenuItem ?? selectedMenuItem ?? '';
  }, [desiredMenuItem, selectedMenuItem]);

  if (!menuItem || !selectedHub) return <></>;

  const treeData: TreeDataNode[] = [];

  selectedHub.entities.forEach((item, index) => {
    const child = {
      title: (
        <Flex align="center" justify="start" gap="small">
          <ContainerLogo container={item} />

          <Flex vertical align="start">
            <TypographyStyled
              strong
              ellipsis={{ tooltip: getTitle(item.title) }}
              level={item.level}
              draft={item.status === 'draft'}
            >
              {item.title || t('untitled')}
            </TypographyStyled>
          </Flex>
          {item.status === 'draft' && <TagStyled>{t('draft').toLowerCase()}</TagStyled>}
        </Flex>
      ),
      key: item.id,
      children: [],
    };
    if (index === 0) {
      treeData.push(child);
    } else {
      if (item.level === 0) {
        treeData[0].children?.push(child);
      } else {
        const parent = (treeData[0]?.children as TreeDataNode[]) ?? [];
        parent[parent.length - 1]?.children?.push(child);
      }
    }
  });

  const isSelected = () => {
    if (
      router.pathname.includes('/list/') ||
      router.pathname.includes('/hub/') ||
      router.pathname.includes('/workspace/')
    ) {
      return [router.query.id as string];
    }
    return;
  };

  if (createModalType === FrontPath.Home) {
    const selected = selectedSpace || selectedHub;
    router.push(`/workspace/create?entityId=${selected.id}`);
    setCreateModalType(null);
  }
  return (
    <FlexStyled vertical align="stretch">
      <Flex justify="space-between" align="center">
        <MenuTitleStyled level={3}>{t(menuItems[menuItem].translationId)}</MenuTitleStyled>
        {menuItems[menuItem].add_modal_key && (
          <ButtonStyled
            icon={<PlusOutlined />}
            type="text"
            onClick={() => {
              setCreateModalType(menuItems[menuItem].add_modal_key);
            }}
          />
        )}
      </Flex>

      {menuItems[menuItem].secondaryMenu && <>{menuItems[menuItem].secondaryMenu}</>}
      {menuItems[menuItem].secondaryMenuItems && (
        <MenuStyled
          selectedKeys={[selectedSecondaryMenuItem]}
          items={Object.keys(menuItems[menuItem].secondaryMenuItems).map((key) => ({
            key: key,
            label: (
              <BadgeStyled dot={hasNewItemsHub(menuItem, selectedHub?.id, key) === true}>
                <TypographyStyled strong>
                  {t(menuItems[menuItem].secondaryMenuItems[key].translationId)}
                </TypographyStyled>
              </BadgeStyled>
            ),
            onClick: () => {
              setSelectedSecondaryMenuItem(key);
              router.push(
                `/dashboard/${menuItem}/${selectedHub.id}?key=${menuItems[menuItem].secondaryMenuItems[key].key}`
              );
            },
          }))}
        />
      )}
      {!menuItems[menuItem].secondaryMenu && <Divider />}
      {!menuItems[menuItem].secondaryMenu && (
        <Tree
          showLine
          switcherIcon={<DownOutlined />}
          onSelect={(selectedKey) => {
            const selectedID = (selectedKey[0] as string) ?? selectedHub.id;
            setSelectedSecondaryMenuItem(selectedID);
            router.push(`/${menuItems[menuItem].front_path_generic}/${selectedID}`);
          }}
          treeData={treeData}
          defaultExpandAll={true}
          selectedKeys={isSelected()}
        />
      )}
      {createModalType &&
        (createModalType == FrontPath.User ? (
          <UserInvite handleClose={() => setCreateModalType(null)} containerId={selectedHub?.id} refetch={() => {}} />
        ) : (
          <CreateModal
            handleClose={() => setCreateModalType(null)}
            selectedSpace={selectedSpace || selectedHub}
            modalType={createModalType}
          />
        ))}
    </FlexStyled>
  );
};

export default SecondaryMenu;
