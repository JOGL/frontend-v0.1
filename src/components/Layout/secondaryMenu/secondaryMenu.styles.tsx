import styled from '@emotion/styled';
import { Badge, Button, Flex, Menu, Tag, Typography } from 'antd';
import { FlexFullWidthStyled } from '../../Common/common.styles';

export const MenuStyled = styled(Menu)`
  background-color: transparent;
  margin-left: ${({ theme }) => theme.token.marginXS}px;
  && .ant-menu-title-content {
    width: 100%;
  }
  &.ant-menu-light.ant-menu-root.ant-menu-vertical {
    border-inline-end: none;
  }
`;

export const MenuItemStyled = styled(FlexFullWidthStyled)<{ level: number }>`
  font-weight: ${({ theme }) => theme.fontWeight['600']};
  color: ${({ theme }) => theme.colors.greys['1000']};
  margin: 0 ${(p) => p.theme.space[3 * p.level]};
`;

export const TypographyStyled = styled(Typography.Text)<{ level?: number; draft?: boolean }>`
  color: ${({ theme }) => theme.token.neutral8};
  width: ${({ level = 0, draft = false }) => (level ? 180 - (draft ? 40 : 0) : 200 - (draft ? 40 : 0))}px;
  white-space: nowrap;
  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    width: ${({ level = 0, draft = false }) => (level ? 220 - (draft ? 40 : 0) : 240 - (draft ? 40 : 0))}px;
  }
`;

export const MenuTitleStyled = styled(Typography.Title)`
  margin: ${({ theme }) => theme.token.paddingMD}px ${({ theme }) => theme.token.paddingLG}px;
`;

export const TagStyled = styled(Tag)`
  display: flex;
  align-items: center;
  font-size: ${({ theme }) => theme.fontSizes.sm};
  font-weight: ${({ theme }) => theme.fontWeight['300']};
  color: ${({ theme }) => theme.colors.greys['1000']};
`;

export const ButtonStyled = styled(Button)`
  svg {
    stroke-width: 50;
    stroke: currentColor;
    font-size: 19px;
    color: ${({ theme }) => theme.token.neutral8};
  }
  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    margin-right: ${({ theme }) => theme.token.paddingSM}px;
  }
`;

export const FlexStyled = styled(Flex)`
  background: ${({ theme }) => theme.token.neutral4};
  min-height: 100%;
  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    margin: ${({ theme }) => `-${theme.token.paddingXXS}px 0 }px`};
  }
  .ant-tree-list {
    background: ${({ theme }) => theme.token.neutral4};
  }
  .ant-tree-treenode-selected {
    background: ${({ theme }) => theme.token.neutral4};
    strong {
      color: ${({ theme }) => theme.token.colorPrimary};
    }
  }
`;
