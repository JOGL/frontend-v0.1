import { useEffect } from 'react';
import { useWebSocket } from '~/src/contexts/websocket/websocketProvider';
import useUser from '~/src/hooks/useUser';

const useInbox = (refetch: () => void): [void] => {
  const { subscribe } = useWebSocket();
  const { user } = useUser();

  const subscribeToFeedUpdates = useEffect(() => {
    if (!user) return;
    const unsubscribe = subscribe(user.id, ['FeedActivity'], () => refetch());

    //Unsubscribe on unmount
    return () => {
      unsubscribe();
    };
  }, []);

  return [subscribeToFeedUpdates];
};

export default useInbox;
