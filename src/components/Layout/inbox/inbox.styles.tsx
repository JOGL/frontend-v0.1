import styled from '@emotion/styled';
import { Flex } from 'antd';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FlexFullWidthStyled } from '../../Common/common.styles';

export const InfiniteScrollContainerStyled = styled(FlexFullWidthStyled)`
  && .infinite-scroll-component__outerdiv {
    max-width: 100%;
  }
`;

export const InfiniteScrollStyled = styled(InfiniteScroll)`
  overflow: initial !important;
`;

export const InboxItemStyled = styled(Flex)<{ selected?: boolean }>`
  height: 120px;
  padding: ${({ theme }) => theme.token.paddingMD}px;

  margin: 0 -${({ theme }) => theme.token.paddingMD}px;
  cursor: pointer;
  &:hover {
    background: ${(p) => p.theme.colors.panelListBG};
  }
  background: ${(p) => (p.selected ? p.theme.colors.panelListBG : 'none')};
  border-radius: ${({ theme }) => `${theme.token.borderRadiusLG}px`};
  align-items: flex-start;
  overflow-y: clip;
`;
