import useDiscussions from '../../discussions/useDiscussions';
import { DiscussionItemModel } from '~/__generated__/types';
import { FC, useMemo } from 'react';
import { InboxItemStyled, InfiniteScrollContainerStyled, InfiniteScrollStyled } from './inbox.styles';
import { List, Skeleton } from 'antd';
import { useInfiniteQuery } from '@tanstack/react-query';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import Loader from '../../Common/loader/loader';
import useInbox from './inbox.hooks';
import { InboxItem } from '../inboxItem/inboxItem';
import { useHash } from '../../Common/useHash';

const PAGE_SIZE = 10;

interface Props {
  loadData: (nodeId: string, page: number, pageSize: number) => Promise<DiscussionItemModel[]>;
  queryKey: string;
}

const Inbox: FC<Props> = ({ loadData, queryKey }) => {
  const [, goToPost, goToFeed] = useDiscussions();
  const hash = useHash();
  
  const { selectedHub } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
  }));

  const isSelected = (item: DiscussionItemModel) => {
    return item.id === hash;
  };

  const { data, isLoading, hasNextPage, fetchNextPage, isFetchingNextPage, refetch } = useInfiniteQuery({
    queryKey: [queryKey, selectedHub?.id],
    queryFn: async ({ pageParam }) => {
      if (!selectedHub) {
        return [];
      }

      return await loadData(selectedHub.id, pageParam, PAGE_SIZE);
    },
    getNextPageParam: (lastPage, allPages) => {
      return lastPage.length === PAGE_SIZE ? allPages.length + 1 : undefined;
    },
    initialPageParam: 1,
    enabled: !!selectedHub,
  });

  useInbox(() => {
    refetch();
  });

  const flattenedData = useMemo(() => {
    return data?.pages.flatMap((page) => page ?? []) ?? [];
  }, [data]);

  if (!selectedHub) {
    return <></>;
  }

  if (isLoading) {
    return <Loader />;
  }

  return (
    <InfiniteScrollContainerStyled>
      <InfiniteScrollStyled
        dataLength={flattenedData.length}
        next={fetchNextPage}
        hasMore={!!hasNextPage}
        loader={<Skeleton avatar paragraph={{ rows: 1 }} active={true} />}
        scrollableTarget="secondary-menu"
      >
        <List<DiscussionItemModel>
          dataSource={flattenedData}
          loading={isLoading && !isFetchingNextPage}
          renderItem={(item) => (
            <InboxItemStyled
              selected={isSelected(item)}
              key={item.id}
              onClick={() => {
                item.is_new = false;
                if(item.feed_entity?.id) 
                {
                  if (item.is_reply === true)
                    goToPost(item.feed_entity.id, item.content_entity_id, item.is_reply === true ? item.id : undefined);
                  else 
                    goToFeed(item.feed_entity.id, item.id);
                }
              }}
            >
              <InboxItem item={item} />
            </InboxItemStyled>
          )}
        />
      </InfiniteScrollStyled>
    </InfiniteScrollContainerStyled>
  );
};

export default Inbox;
