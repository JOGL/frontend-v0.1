import styled from '@emotion/styled';

export const TabsContainerStyled = styled.div`
  padding: ${({ theme }) => `${theme.token.paddingSM}px ${theme.token.paddingMD}px`};
`;
