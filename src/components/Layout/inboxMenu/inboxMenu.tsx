import { TabsProps, Tabs } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import Inbox from '../inbox/inbox';
import { TabsContainerStyled } from './inboxMenu.styles';
import { useRouter } from 'next/router';
import { useState } from 'react';

const InboxMenu = () => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const [tab, setTab] = useState<string>((router.query.inboxTab as string) ?? 'all');

  const { selectedHub } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
  }));

  const handleTabChange = (tab: string) => {
    setTab(tab);
    router.replace(
      {
        query: { ...router.query, inboxTab: tab },
      },
      undefined,
      { shallow: true, scroll: false }
    );
  };

  if (!selectedHub) return <></>;
  const items: TabsProps['items'] = [
    {
      key: 'all',
      label: t('all'),
      children: (
        <Inbox
          queryKey={QUERY_KEYS.nodeContentEntityList}
          loadData={async (hubId: string, page: number, pageSize: number) => {
            const response = await api.feed.nodePostsListDetail(hubId, { Page: page, PageSize: pageSize });
            return response.data;
          }}
        />
      ),
    },
    {
      key: 'thread',
      label: t('thread.other'),
      children: (
        <Inbox
          queryKey={QUERY_KEYS.nodeThreadContentEntityList}
          loadData={async (hubId: string, page: number, pageSize: number) => {
            const response = await api.feed.nodeThreadsListDetail(hubId, { Page: page, PageSize: pageSize });
            return response.data;
          }}
        />
      ),
    },
    {
      key: 'mentions',
      label: t('mention.other'),
      children: (
        <Inbox
          queryKey={QUERY_KEYS.nodeMentionContentEntityList}
          loadData={async (hubId: string, page: number, pageSize: number) => {
            const response = await api.feed.nodeMentionsListDetail(hubId, { Page: page, PageSize: pageSize });
            return response.data;
          }}
        />
      ),
    },
  ];

  return (
    <TabsContainerStyled >
      <Tabs activeKey={tab} onChange={handleTabChange} items={items} />
    </TabsContainerStyled>
  );
};

export default InboxMenu;
