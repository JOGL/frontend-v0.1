import styled from '@emotion/styled';
import { Typography, Flex, Badge } from 'antd';
import { FlexFullWidthStyled } from '~/src/components/Common/common.styles';

export const FlexStyled = styled(FlexFullWidthStyled)`
  height: 100%;
  position: relative;
`;

export const BadgeStyled = styled(Badge)`
  position: absolute;
  top: 0;
  left: -10px;
`;

export const LocationTextStyled = styled(Flex)`
  width: 130px;
  span {
    color: ${({ theme }) => theme.token.colorPrimary};
    font-size: 10px;
  }
  svg {
    color: ${({ theme }) => theme.token.colorPrimary};
    font-size: 10px;
  }
`;

export const TextStyled = styled(Typography.Text)`
  font-size: 10px;
  white-space: nowrap;
`;

export const ActionTextStyled = styled(Typography.Text)`
  font-size: 10px;
  padding: 0 0 ${({ theme }) => theme.space[2]} 0;
`;

export const SidebarContentStyled = styled(Flex)`
  max-width: calc(100% - 50px);
`;
export const EditorWrapperStyled = styled.div`
  max-width: 100%;
`;
