import { FC } from 'react';
import { Flex, theme, Typography } from 'antd';
import { DiscussionItemModel, DiscussionItemSource } from '~/__generated__/types';
import { useRouter } from 'next/router';
import { displayObjectRelativeDate, getEntityIcon } from '~/src/utils/utils';
import ReadOnlyEditor from '~/src/components/tiptap/readOnlyEditor/readOnlyEditor';
import { AuthorButtonStyled, AvatarStyled } from '../../discussionDetails/discussionFeed.styles';
import { FlexFullWidthStyled } from '~/src/components/Common/common.styles';
import {
  ActionTextStyled,
  TextStyled,
  FlexStyled,
  LocationTextStyled,
  SidebarContentStyled,
  EditorWrapperStyled,
  BadgeStyled,
} from './inboxItem.styles';
import { UserOutlined } from '@ant-design/icons';
import { stripHtml } from 'string-strip-html';
import useUsers from '~/src/components/users/useUsers';
import useTranslation from 'next-translate/useTranslation';

interface Props {
  item: DiscussionItemModel;
}

const { Text } = Typography;

export const InboxItem: FC<Props> = ({ item }) => {
  const { locale } = useRouter();
  const [, getUserUrl] = useUsers();
  const router = useRouter();
  const { token } = theme.useToken();
  const { t } = useTranslation('common');

  const getActionString = (item: DiscussionItemModel) => {
    switch (item.source) {
      case DiscussionItemSource.Mention:
        return t('mentionedIn');
      case DiscussionItemSource.Reply:
        return t('threadIn');
      default:
        return t('postIn');
    }
  };

  const getPostUserUrl = (item: DiscussionItemModel) => {
    return item.overrides?.user_url ?? getUserUrl(item.created_by_user_id);
  };

  return (
    <FlexFullWidthStyled vertical>
      <FlexStyled vertical gap="small">
        <FlexFullWidthStyled justify="space-between" align="center">
          {item.is_new && <BadgeStyled color={token.colorPrimary} dot offset={[0, -4]} />}
          <Flex gap="small" justify="start">
            <TextStyled type="secondary">{stripHtml(getActionString(item)).result}</TextStyled>
            <LocationTextStyled gap={4}>
              {item.feed_entity && getEntityIcon(item.feed_entity.entity_type)}
              <Typography.Text ellipsis={{ tooltip: item.feed_entity?.title }}>
                {item.feed_entity?.title}
              </Typography.Text>
            </LocationTextStyled>
          </Flex>
          <TextStyled ellipsis type="secondary">
            {displayObjectRelativeDate(item.created, locale ?? 'en', false, false)}
          </TextStyled>
        </FlexFullWidthStyled>
        <Flex gap="small">
          <AvatarStyled
            size={36}
            shape="circle"
            src={item.overrides?.user_image_url ?? item.created_by?.logo_url_sm}
            icon={<UserOutlined />}
          />
          <SidebarContentStyled vertical justify="start" align="start">
            <AuthorButtonStyled
              type="link"
              onClick={() => {
                const route = getPostUserUrl(item);
                route && router.push(route);
              }}
            >
              <Text strong>
                {item?.overrides?.user_name ?? item?.created_by?.first_name + ' ' + item?.created_by?.last_name}
              </Text>
            </AuthorButtonStyled>
            {item.reply_to_text && (
              <ActionTextStyled ellipsis type="secondary">
                {t('repliedTo', { text: stripHtml(item.reply_to_text).result ?? '' })}
              </ActionTextStyled>
            )}
            <EditorWrapperStyled>
              <Typography.Paragraph
                type="secondary"
                ellipsis={{
                  rows: item.reply_to_text ? 1 : 2,
                  expandable: false,
                }}
              >
                {stripHtml(item?.text ?? '').result}
              </Typography.Paragraph>
            </EditorWrapperStyled>
          </SidebarContentStyled>
        </Flex>
      </FlexStyled>
    </FlexFullWidthStyled>
  );
};
