import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { addressFront } from '~/src/utils/utils';

interface MetaHeadProps {
  favicon: string;
  title?: string;
  desc?: string;
  img?: string;
  noIndex?: boolean;
}

const HeadTags = ({ title, desc = '', img, favicon, noIndex = false }: MetaHeadProps) => {
  const { t, lang } = useTranslation('common');
  const router = useRouter();
  const description = desc?.length > 150 ? `${desc.slice(0, 150)}...` : desc;
  const defaultTitle = 'JOGL - Just One Giant Lab';

  return (
    <>
      <title>{title || defaultTitle}</title>
      <link rel="icon" href={favicon} type="image/svg+xml" />
      <link rel="apple-touch-icon" href={favicon} />
      <link rel="mask-icon" href={favicon} color="#000000" />
      <meta charSet="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no maximum-scale=1" />
      <meta name="theme-color" content="#ffffff" />
      <meta name="description" content={description || t('joglMission')} />
      {/* Social media */}
      <meta property="og:type" content="website" />
      <meta property="og:url" content={addressFront + router.asPath} />
      <meta property="og:title" content={title || defaultTitle} />
      <meta property="og:image" content={img || `${addressFront}/images/JOGL_banner_2021.png`} />
      <meta property="og:description" content={description || t('joglMission')} />
      <meta property="og:site_name" content="JOGL - Just One Giant Lab" />
      <meta property="og:locale" content={lang} />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:site" content="@justonegiantlab" />
      <meta name="application-name" content="JOGL" />
      {noIndex && <meta name="robots" content="noindex,nofollow" />}
    </>
  );
};

export default HeadTags;
