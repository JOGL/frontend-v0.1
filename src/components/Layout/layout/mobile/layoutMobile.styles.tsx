import styled from '@emotion/styled';

export const HeaderWrapper = styled.div<{ showPwa?: boolean }>`
  position: fixed;
  top: ${({ showPwa }) => (showPwa ? 60 : 0)}px;
  left: 0;
  right: 0;
  width: 100%;
  display: flex;
  z-index: 1000;
  height: 60px;
  background-color: ${({ theme }) => theme.token.neutral1};
  align-items: center;
  padding: ${({ theme }) => `${theme.token.paddingXS}px ${theme.token.paddingSM}px`};
`;

export const FooterWrapper = styled.div<{ isVisible?: boolean }>`
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  width: 100%;
  display: flex;
  z-index: 1000;
  height: 80px;
  background-color: ${({ theme }) => theme.token.neutral1};
  align-items: center;
  padding: ${({ theme }) => `${theme.space[2]} ${theme.space[3]}`};
  border-top: 1px solid ${({ theme }) => theme.token.colorBorder};
  transition: transform 0.3s ease;
  transform: translateY(${({ isVisible = true }) => (isVisible ? '0' : '100%')});
`;

export const ContentWrapper = styled.div<{ showPwa?: boolean; isEditorFocused?: boolean }>`
  border-top: 1px solid ${(p) => p.theme.token.colorBorder};
  flex: 4;
  overflow-y: auto;
  margin-top: ${({ showPwa }) => (showPwa ? 120 : 60)}px;
  height: calc(
    100dvh -
      ${({ showPwa, isEditorFocused }) => {
        const pwaOffset = showPwa ? 200 : 140;
        return isEditorFocused ? pwaOffset - 80 : pwaOffset;
      }}px
  );
  padding: ${({ theme }) => `${theme.token.paddingXXS}px ${theme.token.paddingXXS}px`};
  transition: height 0.3s ease;
`;
