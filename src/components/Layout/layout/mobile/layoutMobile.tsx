import { ContentWrapper, FooterWrapper, HeaderWrapper } from './layoutMobile.styles';
import FooterMobile from '../../mobile/footerMobile/footerMobile';
import HeaderMobile from '../../mobile/headerMobile/headerMobile';
import { usePwa } from '~/src/pwa/pwa';
import PwaAlert from '../../mobile/pwaBanner/pwaBanner';
import { useEditorFocusStore } from '~/src/store/editorOnFocusStore/editorOnFocusStore';

export default function LayoutMobile({ children }) {
  const { showPwaAlert, showIosPwaAlert, manualInstallInfo } = usePwa();
  const showPwa = showPwaAlert || showIosPwaAlert || manualInstallInfo;
  const isEditorFocused = useEditorFocusStore((state) => state.isEditorFocused);

  return (
    <>
      <PwaAlert />
      <HeaderWrapper showPwa={showPwa}>
        <HeaderMobile />
      </HeaderWrapper>
      <ContentWrapper showPwa={showPwa} isEditorFocused={isEditorFocused}>
        {children}
      </ContentWrapper>
      <FooterWrapper isVisible={!isEditorFocused}>
        <FooterMobile />
      </FooterWrapper>
    </>
  );
}
