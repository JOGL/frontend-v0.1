export const createDynamicFavicon = async (count: number): Promise<string> => {
  let notificationSvg = await fetch('/images/notification-favicon.svg').then((res) => res.text());
  // Update the count in the SVG
  notificationSvg = notificationSvg.replace('>55<', `>${count > 99 ? '!' : count}<`);

  const parser = new DOMParser();
  const notificationDoc = parser.parseFromString(notificationSvg, 'image/svg+xml');

  const serializer = new XMLSerializer();
  const updatedSvgString = serializer.serializeToString(notificationDoc);
  return `data:image/svg+xml;base64,${btoa(updatedSvgString)}`;
};
