import useUser from '~/src/hooks/useUser';
import { ReactNode, useEffect } from 'react';
import { clarity } from 'react-microsoft-clarity';
import Head from 'next/head';
import useMobile from '~/src/hooks/useMobile';
import { BodyStyled } from './layout.styles';
import LayoutDesktop from './desktop/layoutDesktop';
import LayoutMobile from './mobile/layoutMobile';
import HeadTags from '../headTags/headTags';

interface LayoutProps {
  title?: string;
  desc?: string;
  noIndex?: boolean;
  children?: ReactNode;
}

export default function Layout({ title, desc = '', noIndex = false, children }: LayoutProps) {
  const { user } = useUser();
  const isMobile = useMobile();

  useEffect(() => {
    if (user) {
      clarity.identify(user.id, {});
    }
  }, [user]);

  return (
    <>
      <Head>
        <HeadTags desc={desc} title={title} favicon={'/images/jogl-favicon.svg'} noIndex={noIndex} />
      </Head>
      <BodyStyled>
        {isMobile === false && <LayoutDesktop>{children}</LayoutDesktop>}
        {isMobile === true && <LayoutMobile>{children}</LayoutMobile>}
      </BodyStyled>
    </>
  );
}
