import styled from '@emotion/styled';

export const HeaderWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  width: 100%;
  display: flex;
  z-index: 1000;
  height: 60px;
  background-color: ${({ theme }) => theme.token.neutral1};
  align-items: center;
  padding: ${({ theme }) => `${theme.token.paddingXS}px ${theme.token.paddingSM}px`};
`;

export const Container = styled.main`
  display: flex;
  height: calc(100vh - 60px);
  margin-top: 60px;
`;

export const PrimaryMenuWrapper = styled.div`
  position: sticky;
  overflow-y: hidden;
  height: calc(100vh - 60px);
  padding: ${({ theme }) => `${theme.token.paddingSM}px`};
`;

export const SecondaryMenuWrapper = styled.div`
  background: ${({ theme }) => theme.token.neutral4};
  flex: 1;
  position: sticky;
  overflow-y: auto;
  height: calc(100vh - 60px);
  padding: ${({ theme }) => `${theme.token.paddingSM}px ${theme.token.paddingXS}px`};
  min-width: 350px;
`;

export const ContentWrapper = styled.div`
  border-top: 1px solid ${(p) => p.theme.token.colorBorder};
  flex: 4;
  overflow-y: auto;
  height: calc(100vh - 60px);
  padding: ${({ theme }) => theme.token.paddingXL}px;
`;
