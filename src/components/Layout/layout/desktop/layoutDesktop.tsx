import {
  Container,
  ContentWrapper,
  PrimaryMenuWrapper,
  SecondaryMenuWrapper,
  HeaderWrapper,
} from './layoutDesktop.styles';
import HeaderNew from '../../header/header';
import PrimaryMenu from '../../primaryMenu/primaryMenu';
import SecondaryMenu from '../../secondaryMenu/secondaryMenu';
import PushNotification from '~/src/components/pushNotifications/pushNotification';
import { useAuth } from '~/auth/auth';

export default function LayoutDesktop({ children }) {
  const { isLoggedIn } = useAuth();

  return (
    <>
      {isLoggedIn === true && (
        <>
          <HeaderWrapper>
            <HeaderNew />
          </HeaderWrapper>
          <Container>
            <PrimaryMenuWrapper>
              <PrimaryMenu />
            </PrimaryMenuWrapper>
            <SecondaryMenuWrapper id="secondary-menu">
              <SecondaryMenu />
            </SecondaryMenuWrapper>
            <ContentWrapper>
              {children}
              <PushNotification />
            </ContentWrapper>
          </Container>
        </>
      )}
      {isLoggedIn !== true && (
        <>
          <HeaderWrapper>
            <HeaderNew />
          </HeaderWrapper>
          <Container>
          <ContentWrapper>
              {children}
            </ContentWrapper>
          </Container>
        </>
      )}
    </>
  );
}
