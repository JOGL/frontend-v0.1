import { Button, Dropdown, Flex, Typography } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { BadgeStyled, MenuItemStyled, MenuStyled, PlusOutlinedStyled, TagStyled, TypographyStyled } from './channelMenu.styles';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import useDiscussions from '../../discussions/useDiscussions';
import useTitleHelper from '~/src/hooks/useTitleHelper';
import { useEffect, useState } from 'react';
import { getDiscussionIcon } from '../../discussions/discussionItem/discussionItem.utils';
import { useRouter } from 'next/router';
import { theme } from 'antd';
import { WorkspaceDiscussionCreate } from '../../discussions/workspace/workspaceDiscussionCreate';
import { useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import { ChannelExtendedModel, ChannelVisibility, Permission } from '~/__generated__/types';
import { LockOutlined } from '@ant-design/icons';
import { FlexFullWidthStyled } from '../../Common/common.styles';
import { useUnreadStore } from '~/src/store/unread/unreadStore';

const ChannelMenu = () => {
  const { t } = useTranslation('common');
  const [goToChannel] = useDiscussions();
  const [getTitle] = useTitleHelper();
  const router = useRouter();
  const { token } = theme.useToken();
  const [openCreateModal, setOpenCreateModal] = useState('');

  const selectedChannelId = router.query.id as string;

  const { selectedHub, openMenuKeys, setOpenMenuKeys } = useHubDiscussionsStore((store) => ({
    selectedHub: store.selectedHubDiscussion,
    openMenuKeys: store.openMenuKeys,
    setOpenMenuKeys: store.setOpenMenuKeys,
  }));

  const { hasNewItems } = useUnreadStore((store) => ({
    hasNewItems: store?.hasNewItemsEntity,
  }));

  const { data, refetch } = useQuery({
    queryKey: [QUERY_KEYS.nodesDetailDetail, selectedHub],
    queryFn: async () => {
      const response = await api.feed.nodesDetailDetail(selectedHub?.id ?? '');
      return response.data;
    },
  });
  const entities = data?.entities || [];

  useEffect(() => {
    if (selectedHub && entities) {
      const allKeys = selectedHub.entities.flatMap((entity) => [
        entity.id,
        ...entity.channels.map((channel) => `${entity.id}_${channel.id}`),
      ]);
      setOpenMenuKeys(allKeys);
    }
  }, [selectedHub, setOpenMenuKeys]);

  if (!selectedHub) return null;

  const handleChannelClick = (e: React.MouseEvent, channelId: string) => {
    e.preventDefault();
    e.stopPropagation();
    goToChannel(channelId);
  };
  const handleOpenChange = (keys: string[]) => {
    setOpenMenuKeys(keys);
  };

  const getChannelItems = (channels: ChannelExtendedModel[]) => {
    return channels.map((channel: ChannelExtendedModel) => {
      return {
        key: channel.id,
        icon: getDiscussionIcon(channel.icon_key),
        label: (
          <FlexFullWidthStyled
            onClick={(e) => handleChannelClick(e, channel.id)}
            style={{
              color: selectedChannelId === channel.id ? token.colorPrimary : 'inherit',
            }}
          >
            <MenuItemStyled justify="space-between" align="center" level={0}>
              <BadgeStyled dot={hasNewItems('channels', channel.id)}>
                <TypographyStyled strong> {getTitle(channel.title)} </TypographyStyled>
              </BadgeStyled>
            </MenuItemStyled>
            {channel.visibility === ChannelVisibility.Private && <LockOutlined />}
          </FlexFullWidthStyled>
        ),
      };
    });
  };

  return (
    <Flex vertical align="stretch">
      {selectedHub && (
        <MenuStyled
          mode="inline"
          openKeys={openMenuKeys}
          onOpenChange={handleOpenChange}
          selectedKeys={[selectedChannelId]}
          defaultOpenKeys={openMenuKeys}
          items={entities.map((entity) => ({
            key: entity.id,
            label: (
              <MenuItemStyled level={entity.level} justify="space-between" align="center">
                <TypographyStyled ellipsis={{ tooltip: getTitle(entity.title) }}>
                  {getTitle(entity.title)}
                </TypographyStyled>
                <Flex align="center">
                  {entity.status === 'draft' && <TagStyled>{t('draft').toLowerCase()}</TagStyled>}
                  <Dropdown
                    trigger={['click']}
                    placement="bottomRight"
                    menu={{
                      onClick: ({ domEvent }) => {
                        domEvent.stopPropagation();
                      },
                      items: [
                        ...(entity.user_access.permissions.includes(Permission.Manage)
                          ? [
                              {
                                label: t('action.add'),
                                key: 'add',
                                onClick: (e) => {
                                  e.domEvent.stopPropagation();
                                  setOpenCreateModal(entity.id);
                                },
                              },
                            ]
                          : []),

                        {
                          label: t('action.browse'),
                          key: 'browse',
                          onClick: (e) => {
                            e.domEvent.stopPropagation();
                            router.push(`/browse/${entity.id}`);
                          },
                        },
                      ],
                    }}
                  >
                    <Button
                      icon={<PlusOutlinedStyled />}
                      type="link"
                      onClick={(e) => {
                        e.preventDefault();
                        e.stopPropagation();
                      }}
                    />
                  </Dropdown>
                </Flex>
              </MenuItemStyled>
            ),
            children: getChannelItems(entity.channels),
          }))}
        />
      )}

      {openCreateModal && (
        <WorkspaceDiscussionCreate
          workspaceId={openCreateModal}
          onClose={() => setOpenCreateModal('')}
          onCreate={() => refetch()}
        />
      )}
    </Flex>
  );
};

export default ChannelMenu;
