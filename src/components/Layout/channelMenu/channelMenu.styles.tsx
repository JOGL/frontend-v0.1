import { PlusOutlined } from '@ant-design/icons';
import styled from '@emotion/styled';
import { Badge, Flex, Menu, Tag, Typography } from 'antd';

export const MenuStyled = styled(Menu)`
  &.ant-menu-light.ant-menu-root.ant-menu-inline {
    border-inline-end: none;
  }
  background: ${({ theme }) => theme.token.neutral4};
  &.ant-menu-inline {
    .ant-menu-submenu-arrow {
      transition: transform 0.3s;
    }
  }
  &.ant-menu-light.ant-menu-inline .ant-menu-sub.ant-menu-inline {
    background: none;
  }
  && .ant-menu-item-selected strong {
    color: ${({ theme }) => theme.token.colorPrimary};
  }

  && .ant-menu-title-content {
    overflow: visible !important;
  }
`;

export const TypographyStyled = styled(Typography.Text)`
  color: ${({ theme }) => theme.token.neutral8};
`;

export const MenuItemStyled = styled(Flex)<{ level?: number }>`
  padding-left: ${({ level = 0 }) => level * 16}px;
  width: 100%;
  white-space: nowrap;
`;

export const TagStyled = styled(Tag)`
  margin: ${({ theme }) => theme.token.marginXS}px;
`;

export const BadgeStyled = styled(Badge)`
  && .ant-badge-dot {
    background: ${({ theme }) => theme.colors.badge};
  }
`;

export const PlusOutlinedStyled = styled(PlusOutlined)`
  svg {
    stroke-width: 50;
    stroke: currentColor;
    font-size: 12px;
  }
`;
