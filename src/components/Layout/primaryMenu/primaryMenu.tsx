import { Flex } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { MenuStyled, MenuItemIconStyled, MenuItemTextStyled } from './primaryMenu.styles';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import { BadgeStyled, FlexFullHeightStyled } from '~/src/components/Common/common.styles';
import { menuItems } from '../menuItems/menuItems';
import { useRouter } from 'next/router';
import usePrimaryMenu from './primaryMenu.hooks';
import { useUnreadStore } from '~/src/store/unread/unreadStore';

const PrimaryMenu = () => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const [getPath] = usePrimaryMenu();

  const { selectedHub, selectedMenuItem, setSelectedMenuItem } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
    selectedMenuItem: store?.selectedMenuItem,
    setSelectedMenuItem: store?.setSelectedMenuItem,
  }));

  const { hasNew } = useUnreadStore((store) => ({
    hasNew: store?.hasNewItemsHub,
  }));

  return (
    <FlexFullHeightStyled vertical align="stretch" justify="space-between">
      <Flex vertical align="stretch">
        <MenuStyled
          selectedKeys={!!selectedMenuItem ? [selectedMenuItem] : []}
          items={Object.entries(menuItems).map(([key, item]) => ({
            key: key,
            label: (
              <Flex vertical align="center" justify="center">
                <MenuItemIconStyled>{item.icon}</MenuItemIconStyled>
                <MenuItemTextStyled>{t(item.translationId)}</MenuItemTextStyled>
                <BadgeStyled dot={hasNew(key, selectedHub?.id) === true} offset={[6, -38]} />
              </Flex>
            ),
            onClick: () => {
              setSelectedMenuItem(key);
              router.push(
                `/${getPath(item.front_path_desktop, {
                  id: selectedHub?.id,
                  home_channel_id: selectedHub?.home_channel_id,
                })}`
              );
            },
          }))}
        />
      </Flex>
    </FlexFullHeightStyled>
  );
};

export default PrimaryMenu;
