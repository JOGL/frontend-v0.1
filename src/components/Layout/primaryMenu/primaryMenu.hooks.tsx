const usePrimaryMenu = (): [(path: string, params: object) => string] => {
  const getPath = (path: string, params: object) => {
    let result = path;
    Object.entries(params).forEach(([key, value]) => {
      result = result.replace(`:${key}`, String(value));
    });
    return result;
  };

  return [getPath];
};

export default usePrimaryMenu;
