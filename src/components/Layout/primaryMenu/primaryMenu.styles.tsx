import styled from '@emotion/styled';
import { Menu, Typography } from 'antd';
import { FlexFullHeightStyled } from '~/src/components/Common/common.styles';

export const MenuStyled = styled(Menu)`
  &.ant-menu-light.ant-menu-root.ant-menu-vertical {
    border-inline-end: none;
  }

  && .ant-menu-item {
    max-width: 100px;
    height: 46px;
    line-height: 46px;
    padding: ${({ theme }) => theme.token.paddingXS}px;
    margin: ${({ theme }) => theme.token.paddingSM}px 0;
    display: flex;
    justify-content: center;
  }

  && .ant-menu-item-selected span {
    color: ${({ theme }) => theme.token.colorPrimary};
  }
`;

export const MenuItemTextStyled = styled(Typography.Text)`
  max-width: 90px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;
export const MenuItemIconStyled = styled(FlexFullHeightStyled)`
  svg {
    font-size: ${({ theme }) => theme.token.fontSizeLG}px;
  }
`;
