import { FC, useState } from 'react';
import tw from 'twin.macro';
import { FilterNotification } from './NotificationUtils';
import { confAlert, displayObjectRelativeDate, entityItem } from '~/src/utils/utils';
import { useApi } from '~/src/contexts/apiContext';
import Loading from '../Tools/Loading';
import { useModal } from '~/src/contexts/modalContext';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { usePushNotificationStore } from '~/src/store/push-notifications/PushNotificationsStoreProvider';
import { Button, Modal } from 'antd';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import ShowOnboarding from '../Onboarding/ShowOnboarding';

interface ActionCardProps {
  notification?: FilterNotification;
  reload?: () => Promise<any>;
}

const ActionCard: FC<ActionCardProps> = ({ notification, reload }) => {
  const api = useApi();
  const router = useRouter();
  const { locale } = useRouter();
  const { t } = useTranslation('common');
  const [loading, setLoading] = useState(false);
  const { showModal, closeModal } = useModal();
  const isRequestNotification = notification?.type === 'adminrequest';
  const isInviteNotification = notification?.type === 'invite';
  const isEventInviteNotification = notification?.type === 'eventinvite' || notification?.type === 'eventinviteupdate';
  const hasUserAnsweredQuestions = isRequestNotification && notification?.data[2]?.entity_onboarding_enabled;
  const [showOnboardingModalType, setShowOnboardingModalType] = useState<'first_onboarding' | 'second_onboarding'>();

  const { showPushNotificationModal } = usePushNotificationStore((s) => ({
    showPushNotificationModal: s.showPushNotificationModal,
  }));
  const communityEntityData = notification?.data?.reverse().find((data: any) => data?.key === 'communityentity');

  const { setSelectedHub, initializeHubs } = useHubDiscussionsStore((store) => ({
    setSelectedHub: store.setSelectedHubDiscussionWithId,
    initializeHubs: store.initialize,
  }));

  const deleteNotification = async () => {
    try {
      await api.post(`/users/notifications/${notification?.id}/actioned`);
    } catch (e) {
      throw e;
    }
  };

  const acceptOrRejectInvite = async (type: 'accept' | 'reject') => {
    //Use reverse+find because some of our clients Safari versions do not support findLast

    const invitationData = notification?.data?.find((data: any) => data?.key === 'invitation');
    const communityEntityDataType = communityEntityData?.community_entity_type;

    try {
      setLoading(true);
      const route = isEventInviteNotification
        ? `/events/attendances/${invitationData.entity_id}/${type}`
        : `/${entityItem[communityEntityData?.community_entity_type]?.back_path}/${communityEntityData?.entity_id}/${
            isInviteNotification || isRequestNotification ? 'invites' : 'communityEntityInvites'
          }/${invitationData?.entity_id}/${type}`;
      await api.post(route);
      await deleteNotification();
      reload && reload();
      confAlert.fire({ icon: 'success', title: `Successfully ${type}ed` });
      closeModal();

      if (notification?.type === 'invite' && type == 'accept') {
        switch (communityEntityDataType) {
          case 'node':
            await initializeHubs();
            setSelectedHub(communityEntityData.entity_id);

            break;
          case 'workspace':
            await initializeHubs();
            break;
        }
        if (communityEntityData.community_entity_onboarding?.rules?.enabled) {
          setShowOnboardingModalType('second_onboarding');
        }
        showPushNotificationModal(router);
      }
    } catch (e) {
      console.log(e);
      confAlert.fire({ icon: 'error', title: t('someUnknownErrorOccurred') });
      closeModal();
    }
    setLoading(false);
  };

  const seeOnboardingAnswers = async () => {
    let res: any;
    const userData = notification?.data?.find((data: any) => data?.key === 'user');
    const communityEntityDataType = communityEntityData?.community_entity_type;
    try {
      setLoading(true);
      res = await api.get(
        `/${entityItem[communityEntityDataType]?.back_path}/${communityEntityData?.entity_id}/onboardingResponses/${userData?.entity_id}`
      );
    } catch (e) {
      // confAlert.fire({ icon: 'error', title: 'No answers for this user' });
      console.warn('No answers for this user');
    }
    showModal({
      children: (
        <div tw="flex flex-col gap-4">
          {res?.data?.items?.map((item, i) => (
            <div key={item.id} tw="flex flex-col">
              <span tw="text-[#717171] text-[13px]">
                {t('question.one')} {i + 1}
              </span>
              <span tw="font-semibold text-[15px] mb-2">{item.question}</span>
              <span tw="py-2 px-2 bg-[#F2F1F1] rounded-sm text-[13px]">{item.answer}</span>
            </div>
          ))}
          <div tw="flex justify-center items-center gap-4 mt-4">
            <Button onClick={() => acceptOrRejectInvite('reject')}>{t('action.reject')}</Button>
            <Button type="primary" onClick={() => acceptOrRejectInvite('accept')} tw="py-[2px] px-[45px] text-[14px]">
              {t('action.accept')}
            </Button>
          </div>
        </div>
      ),
      title: `Answers to join ${communityEntityData?.community_entity_type}`,
    });
    setLoading(false);
  };

  return (
    <Loading active={loading}>
      {showOnboardingModalType && (
        <Modal
          open
          title={showOnboardingModalType === 'first_onboarding' ? t('answeringQuestions') : t('welcome')}
          onCancel={() => setShowOnboardingModalType(undefined)}
          closable={!(showOnboardingModalType === 'second_onboarding')}
          footer={null}
        >
          <ShowOnboarding
            object={{
              onboarding: communityEntityData.community_entity_onboarding,
              id: communityEntityData?.entity_id,
            }}
            itemType="nodes"
            type={showOnboardingModalType}
            setIsOnboardingComplete={(isOnboardingComplete: boolean) => {
              setShowOnboardingModalType(undefined);
            }}
          />
        </Modal>
      )}

      <div
        tw="flex flex-col md:flex-row justify-between items-center gap-2 py-2 px-4 my-2 rounded-lg"
        css={notification?.actioned ? tw`bg-[#F1F2F5]` : tw`bg-[#D5D9DE] shadow-custom`}
        onClick={(e) => e.preventDefault()}
      >
        <div tw="flex flex-col md:flex-row items-center gap-2 flex-1">
          <div tw="flex flex-col flex-1">
            <div tw="(text-[14px] p-0 w-[90%] flex-1 text-center md:text-start)!">{notification?.value}</div>
          </div>
          <div tw="flex gap-3">
            {(notification?.type === 'invite' ||
              (notification?.type === 'adminrequest' && !hasUserAnsweredQuestions) ||
              notification?.type === 'admincommunityentityinvite' ||
              notification?.type === 'eventinvite' ||
              notification?.type === 'eventinviteupdate') &&
              !notification.actioned && (
                <>
                  <Button onClick={() => acceptOrRejectInvite('reject')}>Reject</Button>
                  <Button type="primary" onClick={() => acceptOrRejectInvite('accept')}>
                    Accept
                  </Button>
                </>
              )}
            {notification?.type === 'adminrequest' && hasUserAnsweredQuestions && !notification.actioned && (
              <Button type="primary" onClick={seeOnboardingAnswers}>
                See all answers
              </Button>
            )}
          </div>
        </div>
        <span tw="text-[12px] text-[#454545]">{displayObjectRelativeDate(notification?.created, locale)}</span>
      </div>
    </Loading>
  );
};

export default ActionCard;
