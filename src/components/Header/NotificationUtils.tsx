import { entityItem } from '~/src/utils/utils';
import A from '../primitives/A';
import Link from 'next/link';
import { Translate } from 'next-translate';
import { NotificationModel } from '~/__generated__/types';

export interface NotificationType {
  admin: ReadType;
  adminCount: number;
  personal: ReadType;
  personalCount: number;
}

export interface ReadType {
  notRead: FilterNotification[];
  read: FilterNotification[];
}

export interface FilterNotification {
  created: number;
  value: JSX.Element;
  type?: string;
  count?: number;
  mainImg?: string;
  backImg?: string;
  id: string;
  action: boolean;
  actioned: boolean;
  data?: any[];
  isAdmin?: boolean;
  entityName?: string;
  entityID?: string;
  entityType?: string;
  aggregateAllowed?: boolean;
}

export interface Notification {
  type?: string;
  data?: any[];
  created?: string;
  id: string;
}

const aggregateNotification = (type: string, filterType: FilterNotification[]): FilterNotification[] => {
  const valueJSX = (newCount: number, preVal?: FilterNotification): JSX.Element => {
    switch (type) {
      case 'follower':
        return (
          <>
            <b>{newCount} followers</b> follow you
          </>
        );
      case 'need':
        return (
          <>
            <b>{newCount} needs</b> have been created in{' '}
            <A href={`/${preVal?.entityType}/${preVal?.entityID}`} tw="text-black" passHref withInherit>
              <b>{preVal?.entityName}</b>
            </A>
          </>
        );
      case 'jogldoc':
        return (
          <>
            <b>{newCount} JOGL Docs</b> have been created in{' '}
            <A href={`/${preVal?.entityType}/${preVal?.entityID}`} tw="text-black" passHref withInherit>
              <b>{preVal?.entityName}</b>
            </A>
          </>
        );
      case 'paper':
        return (
          <>
            <b>{newCount} papers</b> were added to{' '}
            <A href={`/${preVal?.entityType}/${preVal?.entityID}`} tw="text-black" passHref withInherit>
              <b>{preVal?.entityName}</b>
            </A>
          </>
        );
      case 'relation':
        return (
          <>
            <b>{newCount} workspaces</b> joined{' '}
            <A href={`/${preVal?.entityType}/${preVal?.entityID}`} tw="text-black" passHref withInherit>
              <b>{preVal?.entityName}</b>
            </A>
          </>
        );
      case 'member':
        return (
          <>
            <b>{newCount} new member(s)</b> joined{' '}
            <A href={`/${preVal?.entityType}/${preVal?.entityID}`} tw="text-black" passHref withInherit>
              <b>{preVal?.entityName}</b>
            </A>
          </>
        );
      case 'comment':
        return (
          <>
            <A href={`/${preVal?.entityType}/${preVal?.entityID}`} tw="text-black" passHref withInherit>
              <b>{preVal?.entityName}</b>
            </A>{' '}
            has <b>{newCount}</b> new reply
          </>
        );
    }
  };

  const isObjectEmpty = (allCurrentType = {}) => {
    const keys = Object.keys(allCurrentType);
    for (let i = 0; i < keys.length; i++) {
      if (allCurrentType[keys[i]]?.length > 1) {
        return false;
      }
    }
    return true;
  };

  const aggregateFn = (typedNotifications = {}): FilterNotification[] => {
    const aggregatedNotifications: FilterNotification[] = [];
    const value = (typedNotification: FilterNotification[]): FilterNotification => {
      const isCountExists = typedNotification.filter((item) => item?.count > 0);
      if (isCountExists.length === 0) {
        return {
          ...typedNotification[0],
          count: 2,
          value: valueJSX(2, typedNotification[0]),
        };
      } else {
        return {
          ...isCountExists[0],
          count: isCountExists[0]?.count + 1,
          value: valueJSX(isCountExists[0]?.count + 1, isCountExists[0]),
        };
      }
    };

    const keys = Object.keys(typedNotifications);
    for (let i = 0; i < keys.length; i++) {
      if (typedNotifications[keys[i]].length === 0) {
        continue;
      }
      aggregatedNotifications.push(value(typedNotifications[keys[i]]));
    }

    return aggregatedNotifications;
  };

  const allNonCurrentType = [];
  const allCurrentType = {};
  for (let i = 0; i < filterType.length; i++) {
    if (filterType[i].type === type && filterType[i].aggregateAllowed) {
      if (!Array.isArray(allCurrentType[`${filterType[i].entityName ?? ''}-${filterType[i].type}`])) {
        allCurrentType[`${filterType[i].entityName ?? ''}-${filterType[i].type}`] = [];
      }
      allCurrentType[`${filterType[i].entityName ?? ''}-${filterType[i].type}`].push(filterType[i]);
    } else {
      allNonCurrentType.push(filterType[i]);
    }
  }
  if (isObjectEmpty(allCurrentType)) {
    return filterType;
  }

  const typeCountItems: FilterNotification[] = aggregateFn(allCurrentType);

  return allNonCurrentType.push(...typeCountItems) && allNonCurrentType;
};

export const getNotificationDescription = (item: Notification, t: Translate): FilterNotification => {
  let entityName = '';
  let entityID = '';
  let entityType = '';
  let aggregateAllowed = false;
  let action = false;
  let isAdmin = false;
  const value = ((): JSX.Element => {
    const firstItem = item?.data?.[0];
    const secondItem = item?.data?.[1];
    const thirdItem = item?.data?.[2];
    const fourthItem = item?.data?.[3];

    //Temporary fix to invite notifications.
    //When refactoring this component, we should stop using indexes and use keys, like below.
    //Note that any should be replaced by a proper interface when refactoring.
    const communityEntityData = item?.data?.find((data: any) => data?.key === 'communityentity');

    switch (item.type) {
      case 'acceptation':
        return (
          <>
            Your request to join
            <span tw="lowercase">{t(entityItem[firstItem?.community_entity_type]?.translationId)}</span>
            &nbsp;
            <A
              href={`/${entityItem[firstItem?.community_entity_type]?.front_path}/${firstItem?.entity_id}`}
              tw="text-black"
              passHref
              withInherit
            >
              <b>{firstItem?.entity_title}</b>
            </A>
            has been accepted
          </>
        );
      case 'adminaccesslevel':
        isAdmin = true;
        return (
          <>
            {/* You are now <b tw="lowercase">{firstItem?.entity_title}</b> of{' '} */}
            You are now <span tw="lowercase">{firstItem?.entity_title}</span> of{' '}
            <span tw="lowercase">{t(entityItem[secondItem?.community_entity_type]?.translationId)}</span>
            &nbsp;
            <A
              href={`/${entityItem[secondItem?.community_entity_type]?.front_path}/${secondItem?.entity_id}`}
              tw="text-black"
              passHref
              withInherit
            >
              <b>{secondItem?.entity_title}</b>
            </A>
          </>
        );
      case 'follower':
        entityName = 'follower';
        aggregateAllowed = true;
        return (
          <>
            <A href={`/user/${firstItem?.entity_id}`} tw="text-black" passHref withInherit>
              <b>{firstItem?.entity_title}</b>
            </A>{' '}
            now follows you
          </>
        );
      case 'need':
        entityName = thirdItem?.entity_title;
        entityID = thirdItem?.entity_id;
        entityType = entityItem[thirdItem?.community_entity_type]?.front_path;
        aggregateAllowed = true;
        return (
          <>
            <A href={`/user/${firstItem?.entity_id}`} tw="text-black" passHref withInherit>
              <b>{firstItem?.entity_title}</b>
            </A>{' '}
            created{' '}
            <A
              href={`/need/${entityItem[thirdItem?.community_entity_type]?.back_path}/${secondItem?.entity_id}`}
              tw="text-black"
              passHref
              withInherit
            >
              <b>a need</b>
            </A>{' '}
            in <span tw="lowercase">{t(entityItem[thirdItem?.community_entity_type]?.translationId)}</span>
            &nbsp;
            <A href={`/${entityType}/${thirdItem?.entity_id}`} tw="text-black" passHref withInherit>
              <b>{thirdItem?.entity_title}</b>
            </A>
          </>
        );
      case 'jogldoc':
        entityName = secondItem?.entity_title;
        entityType = entityItem[secondItem?.community_entity_type]?.front_path;
        entityID = secondItem?.entity_id;
        if (secondItem?.community_entity_type === 'workspace') {
          aggregateAllowed = true;
        }
        return (
          <>
            <A href={`/user/${firstItem?.entity_id}`} tw="text-black" passHref withInherit>
              <b>{firstItem?.entity_title}</b>
            </A>{' '}
            created <b>a JOGL Doc</b> in&nbsp;
            <span tw="lowercase">{t(entityItem[secondItem?.community_entity_type]?.translationId)}</span>
            &nbsp;
            <A
              href={`/${entityItem[secondItem?.community_entity_type]?.front_path}/${secondItem?.entity_id}`}
              tw="text-black"
              passHref
              withInherit
            >
              <b>{secondItem?.entity_title}</b>
            </A>
          </>
        );
      case 'paper':
        entityName = thirdItem?.entity_title;
        entityType = entityItem[thirdItem?.community_entity_type]?.front_path;
        entityID = thirdItem?.entity_id;
        if (thirdItem?.community_entity_type === 'workspace') {
          aggregateAllowed = true;
        }
        if (thirdItem?.community_entity_type === 'workspace') {
          return (
            <>
              A paper{' '}
              <A href={`/paper/${secondItem?.entity_id}`} tw="text-black" passHref withInherit>
                <b>{secondItem?.entity_title}</b>
              </A>{' '}
              was added to&nbsp;
              <span tw="lowercase">{t(entityItem[thirdItem?.community_entity_type]?.translationId)}</span>
              &nbsp;
              <A
                href={`/${entityItem[thirdItem?.community_entity_type]?.front_path}/${thirdItem?.entity_id}`}
                tw="text-black"
                passHref
                withInherit
              >
                <b>{thirdItem?.entity_title}</b>
              </A>
            </>
          );
        }
        return (
          <>
            <A href={`/user/${firstItem?.entity_id}`} tw="text-black" passHref withInherit>
              <b>{firstItem?.entity_title}</b>
            </A>{' '}
            added{' '}
            <A href={`/paper/${secondItem?.entity_id}`} tw="text-black" passHref withInherit>
              <b>a paper</b>
            </A>
            &nbsp;in&nbsp;
            <span tw="lowercase">{t(entityItem[thirdItem?.community_entity_type]?.translationId)}</span>
            &nbsp;
            <A
              href={`/${entityItem[thirdItem?.community_entity_type]?.front_path}/${thirdItem?.entity_id}`}
              tw="text-black"
              passHref
              withInherit
            >
              <b>{thirdItem?.entity_title}</b>
            </A>
          </>
        );
      case 'relation':
        entityName = secondItem?.entity_title;
        entityID = secondItem?.entity_id;
        entityType = entityItem[secondItem?.community_entity_type]?.front_path;
        if (secondItem?.community_entity_type === 'workspace') {
          aggregateAllowed = true;
        }
        return (
          <>
            <A
              href={`/${entityItem[firstItem?.community_entity_type]?.front_path}/${firstItem?.entity_id}`}
              tw="text-black"
              passHref
              withInherit
            >
              <b>
                {firstItem?.entity_title === ''
                  ? `Draft (${firstItem?.community_entity_type})`
                  : firstItem?.entity_title}
              </b>
            </A>{' '}
            joined&nbsp;<span tw="lowercase">{t(entityItem[secondItem?.community_entity_type]?.translationId)}</span>
            &nbsp;
            <A
              href={`/${entityItem[secondItem?.community_entity_type]?.front_path}/${secondItem?.entity_id}`}
              tw="text-black"
              passHref
              withInherit
            >
              <b>{secondItem?.entity_title}</b>
            </A>
          </>
        );
      case 'member':
        entityName = secondItem?.entity_title;
        entityID = secondItem?.entity_id;
        entityType = entityItem[secondItem?.community_entity_type]?.front_path;
        if (secondItem?.community_entity_type === 'workspace' || secondItem?.community_entity_type === 'node') {
          return (
            <>
              <A href={`/user/${firstItem?.entity_id}`} tw="text-black" passHref withInherit>
                <b>{firstItem?.entity_title}</b>
              </A>{' '}
              joined&nbsp;<span tw="lowercase">{t(entityItem[secondItem?.community_entity_type]?.translationId)}</span>
              &nbsp;
              <A
                href={`/${entityItem[secondItem?.community_entity_type]?.front_path}/${secondItem?.entity_id}`}
                tw="text-black"
                passHref
                withInherit
              >
                <b>{secondItem?.entity_title}</b>
              </A>
            </>
          );
        }
        return (
          <>
            <A href={`/user/${firstItem?.entity_id}`} tw="text-black" passHref withInherit>
              <b>{firstItem?.entity_title}</b>
            </A>{' '}
            is now member of&nbsp;
            <span tw="lowercase">{t(entityItem[secondItem?.community_entity_type]?.translationId)}</span>
            &nbsp;
            <A
              href={`/${entityItem[secondItem?.community_entity_type]?.front_path}/${secondItem?.entity_id}`}
              tw="text-black"
              passHref
              withInherit
            >
              <b>{secondItem?.entity_title}</b>
            </A>
          </>
        );
      case 'resource':
        return (
          <>
            1 new resource was created in{' '}
            <span tw="lowercase">{t(entityItem[secondItem?.community_entity_type]?.translationId)}</span>
            &nbsp;
            <A
              href={`/${entityItem[secondItem?.community_entity_type]?.front_path}/${secondItem?.entity_id}`}
              tw="text-black"
              passHref
              withInherit
            >
              <b>{secondItem?.entity_title}</b>
            </A>
          </>
        );
      case 'jogldocedit':
        return (
          <>
            <A href={`/doc/${firstItem?.entity_id}`} tw="text-black" passHref withInherit>
              <b>{firstItem?.entity_title}</b>
            </A>{' '}
            was edited
          </>
        );
      case 'mention':
        return (
          <>
            <A href={`/user/${secondItem?.entity_id}`} tw="text-black" passHref withInherit>
              <b>{secondItem?.entity_title}</b>
            </A>{' '}
            mentioned you in{' '}
            <span tw="lowercase">{t(entityItem[thirdItem?.community_entity_type]?.translationId)}</span>
            &nbsp;
            <A
              href={`/${entityItem[thirdItem?.community_entity_type]?.front_path}/${thirdItem?.entity_id}`}
              tw="text-black"
              passHref
              withInherit
            >
              <b>{thirdItem?.entity_title}</b>
            </A>
          </>
        );
      case 'adminrelation':
        isAdmin = true;
        return (
          <>
            {t(entityItem[firstItem?.community_entity_type]?.translationId)}
            &nbsp;
            <A
              href={`/${entityItem[firstItem?.community_entity_type]?.front_path}/${firstItem?.entity_id}`}
              tw="text-black"
              passHref
              withInherit
            >
              <b>
                {firstItem?.entity_title === ''
                  ? `Draft (${firstItem?.community_entity_type})`
                  : firstItem?.entity_title}
              </b>
            </A>{' '}
            is now linked with{' '}
            <span tw="lowercase">{t(entityItem[secondItem?.community_entity_type]?.translationId)}</span>
            &nbsp;
            <A
              href={`/${entityItem[secondItem?.community_entity_type]?.front_path}/${secondItem?.entity_id}`}
              tw="text-black"
              passHref
              withInherit
            >
              <b>{secondItem?.entity_title}</b>
            </A>
          </>
        );
      case 'adminmember':
        isAdmin = true;
        return (
          <>
            <A href={`/user/${firstItem?.entity_id}`} tw="text-black" passHref withInherit>
              <b>{firstItem?.entity_title}</b>
            </A>{' '}
            is now member of{' '}
            <span tw="lowercase">{t(entityItem[secondItem?.community_entity_type]?.translationId)}</span>
            &nbsp;
            <A
              href={`/${entityItem[secondItem?.community_entity_type]?.front_path}/${secondItem?.entity_id}`}
              tw="text-black"
              passHref
              withInherit
            >
              <b>{secondItem?.entity_title}</b>
            </A>
          </>
        );
      case 'comment':
        aggregateAllowed = true;
        entityName = firstItem?.entity_title;
        entityID = firstItem?.entity_id;
        entityType = entityItem[firstItem?.content_entity_type]?.front_path;
        return (
          <>
            <A href={`/${entityType}/${firstItem?.entity_id}`} tw="text-black" passHref withInherit>
              <b>{firstItem?.content_entity_type === 'announcement' ? 'Your post' : firstItem?.entity_title}</b>
            </A>{' '}
            has <b>1</b> new reply
          </>
        );
      case 'invite':
        action = true;
        return (
          <>
            You have been invited to become&nbsp;
            {['admin', 'owner'].includes(firstItem?.entity_subtype)
              ? `an ${firstItem?.entity_subtype}`
              : 'a member'}{' '}
            of&nbsp;
            <span tw="lowercase">{t(entityItem[communityEntityData?.community_entity_type]?.translationId)}</span>
            &nbsp;
            <Link
              href={`/${entityItem[communityEntityData?.community_entity_type]?.front_path}/${
                communityEntityData?.entity_id
              }`}
              tw="text-black"
              passHref
            >
              <b>{communityEntityData?.entity_title || 'Untitled'}</b>
            </Link>{' '}
          </>
        );
      case 'adminrequest':
        isAdmin = true;
        action = true;
        return (
          <>
            <A href={`/user/${secondItem?.entity_id}`} tw="text-black" passHref withInherit>
              <b>{secondItem?.entity_title}</b>
            </A>{' '}
            is requesting to become a member of{' '}
            <span tw="lowercase">{t(entityItem[thirdItem?.community_entity_type]?.translationId)}</span>
            &nbsp;
            <A
              href={`/${entityItem[thirdItem?.community_entity_type]?.front_path}/${thirdItem?.entity_id}`}
              tw="text-black"
              passHref
              withInherit
            >
              <b>{thirdItem?.entity_title}</b>
            </A>
          </>
        );
      case 'admincommunityentityinvite':
        isAdmin = true;
        action = true;
        return (
          <>
            {t(entityItem[secondItem?.community_entity_type]?.translationId)}&nbsp;
            <A
              href={`/${entityItem[secondItem?.community_entity_type]?.front_path}/${secondItem?.entity_id}`}
              tw="text-black"
              passHref
              withInherit
            >
              <b>
                {secondItem?.entity_title === ''
                  ? `Draft (${secondItem?.community_entity_type})`
                  : secondItem?.entity_title}
              </b>
            </A>{' '}
            is requesting to link with{' '}
            <span tw="lowercase">{t(entityItem[thirdItem?.community_entity_type]?.translationId)}</span>
            &nbsp;
            <A
              href={`/${entityItem[thirdItem?.community_entity_type]?.front_path}/${thirdItem?.entity_id}`}
              tw="text-black"
              passHref
              withInherit
            >
              <b>{thirdItem?.entity_title}</b>
            </A>
          </>
        );
      case 'eventinvite':
        action = true;
        return (
          <>
            You have been invited by&nbsp;
            <Link href={`/user/${fourthItem?.entity_id}`} tw="text-black" passHref>
              <b>{fourthItem?.entity_title || 'Unnamed'}</b>
            </Link>
            &nbsp;to {firstItem?.entity_subtype === 'admin' ? 'be an organizer of' : 'attend'} the event&nbsp;
            <Link href={`/event/${secondItem?.entity_id}`} tw="text-black" passHref>
              <b>{secondItem?.entity_title || 'Untitled'}</b>
            </Link>
            , from {entityItem[thirdItem?.community_entity_type]?.front_path}&nbsp;
            <Link
              href={`/${entityItem[thirdItem?.community_entity_type]?.front_path}/${thirdItem?.entity_id}`}
              tw="text-black"
              passHref
            >
              <b>{thirdItem?.entity_title || 'Untitled'}</b>
            </Link>
          </>
        );

      case 'eventinviteupdate':
        action = true;
        return (
          <>
            <Link href={`/event/${secondItem?.entity_id}`} tw="text-black" passHref>
              <b>{secondItem?.entity_title || 'Untitled'}</b>
            </Link>{' '}
            has been updated and&nbsp;
            <Link href={`/user/${fourthItem?.entity_id}`} tw="text-black" passHref>
              <b>{fourthItem?.entity_title || 'Unnamed'}</b>
            </Link>
            &nbsp;has re-invited you to {firstItem?.entity_subtype === 'admin' ? 'be an organizer of' : 'attend'} the
            event.
          </>
        );
      default:
        return null;
    }
  })();
  return {
    value,
    type: item?.type,
    entityName,
    entityType,
    entityID,
    count: 0,
    id: item?.id,
    action,
    actioned: item?.actioned,
    isAdmin,
    data: item?.data,
    aggregateAllowed,
    created: new Date(item?.created).getTime(),
  };
};

export const filterNCleanActions = (notifications: NotificationModel[], t: Translate, limit = -1): NotificationType => {
  const allNotification = notifications.map((n) => getNotificationDescription(n, t)).filter((item) => item.action);

  const personalNotification: ReadType = {
    notRead: [],
    read: [],
  };
  const adminNotification: ReadType = {
    notRead: [],
    read: [],
  };
  const key = 'read';
  for (let i = 0; i < allNotification.length; i++) {
    if (allNotification[i].isAdmin) {
      adminNotification[key].push(allNotification[i]);
    } else {
      personalNotification[key].push(allNotification[i]);
    }
  }

  const keys = ['read'];
  let adminCount = 0,
    personalCount = 0;

  for (let i = 0; i < keys.length; i++) {
    adminNotification[keys[i]].sort((a, b) => b.created - a.created);
    adminCount += adminNotification[keys[i]].filter((a) => !a.actioned).length;
    personalNotification[keys[i]].sort((a, b) => b.created - a.created);
    personalCount += personalNotification[keys[i]].filter((a) => !a.actioned).length;
    if (limit > 0) {
      adminNotification[keys[i]] = adminNotification[keys[i]].slice(0, limit);
      personalNotification[keys[i]] = personalNotification[keys[i]].slice(0, limit);
    }
  }

  return {
    admin: adminNotification,
    adminCount,
    personal: personalNotification,
    personalCount,
  };
};

export const filterNCleanNotification = (
  notifications: Notification[],
  timeStr: string,
  t: Translate,
  limit = -1
): NotificationType => {
  const allNotification = notifications
    .map((n) => getNotificationDescription(n, t))
    .filter((item) => item.value !== null && !item.action);

  const personalNotification: ReadType = {
    notRead: [],
    read: [],
  };
  const adminNotification: ReadType = {
    notRead: [],
    read: [],
  };
  const readTime = new Date(timeStr).getTime();
  for (let i = 0; i < allNotification.length; i++) {
    let key = 'read';
    if (readTime < allNotification[i].created) {
      key = 'notRead';
    }

    if (allNotification[i].isAdmin) {
      adminNotification[key].push(allNotification[i]);
      // if (key === "notRead") {
      //     adminNotification[key] = aggregateNotification(
      //         allNotification[i].type,
      //         adminNotification[key],
      //     );
      // }
    } else {
      personalNotification[key].push(allNotification[i]);
      if (key === 'notRead') {
        personalNotification[key] = aggregateNotification(allNotification[i].type, personalNotification[key]);
      }
    }
  }

  const keys = ['notRead', 'read'];
  let adminCount = 0,
    personalCount = 0;

  for (let i = 0; i < keys.length; i++) {
    adminNotification[keys[i]].sort((a, b) => b.created - a.created);
    personalNotification[keys[i]].sort((a, b) => b.created - a.created);
    if (keys[i] === 'notRead') {
      adminCount += adminNotification[keys[i]].length;
      personalCount += personalNotification[keys[i]].length;
    }
    if (limit > 0) {
      adminNotification[keys[i]] = adminNotification[keys[i]].slice(
        0,
        keys[i] === 'read' && adminNotification['notRead'].length > 0 ? parseInt(limit / 2 + '') : limit
      );
      personalNotification[keys[i]] = personalNotification[keys[i]].slice(
        0,
        keys[i] === 'read' && personalNotification['notRead'].length > 0 ? parseInt(limit / 2 + '') : limit
      );
    }
  }

  return {
    admin: adminNotification,
    adminCount,
    personalCount,
    personal: personalNotification,
  };
};
