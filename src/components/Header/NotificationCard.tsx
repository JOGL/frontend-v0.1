import { FC } from 'react';
import tw from 'twin.macro';
// import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import { FilterNotification } from './NotificationUtils';
import { displayObjectRelativeDate } from '~/src/utils/utils';
import { useRouter } from 'next/router';
// import A from '../primitives/A';

interface NotificationCardProps {
  isRead?: boolean;
  fromPopup?: boolean;
  notification?: FilterNotification;
}

const NotificationCard: FC<NotificationCardProps> = ({ isRead = false, fromPopup = false, notification }) => {
  const { locale } = useRouter();

  return (
    <div
      css={
        fromPopup && isRead
          ? tw`py-1`
          : !isRead && fromPopup
          ? tw`py-1 bg-[#F1F2F5]`
          : !isRead && !fromPopup
          ? tw`py-3 my-2 bg-[#D5D9DE] rounded-lg`
          : tw`py-3 my-2 bg-[#F1F2F5] rounded-lg`
      }
      tw="flex items-center justify-between gap-2 px-4"
    >
      <div tw="flex items-center gap-2 flex-1">
        <div tw="flex flex-col flex-1">
          <div tw="gap-x-1 flex-wrap inline-block text-[14px] p-0 mr-4">{notification.value}</div>
          {fromPopup && (
            <span tw="(text-[12px])! color[#03AFF9]">{displayObjectRelativeDate(notification?.created, locale)}</span>
          )}
        </div>
      </div>
      {!fromPopup && (
        <span tw="text-[12px] color[#454545]">{displayObjectRelativeDate(notification?.created, locale)}</span>
      )}
    </div>
  );
};

export default NotificationCard;
