import { create } from 'zustand';

export interface HeaderStateValue {
  containerId: string;
  containerTitle: string;
  containerType: string;
  containerLogo: string;
  canCreateNeed: boolean;
  canCreateJOGLDoc: boolean;
  canCreateCfp: boolean;
  canCreateEvents: boolean;
}

export interface HeaderState extends HeaderStateValue {
  setHeaderState: (headerState: Partial<HeaderStateValue>) => void;
}

export const useHeaderStore = create<HeaderState>()((set) => ({
  containerId: '',
  containerTitle: '',
  containerType: '',
  containerLogo: '',
  canCreateNeed: false,
  canCreateJOGLDoc: false,
  canCreateCfp: false,
  canCreateEvents: false,
  setHeaderState: (headerState: Partial<HeaderStateValue>) =>
    set((s) => ({
      ...s,
      ...headerState,
    })),
}));
