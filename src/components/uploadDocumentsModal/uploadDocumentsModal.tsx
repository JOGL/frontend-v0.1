import { UploadOutlined } from '@ant-design/icons';
import { Button, Modal, Upload } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import type { UploadFile, UploadProps } from 'antd';
import { useState } from 'react';
const MAX_FILES_COUNT = 10;
interface Props {
  onClose: () => void;
  onCreate: (fileList: UploadFile[]) => Promise<void>;
}

const UploadDocumentModal = ({ onClose, onCreate }: Props) => {
  const { t } = useTranslation('common');
  const [fileList, setFileList] = useState<UploadFile[]>([]);
  const [loading, setLoading] = useState(false);

  const props: UploadProps = {
    onRemove: (file) => {
      const index = fileList.indexOf(file);
      const newFileList = fileList.slice();
      newFileList.splice(index, 1);
      setFileList(newFileList);
    },
    beforeUpload: (_, newList) => {
      const updatedList = [...fileList, ...newList].slice(0, 10);
      setFileList(updatedList);

      return false;
    },
    fileList,
  };

  const onConfirm = async () => {
    setLoading(true);
    await onCreate(fileList);
    setLoading(false);
  };

  return (
    <Modal
      open
      title={t('action.createDocuments')}
      okButtonProps={{ disabled: !fileList.length, loading }}
      okText={t('action.create')}
      onOk={onConfirm}
      onCancel={onClose}
    >
      <Upload {...props} maxCount={MAX_FILES_COUNT} accept="image/*,.pdf,.xls,.xlsx,.doc,.docx" multiple>
        <Button icon={<UploadOutlined />} disabled={fileList.length >= MAX_FILES_COUNT}>
          {t('clickToUpload', { maxCount: MAX_FILES_COUNT })}
        </Button>
      </Upload>
    </Modal>
  );
};
export default UploadDocumentModal;
