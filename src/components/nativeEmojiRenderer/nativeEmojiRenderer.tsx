import { FC } from 'react';
import { SpanStyled } from './nativeEmojiRenderer.styles';

interface Props {
  unified: string;
  size?: number;
}

export const NativeEmojiRenderer: FC<Props> = ({ unified, size = 14 }) => {
  const emoji = String.fromCodePoint(...unified.split('-')?.map((u) => parseInt(u, 16))) + '\uFE0F';
  return <SpanStyled size={size}>{emoji}</SpanStyled>;
};
