import styled from '@emotion/styled';

export const SpanStyled = styled.span<{ size: number }>`
  font-size: ${(props) => `${props.size}px`};
  font-family: 'Segoe UI Emoji', 'Segoe UI Symbol', 'Apple Color Emoji', 'Twemoji Mozilla', 'Noto Color Emoji',
    'Android Emoji';
`;
