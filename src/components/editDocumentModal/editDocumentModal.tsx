import { Form, Input, Modal } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { DocumentOrFolderModel } from '~/__generated__/types';

const CREATE_DOCUMENT_FORM = 'create-document-form';

export interface EditDocumentFormFields {
  title: string;
  description: string;
}

interface Props {
  document: DocumentOrFolderModel;
  onClose: () => void;
  onConfirm: (data: EditDocumentFormFields) => void;
}

const EditDocumentModal = ({ document, onClose, onConfirm }: Props) => {
  const { t } = useTranslation('common');
  const [form] = Form.useForm<EditDocumentFormFields>();

  const requiredRule = {
    required: true,
    message: t('error.requiredField'),
  };

  return (
    <Modal
      open
      title={t('action.editDocument')}
      okButtonProps={{ htmlType: 'submit', form: CREATE_DOCUMENT_FORM }}
      okText={t('action.save')}
      onCancel={onClose}
    >
      <Form
        form={form}
        layout="vertical"
        id={CREATE_DOCUMENT_FORM}
        onFinish={(data) => onConfirm(data)}
        initialValues={{ title: document?.title ?? '', description: document?.description ?? '' }}
      >
        <Form.Item name="title" label={t('title')} required rules={[requiredRule]}>
          <Input />
        </Form.Item>
        <Form.Item name="description" label={t('shortDescription')}>
          <Input.TextArea placeholder={t('explainToMembersWhyThisDocumentMatters')} rows={4} />
        </Form.Item>
      </Form>
    </Modal>
  );
};
export default EditDocumentModal;
