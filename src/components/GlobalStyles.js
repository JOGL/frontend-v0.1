import { Global, css } from '@emotion/react';
import tw, { theme, GlobalStyles as BaseStyles } from 'twin.macro';

const customStyles = css`
  html,
  body {
    scroll-behavior: smooth;
  }
  body {
    overflow-x: hidden;
    h1 {
      ${tw`text-4xl`};
      color: black;
    }
    h2 {
      ${tw`text-3xl`};
    }
    h3 {
      font-size: 1.75rem;
    }
    h4 {
      font-size: 1.325rem;
    }
    h5 {
      ${tw`text-lg`};
    }
    h6 {
      ${tw`text-base`};
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      margin-bottom: 0.5rem;
      font-weight: 500;
      line-height: 1.2;
    }
    p {
      margin-bottom: 1rem;
    }
    ul {
      ${tw`list-disc`};
      padding-inline-start: 20px;
      margin-bottom: 20px;
    }

    hr {
      margin: 1rem 0;
    }
    /* img {
      max-width: inherit;
    } */
    input,
    /* select, */
    textarea {
      width: 100%;
      &::placeholder {
        color: #bbbbbb;
      }
    }
    .invalid-feedback {
      ${tw`text-danger`}
    }
    .styledInputTextArea {
      ${tw`flex-1 min-w-0 block w-full px-3 py-[0.4rem] rounded-md border-gray-300 mb-4 focus:(ring-primary border-primary)`}
    }
    .styledInputAuth {
      ${tw`flex-1 min-w-0 block w-full px-3 py-[0.4rem] rounded-md border-gray-300 focus:(ring-primary border-primary)`}
    }
  }
`;

const GlobalStyles = () => (
  <>
    <BaseStyles />
    <Global styles={customStyles} />
  </>
);

export default GlobalStyles;
