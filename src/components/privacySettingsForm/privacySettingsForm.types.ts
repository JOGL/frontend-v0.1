import { JoiningRestrictionLevel, PrivacyLevel } from '~/__generated__/types';

export interface FormData {
  listing_privacy: PrivacyLevel;
  content_privacy: PrivacyLevel;
  joining_restriction: JoiningRestrictionLevel;
  workspace_created_by_any_member: boolean;
  channels_created_by_any_member: boolean;
  mention_everyone_by_any_member: boolean;
  events_created_by_any_member: boolean;
  documents_created_by_any_member: boolean;
  needs_created_by_any_member: boolean;
  library_managed_by_any_member: boolean;
}

export interface UpdateData {
  listing_privacy: PrivacyLevel;
  content_privacy: PrivacyLevel;
  joining_restriction: JoiningRestrictionLevel;
  management: string[];
}
