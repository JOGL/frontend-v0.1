import { Button, Flex, Form, Select, Typography } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { WrapperStyled } from './privacySettingsForm.styles';
import { FormData, UpdateData } from './privacySettingsForm.types';
import { ReactNode, useState } from 'react';

const MANAGEMENT_PROPERTIES = [
  'workspace_created_by_any_member',
  'channels_created_by_any_member',
  'events_created_by_any_member',
  'documents_created_by_any_member',
  'needs_created_by_any_member',
  'library_managed_by_any_member',
];

interface Props {
  onUpdate: (data: UpdateData) => void;
  initialValues: UpdateData;
  showWorkspaceCreationPermission?: boolean;
  privacyFormItems: ReactNode;
}

export const PrivacySettingsForm = ({
  onUpdate,
  initialValues,
  showWorkspaceCreationPermission = true,
  privacyFormItems,
}: Props) => {
  const { t } = useTranslation('common');
  const [isSubmitDisabled, setIsSubmitDisabled] = useState(true);
  const [form] = Form.useForm();
  const { listing_privacy, content_privacy, joining_restriction, management } = initialValues;

  const membersPermissionOptions = [
    { label: t('privacySettingsMembersOptions.allMembers'), value: true },
    { label: t('privacySettingsMembersOptions.adminsOnly'), value: false },
  ];

  const initialManagementOptions = MANAGEMENT_PROPERTIES.reduce((acc, option) => {
    if (management.includes(option)) {
      acc[option] = true;
    } else {
      acc[option] = false;
    }
    return acc;
  }, {});

  const handleSubmit = (data: FormData) => {
    const {
      workspace_created_by_any_member,
      channels_created_by_any_member,
      events_created_by_any_member,
      documents_created_by_any_member,
      needs_created_by_any_member,
      library_managed_by_any_member,
      ...rest
    } = data;

    const managementValues = {
      workspace_created_by_any_member,
      channels_created_by_any_member,
      events_created_by_any_member,
      documents_created_by_any_member,
      needs_created_by_any_member,
      library_managed_by_any_member,
    };

    const management = Object.keys(managementValues).filter((key) => managementValues[key]);
    onUpdate({ management, ...rest });
  };

  return (
    <WrapperStyled>
      <Form
        form={form}
        layout="vertical"
        onFinish={handleSubmit}
        onFieldsChange={() => {
          setIsSubmitDisabled(false);
        }}
        initialValues={{
          listing_privacy,
          content_privacy,
          joining_restriction,
          ...initialManagementOptions,
        }}
      >
        <Flex vertical gap="large">
          <div>
            <Typography.Title level={3}>{t('privacy')}</Typography.Title>
            {privacyFormItems}
          </div>
          <div>
            <Typography.Title level={3}>{t('membersPermissions')}</Typography.Title>
            {showWorkspaceCreationPermission && (
              <Form.Item name="workspace_created_by_any_member" label={t('privacySettingMembersLabel.workspace')}>
                <Select options={membersPermissionOptions} />
              </Form.Item>
            )}
            <Form.Item name="channels_created_by_any_member" label={t('privacySettingMembersLabel.discussions')}>
              <Select options={membersPermissionOptions} />
            </Form.Item>
            <Form.Item name="events_created_by_any_member" label={t('privacySettingMembersLabel.events')}>
              <Select options={membersPermissionOptions} />
            </Form.Item>
            <Form.Item name="documents_created_by_any_member" label={t('privacySettingMembersLabel.documents')}>
              <Select options={membersPermissionOptions} />
            </Form.Item>
            <Form.Item name="needs_created_by_any_member" label={t('privacySettingMembersLabel.needs')}>
              <Select options={membersPermissionOptions} />
            </Form.Item>
            <Form.Item name="library_managed_by_any_member" label={t('privacySettingMembersLabel.papers')}>
              <Select options={membersPermissionOptions} />
            </Form.Item>
            <Flex align="center" justify="end" gap="small">
              <Button onClick={() => form.resetFields()}>{t('action.cancel')}</Button>
              <Button type="primary" htmlType="submit" disabled={isSubmitDisabled}>
                {t('action.update')}
              </Button>
            </Flex>
          </div>
        </Flex>
      </Form>
    </WrapperStyled>
  );
};
