import { MessageOutlined, MoreOutlined } from '@ant-design/icons';
import { Badge, Button, Dropdown, MenuProps, Space, theme, Tooltip } from 'antd';
import dayjs from 'dayjs';
import useTranslation from 'next-translate/useTranslation';
import { FeedStatModel, PaperModel, Permission, UserMiniModel } from '~/__generated__/types';
import { AlignType } from 'rc-table/lib/interface';
import { TableStyled } from '../../Common/table.styles';
import Loader from '../../Common/loader/loader';
import usePapers from '../usePapers';
import { FeedEntityLink } from '../../Common/links/feedEntityLink/feedEntityLink';
import { UserLink } from '../../Common/links/userLink/userLink';
import { ImagePasteRule } from '../../tiptap/customExtensions/imageExtensions/imagePasteRule';
import { stripHtml } from 'string-strip-html';
import { useFeedEntity } from '../../Common/feedEntity/feedEntity.hooks';

interface Props {
  papers: PaperModel[];
  loading: boolean;
  hideColumns: string[];
  canManage: boolean;
  onDelete: (item: PaperModel) => void;
}

const PaperTable = ({ papers, loading = false, hideColumns = [], canManage, onDelete }: Props) => {
  const { t } = useTranslation('common');
  const { token } = theme.useToken();
  const [goToPaper] = usePapers();
  const [markFeedAsOpened] = useFeedEntity();

  const columns = [
    {
      dataIndex: 'title',
      title: t('title'),
      render: (title: string) => {
        const stripHtmlTitle = stripHtml(title).result;
        return (
          <Tooltip title={stripHtmlTitle} placement="topLeft">
            <Space>{stripHtmlTitle}</Space>
          </Tooltip>
        );
      },
    },
    {
      dataIndex: 'authors',
      title: t('authors'),
      width: 120,
      ellipsis: true,
      render: (authors: string) => {
        return <>{authors}</>;
      },
    },
    {
      dataIndex: 'publication_date',
      title: t('year'),
      width: 80,
      ellipsis: true,
      render: (publication_date: string) => {
        return publication_date ? dayjs(publication_date).format('YYYY') : '';
      },
    },
    {
      dataIndex: 'journal',
      title: t('journal'),
      width: 120,
      ellipsis: true,
      render: (journal: string) => {
        return <>{journal}</>;
      },
    },
    {
      dataIndex: 'opened',
      title: t('opened'),
      width: 120,
      render: (updated: string) => {
        return updated ? dayjs(updated).format('DD.MM.YYYY') : '';
      },
    },
    {
      dataIndex: 'feed_entity',
      title: t('space.one'),
      ellipsis: true,
      width: 150,
      render: (feed_entity: PaperModel['feed_entity']) => {
        return <FeedEntityLink entity={feed_entity} />;
      },
    },
    {
      dataIndex: 'created_by',
      title: t('added_by'),
      width: 120,
      render: (created_by: UserMiniModel) => {
        return <UserLink user={created_by} />;
      },
    },
    {
      dataIndex: 'feed_stats',
      align: 'left' as AlignType,
      width: 80,
      render: (feed_stats: FeedStatModel, item: PaperModel) => {
        return (
          <Button onClick={() => goToPaper(item.id, 'discussion')} type="text">
            <Badge count={feed_stats.new_post_count} size="small" offset={[4, -4]} color={token.colorPrimary}>
              <Space>
                <MessageOutlined />
                {feed_stats.post_count}
              </Space>
            </Badge>
          </Button>
        );
      },
    },
    {
      dataIndex: 'id',
      align: 'right' as AlignType,
      width: 80,
      render: (id: string, item: PaperModel) => {
        if (!canManage) return null;
        const menuItems: MenuProps['items'] = [
          ...(item.permissions?.includes(Permission.Manage)
            ? [
                {
                  label: t('action.edit'),
                  key: 'edit',
                  onClick: (e) => {
                    e.domEvent.stopPropagation();
                    goToPaper(item.id);
                  },
                },
              ]
            : []),

          ...(item.permissions?.includes(Permission.Delete)
            ? [
                {
                  label: t('action.delete'),
                  key: 'delete',
                  danger: true,
                  onClick: (e) => {
                    e.domEvent.stopPropagation();
                    onDelete(item);
                  },
                },
              ]
            : []),
        ];

        if (menuItems.length === 0) return <></>;

        return (
          <Dropdown
            trigger={['click']}
            placement="bottomRight"
            menu={{
              onClick: ({ domEvent }) => {
                domEvent.stopPropagation();
              },
              items: menuItems,
            }}
          >
            <Button
              type="text"
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
              }}
            >
              <MoreOutlined />
            </Button>
          </Dropdown>
        );
      },
    },
  ];

  if (loading === true) return <Loader />;

  return (
    <TableStyled
      rowClassName={(record: PaperModel) => (record.is_new ? 'new' : '')}
      dataSource={papers}
      columns={columns.filter((col) => !hideColumns.find((hideCol) => col.dataIndex === hideCol))}
      pagination={false}
      onRow={(record: PaperModel) => ({
        onClick: () => {
          goToPaper(record.id);
          markFeedAsOpened(record);
        },
      })}
    />
  );
};

export default PaperTable;
