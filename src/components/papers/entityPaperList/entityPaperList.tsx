import { Flex, Typography, Button, Input, message, Grid } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useState } from 'react';
import PaperTable from '../paperTable/paperTable';
import useDebounce from '~/src/hooks/useDebounce';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import { Permission, SortKey } from '~/__generated__/types';
import CreatePaper from '../../Publications/createPaper/createPaperModal';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import MobileCardView from '../../Common/mobileCardView/mobileCardView';
import usePapers from '../usePapers';
import { useFeedEntity } from '../../Common/feedEntity/feedEntity.hooks';
const { useBreakpoint } = Grid;

interface Props {
  entityId: string;
  canManagePapers: boolean;
}
const EntityPaperList = ({ entityId, canManagePapers }: Props) => {
  const [search, setSearch] = useState('');
  const queryClient = useQueryClient();
  const debouncedSearch = useDebounce(search, 300);
  const { t } = useTranslation('common');
  const [showCreateModal, setShowCreateModal] = useState(false);
  const screens = useBreakpoint();
  const isMobile = screens.xs;
  const [goToPaper] = usePapers();
  const [markFeedAsOpened] = useFeedEntity();

  const { selectedHub } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
  }));

  const selectedSpace = selectedHub?.entities.find((item) => item.id === entityId);

  const { data, isLoading, refetch } = useQuery({
    queryKey: [QUERY_KEYS.entityPaperList, entityId, debouncedSearch],
    queryFn: async () => {
      const response = await api.papers.papersDetail(entityId, {
        Search: debouncedSearch,
        SortKey: SortKey.Createddate,
        SortAscending: false,
      });
      return response.data;
    },
  });
  const deletePaper = useMutation({
    mutationFn: async (id: string) => {
      await api.papers.papersDeleteDetail(id);
    },
    onSuccess: () => {
      message.success(t('paperDeleteSuccess'));
      refetch();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const canManage = selectedSpace?.user_access.permissions.includes(Permission.Managelibrary) ? true : false;
  return (
    <>
      <Flex vertical gap="middle">
        <Typography.Title level={3}>{t('paper.other')}</Typography.Title>
        <Flex justify="stretch" gap="large">
          <Input.Search
            placeholder={t('action.searchForPapers')}
            onChange={(e) => {
              setSearch(e.target.value);
            }}
            onSearch={(value) => {
              setSearch(value);
            }}
          />
          {canManagePapers && <Button onClick={() => setShowCreateModal(true)}>{t('action.add')}</Button>}
          {showCreateModal && (
            <CreatePaper
              selectedSpace={selectedSpace}
              refetch={() => {
                queryClient.invalidateQueries({
                  queryKey: [QUERY_KEYS.entityPaperList],
                });
              }}
              handleCancel={() => setShowCreateModal(false)}
            />
          )}
        </Flex>
        {isMobile ? (
          <MobileCardView
            data={data ?? []}
            loading={isLoading}
            onClickRow={(paper) => {
              goToPaper(paper.id);
              markFeedAsOpened(paper);
            }}
          />
        ) : (
          <PaperTable
            canManage={canManage}
            hideColumns={canManage ? ['feed_entity', 'opened'] : ['feed_entity', 'opened', 'id']}
            loading={isLoading}
            papers={data ?? []}
            onDelete={(need) => {
              deletePaper.mutate(need.id);
            }}
          />
        )}
      </Flex>
    </>
  );
};

export default EntityPaperList;
