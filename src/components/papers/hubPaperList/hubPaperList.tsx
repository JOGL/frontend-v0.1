import { useInfiniteQuery, useMutation, useQuery } from '@tanstack/react-query';
import { Flex, Input, Typography, message, Grid } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useMemo, useState } from 'react';
import { FeedEntityFilter, PaperModel, Permission, SortKey } from '~/__generated__/types';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import useDebounce from '~/src/hooks/useDebounce';
import { ContainerFilter } from '../../Common/filters/containerFilter/containerFilter';
import PaperTable from '../paperTable/paperTable';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import MobileCardView from '../../Common/mobileCardView/mobileCardView';
import usePapers from '../usePapers';
const { useBreakpoint } = Grid;
const PAGE_SIZE = 50;

interface Props {
  hubId: string;
  title: string;
  filter?: FeedEntityFilter;
  sortKey?: SortKey;
}

const HubPaperList = ({ hubId, title, filter, sortKey = SortKey.Updateddate }: Props) => {
  const { t } = useTranslation('common');
  const [search, setSearch] = useState('');
  const debouncedSearch = useDebounce(search, 300);
  const [selectedContainers, setSelectedContainers] = useState<string[]>([]);
  const screens = useBreakpoint();
  const isMobile = screens.xs;
  const [goToPaper] = usePapers();

  const { selectedHub } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
  }));

  const { data: paperData, isLoading, refetch } = useInfiniteQuery({
    queryKey: [QUERY_KEYS.papersAggregate, hubId, debouncedSearch, selectedContainers, filter, sortKey],
    queryFn: async ({ pageParam = 1 }) => {
      const response = await api.nodes.papersAggregateDetail(hubId, {
        Search: debouncedSearch,
        communityEntityIds: selectedContainers.join(',') ?? undefined,
        filter: filter,
        SortKey: sortKey,
        SortAscending: undefined,
        Page: pageParam,
        PageSize: PAGE_SIZE,
      });
      return response.data;
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItemsCount = allPages.reduce((acc, page) => acc + page.items.length, 0);
      return loadedItemsCount < lastPage.total ? allPages.length + 1 : undefined;
    },
  });

  const { data: containers } = useQuery({
    queryKey: [QUERY_KEYS.papersAggregateContainers, hubId],
    queryFn: async () => {
      const response = await api.nodes.papersAggregateCommunityEntitiesDetail(hubId, {});
      return response.data;
    },
  });

  const deletePaper = useMutation({
    mutationFn: async (id: string) => {
      await api.papers.papersDeleteDetail(id);
    },
    onSuccess: () => {
      message.success(t('paperDeleteSuccess'));
      refetch();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const flattenedData: PaperModel[] = useMemo(() => {
    return paperData?.pages.flatMap((page) => page.items) ?? [];
  }, [paperData]);

  const canManage = selectedHub?.user_access.permissions.includes(Permission.Managelibrary) ? true : false;
  return (
    <>
      <Flex vertical gap="middle">
        <Typography.Title>{title}</Typography.Title>
        <Flex justify="stretch">
          <Input.Search
            placeholder={t('action.searchForPapers')}
            onChange={(e) => {
              setSearch(e.target.value);
            }}
            onSearch={(value) => {
              setSearch(value);
            }}
          />
        </Flex>
        <Flex justify="stretch">
          {containers && (
            <ContainerFilter
              containers={containers}
              onChange={(communityEntityIds) => setSelectedContainers(communityEntityIds)}
            />
          )}
        </Flex>
        {isMobile ? (
          <MobileCardView
            data={flattenedData}
            loading={isLoading}
            onClickRow={(paper) => {
              goToPaper(paper.id);
            }}
          />
        ) : (
          <PaperTable
            canManage={canManage}
            papers={flattenedData}
            loading={isLoading}
            hideColumns={canManage ? ['opened'] : ['opened', 'id']}
            onDelete={(paper) => {
              deletePaper.mutate(paper.id);
            }}
          />
        )}
      </Flex>
    </>
  );
};

export default HubPaperList;
