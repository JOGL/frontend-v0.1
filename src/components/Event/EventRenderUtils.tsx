import useTranslation from 'next-translate/useTranslation';
import Button from '../primitives/Button';
import Link from 'next/link';
import Icon from '../primitives/Icon';
import { KnownUrl } from '~/src/types/knownUrl';
import { useEffect, useRef, useState } from 'react';
import { Hub, Organization } from '~/src/types';
import A from '../primitives/A';
import { entityItem } from '~/src/utils/utils';
import useGet from '~/src/hooks/useGet';
import SpinLoader from '../Tools/SpinLoader';
import tw from 'twin.macro';
import { Event, EventAttendance } from '~/src/types/event';
import { EntityMini } from '~/src/types/entity';
import { WorkspaceModel } from '~/__generated__/types';

//### DISPLAY EVENT INFO ###
interface DisplayEventInfoProps {
  icon: string;
  info: string;
  infoLink: string;
  knownUrl?: KnownUrl;
  handleClear: () => void;
}

export const DisplayEventInfo = ({ icon, info, infoLink, knownUrl, handleClear }: DisplayEventInfoProps) => {
  return (
    <Button tw="flex w-full gap-1 h-9 border-gray-300 items-center md:w-[500px] bg-white hocus:(bg-gray-100)">
      {knownUrl ? (
        <img
          src={knownUrl?.icon}
          width="24px"
          height="24px"
          tw="min-width[24px] min-height[24px] mr-1"
          alt={knownUrl?.name}
        />
      ) : (
        <Icon icon={icon} tw="w-6 h-6 min-width[24px] min-height[24px] text-[#454545]" />
      )}
      <Link href={infoLink} target="_blank" tw="line-clamp-1 text-start text-[#454545] break-all flex-auto">
        <span title={info}>{info}</span>
      </Link>
      <Icon
        icon="ic:baseline-close"
        tw="w-4 h-4 min-width[16px] min-height[16px] text-[#454545]"
        onClick={(e) => {
          e.preventDefault();
          handleClear();
        }}
      />
    </Button>
  );
};

//### ADD EVENT MEETING LINK ###
interface AddEventMeetingLinkProps {
  existingLink?: string;
  handleChange: (link: string) => void;
}

export const AddEventMeetingLink = ({ existingLink, handleChange }: AddEventMeetingLinkProps) => {
  const [meetingLink, setMeetingLink] = useState(existingLink ?? '');
  const { t } = useTranslation('common');

  return (
    <div tw="flex flex-wrap items-center gap-2 md:w-[500px] lg:flex-nowrap">
      <div tw="flex gap-2 items-center flex-auto">
        <input
          name="meeting-link"
          id="meeting-link"
          tw="text-black border p-2 h-9 rounded-md border-gray-300 focus:(ring-primary border-primary)"
          placeholder={t('addMeetingLink')}
          value={meetingLink}
          onChange={(e) => {
            const link = e?.target?.value;
            setMeetingLink(link);
            handleChange(link);
          }}
          autoComplete="off"
        />
      </div>
    </div>
  );
};

//### ADD EVENT PHYSICAL LOCATION ###
interface AddEventPhysicalLocationProps {
  existingLocation?: string;
  handleChange: (location: google.maps.places.PlaceResult) => void;
}

export const AddEventPhysicalLocation = ({ existingLocation, handleChange }: AddEventPhysicalLocationProps) => {
  const { t } = useTranslation('common');

  return (
    <div tw="flex flex-wrap items-center gap-2 md:w-[500px]">
      <div tw="flex gap-2 items-center flex-auto">
        <Icon tw="flex-none w-5 h-5 m-0 text-[#858585]" icon="mdi:location" />

        <EventLocationSearch
          existingLocation={existingLocation}
          placeholder={t('searchAddress')}
          onLocationSelect={(place) => handleChange(place)}
        />
      </div>
    </div>
  );
};

interface LocationSearchProps {
  placeholder?: string;
  onLocationSelect: (place: google.maps.places.PlaceResult | null) => void;
  existingLocation?: string;
}

const EventLocationSearch: React.FC<LocationSearchProps> = ({ placeholder, onLocationSelect, existingLocation }) => {
  const inputRef = useRef<HTMLInputElement | null>(null);
  const [predictions, setPredictions] = useState<google.maps.places.AutocompletePrediction[]>([]);
  const autocompleteService = useRef<google.maps.places.AutocompleteService | null>(null);

  useEffect(() => {
    if (inputRef.current) {
      autocompleteService.current = new google.maps.places.AutocompleteService();

      const handleInputChange = async () => {
        const query = inputRef.current?.value;

        if (query.trim() === '') {
          setPredictions([]);
          return;
        }

        try {
          const newPredictions = await getPlacePredictions(query);
          setPredictions(newPredictions);
        } catch (error) {
          console.error('Error fetching predictions:', error);
        }
      };

      const handleOutsideClick = (event: MouseEvent) => {
        if (inputRef.current && !inputRef.current.contains(event.target as Node)) {
          setPredictions([]);
        }
      };

      inputRef.current.addEventListener('input', handleInputChange);
      document.addEventListener('click', handleOutsideClick);

      return () => {
        if (inputRef.current) {
          inputRef.current.removeEventListener('input', handleInputChange);
        }
        document.removeEventListener('click', handleOutsideClick);
      };
    }
  }, []);

  useEffect(() => {
    // Set initial location when provided
    if (inputRef.current && existingLocation) {
      inputRef.current.value = existingLocation;
    }
  }, [existingLocation]);

  const getPlacePredictions = async (query: string) => {
    return new Promise<google.maps.places.AutocompletePrediction[]>((resolve, reject) => {
      if (autocompleteService.current) {
        autocompleteService.current.getPlacePredictions({ input: query }, (predictions, status) => {
          if (status === google.maps.places.PlacesServiceStatus.OK) {
            resolve(predictions || []);
          } else {
            reject(status);
          }
        });
      } else {
        reject('Autocomplete service not initialized');
      }
    });
  };

  const getPlaceDetails = async (placeId: string): Promise<google.maps.places.PlaceResult | null> => {
    return new Promise<google.maps.places.PlaceResult | null>((resolve, reject) => {
      const placesService = new google.maps.places.PlacesService(document.createElement('div'));
      placesService.getDetails({ placeId }, (place, status) => {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          resolve(place);
        } else {
          reject(status);
        }
      });
    });
  };

  const handlePredictionSelect = async (prediction: google.maps.places.AutocompletePrediction) => {
    const place = await getPlaceDetails(prediction?.place_id);

    if (inputRef.current) {
      inputRef.current.value = place?.name || '';
    }

    setPredictions([]);
    onLocationSelect(place);
  };

  return (
    <div tw="flex flex-auto relative text-black">
      <input
        type="text"
        id="location-input"
        placeholder={placeholder ?? 'Enter a location'}
        ref={inputRef}
        tw="border p-2 h-9 rounded-md border-gray-300 focus:(ring-primary border-primary)"
        autoComplete="off"
      />

      {/* Display predictions only if the user has started searching */}
      {predictions.length > 0 && inputRef.current?.value.trim() !== '' && (
        <ul tw="absolute top-full left-0 z-10 list-none p-1 m-0 bg-white shadow rounded-md w-full">
          {predictions.map((prediction) => (
            <li
              key={prediction.place_id}
              onClick={() => handlePredictionSelect(prediction)}
              tw="py-1 px-2 cursor-pointer hover:bg-blue-100"
            >
              {prediction.description}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

//### RENDER HOST INFO
interface RenderHostInfoProps {
  host: Organization | WorkspaceModel | Hub;
}

export const RenderHostInfo = ({ host }: RenderHostInfoProps) => {
  const { t } = useTranslation('common');

  return (
    <div tw="flex flex-col gap-4">
      <div tw="flex items-center space-x-3 w-full">
        <div tw="h-[50px] min-h-[50px] w-[50px] min-w-[50px]">
          <div
            style={{
              backgroundImage: `url(${host?.banner_url_sm ?? entityItem[host?.type]?.default_banner})`,
            }}
            tw="bg-cover bg-center h-[50px] w-[50px] rounded-full border border-solid border-[#ced4da]"
          />
        </div>

        <div tw="flex flex-col">
          <Link href={`/${entityItem[host?.type]?.front_path}/${host.id}`}>
            <span tw="text-lg">{host?.title}</span>
          </Link>

          <span tw="text-xs self-start">{t(entityItem[host?.type]?.translationId)}</span>
        </div>
      </div>
    </div>
  );
};

//### RENDER SPEAKERS
interface RenderSpeakerProps {
  invite: EventAttendance;
}

export const RenderSpeaker = ({ invite }: RenderSpeakerProps) => {
  //Do this to deal with situations where the invite is still not created (it's in the attendances_to_upsert) and thus the full user info is not available yet.
  let user_name = invite?.user?.first_name
    ? `${invite?.user?.first_name} ${invite?.user?.last_name}`
    : ((invite?.user as unknown) as EntityMini)?.full_name;

  return (
    <A href={`/user/${invite?.user?.id}`} noStyle>
      <div tw="flex flex-row items-center gap-2 m-1 background[white] shadow-custom cursor-pointer rounded-lg px-3 py-2 bg-white sm:w-48">
        <div
          style={{
            backgroundImage: `url(${invite?.user?.logo_url_sm ?? '/images/default/default-user.png'})`,
          }}
          tw="bg-cover bg-center h-9 w-9 rounded-full border border-solid border-[#ced4da] flex-none"
        />
        <div tw="flex-auto">
          <span tw="line-clamp-1 font-semibold hover:underline break-all" title={user_name}>
            {user_name}
          </span>
        </div>
      </div>
    </A>
  );
};

//### EVENT BATCH ACTIONS
interface EventInvitesBatchActionsProps {
  handleBatchMessage: () => void;
  handleBatchRemove: () => void;
}

export const EventInvitesBatchActions = ({ handleBatchMessage, handleBatchRemove }: EventInvitesBatchActionsProps) => {
  const { t } = useTranslation('common');

  return (
    <div tw="flex justify-end gap-2 flex-wrap">
      {/* Send message*/}
      <Button onClick={handleBatchMessage}>{t('action.sendMessage')}</Button>

      {/* Remove participants*/}
      <Button onClick={handleBatchRemove} btnType="danger">
        {t('action.remove')}
      </Button>
    </div>
  );
};

//### EVENT INVITEE INFO
interface EventInviteeInfoProps {
  event: Event;
}

export const EventInviteeInfo = ({ event }: EventInviteeInfoProps) => {
  const { t } = useTranslation('common');
  const { data: invitees, isLoading } = useGet<EventAttendance[]>(`events/${event?.id}/attendances?type=user`);

  const renderUserLabels = (invitee: EventAttendance) => {
    return (
      <div tw="flex gap-1 flex-wrap">
        {invitee?.access_level === 'admin' && (
          <span
            key={`${invitee?.id}-organizer}`}
            tw="p-1 text-xs rounded-full border border-solid border-gray-200 self-start"
          >
            {t('organizer.one')}
          </span>
        )}

        {invitee?.labels?.map((label: string) => {
          return (
            <span
              key={`${invitee?.id}-${label}`}
              tw="p-1 text-xs rounded-full border border-solid border-gray-200 self-start"
            >
              {t('speaker.one')}
            </span>
          );
        })}
      </div>
    );
  };

  return isLoading ? (
    <SpinLoader />
  ) : (
    <div tw="flex flex-col gap-4">
      {invitees?.map((invitee: EventAttendance) => (
        <div tw="flex flex-none gap-4" key={`Invitee-${invitee?.user?.id}`}>
          <div tw="h-[50px] min-h-[50px] w-[50px] min-w-[50px]">
            <div
              style={{ backgroundImage: `url(${invitee?.user?.logo_url_sm ?? '/images/default/default-user.png'})` }}
              tw="bg-center bg-cover h-[50px] w-[50px] rounded-full border border-solid border-[#ced4da]"
            />
          </div>

          {/* Member name*/}
          <div tw="flex-auto flex flex-col justify-center">
            <Link href={`/user/${invitee?.user?.id}`}>
              <span tw="text-black font-semibold line-clamp-1 break-all">
                {invitee?.user?.first_name} {invitee?.user?.last_name}
              </span>
            </Link>
            {renderUserLabels(invitee)}
          </div>

          <div
            tw="flex-none inline-block border border-gray-200 rounded-full px-3 py-1 w-[100px] text-[12px] text-center font-semibold h-1/2"
            css={[
              invitee?.status === 'yes'
                ? tw`text-[#1CB52B]`
                : invitee?.status === 'no'
                ? tw`text-[#F60000]`
                : tw`text-[#F2A611]`,
            ]}
          >
            <Icon icon="material-symbols:circle" tw="h-2 w-2" />
            {t(`legacy.eventInviteStatus.${invitee?.status}`)}
          </div>
        </div>
      ))}
    </div>
  );
};
