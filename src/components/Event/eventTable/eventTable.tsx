import React, { FC, useMemo } from 'react';
import { Event } from '~/src/types/event';
import { displayPartsOdDateDate } from '~/src/utils/utils';
import EventListCard from '../EventListCard';
import { EventModel } from '~/__generated__/types';
import Loader from '../../Common/loader/loader';

interface Props {
  events: EventModel[];
  loading: boolean;
  canManage: boolean;
  onClickRow: (document: EventModel) => void;
}

const EventTable: FC<Props> = ({ events, loading, onClickRow }) => {
  // group events by date
  const listEvents: Record<string, Event[]> = useMemo(() => {
    return events?.reduce((grouped, event) => {
      const start = ((event?.start as unknown) as string).split('T')[0];

      if (!grouped[start]) {
        grouped[start] = [];
      }
      grouped[start].push(event);
      return grouped;
    }, {});
  }, [events]);

  if (loading) {
    return <Loader />;
  }

  return (
    <>
      <div tw="mt-4 flex flex-col divide-gray-200 divide-y w-full">
        {/* Loop through all different event dates */}
        {listEvents &&
          Object.entries(listEvents).map((date, i: number) => (
            <div tw="inline-flex items-center gap-5 w-full py-4" key={i}>
              <div tw="flex flex-col text-center w-10">
                <div tw="text-gray-500">{displayPartsOdDateDate(date[0], 'ddd')}</div>
                <div tw="text-gray-500 text-lg font-medium -my-1">{displayPartsOdDateDate(date[0], 'DD')}</div>
                <div tw="text-gray-500">{displayPartsOdDateDate(date[0], 'MMM')}</div>
              </div>
              <div tw="flex flex-col gap-4 w-full">
                {/* Map through all events happening in this date */}
                {date[1]?.map((event: Event) => {
                  return <EventListCard key={event?.id} event={event} onSelectedEvent={onClickRow} />;
                })}
              </div>
            </div>
          ))}
      </div>
    </>
  );
};

export default EventTable;
