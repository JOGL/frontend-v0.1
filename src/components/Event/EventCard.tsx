import Link from 'next/link';
import React, { useRef, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import ObjectCard from '~/src/components/Cards/ObjectCard';
import H2 from '~/src/components/primitives/H2';
import Title from '~/src/components/primitives/Title';
import ShareBtns from '~/src/components/Tools/ShareBtns/ShareBtns';
import { ItemType } from '~/src/types';
import Icon from '~/src/components/primitives/Icon';
import useUser from '~/src/hooks/useUser';
import { Menu, MenuItem, MenuButton, MenuList } from '@reach/menu-button';
import { confAlert, entityItem, formatEventDate } from '~/src/utils/utils';
import { TextChip } from '~/src/types/chip';
import { isKnownUrl } from '~/src/utils/isKnownUrl';
import Button from '../primitives/Button';
import tw from 'twin.macro';
import SpinLoader from '../Tools/SpinLoader';
import EventAttendingDropdown from './EventAttendingDropdown';
import { useApi } from '~/src/contexts/apiContext';
import { TextWithPlural } from '~/src/utils/managePlurals';
import { Event } from '~/src/types/event';

interface Props {
  event: Event;
  sourceType: ItemType;
  sourceId: string;
  width?: string;
  cardFormat?: string;
  recommendedIds?: number[];
  textChips?: TextChip[];
  showParent?: boolean;
  onDelete?: (id: any) => void;
  forceRefresh?: () => void;
}

const EventCard = ({
  event,
  sourceType,
  sourceId,
  width,
  cardFormat,
  recommendedIds = [],
  textChips,
  showParent = false,
  onDelete,
  forceRefresh,
}: Props) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const { user } = useUser();
  const shareBtnRef = useRef(null);
  const knownUrl = isKnownUrl(event?.meeting_url);
  const api = useApi();

  const [updating, setUpdating] = useState<boolean>(false);

  const attendEvent = () => {
    setUpdating(true);
    let originCommunityEntityId = null;

    //If the user is seing the card from listing his own events or the events of the parent container of the event, originCommunityEntityId does not apply
    if (sourceType !== 'user' && sourceId !== event?.feed_entity?.id) {
      originCommunityEntityId = sourceId;
    }

    api
      .post(
        `events/${event?.id}/attend${
          originCommunityEntityId !== null ? `?originCommunityEntityId=${originCommunityEntityId}` : ''
        }`
      )
      .then(() => {
        forceRefresh && forceRefresh();
        confAlert.fire({ icon: 'success', title: t('legacy.eventInviteStatus.yes') });
        setUpdating(false);
      })
      .catch(() => {
        confAlert.fire({ icon: 'error', title: t('error.generic') });
        setUpdating(false);
      });
  };

  const handleInviteStatusChange = (newStatus: string) => {
    setUpdating(true);
    api
      .post(`events/attendances/${event?.user_attendance?.id}/${newStatus === 'yes' ? 'accept' : 'reject'}`, {})
      .then(() => {
        forceRefresh && forceRefresh();
        confAlert.fire({ icon: 'success', title: t(`legacy.eventInviteStatus.${newStatus}`) });
        setUpdating(false);
      })
      .catch(() => {
        confAlert.fire({ icon: 'error', title: t('error.generic') });
        setUpdating(false);
      });
  };

  return (
    <ObjectCard
      key={event?.id}
      imgUrl={event?.banner_url || '/images/default/default-event.png'}
      href={`/event/${event?.id}`}
      width={width}
      textChips={textChips}
      cardFormat={cardFormat}
      tw="overflow-visible"
    >
      {/* Title */}
      <div tw="flex flex-none items-center" css={[cardFormat !== 'inPosts' && tw`h-8`]}>
        <Link href={`/event/${event?.id}`} passHref legacyBehavior>
          <Title>
            {/* Add padding-right if card doesn't have workspace (thus its banner), so title doesn't overlap with position absolute's star icon */}
            <H2
              tw="break-words text-[16px] items-center line-clamp-2 mb-2"
              css={cardFormat !== 'inPosts' ? tw`md:line-clamp-2` : tw`md:line-clamp-1 mb-0`}
              title={event?.title}
            >
              {event?.title}
            </H2>
          </Title>
        </Link>
      </div>

      {event?.start && (
        <>
          <Hr />
          <div tw="flex items-center flex-none py-1">
            <div tw="flex items-center text-sm line-clamp-2">
              <Icon icon="ion:calendar-number-outline" tw="flex-none w-4 h-4" />
              <span tw="text-[#DE3E96]">{formatEventDate(event?.start, event?.timezone)}</span>
            </div>
          </div>
        </>
      )}

      <div tw="flex-none flex flex-col gap-1" css={[cardFormat !== 'inPosts' && tw`h-24`]}>
        {(event?.location || event?.meeting_url || (event?.generate_meet_link && event?.generated_meeting_url)) && (
          <>
            <Hr />
            {/* don't show location if we're also showing the parent of the event, else the 2 of them takes too much space on the card */}
            {event?.location && !showParent && (
              <div tw="text-sm line-clamp-1 break-all">
                <Icon icon="mdi:location" tw="w-4 h-4 text-[#5F5D5D]" />
                <Link
                  href={event?.location?.url}
                  target="_blank"
                  passHref
                  tw="text-[#5F5D5D] font-medium hover:(underline)"
                  title={event?.location?.name}
                >
                  {event?.location?.name}
                </Link>
              </div>
            )}

            {cardFormat !== 'inPosts' && event?.meeting_url && !event?.generate_meet_link && (
              <div tw="flex items-center text-sm">
                <Link
                  href={event?.meeting_url}
                  target="_blank"
                  passHref
                  tw="text-[#5F5D5D] font-medium hover:(underline)"
                  title={event?.meeting_url}
                >
                  <div tw="flex gap-2 p-1 rounded-full bg-[#F8F8F8] border border-solid border-gray-200 items-center text-sm">
                    {knownUrl ? (
                      <img
                        src={knownUrl?.image}
                        width="16px"
                        height="16px"
                        tw="text-[#5F5D5D]"
                        style={{ maxHeight: '16px', maxWidth: '16px' }} //needed for some svg files
                        alt={knownUrl?.name}
                      />
                    ) : (
                      <Icon icon="bx:link" tw="w-4 h-4 text-[#5F5D5D]" />
                    )}
                    <span tw="line-clamp-1 break-all">{event?.meeting_url}</span>
                  </div>
                </Link>
              </div>
            )}

            {/* User entered url takes priority over the generated one */}
            {cardFormat !== 'inPosts' && event?.generate_meet_link && event?.generated_meeting_url && (
              <div tw="flex items-center text-sm">
                <Link
                  href={event?.generated_meeting_url}
                  target="_blank"
                  passHref
                  tw="text-[#5F5D5D] font-medium hover:underline"
                  title={event?.generated_meeting_url}
                >
                  <div tw="flex gap-2 px-[4px] py-[2px] rounded-full bg-[#F8F8F8] border border-solid border-gray-200 items-center text-sm">
                    <img
                      src={'/images/icons/links-googlemeet.png'}
                      width="16px"
                      height="16px"
                      tw="text-[#5F5D5D]"
                      style={{ maxHeight: '16px', maxWidth: '16px' }} //needed for some svg files
                      alt="google-meet-event"
                    />
                    {/* <span tw=" line-clamp-1 break-all"> {event?.generated_meeting_url}</span> */}
                    <span tw="line-clamp-1 break-all">{t('goToMeeting')}</span>
                  </div>
                </Link>
              </div>
            )}
          </>
        )}
        {/* Show parent */}
        {cardFormat !== 'inPosts' && event?.feed_entity && showParent && (
          <>
            <Hr tw="pt-1" />
            <div tw="text-sm line-clamp-2">
              <Icon icon={entityItem[event.feed_entity?.entity_type]?.icon} tw="w-4 h-4" />
              {t(entityItem[event.feed_entity?.entity_type]?.translationId)}:&nbsp;
              <Link
                href={`/${entityItem[event.feed_entity?.entity_type]?.front_path}/${event.feed_entity?.id}`}
                passHref
                tw="text-black font-medium hover:(underline text-black)"
              >
                {event.feed_entity?.title}
              </Link>
            </div>
          </>
        )}
      </div>

      {cardFormat !== 'inPosts' && (
        <div tw="flex flex-col justify-around">
          <Hr tw="pt-2" />
          <div
            tw="flex flex-none justify-between items-center"
            css={[!event?.user_attendance && !user && tw`justify-center`]}
          >
            <div tw="flex gap-2 text-sm items-center">
              <span tw="font-bold">{event?.attendee_count ?? 0}</span>
              <TextWithPlural type="participant" count={event?.attendee_count ?? 0} />
            </div>
            {event?.user_attendance ? (
              <>
                {updating ? (
                  <SpinLoader />
                ) : (
                  <EventAttendingDropdown
                    event={event}
                    disabled={updating || new Date(event?.end).getTime() < Date.now()}
                    isCard={true}
                    handleChange={handleInviteStatusChange}
                  />
                )}
              </>
            ) : user && new Date(event?.end).getTime() > Date.now() ? ( //Only logged users should see this
              <Button
                btnType="primary"
                tw="h-6 flex items-center text-sm font-semibold"
                onClick={attendEvent}
                disabled={updating}
              >
                <>
                  {updating && <SpinLoader />} {t('action.accept')}
                </>
              </Button>
            ) : null}
          </div>
        </div>
      )}

      <div tw="hidden">
        <ShareBtns ref={shareBtnRef} type="event" specialObjId={event?.id} url={`/event/${event?.id}`} />
      </div>
      <div tw="absolute top-2 right-2">
        <Menu>
          <MenuButton tw="rounded-full bg-gray-200 bg-opacity-80 h-6 w-6 p-1 flex justify-center items-center text-[.7rem] text-gray-700">
            •••
          </MenuButton>
          <MenuList
            portal={false}
            className="post-manage-dropdown"
            tw="absolute rounded-lg divide-y divide-gray-300 text-center text-sm"
          >
            {event?.permissions?.includes('manage') && (
              <MenuItem
                onSelect={() =>
                  router.push({
                    pathname: '/event/create',
                    query: {
                      id: event?.id,
                      parentId: event?.feed_entity?.id,
                      parentType: entityItem[event?.feed_entity?.entity_type].back_path,
                    },
                  })
                }
              >
                {t('action.manage')}
              </MenuItem>
            )}
            {event?.permissions?.includes('delete') && onDelete && (
              <MenuItem onSelect={() => onDelete && onDelete(event)}>{t('action.delete')}</MenuItem>
            )}
            <MenuItem onSelect={() => shareBtnRef?.current?.click()}>{t('action.share')}</MenuItem>
          </MenuList>
        </Menu>
      </div>
    </ObjectCard>
  );
};

const Hr = (props) => <div tw="h-[1px] -mx-3 border-0 border-t border-solid border-[rgba(0, 0, 0, 0.07)]" {...props} />;

export default EventCard;
