import { useModal } from '~/src/contexts/modalContext';
import { ContainerPermissions, MenuActions } from '~/src/types';
import { documentAction } from '~/src/utils/getContainerInfo';
import { Event } from '~/src/types/event';
import DocumentTypeSelector from '../Tools/createDocument/documentTypeSelector/documentTypeSelector';
import useTranslation from 'next-translate/useTranslation';

const EventAddActions = (event: Event, permissions: ContainerPermissions, refresh: () => void): MenuActions => {
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();

  const actions = {
    content: [
      {
        ...documentAction(event),
        disabled: !permissions?.canCreateDocuments ?? true,
        handleSelect: () => {
          showModal({
            children: (
              <DocumentTypeSelector
                object={event}
                itemType="events"
                refresh={refresh}
                closeModal={closeModal}
                apiRoute={`events/${event?.id}/documents`}
              />
            ),
            title: t('action.addItem', { item: t('document.one') }),
            modalClassName: '',
          });
        },
      },
    ],
  };

  return actions;
};

export default EventAddActions;
