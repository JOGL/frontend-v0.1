import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import { useMemo } from 'react';
import { FeedEntityFilter, SortKey } from '~/__generated__/types';

const useEvents = (): [
  (id: string, tab?: string) => void,
  (key: string) => string
] => {
  const router = useRouter();
  const { t } = useTranslation('common');

  const goToEvent = (id: string, tab?: string) => {
    if (tab) router.push(`/event/${id}?tab=${tab}`);
    else router.push(`/event/${id}`);
  };

  const getEventViewTitle = (key: string): string => {
    switch (key) {
      case 'invitations':
        return t('invitation.other');
      case 'created':
        return t('createdByYou');
      case 'all':
      case 'new':
        return t('event.new');
      case 'calendar':
      default:
        return t('hubCalendar');
    }
  };

  return [goToEvent, getEventViewTitle];
};

export const getEventViewFilter = (key: string): { sortKey: SortKey; filter?: FeedEntityFilter; allowPast: boolean } => {
  switch (key) {
    case 'invitations':
      return {
        sortKey: SortKey.Invitationdate,
        filter: FeedEntityFilter.Sharedwithuser,
        allowPast: false,
      };
    case 'created':
      return {
        sortKey: SortKey.Updateddate,
        filter: FeedEntityFilter.Createdbyuser,
        allowPast: true,
      };
    case 'all':
    case 'new':
      return { sortKey: SortKey.Updateddate, allowPast: false };
    case 'calendar':
    default:
      return { sortKey: SortKey.Date, allowPast: true };
  }
};

export const useEventDateFilter = (mode: string): [string, string, boolean] => {
  const currentDateString = new Date().toISOString();

  const { from, to, sortAscending } = useMemo(() => {
    return {
      from: mode === 'upcoming' ? currentDateString : '',
      to: mode === 'past' ? currentDateString : '',
      sortAscending: mode === 'upcoming',
    };
  }, [mode]);

  return [from, to, sortAscending];
};

export default useEvents;