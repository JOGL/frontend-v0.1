import React, { useState, FC, useMemo } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import { entityItem } from '~/src/utils/utils';
import { Button, Flex } from 'antd';
import { EventCreationStep } from '~/src/types/event';
import { PickPrivacyStep } from './CreationSteps/PickPrivacyStep';
import { CommunityEntityMiniModel, Permission } from '~/__generated__/types';
import ContainerPicker from '../Common/container/containerPicker/containerPicker';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import { Disabled } from '../Common/disabled/disabled';

interface PropsModal {
  defaultContainer?: CommunityEntityMiniModel;
  defaultCreationStep?: EventCreationStep;
}

export const CreateEventModal: FC<PropsModal> = ({
  defaultContainer,
  defaultCreationStep = EventCreationStep.PickContainer,
}) => {
  const { t } = useTranslation('common');
  const [selectedContainer, setSelectedContainer] = useState(defaultContainer);
  const [selectedPrivacy, setSelectedPrivacy] = useState<string>();
  const [batchInviteMembers, setBatchInviteMembers] = useState<boolean>(false);
  const [currentCreationStep, setCurrentCreationStep] = useState<EventCreationStep>(defaultCreationStep);
  const { push } = useRouter();
  const { selectedHub } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
  }));
  
  const onNext = (e) => {
    selectedContainer &&
      push({
        pathname: '/event/create',
        query: {
          parentId: selectedContainer?.id,
          parentType: entityItem[selectedContainer?.type].back_path,
          privacy: selectedPrivacy,
          batchInviteMembers: batchInviteMembers,
        },
      });
  };

  const handlePrivacySelection = (newChoice: string) => {
    setSelectedPrivacy(newChoice);
  };

  const handleChangeContainerClick = () => {
    setCurrentCreationStep(EventCreationStep.PickContainer);
  };

  const handleBatchInviteMembersClick = (checked: boolean) => {
    setBatchInviteMembers(checked);
  };

  const isButtonDisabled = useMemo(() => {
    if (selectedContainer !== null && selectedPrivacy) {
      return false;
    }
    return true;
  }, [currentCreationStep, selectedContainer, selectedPrivacy]);

  return (
    <Flex vertical gap="middle">
      <ContainerPicker
        permission={Permission.Createevents}
        hubId={selectedHub?.id}
        onChange={(container) => setSelectedContainer(container)}
        value={selectedContainer?.id}
      />

      <Disabled disabled={!selectedContainer}>
        <PickPrivacyStep
          selectedContainer={{
            value: selectedContainer?.id ?? '',
            label: selectedContainer?.title ?? t('space.one'),
            type: selectedContainer?.type ?? '',
            userAccessLevel: selectedContainer?.user_access_level ?? '',
            imageUrl: selectedContainer?.logo_url_sm ?? selectedContainer?.banner_url_sm ?? '',
            content_privacy: selectedContainer?.content_privacy ?? '',
            members_count: selectedContainer?.stats?.members_count ? selectedContainer?.stats?.members_count - 1 : 0,
          }}
          selectedPrivacy={selectedPrivacy ?? ''}
          batchInviteMembers={batchInviteMembers}
          handleChangeContainerClick={handleChangeContainerClick}
          handleBatchInviteMembersClick={handleBatchInviteMembersClick}
          handlePrivacySelection={handlePrivacySelection}
        />
      </Disabled>
      <Flex align="center" justify="center">
        <Button type="primary" disabled={isButtonDisabled} onClick={onNext}>
          {t('action.next')}
        </Button>
      </Flex>
    </Flex>
  );
};
