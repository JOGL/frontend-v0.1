import Link from 'next/link';
import React from 'react';
import useTranslation from 'next-translate/useTranslation';
import H2 from '~/src/components/primitives/H2';
import Title from '~/src/components/primitives/Title';
import Icon from '~/src/components/primitives/Icon';
import { entityItem, formatEventDateInListView } from '~/src/utils/utils';
import { isKnownUrl } from '~/src/utils/isKnownUrl';
import tw from 'twin.macro';
import { useModal } from '~/src/contexts/modalContext';
import { EventInviteeInfo } from './EventRenderUtils';
import { useRouter } from 'next/router';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import { Event, EventDisplayMode } from '~/src/types/event';
import useGet from '~/src/hooks/useGet';
import { User } from '~/src/types';

interface Props {
  event: Event;
  displayMode?: EventDisplayMode;
  onSelectedEvent?: (event: Event) => void;
}

const EventListCard = ({ event, displayMode = 'default', onSelectedEvent }: Props) => {
  const { t } = useTranslation('common');
  const { data: creatorUser } = useGet<User>(`/users/${event?.created_by_user_id}`);
  const router = useRouter();
  const { showModal } = useModal();
  const knownUrl = isKnownUrl(event?.meeting_url);
  const newActivityCount =
    event?.feed_stats?.new_post_count +
    event?.feed_stats?.new_mention_count +
    event?.feed_stats?.new_thread_activity_count;

  const handleEventClick = () => {
    if (displayMode === 'hub_discussions') {
      onSelectedEvent(event);
    } else {
      router.push(`/event/${event?.id}?tab=feed`);
    }
  };

  const handleEventLinkClick = (e) => {
    e.stopPropagation();
  };

  const handleInviteeCountClick = () => {
    showModal({
      children: <EventInviteeInfo event={event} />,
      title: t('invitees'),
      maxWidth: '30rem',
    });
  };

  return (
    <div
      tw="flex shadow-custom rounded-lg h-[86px] cursor-pointer"
      onClick={handleEventClick}
      css={displayMode === 'default' && tw`self-center w-full md:w-3/4`}
    >
      <div tw="flex flex-col flex-auto border-r-2 p-2 justify-between">
        <div tw="flex items-center gap-1">
          {/* Title */}
          <div tw="flex-auto" onClick={(e) => handleEventLinkClick(e)}>
            <Link href={`/event/${event?.id}`} passHref legacyBehavior>
              <Title>
                <H2 tw="break-all text-[14px] items-center line-clamp-2 mb-2" title={event?.title}>
                  {event?.title}
                </H2>
              </Title>
            </Link>
          </div>

          <div
            css={
              event.is_new &&
              tw`flex-none [background:linear-gradient(253.99deg, #F54799 -2.61%, #7A21B5 48.72%, #02B3FF 107.65%),linear-gradient(0deg, #FFFFFF, #FFFFFF)] rounded-full border`
            }
          >
            {event.is_new && (
              <div tw="bg-white rounded-full m-[1px]">
                <span tw="flex text-[10px] px-2 [background:linear-gradient(253.99deg, #F54799 -2.61%, #7A21B5 48.72%, #02B3FF 107.65%),linear-gradient(0deg, #FFFFFF, #FFFFFF)] bg-clip-text! text-transparent">
                  {t('new')}
                </span>
              </div>
            )}
          </div>
        </div>

        <div tw="flex items-center justify-between w-full">
          {event?.start && event?.end && (
            <div tw="flex items-center flex-none w-1/4 justify-center">
              <span
                title={`${formatEventDateInListView(event?.start, event?.timezone)} to ${formatEventDateInListView(
                  event?.end,
                  event?.timezone
                )}`}
                tw="line-clamp-1 text-xs text-[#5F5D5D]"
              >
                {formatEventDateInListView(event?.start, event?.timezone)} to{' '}
                {formatEventDateInListView(event?.end, event?.timezone)}
              </span>
            </div>
          )}

          {displayMode === 'hub_discussions' && event?.feed_entity && (
            <Link
              href={`/${entityItem[event?.feed_entity?.entity_type]?.front_path}/${event?.feed_entity?.id}`}
              passHref
              tw="text-[#5F5D5D] font-medium underline w-1/4 text-center"
              title={event?.feed_entity?.title}
              onClick={(e) => handleEventLinkClick(e)}
            >
              <span tw="line-clamp-1 break-all text-xs">{event?.feed_entity?.title}</span>
            </Link>
          )}

          {creatorUser && (
            <Link
              href={`/user/${creatorUser.id}`}
              passHref
              tw="text-[#5F5D5D] font-medium underline w-1/4 text-center"
              title={`${creatorUser.first_name} ${creatorUser.last_name}`}
              onClick={(e) => handleEventLinkClick(e)}
            >
              <span tw="line-clamp-1 break-all text-xs">{`${creatorUser.first_name} ${creatorUser.last_name}`}</span>
            </Link>
          )}

          <div tw="flex flex-col w-1/4 items-center" onClick={(e) => e.stopPropagation()}>
            {(event?.meeting_url || (event?.generate_meet_link && event?.generated_meeting_url)) && (
              <>
                {event?.meeting_url && !event?.generate_meet_link && (
                  <div tw="flex items-center text-xs">
                    <Link
                      href={event?.meeting_url}
                      target="_blank"
                      passHref
                      tw="text-[#5F5D5D] font-medium underline"
                      title={event?.meeting_url}
                    >
                      <div tw="flex gap-2 p-1 items-center text-xs">
                        {knownUrl ? (
                          <img
                            src={knownUrl?.image}
                            width="16px"
                            height="16px"
                            tw="text-[#5F5D5D]"
                            style={{ maxHeight: '16px', maxWidth: '16px' }} //needed for some svg files
                            alt={knownUrl?.name}
                          />
                        ) : (
                          <Icon icon="bx:link" tw="w-4 h-4 text-[#5F5D5D]" />
                        )}
                      </div>
                    </Link>
                  </div>
                )}

                {/* User entered url takes priority over the generated one */}
                {event?.generate_meet_link && event?.generated_meeting_url && (
                  <div tw="flex items-center text-xs">
                    <Link
                      href={event?.generated_meeting_url}
                      target="_blank"
                      passHref
                      tw="text-[#5F5D5D] font-medium underline"
                      title={event?.generated_meeting_url}
                    >
                      <div tw="flex gap-2 p-1 items-center text-xs">
                        <img
                          src={'/images/icons/links-googlemeet.png'}
                          width="16px"
                          height="16px"
                          tw="text-[#5F5D5D]"
                          style={{ maxHeight: '16px', maxWidth: '16px' }} //needed for some svg files
                          alt="google-meet-event"
                        />
                        <span tw="line-clamp-1 break-all">{t('goToMeeting')}</span>
                      </div>
                    </Link>
                  </div>
                )}
              </>
            )}
          </div>
        </div>
      </div>

      <div
        tw="flex flex-col flex-none w-[60px] items-center text-primary font-semibold text-xs"
        onClick={(e) => e.stopPropagation()}
      >
        <div
          tw="flex gap-2 h-1/2 w-full items-center justify-center hover:bg-[#EBEBEB]"
          onClick={() => handleInviteeCountClick()}
        >
          {event?.invitee_count}
          <Icon icon="fluent:people-checkmark-24-filled" tw="w-4 h-4" />
        </div>

        <hr tw="w-full m-0" />

        <div tw="flex gap-2 h-1/2 w-full items-center justify-center hover:(bg-[#EBEBEB])" onClick={handleEventClick}>
          {newActivityCount > 0 ? (
            <div tw="flex-none w-[15px] h-[15px] [background:linear-gradient(253.99deg, #F54799 -2.61%, #7A21B5 48.72%, #02B3FF 107.65%), linear-gradient(0deg, #FFFFFF, #FFFFFF)] self-center rounded-full text-[8px] text-white text-center content-center font-semibold">
              {newActivityCount}
            </div>
          ) : (
            <span>{event?.feed_stats?.post_count > 0 ? event?.feed_stats?.post_count : ''}</span>
          )}
          <Icon
            icon="fluent:chat-28-filled"
            tw="w-4 h-4"
            css={event?.feed_stats?.post_count === 0 && tw`text-[#868686]`}
          />
        </div>
      </div>
    </div>
  );
};

export default EventListCard;
