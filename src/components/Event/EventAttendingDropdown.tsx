import { FC } from 'react';
import Select from 'react-select';
import useTranslation from 'next-translate/useTranslation';
import { Event } from '~/src/types/event';

interface Props {
  event: Event;
  disabled?: boolean;
  isCard?: boolean;
  handleChange: (value: string) => void;
}

const EventAttendingDropdown: FC<Props> = ({ event, disabled = false, isCard = false, handleChange }) => {
  const { t } = useTranslation('common');

  const customStyles = {
    container: (provided) => ({
      ...provided,
      width: isCard ? '110px' : '150px',
      fontSize: isCard ? 'small' : '',
    }),
    control: (provided) => ({
      ...provided,
      borderRadius: '100px',
      cursor: 'pointer',
    }),
    valueContainer: (provided) => ({
      ...provided,
      justifyContent: 'center',
    }),
    singleValue: (provided) => ({
      ...provided,
      fontWeight: '500',
      color: '#643CC6',
    }),
    dropdownIndicator: (provided) => ({
      ...provided,
      svg: {
        fill: '#643CC6',
        width: isCard ? '10px' : '',
      },
    }),
    menu: (provided) => ({
      ...provided,
      borderRadius: '10px',
      zIndex: 10,
    }),
    option: (provided) => ({
      ...provided,
      fontWeight: '500',
      color: '#643CC6',
      cursor: 'pointer',
      textAlign: 'center',
    }),
  };

  const determineOptions = () => {
    if (event?.user_attendance?.status === 'pending') {
      return [
        {
          value: 'yes',
          label: t('action.accept'),
        },
        { value: 'no', label: t('action.reject') },
      ];
    }

    if (event?.user_attendance?.status === 'yes') {
      return [{ value: 'no', label: t('action.reject') }];
    }

    return [
      {
        value: 'yes',
        label: t('action.accept'),
      },
    ];
  };

  return (
    <div>
      <Select
        options={determineOptions()}
        styles={customStyles}
        isSearchable={false}
        isDisabled={disabled}
        menuShouldScrollIntoView={true} // force scroll into view
        value={{
          value: event?.user_attendance?.status,
          label: t(`legacy.eventInviteStatus.${event?.user_attendance?.status ?? 'pending'}`),
        }}
        defaultValue={{
          value: 'pending',
          label: t(`legacy.eventInviteStatus.${event?.user_attendance?.status ?? 'pending'}`),
        }}
        onChange={(option) => handleChange(option?.value)}
        placeholder={t(`legacy.eventInviteStatus.${event?.user_attendance?.status ?? 'pending'}`)}
      />
    </div>
  );
};

export default EventAttendingDropdown;
