import { useQuery } from '@tanstack/react-query';
import { Button, Flex, Input, Typography } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useState } from 'react';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import useDebounce from '~/src/hooks/useDebounce';
import useEvents, { useEventDateFilter } from '../useEvents';
import EventTable from '../eventTable/eventTable';
import { SortKey } from '~/__generated__/types';
import { Filter } from '../../Common/filters/filter/filter';
import CreateModal from '../../Common/createModal/createModal';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import { useRouter } from 'next/router';
import { FrontPath } from '../../Layout/menuItems/menuItems';

interface Props {
  entityId: string;
  canManageEvents: boolean;
}

const EntityEventList = ({ entityId, canManageEvents }: Props) => {
  const { t } = useTranslation('common');
  const [search, setSearch] = useState('');
  const debouncedSearch = useDebounce(search, 300);
  const [goToEvent] = useEvents();
  const [showEventModal, setShowEventModal] = useState(false);
  const router = useRouter();
  const [mode, setMode] = useState<'upcoming' | 'past'>('upcoming');
  const [from, to, sortAscending] = useEventDateFilter(mode);

  const { selectedHub } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
  }));

  const selectedSpace = router.query.id ? selectedHub?.entities.find((item) => item.id === router.query.id) : null;

  const { data, isLoading } = useQuery({
    queryKey: [QUERY_KEYS.entityEventList, entityId, from, to, debouncedSearch, , sortAscending],
    queryFn: async () => {
      const response = await api.events.eventsDetail(entityId, {
        Search: debouncedSearch,
        from: from,
        to: to,
        SortKey: SortKey.Date,
        SortAscending: sortAscending,
      });

      return response.data;
    },
  });

  return (
    <>
      <Flex vertical gap="middle">
        <Typography.Title level={3}>{t('event.other')}</Typography.Title>
        <Flex justify="stretch" gap="large">
          <Input.Search
            placeholder={t('action.searchForEvents')}
            onChange={(e) => {
              setSearch(e.target.value);
            }}
            onSearch={(value) => {
              setSearch(value);
            }}
          />
          {canManageEvents && <Button onClick={() => setShowEventModal(true)}>{t('action.add')}</Button>}
          {showEventModal && selectedSpace && (
            <CreateModal
              handleClose={() => setShowEventModal(false)}
              selectedSpace={selectedSpace}
              modalType={FrontPath.Event}
            />
          )}
        </Flex>
        <Flex justify="stretch" gap="middle">
          <Filter
            mode="radio"
            value={mode}
            items={[
              { label: t('upcoming'), value: 'upcoming' },
              { label: t('past'), value: 'past' },
            ]}
            onChange={(val) => setMode(val as 'upcoming' | 'past')}
          />
        </Flex>
        <EventTable
          events={data ?? []}
          loading={isLoading}
          onClickRow={(event) => {
            goToEvent(event.id);
          }}
          canManage={false}
        />
      </Flex>
    </>
  );
};

export default EntityEventList;
