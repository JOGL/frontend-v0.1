import { FC, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from '~/src/contexts/apiContext';
import { useModal } from '~/src/contexts/modalContext';
import CreatableSelect from 'react-select/creatable';
import Button from '../primitives/Button';
import { Hub, User } from '~/src/types';
import { entityItem } from '~/src/utils/utils';
import { components, ControlProps, Props } from 'react-select';
import Icon from '../primitives/Icon';
import useGet from '~/src/hooks/useGet';
import MembersMultiSelect from '../Members/MembersMultiSelect';
import Alert from '../Tools/Alert/Alert';
import SpinLoader from '../Tools/SpinLoader';
import useUser from '~/src/hooks/useUser';
import { TabPanels, Tabs, TabPanel } from '@reach/tabs';
import { NavTab } from '../Tabs/TabsStyles';
import tw from 'twin.macro';
import { Event, EventAttendance, EventInvite } from '~/src/types/event';
import { EntityMini } from '~/src/types/entity';
import { WorkspaceModel } from '~/__generated__/types';

type CustomContainerObject = EntityMini & {
  members?: any[];
};

interface Props {
  event: Event;
  parent?: Hub | WorkspaceModel;
  existingEvent?: boolean;
  submitInvites?: boolean;
  defaultRole?: 'organizer' | 'member';
  showRoleDropdown?: boolean;
  showEmailForm?: boolean;
  createFakeAttendances?: boolean;
  disclaimer?: string;
  handleChange?: (newInvites: EventInvite[], fakeAttendances?: EventAttendance[]) => void;
  callBack?: () => void;
}

interface SelectOption {
  value: string;
  label: string;
  logo_url: any;
}

const EventInviteModal: FC<Props> = ({
  event,
  parent,
  existingEvent = true,
  submitInvites = true,
  defaultRole,
  showRoleDropdown = true,
  showEmailForm = true,
  createFakeAttendances = false,
  disclaimer = undefined,
  handleChange = () => {},
  callBack = () => {},
}) => {
  const [usersList, setUsersList] = useState([]);
  const [containerList, setContainerList] = useState([]);
  const [emailsList, setEmailsList] = useState([]);
  const [formattedEmailsList, setFormattedEmails] = useState([]);
  const [inviteSend, setInviteSend] = useState(false);
  const [sending, setSending] = useState(false);
  const [error, setError] = useState(false);
  const [selectedRole, setSelectedRole] = useState(defaultRole ?? 'member');
  const [tab, setTab] = useState(0);
  const api = useApi();

  const { user } = useUser();
  const { closeModal } = useModal();
  const { t } = useTranslation('common');

  //Get the parent ecosystem user info
  const userEndpoint = existingEvent
    ? `/events/${event?.id}/ecosystem/users`
    : `/${entityItem[parent?.type].back_path}/${parent?.id}/ecosystem/users`;

  const { data: ecosystemOptions } = useGet<EntityMini[]>(userEndpoint);

  //Container options for user
  const [containerObjects, setContainerObjects] = useState<CustomContainerObject[]>([]);
  const [containerOptions, setContainerOptions] = useState<SelectOption[]>();

  //Get invites already sent
  const attendanceEndpoint = existingEvent ? `/events/${event?.id}/attendances` : '';
  const { data: invitesSent } = useGet<EventAttendance[]>(attendanceEndpoint);

  const [filteredInvites, setFilteredInvites] = useState<SelectOption[]>();

  useEffect(() => {
    const filtered = filterUserOptions();
    setFilteredInvites(filtered);
  }, [invitesSent, ecosystemOptions]);

  useEffect(() => {
    const loadUserOptions = async () => {
      try {
        const elements = [
          api.get(`/users/${user?.id}/workspaces?permission=manage`),
          api.get(`/users/${user?.id}/nodes?permission=manage`),
        ];

        let content: any[] = await Promise.allSettled(elements);

        let containerObjects: CustomContainerObject[] = [];

        await Promise.all(
          content
            .filter((item) => item.status === 'fulfilled')
            .map((item) => item.value.data)
            .flat()
            .map(async (object: Hub | WorkspaceModel) => {
              let members = await api.get(`/${entityItem[object?.type]?.back_path}/${object?.id}/members`);

              const container: CustomContainerObject = {
                id: object?.id,
                title: object?.title,
                banner_url_sm: object?.banner_url_sm ?? entityItem[object?.type]?.default_banner,
                banner_url: object?.banner_url,
                short_description: object?.short_description,
                short_title: object?.short_title,
                members: members?.data ?? [],
                entity_type: null,
                full_name: null,
                logo_url: null,
                logo_url_sm: null,
              };

              containerObjects.push(container);
            })
        );

        const containerOptions: SelectOption[] = containerObjects?.map((obj: CustomContainerObject) => {
          return {
            value: obj?.id,
            label: obj?.title,
            logo_url: obj?.banner_url_sm,
          };
        });

        setContainerObjects(containerObjects);
        setContainerOptions(containerOptions);
      } catch (e) {
        setContainerObjects([]);
        setContainerOptions([]);
      }
    };

    loadUserOptions();
  }, []);

  const resetState = () => {
    setInviteSend(false);
    setError(false);
  };

  const handleChangeEmail = (content) => {
    const tempEmailsList = [];
    (content ?? []).map(function (invitee_mail) {
      if (invitee_mail) {
        let commaSeparatedValues = invitee_mail.value
          .split(',')
          .map((email) => email.trim())
          .filter((email) => email !== '');
        invitee_mail && tempEmailsList.push(...commaSeparatedValues);
      }
    });

    setEmailsList(tempEmailsList);
    setFormattedEmails(
      tempEmailsList.map((email) => {
        return { label: email, value: email };
      })
    );
  };

  const handleChangeUserList = (content) => {
    const tempUsersList = [];
    (content ?? []).map(function (user) {
      user && tempUsersList.push(user?.value);
    });

    setUsersList(tempUsersList);
  };

  const handleChangeContainerList = (content) => {
    const tempContainersList = [];
    (content ?? []).map(function (user) {
      user && tempContainersList.push(user?.value);
    });

    setContainerList(tempContainersList);
  };

  const onSuccess = () => {
    setInviteSend(true);
    setSending(false);
    setTimeout(() => {
      closeModal(); // close modal after 1,5sec
      resetState();
    }, 1500);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setSending(true);

    let invitesArray: EventInvite[] = [];
    let fakeAttendances: EventAttendance[] = [];

    (usersList ?? []).map(async (id) => {
      let invitee = ecosystemOptions?.find((option: EntityMini) => option?.id === id);

      const userInvite: EventInvite = {
        user_id: invitee?.id,
        user_email: null,
        community_entity_id: null,
        origin_community_entity_id: null,
        labels: [],
        access_level: selectedRole === 'organizer' ? 'admin' : selectedRole,
      };

      //Only create attendance if the same doesn't exist for the user. This is to prevent the user batch inviting users that are already directly invited
      if (
        createFakeAttendances &&
        fakeAttendances?.findIndex((attendance: EventAttendance) => attendance?.id === invitee?.id) === -1
      ) {
        fakeAttendances.push({
          id: userInvite?.user_id,
          user: (invitee as unknown) as User, //invitee is EntityMini not User, but it is enough for what's needed
          labels: [],
          status: 'pending',
          access_level: userInvite?.access_level,
          origin_community_entity: null,
          event_id: null,
          community_entity: null,
          user_email: null,
        });
      }

      invitesArray.push(userInvite);
    });

    (containerList ?? []).map(async (container) => {
      let invitee = containerObjects?.find((option: CustomContainerObject) => option?.id === container);

      const containerUserInvites: EventInvite[] = (invitee as CustomContainerObject)?.members?.map((member: User) => {
        //Only create attendance if the same doesn't exist for the user. This is to prevent the user batch inviting multiple containers that the same user is in
        if (
          createFakeAttendances &&
          fakeAttendances?.findIndex((attendance: EventAttendance) => attendance?.id === member?.id) === -1
        ) {
          fakeAttendances.push({
            id: member?.id,
            user: member,
            labels: [],
            status: 'pending',
            access_level: 'member',
            origin_community_entity: null,
            event_id: null,
            community_entity: null,
            user_email: null,
          });
        }

        //Return undefined if member as already been included in the invites array
        if (invitesArray?.findIndex((invite: EventInvite) => invite?.user_id === member?.id) === -1) {
          return {
            user_id: member?.id,
            user_email: null,
            community_entity_id: null,
            origin_community_entity_id: null,
            labels: [],
            access_level: 'member',
          };
        }
      });

      //Remove undefineds from previous step
      invitesArray.push(...containerUserInvites.filter((invite: EventInvite) => invite));
    });

    (emailsList ?? []).map(async (email) => {
      let emailInvite: EventInvite = {
        user_id: null,
        user_email: email,
        community_entity_id: null,
        origin_community_entity_id: null,
        labels: [],
        access_level: 'member', //email invitees cannot be organizers
      };

      if (createFakeAttendances) {
        fakeAttendances.push({
          id: null,
          user: null,
          labels: [],
          status: 'pending',
          access_level: 'member',
          origin_community_entity: null,
          event_id: null,
          community_entity: null,
          user_email: email,
        });
      }

      invitesArray.push(emailInvite);
    });

    if (submitInvites) {
      api
        .post(`/events/${event?.id}/invite/batch`, invitesArray)
        .then(() => {
          onSuccess();
          callBack();
        })
        .catch(() => {
          /* Ignore errors firstly because they shouldn't happen, and secondly because the backend is now returning 500 even for repeated invites (409) */
          setSending(false);
          closeModal();
          callBack();
        });
    } else {
      onSuccess();

      if (createFakeAttendances) {
        handleChange(invitesArray, fakeAttendances);
      } else {
        handleChange(invitesArray);
      }
    }
  };

  const filterUserOptions = () => {
    //First filter the invites by getting the useful IDs and removing undefined's from the array (those correspond to email invites)
    const filteredInvites = existingEvent
      ? invitesSent
          ?.map((invite: EventAttendance) => (invite?.user ? invite?.user?.id : invite?.community_entity?.id))
          .filter((elem) => elem !== undefined)
      : event?.attendances_to_upsert
          ?.map((invite: EventInvite) => (invite?.user_id ? invite?.user_id : invite?.community_entity_id))
          .filter((elem) => elem !== undefined);

    //Then just make sure the ID isn't already invited and return what we need.
    return ecosystemOptions
      ?.filter((option: EntityMini) => !filteredInvites?.includes(option?.id))
      ?.map((option: EntityMini) => ({
        value: option?.id,
        label: option?.entity_type === 'user' ? option?.full_name : option?.title,
        logo_url:
          option?.entity_type === 'user'
            ? option?.logo_url_sm || '/images/default/default-user.png'
            : option?.banner_url_sm || entityItem[option?.entity_type].default_banner,
      }));
  };

  const ControlEmail = ({ children, ...props }: ControlProps<any, false>) => {
    return (
      <components.Control {...props}>
        <Icon icon="ic:outline-email" tw="h-5 w-5 text-[#5959599e] ml-4 mr-0" />
        {children}
      </components.Control>
    );
  };

  const customEmailStyle = {
    control: (styles) => ({
      ...styles,
      borderRadius: '25px',
    }),
  };

  return (
    <Tabs style={{ width: '100%' }} defaultIndex={0} index={tab} onChange={(id) => setTab(id)}>
      <div tw="flex justify-center w-full mb-6 p-0">
        <div>
          <NavTab css={tab === 0 ? tw`text-primary` : tw`text-[#808080]`}>{t('individualInvites')}</NavTab>
          <NavTab css={tab === 1 ? tw`text-primary` : tw`text-[#808080]`}>{t('batchInvites')}</NavTab>
        </div>
      </div>
      <TabPanels>
        <form>
          <TabPanel>
            <div tw="mb-6">
              <label tw="font-bold" htmlFor="joglUser">
                {t('inviteToJoin')}
              </label>

              {!filteredInvites ? (
                <div>
                  <SpinLoader />
                </div>
              ) : (
                <MembersMultiSelect
                  handleChangeUser={handleChangeUserList}
                  customOptions={filteredInvites}
                  setSelectedRole={setSelectedRole}
                  customPlaceholder={t('userName')}
                  customRoles={['organizer', 'member']}
                  showRoleDropdown={showRoleDropdown}
                  autoFocus
                />
              )}
            </div>

            {showEmailForm && (
              <div tw="mb-3">
                <label tw="font-bold" htmlFor="byEmail">
                  {t('byEmailAddress')}
                </label>
                <CreatableSelect
                  isClearable
                  isMulti
                  noOptionsMessage={() => null}
                  menuShouldScrollIntoView={true} // force scroll into view
                  placeholder={t('enterEmails')}
                  components={{ DropdownIndicator: null, Control: ControlEmail, IndicatorSeparator: null }}
                  formatCreateLabel={(inputValue) => inputValue}
                  onChange={handleChangeEmail}
                  styles={customEmailStyle}
                  value={formattedEmailsList}
                />
              </div>
            )}
          </TabPanel>

          <TabPanel>
            <div tw="mb-6">
              <label tw="font-bold" htmlFor="joglUser">
                {t('inviteToJoin')}
              </label>

              {!containerOptions ? (
                <div>
                  <SpinLoader />
                </div>
              ) : (
                <MembersMultiSelect
                  handleChangeUser={handleChangeContainerList}
                  customOptions={containerOptions}
                  setSelectedRole={setSelectedRole}
                  customPlaceholder={t('containerName')}
                  customRoles={['organizer', 'member']}
                  showRoleDropdown={false}
                  autoFocus
                />
              )}
            </div>
          </TabPanel>

          {disclaimer && <div tw="text-sm mb-4">{disclaimer}</div>}

          <div className="btnZone" tw="text-center">
            {!inviteSend ? (
              <Button
                onClick={handleSubmit}
                disabled={(emailsList.length === 0 && usersList.length === 0 && containerList.length === 0) || sending}
              >
                {sending && <SpinLoader />}
                {t('action.invite')}
              </Button>
            ) : (
              <Alert type="success" message={existingEvent ? t('invitesSent') : t('invitesSaved')} />
            )}
          </div>

          {error && (
            <div className="alert alert-danger" role="alert">
              {t('error.generic')}
            </div>
          )}
        </form>
      </TabPanels>
    </Tabs>
  );
};
export default EventInviteModal;
