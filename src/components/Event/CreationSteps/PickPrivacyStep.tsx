import Trans from 'next-translate/Trans';
import useTranslation from 'next-translate/useTranslation';
import { FC } from 'react';
import { EventSelectContainerOption } from '~/src/types/event';
import { EventPrivacySettings } from '../EventPrivacySettings';
import { Checkbox, Flex } from 'antd';

interface Props {
  selectedContainer: EventSelectContainerOption;
  selectedPrivacy: string;
  batchInviteMembers: boolean;
  handleChangeContainerClick: () => void;
  handleBatchInviteMembersClick: (checked: boolean) => void;
  handlePrivacySelection: (newChoice: string) => void;
}

export const PickPrivacyStep: FC<Props> = ({
  selectedContainer,
  selectedPrivacy,
  batchInviteMembers,
  handleBatchInviteMembersClick,
  handlePrivacySelection,
}) => {
  const { t } = useTranslation('common');
  return (
    <Flex vertical gap="middle">
      <Flex gap="small">
        <EventPrivacySettings
          selectedPrivacy={selectedPrivacy}
          parentContainerName={selectedContainer.label}
          parentContainerPrivacy={selectedContainer.content_privacy}
          handleSelect={handlePrivacySelection}
        />
      </Flex>
      {selectedContainer?.members_count > 0 && (
        <Checkbox checked={!!batchInviteMembers} onChange={(e) => handleBatchInviteMembersClick(e.target.checked)}>
          <Trans
            i18nKey="common:batchInviteContainerMembers"
            components={[<span key="common:batchInviteContainerMembers" tw="lowercase" />]}
            values={{
              membersCount: `${selectedContainer?.members_count} ${t('member', {
                count: selectedContainer?.members_count,
              })}`,
              container: `${selectedContainer?.label}`,
            }}
          />
        </Checkbox>
      )}
    </Flex>
  );
};
