import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import {
  EVENT_PRIVACY_SETTINGS,
  EventPrivacyOption,
  EventPrivacyOptionChild,
  EventPrivacySetting,
} from '~/src/types/event';
import tw from 'twin.macro';

interface BaseProps {
  selectedPrivacy: string;
  parentContainerName: string;
  parentContainerPrivacy: string;
  handleSelect: (newChoice: string) => void;
}

export const EventPrivacySettings: FC<BaseProps> = ({
  selectedPrivacy,
  parentContainerName,
  parentContainerPrivacy,
  handleSelect,
}) => {
  return (
    <div tw="flex flex-col gap-2">
      {EVENT_PRIVACY_SETTINGS?.map((setting: EventPrivacyOption) => {
        switch (setting.key) {
          case EventPrivacySetting.Visibility:
            return (
              <RenderVisibilitySetting
                key={setting.key}
                setting={setting}
                selectedPrivacy={selectedPrivacy}
                parentContainerName={parentContainerName}
                parentContainerPrivacy={parentContainerPrivacy}
                handleSelect={handleSelect}
              />
            );
          default:
            return null;
        }
      })}
    </div>
  );
};

interface VisibilityProps extends BaseProps {
  setting: EventPrivacyOption;
}

const RenderVisibilitySetting: FC<VisibilityProps> = ({
  setting,
  selectedPrivacy,
  parentContainerName,
  parentContainerPrivacy,
  handleSelect,
}) => {
  const { t } = useTranslation('common');

  return (
    <div tw="flex gap-5 justify-evenly max-[532px]:flex-wrap">
      {setting?.options?.map((option: EventPrivacyOptionChild) => {
        const isPublicAndDisabled = option.key === 'public' && parentContainerPrivacy === 'private';

        return (
          <button
            key={`${setting.key}-${option.key}`}
            tw="flex flex-col gap-2 items-center border p-4 w-full"
            disabled={isPublicAndDisabled}
            css={[
              !isPublicAndDisabled ? tw`hover:(bg-[#ECECEC])` : tw`opacity-50`,
              selectedPrivacy === option.key && tw`bg-[#ECECEC]`,
            ]}
            onClick={(e) => {
              e.preventDefault();
              handleSelect(option.key);
            }}
          >
            <span tw="text-[#454545] font-semibold">
              {option?.key === 'container'
                ? `${parentContainerName} ${t('event.one').toLowerCase()}`
                : t(option?.title)}
            </span>
            <span tw="text-xs text-[#8B8B8B]">
              {option?.key === 'container' ? t(option?.info, { container: parentContainerName }) : t(option?.info)}
            </span>
          </button>
        );
      })}
    </div>
  );
};
