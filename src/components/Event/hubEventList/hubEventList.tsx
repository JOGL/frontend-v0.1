import { useInfiniteQuery, useQuery } from '@tanstack/react-query';
import { Flex, Input, Typography } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useMemo, useState } from 'react';
import { AttendanceStatus, EventModel, FeedEntityFilter, SortKey } from '~/__generated__/types';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import useDebounce from '~/src/hooks/useDebounce';
import { ContainerFilter } from '../../Common/filters/containerFilter/containerFilter';
import EventTable from '../eventTable/eventTable';
import useEvents, { useEventDateFilter } from '../useEvents';
import { Filter } from '../../Common/filters/filter/filter';
const PAGE_SIZE = 50;

interface Props {
  hubId: string;
  title: string;
  filter?: FeedEntityFilter;
  sortKey?: SortKey;
  allowPast?: boolean;
}

const HubEventList = ({ hubId, title, filter, sortKey = SortKey.Date, allowPast = true }: Props) => {
  const { t } = useTranslation('common');
  const [search, setSearch] = useState('');
  const debouncedSearch = useDebounce(search, 300);
  const [selectedContainers, setSelectedContainers] = useState<string[]>([]);
  const [goToEvent] = useEvents();
  const [mode, setMode] = useState<'upcoming' | 'past'>('upcoming');
  const [attendance, setAttendance] = useState<AttendanceStatus>();
  const [from, to, sortAscending] = useEventDateFilter(mode);

  const { data: eventsData, isLoading } = useInfiniteQuery({
    queryKey: [
      QUERY_KEYS.eventsAggregate,
      hubId,
      from,
      to,
      attendance,
      selectedContainers,
      debouncedSearch,
      sortKey,
      sortAscending,
    ],
    queryFn: async ({ pageParam = 1 }) => {
      const response = await api.nodes.eventsAggregateDetail(hubId, {
        Search: debouncedSearch,
        communityEntityIds: selectedContainers.join(',') ?? undefined,
        filter: filter,
        SortKey: sortKey,
        SortAscending: sortAscending,
        from: from,
        to: to,
        status: attendance,
        Page: pageParam,
        PageSize: PAGE_SIZE,
      });
      return response.data;
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItemsCount = allPages.reduce((acc, page) => acc + page.items.length, 0);
      return loadedItemsCount < lastPage.total ? allPages.length + 1 : undefined;
    },
  });

  const { data: containers } = useQuery({
    queryKey: [QUERY_KEYS.eventsAggregateContainers, hubId],
    queryFn: async () => {
      const response = await api.nodes.eventsAggregateCommunityEntitiesDetail(hubId, {});
      return response.data;
    },
  });

  const flattenedData: EventModel[] = useMemo(() => {
    return eventsData?.pages.flatMap((page) => page.items) ?? [];
  }, [eventsData]);

  return (
    <>
      <Flex vertical gap="middle">
        <Typography.Title>{title}</Typography.Title>
        <Flex justify="stretch">
          <Input.Search
            placeholder={t('action.searchForEvents')}
            onChange={(e) => {
              setSearch(e.target.value);
            }}
            onSearch={(value) => {
              setSearch(value);
            }}
          />
        </Flex>
        <Flex justify="stretch" gap="middle" wrap="wrap">
          {allowPast === true && (
            <Filter
              mode="radio"
              value={mode}
              items={[
                { label: t('upcoming'), value: 'upcoming' },
                { label: t('past'), value: 'past' },
              ]}
              onChange={(val) => setMode(val as 'upcoming' | 'past')}
            />
          )}
          {containers && (
            <ContainerFilter
              containers={containers}
              onChange={(communityEntityIds) => setSelectedContainers(communityEntityIds)}
            />
          )}
          <Filter
            mode="select"
            placeholder={t('event.all')}
            items={[
              { label: t('refused'), value: AttendanceStatus.No },
              { label: t('pending'), value: AttendanceStatus.Pending },
              { label: t('accepted'), value: AttendanceStatus.Yes },
            ]}
            onChange={(val) => setAttendance(val as AttendanceStatus)}
          />
        </Flex>
        <EventTable
          events={flattenedData}
          loading={isLoading}
          onClickRow={(event) => {
            goToEvent(event.id);
          }}
          canManage={false}
        />
      </Flex>
    </>
  );
};

export default HubEventList;
