import { FC, useEffect, useRef, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import SearchWithApi, { SearchRef } from '../Search/SearchWithApi';
import Button from '../primitives/Button';
import { useModal } from '~/src/contexts/modalContext';
import PeopleAttendanceRecord from './PeopleAttendanceRecord';
import { EventInvitesBatchActions } from './EventRenderUtils';
import usePagedTable from '~/src/hooks/usePagedtable';
import { confAlert } from '~/src/utils/utils';
import PagedTableControl from '../Tools/PagedTableControl';
import SpinLoader from '../Tools/SpinLoader';
import EventInviteModal from './EventInviteModal';
import { useApi } from '~/src/contexts/apiContext';
import { User } from '~/src/types';
import SendEmailToJoglUserModal from '../Tools/SendEmailToJoglUserModal';
import { Event, EventAttendance } from '~/src/types/event';

interface GuestInfoProps {
  event: Event;
}

const EventGuestInfo: FC<GuestInfoProps> = ({ event }) => {
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();

  const api = useApi();
  const searchRef = useRef<SearchRef>(null);

  const [invites, setInvites] = useState<EventAttendance[]>();
  const [selectedInvites, setSelectedInvites] = useState<EventAttendance[]>([]);
  const [tablePage, setTablePage] = useState<number>(1);
  const [rowsPerPage, setRowsPerPage] = useState<number>(25);
  const { slice, totalPages } = usePagedTable(invites, tablePage, rowsPerPage);

  useEffect(() => {
    if (invites) {
      //Update selected invites
      setSelectedInvites((prevSelected) =>
        prevSelected?.filter(
          (selectedInvite: EventAttendance) =>
            invites?.findIndex((invite: EventAttendance) => invite?.id === selectedInvite?.id) !== -1
        )
      );
    }
  }, [invites]);

  const handleSelectAll = () => {
    if (selectedInvites?.length === invites?.length) {
      setSelectedInvites([]);
    } else {
      setSelectedInvites(invites);
    }
  };

  const handleSelectInvite = (selectedInvite: EventAttendance) => {
    const index = selectedInvites.findIndex((invite: EventAttendance) => invite?.id === selectedInvite?.id);

    if (index !== -1) {
      setSelectedInvites((prevSelected) =>
        prevSelected.filter((invite: EventAttendance) => invite?.id !== selectedInvite?.id)
      );
    } else {
      setSelectedInvites((prevSelected) => [...prevSelected, selectedInvite]);
    }
  };

  const handleOrganizerChange = (key: string, invite: EventAttendance) => {
    searchRef?.current?.refetchResult();
    confAlert.fire({ icon: 'success', title: t('rolesChangeSuccess') });
  };

  const handleSpeakerChange = (invite: EventAttendance) => {
    const body = invite?.labels?.includes('speaker') ? [] : ['speaker'];

    api
      .post(`/events/attendances/${invite?.id}/labels`, body)
      .then(() => {
        searchRef?.current?.refetchResult();
        confAlert.fire({ icon: 'success', title: t('speakersChangedSuccess') });
      })
      .catch(() => {
        confAlert.fire({ icon: 'error', title: t('speakersChangedError') });
      });
  };

  const handleSendMessage = (invite: EventAttendance) => {
    showModal({
      children: (
        <SendEmailToJoglUserModal
          containerType="event"
          containerId={event?.id}
          recipients={[invite?.user]}
          closeModal={closeModal}
        />
      ),
      title: t('action.sendEmail'),
    });
  };

  const handleBatchMessage = () => {
    const recipients = selectedInvites
      ?.filter((invite: EventAttendance) => invite?.status !== 'no' && invite?.user)
      ?.map((invite: EventAttendance) => invite?.user);

    if (recipients?.length > 0) {
      showModal({
        children: (
          <SendEmailToJoglUserModal
            containerType="event"
            containerId={event?.id}
            recipients={recipients}
            disclaimer={t('onlyUsersWhoHaveNotRejectCanBeContacted')}
            closeModal={closeModal}
          />
        ),
        title: t('action.sendEmail'),
      });
    } else {
      showModal({
        children: <div>{t('onlyUsersWhoHaveNotRejectCanBeContacted')}</div>,
        title: t('invite.one'),
      });
    }
  };

  const handleRemoveInvite = (invite: EventAttendance) => {
    api
      .delete(`/events/attendances/${invite?.id}`)
      .then(() => {
        searchRef?.current?.refetchResult();
        confAlert.fire({ icon: 'success', title: t('invitesRemovedSuccess') });
      })
      .catch(() => {
        confAlert.fire({ icon: 'error', title: t('invitesRemovedError') });
      });
  };

  const batchRemoveCallback = async (usersToDelete: EventAttendance[]) => {
    api
      .delete(`/events/attendances/batch`, { data: usersToDelete?.map((invite: EventAttendance) => invite?.id) })
      .then(() => {
        searchRef?.current?.refetchResult();
        closeModal();
        confAlert.fire({ icon: 'success', title: t('invitesRemovedSuccess') });
      })
      .catch(() => {
        closeModal();
        confAlert.fire({ icon: 'error', title: t('invitesRemovedError') });
      });
  };

  const handleBatchRemove = () => {
    const usersToDelete = selectedInvites?.filter(
      (invite: EventAttendance) => invite?.user?.id !== event?.created_by_user_id
    );

    const isTryingToDeleteOwner =
      selectedInvites
        ?.map((invite: EventAttendance) => invite?.user)
        ?.findIndex((user: User) => user?.id === event?.created_by_user_id) !== -1;

    if (usersToDelete?.length > 0) {
      showModal({
        children: (
          <div tw="flex flex-col gap-2 items-center">
            {t('deleteInvitesConfirmation')}
            {isTryingToDeleteOwner && <>&nbsp;{t('theCreatorOfTheEventCannotBeRemoved')}</>}
            <Button type="submit" onClick={() => batchRemoveCallback(usersToDelete)}>
              {t('action.confirm')}
            </Button>
          </div>
        ),
        title: t('action.deleteItem', { item: t('invite.other') }),
      });
    } else {
      showModal({
        children: <div> {t('theCreatorOfTheEventCannotBeRemoved')}</div>,
        title: t('invite.one'),
      });
    }
  };

  const constructFilterString = (filters: string[]) => {
    const filterObject: Record<string, any> = {};

    const levelFilters = filters.filter((filter) => ['admin', 'member'].includes(filter));
    if (levelFilters?.length > 0 && levelFilters?.length < 2) {
      filterObject.level = levelFilters.join(',');
    }

    const labelsFilters = filters.filter((filter) => ['speaker'].includes(filter));
    if (labelsFilters?.length > 0) {
      filterObject.labels = labelsFilters;
    }

    if (filters.includes('yes')) {
      filterObject.statuses = 'yes';
    }

    const filterString = Object.entries(filterObject)
      .map(([key, value]) => `${key}=${value}`)
      .join('&');

    return filterString;
  };

  const renderTableHeaders = (headers: string[]) => {
    return (
      <tr>
        {headers?.map((header: string, index: number) => {
          return <th key={`${header}-${index}`}>{header}</th>;
        })}
      </tr>
    );
  };

  const renderTableRows = (rows: EventAttendance[]) => {
    return rows?.map((invite: EventAttendance) => (
      <PeopleAttendanceRecord
        key={`${invite?.id}`}
        invite={invite}
        inviteSelected={selectedInvites?.findIndex((i: EventAttendance) => i?.id === invite?.id) !== -1}
        showRoleDropdown
        type={invite?.user ? 'user' : 'email'}
        event_owner_id={event?.created_by_user_id}
        handleSelectInvite={() => handleSelectInvite(invite)}
        handleRoleChange={(key: string) => handleOrganizerChange(key, invite)}
        handleSpeakerChange={() => handleSpeakerChange(invite)}
        handleSendMessage={() => handleSendMessage(invite)}
        handleRemove={() => handleRemoveInvite(invite)}
      />
    ));
  };

  return (
    <div>
      {event?.permissions?.includes('manage') && (
        <Button
          tw="h-9 font-medium mb-4"
          onClick={() => {
            showModal({
              children: (
                <EventInviteModal
                  event={event}
                  callBack={() => {
                    searchRef?.current?.refetchResult();
                  }}
                />
              ),
              title: t('invite.other'),
              maxWidth: '50rem',
            });
          }}
        >
          {t('invite.one')}
        </Button>
      )}

      <div tw="flex flex-wrap gap-2">
        <SearchWithApi
          key={`${event?.id}-invites`}
          placeHolder={t('action.searchInvite')}
          customCss={{ flex: 'auto' }}
          showPageNoFilter={false}
          ref={searchRef}
          allowMultipleFilters={true}
          aggregateOptions={[
            { label: t('organizer.one'), value: 'admin' },
            { label: t('member.one'), value: 'member' },
            { label: t('speaker.one'), value: 'speaker' },
            { label: t('attending'), value: 'yes' },
          ]}
          searchQuery={(search, page, pageSize, { aggregateOption }) => {
            const filters = constructFilterString(aggregateOption as string[]);

            return `events/${event?.id}/attendances?Search=${search}&Page=${page}&PageSize=${pageSize}&${filters}`;
          }}
          showResultComponent={false}
          getSearchData={(eventInvites) => {
            setInvites(eventInvites?.filter((invite: EventAttendance) => invite?.user || invite?.user_email));
          }}
          renderer={() => <></>}
        />

        <PagedTableControl
          slice={slice}
          page={tablePage}
          totalPages={totalPages}
          rowsPerPage={rowsPerPage}
          rowsPerPageOptions={[5, 10, 25, 50]}
          setPage={setTablePage}
          setRowsPerPage={setRowsPerPage}
        />
      </div>

      {selectedInvites?.length > 0 && (
        <EventInvitesBatchActions handleBatchMessage={handleBatchMessage} handleBatchRemove={handleBatchRemove} />
      )}

      {!invites && (
        <div tw="text-center">
          <SpinLoader />
        </div>
      )}

      {invites?.length > 0 && (
        <div tw="flex items-center mt-8 mb-4">
          <input
            id="members"
            type="checkbox"
            tw="hover:cursor-pointer w-auto"
            onChange={handleSelectAll}
            checked={selectedInvites?.length > 0 && selectedInvites?.length === invites?.length}
          />
          <label htmlFor="members" tw="ml-2 mb-0">
            {t('action.selectAll')}
          </label>
        </div>
      )}

      {/* People invites*/}
      {invites?.length > 0 && (
        <div className="show-scrollbar" tw="overflow-auto sm:(overflow-visible)">
          <table tw="w-full table-auto mb-2">
            <thead tw="border-b">{renderTableHeaders(['', t('inviteType'), t('status'), t('role.one'), ''])}</thead>
            <tbody>
              {/* Trick to add space between head and body */}
              <tr tw="h-3" />
              {renderTableRows(slice)}
            </tbody>
          </table>
        </div>
      )}
    </div>
  );
};

export default EventGuestInfo;
