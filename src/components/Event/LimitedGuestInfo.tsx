import { FC, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Button from '../primitives/Button';
import { useModal } from '~/src/contexts/modalContext';
import usePagedTable from '~/src/hooks/usePagedtable';
import { confAlert } from '~/src/utils/utils';
import PagedTableControl from '../Tools/PagedTableControl';
import EventInviteModal from './EventInviteModal';
import { Hub, User } from '~/src/types';
import useUser from '~/src/hooks/useUser';
import LimitedAttendanceRecord from './LimitedAttendanceRecord';
import { Event, EventAttendance, EventInvite } from '~/src/types/event';
import { WorkspaceModel } from '~/__generated__/types';

interface GuestInfoProps {
  event: Event;
  parent: WorkspaceModel | Hub;
  parentMembers: User[];
  handleAttendancesToUpsertChange: (newInvites: EventInvite[]) => void;
}

const LimitedGuestInfo: FC<GuestInfoProps> = ({ event, parent, parentMembers, handleAttendancesToUpsertChange }) => {
  const { t } = useTranslation('common');
  const { showModal } = useModal();
  const { user } = useUser();

  const [invites, setInvites] = useState<EventAttendance[]>([]);
  const [tablePage, setTablePage] = useState<number>(1);
  const [rowsPerPage, setRowsPerPage] = useState<number>(25);
  const { slice, totalPages } = usePagedTable(invites, tablePage, rowsPerPage);
  useEffect(() => {
    if (user && invites?.length === 0) {
      //If the user selected "Batch invite ..." while creating the event, we need to create the matching attendances to display on the table.
      //At the very least, one will be created for the user that is creating the event
      const invitesToBeSent: EventAttendance[] = event?.attendances_to_upsert?.map((invite: EventInvite) => ({
        user: parentMembers?.find((member: User) => member?.id === invite.user_id),
        id: invite.user_id,
        access_level: invite.user_id === user.id ? 'admin' : 'member',
        status: 'pending',
        labels: [],
        community_entity: null,
        event_id: null,
        origin_community_entity: null,
        user_email: invite.user_email,
      }));
      setInvites(invitesToBeSent);
    }
  }, [user, invites, parentMembers, event]);

  const handleNewInvites = (newInvites: EventInvite[], newAttendances: EventAttendance[]) => {
    //Remove duplicate invites that can be cause when the user batch invites multiple containers with overlapping users
    const cleanInvites: EventInvite[] = [];
    newInvites?.map((newInvite: EventInvite) => {
      if (
        cleanInvites?.findIndex(
          (cleanInvite: EventInvite) =>
            cleanInvite?.user_id === newInvite?.user_id && cleanInvite?.user_email === newInvite?.user_email
        ) === -1
      ) {
        cleanInvites.push(newInvite);
      }
    });

    const filteredInvites = cleanInvites
      ?.map((newInvite: EventInvite) => {
        //First check if there's already the same invite in the array
        let index = event?.attendances_to_upsert?.findIndex(
          (invite: EventInvite) =>
            invite?.user_id === newInvite?.user_id && invite?.user_email === newInvite?.user_email
        );

        //Means the invite is not already there.
        if (index === -1) {
          return newInvite;
        }
      })
      .filter((invite: EventInvite) => invite); //remove undefineds caused by existing invites

    const filteredAttendances = newAttendances?.filter(
      (fakeAttendance: EventAttendance) =>
        filteredInvites?.findIndex(
          (invite: EventInvite) =>
            invite?.user_id === fakeAttendance?.id && invite?.user_email === fakeAttendance?.user_email
        ) !== -1
    );

    handleAttendancesToUpsertChange([...event?.attendances_to_upsert, ...filteredInvites]);
    setInvites((prevInvites: EventAttendance[]) => [...prevInvites, ...filteredAttendances]);
  };

  const handleRoleChange = (key: string, invite: EventAttendance) => {
    //Modifying the arrays directly is fine here
    let currentInvites = event?.attendances_to_upsert;
    let currentAttendances = invites;

    let inviteIndex = currentInvites?.findIndex(
      (existingInvite: EventInvite) => existingInvite?.user_id === invite?.user?.id
    );

    let attendanceIndex = currentAttendances?.findIndex(
      (existingAttendance: EventAttendance) => existingAttendance?.user?.id === invite?.user?.id
    );

    if (inviteIndex !== -1 && attendanceIndex !== -1) {
      currentInvites[inviteIndex].access_level = key === 'organizer' ? 'admin' : 'member';
      currentAttendances[attendanceIndex].access_level = key === 'organizer' ? 'admin' : 'member';
    }

    handleAttendancesToUpsertChange(currentInvites);
    setInvites(currentAttendances);
    confAlert.fire({ icon: 'success', title: t('rolesChangeSuccess') });
  };

  const handleSpeakerChange = (invite: EventAttendance) => {
    //Modifying the arrays directly is fine here
    let currentInvites = event?.attendances_to_upsert;
    let currentAttendances = invites;

    let inviteIndex = currentInvites?.findIndex((existingInvite: EventInvite) => {
      if (invite?.user) {
        return existingInvite?.user_id === invite?.user?.id;
      }
      return existingInvite?.user_email === invite?.user_email;
    });

    let attendanceIndex = currentAttendances?.findIndex(
      (existingAttendance: EventAttendance) =>
        existingAttendance?.user?.id === invite?.user?.id && existingAttendance?.user_email === invite?.user_email
    );

    if (inviteIndex !== -1 && attendanceIndex !== -1) {
      currentInvites[inviteIndex].labels = invite?.labels?.includes('speaker') ? [] : ['speaker'];
      currentAttendances[attendanceIndex].labels = invite?.labels?.includes('speaker') ? [] : ['speaker'];
    }

    handleAttendancesToUpsertChange(currentInvites);
    setInvites(currentAttendances);
    confAlert.fire({ icon: 'success', title: t('speakersChangedSuccess') });
  };

  const handleRemoveInvite = (invite: EventAttendance) => {
    let currentInvites = event?.attendances_to_upsert ?? [];
    //Here we need a new reference to the array because we need to trigger the usePagedTable hook to work again
    let currentAttendances = [...invites];

    let inviteIndex = currentInvites?.findIndex((existingInvite: EventInvite) => {
      if (invite?.user) {
        return existingInvite?.user_id === invite?.user?.id;
      }
      return existingInvite?.user_email === invite?.user_email;
    });

    let attendanceIndex = currentAttendances?.findIndex(
      (existingAttendance: EventAttendance) =>
        existingAttendance?.user?.id === invite?.user?.id && existingAttendance?.user_email === invite?.user_email
    );

    if (inviteIndex !== -1 && attendanceIndex !== -1) {
      currentInvites.splice(inviteIndex, 1);
      currentAttendances.splice(attendanceIndex, 1);
    }

    handleAttendancesToUpsertChange(currentInvites);
    setInvites(currentAttendances);
    confAlert.fire({ icon: 'success', title: t('invitesRemovedSuccess') });
  };

  return (
    <div className="show-scrollbar" tw="flex flex-col overflow-auto sm:(overflow-visible)">
      <div tw="flex flex-wrap justify-between items-center gap-2 mb-4 w-full">
        <Button
          tw="h-9 font-medium"
          onClick={() => {
            showModal({
              children: (
                <EventInviteModal
                  event={event}
                  parent={parent}
                  existingEvent={false}
                  submitInvites={false}
                  createFakeAttendances={true}
                  handleChange={handleNewInvites}
                />
              ),
              title: t('invite.other'),
              maxWidth: '50rem',
            });
          }}
        >
          {t('invite.one')}
        </Button>

        <PagedTableControl
          slice={slice}
          page={tablePage}
          totalPages={totalPages}
          rowsPerPage={rowsPerPage}
          rowsPerPageOptions={[5, 10, 25, 50]}
          setPage={setTablePage}
          setRowsPerPage={setRowsPerPage}
        />
      </div>

      <table tw="w-full table-auto mb-2">
        <thead tw="border-b">
          <RenderTableHeaders headers={['', t('inviteType'), t('status'), t('role.one'), '']} />
        </thead>

        <tbody>
          <tr tw="h-3" /> {/* Trick to add space between head and body */}
          <RenderTableRows
            rows={slice}
            handleRoleChange={handleRoleChange}
            handleSpeakerChange={handleSpeakerChange}
            handleRemoveInvite={handleRemoveInvite}
          />
        </tbody>
      </table>
    </div>
  );
};

export default LimitedGuestInfo;

const RenderTableHeaders: FC<{ headers: string[] }> = ({ headers }) => {
  return (
    <tr>
      {headers?.map((header: string, index: number) => {
        return <th key={`${header}-${index}`}>{header}</th>;
      })}
    </tr>
  );
};

interface TableRowsProps {
  rows: EventAttendance[];
  handleRoleChange: (key: string, invite: EventAttendance) => void;
  handleSpeakerChange: (invite: EventAttendance) => void;
  handleRemoveInvite: (invite: EventAttendance) => void;
}

const RenderTableRows: FC<TableRowsProps> = ({ rows, handleRoleChange, handleSpeakerChange, handleRemoveInvite }) => {
  const { user } = useUser();
  return (
    <>
      {rows?.map((invite: EventAttendance) => (
        <LimitedAttendanceRecord
          key={`${invite?.id}`}
          invite={invite}
          showRoleDropdown
          type={invite?.user ? 'user' : 'email'}
          event_owner_id={user?.id}
          handleRoleChange={(key: string) => handleRoleChange(key, invite)}
          handleSpeakerChange={() => handleSpeakerChange(invite)}
          handleRemove={() => handleRemoveInvite(invite)}
        />
      ))}
    </>
  );
};
