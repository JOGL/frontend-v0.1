import { Menu, MenuButton, MenuItem, MenuList } from '@reach/menu-button';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import { FC } from 'react';
import ReactTooltip from 'react-tooltip';
import { useModal } from '~/src/contexts/modalContext';
import Button from '../primitives/Button';
import DropdownRole from '../Tools/DropdownRole';
import tw from 'twin.macro';
import { entityItem } from '~/src/utils/utils';
import Icon from '../primitives/Icon';
import { EventAttendance } from '~/src/types/event';

interface PeopleAttendanceRecordProps {
  invite: EventAttendance;
  inviteSelected: boolean;
  showRoleDropdown?: boolean;
  type: string;
  event_owner_id: string;
  handleSelectInvite: () => void;
  handleRoleChange: (key: string) => void;
  handleSpeakerChange: () => void;
  handleSendMessage: () => void;
  handleRemove: () => void;
}

const PeopleAttendanceRecord: FC<PeopleAttendanceRecordProps> = ({
  invite,
  inviteSelected,
  showRoleDropdown = false,
  type,
  event_owner_id,
  handleSelectInvite,
  handleRoleChange,
  handleSpeakerChange,
  handleSendMessage,
  handleRemove,
}) => {
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();

  const renderUserLabels = () => {
    return (
      <div tw="flex gap-1 flex-wrap">
        {invite?.labels?.map((label: string) => {
          return (
            <span
              key={`${invite?.id}-${label}`}
              tw="p-1 text-xs rounded-md bg-[#D1D5DB4D] border border-solid border-gray-200 self-start"
            >
              {t('speaker.one')}
            </span>
          );
        })}
      </div>
    );
  };

  const getLogoUrl = () => {
    if (type === 'email') {
      return '/images/default/default-email.png';
    }

    return invite?.user.logo_url_sm ?? '/images/default/default-user.png';
  };

  return (
    <tr>
      <td tw="flex items-center space-x-3 max-w-[400px] min-w-60 mb-3">
        {/* Select box*/}
        <input
          id={invite?.id}
          type="checkbox"
          tw="hover:cursor-pointer w-auto"
          checked={inviteSelected}
          onChange={handleSelectInvite}
        />
        <div tw="h-[50px] min-h-[50px] w-[50px] min-w-[50px]">
          <div
            style={{ backgroundImage: `url(${getLogoUrl()})` }}
            tw="bg-center h-[50px] w-[50px] rounded-full border border-solid border-[#ced4da]"
            css={[type === 'email' ? tw`bg-no-repeat` : tw`bg-cover`]}
          />
        </div>

        {/* Member name*/}
        <div tw="flex flex-col">
          {renderUserLabels()}
          {invite?.user?.id ? (
            <Link href={`/user/${invite?.user?.id}`}>
              {invite?.user?.first_name} {invite?.user?.last_name}
            </Link>
          ) : (
            <span>{invite?.user_email}</span>
          )}
        </div>
      </td>

      <td tw="text-center min-w-28">
        <span tw="inline-block border border-gray-200 rounded-full px-3 py-1 w-fit text-[14px]">
          {invite?.origin_community_entity ? (
            <Link
              href={`/${entityItem[invite?.origin_community_entity?.type]?.front_path}/${
                invite?.origin_community_entity?.id
              }`}
              target="_blank"
              tw="text-black"
            >
              {t('invitedVia')}&nbsp;
              <span tw="font-semibold">{invite?.origin_community_entity?.title}</span>
            </Link>
          ) : (
            <>{t('directInvite')}</>
          )}
        </span>
      </td>

      <td tw="text-center min-w-28">
        <div
          tw="inline-block border border-gray-200 rounded-full px-3 py-1 w-fit text-[14px] font-semibold"
          css={[
            invite?.status === 'yes'
              ? tw`text-[#1CB52B]`
              : invite?.status === 'no'
              ? tw`text-[#F60000]`
              : tw`text-[#F2A611]`,
          ]}
        >
          <Icon icon="material-symbols:circle" tw="h-2 w-2" />
          {t(`legacy.eventInviteStatus.${invite?.status}`)}
        </div>
      </td>

      <td tw="text-center min-w-28">
        {/* Role dropdown*/}
        {showRoleDropdown && (
          <div tw="w-36 inline-block">
            <DropdownRole
              actualRole={invite?.access_level === 'admin' ? 'organizer' : invite?.access_level}
              onRoleChanged={() => {}}
              itemId={invite?.id}
              itemType="event"
              listRole={['organizer', 'member']}
              member={invite}
              callBack={handleRoleChange}
              setNewRole={() => {}}
              isDisabled={invite?.status === 'no' || invite?.user?.id === event_owner_id || type === 'email'} //Prevent changing roles on: rejected invites, owner, and email invitees
            />
          </div>
        )}
      </td>

      <td>
        {/* More actions */}
        <Menu
          // show/hide tooltip on element focus/blur
          onFocus={(e) => ReactTooltip.show(e.target)}
          onBlur={(e) => ReactTooltip.hide(e.target)}
          tabIndex="0"
          data-tip={t('moreActions')}
          data-for="more_actions"
        >
          <MenuButton tw="text-[.8rem] h-[fit-content] text-gray-700 px-1 hover:bg-gray-300 rounded-sm">•••</MenuButton>
          <MenuList className="post-manage-dropdown" tw="text-center">
            {invite?.status !== 'no' && (
              <>
                <MenuItem onSelect={handleSpeakerChange}>
                  {invite?.labels?.includes('speaker') ? t('removeSpeaker') : t('addAsSpeaker')}
                </MenuItem>

                {invite?.user && (
                  <>
                    <hr tw="m-0" /> <MenuItem onSelect={handleSendMessage}>{t('action.sendMessage')}</MenuItem>
                  </>
                )}
              </>
            )}

            {event_owner_id !== invite?.user?.id && (
              <>
                {invite?.status !== 'no' && <hr tw="m-0" />}
                <MenuItem
                  onSelect={() => {
                    showModal({
                      children: (
                        <div tw="flex flex-col gap-2 items-center">
                          {t('deleteItemConfirmation', { item: t('invite.one') })}
                          <Button
                            type="submit"
                            onClick={() => {
                              handleRemove();
                              closeModal();
                            }}
                          >
                            {t('action.confirm')}
                          </Button>
                        </div>
                      ),
                      title: t('action.deleteItem', { item: t('invite.one') }),
                      maxWidth: '30rem',
                    });
                  }}
                >
                  {t('action.remove')}
                </MenuItem>
              </>
            )}
          </MenuList>
        </Menu>
        <ReactTooltip id="more_actions" delayHide={300} effect="solid" role="tooltip" />
      </td>
    </tr>
  );
};

export default PeopleAttendanceRecord;
