import { FC, ReactNode, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import FormDefaultComponent from '~/src/components/Tools/Forms/FormDefaultComponent';
import FormImgComponent from '~/src/components/Tools/Forms/FormImgComponent';
import { Hub, User } from '~/src/types';
import FormSkillsComponent from '../Tools/Forms/FormSkillsComponent';
import isMandatoryField from '~/src/utils/isMandatoryField';
import FormMissingFieldsList from '../Tools/Forms/FormMissingFieldsList';
import DatePicker from 'react-datepicker';
import TitleInfo from '../Tools/TitleInfo';
import FormTimezonePicker from '../Tools/Forms/FormTimezonePicker';
import Button from '../primitives/Button';
import tw from 'twin.macro';
import { ITimezoneOption } from 'react-timezone-select';
import { isKnownUrl } from '~/src/utils/isKnownUrl';
import { AddEventMeetingLink, AddEventPhysicalLocation, DisplayEventInfo, RenderHostInfo } from './EventRenderUtils';
import { isValidHttpUrl } from '~/src/utils/utils';
import EventInvites from './EventInvites';
import { Event, EventInvite } from '~/src/types/event';
import { EventPrivacySettings } from './EventPrivacySettings';
import Icon from '../primitives/Icon';
import { AdvancedEditor } from '../tiptap/advancedEditor/advancedEditor';
import { Card, Flex } from 'antd';
import { WorkspaceModel } from '~/__generated__/types';

interface Props {
  event: Event;
  parent: WorkspaceModel | Hub;
  parentMembers: User[];
  existingEvent: boolean;
  stateValidation: any;
  showErrorList: boolean;
  handleChange: (key: any, content: any) => void;
  handleAttendancesToUpsertChange: (newArray: EventInvite[]) => void;
}

const EventForm: FC<Props> = ({
  event,
  parent,
  parentMembers,
  existingEvent,
  stateValidation,
  showErrorList,
  handleChange,
  handleAttendancesToUpsertChange,
}) => {
  const { t } = useTranslation('common');
  const { valid_title, valid_start, valid_end, valid_timezone } = stateValidation || {};

  const [startDate, setStartDate] = useState(new Date(event?.start));
  const [endDate, setEndDate] = useState(new Date(event?.end));
  const [timeDifference, setTimeDifference] = useState<number>(60 * 60 * 1000);

  const [expandAdditionalInfo, setExpandAdditionalInfo] = useState<boolean>(false);
  const [expandEventVisibility, setExpandEventVisibility] = useState<boolean>(false);

  const customTitleColor = '#000000';

  const handleDateChange = (key: string, date: any) => {
    if (key === 'start') {
      const newTimeDifference = startDate && endDate ? endDate?.getTime() - startDate?.getTime() : timeDifference;

      //If start date has been deleted, set only the start date and timeDifference (although this should still be the same)
      if (!date) {
        setStartDate(date);
        setTimeDifference(newTimeDifference);
      } else {
        date.setSeconds(0, 0);
        //Else, update the end date with the same time difference there was before
        setStartDate(date);
        setEndDate(new Date(date?.getTime() + newTimeDifference));
        setTimeDifference(newTimeDifference);

        handleChange('end', new Date(date?.getTime() + newTimeDifference));
      }
    } else {
      if (date) {
        date.setSeconds(0, 0);
      }

      setEndDate(date);
    }
    handleChange(key, date);
  };

  const handleLocationChange = (place: google.maps.places.PlaceResult) => {
    handleChange('location', {
      name: place?.name,
      url: place?.url,
    });
  };

  const handleTimezoneChange = (timezone: ITimezoneOption) => {
    handleChange('timezone', {
      value: timezone.value,
      label: timezone.label,
    });
  };

  const handleMeetingLinkChange = (link: string) => {
    const normalizedLink = isValidHttpUrl(link) ? link : `https://${link}`;
    handleChange('meeting_url', normalizedLink);
  };

  const handleVisibilityChange = (newVisibility: string) => {
    handleChange('visibility', newVisibility);
  };

  const filterTime = (time) => {
    const selectedDate = new Date(time);
    return startDate?.getTime() < selectedDate?.getTime();
  };

  return (
    <form tw="flex flex-col gap-7">
      {showErrorList && <FormMissingFieldsList stateValidation={stateValidation} />}

      <SectionWrapper>
        <FormDefaultComponent
          id="title"
          content={event?.title}
          title={t('title')}
          customTitleColor={customTitleColor}
          icon="material-symbols:title"
          placeholder={t('anAwesomeEvent')}
          onChange={handleChange}
          mandatory={isMandatoryField('event', 'title')}
          errorCodeMessage={valid_title ? valid_title.message : ''}
          isValid={valid_title ? !valid_title.isInvalid : undefined}
          maxChar={120}
        />
      </SectionWrapper>

      <SectionWrapper>
        <div tw="flex gap-2 mb-4 flex-wrap lg:flex-nowrap">
          <div tw="w-full">
            <FieldTitleWithIcon
              customTitleColor={customTitleColor}
              icon="uiw:date"
              titleId={t('legacy.formValidator.start')}
              isMandatory={isMandatoryField('event', 'start')}
            />

            <DatePicker
              id="start"
              onChange={(date) => handleDateChange('start', date)}
              selected={startDate}
              dateFormat="EEEE, MMMM d, yyyy h:mm aa"
              showTimeSelect
              timeIntervals={15}
              selectsStart
              startDate={startDate}
              endDate={endDate}
              wrapperClassName="event-date-picker"
              tw="rounded-md border border-gray-300"
              css={[valid_start?.isInvalid && tw`border-danger focus:(ring-danger border-danger)`]}
              placeholderText={t('eventStartingDate')}
              isClearable
            />
            {valid_start?.isInvalid && <span tw="text-[#DC3545]">{t(valid_start?.message)}</span>}
          </div>

          <div tw="w-full">
            <FieldTitleWithIcon
              customTitleColor={customTitleColor}
              icon="uiw:date"
              titleId={t('legacy.formValidator.end')}
              isMandatory={isMandatoryField('event', 'end')}
            />

            <DatePicker
              id="end"
              onChange={(date) => handleDateChange('end', date)}
              selected={endDate}
              dateFormat="EEEE, MMMM d, yyyy h:mm aa"
              showTimeSelect
              timeIntervals={15}
              selectsEnd
              startDate={startDate}
              endDate={endDate}
              minDate={startDate}
              filterTime={filterTime}
              wrapperClassName="event-date-picker"
              tw="rounded-md border border-gray-300"
              css={[valid_end?.isInvalid && tw`border-danger focus:(ring-danger border-danger)`]}
              placeholderText={t('eventEndingDate')}
              isClearable
            />
            {valid_end?.isInvalid && <span tw="text-[#DC3545]">{t(valid_end?.message)}</span>}
          </div>
        </div>

        <div tw="mb-4">
          <FormTimezonePicker
            customTitleColor={customTitleColor}
            tw="w-full"
            defaultValue={event?.timezone}
            handleChange={handleTimezoneChange}
            mandatory={isMandatoryField('event', 'timezone')}
            errorCodeMessage={valid_timezone ? valid_timezone.message : ''}
            icon="mdi:timezone-outline"
          />
        </div>
      </SectionWrapper>

      <SectionWrapper>
        <div tw="mb-4 flex flex-col flex-wrap lg:flex-nowrap gap-2">
          <FieldTitleWithIcon
            customTitleColor={customTitleColor}
            icon="mdi:location-on-outline"
            titleId={t('location')}
            isMandatory={false}
          />

          <div tw="flex flex-col gap-2 text-[#868686]">
            {/* Meeting Link*/}
            <span tw="text-sm text-[#000000] font-medium">{t('online')}</span>
            <div tw="flex gap-6 flex-wrap items-center">
              <div tw="flex flex-col gap-4">
                <div tw="text-sm flex items-center w-max cursor-pointer">
                  <input
                    tw="size-3 text-[#858585]"
                    type="radio"
                    checked={!event?.generate_meet_link}
                    onChange={() => handleChange('generate_meet_link', false)}
                  />

                  <AddEventMeetingLink
                    existingLink={event?.meeting_url}
                    handleChange={(link) => handleMeetingLinkChange(link)}
                  />
                </div>

                <div tw="flex flex-col">
                  <div tw="text-sm flex items-center h-9">
                    <input
                      tw="size-3 text-[#858585] cursor-pointer"
                      type="radio"
                      checked={event?.generate_meet_link}
                      onChange={() => handleChange('generate_meet_link', true)}
                    />
                    {!event?.generate_meet_link && t('generateGoogleMeet')}

                    {event?.generate_meet_link &&
                      (event?.generated_meeting_url ? (
                        <DisplayEventInfo
                          icon="system-uicons:video"
                          info={event?.generated_meeting_url}
                          infoLink={event?.generated_meeting_url}
                          knownUrl={isKnownUrl(event?.generated_meeting_url)}
                          handleClear={() => handleChange('generate_meet_link', false)}
                        />
                      ) : (
                        <Button tw="flex gap-1 h-9 border-gray-300 items-center md:w-[500px] bg-white" disabled>
                          <img src={'/images/icons/links-googlemeet.png'} tw="w-5 h-5 min-w-[20px] min-h-[20px]" />
                          <span tw="text-black line-clamp-1">{t('linkGeneratedAfterTheCreationOfEvent')}</span>
                        </Button>
                      ))}
                  </div>

                  <span tw="text-xs">{t('googleMeetDisclaimer')}</span>
                </div>
              </div>
            </div>

            {/* Physical location */}
            <span tw="text-sm text-[#000000] font-medium">{t('onSite')}</span>
            <AddEventPhysicalLocation
              existingLocation={event?.location?.name}
              handleChange={(place) => handleLocationChange(place)}
            />
          </div>
        </div>
      </SectionWrapper>

      <SectionWrapper>
        <ExpandableSection
          titleId="additionalInformation"
          isExpanded={expandAdditionalInfo}
          handleClick={() => setExpandAdditionalInfo(!expandAdditionalInfo)}
          icon="mdi:information-outline"
        />

        <div css={!expandAdditionalInfo && tw`hidden`}>
          <FormImgComponent
            id="banner_id"
            type="banner"
            title={t('eventBanner')}
            customTitleColor={customTitleColor}
            itemType="events"
            itemId={event?.id ?? 'event'}
            imageUrl={event?.banner_url}
            defaultImg="/images/default/default-event.png"
            showAddButton={event?.banner_url === null}
            onChange={handleChange}
            maxSizeFile={4194304}
            mandatory={isMandatoryField('event', 'banner_id')}
            icon="tdesign:image"
          />

          <Flex vertical gap="small">
            <Flex>
              <Icon icon="cil:paragraph" tw="w-5 h-5 m-0" />
              <TitleInfo title={t('about')} customTitleColor={customTitleColor} />
            </Flex>

            <Card size="small">
              <AdvancedEditor
                content={event?.description}
                defaultPlaceholder={t('describeYourEventInDetail')}
                handleChange={(content) => {
                  handleChange('description', content);
                }}
              />
            </Card>
          </Flex>

          <div tw="mb-4">
            <FormSkillsComponent
              id="keywords"
              type="keywords"
              placeholder={t('skillsPlaceholder')}
              title={t('keyword.other')}
              customTitleColor={customTitleColor}
              showHelp={false}
              content={event?.keywords}
              mandatory={false}
              onChange={handleChange}
              icon="mdi:hashtag"
            />
          </div>
        </div>
      </SectionWrapper>

      <SectionWrapper>
        <ExpandableSection
          titleId="visibilitySettings"
          isExpanded={expandEventVisibility}
          handleClick={() => setExpandEventVisibility(!expandEventVisibility)}
          icon="mdi:eye-outline"
        />

        <div tw="flex flex-col gap-2" css={!expandEventVisibility && tw`hidden`}>
          <RenderHostInfo host={parent} />
          <EventPrivacySettings
            parentContainerName={parent.title}
            parentContainerPrivacy={parent?.content_privacy}
            selectedPrivacy={event.visibility}
            handleSelect={handleVisibilityChange}
          />
        </div>
      </SectionWrapper>

      <SectionWrapper>
        <EventInvites
          event={event}
          existingEvent={existingEvent}
          parent={parent}
          parentMembers={parentMembers}
          handleAttendancesToUpsertChange={handleAttendancesToUpsertChange}
        />
      </SectionWrapper>
    </form>
  );
};

export default EventForm;

const SectionWrapper: FC<{ children: ReactNode }> = ({ children }) => (
  <div tw="p-2 rounded-lg shadow-custom">{children}</div>
);

interface ExpandableSectionProps {
  icon?: string;
  titleId: string;
  isExpanded: boolean;
  handleClick: () => void;
}

const ExpandableSection: FC<ExpandableSectionProps> = ({ icon, titleId, isExpanded, handleClick }) => {
  const { t } = useTranslation('common');

  return (
    <div tw="flex gap-2 items-center" css={isExpanded && tw`mb-4`}>
      <Icon icon={icon} tw="w-6 h-6 m-0" />
      <h5 tw="m-0 font-semibold">{t(titleId)}</h5>
      <Icon
        icon={isExpanded ? 'bxs:up-arrow' : 'bxs:down-arrow'}
        tw="w-4 h-4 m-0 cursor-pointer hover:(opacity-50)"
        onClick={handleClick}
      />
    </div>
  );
};

interface FieldTitleWithIconProps {
  titleId: string;
  icon: string;
  isMandatory: boolean;
  customTitleColor: string;
}

const FieldTitleWithIcon: FC<FieldTitleWithIconProps> = ({ titleId, icon, isMandatory, customTitleColor }) => (
  <div tw="flex gap-2 items-center">
    <Icon icon={icon} tw="w-5 h-5 m-0" />
    <TitleInfo customTitleColor={customTitleColor} title={titleId} mandatory={isMandatory} />
  </div>
);
