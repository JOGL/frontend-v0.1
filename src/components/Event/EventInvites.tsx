import { FC } from 'react';
import { Hub, User } from '~/src/types';
import EventGuestInfo from './EventGuestInfo';
import LimitedGuestInfo from './LimitedGuestInfo';
import { Event, EventInvite } from '~/src/types/event';
import { WorkspaceModel } from '~/__generated__/types';

interface Props {
  event: Event;
  existingEvent: boolean;
  parent: WorkspaceModel | Hub;
  parentMembers: User[];
  handleAttendancesToUpsertChange: (newInvites: EventInvite[]) => void;
}

const EventInvites: FC<Props> = ({ event, existingEvent, parent, parentMembers, handleAttendancesToUpsertChange }) => {
  return (
    <div>
      {existingEvent ? (
        //If the event is already created, display the actual guest info with all options available
        <EventGuestInfo event={event} />
      ) : (
        //If the event is being created, display a limited version of what EventGuestInfo will look like after
        <LimitedGuestInfo
          event={event}
          parent={parent}
          handleAttendancesToUpsertChange={handleAttendancesToUpsertChange}
          parentMembers={parentMembers}
        />
      )}
    </div>
  );
};

export default EventInvites;
