import { CSSProperties, FC, useState } from 'react';
import Icon from '../primitives/Icon';
import Button from '../primitives/Button';
import { confAlert } from '~/src/utils/utils';
import { useApi } from '~/src/contexts/apiContext';
import AsyncSelect from 'react-select/async';
import { components, ControlProps } from 'react-select';
import styled from '~/src/utils/styled';
import { Organization } from '~/src/types';
import useTranslation from 'next-translate/useTranslation';

interface OrganizationAttachRequest {
  itemId: string;
  itemType: string;
  alreadyAttachedOrganizations: Organization[];
  callBack: () => void;
  showModal: () => void;
}

const OrganizationAttachRequest: FC<OrganizationAttachRequest> = ({
  itemId,
  itemType,
  alreadyAttachedOrganizations,
  callBack,
  showModal,
}) => {
  const api = useApi();
  const { t } = useTranslation('common');
  const [organizationsList, setOrganizationsList] = useState([]);
  const [sending, setSending] = useState(false);

  const searchOrganization = async (resolve: (value: unknown) => void, value: string) => {
    try {
      let content = (await api.get(`/organizations?Search=${value}&Page=1&PageSize=5`))?.data?.items ?? [];
      content = content
        // filter organizations that were already added
        .filter((x) => !alreadyAttachedOrganizations?.map((x) => x.id).includes(x.id))
        .map((organization) => {
          return {
            value: organization.id,
            label: organization.title,
            logo_url: organization.banner_url_sm || '/images/default/default-organization.png',
          };
        });
      resolve(content);
    } catch (e) {
      resolve([]);
    }
  };

  const loadOptions = (inputValue) =>
    new Promise((resolve) => {
      searchOrganization(resolve, inputValue);
    });

  const inviteOrganization = async (organizationId: string) => {
    await api.post(`/${itemType}/${itemId}/communityEntityInvite/${organizationId}`);
  };

  const handleChangeOrganization = (content) => {
    const tempOrganizationsList = [];
    content?.map(function (user) {
      user && tempOrganizationsList.push(user.value);
    });
    setOrganizationsList(tempOrganizationsList);
  };

  const formatOptionLabel = ({ label, logo_url }) => (
    <OrganizationLabel tw="inline-flex items-center">
      <img src={logo_url} />
      <div>{label}</div>
    </OrganizationLabel>
  );

  const handleSubmit = () => {
    const sentInvitation = [];
    setSending(true);
    for (const item of organizationsList) {
      sentInvitation.push(inviteOrganization(item));
    }
    Promise.all(sentInvitation)
      .then(() => {
        confAlert.fire({ icon: 'success', title: `Request sent` });
        callBack();
      })
      .catch((err) => {
        confAlert.fire({
          icon: 'error',
          title:
            err.response.status === 409
              ? t('thisItemIsPendingAdminApproval', { item: t('organization.one') })
              : t('someUnknownErrorOccurred'),
        });
      })
      .finally(() => {
        setSending(false);
      });
  };

  const customStyles: Record<string, (css: CSSProperties) => CSSProperties> = {
    container: (provided) => ({
      ...provided,
      width: '100%',
    }),
    control: (css) => ({
      ...css,
      position: 'relative',
      borderRadius: '4px',
      minHeight: '10px',
    }),
    valueContainer: (css) => ({
      ...css,
    }),
    multiValue: (css) => ({
      ...css,
      '> div': {
        display: 'flex',
        alignItems: 'center',
      },
    }),
  };

  const Control = ({ children, ...props }: ControlProps<any, false>) => {
    return (
      <components.Control {...props}>
        <div tw="bg-[#D0D0D0] p-1 rounded-tl-sm rounded-bl-sm items-center flex justify-center h-[38px] w-[38px]">
          <Icon icon="mdi:at" tw="h-5 w-5 text-[#5959599e] mr-0" />
        </div>
        {children}
      </components.Control>
    );
  };

  return (
    <div>
      <div tw="mb-4">
        <label tw="font-bold" htmlFor="joglUser">
          {t('joglOrganizations')}
        </label>
        <AsyncSelect
          isMulti
          cacheOptions
          styles={customStyles}
          onChange={handleChangeOrganization}
          formatOptionLabel={formatOptionLabel}
          placeholder={t('typeToSearchItem', { item: t('organization.other') })}
          loadOptions={loadOptions}
          noOptionsMessage={() => null}
          components={{ DropdownIndicator: null, Control, IndicatorSeparator: null }}
          isClearable
        />
      </div>
      <Button onClick={handleSubmit} disabled={organizationsList.length === 0 || sending} className="invite-btn">
        {t('request')}
      </Button>
    </div>
  );
};

const OrganizationLabel = styled.div`
  div {
    font-size: 15px;
    color: #5c5d5d;
  }
  img {
    width: 28px;
    height: 28px;
    border-radius: 50%;
    object-fit: cover;
    margin-right: 6px;
  }
`;

export default OrganizationAttachRequest;
