import React, { FC } from 'react';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';

interface Props {
  description: string;
}

const OrganizationAbout: FC<Props> = ({ description }) => {
  return (
    <div tw="pb-4 md:pb-0">
      <InfoHtmlComponent content={description} />
    </div>
  );
};
export default OrganizationAbout;
