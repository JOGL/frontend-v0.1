import { TabPanels, TabPanel as SpecialTabPanel, Tabs } from '@reach/tabs';
import ObjectAdminCard from '~/src/components/Tools/ObjectAdminCard';
import Loading from '~/src/components/Tools/Loading';
import { NavTab, TabListStyleNoSticky } from '~/src/components/Tabs/TabsStyles';
import { FC } from 'react';
import Button from '../primitives/Button';
import useTranslation from 'next-translate/useTranslation';
import { useModal } from '~/src/contexts/modalContext';
import { useRouter } from 'next/router';
import { Organization, ItemType } from '~/src/types';
import OrganizationAttachRequest from './OrganizationAttachRequest';
import NoResults from '../Tools/NoResults';

interface Props {
  childId: string;
  childType: ItemType;
  organizations: Organization[];
  organizationsmutate: () => void;
  incomingObjInvites: Organization[];
  incomingObjmutate: () => void;
  outgoingObjInvites: Organization[];
  outgoingObjmutate: () => void;
}

const OrganizationManageAffiliation: FC<Props> = ({
  childId,
  childType,
  organizations,
  organizationsRevalidate,
  incomingObjInvites,
  incomingObjRevalidate,
  outgoingObjInvites,
  outgoingObjRevalidate,
}) => {
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const router = useRouter();

  const OrganizationAttachRequestButton = () => (
    <Button
      onClick={() => {
        showModal({
          children: (
            <OrganizationAttachRequest
              showModal={showModal}
              alreadyAttachedOrganizations={organizations}
              itemType={childType}
              itemId={childId}
              callBack={() => {
                closeModal();
                outgoingObjRevalidate();
              }}
            />
          ),
          title: t('linkRequestToOrganization'),
        });
      }}
      tw="mb-6"
    >
      {t('linkRequestToOrganization')}
    </Button>
  );

  return (
    <Tabs>
      <TabListStyleNoSticky>
        <NavTab>{t('organization', { count: 2 })}</NavTab>
        <NavTab>
          <div tw="inline-flex items-center justify-center">
            {t('outgoingRequests')}
            <span tw="bg-[#E2E2E2] rounded-full w-7 h-7 ml-2">
              <span tw="flex justify-center">
                {outgoingObjInvites?.filter((x) => x.type === 'organization')?.length}
              </span>
            </span>
          </div>
        </NavTab>
        <NavTab>
          <div tw="inline-flex items-center justify-center">
            {t('incomingRequests')}
            <span tw="bg-[#E2E2E2] rounded-full w-7 h-7 ml-2">
              <span tw="flex justify-center">
                {incomingObjInvites?.filter((x) => x.type === 'organization').length}
              </span>
            </span>
          </div>
        </NavTab>
      </TabListStyleNoSticky>
      <TabPanels tw="justify-center">
        {/* Affiliated organizations */}
        <SpecialTabPanel>
          {router.query.id ? (
            <div tw="flex flex-col pb-6">
              <div tw="content-end">
                <OrganizationAttachRequestButton />
                {organizations?.map((organization, i) => (
                  <ObjectAdminCard
                    key={i}
                    object={organization}
                    objUrl={`/organization/${organization.id}`}
                    parentType={childType}
                    parentId={childId}
                    callBack={() => {
                      incomingObjRevalidate();
                      organizationsRevalidate();
                    }}
                  />
                ))}
              </div>
            </div>
          ) : null}
        </SpecialTabPanel>
        {/* Outgoing organizations requests/invites */}
        <SpecialTabPanel>
          {router.query.tab && (
            <div>
              <OrganizationAttachRequestButton />
              {outgoingObjInvites?.filter((x) => x.type === 'organization')?.length === 0 && <NoResults />}
              {outgoingObjInvites?.filter((x) => x.type === 'organization') ? (
                <>
                  {outgoingObjInvites
                    ?.filter((x) => x.type === 'organization')
                    ?.map((organization, i) => (
                      <ObjectAdminCard
                        key={i}
                        object={organization}
                        objUrl={`/organization/${organization.id}`}
                        parentType={childType}
                        parentId={childId}
                        callBack={() => {
                          outgoingObjRevalidate();
                          organizationsRevalidate();
                        }}
                        type="invite"
                      />
                    ))}
                </>
              ) : (
                <Loading />
              )}
            </div>
          )}
        </SpecialTabPanel>
        {/* Incoming organizations requests */}
        <SpecialTabPanel>
          {router.query.tab && (
            <div>
              {incomingObjInvites?.filter((x) => x.type === 'organization')?.length === 0 && <NoResults />}
              {incomingObjInvites?.filter((x) => x.type === 'organization') ? (
                <>
                  {incomingObjInvites
                    ?.filter((x) => x.type === 'organization')
                    ?.map((organization, i) => (
                      <ObjectAdminCard
                        key={i}
                        object={organization}
                        objUrl={`/organization/${organization.id}`}
                        parentType={childType}
                        parentId={childId}
                        callBack={() => {
                          incomingObjRevalidate();
                          organizationsRevalidate();
                        }}
                        type="request"
                      />
                    ))}
                </>
              ) : (
                <Loading />
              )}
            </div>
          )}
        </SpecialTabPanel>
      </TabPanels>
    </Tabs>
  );
};

export default OrganizationManageAffiliation;
