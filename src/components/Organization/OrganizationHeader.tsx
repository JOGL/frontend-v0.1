/* eslint-disable camelcase */
import Icon from '~/src/components/primitives/Icon';
import useTranslation from 'next-translate/useTranslation';
import React, { FC, useState } from 'react';
import H1 from '~/src/components/primitives/H1';
import Button from '~/src/components/primitives/Button';
import { Organization } from '~/src/types';
import ShareBtns from '~/src/components/Tools/ShareBtns/ShareBtns';
import OrganizationInfo from './OrganizationInfo';
import BtnJoin from '~/src/components/Tools/BtnJoin';
import { useRouter } from 'next/router';
import { useModal } from '~/src/contexts/modalContext';
import { isAdmin, isMember, isOwner, linkify } from '~/src/utils/utils';
import useGet from '~/src/hooks/useGet';
import { useApi } from '~/src/contexts/apiContext';
import { Menu, MenuItem, MenuButton, MenuList } from '@reach/menu-button';
import BtnAdd from '../Tools/BtnAdd';
import { securityOptionsOrganization } from '~/src/utils/security_options';
import Link from 'next/link';
import InfoDefaultComponent from '../Tools/Info/InfoDefaultComponent';
import { ContainerPrivacyAndSecurityInfo } from '../Tools/ContainerPrivacyAndSecurityInfo';
import { usePushNotificationStore } from '~/src/store/push-notifications/PushNotificationsStoreProvider';

interface Props {
  organization: Organization;
  limitedVisibility: boolean;
  refresh?: () => void;
}

const OrganizationHeader: FC<Props> = ({ organization, limitedVisibility, refresh }) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const { showModal, closeModal } = useModal();
  const [hasAcceptedOrRejectedInvite, setHasAcceptedOrRejectedInvite] = useState(false);
  const api = useApi();
  const { showPushNotificationModal } = usePushNotificationStore((s) => ({
    showPushNotificationModal: s.showPushNotificationModal,
  }));

  const { data: pending_invite, mutate: refreshInvites } = useGet(
    organization.user_joining_restriction_level === 'visitor' || organization.user_access_level === 'pending'
      ? `/organizations/${organization.id}/invitation`
      : ''
  );

  const leaveObject = () => {
    api.post(`/organizations/${organization.id}/leave`).then((res) => {
      res.status === 200 && location.reload();
    });
  };

  const acceptOrRejectInvite = (e, type) => {
    // if (itemId && itemType && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
    if ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      // setSending(true);
      const route = `/organizations/${organization.id}/invites/${pending_invite?.invitation_id}/${type}`;
      // const route = `/${itemType}/${itemId}/invites/${pending_invite.invitation_id}/${po}`
      api
        .post(route)
        .then((res) => {
          refreshInvites();
          setHasAcceptedOrRejectedInvite(true);
          // refresh page if accepting
          if (type === 'accept') {
            showPushNotificationModal(router);
            location.reload();
          }
        })
        .catch((err) => {
          // console.error(`Couldn't PUT ${itemType} with itemId=${itemId} and action=${action}`, err);
          // setSending(false);
        });
    }
  };

  const { id, title, short_description } = organization;

  const displayExtraActionButtons = () => (
    <div tw="gap-2 md:gap-1 flex">
      <Icon
        icon="majesticons:scroll-text-line"
        tw="w-8 h-8 border-gray-200 border p-1 rounded-full cursor-pointer hover:bg-gray-200"
        onClick={() => {
          showModal({
            children: (
              <ContainerPrivacyAndSecurityInfo
                object={organization}
                limitedVisibility={limitedVisibility}
                securityOptions={securityOptionsOrganization}
              />
            ),
            title: t('privacyAndSecurity'),
            maxWidth: '60rem',
          });
        }}
      />
      <Menu>
        <MenuButton tw="text-[.8rem] text-gray-700 hover:bg-gray-200 rounded-full border-gray-200 border w-8 h-8 mx-auto">
          •••
        </MenuButton>
        <MenuList className="post-manage-dropdown">
          <ShareBtns type="organization" specialObjId={id} isActionMenu />
          {organization.user_access_level !== 'visitor' &&
            organization.user_access_level !== 'pending' &&
            !isOwner(organization) && (
              <MenuItem
                onSelect={() => {
                  showModal({
                    children: (
                      <div tw="inline-flex space-x-3">
                        <Button btnType="danger" onClick={leaveObject}>
                          {t('yes')}
                        </Button>
                        <Button onClick={closeModal}>{t('no')}</Button>
                      </div>
                    ),
                    title: t('areYouSureYouWantToLeaveThisContainer', { container: t('organization.one') }),
                    maxWidth: '30rem',
                  });
                }}
              >
                <Icon icon="pepicons-pop:leave" />
                {t('action.leave')}
              </MenuItem>
            )}
        </MenuList>
      </Menu>
    </div>
  );

  const LogoImgComponent = () => (
    <img
      src={organization.logo_url || '/images/default/default-organization-logo.png'}
      alt="organization logo"
      tw="mx-auto w-20 h-20 self-center object-contain bg-white rounded-full shadow-custom sm:(w-28 h-28 mt-5) md:(w-36 h-36)"
    />
  );

  return (
    <header tw="relative w-full md:border-b border-gray-200">
      <div tw="flex flex-wrap md:divide-x divide-gray-200 md:flex-nowrap">
        <div tw="max-md:hidden">
          <LogoImgComponent />
          <OrganizationInfo organization={organization} limitedVisibility={limitedVisibility} />
        </div>

        <div tw="px-4 py-4 md:px-8 md:pt-6 w-full">
          <div tw="inline-flex items-center mb-1 justify-between w-full gap-3">
            <div tw="inline-flex items-center max-md:gap-3">
              <div tw="md:hidden flex-none">
                <LogoImgComponent />
              </div>
              <H1 tw="text-[1.7rem] md:text-[2rem]">{title}</H1>
            </div>
            <span tw="max-md:hidden">{displayExtraActionButtons()}</span>
          </div>

          <div tw="text-gray-600 mt-4">
            <InfoDefaultComponent content={linkify(short_description)} containsHtml />
          </div>

          <div tw="flex items-center gap-2 flex-wrap">
            {isMember(organization) && <BtnAdd container={organization} type="organization" refresh={refresh} />}
            {isAdmin(organization) && (
              <Link href={`/organization/${organization.id}/edit`}>
                <Button btnType="secondary">
                  <Icon icon="bxs:edit" tw="w-5 h-5" />
                  {t('action.manage')}
                </Button>
              </Link>
            )}
            <span tw="md:hidden">{displayExtraActionButtons()}</span>
          </div>

          <div tw="flex flex-wrap gap-3 mb-2">
            {/* Join btn */}
            {!hasAcceptedOrRejectedInvite && pending_invite && pending_invite.invitation_type !== 'request' ? (
              <div tw="flex px-6 py-3 border border-gray-300 border-solid rounded-md text-gray-500 items-center">
                {t('youHaveBeenInvitedToJoinThisContainer', { container: t('organization.one') })}
                <div tw="space-x-2 pl-5">
                  <Button
                    tw="text-green-500 border-green-500 bg-white hover:(text-white bg-green-500)"
                    // disabled={sending}
                    onClick={(e) => acceptOrRejectInvite(e, 'accept')}
                  >
                    {/* {sending && <SpinLoader />} */}
                    {t('action.accept')}
                  </Button>
                  <Button
                    tw="text-danger border-danger bg-white hover:(text-white bg-danger)"
                    // disabled={sending === 'remove'}
                    onClick={(e) => acceptOrRejectInvite(e, 'reject')}
                  >
                    {/* {sending === 'remove' && <SpinLoader />} */}
                    {t('action.reject')}
                  </Button>
                </div>
              </div>
            ) : (
              (organization.user_access_level === 'visitor' || organization.user_access_level === 'pending') &&
              organization.joining_restriction !== 'invite' &&
              (!organization.onboarding?.enabled || organization.user_access_level === 'pending') && (
                <BtnJoin
                  joinType={organization.joining_restriction}
                  isPrivate={organization.content_privacy === 'private'}
                  itemType="organizations"
                  itemId={organization.id}
                  hasNoStat={false}
                  textJoin={organization.joining_restriction === 'open' ? t('action.join') : t('action.requestToJoin')}
                  count={organization.stats.members_count}
                  pending_invite={pending_invite}
                  showMembersModal={() =>
                    router.push(`/organization/${router.query.id}?tab=members`, undefined, { shallow: true })
                  }
                />
              )
            )}

            {/* Follow btn */}
            {/* <BtnFollow
              followState={organization.user_follows}
              itemType="organizations"
              itemId={organization.id}
              count={organization.followers_count}
            /> */}
          </div>
        </div>
      </div>
    </header>
  );
};

export default OrganizationHeader;
