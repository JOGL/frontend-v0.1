/* eslint-disable camelcase */
import Link from 'next/link';
import React from 'react';
import ObjectCard from '~/src/components/Cards/ObjectCard';
import H2 from '~/src/components/primitives/H2';
import Title from '~/src/components/primitives/Title';
import { TextWithPlural } from '~/src/utils/managePlurals';
import { displayObjectRelativeDate } from '~/src/utils/utils';
import tw from 'twin.macro';
import { ImageChip, TextChip } from '~/src/types/chip';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';

interface Props {
  id: string;
  title: string;
  short_title: string;
  short_description?: string;
  members_count?: number;
  activitiesCount?: number;
  banner_url: string;
  width?: string;
  cardFormat?: string;
  status?: string;
  chip?: TextChip;
  textChips?: TextChip[];
  imageChips?: ImageChip[];
  last_activity?: string;
}
const OrganizationCard = ({
  id,
  title,
  short_title,
  short_description,
  members_count,
  banner_url,
  width,
  cardFormat,
  status,
  chip,
  textChips = [],
  imageChips = [],
  last_activity,
}: Props) => {
  const { t } = useTranslation('common');
  const { locale } = useRouter();
  const organizationUrl = `/organization/${id}/${short_title}`;

  return (
    <ObjectCard
      imgUrl={banner_url || '/images/default/default-organization.png'}
      href={organizationUrl}
      width={width}
      chip={chip}
      textChips={status === 'draft' ? [{ name: 'draft' }, ...textChips] : textChips}
      imageChips={imageChips}
      cardFormat={cardFormat}
    >
      {/* Title */}
      <div tw="inline-flex items-center" css={cardFormat !== 'inPosts' && tw`md:h-8`}>
        <Link href={organizationUrl} passHref legacyBehavior>
          <Title>
            <H2
              tw="break-words text-[16px] items-center"
              title={title || t('untitledItem', { item: t('organization.one') })}
              css={cardFormat !== 'inPosts' ? tw`md:line-clamp-2` : tw`md:line-clamp-1`}
            >
              {title || t('untitledItem', { item: t('organization.one') })}
            </H2>
          </Title>
        </Link>
      </div>

      {cardFormat !== 'inPosts' ? (
        <>
          <Hr tw="mt-2 pt-4" />
          {/* Description */}
          <div tw="[line-height:18px] h-24">
            <div tw="text-[#343a40] text-sm line-clamp-5">{short_description}</div>
          </div>
        </>
      ) : (
        <div tw="text-[#343a40] text-sm line-clamp-2 pt-2">{short_description}</div>
      )}
      {/* Stats */}
      {cardFormat !== 'inPosts' && (
        <>
          <Hr tw="pt-2 mt-5" />
          <div tw="items-center justify-around space-x-2 flex">
            <CardData value={members_count} title={<TextWithPlural type="member" count={members_count} />} />
            <CardData
              value={last_activity && displayObjectRelativeDate(last_activity, locale)}
              title={t('lastActivity')}
            />
          </div>
        </>
      )}
    </ObjectCard>
  );
};

const CardData = ({ value, title }) => (
  <div tw="flex flex-col justify-center items-center text-[13px]">
    <div tw="font-bold line-clamp-1">{value || '--'}</div>
    <div tw="text-gray-500 font-medium">{title}</div>
  </div>
);
const Hr = (props) => <div tw="-mx-4 border-0 border-t border-solid border-color[rgba(0, 0, 0, 0.07)]" {...props} />;

export default OrganizationCard;
