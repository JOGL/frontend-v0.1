import React, { useState, FC, CSSProperties, useMemo } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Button from '../primitives/Button';
import Select from 'react-select';
import Icon from '../primitives/Icon';
import { Hub, Organization, SelectContainerOption } from '~/src/types';
import Link from 'next/link';
import useGet from '~/src/hooks/useGet';
import Loading from '../Tools/Loading';
import { entityItem } from '~/src/utils/utils';
import { CommunityEntityMini } from '~/src/types/entity';

interface PropsModal {
  organization: Organization;
  userLogoUrl: string;
  userObjectsUrl: string;
  isAdmin: boolean;
  type: string;
  alreadyLinked: Hub[];
  onSubmitProps?: (containerId: string, containerName: string) => void;
  closeModal: () => void;
}

export const LinkToOrganization: FC<PropsModal> = ({
  organization,
  userLogoUrl,
  userObjectsUrl,
  isAdmin,
  type,
  alreadyLinked,
  onSubmitProps,
  closeModal,
}) => {
  const { t } = useTranslation('common');
  const [selectedContainer, setSelectedContainer] = useState<SelectContainerOption>(undefined);
  const [isFiltering, setIsFiltering] = useState<boolean>(false);
  const selectText = (selectType) => `Select ${selectType} to link to ${organization.title}`;
  const { data, isLoading: isLoadingObjects } = useGet<[]>(userObjectsUrl);

  type = 'nodes' === type ? 'hub' : type;

  const onSubmit = (e) => {
    if (onSubmitProps) {
      onSubmitProps(selectedContainer?.value, selectedContainer?.label);
      return;
    }
  };

  // Filter the objects that are already in this hub so you don't add it twice!
  const filteredObjects = useMemo(() => {
    setIsFiltering(true);

    const filteredData: SelectContainerOption[] = data
      ?.filter((obj) => alreadyLinked?.findIndex((c) => c?.id === obj?.id) === -1)
      .map((data: CommunityEntityMini) => {
        return {
          value: data.id,
          label: data.title,
          type: data.type,
          userAccessLevel: data?.user_access_level,
          imageUrl: data?.logo_url_sm ?? data?.banner_url_sm,
        };
      });

    setIsFiltering(false);
    return filteredData;
  }, [data]);

  const customClassNames: Record<string, () => string> = {
    menuList: () => 'show-scrollbar',
  };

  const customStyles: Record<string, (css: CSSProperties) => CSSProperties> = {
    container: (provided) => ({ ...provided, width: '100%' }),
    control: (provided) => ({
      ...provided,
    }),
    valueContainer: (provided) => ({
      ...provided,
      height: 50,
    }),
    singleValue: (provided) => ({
      ...provided,
      width: '-webkit-fill-available',
    }),
    menu: (provided) => ({
      ...provided,
      zIndex: 0,
      position: 'unset',
    }),
    menuList: (provided) => ({
      ...provided,
      maxHeight: 175,
    }),
  };

  const CustomOption = ({ value, label, type, imageUrl, userAccessLevel }) => {
    return (
      <div tw="flex gap-2.5 cursor-pointer">
        <div
          style={{
            backgroundImage: `url(${imageUrl || entityItem[type].default_banner})`,
          }}
          tw="bg-cover bg-center h-9 w-9 rounded-full border border-solid border-[#ced4da]"
        />
        <div tw="flex flex-col flex-auto self-center">
          <span tw="text-[14px] font-bold">{label || t('untitled')}</span>
          {/* <span tw="text-[14px] italic">{type}</span> */}
        </div>
        <div tw="flex flex-col w-14 items-center">
          <img
            src={userLogoUrl ?? '/images/default/default-user.png'}
            alt={label}
            tw="mx-auto w-5 h-5 self-center rounded-full shadow-custom"
          />{' '}
          <span tw="text-[14px] italic">{userAccessLevel}</span>
        </div>
      </div>
    );
  };

  return (
    <div tw="flex flex-col gap-5">
      <div>
        <div tw="flex flex-col flex-auto gap-4">
          <span tw="text-[14px] font-bold">
            "{organization.title}" and the {type} will be in each other's ecosystem.
          </span>
          <span tw="text-[14px]">
            It means that depending on the privacy settings of the organization, the members of your selected {type}{' '}
            will have access to more content from the organization; and depending on the privacy settings of your
            selected {type}, the organization members will have access to more content from your selected {type}.
          </span>
          <div>
            {isLoadingObjects || isFiltering ? (
              <Loading />
            ) : (
              <>
                <span tw="text-[14px] font-bold">
                  {selectText(type === 'organization' ? 'an organization' : `a ${type}`)}
                </span>
                <Select
                  options={filteredObjects}
                  menuShouldScrollIntoView={true} // force scroll into view
                  noOptionsMessage={() => `No ${type} to link`}
                  placeholder={`Type to search ${type}`}
                  onChange={(content) => setSelectedContainer(content)}
                  formatOptionLabel={CustomOption}
                  styles={customStyles}
                  classNames={customClassNames}
                  autoFocus
                  value={selectedContainer}
                />
              </>
            )}
          </div>
          {isAdmin && (
            <div tw="border border-[#D1D1D1] p-1.5">
              <span tw="text-[14px] text-[#5F5D5D]">
                <Icon icon="material-symbols:info-outline" />
                <Link
                  tw="underline text-inherit"
                  href={`/organization/${organization?.id}/edit?tab=privacy_security`}
                  target="_blank"
                >
                  Manage the privacy
                </Link>
                &nbsp;of the organization.
              </span>
            </div>
          )}
        </div>
      </div>

      <div className="btnZone" tw="flex flex-row gap-2 self-center">
        <Button btnType="secondary" onClick={closeModal}>
          <>{t('action.cancel')}</>
        </Button>
        <Button type="submit" onClick={onSubmit} disabled={!selectedContainer}>
          <>{t('action.link')}</>
        </Button>
      </div>
    </div>
  );
};
