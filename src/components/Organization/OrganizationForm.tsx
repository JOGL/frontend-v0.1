import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import FormDefaultComponent from '~/src/components/Tools/Forms/FormDefaultComponent';
import FormImgComponent from '~/src/components/Tools/Forms/FormImgComponent';
import { Organization } from '~/src/types';
import { generateSlug } from '~/src/utils/utils';
import FormSkillsComponent from '../Tools/Forms/FormSkillsComponent';
import FormInterestsComponent from '../Tools/Forms/FormInterestsComponent';
import isMandatoryField from '~/src/utils/isMandatoryField';
import FormTextAreaComponent from '../Tools/Forms/FormTextAreaComponent';
import FormMissingFieldsList from '../Tools/Forms/FormMissingFieldsList';
import Alert from '../Tools/Alert/Alert';

interface Props {
  organization: Organization;
  stateValidation: {};
  showErrorList: boolean;
  handleChange: (key: any, content: any) => void;
}

const OrganizationForm: FC<Props> = ({ organization, stateValidation, showErrorList, handleChange }) => {
  const { t } = useTranslation('common');

  const handleChangeOrganization = (key, content) => {
    if (key === 'title') {
      // generate a shortname when typing a title, and update short_name field with the value
      handleChange('short_title', generateSlug(content));
    }
    handleChange(key, content);
  };

  const handleChangeLogo = (key, content) => {
    handleChange('logo_url', content?.id ? `${process.env.ADDRESS_BACK}/images/${content?.id}/full` : null);
    handleChange('logo_url_sm', content?.id ? `${process.env.ADDRESS_BACK}/images/${content?.id}/tn` : null);
    handleChange('logo_id', content?.id ?? null);
  };

  const { valid_title, valid_keywords, valid_short_description } = stateValidation || {};

  return (
    <form className="organizationForm">
      {showErrorList && <FormMissingFieldsList stateValidation={stateValidation} />}

      {/* Show warning msg if organization status is draft */}
      {organization.status === 'draft' && <Alert type="warning" message={t('draftWarningMessage')} />}

      {/* Basic fields */}
      <div tw="flex gap-1">
        <div tw="mr-4">
          <FormImgComponent
            itemId={organization?.id}
            maxSizeFile={4194304}
            aspectRatio={1}
            isRounded
            itemType="users"
            type="avatar"
            id="logo_id"
            imageUrl={organization?.logo_url}
            defaultImg="/images/default/default-organization-logo.png"
            onChange={handleChangeLogo}
          />
        </div>

        <div tw="flex-1">
          <FormDefaultComponent
            id="title"
            content={organization.title}
            title={t('title')}
            placeholder={t('anAwesomeOrganization')}
            onChange={handleChangeOrganization}
            mandatory={isMandatoryField('organization', 'title')}
            isValid={valid_title ? !valid_title.isInvalid : undefined}
            errorCodeMessage={valid_title ? valid_title.message : ''}
            maxChar={120}
          />
        </div>
      </div>

      <FormImgComponent
        id="banner_id"
        type="banner"
        title={t('organizationBanner')}
        itemType="organizations"
        itemId={organization.id}
        imageUrl={organization.banner_url}
        showAddButton={organization?.banner_url === null}
        defaultImg="/images/default/default-organization.png"
        onChange={handleChangeOrganization}
      />

      <FormTextAreaComponent
        content={organization.short_description}
        id="short_description"
        maxChar={500}
        onChange={handleChange}
        rows={3}
        title={t('shortDescription')}
        placeholder={t('theOrganizationBrieflyExplained')}
        errorCodeMessage={valid_short_description ? valid_short_description.message : ''}
        isValid={valid_short_description ? !valid_short_description.isInvalid : undefined}
        mandatory={isMandatoryField('organization', 'short_description')}
      />

      <FormDefaultComponent
        id="address"
        content={organization.address}
        title={t('address.one')}
        placeholder={t('typeYourAddress')}
        onChange={handleChangeOrganization}
        mandatory={false}
      />

      <FormSkillsComponent
        id="keywords"
        type="keywords"
        placeholder={t('skillsPlaceholder')}
        title={t('keyword.other')}
        showHelp={true}
        content={organization?.keywords}
        mandatory={isMandatoryField('organization', 'keywords')}
        errorCodeMessage={valid_keywords ? valid_keywords.message : ''}
        onChange={handleChangeOrganization}
      />

      <FormInterestsComponent title={t('sdgs')} content={organization?.interests} onChange={handleChangeOrganization} />
    </form>
  );
};
export default OrganizationForm;
