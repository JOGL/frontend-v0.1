import React, { FC } from 'react';
import { Organization } from '~/src/types';
import InfoStats from '~/src/components/Tools/InfoStats';
import useTranslation from 'next-translate/useTranslation';
import Icon from '../primitives/Icon';

interface Props {
  organization: Organization;
  limitedVisibility: boolean;
}

const OrganizationInfo: FC<Props> = ({ organization, limitedVisibility }) => {
  const { t } = useTranslation('common');

  return (
    <div tw="flex-shrink-0 md:p-4 md:w-80 md:pt-6">
      <div tw="flex flex-wrap mb-2 justify-around md:mt-0">
        <InfoStats
          object="organization"
          type="member"
          tab="members"
          count={organization.stats.members_count}
          {...(limitedVisibility && { noLink: true })}
        />
      </div>

      {organization.address && (
        <>
          <hr tw="mb-4" />
          <div tw="flex items-center justify-start flex-wrap gap-2">
            <div>{t('location')}:</div>
            <div tw="inline-flex items-center">
              <Icon icon="humbleicons:location" tw="w-5 h-5 mb-1" />
              <span tw="font-bold line-clamp-2">{organization.address}</span>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default OrganizationInfo;
