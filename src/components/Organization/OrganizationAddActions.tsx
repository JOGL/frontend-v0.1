import useTranslation from 'next-translate/useTranslation';
import { useModal } from '~/src/contexts/modalContext';
import { ContainerPermissions, Hub, MenuActions, Organization, User } from '~/src/types';
import { membersAction, hubAction } from '~/src/utils/getContainerInfo';
import MembersAddModal from '../Members/MembersAddModal';
import useGet from '~/src/hooks/useGet';
import { LinkToOrganization } from './LinkToOrganization';
import { confAlert } from '~/src/utils/utils';
import { useApi } from '~/src/contexts/apiContext';

const OrganizationAddActions = (
  user: User,
  organization: Organization,
  permissions: ContainerPermissions,
  refresh: () => void
): MenuActions => {
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const api = useApi();

  const { data: alreadyLinkedHubs, mutate: refetchLinkedHubs } = useGet<Hub[]>(
    `/organizations/${organization?.id}/nodes`
  );

  const linkToOrganization = async (containerId: string, containerName: string, refetchLinked: () => void) => {
    await api
      .post(`/organizations/${organization?.id}/communityEntity/${containerId}/link`)
      .then(() => {
        refresh();
        refetchLinked();
        confAlert.fire({ icon: 'success', title: `${organization.title} is now linked to ${containerName}` });
      })
      .catch((err) => {
        confAlert.fire({ icon: 'error', title: `Couldn't link ${containerName} to organization.` });
        console.error("Couldn't link to hub", err);
      });

    closeModal();
  };

  return {
    members: {
      ...membersAction,
      disabled: !permissions?.canAddMembers ?? true,
      handleSelect: () => {
        showModal({
          children: <MembersAddModal itemType={'organizations'} itemId={organization?.id} />,
          title: t('inviteMembers'),
          maxWidth: '40rem',
        });
      },
    },
    content: undefined,
    containers: [
      {
        ...hubAction,
        disabled: !permissions?.canCreateHub ?? true,
        handleSelect: () => {
          showModal({
            children: (
              <LinkToOrganization
                organization={organization}
                type="nodes"
                userLogoUrl={user?.logo_url_sm ?? user?.logo_url}
                userObjectsUrl={`/users/${user?.id}/nodes?permission=manage`}
                isAdmin={permissions?.isAdmin}
                alreadyLinked={alreadyLinkedHubs}
                onSubmitProps={(containerId, containerName) => {
                  linkToOrganization(containerId, containerName, refetchLinkedHubs);
                }}
                closeModal={closeModal}
              />
            ),
            title: `Link a hub to ${organization?.title}`,
            maxWidth: '50rem',
          });
        },
      },
    ],
  };
};

export default OrganizationAddActions;
