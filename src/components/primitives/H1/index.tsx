const H1 = (props) => <h1 tw="font-black letter-spacing[-1pX] mb-0" {...props} />;

export default H1;
