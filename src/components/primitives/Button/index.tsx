import { CSSProperties, FC } from 'react';
import tw, { css, styled } from 'twin.macro';

interface Props {
  btnType: 'secondary' | 'primary' | 'danger';
  width: string;
  disabled: boolean;
  style?: CSSProperties;
}

const Container: FC<Props> = styled.button(({ btnType = 'primary', width, disabled }) => [
  tw`px-2 py-[.3rem] text-base`,
  tw`transition-colors duration-150`,
  btnType === 'secondary'
    ? tw`bg-white border-gray-300`
    : btnType === 'danger'
    ? tw`bg-danger border-danger`
    : tw`border-[#4a3292] bg-primary`,
  btnType === 'secondary' ? tw`text-gray-900` : tw`text-white`,
  tw`border rounded-full border-solid focus:(outline-none border-blue-300)`,
  // btnType === 'secondary' && !disabled && tw`hocus:(text-white bg-primary)`,
  btnType === 'primary' && !disabled && tw`hocus:(bg-[#4a3292] border-[#4a3292])`,
  btnType === 'secondary' && !disabled && tw`hocus:(text-gray-700 bg-gray-100 border-gray-400)`,
  btnType === 'danger' && !disabled && tw`hocus:bg-red-700`,
  // btnType !== 'secondary' && !disabled && tw`hover:opacity[.95]`,
  disabled ? tw`cursor-not-allowed opacity-60` : tw`cursor-pointer`,
  css`
    width: ${width || 'fit-content'};
  `,
]);
export default function Button(props) {
  return <Container style={props?.style} type={props.type || 'button'} {...props} />;
}
