import NextImage, { ImageProps } from 'next/image';

const Image = (props: ImageProps) => {
  return (
    <div tw="relative!">
      <NextImage fill quality={90} tw="relative!" {...props} />
    </div>
  );
};

export default Image;
