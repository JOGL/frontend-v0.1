const H2 = (props) => <h2 tw="font-bold text-[1.4rem] letter-spacing[-.5px] mb-0" {...props} />;

export default H2;
