import Link, { LinkProps } from 'next/link';
import React, { FC } from 'react';
import tw from 'twin.macro';
import styled from '~/src/utils/styled';

interface IContainer {
  noStyle: boolean;
  withInherit: boolean;
}
const Container = styled.a<IContainer>`
  font-size: ${(p) => (!p.withInherit ? p.theme.fontSizes[2] : 'inherit')};
  color: ${(p) => (!p.noStyle && !p.withInherit ? p.theme.colors.primary : 'inherit')};
  cursor: pointer;
  :hover {
    text-decoration: ${(p) => (!p.noStyle && p.withInherit ? 'underline' : 'none')} !important;
  }
`;
interface Props extends LinkProps {
  noStyle?: boolean;
  withInherit?: boolean;
  forEmptyUserField?: boolean;
}
const recordClickEvent = (route) => {
  // send click event to google analytics
};
const A: FC<Props> = ({ noStyle, withInherit = false, forEmptyUserField = false, children, ...props }) => {
  return (
    <Link {...props} passHref legacyBehavior>
      <Container
        noStyle={noStyle}
        withInherit={withInherit}
        css={
          withInherit &&
          tw`(inline-block vertical-align[middle] overflow-hidden white-space[nowrap] max-width[290px] text-overflow[ellipsis])!`
        }
        css={forEmptyUserField && tw`(color[rgba(95, 93, 93, 1)] underline font-bold italic text-[14px])!`}
        onClick={() => recordClickEvent(props.as)}
      >
        {children}
      </Container>
    </Link>
  );
};
export default A;
