import { Icon as ReactIcon } from '@iconify/react';

const Icon = (props) => <ReactIcon tw="inline-block overflow-hidden w-6 h-6" {...props} />;

export default Icon;
