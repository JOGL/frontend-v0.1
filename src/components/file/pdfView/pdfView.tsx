import 'react-pdf/dist/Page/AnnotationLayer.css';
import 'react-pdf/dist/Page/TextLayer.css';
import React, { useEffect, useState } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';
import { Button, Flex, Typography } from 'antd';
import { ExclamationCircleOutlined, ZoomInOutlined, ZoomOutOutlined } from '@ant-design/icons';
import Loader from '../../Common/loader/loader';
import { Grid } from 'antd';
import { ScrollbarStyled, WarningStyled } from '../file.styles';
import useTranslation from 'next-translate/useTranslation';
const { useBreakpoint } = Grid;

interface Props {
  file?: string;
  showLoading?: boolean;
  initialScale?: number;
}

const PdfView: React.FC<Props> = ({ file, showLoading = false, initialScale = 1 }) => {
  const [numPages, setNumPages] = useState<number>();
  const [scale, setScale] = useState<number>(initialScale);
  const screens = useBreakpoint();
  const { t } = useTranslation('common');

  useEffect(() => {
    pdfjs.GlobalWorkerOptions.workerSrc = new URL('pdfjs-dist/build/pdf.worker.min.js', import.meta.url).toString();
  }, []);

  const onDocumentLoadSuccess = ({ numPages }: { numPages: number }) => {
    setNumPages(numPages);
  };

  const zoomIn = () => setScale((s) => Math.min(3, s + 0.5));
  const zoomOut = () => setScale((s) => Math.max(0.5, s - 0.5));

  return (
    <Flex vertical align="center" gap="small">
      {!!numPages && numPages > 10 && (
        <WarningStyled gap="small">
          <ExclamationCircleOutlined />
          <Typography.Text>{t('pdfPreviewWarning')}</Typography.Text>
        </WarningStyled>
      )}
      <Flex>
        <Button onClick={zoomOut} icon={<ZoomOutOutlined />} />
        <Button onClick={zoomIn} icon={<ZoomInOutlined />} />
      </Flex>
      <ScrollbarStyled>
        <Document
          file={file}
          loading={showLoading ? <Loader /> : null}
          onLoadSuccess={onDocumentLoadSuccess}
          error={null}
        >
          {!!numPages &&
            Array.from(new Array(numPages > 10 ? 10 : numPages), (_, index) => (
              <React.Fragment key={`page_${index + 1}`}>
                <Page pageNumber={index + 1} scale={scale} width={screens.lg ? 990 : undefined} />
                <br />
              </React.Fragment>
            ))}
        </Document>
      </ScrollbarStyled>
    </Flex>
  );
};

export { PdfView };
