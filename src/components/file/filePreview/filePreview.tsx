import { DocumentConversionUpsertModel, DocumentModel } from '~/__generated__/types';
import { useCallback, useEffect, useState } from 'react';
import { Alert, Button, Card, Flex, message, Spin } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useAuth } from '~/auth/auth';
import { isOfficeSuiteFileType, isPdfFileType } from '~/src/utils/utils';
import axios from 'axios';
import api from '~/src/utils/api/api';
import { useMutation } from '@tanstack/react-query';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import FilePreviewWrapper from '../../filePreviewWrapper/filePreviewWrapper';
import { Grid } from 'antd';
import { LeftOutlined, RightOutlined } from '@ant-design/icons';
import { DisplayableAttachment } from '../../Feed/types';
import { PdfView } from '../pdfView/pdfView';
const { useBreakpoint } = Grid;

interface Props {
  file: DocumentModel | DisplayableAttachment;
  onClose: () => void;
  onPrevious?: () => void;
  onNext?: () => void;
}

const FilePreview = ({ file, onClose, onPrevious, onNext }: Props) => {
  const [isDownloading, setIsDownloading] = useState(false);
  const { accessToken } = useAuth();
  const { t } = useTranslation('common');
  const [isLoading, setIsLoading] = useState(true);
  const [hasError, setHasError] = useState(false);
  const [displayFile, setDisplayFile] = useState<string>('');

  const screens = useBreakpoint();
  const isPreviewAvailable =
    isPdfFileType(file.file_type) || isOfficeSuiteFileType(file.file_name || file.title, file.file_type);

  const documentConvertToPdf = useMutation({
    mutationKey: [MUTATION_KEYS.channelUpdate],
    mutationFn: async (data: DocumentConversionUpsertModel) => {
      const response = await api.documents.convertPdfCreate(data);
      return response.data;
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const onFileDownload = useCallback(
    (file: DocumentModel | DisplayableAttachment) => {
      if (!file?.document_url) {
        return null;
      }

      const download = async () => {
        try {
          setIsDownloading(true);
          let downloadUrl = '';
          if (accessToken) {
            const image = await fetch(`${file.document_url}`, {
              method: 'GET',
              headers: {
                'Content-Type': file.file_type,
                Authorization: accessToken,
              },
            });
            const imageBlog = await image.blob();
            downloadUrl = URL.createObjectURL(imageBlog);
          }

          const link = document?.createElement('a');
          link.href = downloadUrl;
          link.download = file.title;
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        } catch (error) {
          message.error(t('someUnknownErrorOccurred'));
        } finally {
          setIsDownloading(false);
        }
      };

      download();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [accessToken]
  );

  useEffect(() => {
    const fetchPdfAsync = async () => {
      setIsLoading(true);

      try {
        let base64 = '';
        if (file.document_url && accessToken) {
          const { data } = await axios.get(`${file.document_url}`, {
            responseType: 'arraybuffer',
            headers: { 'Content-type': file.file_type, Authorization: accessToken },
          });

          const encoding = btoa(new Uint8Array(data).reduce((acc, current) => acc + String.fromCharCode(current), ''));
          base64 = `data:${file.file_type};base64,${encoding}`;

          if (isOfficeSuiteFileType(file.file_name || file.title, file.file_type)) {
            documentConvertToPdf.mutate({
              data: base64,
            });
          }
        }
        setDisplayFile(base64);
      } catch (error) {
        setHasError(true);
      } finally {
        setIsLoading(false);
      }
    };
    if (!isPreviewAvailable) {
      message.error(t('noPreviewForThisFileType'));
      onClose();
    } else {
      fetchPdfAsync();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [file, accessToken, isPreviewAvailable]);

  if (!isPreviewAvailable) return null;
  const fileToDisplay = documentConvertToPdf.data || displayFile;

  return (
    <FilePreviewWrapper
      title={file.file_name || file.title}
      isDownloading={isDownloading}
      onDownload={() => onFileDownload(file)}
      onClose={onClose}
      isScrollable={!isLoading}
    >
      <Flex justify="center" align="center" gap="large">
        <Spin spinning={isLoading} />
        {onPrevious && !isLoading && <Button shape="circle" ghost icon={<LeftOutlined />} onClick={onPrevious} />}
        {hasError ? (
          <Card>
            <Alert message="Error" description={t('error.somethingWentWrong')} type="error" showIcon />
          </Card>
        ) : (
          <>{fileToDisplay && <PdfView file={fileToDisplay} initialScale={screens.xs ? 0.5 : 1} />}</>
        )}
        {onNext && !isLoading && <Button shape="circle" ghost icon={<RightOutlined />} onClick={onNext} />}
      </Flex>
    </FilePreviewWrapper>
  );
};

export default FilePreview;
