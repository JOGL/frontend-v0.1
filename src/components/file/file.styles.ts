import { FilePdfOutlined } from '@ant-design/icons';
import styled from '@emotion/styled';
import { Badge, Flex, Typography } from 'antd';

export const FileWrapperStyled = styled.div`
  cursor: pointer;
  display: grid;
  grid-template-columns: auto 1fr;
  grid-gap: ${({ theme }) => theme.space[4]};
  align-items: center;
  border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;
  background-color: ${({ theme }) => theme.token.neutral4};
  padding: ${({ theme }) => theme.space[2]};
`;

export const FileIconWrapperStyled = styled(Flex)`
  border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;
  background-color: ${({ theme }) => theme.token.neutral9};
  padding: ${({ theme }) => theme.space[2]};
  width: 34px;
  height: 34px;
  color: ${({ theme }) => theme.token.colorBgBase};
`;

export const FilePdfOutlinedStyled = styled(FilePdfOutlined)`
  font-size: 18px;
`;

export const FileDescriptionStyled = styled.div`
  min-width: 0;
`;

export const FileDateStyled = styled(Typography.Text)`
  white-space: nowrap;
`;

export const ScrollbarStyled = styled.div`
  max-height: 70vh;
  overflow: auto;

  &::-webkit-scrollbar {
    width: 8px;
  }

  &::-webkit-scrollbar-thumb {
    background: ${({ theme }) => theme.colors.grey};
  }
`;

export const BadgeStyled = styled(Badge)`
  && .ant-badge-dot {
    background: ${({ theme }) => theme.colors.badge};
  }
`;

export const WarningStyled = styled(Flex)`
  align-items: flex-start;
  svg,
  span {
    color: #fff;
  }
`;
