import dayjs from 'dayjs';
import {
  FileWrapperStyled,
  FilePdfOutlinedStyled,
  FileDescriptionStyled,
  FileIconWrapperStyled,
  FileDateStyled,
} from './file.styles';
import { Flex, Typography } from 'antd';
import { DocumentModel } from '~/__generated__/types';
import { isPdfFileType } from '~/src/utils/utils';
import { FileOutlined } from '@ant-design/icons';
import { useState } from 'react';
import FilePreview from './filePreview/filePreview';

interface Props {
  file: DocumentModel;
  withPreview?: boolean;
}

const File = ({ file, withPreview = true }: Props) => {
  const [showPreview, setShowPreview] = useState(false);
  const userName = `${file.created_by?.first_name} ${file.created_by?.last_name}`;
  return (
    <>
      <FileWrapperStyled onClick={() => setShowPreview(true)}>
        <FileIconWrapperStyled align="center" justify="center">
          {isPdfFileType(file.file_type) ? <FilePdfOutlinedStyled /> : <FileOutlined />}
        </FileIconWrapperStyled>
        <FileDescriptionStyled>
          <Typography.Text strong ellipsis={{ tooltip: file.file_name }}>
            {file.file_name}
          </Typography.Text>
          <Flex gap="middle">
            <Typography.Text ellipsis={{ tooltip: userName }}>{userName}</Typography.Text>
            <FileDateStyled type="secondary">{dayjs(file.created).format('MMM Do')}</FileDateStyled>
          </Flex>
        </FileDescriptionStyled>
      </FileWrapperStyled>
      {withPreview && showPreview && <FilePreview file={file} onClose={() => setShowPreview(false)} />}
    </>
  );
};

export default File;
