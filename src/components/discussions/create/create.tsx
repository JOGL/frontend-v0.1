import { Modal } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { MemberModel, ChannelUpsertModel } from '~/__generated__/types';

import { CreateForm } from '../createForm/createForm';

interface Props {
  members: MemberModel[];
  onClose: () => void;
  onSave: (values: ChannelUpsertModel) => void;
}

export const CreateDiscussion = ({ members, onClose, onSave }: Props) => {
  const { t } = useTranslation('common');

  return (
    <Modal open onCancel={onClose} title={t('newDiscussion')} footer={null}>
      <CreateForm members={members} onCancel={onClose} onSave={onSave} />
    </Modal>
  );
};
