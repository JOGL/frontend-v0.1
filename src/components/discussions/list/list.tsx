import { Divider, Empty } from 'antd';

import { ChannelModel } from '~/__generated__/types';
import DiscussionItem, { DiscussionItemLoader } from '../discussionItem/discussionItem';

export const DiscussionsListLoader = () => {
  return (
    <div>
      {Array.from({ length: 5 }).map((_, index) => {
        return (
          <>
            <DiscussionItemLoader key={index} />
            {index !== 4 && <Divider />}
          </>
        );
      })}
    </div>
  );
};

interface Props {
  discussions?: ChannelModel[];
  onJoin: () => void;
}

export const DiscussionsList = ({ discussions, onJoin }: Props) => {
  if (!discussions || !discussions.length) {
    return <Empty />;
  }

  return (
    <div>
      {discussions?.map((discussion, idx) => {
        return (
          <>
            <DiscussionItem key={discussion.id} discussion={discussion} onJoin={onJoin} />
            {idx !== discussions.length - 1 && <Divider />}
          </>
        );
      })}
    </div>
  );
};
