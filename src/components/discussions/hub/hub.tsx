import { useQuery, keepPreviousData } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import useTranslation from 'next-translate/useTranslation';
import { DiscussionsList } from '../list/list';
import { Loader } from '../loader/loader';
import { HubDiscussionCreate } from './hubDiscussionCreate';
import { useState } from 'react';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { SpaceTabHeader } from '../../Common/space/spaceTabHeader/spaceTabHeader';
import { SpaceContentStyled } from '../../Common/space/space.styles';
import useDebounce from '~/src/hooks/useDebounce';

interface Props {
  hubId: string;
  canCreate: boolean;
}

export const DiscussionsListHub = ({ hubId, canCreate }: Props) => {
  const { t } = useTranslation('common');
  const [openCreateModal, setOpenCreateModal] = useState(false);
  const [searchText, setSearchText] = useState('');
  const debouncedSearch = useDebounce(searchText, 300);

  const { data, isLoading, refetch, isPlaceholderData } = useQuery({
    queryKey: [QUERY_KEYS.nodeChannelsList, debouncedSearch],
    queryFn: async () => {
      const response = await api.nodes.channelsDetail(hubId, { Search: debouncedSearch });
      return response.data;
    },
    placeholderData: keepPreviousData,
  });

  if (isLoading && !data && !isPlaceholderData) {
    return <Loader />;
  }
  return (
    <>
      <SpaceTabHeader
        setSearchText={setSearchText}
        canCreate={canCreate}
        searchText={searchText}
        handleClick={() => setOpenCreateModal(true)}
        title={t('discussion.other')}
        showSearch={!!searchText || !!data?.length}
      />

      <SpaceContentStyled>
        <DiscussionsList discussions={data} onJoin={() => refetch()} />
      </SpaceContentStyled>

      {openCreateModal && (
        <HubDiscussionCreate hubId={hubId} onClose={() => setOpenCreateModal(false)} onCreate={() => refetch()} />
      )}
    </>
  );
};
