import useTranslation from 'next-translate/useTranslation';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { CreateDiscussion } from '../create/create';
import { ChannelUpsertModel } from '~/__generated__/types';
import { message } from 'antd';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import useUserData from '~/src/hooks/useUserData';
import { useRouter } from 'next/router';

interface Props {
  workspaceId: string;
  onClose: () => void;
  onCreate: () => void;
}

export const WorkspaceDiscussionCreate = ({ workspaceId, onClose, onCreate }: Props) => {
  const { t } = useTranslation('common');
  const queryClient = useQueryClient();
  const router = useRouter();
  const { userData } = useUserData();
  const { data, isPending } = useQuery({
    queryKey: [QUERY_KEYS.workspaceMembersList],
    queryFn: async () => {
      const response = await api.communityEntities.membersDetail(workspaceId);
      return response.data;
    },
  });

  const createDiscussion = useMutation({
    mutationKey: [MUTATION_KEYS.workspaceChannelCreate],
    mutationFn: async (data: ChannelUpsertModel) => {
      const res = await api.channels.channelsCreate(workspaceId, data);
      return res;
    },
    onSuccess: (res) => {
      message.success(t('channelWasCreated'));
      queryClient.invalidateQueries({ queryKey: [QUERY_KEYS.feedNodesList] });
      onCreate();
      onClose();
      router.push(`/channel/${res.data}`);
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  if (isPending && !data) return null;
  const members = data?.filter((member) => member.id !== userData?.id);
  return (
    <CreateDiscussion onClose={onClose} members={members || []} onSave={(data) => createDiscussion.mutate(data)} />
  );
};
