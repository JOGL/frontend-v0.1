import { keepPreviousData, useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { DiscussionsList } from '../list/list';
import { Loader } from '../loader/loader';
import useTranslation from 'next-translate/useTranslation';
import { useState } from 'react';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { SpaceTabHeader } from '../../Common/space/spaceTabHeader/spaceTabHeader';
import { SpaceContentStyled } from '../../Common/space/space.styles';
import useDebounce from '~/src/hooks/useDebounce';
import { WorkspaceDiscussionCreate } from './workspaceDiscussionCreate';

interface Props {
  workspaceId: string;
  canCreate: boolean;
}

export const DiscussionsListWorkspace = ({ workspaceId, canCreate }: Props) => {
  const { t } = useTranslation('common');
  const [openCreateModal, setOpenCreateModal] = useState(false);
  const [searchText, setSearchText] = useState('');
  const debouncedSearch = useDebounce(searchText, 300);

  const { data, isLoading, refetch, isPlaceholderData } = useQuery({
    queryKey: [QUERY_KEYS.workspaceChannelsList, debouncedSearch],
    queryFn: async () => {
      const response = await api.workspaces.channelsDetail(workspaceId, { Search: debouncedSearch });
      return response.data;
    },
    placeholderData: keepPreviousData,
  });

  if (isLoading && !data && !isPlaceholderData) {
    return <Loader />;
  }

  return (
    <>
      <SpaceTabHeader
        title={t('discussion.other')}
        searchText={searchText}
        setSearchText={setSearchText}
        canCreate={canCreate}
        handleClick={() => setOpenCreateModal(true)}
        showSearch={!!searchText || !!data?.length}
      />
      <SpaceContentStyled>
        <DiscussionsList discussions={data} onJoin={() => refetch()} />
      </SpaceContentStyled>

      {openCreateModal && (
        <WorkspaceDiscussionCreate
          workspaceId={workspaceId}
          onClose={() => setOpenCreateModal(false)}
          onCreate={() => refetch()}
        />
      )}
    </>
  );
};
