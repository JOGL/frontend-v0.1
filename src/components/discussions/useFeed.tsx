import { useMutation, useQueryClient } from '@tanstack/react-query';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import useUser from '~/src/hooks/useUser';
import api from '~/src/utils/api/api';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';

const useFeed = (feedId: string): [
  void
] => {
  const queryClient = useQueryClient();
  const { user } = useUser();

  const feedSeenMutation = useMutation({
    mutationKey: [MUTATION_KEYS.feedSeen],
    mutationFn: async (feedId: string) => {
      const response = await api.feed.seenCreate(feedId);
      return { status: response.status, data: response.data, feedId };
    },
    onSuccess: (result) => {
      if (result.status === 200) {
        queryClient.invalidateQueries({
          queryKey: [QUERY_KEYS.feedNodesList],
        });
        queryClient.invalidateQueries({
          queryKey: [QUERY_KEYS.feedAllPostsList, result.feedId],
        });
        queryClient.invalidateQueries({
          queryKey: [QUERY_KEYS.feedMentionsList, result.feedId],
        });
        queryClient.invalidateQueries({
          queryKey: [QUERY_KEYS.feedThreadsList, result.feedId],
        });
      }
    },
  });

  const markFeedAsSeen = useEffect(() => {
    const timeoutId = setTimeout(() => {
      user && feedSeenMutation.mutate(feedId);
    }, 1000);

    return () => {
      clearTimeout(timeoutId);
    };
  }, [feedId]);

  return [markFeedAsSeen];
};

export default useFeed;
