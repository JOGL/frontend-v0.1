import { Skeleton, Space, Typography } from 'antd';

import { HeaderStyled, WrapperStyled } from '../discussions.styles';
import useTranslation from 'next-translate/useTranslation';
import { DiscussionsListLoader } from '../list/list';

export const Loader = () => {
  const { t } = useTranslation('common');

  return (
    <WrapperStyled>
      <HeaderStyled>
        <Typography.Title level={3}>{t('discussion', { count: 2 })}</Typography.Title>
        <Space>
          <Skeleton.Input />
          <Skeleton.Button />
        </Space>
      </HeaderStyled>
      <DiscussionsListLoader />
    </WrapperStyled>
  );
};
