export const ICON_KEYS = {
  bulb: 'bulb',
  table: 'table',
  compass: 'compass',
  bank: 'bank',
  bell: 'bell',
  cloud: 'cloud',
  fire: 'fire',
  paperClip: 'paperClip',
  sound: 'sound',
  thunderbolt: 'thunderbolt',
  trophy: 'trophy',
  certificate: 'certificate',
  pushpin: 'pushpin',
} as const;

export const ICONS_LIST = Object.keys(ICON_KEYS) as (keyof typeof ICON_KEYS)[];

export const MANAGEMENT_OPTIONS = ['adminOnly', 'everyone'];
export const MANAGEMENT_PROPERTIES = [
  'content_entities_created_by_any_member',
  'comments_created_by_any_member',
  'mention_everyone_by_any_member',
  'members_invited_by_any_member',
];
