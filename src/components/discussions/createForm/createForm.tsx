import { Col, Form, Input, Row, Select, Radio, Space, Flex, Typography, Checkbox, Avatar, Empty, Button } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { ICONS_LIST, MANAGEMENT_OPTIONS } from './createForm.utils';
import { getDiscussionIcon } from '../discussionItem/discussionItem.utils';
import {
  MemberModel,
  AccessLevel,
  ChannelMemberUpsertModel,
  ChannelUpsertModel,
  UserMiniModel,
} from '~/__generated__/types';
import { useState } from 'react';
import type { CheckboxProps } from 'antd';
import { CardStyled } from './createForm.styles';
import { FormValues } from './createForm.types';
import { UserOutlined } from '@ant-design/icons';

const DEFAULT_INITIAL_VALUES = {
  icon_key: 'bulb',
  content_entities_created_by_any_member: true,
  comments_created_by_any_member: true,
  mention_everyone_by_any_member: false,
  members_invited_by_any_member: false,
};

interface Props {
  members: MemberModel[] | UserMiniModel[];
  onCancel: () => void;
  onSave: (values: ChannelUpsertModel) => void;
}

export const CreateForm = ({ members, onCancel, onSave }: Props) => {
  const { t } = useTranslation('common');
  const [form] = Form.useForm<FormValues>();
  const visibility = Form.useWatch('visibility', form);
  const auto_join = Form.useWatch('auto_join', form);
  const managementOptions = MANAGEMENT_OPTIONS.map((option) => ({
    label: t(`${option}`),
    value: option === 'everyone',
  }));

  const [selectedMembers, setSelectedMembers] = useState<MemberModel[] | UserMiniModel[]>([]);

  const onFinish = (values: FormValues) => {
    const {
      content_entities_created_by_any_member,
      comments_created_by_any_member,
      mention_everyone_by_any_member,
      members_invited_by_any_member,
      ...rest
    } = values;

    const managementValues = {
      content_entities_created_by_any_member,
      comments_created_by_any_member,
      mention_everyone_by_any_member,
      members_invited_by_any_member,
    };

    const management = Object.keys(managementValues).filter((key) => managementValues[key]);
    onSave({ ...rest, management, members: selectedMembers });
  };

  const onCheckAllChange: CheckboxProps['onChange'] = (e) => {
    setSelectedMembers(
      e.target.checked ? members.map((member) => ({ user_id: member.id, access_level: AccessLevel.Member })) : []
    );
  };

  return (
    <Form
      form={form}
      layout="vertical"
      onFinish={onFinish}
      requiredMark="optional"
      initialValues={DEFAULT_INITIAL_VALUES}
      onFieldsChange={(changedFields) => {
        const autoJoin = changedFields.find((field) => field.name.includes('auto_join'));
        if (autoJoin && autoJoin.value) {
          setSelectedMembers(members.map((member) => ({ user_id: member.id, access_level: AccessLevel.Member })));
        }
      }}
    >
      <Row gutter={16}>
        <Col span={4}>
          <Form.Item name="icon_key" label={t('icon')} rules={[{ required: true }]}>
            <Select
              options={ICONS_LIST.map((icon) => ({ value: icon, label: getDiscussionIcon(icon) }))}
              optionRender={(option) => getDiscussionIcon(option.value as string)}
            />
          </Form.Item>
        </Col>
        <Col span={20}>
          <Form.Item name="title" label={t('name')} rules={[{ required: true, message: t('error.requiredField') }]}>
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Form.Item name="description" label={t('description')}>
        <Input />
      </Form.Item>
      <Form.Item
        name="visibility"
        label={t('privacySettings')}
        rules={[{ required: true, message: t('error.requiredField') }]}
      >
        <Radio.Group>
          <Space direction="vertical">
            <Radio value="private">
              <Flex vertical>
                <Typography.Text>{t('private')}</Typography.Text>
                <Typography.Text type="secondary">{t('privateDiscussionDescription')}</Typography.Text>
              </Flex>
            </Radio>
            <Radio value="open">
              <Flex vertical>
                <Typography.Text>{t('open')}</Typography.Text>
                <Typography.Text type="secondary">{t('openDiscussionDescription')}</Typography.Text>
              </Flex>
            </Radio>
          </Space>
        </Radio.Group>
      </Form.Item>
      {visibility === 'open' && (
        <Row>
          <Col offset={2}>
            <Form.Item name="auto_join" valuePropName="checked">
              <Checkbox>
                <Flex vertical>
                  <Typography.Text>{t('enableAutoJoin')}</Typography.Text>
                  <Typography.Text type="secondary">{t('enableAutoJoinDescription')}</Typography.Text>
                </Flex>
              </Checkbox>
            </Form.Item>
          </Col>
        </Row>
      )}
      <Typography.Text>{t('selectMembersWithCount', { count: selectedMembers.length })}</Typography.Text>
      <CardStyled>
        {members.length ? (
          <Flex vertical gap="small">
            <Checkbox
              indeterminate={selectedMembers.length > 0 && selectedMembers.length < members.length}
              checked={selectedMembers.length === members.length}
              onChange={onCheckAllChange}
              disabled={auto_join}
            >
              {t('selectAll')}
            </Checkbox>
            {members.map((member) => {
              const isChecked = !!selectedMembers.find((m) => m.user_id === member.id);
              return (
                <Row key={member.id}>
                  <Col span={18}>
                    <Checkbox
                      disabled={auto_join}
                      checked={isChecked}
                      onChange={(e) => {
                        if (e.target.checked) {
                          setSelectedMembers((prev) => [
                            ...prev,
                            { user_id: member.id, access_level: AccessLevel.Member },
                          ]);
                        } else {
                          setSelectedMembers((prev) => [...prev.filter((m) => m.user_id !== member.id)]);
                        }
                      }}
                    >
                      {
                        <Flex gap="small" align="center" flex={'auto 1 auto'}>
                          <Avatar icon={<UserOutlined />} src={member.logo_url} />
                          <Flex vertical>
                            <Typography.Text strong>{member.first_name}</Typography.Text>
                            <Typography.Text type="secondary">{`@${member.username}`}</Typography.Text>
                          </Flex>
                        </Flex>
                      }
                    </Checkbox>
                  </Col>
                  <Col span={6}>
                    {isChecked && (
                      <Select
                        size="small"
                        defaultValue={AccessLevel.Member}
                        style={{ width: '100%' }}
                        options={[
                          { label: 'Admin', value: AccessLevel.Admin },
                          { label: 'Member', value: AccessLevel.Member },
                        ]}
                        value={selectedMembers.find((m) => m.user_id === member.id && m.access_level)?.access_level}
                        onChange={(access_level) => {
                          setSelectedMembers((prev) =>
                            prev.map((m) => {
                              if (m.user_id === member.id) {
                                return { ...m, access_level };
                              }
                              return m;
                            })
                          );
                        }}
                      />
                    )}
                  </Col>
                </Row>
              );
            })}
          </Flex>
        ) : (
          <Empty description={t('thereAreNoMembers')} />
        )}
      </CardStyled>
      <Form.Item name="content_entities_created_by_any_member" label={t('whoCanPost')} required>
        <Select options={managementOptions} />
      </Form.Item>
      <Form.Item name="comments_created_by_any_member" label={t('whoCanReply')} required>
        <Select options={managementOptions} />
      </Form.Item>
      <Form.Item name="mention_everyone_by_any_member" label={t('whoCanMentionEveryone')} required>
        <Select options={managementOptions} />
      </Form.Item>
      <Form.Item name="members_invited_by_any_member" label={t('whoCanAddPeople')} required>
        <Select options={managementOptions} />
      </Form.Item>
      <Row justify="end" gutter={16}>
        <Col>
          <Button type="default" onClick={onCancel}>
            {t('action.cancel')}
          </Button>
        </Col>
        <Col>
          <Button type="primary" htmlType="submit">
            {t('action.create')}
          </Button>
        </Col>
      </Row>
    </Form>
  );
};
