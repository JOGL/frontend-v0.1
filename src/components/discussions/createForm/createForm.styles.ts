import styled from '@emotion/styled';
import { Card } from 'antd';

export const CardStyled = styled(Card)`
  margin: ${({ theme }) => theme.space[2]} 0px;
  max-height: 300px;
  overflow-y: auto;
`;
