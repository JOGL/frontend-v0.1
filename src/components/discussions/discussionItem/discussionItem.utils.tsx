import {
  BankOutlined,
  BellOutlined,
  BorderlessTableOutlined,
  BulbOutlined,
  CloudOutlined,
  CompassOutlined,
  FireOutlined,
  PaperClipOutlined,
  PushpinOutlined,
  SafetyCertificateOutlined,
  SoundOutlined,
  ThunderboltOutlined,
  TrophyOutlined,
} from '@ant-design/icons';
import { ICON_KEYS } from '../createForm/createForm.utils';

export const getDiscussionIcon = (key: string) => {
  switch (key) {
    case ICON_KEYS.bulb:
      return <BulbOutlined />;
    case ICON_KEYS.table:
      return <BorderlessTableOutlined />;
    case ICON_KEYS.compass:
      return <CompassOutlined />;
    case ICON_KEYS.bank:
      return <BankOutlined />;
    case ICON_KEYS.bell:
      return <BellOutlined />;
    case ICON_KEYS.cloud:
      return <CloudOutlined />;
    case ICON_KEYS.fire:
      return <FireOutlined />;
    case ICON_KEYS.paperClip:
      return <PaperClipOutlined />;
    case ICON_KEYS.sound:
      return <SoundOutlined />;
    case ICON_KEYS.thunderbolt:
      return <ThunderboltOutlined />;
    case ICON_KEYS.trophy:
      return <TrophyOutlined />;
    case ICON_KEYS.certificate:
      return <SafetyCertificateOutlined />;
    case ICON_KEYS.pushpin:
      return <PushpinOutlined />;
    default:
      return <BulbOutlined />;
  }
};
