import styled from '@emotion/styled';
import { Typography } from 'antd';

export const RowWrapperStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: ${({ theme }) => theme.space[4]};
  padding: 0px ${({ theme }) => theme.space[4]};
`;

export const DetailsWrapperStyled = styled.div`
  display: flex;
  flex-direction: column;
  gap: ${({ theme }) => theme.space[1]};
  min-width: 0px;
`;

export const JoinedStyled = styled.div`
  display: flex;
  gap: ${({ theme }) => theme.space[2]};
  color: ${({ theme }) => theme.token.colorPrimary};

  & > span {
    color: ${({ theme }) => theme.token.colorPrimary};
    white-space: nowrap;
  }
`;

export const TitleStyled = styled(Typography.Text)`
  cursor: pointer;
`;

export const DescriptionInfoStyled = styled(Typography.Text)`
  white-space: nowrap;
`;
