import { Button, Flex, Typography, Skeleton, theme, message } from 'antd';
import { CheckOutlined, LockOutlined, UserOutlined } from '@ant-design/icons';
import useTranslation from 'next-translate/useTranslation';
import { FC } from 'react';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { ChannelModel, Permission } from '~/__generated__/types';
import api from '~/src/utils/api/api';
import { getDiscussionIcon } from './discussionItem.utils';
import {
  RowWrapperStyled,
  DetailsWrapperStyled,
  JoinedStyled,
  TitleStyled,
  DescriptionInfoStyled,
} from './discussionItem.styles';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { useRouter } from 'next/router';

const { useToken } = theme;

export const DiscussionItemLoader = () => {
  return (
    <RowWrapperStyled>
      <DetailsWrapperStyled>
        <Skeleton.Input block size="small" />
        <Skeleton.Input block size="small" />
      </DetailsWrapperStyled>
      <Skeleton.Button />
    </RowWrapperStyled>
  );
};

export interface Props {
  discussion: ChannelModel;
  onJoin: () => void;
}

const DiscussionItem: FC<Props> = ({ discussion, onJoin }) => {
  const { t } = useTranslation('common');
  const queryClient = useQueryClient();
  const { token } = useToken();
  const router = useRouter();

  const joinDiscussion = useMutation({
    mutationKey: [MUTATION_KEYS.channelMemberJoin],
    mutationFn: async () => {
      await api.channels.membersJoinCreate(discussion.id);
    },
    onSuccess: () => {
      message.success(t('youJoinedNewDiscussion'));
      queryClient.invalidateQueries({ queryKey: [QUERY_KEYS.feedNodesList] });
      router.push(`/channel/${discussion.id}`);
      onJoin();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  return (
    <RowWrapperStyled>
      <DetailsWrapperStyled>
        <Flex align="center" gap={4}>
          {getDiscussionIcon(discussion.icon_key)}
          <Button
            type="text"
            onClick={() => {
              router.push(`/channel/${discussion.id}`);
            }}
          >
            <TitleStyled strong>{discussion.title}</TitleStyled>
          </Button>
          {discussion.visibility === 'private' && (
            <Typography.Text type="secondary">
              {' • '}
              <LockOutlined style={{ color: token.colorTextDescription }} />
            </Typography.Text>
          )}
        </Flex>
        <Flex gap="small" align="center">
          <UserOutlined style={{ color: token.colorTextDescription }} />
          <DescriptionInfoStyled type="secondary">
            {t('discussionMembers', {
              count: discussion.stats?.members_count,
            })}
          </DescriptionInfoStyled>
          {discussion.description && (
            <Typography.Text
              type="secondary"
              italic
              ellipsis={{ tooltip: discussion.description }}
              style={{ maxWidth: '100%' }}
            >
              {` •  ${discussion.description}`}
            </Typography.Text>
          )}
          {discussion.auto_join && (
            <DescriptionInfoStyled type="secondary" italic>
              {' • '}
              {t('autoJoinEnabled')}
            </DescriptionInfoStyled>
          )}
        </Flex>
      </DetailsWrapperStyled>
      {discussion.permissions?.includes(Permission.Join) &&
        (discussion.user_access_level ? (
          <JoinedStyled>
            <CheckOutlined />
            <Typography.Text>{t('joined')}</Typography.Text>
          </JoinedStyled>
        ) : (
          <Button type="primary" onClick={() => joinDiscussion.mutate()} loading={joinDiscussion.isPending}>
            {t('join')}
          </Button>
        ))}
    </RowWrapperStyled>
  );
};

export default DiscussionItem;
