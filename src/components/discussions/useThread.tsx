import { useMutation } from '@tanstack/react-query';
import { useEffect } from 'react';
import useUser from '~/src/hooks/useUser';
import api from '~/src/utils/api/api';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';

const useFeed = (postId: string, feedId: string): [
  void
] => {
  const { user } = useUser();

  const postSeenMutation = useMutation({
    mutationKey: [MUTATION_KEYS.feedSeen],
    mutationFn: async (postId: string) => {
      const response = await api.feed.contentEntitiesSeenCreate(postId);
      return { status: response.status, data: response.data, feedId };
    }
  });

  const markFeedAsSeen = useEffect(() => {
    const timeoutId = setTimeout(() => {
      user && postSeenMutation.mutate(postId);
    }, 1000);

    return () => {
      clearTimeout(timeoutId);
    };
  }, [postId]);

  return [markFeedAsSeen];
};

export default useFeed;
