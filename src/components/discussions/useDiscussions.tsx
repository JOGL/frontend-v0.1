import { useRouter } from 'next/router';

const useDiscussions = (): [
  (id: string, tab?: string) => void,
  (feedId: string, id: string, commentId?: string) => void,
  (feedId: string, postId?: string) => void
] => {
  const router = useRouter();

  const goToChannel = (id: string, tab?: string) => {
    if (tab) router.push(`/channel/${id}?tab=${tab}`);
    else router.push(`/channel/${id}`);
  };

  const goToPost = (feedId: string, id: string, commentId?: string) => {
    const tab = router.query.inboxTab ?? 'all';
    let path = `/discussion/${feedId}?inboxTab=${tab}`;
    path += `&postId=${id}`;
    if (commentId) path += `#${commentId}`;

    router.push(path);
  };

  const goToFeed = (feedId: string, postId?: string) => {
    const tab = router.query.inboxTab ?? 'all';
    let path = `/discussion/${feedId}?inboxTab=${tab}`;
    if (postId) path += `#${postId}`;

    router.push(path);
  };

  return [goToChannel, goToPost, goToFeed];
};

export default useDiscussions;