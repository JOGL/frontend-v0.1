import { useInfiniteQuery, useMutation, useQuery } from '@tanstack/react-query';
import { Flex, Input, Typography, message, Grid } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useMemo, useState } from 'react';
import {
  DocumentModel,
  DocumentOrFolderModel,
  DocumentType,
  DocumentUpdateModel,
  FeedEntityFilter,
  Permission,
  SortKey,
} from '~/__generated__/types';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import useDebounce from '~/src/hooks/useDebounce';
import { DocumentPreviewModal } from '../documentPreviewModal/documentPreviewModal';
import { ContainerFilter } from '../../Common/filters/containerFilter/containerFilter';
import DocumentTable from '../documentTable/documentTable';
import useDocuments from '../useDocuments';
import { useFeedEntity } from '../../Common/feedEntity/feedEntity.hooks';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import EditDocumentModal, { EditDocumentFormFields } from '../../editDocumentModal/editDocumentModal';
import LinkModal, { LinkFormFields } from '../../linkModal/linkModal';
import MobileCardView from '../../Common/mobileCardView/mobileCardView';
const { useBreakpoint } = Grid;
const PAGE_SIZE = 50;

interface Props {
  hubId: string;
  title: string;
  filter?: FeedEntityFilter;
  sortKey?: SortKey;
}

const HubDocumentList = ({ hubId, title, filter, sortKey = SortKey.Updateddate }: Props) => {
  const { t } = useTranslation('common');
  const [search, setSearch] = useState('');
  const debouncedSearch = useDebounce(search, 300);
  const [selectedContainers, setSelectedContainers] = useState<string[]>([]);
  const [previewDocument, setPreviewDocument] = useState<DocumentModel | null>(null);
  const [goToDocument] = useDocuments();
  const [markFeedAsOpened] = useFeedEntity();
  const [editDocument, setEditDocument] = useState<DocumentOrFolderModel | null>(null);
  const screens = useBreakpoint();
  const isMobile = screens.xs;

  const { selectedHub } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
  }));

  const { data: documentsData, refetch, isLoading } = useInfiniteQuery({
    queryKey: [QUERY_KEYS.documentsAggregate, hubId, debouncedSearch, selectedContainers, filter, sortKey],
    queryFn: async ({ pageParam = 1 }) => {
      const response = await api.nodes.documentsAggregateDetail(hubId, {
        Search: debouncedSearch,
        communityEntityIds: selectedContainers.join(',') ?? undefined,
        filter: filter,
        SortKey: sortKey,
        SortAscending: undefined,
        Page: pageParam,
        PageSize: PAGE_SIZE,
      });
      return response.data;
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItemsCount = allPages.reduce((acc, page) => acc + page.items.length, 0);
      return loadedItemsCount < lastPage.total ? allPages.length + 1 : undefined;
    },
  });

  const { data: containers } = useQuery({
    queryKey: [QUERY_KEYS.documentsAggregateContainers, hubId],
    queryFn: async () => {
      const response = await api.nodes.documentsAggregateCommunityEntitiesDetail(hubId, {});
      return response.data;
    },
  });
  const deleteFile = useMutation({
    mutationFn: async (id: string) => {
      await api.documents.documentsDeleteDetail(id);
    },
    onSuccess: () => {
      message.success(t('documentDeleteSuccess'));
      refetch();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const flattenedData: DocumentModel[] = useMemo(() => {
    return documentsData?.pages.flatMap((page) => page.items) ?? [];
  }, [documentsData]);

  const updateFile = useMutation({
    mutationFn: async ({
      document,
      updateValues,
    }: {
      document: DocumentOrFolderModel;
      updateValues: Partial<DocumentUpdateModel>;
    }) => {
      const { id, type, ...rest } = document;
      await api.documents.documentsUpdateDetail(id, {
        ...rest,
        type: type as DocumentType,
        ...updateValues,
      });
    },
    onSuccess: () => {
      message.success(t('fileUpdateSuccess'));
      refetch();
      setEditDocument(null);
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const canManage = selectedHub?.user_access.permissions.includes(Permission.Managedocuments) ? true : false;
  return (
    <>
      <Flex vertical gap="middle">
        <Typography.Title>{title}</Typography.Title>
        <Flex justify="stretch">
          <Input.Search
            placeholder={t('action.searchForDocuments')}
            onChange={(e) => {
              setSearch(e.target.value);
            }}
            onSearch={(value) => {
              setSearch(value);
            }}
          />
        </Flex>
        <Flex justify="stretch">
          {containers && (
            <ContainerFilter
              containers={containers}
              onChange={(communityEntityIds) => setSelectedContainers(communityEntityIds)}
            />
          )}
        </Flex>
        {isMobile ? (
          <MobileCardView
            data={flattenedData}
            loading={isLoading}
            onClickRow={(document: DocumentModel) => {
              if (document?.type === DocumentType.Jogldoc) {
                goToDocument(document.id);
                return;
              }
              markFeedAsOpened(document);
              if (document?.type === DocumentType.Link && document.url) {
                window.open(document?.url, '_blank');
                return;
              }
              setPreviewDocument(document);
            }}
          />
        ) : (
          <DocumentTable
            documents={flattenedData}
            loading={isLoading}
            canManage={canManage}
            hideColumns={canManage ? [] : ['id']}
            onClickRow={(document) => {
              if (document?.type === DocumentType.Jogldoc) {
                goToDocument(document.id);
                return;
              }

              //mark document as opened if file or link
              markFeedAsOpened(document);

              if (document?.type === DocumentType.Link && document.url) {
                window.open(document?.url, '_blank');
                return;
              }
              setPreviewDocument(document);
            }}
            onDelete={(document) => {
              deleteFile.mutate(document.id);
            }}
            onEdit={(document) => {
              if (document?.type === DocumentType.Jogldoc) {
                goToDocument(document.id);
                return;
              }
              setEditDocument(document);
            }}
          />
        )}
      </Flex>
      {editDocument?.type === DocumentType.Link && (
        <LinkModal
          link={editDocument}
          onClose={() => {
            setEditDocument(null);
          }}
          onConfirm={(data: LinkFormFields) => {
            if (editDocument) {
              updateFile.mutate({
                document: editDocument,
                updateValues: data,
              });
            }
          }}
        />
      )}
      {editDocument?.type === DocumentType.Document && (
        <EditDocumentModal
          document={editDocument}
          onClose={() => {
            setEditDocument(null);
          }}
          onConfirm={(params: EditDocumentFormFields) => {
            updateFile.mutate({
              document: editDocument,
              updateValues: params,
            });
          }}
        />
      )}
      {previewDocument && <DocumentPreviewModal document={previewDocument} onClose={() => setPreviewDocument(null)} />}
    </>
  );
};

export default HubDocumentList;
