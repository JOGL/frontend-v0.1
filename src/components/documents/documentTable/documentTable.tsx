import { MessageOutlined, MoreOutlined } from '@ant-design/icons';
import { Badge, Button, Dropdown, MenuProps, Space, Tag, theme } from 'antd';
import dayjs from 'dayjs';
import useTranslation from 'next-translate/useTranslation';
import { DocumentModel, FeedStatModel, Permission, UserMiniModel } from '~/__generated__/types';
import { AlignType } from 'rc-table/lib/interface';
import { TableStyled } from '../../Common/table.styles';
import Loader from '../../Common/loader/loader';
import useDocuments from '../useDocuments';
import { FeedEntityLink } from '../../Common/links/feedEntityLink/feedEntityLink';
import { UserLink } from '../../Common/links/userLink/userLink';

interface Props {
  documents: DocumentModel[];
  loading: boolean;
  hideColumns: string[];
  canManage: boolean;
  onClickRow: (document: DocumentModel) => void;
  onEdit?: (document: DocumentModel) => void;
  onMove?: (document: DocumentModel) => void;
  onDelete?: (document: DocumentModel) => void;
}

const DocumentTable = ({
  documents,
  loading = false,
  hideColumns = [],
  canManage,
  onClickRow,
  onEdit,
  onMove,
  onDelete,
}: Props) => {
  const { t } = useTranslation('common');
  const { token } = theme.useToken();
  const [goToDocument] = useDocuments();

  const columns = [
    {
      dataIndex: 'title',
      title: t('title'),
      ellipsis: true,
    },
    {
      dataIndex: 'type',
      title: t('type'),
      width: 120,
      render: (type: string) => {
        return type ? <Tag>{t(`documentType.${type}`)}</Tag> : <Tag>{t('folder.one')}</Tag>;
      },
    },
    {
      dataIndex: 'feed_entity',
      title: t('space.one'),
      ellipsis: true,
      render: (feed_entity: DocumentModel['feed_entity']) => {
        return <FeedEntityLink entity={feed_entity} />;
      },
    },
    {
      dataIndex: 'updated',
      title: t('modified'),
      width: 120,
      render: (updated: string) => {
        return updated ? dayjs(updated).format('DD.MM.YYYY') : '';
      },
    },
    {
      dataIndex: 'created_by',
      title: t('createdBy'),
      width: 120,
      render: (created_by: UserMiniModel) => {
        return <UserLink user={created_by} />;
      },
    },
    {
      dataIndex: 'feed_stats',
      align: 'left' as AlignType,
      width: 80,
      render: (feed_stats: FeedStatModel, item: DocumentModel) => {
        return (
          <Button type="text" onClick={() => goToDocument(item.id, 'discussion')}>
            <Badge count={feed_stats.new_post_count} size="small" offset={[4, -4]} color={token.colorPrimary}>
              <Space>
                <MessageOutlined />
                {feed_stats.post_count}
              </Space>
            </Badge>
          </Button>
        );
      },
    },
    {
      dataIndex: 'id',
      align: 'right' as AlignType,
      width: 80,
      render: (id: string, item: DocumentModel) => {
        if (!canManage) return null;
        if (!onEdit && !onDelete) return null;
        const menuItems: MenuProps['items'] = [
          ...(onEdit && item.permissions?.includes(Permission.Manage)
            ? [
                {
                  label: t('action.edit'),
                  key: 'edit',
                  onClick: (e) => {
                    e.domEvent.stopPropagation();
                    onEdit(item);
                  },
                },
              ]
            : []),

          ...(onMove && item.permissions?.includes(Permission.Manage)
            ? [
                {
                  label: t('action.move'),
                  key: 'move',
                  onClick: (e) => {
                    e.domEvent.stopPropagation();
                    onMove(item);
                  },
                },
              ]
            : []),
          ...(onDelete && item.permissions?.includes(Permission.Delete)
            ? [
                {
                  label: t('action.delete'),
                  key: 'delete',
                  danger: true,
                  onClick: (e) => {
                    e.domEvent.stopPropagation();
                    onDelete(item);
                  },
                },
              ]
            : []),
        ];

        if(menuItems.length===0)
          return <></>
          
        return (
          <Dropdown
            trigger={['click']}
            placement="bottomRight"
            menu={{
              onClick: ({ domEvent }) => {
                domEvent.stopPropagation();
              },
              items: menuItems,
            }}
          >
            <Button
              type="text"
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
              }}
            >
              <MoreOutlined />
            </Button>
          </Dropdown>
        );
      },
    },
  ];

  if (loading === true) return <Loader />;

  return (
    <TableStyled
      rowClassName={(record: DocumentModel) => (record.is_new ? 'new' : '')}
      dataSource={documents}
      columns={columns.filter((col) => !hideColumns.find((hideCol) => col.dataIndex === hideCol))}
      pagination={false}
      onRow={(record: DocumentModel) => ({
        onClick: () => {
          onClickRow(record);
        },
      })}
    />
  );
};

export default DocumentTable;
