import { FileOutlined, FolderOutlined, MessageOutlined, MoreOutlined } from '@ant-design/icons';
import { Badge, Button, Dropdown, MenuProps, Space, Tag, theme } from 'antd';
import dayjs from 'dayjs';
import useTranslation from 'next-translate/useTranslation';
import { FeedStatModel, DocumentOrFolderModel, UserMiniModel, DocumentType } from '~/__generated__/types';
import { AlignType } from 'rc-table/lib/interface';
import { useRouter } from 'next/router';
import { TableStyled } from '../../Common/table.styles';
import Loader from '../../Common/loader/loader';
import useDocuments from '../useDocuments';
import { UserLink } from '../../Common/links/userLink/userLink';

interface Props {
  documents: DocumentOrFolderModel[];
  loading: boolean;
  hideColumns: string[];
  canManage: boolean;
  onClickRow: (document: DocumentOrFolderModel) => void;
  onEdit: (document: DocumentOrFolderModel) => void;
  onMove?: (document: DocumentOrFolderModel) => void;
  onDelete: (document: DocumentOrFolderModel) => void;
}

const DocumentOrFolderTable = ({
  documents,
  loading = false,
  hideColumns,
  canManage,
  onClickRow,
  onEdit,
  onMove,
  onDelete,
}: Props) => {
  const { t } = useTranslation('common');
  const { token } = theme.useToken();
  const router = useRouter();
  const [goToDocument] = useDocuments();

  const columns = [
    {
      dataIndex: 'title',
      title: t('title'),
      ellipsis: true,
      render: (title: string, item: DocumentOrFolderModel) => {
        return (
          <Space>
            {item.type ? <FileOutlined /> : <FolderOutlined />}
            {title}
          </Space>
        );
      },
    },
    {
      dataIndex: 'type',
      title: t('type'),
      width: 120,
      render: (type: string) => {
        return type ? <Tag>{t(`documentType.${type}`)}</Tag> : <Tag>{t('folder.one')}</Tag>;
      },
    },
    {
      dataIndex: 'updated',
      title: t('modified'),
      width: 120,
      render: (updated: string) => {
        return updated ? dayjs(updated).format('DD.MM.YYYY') : '';
      },
    },
    {
      dataIndex: 'created_by',
      title: t('createdBy'),
      width: 120,
      render: (created_by: UserMiniModel) => {
        return <UserLink user={created_by} />;
      },
    },
    {
      dataIndex: 'feed_stats',
      align: 'left' as AlignType,
      width: 80,
      render: (feed_stats: FeedStatModel, item: DocumentOrFolderModel) => {
        if (!feed_stats || item?.type !== DocumentType.Jogldoc) return null;
        return (
          <Button type="text" onClick={() => goToDocument(item.id, 'discussion')}>
            <Badge
              count={feed_stats.new_mention_count + feed_stats.new_thread_activity_count}
              size="small"
              offset={[4, -4]}
              color={token.colorPrimary}
            >
              <Space>
                <MessageOutlined />
                {feed_stats.post_count}
              </Space>
            </Badge>
          </Button>
        );
      },
    },
    {
      dataIndex: 'id',
      align: 'right' as AlignType,
      width: 80,
      render: (id: string, item: DocumentOrFolderModel) => {
        if (!canManage) return null;
        const menuItems: MenuProps['items'] = [
          {
            label: t('action.edit'),
            key: 'edit',
            onClick: (e) => {
              e.domEvent.stopPropagation();
              onEdit(item);
            },
          },

          ...(onMove
            ? [
                {
                  label: t('action.move'),
                  key: 'move',
                  onClick: (e) => {
                    e.domEvent.stopPropagation();
                    onMove(item);
                  },
                },
              ]
            : []),
          {
            label: t('action.delete'),
            key: 'delete',
            danger: true,
            onClick: (e) => {
              e.domEvent.stopPropagation();
              onDelete(item);
            },
          },
        ];

        return (
          <Dropdown
            trigger={['click']}
            placement="bottomRight"
            menu={{
              onClick: ({ domEvent }) => {
                domEvent.stopPropagation();
              },
              items: menuItems,
            }}
          >
            <Button
              type="text"
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
              }}
            >
              <MoreOutlined />
            </Button>
          </Dropdown>
        );
      },
    },
  ];

  if (loading === true) return <Loader />;

  return (
    <TableStyled
      rowClassName={(record: DocumentOrFolderModel) => (record.is_new ? 'new' : '')}
      dataSource={documents}
      columns={columns.filter((col) => !hideColumns.find((hideCol) => col.dataIndex === hideCol))}
      pagination={false}
      onRow={(record: DocumentOrFolderModel) => ({
        onClick: () => {
          onClickRow(record);
        },
      })}
    />
  );
};

export default DocumentOrFolderTable;
