import { useMutation, useQuery } from '@tanstack/react-query';
import { Flex, Typography, message, Button, Input } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useState } from 'react';
import {
  DocumentInsertModel,
  DocumentModel,
  DocumentOrFolderModel,
  DocumentType,
  DocumentUpdateModel,
} from '~/__generated__/types';
import api from '~/src/utils/api/api';
import type { UploadFile } from 'antd';
import { convertBase64 } from '~/src/utils/utils';
import EditDocumentModal, { EditDocumentFormFields } from '~/src/components/editDocumentModal/editDocumentModal';
import UploadDocumentModal from '~/src/components/uploadDocumentsModal/uploadDocumentsModal';
import { DocumentPreviewModal } from '~/src/components/documents/documentPreviewModal/documentPreviewModal';
import DocumentTable from '../documentTable/documentTable';
import useDebounce from '~/src/hooks/useDebounce';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { useFeedEntity } from '../../Common/feedEntity/feedEntity.hooks';

interface Props {
  entityId: string;
  canManageDocuments: boolean;
}

const EntityDocumentListSimple = ({ entityId, canManageDocuments }: Props) => {
  const { t } = useTranslation('common');
  const [openUploadModal, setOpenUploadModal] = useState(false);
  const [previewDocument, setPreviewDocument] = useState<DocumentModel | null>(null);
  const [editDocument, setEditDocument] = useState<DocumentModel | null>(null);
  const [markFeedAsOpened] = useFeedEntity();

  const [search, setSearch] = useState('');
  const debouncedSearch = useDebounce(search, 300);
  const { data, isLoading, refetch } = useQuery({
    queryKey: [QUERY_KEYS.entityDocumentList, entityId, debouncedSearch],
    queryFn: async () => {
      const response = await api.documents.documentsDetail(entityId, { Search: debouncedSearch });
      return response.data;
    },
  });

  const updateFile = useMutation({
    mutationFn: async ({
      document,
      updateValues,
    }: {
      document: DocumentOrFolderModel;
      updateValues: Partial<DocumentUpdateModel>;
    }) => {
      const { id, type, ...rest } = document;
      await api.documents.documentsUpdateDetail(id, {
        ...rest,
        type: type as DocumentType,
        ...updateValues,
      });
    },
    onSuccess: () => {
      message.success(t('fileUpdateSuccess'));
      refetch();
      setEditDocument(null);
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const deleteFile = useMutation({
    mutationFn: async (id: string) => {
      await api.documents.documentsDeleteDetail(id);
    },
    onSuccess: () => {
      message.success(t('documentDeleteSuccess'));
      refetch();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const createDocument = useMutation({
    mutationFn: async (params: DocumentInsertModel) => {
      await api.documents.documentsCreate(entityId, params);
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  return (
    <>
      <Flex vertical gap="middle">
        <Typography.Title level={3}>{t('document.other')}</Typography.Title>
        <Flex justify="stretch" gap="large">
          <Input.Search
            placeholder={t('action.searchForDocuments')}
            onChange={(e) => {
              setSearch(e.target.value);
            }}
            onSearch={(value) => {
              setSearch(value);
            }}
          />
          {canManageDocuments && <Button onClick={() => setOpenUploadModal(true)}>{t('action.add')}</Button>}
        </Flex>
        <DocumentTable
          hideColumns={['feed_entity', 'created_by']}
          documents={data ?? []}
          loading={isLoading}
          canManage={canManageDocuments}
          onClickRow={(document) => {
            markFeedAsOpened(document);
            setPreviewDocument(document);
          }}
          onEdit={(document) => {
            setEditDocument(document);
          }}
          onDelete={(document) => {
            deleteFile.mutate(document.id);
          }}
        />
        {editDocument?.title}
        {editDocument?.type === DocumentType.Document && (
          <EditDocumentModal
            document={editDocument}
            onClose={() => {
              setEditDocument(null);
            }}
            onConfirm={(params: EditDocumentFormFields) => {
              updateFile.mutate({
                document: editDocument,
                updateValues: params,
              });
            }}
          />
        )}
        {openUploadModal && (
          <UploadDocumentModal
            onClose={() => {
              setOpenUploadModal(false);
            }}
            onCreate={async (fileList: UploadFile[]) => {
              await Promise.all(
                fileList.map(async (file) => {
                  const base64 = await convertBase64(file);
                  await createDocument.mutateAsync({
                    title: file.name,
                    file_name: file.name,
                    type: DocumentType.Document,
                    folder_id: null,
                    data: base64 as string,
                  });
                })
              ).then(() => {
                message.success(t('documentCreateSuccess', { count: fileList.length }));
                setOpenUploadModal(false);
                refetch();
              });
            }}
          />
        )}
      </Flex>

      {previewDocument && <DocumentPreviewModal document={previewDocument} onClose={() => setPreviewDocument(null)} />}
    </>
  );
};

export default EntityDocumentListSimple;
