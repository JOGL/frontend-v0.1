import styled from '@emotion/styled';
import { Breadcrumb } from 'antd';

export const BreadcrumbStyled = styled(Breadcrumb)`
  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;
