import { useMutation, useQuery } from '@tanstack/react-query';
import { Flex, Typography, message, Input, Grid } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useState } from 'react';
import {
  DocumentInsertModel,
  DocumentModel,
  DocumentOrFolderModel,
  DocumentType,
  DocumentUpdateModel,
  FolderUpsertModel,
} from '~/__generated__/types';
import api from '~/src/utils/api/api';
import { BreadcrumbStyled } from './entityDocumentList.styles';
import { HomeOutlined } from '@ant-design/icons';
import FolderModal from '../../folderModal/folderModal';
import LinkModal, { LinkFormFields } from '../../linkModal/linkModal';
import EditDocumentModal, { EditDocumentFormFields } from '../../editDocumentModal/editDocumentModal';
import type { UploadFile } from 'antd';
import { convertBase64 } from '~/src/utils/utils';
import MoveToFolderModal from '../../moveToFolderModal/moveToFolderModal';
import { DocumentPreviewModal } from '../documentPreviewModal/documentPreviewModal';
import DocumentsMenu from '../documentsMenu/documentsMenu';
import UploadDocumentModal from '../../uploadDocumentsModal/uploadDocumentsModal';
import DocumentOrFolderTable from '../documentOrFolderTable/documentOrFolderTable';
import useDebounce from '~/src/hooks/useDebounce';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import useDocuments from '../useDocuments';
import MobileCardView from '../../Common/mobileCardView/mobileCardView';
import { useFeedEntity } from '../../Common/feedEntity/feedEntity.hooks';
const { useBreakpoint } = Grid;
const ROOT = 'ROOT';

interface Props {
  entityId: string;
  canManageDocuments: boolean;
}

const EntityDocumentList = ({ entityId, canManageDocuments }: Props) => {
  const { t } = useTranslation('common');
  const [goToDocument] = useDocuments();
  const [openCreateFolderModal, setOpenCreateFolderModal] = useState(false);
  const [openCreateLinkModal, setOpenCreateLinkModal] = useState(false);
  const [openUploadModal, setOpenUploadModal] = useState(false);
  const [previewDocument, setPreviewDocument] = useState<DocumentOrFolderModel | null>(null);
  const [selectedFolder, setSelectedFolder] = useState(ROOT);
  const [moveDocument, setMoveDocument] = useState<DocumentOrFolderModel | null>(null);
  const [editDocument, setEditDocument] = useState<DocumentOrFolderModel | null>(null);
  const [path, setPath] = useState([ROOT]);
  const screens = useBreakpoint();
  const isMobile = screens.xs;
  const [markFeedAsOpened] = useFeedEntity();

  const docData = {
    title: t('untitledPage'),
    type: DocumentType.Jogldoc,
    folder_id: selectedFolder !== ROOT ? selectedFolder : undefined,
  };

  const [search, setSearch] = useState('');
  const debouncedSearch = useDebounce(search, 300);
  const { data, isLoading, refetch } = useQuery({
    queryKey: [QUERY_KEYS.entityDocumentList, entityId, debouncedSearch],
    queryFn: async () => {
      const response = await api.documents.documentsAllDetail(entityId, { Search: debouncedSearch });
      return response.data;
    },
  });

  const mutation = useMutation({
    mutationFn: async () => {
      const response = await api.documents.documentsCreate(entityId, docData);
      return response.data;
    },
    onSuccess: (id) => {
      goToDocument(id);
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const createFolder = useMutation({
    mutationFn: async (params: FolderUpsertModel) => {
      await api.documents.foldersCreateDetail(entityId, params);
    },
    onSuccess: () => {
      message.success(t('folderCreateSuccess'));
      setOpenCreateFolderModal(false);
      refetch();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const createLinkOrDocument = useMutation({
    mutationFn: async (params: DocumentInsertModel) => {
      await api.documents.documentsCreate(entityId, params);
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const deleteFolder = useMutation({
    mutationFn: async (id: string) => {
      await api.documents.foldersDeleteDetail(id);
    },
    onSuccess: () => {
      message.success(t('folderDeleteSuccess'));
      if (selectedFolder !== ROOT) {
        setSelectedFolder(ROOT);
        setPath([ROOT]);
      }
      refetch();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const updateFile = useMutation({
    mutationFn: async ({
      document,
      updateValues,
    }: {
      document: DocumentOrFolderModel;
      updateValues: Partial<DocumentUpdateModel>;
    }) => {
      const { id, type, ...rest } = document;
      await api.documents.documentsUpdateDetail(id, {
        ...rest,
        type: type as DocumentType,
        ...updateValues,
      });
    },
    onSuccess: () => {
      message.success(t('fileUpdateSuccess'));
      refetch();
      setMoveDocument(null);
      setEditDocument(null);
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const updateFolder = useMutation({
    mutationFn: async ({
      folder,
      updateValues,
    }: {
      folder: DocumentOrFolderModel;
      updateValues: Partial<FolderUpsertModel>;
    }) => {
      await api.documents.foldersUpdateDetail(folder.id, {
        name: folder.title,
        parent_folder_id: folder.parent_folder_id,
        ...updateValues,
      });
    },
    onSuccess: () => {
      message.success(t('folderUpdateSuccess'));
      refetch();
      setMoveDocument(null);
      setEditDocument(null);
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const deleteFile = useMutation({
    mutationFn: async (id: string) => {
      await api.documents.documentsDeleteDetail(id);
    },
    onSuccess: () => {
      message.success(t('documentDeleteSuccess'));
      refetch();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const documentsByFolder = data?.reduce((acc, documentOrFolder) => {
    const folderId = documentOrFolder?.parent_folder_id || documentOrFolder?.folder_id || ROOT;
    acc[folderId] = [...(acc[folderId] || []), documentOrFolder];
    return acc;
  }, {});

  const allDocuments = data?.reduce<DocumentOrFolderModel[]>((acc, item) => {
    if (!item.is_folder) {
      acc.push(item);
    }
    return acc;
  }, []);

  return (
    <>
      <Flex vertical gap="middle">
        <Typography.Title level={3}>{t('document.other')}</Typography.Title>
        <Flex justify="stretch" gap="large">
          <Input.Search
            placeholder={t('action.searchForDocuments')}
            onChange={(e) => {
              setSearch(e.target.value);
            }}
            onSearch={(value) => {
              setSearch(value);
            }}
          />
          {canManageDocuments && (
            <DocumentsMenu
              onAddFolder={!isMobile ? () => setOpenCreateFolderModal(true) : undefined}
              onAddLink={() => setOpenCreateLinkModal(true)}
              onAddJoglDoc={() => {
                mutation.mutate();
              }}
              onAddDocument={() => setOpenUploadModal(true)}
            />
          )}
        </Flex>
        {!isMobile && (
          <BreadcrumbStyled
            items={path.map((pathItem) => {
              if (pathItem === ROOT) {
                return {
                  title: <HomeOutlined />,
                  onClick: () => {
                    setSelectedFolder(ROOT);
                    setPath([ROOT]);
                  },
                };
              }
              const folder = data?.find(({ id }) => id === pathItem);
              return {
                title: folder?.title,
                onClick: () => {
                  if (folder?.id) {
                    const index = path.indexOf(folder?.id) + 1;
                    setSelectedFolder(folder?.id);
                    setPath((prev) => [...prev.slice(0, index)]);
                  }
                },
              };
            })}
          />
        )}
      </Flex>
      {isMobile ? (
        <MobileCardView
          data={allDocuments ?? []}
          loading={isLoading}
          onClickRow={(document: DocumentModel) => {
            if (document?.type === DocumentType.Jogldoc) {
              goToDocument(document.id);
              return;
            }
            if (document?.type === DocumentType.Link && document.url) {
              window.open(document?.url, '_blank');
              return;
            }
            setPreviewDocument(document);
            markFeedAsOpened(document);
          }}
        />
      ) : (
        <DocumentOrFolderTable
          documents={documentsByFolder ? documentsByFolder[selectedFolder] : []}
          loading={isLoading}
          canManage={canManageDocuments}
          onClickRow={(document) => {
            if (document.is_folder) {
              setSelectedFolder(document.id);
              setPath((prev) => [...prev, document.id]);
              return;
            }
            if (document?.type === DocumentType.Jogldoc) {
              goToDocument(document.id);
              return;
            }
            if (document?.type === DocumentType.Link && document.url) {
              window.open(document?.url, '_blank');
              return;
            }
            setPreviewDocument(document);
            markFeedAsOpened(document);
          }}
          onEdit={(document) => {
            if (document?.type === DocumentType.Jogldoc) {
              goToDocument(document.id);
              return;
            }
            setEditDocument(document);
          }}
          onMove={(document) => {
            setMoveDocument(document);
          }}
          onDelete={(document) => {
            if (document.is_folder) {
              deleteFolder.mutate(document.id);
            } else {
              deleteFile.mutate(document.id);
            }
          }}
          hideColumns={['space']}
        />
      )}
      {(openCreateFolderModal || editDocument?.is_folder) && (
        <FolderModal
          folder={editDocument}
          onClose={() => {
            setOpenCreateFolderModal(false);
            setEditDocument(null);
          }}
          onConfirm={(name: string) => {
            if (editDocument) {
              updateFolder.mutate({
                folder: editDocument,
                updateValues: { name },
              });
            } else {
              createFolder.mutate({ name, parent_folder_id: selectedFolder === ROOT ? null : selectedFolder });
            }
          }}
        />
      )}
      {(openCreateLinkModal || editDocument?.type === DocumentType.Link) && (
        <LinkModal
          link={editDocument}
          onClose={() => {
            setOpenCreateLinkModal(false);
            setEditDocument(null);
          }}
          onConfirm={(data: LinkFormFields) => {
            if (editDocument) {
              updateFile.mutate({
                document: editDocument,
                updateValues: data,
              });
            } else {
              createLinkOrDocument
                .mutateAsync({
                  ...data,
                  type: DocumentType.Link,
                  folder_id: selectedFolder === ROOT ? null : selectedFolder,
                })
                .then(() => {
                  message.success(t('documentCreateSuccess.one'));
                  setOpenCreateLinkModal(false);
                  refetch();
                });
            }
          }}
        />
      )}
      {editDocument?.type === DocumentType.Document && (
        <EditDocumentModal
          document={editDocument}
          onClose={() => {
            setEditDocument(null);
          }}
          onConfirm={(params: EditDocumentFormFields) => {
            updateFile.mutate({
              document: editDocument,
              updateValues: params,
            });
          }}
        />
      )}
      {openUploadModal && (
        <UploadDocumentModal
          onClose={() => {
            setOpenUploadModal(false);
          }}
          onCreate={async (fileList: UploadFile[]) => {
            await Promise.all(
              fileList.map(async (file) => {
                const base64 = await convertBase64(file);
                await createLinkOrDocument.mutateAsync({
                  title: file.name,
                  file_name: file.name,
                  type: DocumentType.Document,
                  folder_id: selectedFolder === ROOT ? null : selectedFolder,
                  data: base64 as string,
                });
              })
            ).then(() => {
              message.success(t('documentCreateSuccess', { count: fileList.length }));
              setOpenUploadModal(false);
              refetch();
            });
          }}
        />
      )}
      {moveDocument && (
        <MoveToFolderModal
          currentFolder={data?.find(({ id }) => id === selectedFolder)}
          currentDocument={moveDocument}
          documentsByFolder={documentsByFolder}
          onClose={() => setMoveDocument(null)}
          onMove={(folderId) => {
            if (moveDocument.is_folder) {
              updateFolder.mutate({ folder: moveDocument, updateValues: { parent_folder_id: folderId } });
            } else {
              updateFile.mutate({ document: moveDocument, updateValues: { folder_id: folderId } });
            }
          }}
        />
      )}
      {previewDocument && <DocumentPreviewModal document={previewDocument} onClose={() => setPreviewDocument(null)} />}
    </>
  );
};

export default EntityDocumentList;
