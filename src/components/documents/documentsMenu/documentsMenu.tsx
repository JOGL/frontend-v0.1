import { Button, Dropdown, Space } from 'antd';
import useTranslation from 'next-translate/useTranslation';

import { EditOutlined, FolderOutlined, LinkOutlined, UploadOutlined } from '@ant-design/icons';

interface Props {
  onAddFolder?: () => void;
  onAddLink: () => void;
  onAddJoglDoc: () => void;
  onAddDocument: () => void;
}
const DocumentsMenu = ({ onAddFolder, onAddLink, onAddJoglDoc, onAddDocument }: Props) => {
  const { t } = useTranslation('common');

  const menuItems = [
    ...(onAddFolder
      ? [
          {
            label: (
              <Space>
                <FolderOutlined />
                {t('folder.one')}
              </Space>
            ),
            key: 'folder',
            onClick: onAddFolder,
          },
        ]
      : []),
    {
      label: (
        <Space>
          <LinkOutlined />
          {t('link.one')}
        </Space>
      ),
      key: 'link',
      onClick: onAddLink,
    },
    {
      label: (
        <Space>
          <EditOutlined />
          {t('page')}
        </Space>
      ),
      key: 'page',
      onClick: onAddJoglDoc,
    },
    {
      label: (
        <Space>
          <UploadOutlined />
          {t('action.upload')}
        </Space>
      ),
      key: 'upload',
      onClick: onAddDocument,
    },
  ];

  return (
    <Dropdown
      placement="bottomRight"
      trigger={['click']}
      menu={{
        items: menuItems,
      }}
    >
      <Button>{t('action.add')}</Button>
    </Dropdown>
  );
};

export default DocumentsMenu;
