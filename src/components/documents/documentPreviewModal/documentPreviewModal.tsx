import { Alert } from 'antd';
import { DocumentModel, DocumentOrFolderModel } from '~/__generated__/types';
import { useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import Loader from '../../Common/loader/loader';
import useTranslation from 'next-translate/useTranslation';
import MediaPreview from '../../media/mediaPreview/mediaPreview';
import FilePreview from '../../file/filePreview/filePreview';

interface Props {
  document: DocumentOrFolderModel | DocumentModel;
  onClose: () => void;
}

export const DocumentPreviewModal = ({ document, onClose }: Props) => {
  const { t } = useTranslation('common');
  const { data, isLoading, error } = useQuery({
    queryKey: [QUERY_KEYS.documentPreview, document.id],
    queryFn: async () => {
      const response = await api.documents.documentsDetailDetail(document.id);
      return response.data;
    },
  });

  if (isLoading && !data) return <Loader />;
  if (error) return <Alert message={t('error.somethingWentWrong')} />;
  if (!data) return null;

  return (
    <>
      {data.is_media ? <MediaPreview media={data} onClose={onClose} /> : <FilePreview file={data} onClose={onClose} />}
    </>
  );
};
