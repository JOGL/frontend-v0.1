import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import { FeedEntityFilter, SortKey } from '~/__generated__/types';

const useDocuments = (): [
  (id: string, tab?: string) => void,
  (key: string) =>  string 
] => {
  const router = useRouter();
  const { t } = useTranslation('common');

  const goToDocument = (id: string, tab?: string) => {
    if (tab) router.push(`/doc/${id}?tab=${tab}`);
    else router.push(`/doc/${id}`);
  };
  
  const getDocumentViewTitle = (key: string): string => {
    switch (key) {
      case 'shared':
        return t('sharedWithYou');
      case 'created':
        return t('createdByYou');
      case 'recent':
        return t('document.recent');
      case 'all':
      default:
        return t('document.all');
    }
  };
  
  return [goToDocument, getDocumentViewTitle];
};

export const getDocumentViewFilter = (key: string): { sortKey: SortKey; filter?: FeedEntityFilter } => {
  switch (key) {
    case 'shared':
      return { sortKey: SortKey.Updateddate, filter: FeedEntityFilter.Sharedwithuser };
    case 'created':
      return { sortKey: SortKey.Updateddate, filter: FeedEntityFilter.Createdbyuser };
    case 'recent':
      return { sortKey: SortKey.Recentlyopened, filter: FeedEntityFilter.Openedbyuser };
    case 'all':
    default:
      return { sortKey: SortKey.Updateddate };
  }
};

export default useDocuments;
