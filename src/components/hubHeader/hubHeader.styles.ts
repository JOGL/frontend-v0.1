import styled from '@emotion/styled';
import { Avatar, Space, Typography } from 'antd';

export const InvitationActionWrapper = styled(Space)`
  margin-left: ${({ theme }) => theme.space[4]};
`;

export const HeaderStyled = styled.header`
  padding: ${({ theme }) => theme.space[6]};
  border-bottom: solid 1px ${({ theme }) => theme.token.colorBorder};
`;

export const HubDescriptionStyled = styled(Typography.Text)`
  white-space: normal;
  margin-bottom: ${({ theme }) => theme.space[4]};
`;

export const AvatarStyled = styled(Avatar)`
  flex-shrink: 0;
`;
