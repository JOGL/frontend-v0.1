import useTranslation from 'next-translate/useTranslation';
import React, { FC, useEffect, useMemo, useState } from 'react';
import BtnJoin from '../Tools/BtnJoin';
import { useRouter } from 'next/router';
import { addressFront, isAdmin, isMember, linkify, toInitials } from '~/src/utils/utils';
import ShowOnboarding from '../Onboarding/ShowOnboarding';
import useUser from '~/src/hooks/useUser';
import tw from 'twin.macro';
import { ContainerMoreModal } from '../Tools/ContainerMoreModal';
import { securityOptionsHub } from '~/src/utils/security_options';
import { usePushNotificationStore } from '~/src/store/push-notifications/PushNotificationsStoreProvider';
import { Flex, Space, Typography, Button, Alert, message, Grid, Tag, Modal } from 'antd';
import { EditOutlined, ShareAltOutlined, UserAddOutlined, UserOutlined } from '@ant-design/icons';
import { AvatarStyled, HeaderStyled, HubDescriptionStyled, InvitationActionWrapper } from './hubHeader.styles';
import { useMutation, useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import ShareModal from '..//shareModal/shareModal';
import { NodeDetailModel } from '~/__generated__/types';
const { useBreakpoint } = Grid;

interface Props {
  hub: NodeDetailModel;
  limitedVisibility: boolean;
}

const HubHeader: FC<Props> = ({ hub, limitedVisibility }) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const screens = useBreakpoint();
  const [showShareModal, setShowShareModal] = useState(false);
  const [showMoreModal, setShowMoreModal] = useState(false);
  const [showOnboardingModalType, setShowOnboardingModalType] = useState<'first_onboarding' | 'second_onboarding'>();
  const [isOnboardingComplete, setIsOnboardingComplete] = useState(hub.user_onboarded);
  const { user } = useUser();
  const { showPushNotificationModal } = usePushNotificationStore((s) => ({
    showPushNotificationModal: s.showPushNotificationModal,
  }));

  const { data: pending_invite, refetch: refreshInvites } = useQuery({
    queryKey: [QUERY_KEYS.nodeInvitationDetails, hub.id],
    queryFn: async () => {
      const response = await api.nodes.invitationDetail(hub.id);
      return response.data;
    },
    enabled: hub.user_joining_restriction_level === 'visitor' || hub.user_access_level === 'pending',
  });

  const showInviteBox = !!(pending_invite && pending_invite?.invitation_type !== 'request');

  const { showRequestToJoinButton, showJoinButton } = useMemo(() => {
    return {
      showRequestToJoinButton:
        hub.onboarding?.enabled &&
        hub.user_access_level === 'visitor' &&
        hub.user_joining_restriction_level !== 'forbidden' &&
        hub.joining_restriction === 'request' &&
        hub.onboarding?.questionnaire.enabled &&
        hub.onboarding?.questionnaire.items.length > 0,

      showJoinButton:
        (hub.user_access_level === 'visitor' || hub.user_access_level === 'pending') &&
        hub.joining_restriction !== 'invite' &&
        hub.user_joining_restriction_level !== null,
    };
  }, [hub]);

  const showOnboardingModal =
    !isAdmin(hub) &&
    hub?.status !== 'draft' &&
    isMember(hub) &&
    hub.onboarding?.enabled &&
    !isOnboardingComplete &&
    (hub.onboarding.rules.enabled || hub.onboarding.presentation.enabled);

  // Launch show onboarding modal with intro and/or rules if:
  // 1) onboarding is enabled 2) user is member 3) user didn't complete onboarding yet
  useEffect(() => {
    if (showOnboardingModal) {
      setShowOnboardingModalType('second_onboarding');
    }
  }, [showOnboardingModal]);

  const acceptInvite = useMutation({
    mutationKey: [MUTATION_KEYS.nodeInviteAccept],
    mutationFn: async (invitationId?: string) => {
      if (!invitationId) return;
      const response = await api.nodes.invitesAcceptCreate(hub.id, invitationId);
      return response.data;
    },
    onSuccess: () => {
      refreshInvites();
      showPushNotificationModal(router);
      location.reload();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const rejectInvite = useMutation({
    mutationKey: [MUTATION_KEYS.nodeInviteReject],
    mutationFn: async (invitationId?: string) => {
      if (!invitationId) return;
      const response = await api.nodes.invitesRejectCreate(hub.id, invitationId);
      return response.data;
    },
    onSuccess: (data) => {
      refreshInvites();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const acceptOrRejectInvite = (e, type) => {
    // if function is launched via keypress, execute only if it's the 'enter' key
    if ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) {
      if (type === 'accept') {
        acceptInvite.mutate(pending_invite?.invitation_id);
      }
      if (type === 'reject') {
        rejectInvite.mutate(pending_invite?.invitation_id);
      }
    }
  };

  const handleShareButtonClick = () => {
    // check if browser/phone support the native share api (meaning we are in a mobile)
    if (navigator.share && window?.innerWidth < 700) {
      navigator
        .share({
          title: t('hub.one'),
          text: addressFront + router.asPath,
          url: addressFront + router.asPath,
        })
        .catch(console.error);
    } else {
      setShowShareModal(true);
    }
  };

  const hubDescription = linkify(hub?.short_description);

  return (
    <>
      <HeaderStyled>
        <Flex gap="middle">
          {screens.md && (
            <Flex flex="none">
              <AvatarStyled src={hub.logo_url} size={160}>
                {toInitials(hub.title || t('untitled'))}
              </AvatarStyled>
            </Flex>
          )}
          <Flex vertical gap="small" flex="1">
            <Flex align="center" justify="space-between" gap="middle">
              <Flex align="center" gap="middle">
                {!screens.md && (
                  <AvatarStyled src={hub.logo_url} size={80}>
                    {toInitials(hub.title || t('untitled'))}
                  </AvatarStyled>
                )}
                <Flex vertical>
                  <Typography.Title level={2}>{hub.title}</Typography.Title>
                  <Flex gap="small">
                    <Tag>{t('hub.one').toUpperCase()}</Tag>
                    <Tag>
                      <Space>
                        <UserOutlined />
                        {t(`legacy.role.${hub.user_access_level}`)}
                      </Space>
                    </Tag>
                  </Flex>
                </Flex>
              </Flex>

              {screens.md && <Button icon={<ShareAltOutlined />} onClick={handleShareButtonClick} />}
            </Flex>

            {hubDescription && (
              <HubDescriptionStyled>
                <div dangerouslySetInnerHTML={{ __html: hubDescription }} />
              </HubDescriptionStyled>
            )}

            <Flex gap="small" wrap>
              <Button type="primary" onClick={() => setShowMoreModal(true)}>
                {t('more')}
              </Button>

              {isAdmin(hub) && (
                <Button icon={<EditOutlined />} onClick={() => router.push(`/hub/${hub.id}/edit`)}>
                  {t('action.edit')}
                </Button>
              )}

              {/* Join/request btn, or onboarding modal, or accept/reject buttons */}
              {showInviteBox ? (
                <Alert
                  description={t('youHaveBeenInvitedToJoinThisContainer', { container: t('hub.one') })}
                  type="info"
                  action={
                    <InvitationActionWrapper>
                      <Button
                        size="small"
                        type="primary"
                        loading={acceptInvite.isPending}
                        onClick={() => acceptInvite.mutate(pending_invite?.invitation_id)}
                      >
                        {t('action.accept')}
                      </Button>
                      <Button
                        size="small"
                        danger
                        ghost
                        loading={rejectInvite.isPending}
                        onClick={() => rejectInvite.mutate(pending_invite?.invitation_id)}
                      >
                        {t('action.reject')}
                      </Button>
                    </InvitationActionWrapper>
                  }
                />
              ) : // if onboarding questionnaire is enabled, and multiple other conditions, show button to open onboarding questionnaire modal
              showRequestToJoinButton ? (
                <Button
                  icon={<UserAddOutlined />}
                  type="primary"
                  ghost
                  onClick={() => {
                    setShowOnboardingModalType('first_onboarding');
                  }}
                >
                  {t('action.requestToJoin')}
                </Button>
              ) : (
                // else show the basic "join" button (can be open or request to join)
                showJoinButton && (
                  <BtnJoin
                    joinType={hub.joining_restriction}
                    itemType="nodes"
                    itemId={hub.id}
                    textJoin={hub.joining_restriction === 'open' ? t('action.join') : t('action.requestToJoin')}
                    count={hub.stats.members_count}
                    pending_invite={pending_invite}
                    showMembersModal={() =>
                      router.push(`/hub/${router.query.id}?tab=members`, undefined, { shallow: true })
                    }
                    hasNoStat
                    customCss={[tw`text-primary text-sm font-semibold uppercase rounded-md border-primary`]}
                  />
                )
              )}
              {!screens.md && <Button icon={<ShareAltOutlined />} onClick={handleShareButtonClick} />}
            </Flex>
          </Flex>
        </Flex>
      </HeaderStyled>
      {showShareModal && (
        <ShareModal
          id={hub.id}
          type="node"
          title={t('action.shareItem', {
            item: t('hub.one'),
          })}
          onClose={() => setShowShareModal(false)}
        />
      )}
      {showOnboardingModalType && (
        <Modal
          open
          title={showOnboardingModalType === 'first_onboarding' ? t('answeringQuestions') : t('welcome')}
          onCancel={() => setShowOnboardingModalType(undefined)}
          closable={!(showOnboardingModalType === 'second_onboarding')}
          footer={null}
        >
          <ShowOnboarding
            object={hub}
            itemType="nodes"
            type={showOnboardingModalType}
            setIsOnboardingComplete={(isOnboardingComplete: boolean) => {
              setShowOnboardingModalType(undefined);
              setIsOnboardingComplete(isOnboardingComplete);
            }}
          />
        </Modal>
      )}
      {showMoreModal && (
        <Modal open onCancel={() => setShowMoreModal(false)} closable={false} footer={null} width={890}>
          <ContainerMoreModal
            container={hub}
            containerType="hub"
            limitedVisibility={limitedVisibility}
            securityOptions={securityOptionsHub}
            showJoinButton={showJoinButton}
            showRequestToJoinButton={showRequestToJoinButton}
            showInviteBox={showInviteBox}
            pendingInvite={pending_invite}
            acceptOrRejectInvite={acceptOrRejectInvite}
            setIsOnboardingComplete={setIsOnboardingComplete}
          />
        </Modal>
      )}
    </>
  );
};

export default HubHeader;
