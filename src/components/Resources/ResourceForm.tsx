/* eslint-disable @rushstack/no-null */
import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
/** * Validators ** */
import FormValidator from '~/src/components/Tools/Forms/FormValidator';
import FormWysiwygComponent from '../Tools/Forms/FormWysiwygComponent';
import FormDefaultComponent from '../Tools/Forms/FormDefaultComponent';
import Button from '~/src/components/primitives/Button';
import { confAlert } from '~/src/utils/utils';
import resourceFormRules from './resourceFormRules.json';
import ImageUploadComponent from '../JOGLDoc/ImageUploadComponent';
import FormDropdownComponent from '../Tools/Forms/FormDropdownComponent';
import tw from 'twin.macro';

interface Props {
  action: 'update' | 'create';
  resource: any;
  userId?: string;
  uploading: boolean;
  handleChange: (key, content) => void;
  handleSubmit: () => void;
}

const FormKeys = ['title', 'type', 'description', 'image_url'];

const ResourceForm: FC<Props> = ({
  action = 'create',
  resource = {},
  uploading = false,
  handleChange,
  handleSubmit,
}) => {
  const { t } = useTranslation('common');
  const validator = new FormValidator(resourceFormRules, t);
  const validation: any = validator.validate(resource);

  const handleChangeResource = (key, content) => {
    if (key === 'image_id') {
      handleChange('image_url', content?.id ? `${process.env.ADDRESS_BACK}/images/${content?.id}/full` : null);
      handleChange('image_url_sm', content?.id ? `${process.env.ADDRESS_BACK}/images/${content?.id}/tn` : null);
      handleChange('image_id', content?.id ?? null);
    } else {
      handleChange(key, content);
    }
  };

  const handleSubmitResource = (event) => {
    event.preventDefault();
    /* Validators control before submit */
    if (!validation.isValid) {
      const touched = {};
      for (let item of FormKeys) {
        touched[item] = { touch: true };
      }
      handleChange('touch', touched);
      confAlert.fire({ icon: 'error', title: 'Complete the form' });
      return;
    }

    handleSubmit();
  };

  const renderInvalid = (valid_obj, key, display = true) => {
    if (valid_obj && resource?.touch && resource?.touch[key] && resource?.touch[key]?.touch) {
      if (valid_obj.message !== '') {
        return (
          <div className="invalid-feedback" style={{ opacity: display ? '1' : '0' }}>
            {t(valid_obj.message)}
          </div>
        );
      }
    }
    // eslint-disable-next-line @rushstack/no-null
    return null;
  };

  return (
    <div tw="md:px-10 md:pt-6">
      <div tw="flex flex-row gap-2 justify-between py-2 px-3 mb-4 flex-wrap border[1px solid #D1D5DB]">
        <div tw="self-start w-[200px]">
          <FormDropdownComponent
            id="resourceType"
            title={t('type')}
            content={resource?.type}
            mandatory={true}
            options={['funding', 'hardware', 'software', 'wetware', 'code', 'dataset', 'service', 'subscription']}
            onChange={(k, v) => {
              handleChangeResource('type', v);
            }}
          />
          {renderInvalid(validation?.type, 'type')}
        </div>
        <div tw="self-end w-[200px]">
          <FormDropdownComponent
            id="resourceCondition"
            title={t('condition')}
            content={resource?.condition}
            options={['free', 'for_members', 'for_a_fee', 'call_for_proposals', 'no_conditions']}
            onChange={(k, v) => {
              handleChangeResource('condition', v);
            }}
          />
          {renderInvalid(validation?.type, 'type', false)}
        </div>
      </div>
      <div tw="grid">
        <FormDefaultComponent
          id="title"
          content={resource?.title}
          title={t('resourceModalTitle')}
          placeholder={t('resourceModalTitle')}
          onChange={handleChangeResource}
          mandatory={true}
          maxChar={80}
        />
        {renderInvalid(validation?.title, 'title')}
      </div>

      <div tw="grid">
        <ImageUploadComponent
          imageUrl={resource?.image_url ?? '/images/default/default-resource.png'}
          itemId={resource?.id}
          title={t('resourceModalImage')}
          onChange={handleChangeResource}
        />
        {renderInvalid(validation?.image_url, 'image_url')}
      </div>

      <div tw="grid">
        <FormWysiwygComponent
          id="description"
          content={resource?.description}
          title={t('resourceModalDescription')}
          tw="shadow-sm pb-0"
          mandatory={true}
          placeholder={t('resourceModalDescription')}
          onChange={handleChangeResource}
        />
        {renderInvalid(validation?.description, 'description')}
      </div>

      <div tw="mt-6 mb-2 text-center space-x-4">
        <Button disabled={!!uploading} onClick={handleSubmitResource} type="submit">
          {action === 'update' ? t('action.update') : t('action.create')}
        </Button>
      </div>
    </div>
  );
};
export default ResourceForm;
