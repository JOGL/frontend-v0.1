import { FC, useMemo, useState } from 'react';
import Loading from '../Tools/Loading';
import useUserData from '~/src/hooks/useUserData';
import { useApi } from '~/src/contexts/apiContext';
import ResourceForm from './ResourceForm';
import { confAlert } from '~/src/utils/utils';
import { ItemType } from '~/src/types';

interface ResourceCreateProps {
  action?: 'create' | 'update';
  closeModal?: () => void;
  entityId: string;
  entityType: ItemType;
  refetchResult?: () => void;
  editResource?: any;
}

const ResourceCreate: FC<ResourceCreateProps> = ({
  entityId,
  entityType,
  editResource = null,
  action = 'create',
  refetchResult,
  closeModal,
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const { userData } = useUserData();
  const api = useApi();

  const [resource, setResource] = useState(
    useMemo(
      () => ({
        condition: 'free',
        ...(editResource ?? {}),
      }),
      []
    )
  );

  const handleChange = (key, content) => {
    setResource((prevResource) => ({ ...prevResource, [key]: content }));
  };

  const handleSubmit = async () => {
    try {
      setIsLoading(true);
      if (editResource && editResource?.id) {
        await api.put(`/${entityType}/resources/${editResource?.id}`, resource);
      } else {
        await api.post(`/${entityType}/${entityId}/resources`, resource);
      }
      refetchResult && refetchResult();
      confAlert.fire({ icon: 'success', title: 'Resource was successfully created!' });
      closeModal && closeModal();
    } catch (e) {
      confAlert.fire({ icon: 'error', title: 'Unknown error occured!' });
    }
    setIsLoading(false);
  };

  return (
    <Loading active={isLoading}>
      <ResourceForm
        action={action}
        resource={resource}
        userId={userData?.id}
        handleChange={handleChange}
        handleSubmit={handleSubmit}
        uploading={isLoading}
      />
    </Loading>
  );
};

export default ResourceCreate;
