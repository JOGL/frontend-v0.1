import { FC, forwardRef, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { ItemType } from '~/src/types';
import ResourceCreate from './ResourceCreate';
import { DocumentCard } from '../Tools/createDocument/DocumentCard';
import { useApi } from '~/src/contexts/apiContext';
import Icon from '../primitives/Icon';
import { keepPreviousData, useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import { SpaceTabHeader } from '../Common/space/spaceTabHeader/spaceTabHeader';
import { Empty, Flex, Modal } from 'antd';
import Loading from '../Tools/Loading';
import { SpaceContentStyled } from '../Common/space/space.styles';
import { Permission } from '~/__generated__/types';
import useDebounce from '~/src/hooks/useDebounce';

interface ResourceListProps {
  itemType: ItemType;
  entity: any;
  isAdmin?: boolean;
  shouldForceRefresh?: boolean;
  showResourcesTabDisclaimerToAdmin?: boolean;
}

const ResourceList: FC<ResourceListProps> = forwardRef(
  ({ itemType, entity, isAdmin = false, shouldForceRefresh = false, showResourcesTabDisclaimerToAdmin = false }) => {
    const { t } = useTranslation('common');
    const [showModal, setShowModal] = useState(false);
    const [editResource, setEditResource] = useState(null);
    const [searchText, setSearchText] = useState('');
    const debouncedSearch = useDebounce(searchText, 300);

    const legacyApi = useApi();

    const { data, isLoading, refetch, isPlaceholderData } = useQuery({
      queryKey: [QUERY_KEYS.nodeResourceList, debouncedSearch],
      queryFn: async () => {
        const response = await api[itemType].resourcesDetail(entity.id, { Search: debouncedSearch });
        return response.data;
      },
      placeholderData: keepPreviousData,
    });

    const deleteResource = async (id) => {
      try {
        await legacyApi.delete(`/${itemType}/resources/${id}`);
        refetch();
      } catch (e) {
        console.error(e);
      }
    };

    return (
      <div tw="w-full">
        {showResourcesTabDisclaimerToAdmin && (
          <div tw="flex items-center text-sm gap-1 mb-4">
            <Icon icon="mdi:information-outline" tw="flex-none m-0 h-4 w-4" />
            <span>{t('tabHiddenForMembersDisclaimer')}</span>
          </div>
        )}

        <SpaceTabHeader
          title={t('resource.other')}
          searchText={searchText}
          setSearchText={setSearchText}
          canCreate={entity?.user_access?.permissions?.includes(Permission.Postresources) || isAdmin}
          handleClick={() => setShowModal(true)}
          showSearch={!!searchText || !!data?.length}
        />

        <SpaceContentStyled>
          {!isLoading && !data?.length && <Empty />}
          <Flex wrap gap="middle">
            {isLoading && !isPlaceholderData ? (
              <Loading />
            ) : (
              data.map((resource) => (
                <DocumentCard
                  key={`${resource?.id}-${resource?.title}-${resource.description}`}
                  type="resource"
                  title={resource?.title}
                  content={resource?.description}
                  id={resource?.id}
                  showMenu={isAdmin}
                  item={resource}
                  showEdit={true}
                  onClickEdit={() => setEditResource(resource)}
                  deleteCard={() => {
                    deleteResource(resource?.id);
                  }}
                  src={resource?.image_url ?? '/images/default/default-resource.png'}
                />
              ))
            )}
          </Flex>
        </SpaceContentStyled>
        {showModal && (
          <Modal open onCancel={() => setShowModal(false)} title={t('addResource')} footer={null}>
            <ResourceCreate
              entityId={entity?.id}
              entityType={itemType}
              closeModal={() => setShowModal(false)}
              refetchResult={refetch}
            />
          </Modal>
        )}

        {editResource && (
          <Modal open onCancel={() => setEditResource(null)} title={t('editResource')} footer={null}>
            <EditResources
              action="update"
              entityId={entity?.id}
              closeModal={() => setEditResource(null)}
              entityType={itemType}
              refetchResult={refetch}
              editResource={editResource}
            />
          </Modal>
        )}
      </div>
    );
  }
);

interface EditResourcesProps {
  action: 'create' | 'update';
  closeModal: () => void;
  entityId: string;
  refetchResult: () => void;
  editResource: any;
  entityType: ItemType;
}

const EditResources: FC<EditResourcesProps> = ({
  action,
  closeModal,
  entityId,
  refetchResult,
  entityType,
  editResource,
}) => {
  return (
    <ResourceCreate
      action={action}
      entityId={entityId}
      editResource={editResource}
      closeModal={closeModal}
      refetchResult={refetchResult}
      entityType={entityType}
    />
  );
};

export default ResourceList;
