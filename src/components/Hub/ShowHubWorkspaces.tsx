import { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import {
  getContainerAccessLevelChip,
  getContainerJoiningRestrictionChip,
  getContainerVisibilityChip,
} from '~/src/utils/utils';
import Loading from '../Tools/Loading';
import { useRouter } from 'next/router';
import api from '~/src/utils/api/api';
import { useQuery, keepPreviousData } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { Empty, Flex } from 'antd';
import { SpaceTabHeader } from '../Common/space/spaceTabHeader/spaceTabHeader';
import { SpaceContentStyled } from '../Common/space/space.styles';
import { NodeDetailModel, Permission, WorkspaceModel } from '~/__generated__/types';
import useDebounce from '~/src/hooks/useDebounce';
import WorkspaceCard from '../workspace/workspaceCard/workspaceCard';

interface Props {
  hub: NodeDetailModel;
}

export const ShowHubWorkspaces: FC<Props> = ({ hub }) => {
  const { t } = useTranslation('common');
  const [searchText, setSearchText] = useState('');
  const debouncedSearch = useDebounce(searchText, 300);
  const router = useRouter();

  const fetchWorkspaces = async () => {
    const response = await api.nodes.workspacesDetail(hub.id, { Search: debouncedSearch });
    return response.data;
  };

  const { data, isLoading: isLoadingWorkspaces, isPlaceholderData } = useQuery({
    queryKey: [QUERY_KEYS.nodeWorkspacesList, debouncedSearch],
    queryFn: fetchWorkspaces,
    placeholderData: keepPreviousData,
  });

  const isLoading = isLoadingWorkspaces && !isPlaceholderData;

  const createLinkedWorkspace = () => {
    router.push(`/workspace/create?entityId=${hub.id}`);
  };
  return (
    <>
      <SpaceTabHeader
        title={t('workspace.other')}
        searchText={searchText}
        setSearchText={setSearchText}
        canCreate={hub.user_access?.permissions?.includes(Permission.Createworkspaces)}
        handleClick={createLinkedWorkspace}
        showSearch={!!searchText || !!data?.length}
      />

      <SpaceContentStyled>
        {isLoading ? (
          <Loading />
        ) : (
          <>
            {!data?.length && <Empty />}
            <Flex wrap gap="middle">
              {data?.map((workspace: WorkspaceModel) => (
                <WorkspaceCard
                  id={workspace.id}
                  key={workspace.id}
                  title={workspace.title}
                  last_activity={workspace.updated?.toString() ?? ''}
                  short_description={workspace.short_description}
                  membersCount={workspace.stats?.members_count}
                  status={workspace.status}
                  banner_url={workspace.banner_url || workspace.banner_url_sm}
                  chip={workspace?.user_access_level && getContainerAccessLevelChip(workspace)}
                  imageChips={
                    !workspace?.user_access_level
                      ? [getContainerVisibilityChip(workspace), getContainerJoiningRestrictionChip(workspace)]
                      : [getContainerVisibilityChip(workspace)]
                  }
                />
              ))}
            </Flex>
          </>
        )}
      </SpaceContentStyled>
    </>
  );
};
