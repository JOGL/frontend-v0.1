import { TabPanels, TabPanel as SpecialTabPanel, Tabs } from '@reach/tabs';
import ObjectAdminCard from '~/src/components/Tools/ObjectAdminCard';
import Loading from '~/src/components/Tools/Loading';
import { NavTab, TabListStyleNoSticky } from '~/src/components/Tabs/TabsStyles';
import { FC } from 'react';
import Button from '../primitives/Button';
import useTranslation from 'next-translate/useTranslation';
import { useModal } from '~/src/contexts/modalContext';
import { useRouter } from 'next/router';
import { Hub, ItemType } from '~/src/types';
import HubAttachRequest from './HubAttachRequest';
import NoResults from '../Tools/NoResults';

interface Props {
  childId: string;
  childType: ItemType;
  hubs: Hub[];
  hubsmutate: () => void;
  incomingObjInvites: Hub[];
  incomingObjmutate: () => void;
  outgoingObjInvites: Hub[];
  outgoingObjmutate: () => void;
}

const HubManageAffiliation: FC<Props> = ({
  childId,
  childType,
  hubs,
  hubsRevalidate,
  incomingObjInvites,
  incomingObjRevalidate,
  outgoingObjInvites,
  outgoingObjRevalidate,
}) => {
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const router = useRouter();

  const HubAttachRequestButton = () => (
    <Button
      onClick={() => {
        showModal({
          children: (
            <HubAttachRequest
              showModal={showModal}
              alreadyAttachedHubs={hubs}
              itemType={childType}
              itemId={childId}
              callBack={() => {
                closeModal();
                outgoingObjRevalidate();
              }}
            />
          ),
          title: t('linkRequestToHub'),
        });
      }}
      tw="mb-6"
    >
      {t('linkRequestToHub')}
    </Button>
  );

  return (
    <Tabs>
      <TabListStyleNoSticky>
        <NavTab>{t('hub', { count: 2 })}</NavTab>
        <NavTab>
          <div tw="inline-flex items-center justify-center">
            {t('outgoingRequests')}
            <span tw="bg-[#E2E2E2] rounded-full w-7 h-7 ml-2">
              <span tw="flex justify-center">{outgoingObjInvites?.filter((x) => x.type === 'node')?.length}</span>
            </span>
          </div>
        </NavTab>
        <NavTab>
          <div tw="inline-flex items-center justify-center">
            {t('incomingRequests')}
            <span tw="bg-[#E2E2E2] rounded-full w-7 h-7 ml-2">
              <span tw="flex justify-center">{incomingObjInvites?.filter((x) => x.type === 'node').length}</span>
            </span>
          </div>
        </NavTab>
      </TabListStyleNoSticky>
      <TabPanels tw="justify-center">
        {/* Affiliated hubs */}
        <SpecialTabPanel>
          {router.query.id ? (
            <div tw="flex flex-col pb-6">
              <div tw="content-end">
                <HubAttachRequestButton />
                {hubs?.map((hub, i) => (
                  <ObjectAdminCard
                    key={i}
                    object={hub}
                    objUrl={`/hub/${hub.id}`}
                    parentType={childType}
                    parentId={childId}
                    callBack={() => {
                      incomingObjRevalidate();
                      hubsRevalidate();
                    }}
                  />
                ))}
              </div>
            </div>
          ) : null}
        </SpecialTabPanel>
        {/* Outgoing hubs requests/invites */}
        <SpecialTabPanel>
          {router.query.tab && (
            <div>
              <HubAttachRequestButton />
              {outgoingObjInvites?.filter((x) => x.type === 'node')?.length === 0 && <NoResults />}
              {outgoingObjInvites?.filter((x) => x.type === 'node') ? (
                <>
                  {outgoingObjInvites
                    ?.filter((x) => x.type === 'node')
                    ?.map((hub, i) => (
                      <ObjectAdminCard
                        key={i}
                        object={hub}
                        objUrl={`/hub/${hub.id}`}
                        parentType={childType}
                        parentId={childId}
                        callBack={() => {
                          outgoingObjRevalidate();
                          hubsRevalidate();
                        }}
                        type="invite"
                      />
                    ))}
                </>
              ) : (
                <Loading />
              )}
            </div>
          )}
        </SpecialTabPanel>
        {/* Incoming hubs requests */}
        <SpecialTabPanel>
          {router.query.tab && (
            <div>
              {incomingObjInvites?.filter((x) => x.type === 'node')?.length === 0 && <NoResults />}
              {incomingObjInvites?.filter((x) => x.type === 'node') ? (
                <>
                  {incomingObjInvites
                    ?.filter((x) => x.type === 'node')
                    ?.map((hub, i) => (
                      <ObjectAdminCard
                        key={i}
                        object={hub}
                        objUrl={`/hub/${hub.id}`}
                        parentType={childType}
                        parentId={childId}
                        callBack={() => {
                          incomingObjRevalidate();
                          hubsRevalidate();
                        }}
                        type="request"
                      />
                    ))}
                </>
              ) : (
                <Loading />
              )}
            </div>
          )}
        </SpecialTabPanel>
      </TabPanels>
    </Tabs>
  );
};

export default HubManageAffiliation;
