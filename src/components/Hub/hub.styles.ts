import styled from '@emotion/styled';
import { Flex } from 'antd';

export const PageWrapperStyled = styled(Flex)`
  min-height: 80vh;
`;
