import { CSSProperties, FC, useState } from 'react';
import Icon from '../primitives/Icon';
import Button from '../primitives/Button';
import { confAlert } from '~/src/utils/utils';
import { useApi } from '~/src/contexts/apiContext';
import AsyncSelect from 'react-select/async';
import { components, ControlProps } from 'react-select';
import styled from '~/src/utils/styled';
import { Hub } from '~/src/types';
import useTranslation from 'next-translate/useTranslation';

interface HubAttachRequestProps {
  itemId: string;
  itemType: string;
  alreadyAttachedHubs: Hub[];
  callBack: () => void;
  showModal: () => void;
}

const HubAttachRequest: FC<HubAttachRequestProps> = ({
  itemId,
  itemType,
  alreadyAttachedHubs,
  callBack,
  showModal,
}) => {
  const api = useApi();
  const { t } = useTranslation('common');
  const [hubsList, setHubsList] = useState([]);
  const [sending, setSending] = useState(false);

  const searchHub = async (resolve: (value: unknown) => void, value: string) => {
    try {
      let content = (await api.get(`/nodes?Search=${value}&Page=1&PageSize=5`))?.data?.items ?? [];
      content = content
        // filter hubs that were already added
        .filter((x) => !alreadyAttachedHubs?.map((x) => x.id).includes(x.id))
        .map((hub) => {
          return {
            value: hub.id,
            label: hub.title,
            logo_url: hub.banner_url_sm || '/images/default/default-hub.png',
          };
        });
      resolve(content);
    } catch (e) {
      resolve([]);
    }
  };

  const loadOptions = (inputValue) =>
    new Promise((resolve) => {
      searchHub(resolve, inputValue);
    });

  const inviteHub = async (hubId: string) => {
    await api.post(`/${itemType}/${itemId}/communityEntityInvite/${hubId}`);
  };

  const handleChangeHub = (content) => {
    const tempHubsList = [];
    content?.map(function (user) {
      user && tempHubsList.push(user.value);
    });
    setHubsList(tempHubsList);
  };

  const formatOptionLabel = ({ label, logo_url }) => (
    <HubLabel tw="inline-flex items-center">
      <img src={logo_url} />
      <div>{label}</div>
    </HubLabel>
  );

  const handleSubmit = () => {
    const sentInvitation = [];
    setSending(true);
    for (const item of hubsList) {
      sentInvitation.push(inviteHub(item));
    }
    Promise.all(sentInvitation)
      .then(() => {
        confAlert.fire({ icon: 'success', title: `Request sent` });
        callBack();
      })
      .catch((err) => {
        confAlert.fire({
          icon: 'error',
          title:
            err.response.status === 409
              ? t('thisItemIsPendingAdminApproval', { item: t('hub.one') })
              : t('someUnknownErrorOccurred'),
        });
        console.warn(err);
      })
      .finally(() => {
        setSending(false);
      });
  };

  const customStyles: Record<string, (css: CSSProperties) => CSSProperties> = {
    container: (provided) => ({
      ...provided,
      width: '100%',
    }),
    control: (css) => ({
      ...css,
      position: 'relative',
      borderRadius: '4px',
      minHeight: '10px',
    }),
    valueContainer: (css) => ({
      ...css,
    }),
    multiValue: (css) => ({
      ...css,
      '> div': {
        display: 'flex',
        alignItems: 'center',
      },
    }),
  };

  const Control = ({ children, ...props }: ControlProps<any, false>) => {
    return (
      <components.Control {...props}>
        <div tw="bg-[#D0D0D0] p-1 rounded-tl-sm rounded-bl-sm items-center flex justify-center h-[38px] w-[38px]">
          <Icon icon="mdi:at" tw="h-5 w-5 text-[#5959599e] mr-0" />
        </div>
        {children}
      </components.Control>
    );
  };

  return (
    <div>
      <div tw="mb-4">
        <label tw="font-bold" htmlFor="joglUser">
          {t('joglHubs')}
        </label>
        <AsyncSelect
          isMulti
          cacheOptions
          styles={customStyles}
          onChange={handleChangeHub}
          formatOptionLabel={formatOptionLabel}
          placeholder={t('typeToSearchItem', { item: t('hub.other') })}
          loadOptions={loadOptions}
          noOptionsMessage={() => null}
          components={{ DropdownIndicator: null, Control, IndicatorSeparator: null }}
          isClearable
        />
      </div>
      <Button onClick={handleSubmit} disabled={hubsList.length === 0 || sending} className="invite-btn">
        {t('request')}
      </Button>
    </div>
  );
};

const HubLabel = styled.div`
  div {
    font-size: 15px;
    color: #5c5d5d;
  }
  img {
    width: 28px;
    height: 28px;
    border-radius: 50%;
    object-fit: cover;
    margin-right: 6px;
  }
`;

export default HubAttachRequest;
