import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import FormDefaultComponent from '~/src/components/Tools/Forms/FormDefaultComponent';
import FormImgComponent from '~/src/components/Tools/Forms/FormImgComponent';
import { generateSlug } from '~/src/utils/utils';
import isMandatoryField from '~/src/utils/isMandatoryField';
import FormTextAreaComponent from '../Tools/Forms/FormTextAreaComponent';
import FormMissingFieldsList from '../Tools/Forms/FormMissingFieldsList';
import Alert from '../Tools/Alert/Alert';
import { NodeModel } from '~/__generated__/types';

interface Props {
  hub: NodeModel;
  stateValidation: {};
  showErrorList: boolean;
  handleChange: (key: any, content: any) => void;
}

const HubForm: FC<Props> = ({ hub, showErrorList, stateValidation, handleChange }) => {
  const { t } = useTranslation('common');
  const handleChangeHub = (key, content) => {
    if (key === 'title') {
      // generate a shortname when typing a title, and update short_name field with the value
      handleChange('short_title', generateSlug(content));
    }

    handleChange(key, content);
  };

  const { valid_title, valid_short_description } = stateValidation || {};

  return (
    <form className="hubForm">
      {showErrorList && <FormMissingFieldsList stateValidation={stateValidation} />}

      {/* Show warning msg if hub status is draft */}
      {hub.status === 'draft' && <Alert type="warning" message={t('draftWarningMessage')} />}

      {/* Basic fields */}
      <div tw="flex gap-1">
        <div tw="mr-4">
          <FormImgComponent
            itemId={hub?.id}
            maxSizeFile={4194304}
            itemType="hub"
            type="avatar"
            id="logo_id"
            hubTitle={hub?.title || '-'}
            imageUrl={hub?.logo_url}
            onChange={handleChangeHub}
            aspectRatio={1}
          />
        </div>
        <div tw="flex-1">
          <FormDefaultComponent
            id="title"
            content={hub.title}
            title={t('title')}
            onChange={handleChangeHub}
            mandatory={isMandatoryField('hub', 'title')}
            errorCodeMessage={valid_title ? valid_title.message : ''}
            isValid={valid_title ? !valid_title.isInvalid : undefined}
            maxChar={120}
          />
        </div>
      </div>

      <FormImgComponent
        id="banner_id"
        type="banner"
        title={t('hubBanner')}
        itemType="hubs"
        itemId={hub.id}
        imageUrl={hub.banner_url}
        showAddButton={hub?.banner_url === null}
        defaultImg="/images/default/default-hub.png"
        onChange={handleChangeHub}
      />

      <FormTextAreaComponent
        content={hub.short_description}
        id="short_description"
        maxChar={500}
        onChange={handleChange}
        rows={3}
        title={t('shortDescriptionShownOnCards')}
        placeholder={t('theHubBrieflyExplained')}
        mandatory={isMandatoryField('hub', 'short_description')}
        errorCodeMessage={valid_short_description ? valid_short_description.message : ''}
        isValid={valid_short_description ? !valid_short_description.isInvalid : undefined}
      />
      <FormDefaultComponent
        id="external_website"
        content={hub.external_website}
        title={t('website')}
        onChange={handleChangeHub}
      />
    </form>
  );
};
export default HubForm;
