import useTranslation from 'next-translate/useTranslation';
import React, { FC } from 'react';
import { Link } from '~/__generated__/types';

interface Props {
  links: Link[];
  noText?: boolean;
}

const ShowLinks: FC<Props> = ({ links, noText = false }) => {
  const { t } = useTranslation('common');

  return (
    <div tw="inline-flex items-center md:px-4 my-2">
      {!noText && <span tw="font-bold mr-2">{t('findUsOn')}:</span>}
      <div tw="grid grid-cols-1 items-center text-center">
        <div tw="flex flex-wrap gap-2">
          {[...links].map((link, i) => (
            <a tw="items-center" key={i} href={link.url} target="_blank" rel="noreferrer">
              <img tw="w-7 hover:opacity-80" src={`/images/icons/links-${link.type.replace(/\s/g, '')}.png`} />
            </a>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ShowLinks;
