import useTranslation from 'next-translate/useTranslation';
import { useRef, useState } from 'react';
import Icon from '../primitives/Icon';
import CreatableSelect from 'react-select/creatable';
import { confAlert, isValidEmail } from '~/src/utils/utils';
import Button from '../primitives/Button';
import Alert from './Alert/Alert';

interface Props {
  handleSubmit: (emailList: string[]) => void;
}

//Allows user to invite a list of emails from a CSV file to a container.
export default function BtnUploadCSV({ handleSubmit }: Props) {
  const [filename, setFilename] = useState<string>();
  const [emailList, setEmailList] = useState<string[]>([]);
  const [warning, setWarning] = useState<string>();
  const inputRef = useRef(null);
  const { t } = useTranslation('common');

  const handleFileChange = (event) => {
    const file = event.target.files[0];
    setWarning(undefined);

    if (file) {
      setFilename(file?.name);
      const reader = new FileReader();
      reader.onload = (e) => {
        const result = e.target.result;
        const csvContent = typeof result === 'string' ? result : new TextDecoder().decode(result);
        const separators = [',', ';', '\t', ' ']; // Add more separators as needed

        // Split lines based on line breaks
        let lines = csvContent.split(/\r?\n/);

        // Process each line to extract email addresses
        let emailAddresses = lines.flatMap((line) => {
          // Trim, remove double quotes from each email line, and split each line based on any of the specified separators
          let emailsInLine = line
            .trim()
            .replace(/^"(.*)"$/, '$1')
            .split(new RegExp(separators.join('|')));

          return emailsInLine.map((email) => email);
        });

        // Remove empty strings from the array (resulting from consecutive separators)
        emailAddresses = emailAddresses.filter((email) => email !== '');

        let originalLength = emailAddresses?.length - 1;
        emailAddresses = emailAddresses.filter(
          (email, index, self) => email && isValidEmail(email) && self.indexOf(email) === index
        ); // Filter out empty, invalid, and duplicate emails

        setEmailList(emailAddresses);

        if (originalLength > emailAddresses.length) {
          if (emailAddresses?.length === 0) {
            setWarning(`No emails were imported. Please verify that the file conforms to the instructions above.`);
          } else {
            setWarning(
              `${emailAddresses.length} emails were imported from the original ${originalLength}. Please verify that the imported contents are correct and that the file is formatted correctly.`
            );
          }
        }

        // Reset the value of the file input so user can reupload the same file if needed
        if (inputRef.current) {
          inputRef.current.value = '';
        }
      };
      reader.readAsText(file);
    } else {
      confAlert.fire({ icon: 'error', title: 'Please select a valid file' });
    }
  };

  const handleChangeEmail = (content) => {
    const tempEmailsList = [];
    content?.map(function (invitee_mail) {
      if (invitee_mail) {
        let commaSeparatedValues = invitee_mail.value.split(',');
        invitee_mail && tempEmailsList.push(...commaSeparatedValues);
      }
    });
    setEmailList(tempEmailsList);
  };

  return (
    <div tw="flex flex-col gap-4">
      <span>
        {t('emailsCsvUploadInstructions')}
        <br />
        <a
          target="blank"
          href="https://jogldoc.notion.site/Inviting-and-managing-members-35960433e5bf4c478f217855ce8cc490"
          rel="noopener noreferrer"
        >
          {t('action.seeMore')}.
        </a>
      </span>

      <input
        ref={inputRef}
        style={{ display: 'none' }}
        type="file"
        accept=".csv"
        id="fileInput"
        onChange={(e) => handleFileChange(e)}
      />
      <div
        tw="bg-[#D1D1D180] flex flex-row gap-1 cursor-pointer items-center justify-center p-4 border rounded-md"
        onClick={(e) => {
          e.preventDefault();
          inputRef && inputRef.current && inputRef.current.click();
        }}
      >
        <Icon icon="material-symbols:upload" tw="w-5 h-5" />
        <span tw="underline font-bold text-[17px] hover:opacity-80">
          {filename ? filename : t('action.uploadItem', { item: 'CSV' })}
        </span>
      </div>

      {warning && <Alert message={warning} type="warning" />}
      {emailList?.length > 0 && (
        <div tw="flex flex-col gap-4">
          <div>
            <span tw="font-bold">{t('nEmailsImported', { count: emailList.length })}</span>

            <CreatableSelect
              tw="max-h-40 overflow-auto"
              isClearable
              isMulti
              noOptionsMessage={() => null}
              menuShouldScrollIntoView={true} // force scroll into view
              components={{ DropdownIndicator: null, IndicatorSeparator: null }}
              onChange={handleChangeEmail}
              className="react-select-emails"
              value={emailList.map((email) => {
                return { label: email, value: email };
              })}
            />
          </div>

          <Button tw="self-center" onClick={() => handleSubmit(emailList)} disabled={emailList.length === 0}>
            {t('action.invite')}
          </Button>
        </div>
      )}
    </div>
  );
}
