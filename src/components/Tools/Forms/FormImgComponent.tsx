import { FC, useReducer, useRef } from 'react';
import Image from '~/src/components/primitives/Image';
import TitleInfo from '~/src/components/Tools/TitleInfo';
import { ItemType } from '~/src/types';
import SpinLoader from '../SpinLoader';
import tw from 'twin.macro';
import SingleImageUploadFile from '../SingleImageUploadFile';
import useTranslation from 'next-translate/useTranslation';
import { toInitials } from '~/src/utils/utils';
import Button from '~/src/components/primitives/Button';
import Icon from '~/src/components/primitives/Icon';
import { Typography } from 'antd';

interface Props {
  defaultImg?: string;
  id: string;
  imageUrl: string;
  itemId: string;
  itemType: ItemType;
  mandatory?: boolean;
  maxSizeFile?: number;
  onChange: (key: any, value: any) => void;
  title?: string;
  tooltipMessage?: string;
  type: 'avatar' | 'banner';
  isRounded?: boolean;
  aspectRatio?: number;
  hubTitle?: string;
  showAddButton?: boolean;
  errorCodeMessage?: string;
  isValid?: boolean;
  customTitleColor?: string;
  icon?: string;
}

const FormImgComponent: FC<Props> = ({
  isRounded = false,
  defaultImg = '',
  id = '',
  imageUrl = '',
  itemId = '',
  itemType,
  mandatory = false,
  tooltipMessage,
  onChange = (value) => console.warn(`onChange doesn't exist to update ${value}`),
  title = '',
  type,
  aspectRatio = null,
  hubTitle,
  showAddButton = false,
  errorCodeMessage = '',
  isValid = true,
  customTitleColor,
  icon,
}) => {
  const { t } = useTranslation('common');
  const [state, setState] = useReducer((state, action) => ({ ...state, ...action }), {
    error: '',
    uploading: false,
  });
  const inputRef = useRef<any>();
  const handleChange = (result: any) => {
    if (id !== '') {
      if (result.error !== '') {
        setState({ error: result.error });
      } else {
        setState({ error: '' });
        if (result.img !== '') {
          onChange(id, result.img);
        }
      }
    } else {
      console.warn('id is missing (key to set)');
    }
  };

  const removeImage = () => {
    if (type === 'avatar') {
      onChange('logo_id', null);
      onChange('logo_url', null);
      onChange('logo_url_sm', null);
    } else {
      onChange('banner_id', null);
      onChange('banner_url', null);
      onChange('banner_url_sm', null);
    }
  };

  const { error, uploading } = state;

  return (
    <div className="formImg" tw="pt-0">
      <div tw="flex gap-1 items-center">
        {icon && <Icon icon={icon} tw="w-5 h-5 m-0" />}
        {title && (
          <TitleInfo
            title={title}
            mandatory={mandatory}
            tooltipMessage={tooltipMessage}
            customTitleColor={customTitleColor}
          />
        )}
      </div>
      {type === 'banner' && itemType !== 'users' && <Typography.Text>{t('usedForTheCard')}</Typography.Text>}

      <div className="btnUploadZone">{uploading && <SpinLoader />}</div>
      {showAddButton && (
        <Button
          btnType="secondary"
          style={{ color: '#643CC6', fontWeight: '500', fontSize: '14px', marginTop: '0.5rem' }}
          onClick={(e) => {
            e.preventDefault();
            inputRef.current && inputRef.current?.click();
          }}
        >
          {t(`legacy.add_${type}`)}
        </Button>
      )}

      {(imageUrl || defaultImg || hubTitle) && (
        <div
          className={`preview ${type}`}
          css={[
            tw`relative`,
            inputRef?.current && tw`cursor-pointer`,
            type === 'avatar' && tw`flex items-center justify-center`,
            showAddButton && tw`hidden`,
          ]}
          onClick={(e) => {
            e.preventDefault();
            inputRef.current && inputRef.current?.click();
          }}
        >
          <SingleImageUploadFile
            aspectRatio={aspectRatio || 16 / 5}
            imageUrl={imageUrl}
            isRounded={isRounded}
            itemId={itemId}
            itemType={itemType}
            ref={inputRef}
            type={type}
            text={t('chooseAnImage')}
            fileTypes={['image/jpeg', 'image/png', 'image/jpg']}
            maxSizeFile={4194304}
            handleOnChange={handleChange}
            handleRemoveImage={removeImage}
          />
          {!hubTitle || imageUrl ? (
            <Image
              src={imageUrl || defaultImg}
              alt={title}
              priority
              className={isValid ? '' : 'is-invalid'}
              css={
                type === 'banner'
                  ? tw`(max-h-[310px] max-w-full object-cover)!`
                  : tw`(w-full max-h-full max-w-full min-h-[auto] min-w-[auto])! xs:(max-h-[250px])`
              }
            />
          ) : (
            <div tw="bg-center relative flex bg-cover uppercase text-[35px] items-center font-medium text-gray-500 justify-center bg-no-repeat rounded-full bg-white h-[100px] w-[100px] border border-solid border-[#ced4da]">
              {toInitials(hubTitle)}
            </div>
          )}
          {!title && mandatory && <div tw="text-danger absolute top-0 right-1">&nbsp;*</div>}
        </div>
      )}
      {!isValid && (
        <div tw="inline-flex justify-between w-full">
          {errorCodeMessage ? <div tw="text-danger">{t(errorCodeMessage)}</div> : ''}
        </div>
      )}
    </div>
  );
};

export default FormImgComponent;
