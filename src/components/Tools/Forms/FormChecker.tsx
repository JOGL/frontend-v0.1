import validator from 'validator';

function applyPattern(value, previousValue, pattern) {
  if (pattern) {
    if (value.match(pattern) === null) {
      value = '';
    } else if (value.match(pattern).length !== value.length) {
      value = previousValue;
    }
  }
  return value;
}

// obsolete after 1 day :(
function validateInput(value, mandatory, maxChar) {
  if (mandatory && validator.isEmpty(value, { ignore_whitespace: true })) {
    return { isValid: false, errorCodeMessage: 'error.valueIsMissing' };
  }
  if (maxChar && !validator.isLength(value, { max: maxChar })) {
    return { isValid: false, errorCodeMessage: 'error.valueIsTooLong' };
  }
  return { isValid: true, errorCodeMessage: '' };
}

export { applyPattern, validateInput };
