import useTranslation from 'next-translate/useTranslation';
import AsyncCreatableSelect from 'react-select/async-creatable';
import TitleInfo from '~/src/components/Tools/TitleInfo';
import { useApi } from '~/src/contexts/apiContext';
import Icon from '~/src/components/primitives/Icon';
// import "./FormSkillsComponent.scss";

const FormSkillsComponent = ({
  content = [],
  errorCodeMessage = '',
  id = 'keywords',
  mandatory = false,
  showHelp = true,
  type = 'default',
  onChange = (value, skills) => console.warn(`onChange doesn't exist to update ${value} of ${skills}`),
  placeholder = '',
  title = 'Title',
  tooltipMessage = '',
  customTitleColor = undefined,
  icon = undefined,
}) => {
  const { t } = useTranslation('common');
  const api = useApi();

  const handleChange = (skillsList) => {
    const newElement = Array.isArray(skillsList) && skillsList?.filter((item) => item['__isNew__'] === true);
    if (newElement.length > 0) {
      addSkillOrInterests(newElement[0]['value']);
    }
    // convert skillsList array into an array with just the skills labels
    const skills = skillsList?.map(({ label }) => label);
    // send the whole initial array (value, label, skillId) only for the moderator dashboard feature
    type === 'moderator' ? onChange(id, skillsList) : onChange(id, skills);
  };

  let fetchedItems = [];

  const fetchDB = async (resolve: any, value: any) => {
    try {
      let searchContent: any = null;
      if (type === 'keywords' || id === 'interests') {
        searchContent = (
          await (type === 'keywords'
            ? api.get(`/users/skills?Search=${value}&Page=1&PageSize=5`)
            : api.get(`/users/interests?Search=${value}&Page=1&PageSize=5`))
        ).data;
      }

      if (searchContent) {
        if (type === 'keywords' || id === 'interests') {
          fetchedItems = searchContent.map(({ value }) => {
            return { value: value, label: value, skillId: value };
          });
        }
      } else {
        fetchedItems = [''];
      }

      resolve(fetchedItems);
    } catch (e) {
      resolve(['']);
    }
  };

  const addSkillOrInterests = async (value: string) => {
    try {
      await (type === 'keywords' ? api.post(`/users/skills`, { value }) : api.post(`/users/interests`, { value }));
    } catch (e) {}
  };

  const loadOptions = (inputValue) =>
    new Promise((resolve) => {
      fetchDB(resolve, inputValue);
    });

  const currentSkills = content.map((skill) => ({ label: skill, value: skill }));

  const customStyles = {
    loadingIndicator: (provided) => ({
      ...provided,
      display: 'none',
    }),
    multiValue: (provided) => ({
      ...provided,
      // borderRadius: '10px',
      background: 'rgb(241, 246, 253)',
      borderRadius: '13px',
      padding: '4px 0px',
      border: '1px solid rgb(212, 222, 231)',
      color: '#003054',
    }),
    multiValueRemove: (provided) => ({
      ...provided,
      background: 'rgb(241, 246, 253)',
      cursor: 'pointer',
      borderRadius: '13px',
    }),
    control: (provided) => ({
      ...provided,
      // different border style depending if value is empty or not (validation)
      border: errorCodeMessage ? '1px solid #dc3545' : '1px solid lightgrey',
    }),
    placeholder: (provided) => ({
      ...provided,
      // different border style depending if value is empty or not (validation)
      color: '#bbbbbb!important',
    }),
  };

  return (
    <div className="formSkills">
      <div tw="flex gap-1 items-center">
        {icon && <Icon icon={icon} tw="w-5 h-5 m-0" />}
        {title && (
          <TitleInfo
            title={title}
            mandatory={mandatory}
            tooltipMessage={tooltipMessage}
            customTitleColor={customTitleColor}
          />
        )}
      </div>

      <div className="content skill-container" id={id}>
        {showHelp && (
          <label tw="text-gray-400" className="form-check-label" htmlFor={`show${id}`}>
            {t(`legacy.enterFormValues.skills.${type}`)}
          </label>
        )}
        <AsyncCreatableSelect
          name={id}
          cacheOptions
          isMulti
          value={currentSkills && currentSkills}
          defaultOptions={false}
          components={{ DropdownIndicator: null, IndicatorSeparator: null }}
          formatCreateLabel={(inputValue) => `${t('action.add')} "${inputValue}"`}
          blurInputOnSelect={false} // force keeping focus when selecting option (for mobile)
          noOptionsMessage={() => null}
          loadOptions={loadOptions}
          onChange={handleChange}
          placeholder={placeholder}
          // allowCreateWhileLoading={true}
          styles={customStyles}
          // getOptionValue={option => option['id']}
        />
        {errorCodeMessage && <div className="invalid-feedback">{t(errorCodeMessage)}</div>}
      </div>
    </div>
  );
};

export default FormSkillsComponent;
