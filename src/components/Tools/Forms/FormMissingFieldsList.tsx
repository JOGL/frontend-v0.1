interface FormProps {
  stateValidation: {};
}

export default function FormMissingFieldsList({ stateValidation }: FormProps) {
  return (
    <div tw="rounded-md bg-[#ffcaca] text-[#DC3545] max-h-[150px] overflow-auto p-4 mb-[10px]">
      <span>
        <p tw="font-bold mb-0">Don't forget to fill the mandatory fields:</p>
        <ul tw="mb-0">
          {Object.values(stateValidation)
            ?.filter((validation: any) => validation.isInvalid)
            .map((error: any) => {
              return <li key={error.displayName}>{error.displayName}</li>;
            })}
        </ul>
      </span>
    </div>
  );
}
