import React, { useState } from 'react';
import TitleInfo from '../TitleInfo';
import TimezoneSelect from 'react-timezone-select';
import useTranslation from 'next-translate/useTranslation';
import tw from 'twin.macro';
import { EventTimezone } from '~/src/types/event';
import Icon from '~/src/components/primitives/Icon';

interface Props {
  defaultValue?: EventTimezone;
  mandatory?: boolean;
  errorCodeMessage?: string;
  customTitleColor?: string;
  icon?: string;
  handleChange: (timezone) => void;
}

export default function FormTimezonePicker({
  defaultValue,
  mandatory,
  errorCodeMessage,
  customTitleColor,
  icon,
  handleChange,
}: Props) {
  const [selectedOption, setSelectedOption] = useState(
    defaultValue?.value ?? Intl.DateTimeFormat().resolvedOptions().timeZone
  );
  const { t } = useTranslation('common');

  const handleTimezoneChange = (selected) => {
    setSelectedOption(selected);
    handleChange(selected);
  };

  const customStyles = {
    menu: (provided) => ({
      ...provided,
      zIndex: 10,
    }),
  };

  return (
    <>
      <div tw="flex gap-1 items-center">
        {icon && <Icon icon={icon} tw="w-5 h-5 m-0" />}
        <TitleInfo title={t('timezone')} mandatory={mandatory} customTitleColor={customTitleColor} />
      </div>
      <TimezoneSelect
        value={selectedOption}
        styles={customStyles}
        onChange={handleTimezoneChange}
        css={[
          errorCodeMessage
            ? tw`border-danger focus:(ring-danger border-danger)`
            : tw`border-gray-300 focus:(ring-primary border-primary)`,
        ]}
      />
      {errorCodeMessage && (
        <div tw="inline-flex justify-between w-full">
          <div tw="text-danger">{t(errorCodeMessage)}</div>
        </div>
      )}
    </>
  );
}
