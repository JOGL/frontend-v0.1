import useTranslation from 'next-translate/useTranslation';
import { useEffect, useRef, useState } from 'react';
import TitleInfo from '../TitleInfo';

interface FormLocationComponentProps {
  existingLocation?: string;
  title?: string;
  placeholder?: string;
  mandatory?: boolean;
  handleChange: (location: string | null) => void;
}

export const FormLocationComponent = ({
  existingLocation,
  title,
  placeholder,
  mandatory,
  handleChange,
}: FormLocationComponentProps) => {
  const { t } = useTranslation('common');

  return (
    <div tw="w-full">
      {title && <TitleInfo title={title} id="location" mandatory={mandatory} />}
      <LocationSearch
        existingLocation={existingLocation}
        placeholder={placeholder ?? t('searchAddress')}
        onLocationSelect={(place) => handleChange(place)}
      />
    </div>
  );
};

interface LocationSearchProps {
  placeholder?: string;
  onLocationSelect: (place: string | null) => void;
  existingLocation?: string;
}

export const LocationSearch: React.FC<LocationSearchProps> = ({ placeholder, onLocationSelect, existingLocation }) => {
  const inputRef = useRef<HTMLInputElement | null>(null);
  const [predictions, setPredictions] = useState<google.maps.places.AutocompletePrediction[]>([]);
  const autocompleteService = useRef<google.maps.places.AutocompleteService | null>(null);

  const getPlacePredictions = async (query: string) => {
    return new Promise<google.maps.places.AutocompletePrediction[]>((resolve, reject) => {
      if (autocompleteService.current) {
        autocompleteService.current.getPlacePredictions(
          { input: query, types: ['(cities)'] },
          (predictions, status) => {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
              resolve(predictions || []);
            } else {
              reject(status);
            }
          }
        );
      } else {
        reject('Autocomplete service not initialized');
      }
    });
  };

  useEffect(() => {
    if (inputRef.current) {
      autocompleteService.current = new google.maps.places.AutocompleteService();

      const handleInputChange = async () => {
        const query = inputRef.current?.value;

        if (query.trim() === '') {
          setPredictions([]);
          return;
        }

        try {
          const newPredictions = await getPlacePredictions(query);
          setPredictions(newPredictions);
        } catch (error) {
          console.error('Error fetching predictions:', error);
        }
      };

      const handleOutsideClick = (event: MouseEvent) => {
        if (inputRef.current && !inputRef.current.contains(event.target as Node)) {
          setPredictions([]);
        }
      };

      inputRef.current.addEventListener('input', handleInputChange);
      document.addEventListener('click', handleOutsideClick);

      return () => {
        if (inputRef.current) {
          inputRef.current.removeEventListener('input', handleInputChange);
        }
        document.removeEventListener('click', handleOutsideClick);
      };
    }
  }, []);

  useEffect(() => {
    // Set initial location when provided
    if (inputRef.current && existingLocation) {
      inputRef.current.value = existingLocation;
    }
  }, [existingLocation]);

  const handlePredictionSelect = async (prediction: google.maps.places.AutocompletePrediction) => {
    if (inputRef.current) {
      inputRef.current.value = prediction.description || '';
    }

    setPredictions([]);
    onLocationSelect(prediction.description || '');
  };

  return (
    <div tw="flex flex-auto relative text-black">
      <input
        type="text"
        id="location-input"
        placeholder={placeholder ?? 'Enter a location'}
        ref={inputRef}
        tw="border p-2 h-9 rounded-md border-gray-300 focus:(ring-primary border-primary)"
        autoComplete="off"
        onChange={(ev) => {
          if (!ev.currentTarget.value) onLocationSelect(null);
        }}
      />

      {/* Display predictions only if the user has started searching */}
      {predictions.length > 0 && inputRef.current?.value.trim() !== '' && (
        <ul tw="absolute top-full left-0 z-10 list-none p-1 m-0 bg-white shadow rounded-md w-full">
          {predictions.map((prediction) => (
            <li
              key={prediction.place_id}
              onClick={() => handlePredictionSelect(prediction)}
              tw="py-1 px-2 cursor-pointer hover:bg-blue-100"
            >
              {prediction.description}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};
