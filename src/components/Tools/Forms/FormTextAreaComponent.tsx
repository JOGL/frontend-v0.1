import { FC } from 'react';
import InfoMaxCharComponent from '~/src/components/Tools/Info/InfoMaxCharComponent';
import TitleInfo from '~/src/components/Tools/TitleInfo';
import tw from 'twin.macro';
import useTranslation from 'next-translate/useTranslation';
import Icon from '~/src/components/primitives/Icon';

interface FormTextAreaComponentProps {
  errorCodeMessage?: string;
  id: string;
  isValid?: boolean;
  onChange: (key: any, value: any) => void;
  placeholder: string;
  maxChar?: number;
  mandatory?: boolean;
  title?: string;
  tooltipMessage?: any;
  rows?: number;
  content?: string;
  supportLinks?: boolean;
  showTitle?: boolean;
  showMaxChar?: boolean;
  showWordCount?: boolean;
  showCharCountMax?: boolean;
  customTitleColor?: string;
  icon?: string;
}

const FormTextAreaComponent: FC<FormTextAreaComponentProps> = (props) => {
  const {
    content,
    errorCodeMessage,
    id,
    isValid,
    maxChar,
    mandatory,
    placeholder,
    title,
    tooltipMessage = undefined,
    onChange,
    showTitle,
    showMaxChar,
    showCharCountMax,
    showWordCount,
    rows,
    supportLinks,
    customTitleColor,
    icon,
  } = {
    ...{
      errorCodeMessage: '',
      id: 'default',
      isValid: undefined,
      onChange: (key, value) => console.warn(`onChange doesn't exist to update ${value} for the ${key}`),
      placeholder: '',
      maxChar: undefined,
      rows: 5,
      mandatory: false,
      title: 'Title',
      tooltipMessage: undefined,
      showTitle: true,
      showMaxChar: true,
      showCharCountMax: false,
      supportLinks: false,
      showWordCount: false,
    },
    ...props,
  };

  const { t } = useTranslation('common');

  const handleChange = (event: any) => {
    onChange(event.target.id, event.target.value);
  };

  return (
    <div tw="mb-4 w-full">
      {(showMaxChar || showTitle) && (
        <div tw="flex gap-1 items-center flex-wrap">
          {icon && <Icon icon={icon} tw="w-5 h-5 m-0" />}
          {showTitle && (
            <div tw="mr-2">
              <TitleInfo
                title={title}
                mandatory={mandatory}
                customTitleColor={customTitleColor}
                {...(tooltipMessage && { tooltipMessage: tooltipMessage })}
              />
            </div>
          )}
          {showMaxChar && (
            <div tw="italic">
              ({supportLinks && `${t('supportLinks')}, `}
              {maxChar || '340'} {t('charactersMax')})
            </div>
          )}
        </div>
      )}
      <textarea
        className={`form-control ${isValid !== undefined ? (isValid ? 'is-valid' : 'is-invalid') : ''}`}
        id={id}
        name={id}
        placeholder={placeholder}
        value={content === null ? '' : content}
        rows={rows}
        tw="shadow-sm block w-full rounded-md sm:border"
        css={[
          errorCodeMessage
            ? tw`border-danger focus:(ring-danger border-danger)`
            : tw`border-gray-300 focus:(ring-primary border-primary)`,
        ]}
        maxLength={maxChar || 500}
        onChange={handleChange}
      />
      <div css={[errorCodeMessage && tw`inline-flex justify-between w-full`]}>
        {errorCodeMessage && <div className="invalid-feedback">{t(errorCodeMessage)}</div>}
        <InfoMaxCharComponent
          showCharCountMax={showCharCountMax}
          showWordCount={showWordCount}
          showMaxChar={showMaxChar}
          content={content}
          maxChar={maxChar}
        />
      </div>
    </div>
  );
};

export default FormTextAreaComponent;
