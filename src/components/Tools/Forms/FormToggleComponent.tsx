import { Component } from 'react';
import withTranslation from 'next-translate/withTranslation';
import TitleInfo from '~/src/components/Tools/TitleInfo';
import { UserContext } from '~/src/contexts/UserProvider';
import tw from 'twin.macro';
// import "./FormToggleComponent.scss";

class FormToggleComponent extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  static get defaultProps() {
    return {
      id: 'default',
      onChange: (value) => console.warn(`onChange doesn't exist to update ${value}`),
      isChecked: false,
    };
  }

  handleChange(event) {
    this.props.onChange(event.target.id, event.target.checked);
  }

  render() {
    const { id, title, isChecked, color1, color2, choice1, choice2, warningMsg, isDisabled, toggleType } = this.props;
    const colorUnchecked = color2 || 'grey';
    const colorChecked = color1 || '#27B40E';

    return (
      <div
        tw="flex flex-col"
        css={isDisabled ? tw`opacity-20` : tw`opacity-100`}
        className={`formToggle ${toggleType === 'notif' && 'notif'}`}
      >
        {title && <TitleInfo title={title} />}
        <div className="content">
          {warningMsg && <p>{warningMsg}</p>}
          <div className="toggle">
            {choice1 && <span className={`choice left ${isChecked ? ' selected' : ''}`}>{choice1}</span>}
            <label className="switch">
              <input id={id} type="checkbox" checked={isChecked} onChange={this.handleChange} disabled={isDisabled} />
              <div
                tw="flex flex-col"
                css={[
                  isDisabled ? tw`cursor-not-allowed` : tw`cursor-pointer`,
                  { backgroundColor: isChecked ? colorChecked : colorUnchecked },
                ]}
                className="slider round"
              />
            </label>
            {choice2 && <span className={`choice${!isChecked ? ' selected' : ''}`}>{choice2}</span>}
          </div>
        </div>
      </div>
    );
  }
}
export default withTranslation(FormToggleComponent, 'common');
FormToggleComponent.contextType = UserContext;
