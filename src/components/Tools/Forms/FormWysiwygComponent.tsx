import dynamic from 'next/dynamic';
import { FC, useMemo, useRef, useState } from 'react';
import TitleInfo from '~/src/components/Tools/TitleInfo';
import { useApi } from '~/src/contexts/apiContext';
import useTranslation from 'next-translate/useTranslation';
import { css } from 'twin.macro';
import InfoMaxCharComponent from '../Info/InfoMaxCharComponent';
import useGet from '~/src/hooks/useGet';

interface FormWysiwygComponentProps {
  id?: string;
  title?: string;
  content?: string;
  onChange?: (value: string, ...args: any[]) => void;
  placeholder?: string;
  maxChar?: number;
  mandatory?: boolean;
  errorCodeMessage?: string;
  specialFormat?: string;
  isExpanded?: boolean;
}

const FormWysiwygComponent: FC<FormWysiwygComponentProps> = ({
  id = 'default',
  title = 'Title',
  content = '',
  onChange = (value) => console.warn(`onChange doesn't exist to update ${value}`),
  placeholder = '',
  errorCodeMessage,
  maxChar = undefined,
  mandatory = false,
  specialFormat,
  isExpanded = false,
}) => {
  const CustomReactQuill = useMemo(
    () => dynamic(import('~/src/components/Tools/CustomReactQuill'), { ssr: false }),
    []
  );
  const { t } = useTranslation('common');
  const [charLength, setCharLength] = useState(0);
  const api = useApi();
  const { data: canMentionEveryone } = useGet(id ? `/entities/permission/${id}/mentioneveryone` : '');
  const canMentionEveryoneRef = useRef(canMentionEveryone);
  canMentionEveryoneRef.current = canMentionEveryone;

  const formats =
    specialFormat === 'short'
      ? ['bold', 'italic', 'underline', 'link', 'list', 'bullet']
      : specialFormat === 'postComment'
      ? ['bold', 'italic', 'underline', 'link', 'list', 'bullet', 'mention', 'indent']
      : [
          'header',
          'align',
          'color',
          'background',
          'script',
          'bold',
          'italic',
          'underline',
          'strike',
          'blockquote',
          'list',
          'bullet',
          'indent',
          'link',
          'image',
          'code-block',
          'span',
          'video',
          'mention',
          'alt',
          'width',
          'height',
          'style',
        ];

  const modules = {
    keyboard: {
      bindings: {
        tab: {
          key: 9,
          handler: function (range, context) {
            return true;
          },
        },
      },
    },
    toolbar:
      specialFormat === 'short' || specialFormat === 'postComment'
        ? [['bold', 'italic', 'underline', 'link', { list: 'bullet' }, { list: 'ordered' }, 'mentionUser']]
        : // ? false
          [
            [{ header: 1 }, { header: 2 }],
            ['bold', 'italic', 'underline', 'strike'],
            [{ color: [] }, { background: [] }],
            [{ align: [] }],
            [{ script: 'sub' }, { script: 'super' }],
            ['code-block', 'blockquote'],
            [{ list: 'bullet' }, { list: 'ordered' }, { indent: '-1' }, { indent: '+1' }],
            ['link', 'image', 'video'],
            ['clean'],
          ],
    imageDrop: specialFormat !== 'short' && specialFormat !== 'postComment', // ability to drop image directly in the editor
    imageResize: {
      modules: ['Resize', 'DisplaySize'],
    },
    magicUrl: {
      // Regex used to check URLs during typing
      urlRegularExpression: /(https?:\/\/[\S]+)|(www.[\S]+)|(tel:[\S]+)/g,
      // Regex used to check URLs on paste
      globalRegularExpression: /(https?:\/\/|www\.|tel:)[\S]+/g,
    },
    mention: {
      // quill-mention features
      allowedChars: /^.*$/, // allowed characters
      mentionDenotationChars: ['@', '#'], // characters that triggers source function
      linkTarget: '_blank',
      fixMentionsToQuill: true,
      defaultMenuOrientation: 'bottom',
      source: async (searchTerm, renderList, mentionChar) => {
        // function triggered when typing one of mentionDenotationChars
        const index =
          mentionChar === '@'
            ? api.get(`/feed/${id}/users?Search=${searchTerm}&Page=1&PageSize=10`)
            : api.get(`/workspaces?Search=${searchTerm}&Page=1&PageSize=10`);
        const content = (await index).data;

        const objectsList =
          mentionChar === '@'
            ? // if mentionChar is @ (user), render a user specific object
              content.map((obj) => ({
                value: `${obj.first_name} ${obj.last_name}`, // "value" is needed by quill-mention, it's the text that will be displayed after selecting the mention. Here it's user full name
                name: obj.username, // not required by quill-mention but we set "name" so it can be displayed in the mention list (renderItem function). Here it's user nickname
                link: `/user/${obj.id}`, // "link" is required by quill-mention to englobe "value" around a link automatically. Here it's relative link to the user
                imageUrl: obj.logo_url_sm || '/images/default/default-user.png', // not required by quill-mention but we set "imageUrl" so it can be displayed in the mention list (renderItem function). Here it's user profile image
              }))
            : // else render object specific object
              content.map((obj) => ({
                value: obj.title, // object title
                name: obj.title, // object title
                link: `/workspace/${obj.id}`, // relative link to the workspace
                imageUrl: obj.banner_url_sm ? obj.banner_url_sm : '/images/default/default-workspace.png', // workspace banner image
              }));
        // if user has permission to mention everyone, show new "Everyone" option on top of suggestions
        const newObjectsList =
          canMentionEveryoneRef.current && specialFormat === 'postComment'
            ? [
                ...objectsList,
                {
                  value: 'Everyone',
                  name: 'everyone',
                  link: '/user/everyone',
                  imageUrl: '/images/icons/at_symbol_icon.png',
                },
              ]
            : objectsList;
        renderList(newObjectsList, searchTerm); // quill-mention function that renders the list of matching mentions
      },
      renderItem(item) {
        // quill-mention function that gives control over how matching mentions from source are displayed within the list
        return `
          <div class="cql-list-item-inner">
            <img src="${item.imageUrl}"/>
            <div>
              <div>${item.value}</div>
              <span>@${item.name}</span>
            </div>
          </div>`;
      },
    },
  };

  const handleChange = (event) => onChange(id, event);

  return (
    <div className="formTextArea">
      <TitleInfo title={title} mandatory={mandatory} />
      {maxChar && maxChar > 0 ? (
        <InfoMaxCharComponent showCharCountMax showWordCount showMaxChar content={charLength} maxChar={maxChar} />
      ) : (
        ''
      )}
      <div
        id={`postBox${id}`}
        css={[
          specialFormat === 'short' &&
            css`
              .ql-container {
                height: 80px !important;
              }
              .ql-editor:not(.rendered-quill-content) {
                min-height: 80px !important;
              }
            `,
          specialFormat === 'postComment' &&
            css`
              .ql-editor:not(.rendered-quill-content) {
                min-height: 80px !important;
              }
              .ql-container.ql-snow,
              .ql-toolbar.ql-snow {
                border: none;
              }
            `,
          errorCodeMessage &&
            css`
              .ql-container {
                border-style: none solid solid solid;
                border-color: #dc3545;
                border-width: 1px;
              }

              .ql-toolbar {
                border: 1px solid #dc3545;
              }
            `,
          // https://github.com/quilljs/quill/issues/1874
          css`
            .ql-editor,
            .ql-align-justify {
              white-space: pre-wrap !important;
            }
          `,
        ]}
      >
        <CustomReactQuill
          id={id}
          formats={formats}
          modules={modules}
          onChange={handleChange}
          placeholder={placeholder}
          setCharLength={setCharLength}
          style={{ width: '100%', wordBreak: 'break-all' }}
          theme="snow"
          value={content !== null ? content : ''}
          isExpanded={isExpanded}
        />

        {errorCodeMessage && (
          <div className="invalid-feedback" style={{ display: 'inline' }}>
            {t(errorCodeMessage)}
          </div>
        )}
      </div>
    </div>
  );
};

export default FormWysiwygComponent;
