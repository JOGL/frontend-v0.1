import { CSSProperties, FC } from 'react';
import { applyPattern } from '~/src/components/Tools/Forms/FormChecker';
import TitleInfo from '~/src/components/Tools/TitleInfo';
import InfoMaxCharComponent from '../Info/InfoMaxCharComponent';
import tw from 'twin.macro';
import useTranslation from 'next-translate/useTranslation';
import Icon from '~/src/components/primitives/Icon';

interface FormDefaultComponentProps {
  content?: string;
  errorCodeMessage?: string;
  id?: string;
  maxChar?: any;
  isValid?: any;
  isPrivate?: boolean;
  mandatory?: boolean;
  onChange?: (id: any, value: any) => void;
  pattern?: any;
  placeholder?: string;
  title?: any;
  icon?: string;
  tooltipMessage?: any;
  type?: string;
  isDisabled?: boolean;
  description?: string;
  baseUrl?: string;
  prepend?: any;
  minDate?: any;
  maxDate?: any;
  customCss?: CSSProperties;
  customTitleColor?: string;
  preAddons?: () => JSX.Element;
}

const FormDefaultComponent: FC<FormDefaultComponentProps> = ({
  content = '',
  errorCodeMessage = '',
  id = 'default',
  maxChar = undefined,
  isValid = undefined,
  mandatory = false,
  onChange = (id, value) => console.warn(`onChange doesn't exist to update ${value}`),
  pattern = undefined,
  placeholder = '',
  title = undefined,
  tooltipMessage = undefined,
  type = 'text',
  isDisabled = false,
  isPrivate = false,
  description = '',
  baseUrl = '',
  preAddons,
  prepend,
  minDate,
  maxDate,
  customCss,
  customTitleColor,
  icon = undefined,
}) => {
  const { t } = useTranslation('common');

  const handleChange = (event) => {
    let { id, value } = event.target;
    value = applyPattern(value, content, pattern);
    onChange(id, value);
  };

  return (
    <div tw="w-full">
      <div tw="inline-flex justify-between w-full">
        <div tw="flex gap-1 items-center">
          {icon && <Icon icon={icon} tw="w-5 h-5 m-0" />}
          {title && (
            <TitleInfo
              title={title}
              id={id}
              mandatory={mandatory}
              isPrivate={isPrivate}
              customTitleColor={customTitleColor}
              {...(tooltipMessage && { tooltipMessage: tooltipMessage })}
            />
          )}
        </div>
        <InfoMaxCharComponent content={content} maxChar={maxChar} />
      </div>
      {description && <p tw="italic mb-0">{description}</p>}
      {prepend && baseUrl && <span tw="text-[.95rem]">{baseUrl.substr(7) + content}</span>}
      <div tw="mb-4 flex flex-col rounded-md" style={customCss}>
        <div tw="flex">
          {prepend && (
            <span tw="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-100 text-gray-500">
              {prepend}
            </span>
          )}
          {preAddons && (
            <span tw="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-100 text-gray-500">
              {preAddons()}
            </span>
          )}
          <input
            type={type}
            name={id}
            id={id}
            tw="flex-1 min-w-0 block w-full px-3 py-[0.4rem] invalid:text-[#bbbbbb] border"
            css={[
              prepend || preAddons ? tw`rounded-r-md` : tw`rounded-md`,
              isDisabled && tw`opacity-50 cursor-not-allowed`,
              errorCodeMessage
                ? tw`border-danger focus:(ring-danger border-danger)`
                : tw`border-gray-300 focus:(ring-primary border-primary)`,
            ]}
            className={`form-control ${isValid !== undefined ? (isValid ? 'is-valid' : 'is-invalid') : ''} ${
              content === '' ? 'isEmpty' : ''
            }`}
            placeholder={placeholder}
            value={content === null ? '' : content}
            onChange={handleChange.bind(this)}
            // if input is date and has a minDate, disable selecting all days before minDate
            min={minDate && minDate}
            max={maxDate && maxDate}
            disabled={isDisabled}
            maxLength={maxChar || 500}
          />
        </div>
        <div tw="inline-flex justify-between w-full">
          {errorCodeMessage ? <div tw="text-danger">{t(errorCodeMessage)}</div> : ''}
        </div>
      </div>
    </div>
  );
};

export default FormDefaultComponent;
