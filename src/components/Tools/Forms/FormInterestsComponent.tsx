import { Component } from 'react';
import withTranslation from 'next-translate/withTranslation';
import useTranslation from 'next-translate/useTranslation';
import TitleInfo from '~/src/components/Tools/TitleInfo';
import Select, { components } from 'react-select';
import ReactTooltip from 'react-tooltip';
import Icon from '~/src/components/primitives/Icon';

import styled from '~/src/utils/styled';
import { defaultSdgsInterests } from '~/src/utils/utils';

const { Option, MultiValueLabel, MultiValueRemove } = components;

// Since both labels and options have the same styling, create a common component
const StyledInterest = ({ Component, ...props }) => {
  const { t } = useTranslation('common');
  return (
    <Component {...props}>
      <span style={{ fontSize: '1.2rem' }}>{props.data.value}</span>
      <img
        style={{ width: '60px', height: '28px', objectFit: 'contain', paddingRight: '6px' }}
        src={`/images/interests/Interest-${props.data.value}-icon.png`}
      />
      <span style={{ fontSize: '.9rem' }}>{props.data.label}</span>
      {/* (?) tooltip to show user the sdg's full description */}
      <span style={{ marginLeft: '8px' }}>
        <Icon
          icon="solar:question-circle-bold"
          tw="w-4 h-4"
          data-for={`sdg-${props.data.value}`}
          data-tip={t(`legacy.sdg.description.${props.data.value}`)}
          tabIndex={0}
          onFocus={(e) => ReactTooltip.show(e.target)}
          onBlur={(e) => ReactTooltip.hide(e.target)}
        />
      </span>
      <ReactTooltip
        id={`sdg-${props.data.value}`}
        effect="solid"
        type="info"
        className="solid-tooltip"
        arrowColor="transparent"
      />
    </Component>
  );
};

const RemoveCross = styled.div`
  display: flex;
  flex-direction: column;
  color: white;
  :hover {
    color: black !important;
  }
`;

// Used for custom multi-select styling
const IconOption = (props) => <StyledInterest Component={Option} {...props} />;
const CustomMultiValueLabel = (props) => <StyledInterest Component={MultiValueLabel} {...props} />;
const AccessibleMultiValueRemove = (props) => (
  <MultiValueRemove {...props}>
    <RemoveCross as="button" tabIndex={0}>
      x
    </RemoveCross>
  </MultiValueRemove>
);

// Styles for react-select
const interestStyles = {
  option: (styles, { data, isFocused }) => {
    return {
      ...styles,
      fontWeight: 'bold',
      backgroundColor: data.color,
      display: 'inline-flex',
      alignItems: 'center',
      opacity: isFocused ? '.75' : '1',
      color: '#FFF',
      ':hover': {
        cursor: 'pointer',
        opacity: '.9',
      },
      ':active': {
        ...styles[':active'],
        backgroundColor: '#333333',
      },
    };
  },
  multiValue: (styles, { data }) => {
    return {
      ...styles,
      fontWeight: 'bold',
      backgroundColor: data.color,
    };
  },
  multiValueLabel: (styles) => ({
    ...styles,
    color: '#FFF',
    padding: '8px',
    display: 'flex',
    alignItems: 'center',
  }),
  placeholder: (styles) => ({
    ...styles,
    fontSize: '16px',
    fontWeight: '400',
  }),
  multiValueRemove: (styles) => ({
    ...styles,
    borderLeft: '1px solid white',
    ':hover': {
      cursor: 'pointer',
    },
  }),
};

class FormInterestsComponent extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  static get defaultProps() {
    return {
      content: [],
      errorCodeMessage: '',
      mandatory: false,
      onChange: (value) => console.warn(`onChange doesn't exist to update ${value}`),
    };
  }

  handleChange(interests) {
    // For multi-select interests component, interests is a list of interest objects for the multiselect
    let actualContent = this.props.content;
    if (!interests) {
      interests = [];
    }
    if (interests) {
      actualContent = interests.map((el) => el.value);
    }
    actualContent.sort((a, b) => a - b);
    this.props.onChange('interests', actualContent);
  }

  render() {
    const { content, errorCodeMessage, mandatory, title, showHelp } = this.props;
    const { t } = this.props.i18n;

    //Add control here so we can use the "errorCodeMessage ? " condition
    const fullInterestStyles = {
      ...interestStyles,
      control: (provided) => ({
        ...provided,
        // different border style depending if value is empty or not (validation)
        border: errorCodeMessage ? '1px solid #dc3545' : '1px solid lightgrey',
      }),
    };
    const inputTitle = title || t('interestedIn');

    // get sdgs infos array from external function
    const defaultInterests = defaultSdgsInterests(t);

    const tooltipMessage = t('sdgsTooltip');

    return (
      <div className="formInterests">
        <TitleInfo title={inputTitle} mandatory={mandatory} tooltipMessage={tooltipMessage} />
        <div className="content" id="interests">
          {showHelp && (
            <label tw="text-gray-400" className="form-check-label" htmlFor={`showkeywords`}>
              {t(`legacy.enterFormValues.skills.keywords`)}
            </label>
          )}
          <Select
            closeMenuOnSelect={false}
            value={defaultInterests.filter((el) => content.indexOf(el.value) !== -1)}
            components={{
              Option: IconOption,
              MultiValueLabel: CustomMultiValueLabel,
              MultiValueRemove: AccessibleMultiValueRemove,
            }}
            placeholder={t('sdgsPlaceholder')}
            isMulti
            options={defaultInterests}
            styles={fullInterestStyles}
            onChange={this.handleChange}
            isSearchable={false}
            tabSelectsValue={false}
            menuShouldScrollIntoView={true} // force scroll into view
            noOptionsMessage={() => null}
            selectProps={t} // so we can pass translate object to the custom component
          />
          {errorCodeMessage && (
            <div className="invalid-feedback" style={{ display: 'inline' }}>
              {t(errorCodeMessage)}
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default withTranslation(FormInterestsComponent, 'common');
