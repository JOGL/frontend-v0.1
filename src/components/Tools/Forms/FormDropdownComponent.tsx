import TitleInfo from '~/src/components/Tools/TitleInfo';
import useTranslation from 'next-translate/useTranslation';
import Select from 'react-select';
import { FC } from 'react';

interface Props {
  id: string;
  title: any;
  content: string;
  options: any[];
  onChange?: (id: number, value: string | number) => void;
  mandatory?: boolean;
  type?: string;
  warningMsg?: string;
  errorCodeMessage?: string;
  placeholder?: string;
  isSearchable?: boolean;
  tooltipMessage?: string;
  isDisabled?: boolean;
  isMulti?: boolean;
  isValid?: any;
  isPrivate?: boolean;
  containerType?: string;
}

const FormDropdownComponent: FC<Props> = ({
  id,
  title,
  content = '',
  options = [],
  onChange = (id, value) => console.warn(`onChange doesn't exist to update ${value} of ${id}`),
  mandatory = false,
  type = '',
  warningMsg,
  errorCodeMessage,
  placeholder = '',
  isSearchable = false,
  tooltipMessage,
  isDisabled = false,
  isMulti = false,
  isValid = undefined,
  isPrivate = false,
  containerType = undefined,
}) => {
  const { t } = useTranslation('common');
  const handleChange = (key, content) => {
    onChange(content.name, key.value);
  };

  const optionTranslation = (option) =>
    id === 'name' || type === 'noTranslation' || id === 'birth_date' || id.includes('privacy') // if id of form is name or birth_date, just display option
      ? option
      : type === 'need'
      ? t(`legacy.needType.${option}`)
      : type === 'status'
      ? t(`legacy.entityInfoStatus.${option}`)
      : id === 'gender' // if dropdown is a user category, show them
      ? t(`legacy.gender.${option}`)
      : id === 'jogldocVisibility' // if dropdown comes from jogldoc visibility
      ? option === 'entity' && containerType
        ? `${containerType} ${t(`legacy.visibleType.${option}`)}`
        : t(`legacy.visibleType.${option}`)
      : id === 'resourceType'
      ? t(`legacy.typeOptions.${option}`)
      : id === 'resourceCondition'
      ? t(`legacy.conditionOptions.${option}`)
      : type === 'cfpTemplateType'
      ? t(`legacy.cfpTemplateType.${option}`)
      : type === 'proposalStatus'
      ? t(`legacy.proposalStatus.${option}`)
      : // else display default status translation
        t(`legacy.entityInfoStatus.${option}`);

  const optionsList = options?.map((option) => {
    return {
      value: option,
      label: optionTranslation(option),
    };
  });

  const customStyles = {
    option: (provided) => ({
      ...provided,
      padding: '6px 10px',
    }),
    control: (provided) => ({
      ...provided,
      // different border style depending if value is empty or not (validation)
      border: isValid !== undefined ? (!isValid ? '1px solid #dc3545' : '1px solid #28a745') : '1px solid lightgrey',
    }),
    placeholder: (provided) => ({
      ...provided,
      // different border style depending if value is empty or not (validation)
      color: '#bbbbbb!important',
    }),
  };

  return (
    <div className="formDropdown">
      <TitleInfo title={title} mandatory={mandatory} isPrivate={isPrivate} tooltipMessage={tooltipMessage} />
      <div className="content">
        {warningMsg && content === 'draft' && (
          /* display warning message if applicable */
          <span tw="text-red-500 italic">{warningMsg}</span>
        )}
        <Select
          name={id}
          id={id}
          options={optionsList}
          styles={customStyles}
          isSearchable={isSearchable}
          menuShouldScrollIntoView={true} // force scroll into view
          placeholder={placeholder || t('selectAnOption')}
          value={content && { value: content, label: optionTranslation(content) }}
          defaultValue={content && { value: content, label: optionTranslation(content) }}
          noOptionsMessage={() => null}
          onChange={handleChange}
          isMulti={isMulti}
          isDisabled={isDisabled}
          className={`form-control ${isValid !== undefined ? (isValid ? 'is-valid' : 'is-invalid') : ''}`}
        />
      </div>
      {errorCodeMessage && (
        <div className="invalid-feedback" style={{ display: 'block' }}>
          {t(errorCodeMessage)}
        </div>
      )}
    </div>
  );
};
export default FormDropdownComponent;
