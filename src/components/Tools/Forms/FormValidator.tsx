import validator from 'validator';

export default class FormValidator {
  constructor(validations, t, fromParent? = '') {
    // validations is an array of rules specific to a form
    this.validations = validations;
    this.t = t;
    this.fromParent = fromParent;
  }

  validate(state) {
    // start out assuming valid
    const validation = this.valid();
    // for each validation rule
    this.validations.forEach((rule) => {
      // if the field isn't already marked invalid by an earlier rule
      if (!validation[rule.field].isInvalid) {
        // determine the field value, the method to invoke and
        // optional args from the rule definition
        const field_value = state[rule.field] ? state[rule.field].toString() : '';
        const args = rule.args || [];
        let validation_method = typeof rule.method === 'string' ? validator[rule.method] : rule.method;
        if (rule.method === 'isObjectEmpty') {
          validation_method = function (value, args, state) {
            return !value ? false : Object.keys(value).length !== 0;
          };
        }
        if (rule.method === 'minLength') {
          validation_method = function (value, args, state) {
            return !value ? false : value.length > args.min;
          };
        }
        if (rule.method === 'notEqual') {
          validation_method = function (value, args, state) {
            return !value ? false : value !== args.value;
          };
        }
        // call the validation_method with the current field value
        // as the first argument, any additional arguments, and the
        // whole state as a final argument.  If the result doesn't
        // match the rule.validWhen property, then modify the
        // validation object for the field and set the isValid
        // field to false
        if (validation_method(field_value, ...args, state) !== rule.validWhen) {
          validation[rule.field] = {
            isInvalid: true,
            message: rule.message,
            displayName:
              (this.fromParent !== 'user'
                ? this.t(`legacy.formValidator.${rule?.field}`)
                : // workaround to display City/Country instead of just "city" that is the default field title in the user form validaiton file
                rule?.field === 'city'
                ? this.t('location')
                : this.t(`legacy.form.${rule?.field}`)) ?? '',
          };
          validation.isValid = false;
        }
      }
    });
    return validation;
  }

  // create a validation object for a valid form
  valid() {
    const validation = {};

    this.validations.map((rule) => (validation[rule.field] = { isInvalid: false, message: '' }));
    return { isValid: true, ...validation };
  }
}
