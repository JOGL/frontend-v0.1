import TitleInfo from '~/src/components/Tools/TitleInfo';
import tw from 'twin.macro';

export default function InfoDefaultComponent({
  title = '',
  content,
  prepend = '',
  containsHtml = false,
  noPadding = false,
}) {
  if (content) {
    return (
      <div className="infoDefault" css={noPadding && tw`p-0`}>
        {title && <TitleInfo title={title} />}
        <div className="content">
          {!containsHtml ? prepend + content : <div dangerouslySetInnerHTML={{ __html: content }} />}
        </div>
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
}
