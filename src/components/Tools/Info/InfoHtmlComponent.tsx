import 'quill/dist/quill.snow.css'; // Add css for snow theme

export default function InfoHtmlComponent({ content, ...props }) {
  if (content) {
    return (
      <div {...props} className={`infoHtml ${props.className || ''}`}>
        <div className="content">
          <div dangerouslySetInnerHTML={{ __html: content }} className="rendered-quill-content ql-editor" />
        </div>
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
}
