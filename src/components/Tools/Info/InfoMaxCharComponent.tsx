import tw, { styled } from 'twin.macro';

export default function InfoMaxCharComponent({
  content = '',
  showMaxChar = true,
  showCharCountMax = false,
  showWordCount = false,
  maxChar = 340,
}) {
  const actualSize = typeof content === 'number' ? content : content?.length;
  if ((content?.length > maxChar * 0.8 && showMaxChar) || showCharCountMax)
    return (
      <StyledInput type={actualSize > maxChar ? 'danger' : actualSize > maxChar * 0.8 ? 'warning' : 'default'}>
        {actualSize}/{maxChar} max.
      </StyledInput>
    );
  if (showWordCount) return <StyledInput type="default">{actualSize} characters.</StyledInput>;

  return null;
}
const StyledInput = styled.div`
  text-align: right;
  ${({ type }) => (type === 'warning' ? tw`text-yellow-500` : type === 'danger' ? tw`text-danger` : tw`text-gray-500`)}
`;
