import React, { Fragment, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Button from '~/src/components/primitives/Button';
import H2 from '~/src/components/primitives/H2';
import Card from '~/src/components/Cards/Card';
import FormDefaultComponent from './Forms/FormDefaultComponent';
import Icon from '../primitives/Icon';
import { Menu, MenuItem, MenuButton, MenuList } from '@reach/menu-button';
import { confAlert } from '~/src/utils/utils';
import FormWysiwygComponent from './Forms/FormWysiwygComponent';
import InfoHtmlComponent from './Info/InfoHtmlComponent';

const ManageFaq = ({ faqs: faqsProp, handleChange }) => {
  const { t } = useTranslation('common');
  const [faqs, setFaqs] = useState(faqsProp);

  const addFaq = () => {
    const newFaq = { question: '', answer: '' };
    setFaqs([...faqs, newFaq]);
  };

  const Item = ({ faqItem, setFaqs, faqs }) => {
    const [edit, setEdit] = useState(faqItem.question === '' ? true : false);
    const [faq, setFaq] = useState(faqItem);

    const handleEditChange: (key: string, content: string) => void = (key, content) => {
      setFaq((faq) => ({ ...faq, [key]: content })); // set an object containing only the fields/inputs that are updated by user
    };

    const handleEdit = () => {
      setEdit(!edit);
    };

    const handleDelete = (faqQuestion) => {
      const faqId = faqs.map((e) => e.question).indexOf(faqQuestion);
      const newFaqs = [...faqs].filter((faq) => faqs[faqId] !== faq);
      handleChange('faq', newFaqs);
      setFaqs(newFaqs);
    };

    const handleEditSubmit = (faqQuestionOld) => {
      const faqId = faqs.map((e) => e.question).indexOf(faqQuestionOld);
      const newFaqs = faqs.map((oldFaq) => (faqs[faqId] === oldFaq ? faq : oldFaq));
      if (faq.question !== '' && faq.answer !== '') {
        setFaqs(newFaqs);
        handleChange('faq', newFaqs);
        handleEdit();
      } else confAlert.fire({ icon: 'error', title: 'Please fill all fields' });
    };
    return (
      <Fragment key={faqItem.question}>
        {!edit ? (
          <Card tw="flex-row flex-wrap relative">
            <div tw="flex flex-col">
              <div tw="break-all font-bold">{faqItem.question}</div>{' '}
              <InfoHtmlComponent tw="break-all" content={faqItem.answer} />
            </div>
            <div tw="absolute right-2">
              <Menu>
                <MenuButton tw="font-size[.8rem] height[fit-content] text-gray-700 px-1 hover:bg-gray-300 rounded-sm bg-white pl-2">
                  •••
                </MenuButton>
                <MenuList className="post-manage-dropdown">
                  <MenuItem onSelect={handleEdit}>
                    <Icon icon="bxs:edit" />
                    {t('action.edit')}
                  </MenuItem>
                  <MenuItem onSelect={() => handleDelete(faqItem.question)}>
                    <Icon icon="ic:round-delete" tw="w-5 h-5" />
                    {t('action.delete')}
                  </MenuItem>
                </MenuList>
              </Menu>
            </div>
          </Card>
        ) : (
          <Card>
            <div tw="flex flex-col justify-between md:flex-row">
              <div tw="flex flex-col w-full pr-0 md:pr-5">
                <FormDefaultComponent
                  id="question"
                  content={faq.question}
                  title={t('question.one')}
                  onChange={handleEditChange}
                />
                <FormWysiwygComponent
                  id="answer"
                  content={faq.answer}
                  title={t('answer')}
                  onChange={handleEditChange}
                />
              </div>
              <div tw="flex gap-2 md:(flex-col justify-center)">
                <Button tw="md:w-full" btnType="secondary" onClick={handleEdit}>
                  {t('action.cancel')}
                </Button>
                <Button tw="md:w-full" onClick={() => handleEditSubmit(faqItem.question)}>
                  {t('action.save')}
                </Button>
              </div>
            </div>
          </Card>
        )}
      </Fragment>
    );
  };

  return (
    <div tw="space-y-3">
      <H2>{t('faqAcronym')}</H2>
      {faqs?.length > 0 ? (
        <div tw="flex flex-col pt-2 gap-5">
          {faqs.map((faqItem, i) => {
            return <Item faqItem={faqItem} setFaqs={setFaqs} faqs={faqs} key={i} />;
          })}
        </div>
      ) : (
        <div>{t('noItems', { item: t('faqAcronym') })}</div>
      )}
      <Button onClick={addFaq}>{t('action.add')}</Button>
    </div>
  );
};

export default ManageFaq;
