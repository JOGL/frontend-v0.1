import React, { useState, createRef, forwardRef, FC, useEffect, useRef, useImperativeHandle } from 'react';
import Cropper, { ReactCropperElement } from 'react-cropper';
import 'cropperjs/dist/cropper.css';
import Icon from '../primitives/Icon';
import Button from '../primitives/Button';
import useTranslation from 'next-translate/useTranslation';
import Loading from './Loading';
import { confAlert } from '~/src/utils/utils';
import { useApi } from '~/src/contexts/apiContext';
import Alert from './Alert/Alert';
import { MAX_PAYLOAD_SIZE } from '~/src/utils/constants';

const ImageCropper: FC<any> = forwardRef(
  (
    {
      currentImage = '',
      aspectRatio,
      customClassName,
      onSave,
      onRemove,
      onCancel,
      maxSizeFile = MAX_PAYLOAD_SIZE,
      fileTypes,
      handleOnChange,
      openFileDialog = false,
      noteFiles = [],
      type = '',
    },
    ref: any
  ) => {
    const [image, setImage] = useState(currentImage);
    const [fileInfo, setFileInfo] = useState({});
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState<JSX.Element>();
    const cropperRef = createRef<ReactCropperElement>();
    const hasClicked = useRef<boolean>(false);
    const inputRef = useRef<any>();
    const { t } = useTranslation('common');
    const api = useApi();

    useImperativeHandle(ref, () => ({
      click: () => {
        inputRef.current && inputRef.current?.click();
      },
    }));

    function getRoundedCanvas(sourceCanvas) {
      const canvas = document.createElement('canvas');
      const context = canvas.getContext('2d');
      const width = sourceCanvas.width;
      const height = sourceCanvas.height;

      canvas.width = width;
      canvas.height = height;
      context.imageSmoothingEnabled = true;
      context.drawImage(sourceCanvas, 0, 0, width, height);
      context.globalCompositeOperation = 'destination-in';
      context.beginPath();
      context.arc(width / 2, height / 2, Math.min(width, height) / 2, 0, 2 * Math.PI, true);
      context.fill();
      return canvas;
    }

    const validFilesSize = (files) => {
      let nbFilesValid = 0;
      if (files) {
        for (let index = 0; index < files.length; index++) {
          if (files[index].size <= maxSizeFile) {
            nbFilesValid++;
          }
        }
      }
      return nbFilesValid === files.length; // return true or false depending on condition
    };

    const onChange = (e: any) => {
      e.preventDefault();
      let files;
      if (e.dataTransfer) {
        files = e.dataTransfer.files;
      } else if (e.target) {
        files = e.target.files;
      }

      if (files.length === 0) {
        onCancel();
        setError(undefined);
      } else {
        if (validFilesSize(files)) {
          const reader = new FileReader();
          reader.onload = () => {
            setImage(reader.result as any);
          };
          reader.readAsDataURL(files[0]);

          setError(undefined);
          setFileInfo(files[0]);
        } else {
          setError(
            <>
              {t('error.fileTooLarge')}
              &nbsp;(max : {maxSizeFile / 1024 / 1024}
              Mo)
            </>
          );
        }
      }
    };

    const handleRemoveImage = () => {
      setImage(undefined);
      setFileInfo({});
      setError(undefined);
      onRemove();
    };

    const getCropData = () => {
      if (typeof cropperRef.current?.cropper !== 'undefined') {
        let croppedCanvas = cropperRef.current?.cropper.getCroppedCanvas();

        if (customClassName) {
          croppedCanvas = getRoundedCanvas(croppedCanvas);
        }

        //This param will be needed if we want to actually keep the original image not only the cropped one.
        onSave(fileInfo, croppedCanvas.toDataURL() /* !currentImage ? image : currentImage.bigData*/);
        onCancel && onCancel();
        return;
      }
      onCancel && onCancel();
    };

    const loadExistingFileInfo = () => {
      api
        .get(`${currentImage}/model`)
        .then((res) => {
          setFileInfo(res.data);
        })
        .catch((err) => {
          console.error(err);
        });
    };

    useEffect(() => {
      if (openFileDialog && !hasClicked.current && noteFiles?.length === 0) {
        inputRef?.current && inputRef?.current?.click();
        hasClicked.current = true;
      }
      if (openFileDialog && noteFiles?.length > 0 && !hasClicked.current) {
        setIsLoading(true);
        hasClicked.current = true;
        const reader = new FileReader();
        reader.onload = () => {
          setImage(reader.result as any);
          setIsLoading(false);
        };
        reader.onerror = () => {
          setIsLoading(false);
          confAlert.fire({ icon: 'error', title: t('someUnknownErrorOccurred') });
        };
        reader.readAsDataURL(noteFiles[0]);

        setFileInfo(noteFiles[0]);
      }

      if (currentImage) {
        loadExistingFileInfo();
      }

      document.getElementById('file-input-dialog')?.addEventListener('cancel', (evt) => {
        //Closed the file picker dialog without selecting a file
        onCancel();
      });
    }, []);

    return (
      <div>
        <Loading active={isLoading}>
          {error && <Alert type="danger" message={error} />}

          <div style={{ width: '100%' }}>
            {image && (
              <Cropper
                ref={cropperRef}
                aspectRatio={aspectRatio || 1}
                zoomTo={0.1}
                tw="overflow-hidden h-96 w-full"
                className={customClassName}
                src={image}
                viewMode={2}
                responsive={true}
                autoCropArea={2}
                checkOrientation={false}
                guides={true}
              />
            )}
          </div>
          <div tw="mt-4">
            <div tw="flex flex-col justify-between flex-wrap gap-4">
              {(!currentImage || !image) && (
                <div>
                  <Button btnType="secondary" type="button" onClick={() => inputRef.current?.click()}>
                    {t('chooseFile')}
                  </Button>
                  {!inputRef.current?.value && <span tw="ml-2">{t('noFileChosen')}</span>}
                  <input
                    id="file-input-dialog"
                    accept={fileTypes.join()}
                    ref={inputRef}
                    type="file"
                    onChange={onChange}
                    tw="hidden"
                  />
                </div>
              )}

              <div tw="flex flex-col">
                <span tw="text-xs text-[#EDB95E] font-bold">
                  {type !== 'banner'
                    ? t('minimumRecommendedSize500x500Pixels')
                    : t('minimumRecommendedSize1280x400Pixels')}
                </span>

                {image && (
                  <div tw="self-center">
                    <button
                      title="Zoom-in"
                      onClick={() => {
                        cropperRef.current?.cropper.zoom(0.1);
                      }}
                      tw="rounded-l-lg bg-cyan-950 hover:bg-cyan-900 p-2 text-white"
                    >
                      <Icon icon="mdi:magnify-plus-outline" tw="ml-1" />
                    </button>
                    <button
                      title="Zoom-out"
                      onClick={() => {
                        cropperRef.current?.cropper.zoom(-0.1);
                      }}
                      tw="bg-cyan-950 hover:bg-cyan-900 p-2 text-white"
                    >
                      <Icon icon="mdi:magnify-minus-outline" tw="mr-0" />
                    </button>
                    <button
                      title="Remove"
                      onClick={handleRemoveImage}
                      tw="rounded-r-lg bg-cyan-950 hover:bg-cyan-900 p-2 text-white"
                    >
                      <Icon icon="bi:trash-fill" tw="mr-1" />
                    </button>
                  </div>
                )}
              </div>

              <div tw="flex gap-2 self-center">
                <Button tw="px-4 text-white" btnType="primary" onClick={getCropData}>
                  {t('action.save')}
                </Button>
                <Button tw="px-4" btnType="secondary" onClick={onCancel}>
                  {t('action.cancel')}
                </Button>
              </div>
            </div>
          </div>
        </Loading>
      </div>
    );
  }
);

export default ImageCropper;
