import React, { Fragment, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Button from '~/src/components/primitives/Button';
import H2 from '~/src/components/primitives/H2';
import Card from '~/src/components/Cards/Card';
import FormDefaultComponent from './Forms/FormDefaultComponent';
import FormDropdownComponent from './Forms/FormDropdownComponent';
import Icon from '../primitives/Icon';
import { Menu, MenuItem, MenuButton, MenuList } from '@reach/menu-button';
import { confAlert } from '~/src/utils/utils';

const linkStartWithProtocolPattern = /^((http|https|ftp):\/\/)/;

const ManageExternalLink = ({ links: linksProp, handleChange }) => {
  const { t } = useTranslation('common');
  const [links, setLinks] = useState(linksProp);

  const addLink = () => {
    const newLink = { url: '', type: 'website' };
    setLinks([...links, newLink]);
  };

  return (
    <div tw="space-y-3">
      <H2>{t('externalItem', { item: t('link', { count: 2 }) })}</H2>
      {links.length > 0 ? (
        <div tw="grid pt-2 gridTemplateColumns[1fr] gap-5 lg:gridTemplateColumns[1fr 1fr]">
          {links.map((linkItem, i) => {
            return <Item linkItem={linkItem} setLinks={setLinks} links={links} key={i} handleChange={handleChange} />;
          })}
        </div>
      ) : (
        <div>{t('noItems', { item: t('link.other') })}</div>
      )}

      <Button onClick={addLink}>{t('action.add')}</Button>
    </div>
  );
};

export default React.memo(ManageExternalLink);

const Item = ({ linkItem, setLinks, links, handleChange }) => {
  const [edit, setEdit] = useState(linkItem.url === '' ? true : false);
  const [link, setLink] = useState(linkItem);
  const { t } = useTranslation('common');

  const handleEditChange = (key, content) => {
    setLink((link) => ({ ...link, [key]: content })); // set an object containing only the fields/inputs that are updated by user
  };

  const handleEdit = () => {
    setEdit(!edit);
  };

  const handleDelete = (linkUrl) => {
    const linkId = links.map((e) => e.url).indexOf(linkUrl);
    const newLinks = [...links].filter((link) => links[linkId] !== link);
    handleChange('links', newLinks);
    setLinks(newLinks);
  };

  const handleEditSubmit = (linkUrlOld) => {
    const linkId = links.map((e) => e.url).indexOf(linkUrlOld);
    const newLinks = links.map((oldLink) => (links[linkId] === oldLink ? link : oldLink));
    if (linkStartWithProtocolPattern.test(link.url)) {
      setLinks(newLinks);
      handleChange('links', newLinks);
      handleEdit();
    } else confAlert.fire({ icon: 'error', title: t('wrongProtocolWarning') });
  };

  return (
    <Fragment key={linkItem.url}>
      {!edit ? (
        <Card tw="flex-row flex-wrap relative">
          <img src={`/images/icons/links-${link.type.replace(/\s/g, '')}.png`} tw="w-[25px] h-[25px] mr-2" />
          <div tw="break-all">{linkItem.url.replace(/(.{40})..+/, '$1…')}</div>{' '}
          <div tw="absolute right-2">
            <Menu>
              <MenuButton tw="font-size[.8rem] height[fit-content] text-gray-700 px-1 hover:bg-gray-300 rounded-sm bg-white pl-2">
                •••
              </MenuButton>
              <MenuList className="post-manage-dropdown">
                <MenuItem onSelect={handleEdit}>
                  <Icon icon="bxs:edit" />
                  {t('action.edit')}
                </MenuItem>
                <MenuItem onSelect={() => handleDelete(linkItem.url)}>
                  <Icon icon="ic:round-delete" tw="w-5 h-5" />
                  {t('action.delete')}
                </MenuItem>
              </MenuList>
            </Menu>
          </div>
        </Card>
      ) : (
        <Card>
          <div tw="flex flex-col justify-between md:flex-row">
            <div tw="flex flex-col w-full pr-0 md:pr-5">
              <div tw="inline-flex">
                <FormDropdownComponent
                  id="type"
                  type="noTranslation"
                  title={t('externalItem', { item: t('website') })}
                  content={link.type}
                  // prettier-ignore
                  options={['linkedin', 'twitter','github', "website", 'dribbble', 'google drive', 'google docs', 'google sheets', 'google slides', 'notion', 'one drive', 'dropbox', 'gitlab','instagram', 'medium', 'skype', 'vimeo', 'youtube']}
                  onChange={handleEditChange}
                />
                <div tw="flex items-center pt-5 pl-4">
                  <div className="preview iconPreview">
                    <img src={`/images/icons/links-${link.type.replace(/\s/g, '')}.png`} width="40px" />
                  </div>
                </div>
              </div>
              <FormDefaultComponent
                id="url"
                content={link.url}
                title={t('url')}
                placeholder="https://www.facebook.com/justonegiantlab/"
                onChange={handleEditChange}
              />
            </div>
            <div tw="flex gap-2 md:(flex-col justify-center)">
              <Button tw="md:w-full" btnType="secondary" onClick={handleEdit}>
                {t('action.cancel')}
              </Button>
              <Button tw="md:w-full" onClick={() => handleEditSubmit(linkItem.url)}>
                {t('action.save')}
              </Button>
            </div>
          </div>
        </Card>
      )}
    </Fragment>
  );
};
