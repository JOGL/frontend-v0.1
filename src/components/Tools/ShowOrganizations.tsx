import { FC } from 'react';
import Grid from '../Grid';
import Loading from './Loading';
import NoResults from './NoResults';
import useGet from '~/src/hooks/useGet';
import OrganizationCard from '../Organization/OrganizationCard';
import { ItemType } from '~/src/types';
import {
  getContainerAccessLevelChip,
  getContainerJoiningRestrictionChip,
  getContainerVisibilityChip,
} from '~/src/utils/utils';

interface Props {
  objectId: string;
  objectType: ItemType;
}

const ShowOrganizations: FC<Props> = ({ objectId, objectType }) => {
  const { data: organizations } = useGet(`/${objectType}/${objectId}/organizations`);

  return (
    <div>
      <Grid>
        {!organizations ? (
          <Loading />
        ) : organizations?.length === 0 ? (
          <NoResults />
        ) : (
          [...organizations]
            // .reverse()
            .map((organization, i) => (
              <OrganizationCard
                id={organization.id}
                key={organization.id}
                short_title={organization.short_title}
                title={organization.title}
                short_description={organization.short_description}
                members_count={organization.stats?.members_count}
                status={organization.status}
                banner_url={organization.banner_url || organization.banner_url_sm}
                last_activity={organization.updated}
                chip={organization?.user_access_level && getContainerAccessLevelChip(organization)}
                imageChips={
                  !organization?.user_access_level
                    ? [getContainerVisibilityChip(organization), getContainerJoiningRestrictionChip(organization)]
                    : [getContainerVisibilityChip(organization)]
                }
              />
            ))
        )}
      </Grid>
    </div>
  );
};

export default ShowOrganizations;
