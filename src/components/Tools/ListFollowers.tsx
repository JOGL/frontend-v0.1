import React, { FC } from 'react';
import Loading from '~/src/components/Tools/Loading';
import useGet from '~/src/hooks/useGet';
import { ItemType } from '~/src/types';
import Grid from '../Grid';
import UserCard from '../User/UserCard';

interface Props {
  itemId: number;
  itemType: ItemType;
}
const ListFollowers: FC<Props> = ({ itemId, itemType }) => {
  const { data, isLoading } = useGet(`/${itemType}/${itemId}/followers`);

  if (isLoading) return <Loading />;
  return (
    <div className="listFollowers">
      <Grid tw="gap-4">
        {data?.map((follower, i) => (
          <UserCard key={i} user={follower} isCompact />
        ))}
      </Grid>
    </div>
  );
};
export default ListFollowers;
