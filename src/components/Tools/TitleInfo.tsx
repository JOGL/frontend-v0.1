import { FC } from 'react';
import ReactTooltip from 'react-tooltip';
import tw from 'twin.macro';
import Icon from '~/src/components/primitives/Icon';

interface Props {
  title: string;
  id?: string;
  mandatory?: boolean;
  tooltipMessage?: string;
  isFormSubpartTitle?: boolean;
  isPrivate?: boolean;
  customTitleColor?: string;
}

const TitleInfo: FC<Props> = ({
  title,
  id,
  mandatory,
  tooltipMessage,
  isFormSubpartTitle = false,
  isPrivate = false,
  customTitleColor,
}) => (
  <label htmlFor={id} tw="flex items-center" className="titleInfo">
    <span
      style={customTitleColor && { color: customTitleColor }}
      css={[
        tw`text-[1rem] font-bold text-gray-700 underline sm:no-underline`,
        isFormSubpartTitle && tw`mb-1 text-lg no-underline text-primary`,
      ]}
    >
      {title}
    </span>
    {mandatory && <div tw="text-danger contents">&nbsp;*</div>}
    {isPrivate && (
      <div tw="pl-2">
        <Icon icon="mdi:eye-off-outline" tw="color[rgba(147, 144, 144, 0.9)]" />
      </div>
    )}
    {tooltipMessage && (
      <div tw="pl-2">
        <Icon
          icon="pajamas:question"
          tw="w-4 h-4"
          data-tip={tooltipMessage}
          data-for="bannerTooltip"
          tabIndex={0}
          onFocus={(e) => ReactTooltip.show(e.target)}
          onBlur={(e) => ReactTooltip.hide(e.target)}
        />
        <ReactTooltip
          id="bannerTooltip"
          effect="solid"
          type="dark"
          className="solid-tooltip"
          overridePosition={({ left, top }, _e, _t, node) => {
            return {
              top,
              left: typeof node === 'string' ? left : Math.max(left, 20),
            };
          }}
        />
      </div>
    )}
  </label>
);

export default TitleInfo;
