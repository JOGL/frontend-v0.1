import Axios from 'axios';
import React, { FC, useEffect, useState, ReactNode } from 'react';
import { useApi } from '~/src/contexts/apiContext';
import { ItemType } from '~/src/types';
import useTranslation from 'next-translate/useTranslation';
import useUser from '~/src/hooks/useUser';
import SpinLoader from './SpinLoader';
import Icon from '~/src/components/primitives/Icon';
import { TwStyle } from 'twin.macro';
import { entityItem } from '~/src/utils/utils';

interface Props {
  joinState?: string;
  joinType: string;
  itemId: string;
  itemType: ItemType;
  textJoin?: string | ReactNode;
  textUnjoin?: string | ReactNode;
  isPrivate?: boolean;
  count?: number;
  hasNoStat?: boolean;
  pending_invite?: any;
  showMembersModal?: (e: any) => void | Promise<boolean>;
  onJoin?: () => void | Promise<boolean>;
  isDisabled?: boolean;
  customCss?: TwStyle[];
}

const BtnJoin: FC<Props> = ({
  joinState: propsJoinState = '',
  joinType,
  itemId,
  itemType,
  textJoin,
  textUnjoin,
  isPrivate,
  count: countProp = 0,
  hasNoStat = false,
  pending_invite,
  showMembersModal,
  onJoin,
  isDisabled,
  customCss,
}) => {
  const [joinState, setJoinState] = useState(propsJoinState);
  const [action, setAction] = useState<'request' | 'leave'>(!propsJoinState ? 'request' : 'leave');
  const [count, setCount] = useState(countProp);
  const [sending, setSending] = useState(false);
  const api = useApi();
  const { t } = useTranslation('common');
  const { user } = useUser();

  useEffect(() => {
    const axiosSource = Axios.CancelToken.source();
    // This will prevent to setJoinState on unmounted BtnJoin
    return () => axiosSource.cancel();
  }, [api, itemId, itemType, user]);

  useEffect(() => {
    setAction(joinType === 'open' ? 'join' : 'request');
  }, [joinState]);

  useEffect(() => {
    pending_invite && setJoinState('pending');
  }, [pending_invite]);

  const changeStateJoin = (e) => {
    if (itemId && itemType && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      setSending(true);
      const route =
        joinState === 'pending'
          ? `/${entityItem[itemType].back_path}/${itemId}/invites/${pending_invite?.invitation_id}/cancel`
          : `/${entityItem[itemType].back_path}/${itemId}/${action}`;
      action === 'request' && setJoinState('pending');
      joinState === 'pending' && setJoinState('request');

      api
        .post(route)
        .then((res) => {
          const userId = res.config.headers.userId;
          joinState === 'pending' && setJoinState('request');
          action === 'request' && setJoinState('pending');
          setSending(false);
          action === 'request' ? onJoin() : location.reload();
        })
        .catch((err) => {
          console.error(`Couldn't PUT ${itemType} with itemId=${itemId} and action=${action}`, err);
          setSending(false);
        });
    }
  };

  const textJoin2 = textJoin || t('action.join');
  const textUnjoin2 = textUnjoin || t('action.leave');
  const textPending = t('pending');
  const btnText = joinState === 'pending' ? textPending : joinState === true ? textUnjoin2 : textJoin2;

  return (
    <span
      tw="relative z-0 cursor-pointer inline-flex items-center border px-[12px] py-[0px] text-[18px] font-[500] border-solid border-[rgba(228, 228, 228, 1)] rounded-[40px] bg-[white] color[rgba(95, 93, 93, 1)] hover:bg-gray-100"
      tabIndex={0}
      css={customCss}
    >
      <div
        tw="relative inline-flex items-center py-[5px] pl-[4px]"
        onClick={(e) => {
          e.preventDefault();
          !(sending || isDisabled) && changeStateJoin(e);
        }}
      >
        {sending ? (
          <SpinLoader />
        ) : joinState ? (
          <Icon icon="ic:round-groups" />
        ) : (
          <Icon icon="material-symbols:person-add-outline" tw="w-5 h-5" />
        )}
        <span>{btnText}</span>
      </div>
      {!hasNoStat && count && (
        <>
          <span tw="h-full w-[1px] bg-[rgba(228, 228, 228, 1)] mx-[12px]" />
          <div tw="-ml-px relative inline-flex items-center py-[3px]" onClick={showMembersModal}>
            {count}
          </div>
        </>
      )}
    </span>
  );
};

export default BtnJoin;
