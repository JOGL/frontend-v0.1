import { FC } from 'react';
import A from '../primitives/A';
import { useRouter } from 'next/router';
import { TextWithPlural } from '~/src/utils/managePlurals';
import { ItemType } from '~/src/types';

interface Props {
  type: ItemType;
  object?: string;
  tab?: string;
  count?: number;
  noLink?: boolean;
}

const InfoStats: FC<Props> = ({ object, type, tab, count, noLink = false }) => {
  const router = useRouter();
  const objectId = router.query.id;
  if (!noLink) {
    return (
      <div tw="flex flex-col justify-between px-2 py-1 text-center hover:opacity-80">
        <A href={`/${object}/${objectId}?tab=${tab}`} shallow noStyle scroll={false}>
          <>
            <div tw="text-xl font-bold">{count}</div>
            <TextWithPlural type={type} count={count} />
          </>
        </A>
      </div>
    );
  }
  return (
    <div tw="flex flex-col justify-between px-2 py-1 text-center hover:opacity-80">
      <div tw="text-2xl font-bold">{count}</div>
      <TextWithPlural type={type} count={count} />
    </div>
  );
};

export default InfoStats;
