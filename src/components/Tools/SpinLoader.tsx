import { CSSProperties, FC } from 'react';
import Icon from '~/src/components/primitives/Icon';

interface Props {
  size?: string;
  customCSS?: CSSProperties;
}
const SpinLoader: FC<Props> = ({ size = '18', customCSS }) => (
  <Icon icon="quill:loading-spin" width={size} height={size} style={customCSS} tw="animate-spin mr-2" />
);
export default SpinLoader;
