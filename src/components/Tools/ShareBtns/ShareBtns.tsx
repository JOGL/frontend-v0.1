import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import { useModal } from '~/src/contexts/modalContext';
import { addressFront, copyLink, confAlert, entityItem, copyPostLink } from '~/src/utils/utils';
import useUser from '~/src/hooks/useUser';
import { ItemType } from '~/src/types';
import { FC, forwardRef } from 'react';
import tw from 'twin.macro';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import Icon from '~/src/components/primitives/Icon';

interface Props {
  type: ItemType;
  isActionMenu?: boolean;
  ref?: any;
  specialObjId?: string;
  url?: string;
}

const generateEmbedCode = (objectId, objectType) => {
  const link = `${window.location.origin}/embedded/${entityItem[objectType].back_path}?id=${objectId}`;
  return `<iframe src="${link}" height="450" width="305" frameborder="0" allowfullscreen="" title="JOGL ${objectType} ${objectId}"></iframe>`;
};

const ShareBtns: FC<Props> = forwardRef(({ type, specialObjId, isActionMenu, url }, ref: any) => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const { user } = useUser();
  const { showModal } = useModal();
  const onShareButtonClick = (e) => {
    // set objectId depending on type
    const objectId =
      type === 'post' || type === 'need' || type === 'jogldoc' || type === 'paper' || type === 'event'
        ? specialObjId
        : router.query.id;
    const iframeObjectId = specialObjId || router.query.id;
    // make objUrlPath the router asPath, except if we are sharing need or posts, in which case the url is the one from the single need or post page
    const objUrlPath =
      url ||
      (type === 'need'
        ? `/need/${objectId}`
        : type === 'post'
        ? `/post/${objectId}`
        : type === 'jogldoc'
        ? `/doc/${objectId}`
        : type === 'event'
        ? `/event/${objectId}`
        : type === 'paper'
        ? `/${entityItem[type].front_path}`
        : router.asPath);

    // check if browser/phone support the native share api (meaning we are in a mobile)
    if (navigator.share && window?.innerWidth < 700) {
      navigator
        .share({
          title: type === 'jogldoc' ? 'JOGL Doc' : t(entityItem[type].translationId),
          text: `${t('action.checkOutThisItem', {
            item: type === 'jogldoc' ? 'JOGL Doc' : t(entityItem[type].translationId),
          })}: `,
          url: addressFront + objUrlPath,
        })
        .catch(console.error);
    } else {
      // else open modal with defined sharing methods
      if ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) {
        // if function is launched via keypress, execute only if it's the 'enter' key
        showModal({
          children: (
            <ShareModal
              type={type}
              objectId={iframeObjectId}
              objUrlPath={objUrlPath}
              userId={user?.id}
              url={url}
              t={t}
            />
          ),
          title:
            type === 'jogldoc'
              ? 'JOGL Doc'
              : t('action.shareItem', {
                  item: type !== 'user' ? t(entityItem[type].translationId) : t('profile'),
                }),
        });
      }
    }
  };

  return (
    // different style depending on if type is post
    !isActionMenu ? (
      <span tw="relative z-0 inline-flex rounded-md" css={[type !== 'post' && tw`shadow-sm`]}>
        <button
          type="button"
          ref={ref}
          tw="relative inline-flex items-center px-2 py-[6px] rounded-md border bg-white text-sm font-medium text-gray-700 focus:(z-10 outline-none ring-1 ring-primary border-primary)"
          css={[type !== 'post' && tw`border-gray-300 border-solid hover:bg-gray-100`]}
          className={type === 'post' ? 'btn-postcard' : ''}
          title={t('action.shareItem', { item: t(entityItem[type].translationId) })}
          onClick={onShareButtonClick}
        >
          {type === 'post' ? <Icon icon="uil:share" /> : <Icon icon="fa-solid:share" tw="w-5 h-5" />}
          <span tw="pl-[2px]">{t('action.share')}</span>
        </button>
      </span>
    ) : (
      <div onClick={onShareButtonClick} tw="flex items-center cursor-pointer hover:bg-[#1D4B99] hover:text-white!">
        <Icon icon="gg:arrow-up-o" tw="w-4 h-4" />
        {t('action.share')}
      </div>
    )
  );
});

export const ShareModal = ({ type, objectId, objUrlPath, userId, t, url }) => {
  const objUrl = addressFront + encodeURIComponent(objUrlPath);

  const openWindow = (socialMedia) => {
    let shareUrl;
    if (socialMedia === 'facebook') shareUrl = `https://www.facebook.com/sharer/sharer.php?u=${objUrl}`;
    if (socialMedia === 'twitter')
      shareUrl = `https://twitter.com/intent/tweet/?text=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform:&hashtags=JOGL&url=${objUrl}`;
    if (socialMedia === 'linkedin')
      shareUrl = `https://www.linkedin.com/shareArticle?mini=true&url=${objUrl}&title=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform&amp;&source=${objUrl}`;
    if (socialMedia === 'reddit')
      shareUrl = `https://reddit.com/submit/?url=${objUrl}&resubmit=true&amp;title=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform:&amp;`;
    if (socialMedia === 'whatsapp')
      shareUrl = `https://api.whatsapp.com/send?text=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform: ${objUrl}&preview_url=true`;
    if (socialMedia === 'telegram')
      shareUrl = `https://telegram.me/share/url?text=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform:&amp;url=${objUrl}`;
    window.open(shareUrl, 'share-dialog', 'width=626,height=436');
  };

  return (
    <div className="share-dialog is-open">
      <div tw="inline-flex gap-2 w-full">
        <div tw="flex space-x-2 relative w-full">
          <div tw="absolute top-0 bg-[#D0D0D0] p-1 rounded-tl-md rounded-bl-md h-[39px] w-[39px] items-center flex justify-center">
            <Icon icon="ph:link" tw="mr-0 h-5 w-5 text-[#5959599e]" />
          </div>
          <input
            type="text"
            name="link"
            id="link"
            tw="flex-1 min-w-0 block w-full px-3 py-[0.4rem] rounded-md border-[#cccccc] pl-10"
            value={`${window.location.origin}${objUrlPath}`}
            disabled={true}
          />
          <button
            type="button"
            onClick={() => (url ? copyLink(url, t) : copyPostLink(objectId, entityItem[type].front_path, undefined, t))}
            rel="noopener"
            aria-label="Copy link"
          >
            <Icon icon="ic:baseline-content-copy" />
          </button>
        </div>
      </div>
      <p tw="text-xl font-medium mt-5 mb-0">Social medias</p>
      <div className="targets">
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('facebook')}
          aria-label="Share on Facebook"
        >
          <div className="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--large">
            <Icon icon="ic:round-facebook" tw="w-5 h-5" />
            Facebook
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('twitter')}
          aria-label="Share on Twitter"
        >
          <div className="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--large">
            <Icon icon="ri:twitter-fill" tw="w-5 h-5" /> Twitter
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('linkedin')}
          aria-label="Share on LinkedIn"
        >
          <div className="resp-sharing-button resp-sharing-button--linkedin resp-sharing-button--large">
            <Icon icon="mdi:linkedin" tw="w-5 h-5" /> LinkedIn
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('reddit')}
          aria-label="Share on Reddit"
        >
          <div className="resp-sharing-button resp-sharing-button--reddit resp-sharing-button--large">
            <Icon icon="ic:baseline-reddit" tw="w-5 h-5" /> Reddit
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('whatsapp')}
          aria-label="Share on WhatsApp"
        >
          <div className="resp-sharing-button resp-sharing-button--whatsapp resp-sharing-button--large">
            <Icon icon="ic:baseline-whatsapp" tw="w-5 h-5" />
            Whatsapp
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('telegram')}
          aria-label="Share on Telegram"
        >
          <div className="resp-sharing-button resp-sharing-button--telegram resp-sharing-button--large">
            <Icon icon="ic:baseline-telegram" tw="w-5 h-5" />
            Telegram
          </div>
        </button>
        <button type="button" className="resp-sharing-button__link" rel="noopener" aria-label="Share by E-Mail">
          <a
            href={`mailto:?subject=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform&body=${objUrl}`}
            target="_self"
          >
            <div className="resp-sharing-button resp-sharing-button--email resp-sharing-button--large">
              <Icon icon="fa6-solid:envelope" tw="w-5 h-5" />
              Email
            </div>
          </a>
        </button>
      </div>
      {/* share as iframe! */}
      <div tw="mt-5">
        <div tw="flex justify-between">
          <p tw="text-xl font-medium mb-0">{t('action.embed')}</p>
          <CopyToClipboard
            text={generateEmbedCode(objectId, type)}
            onCopy={() => confAlert.fire({ icon: 'success', title: t('copiedSuccess') })}
          >
            <button tw="text-blue-500">{t('action.copyCode')}</button>
          </CopyToClipboard>
        </div>
        <pre tw="w-full bg-gray-100 p-2 mt-2 mb-2 max-h-52 break-words" style={{ whiteSpace: 'pre-wrap' }}>
          {generateEmbedCode(objectId, type)}
        </pre>
      </div>
    </div>
  );
};

export default ShareBtns;
