import useTranslation from 'next-translate/useTranslation';
import { User } from '~/src/types';
import Button from '../primitives/Button';
import { useModal } from '~/src/contexts/modalContext';
import { FC, useState } from 'react';
import Alert from '~/src/components/Tools/Alert/Alert';
import P from '../primitives/P';
import { useApi } from '~/src/contexts/apiContext';
import { confAlert } from '~/src/utils/utils';
import { useRouter } from 'next/router';

interface MoreTabForContainerOrProfileArchiveOrDeleteProps {
  containerOrProfile: User | any;
  setContainerOrProfile: (data: any) => void;
  handleSubmit: (data: any) => void;
  user: User;
  apiURL: string;
  translateText: string;
  containerType: 'hub' | 'organization' | 'user';
}

export const MoreTabForContainerOrProfileArchiveOrDelete: FC<MoreTabForContainerOrProfileArchiveOrDeleteProps> = ({
  containerOrProfile,
  setContainerOrProfile,
  handleSubmit,
  user,
  containerType,
  translateText,
  apiURL,
}) => {
  const { t } = useTranslation('common');
  const api = useApi();
  const router = useRouter();
  const [errors, setErrors] = useState(undefined);

  const delBtnTitleId = containerOrProfile?.members_count > 1 ? 'action.archive' : 'action.delete';
  const delBtnTextId = containerOrProfile?.members_count > 1 ? 'areYouSure' : 'deleteConfirmation';

  const { showModal, closeModal } = useModal();

  const handleContainerOrProfileArchive = () => {
    const status = containerOrProfile.status === 'archived' ? 'active' : 'archived';
    setContainerOrProfile((prevState) => ({
      ...prevState,
      status,
    }));
    handleSubmit({ status });
  };

  const deleteProj = () => {
    api
      .delete(`${apiURL}/${containerOrProfile.id}`)
      .then(() => {
        closeModal(); // close modal
        confAlert.fire({ icon: 'success', title: `${containerType} was deleted` });
        router.push('/');
      })
      .catch((error) => {
        setErrors(error.toString());
      });
  };

  const errorMessage = errors?.includes('err-') ? t(errors) : errors;

  return (
    <div className="deleteBtns">
      {containerOrProfile && (
        <button
          tw="ml-3 bg-blue-500 active:bg-blue-600 hover:bg-blue-600 text-white font-semibold py-2 px-4 rounded-lg border border-blue-600 mr-4"
          onClick={handleContainerOrProfileArchive}
        >
          {containerOrProfile.status === 'archived' ? t('action.publish') : t('action.archive')} {t(translateText)}
        </button>
      )}
      {containerOrProfile && containerOrProfile.status === 'archived' && (
        <Button
          onClick={() => {
            showModal({
              children: (
                <>
                  {errors && <Alert type="danger" message={errorMessage} />}
                  <P tw="text-base">{t(delBtnTextId)}</P>
                  <div tw="inline-flex space-x-3">
                    <Button btnType="danger" onClick={deleteProj}>
                      {t('yes')}
                    </Button>
                    <Button onClick={closeModal}>{t('no')}</Button>
                  </div>
                </>
              ),
              title: t(delBtnTitleId),
              maxWidth: '30rem',
            });
          }}
          btnType="danger"
          tw="bg-red-500 hover:bg-red-600 text-white font-semibold py-2 px-4 rounded-lg"
        >
          {t(delBtnTitleId)} {t(translateText)}
        </Button>
      )}
    </div>
  );
};
