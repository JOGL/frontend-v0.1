import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Chips from '../Chip/Chips';
import { defaultSdgsInterests } from '~/src/utils/utils';
import tw from 'twin.macro';

interface Props {
  interests: string[];
  skills: string[];
  noPadding?: boolean;
}

const ShowSkillSdg: FC<Props> = ({ interests, skills, noPadding = false }) => {
  const { t } = useTranslation('common');
  const defaultInterests = defaultSdgsInterests(t);

  return (
    <div tw="flex-wrap w-full my-2" css={!noPadding && tw`md:px-4`}>
      <div tw="flex flex-col w-full md:justify-between">
        <div tw="w-full flex flex-col flex-wrap">
          <div tw="flex gap-2 flex-wrap">
            <span tw="underline font-bold text-gray-700 sm:no-underline">{t('keyword', { count: 2 })}:</span>
            {skills?.length > 0 && (
              <Chips
                data={skills.map((skill) => ({
                  title: skill,
                }))}
                type="skills"
                smallChips
                overflowText="seeMore"
                showCount={5}
              />
            )}
            {interests?.length > 0 && (
              <Chips
                data={(interests ?? [])?.map((interestId) => {
                  const interest = defaultInterests.find((el) => el.value === interestId);
                  return {
                    title: `${t('sdgs')} ${interest?.value} : ${interest?.label}`,
                    href: `/`,
                  };
                })}
                type="interests"
                smallChips
                overflowText="seeMore"
                showCount={5}
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ShowSkillSdg;
