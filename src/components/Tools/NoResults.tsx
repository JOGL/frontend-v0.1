import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';

interface Props {
  type?: 'activity' | 'proposal' | 'hub' | 'need' | 'event' | 'document' | 'publication';
}
const NoResults: FC<Props> = ({ type }) => {
  const { t } = useTranslation('common');
  return (
    <p tw="font-medium text-[#C5CED5] text-2xl">
      {type ? t('noItems', { item: t(type, { count: 2 }) }) : t('noResult', { count: 2 })}
    </p>
  );
};

export default NoResults;
