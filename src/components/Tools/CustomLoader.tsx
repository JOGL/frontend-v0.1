import Loading from './Loading';

export default function CustomLoader() {
  return (
    <div
      style={{
        display: 'flex',
        height: '100vh',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        backgroundColor: '#f0f0f0',
      }}
    >
      <img src="/images/logo-vertical.png" alt="JOGL icon" width="150px" />
      <Loading />
    </div>
  );
}
