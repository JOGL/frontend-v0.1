import { FC, forwardRef, useImperativeHandle, useRef, useState } from 'react';
import SpinLoader from './SpinLoader';
import Icon from '~/src/components/primitives/Icon';
import ImageCropper from './ImageCropper';
import { useModal } from '~/src/contexts/modalContext';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from '~/src/contexts/apiContext';
import tw from 'twin.macro';

const SingleImageUploadFile: FC<any> = forwardRef(
  (
    {
      text,
      imageUrl,
      fileTypes,
      maxSizeFile,
      handleOnChange,
      handleRemoveImage,
      itemId,
      itemType,
      type,
      userId = undefined,
      aspectRatio = 1,
      isRounded,
    },
    ref: any
  ) => {
    const [loading, setLoading] = useState(false);
    const { showModal, closeModal } = useModal();
    const { t } = useTranslation('common');
    const api = useApi();
    const inputRef = useRef<any>();

    useImperativeHandle(ref, () => ({
      click: () => {
        changeProfileImage({
          openFileDialog: imageUrl ? false : true,
        });
      },
    }));

    const submit = (file, base64) => {
      const route = '/images';
      api
        .post(route, {
          title: file.name ?? file.file_name,
          file_name: file.name ?? file.file_name,
          description: '',
          created: file?.lastModified ? new Date(file.lastModified).toISOString() : file.created,
          updated: file?.lastModified ? new Date(file.lastModified).toISOString() : new Date(),
          data: base64,
          created_by_user_id: userId ?? '',
        })
        .then((res) => {
          if (res.status === 200) {
            setLoading(false); // set to false to show user upload has been successful
            if (itemType === 'needs') alert(t('fileUploadedUpdateTheNeed'));
            if (res.data) {
              handleOnChange({ error: '', img: res.data }); // on success, show new uploading image in the front
            }
          } else {
            setLoading(false); // stop uploading if error
            handleOnChange({ error: t('error.generic'), img: '' });
          }
        })
        .catch((err) => {
          console.error(err);
          setLoading(false);
        });
      closeModal();
    };

    const handleChangeLogo = (data, base64) => {
      if (!itemId || !itemType || !type) {
        handleOnChange({ error: t('error.generic') });
      } else {
        setLoading(true);
        submit(data, base64);
      }
    };

    const changeProfileImage = (event) => {
      // eslint-disable-next-line no-unused-expressions
      event?.preventDefault?.();
      const customClassName = isRounded && 'custom-profile-image';

      showModal({
        children: (
          <ImageCropper
            currentImage={imageUrl}
            handleOnChange={handleOnChange}
            maxSizeFile={maxSizeFile}
            fileTypes={fileTypes}
            aspectRatio={aspectRatio}
            ref={inputRef}
            openFileDialog={imageUrl ? false : true}
            customClassName={customClassName}
            onSave={(data, base64) => handleChangeLogo(data, base64)}
            onRemove={handleRemoveImage}
            onCancel={closeModal}
            type={type}
          />
        ),
        showCloseButton: false,
        showTitle: false,
        maxWidth: '50rem',
      });
    };

    return (
      <div
        className="upload-btn-wrapper"
        css={type !== 'avatar' && tw`right-5`}
        tw="absolute bottom-0 right-0 z-[1] flex items-center justify-center text-gray-700 rounded-full"
      >
        <div>
          {loading ? ( // render differently depending on uploading state
            <SpinLoader customCSS={tw`m-0`} />
          ) : (
            <div tw="cursor-pointer" onClick={changeProfileImage}>
              <Icon icon="typcn:plus" tw="text-white m-0" />
            </div>
          )}
        </div>
      </div>
    );
  }
);

export default SingleImageUploadFile;
