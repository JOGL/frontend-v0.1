import { FC, useState } from 'react';
import useUserData from '~/src/hooks/useUserData';
import CfpCard from '../Cfp/CfpCard';
import Loading from './Loading';
import { CfpAttachToWorkspace } from '../Cfp/CfpAttachToWorkspace';
import { ItemType } from '~/src/types';
import {
  getContainerAccessLevelChip,
  getContainerJoiningRestrictionChip,
  getContainerVisibilityChip,
} from '~/src/utils/utils';
import api from '~/src/utils/api/api';
import { useQuery, keepPreviousData } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { SpaceTabHeader } from '../Common/space/spaceTabHeader/spaceTabHeader';
import { Empty, Flex, Modal } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { SpaceContentStyled } from '../Common/space/space.styles';
import useDebounce from '~/src/hooks/useDebounce';

interface Props {
  parentType: ItemType;
  parentId: string;
  isAdmin: boolean;
}

const ShowCfps: FC<Props> = ({ parentType, parentId, isAdmin }) => {
  const { t } = useTranslation('common');
  const { userData } = useUserData();
  const [searchText, setSearchText] = useState('');
  const debouncedSearch = useDebounce(searchText, 300);

  const [showModal, setShowModal] = useState(false);

  const fetchCFP = async () => {
    const response = await api[parentType].cfpsDetail(parentId, { Search: debouncedSearch });
    return response.data;
  };

  const { data: cfps, isLoading, isPlaceholderData } = useQuery({
    queryKey: [QUERY_KEYS[`${parentType}CfpsList`], debouncedSearch],
    queryFn: fetchCFP,
    placeholderData: keepPreviousData,
  });

  return (
    <div>
      <SpaceTabHeader
        title={t('callForProposals.other')}
        searchText={searchText}
        setSearchText={setSearchText}
        canCreate={isAdmin && parentType === 'workspaces'}
        handleClick={() => setShowModal(true)}
        showSearch={!!searchText || !!cfps?.length}
      />

      <SpaceContentStyled>
        {!isLoading && !cfps?.length && <Empty />}

        <Flex wrap gap="middle">
          {isLoading && !isPlaceholderData ? (
            <Loading />
          ) : (
            [...cfps]
              // .reverse()
              .map((cfp, i) => (
                <CfpCard
                  id={cfp.id}
                  key={cfp.id}
                  short_title={cfp.short_title}
                  title={cfp.title}
                  short_description={cfp.short_description}
                  membersCount={cfp.stats?.members_count}
                  proposalsCount={cfp.stats?.submitted_proposals_count}
                  status={cfp.status}
                  banner_url={cfp.banner_url || cfp.banner_url_sm}
                  last_activity={cfp.updated}
                  submissions_from={cfp.submissions_from}
                  submissions_to={cfp.submissions_to}
                  chip={cfp?.user_access_level && getContainerAccessLevelChip(cfp)}
                  imageChips={
                    !cfp?.user_access_level
                      ? [getContainerVisibilityChip(cfp), getContainerJoiningRestrictionChip(cfp)]
                      : [getContainerVisibilityChip(cfp)]
                  }
                />
              ))
          )}
        </Flex>
      </SpaceContentStyled>

      {showModal && (
        <Modal open onCancel={() => setShowModal(false)} title={t('createACallForProposal')} footer={null}>
          <CfpAttachToWorkspace
            userId={userData?.id}
            defaultValue={{ id: parentId }}
            userLogoUrl={userData?.logo_url ?? userData?.logo_url_sm}
            closeModal={() => setShowModal(false)}
          />
        </Modal>
      )}
    </div>
  );
};

export default ShowCfps;
