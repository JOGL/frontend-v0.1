import { Dispatch, SetStateAction, useEffect } from 'react';
import Select from 'react-select';
import Icon from '../primitives/Icon';

interface PagedTableControlProps {
  slice?: unknown[];
  page: number;
  totalPages: number;
  rowsPerPage: number;
  rowsPerPageOptions: number[];
  setPage: Dispatch<SetStateAction<number>>;
  setRowsPerPage: Dispatch<SetStateAction<number>>;
}

const PagedTableControl = ({
  slice,
  page,
  totalPages,
  rowsPerPage,
  rowsPerPageOptions,
  setPage,
  setRowsPerPage,
}: PagedTableControlProps) => {
  useEffect(() => {
    if ((slice?.length ?? 0) < 1 && page !== 1) {
      setPage(page - 1);
    }
  }, [slice, page, totalPages, rowsPerPage, setPage]);

  return (
    <div tw="flex gap-2 items-center h-[42px]">
      <Select
        options={rowsPerPageOptions?.map((op) => {
          return {
            value: op,
            label: `${op}`,
          };
        })}
        defaultValue={{
          value: rowsPerPage,
          label: `${rowsPerPage}`,
        }}
        isSearchable={false}
        onChange={(option) => setRowsPerPage(option?.value)}
        tw="w-24 text-center"
      />

      <div>
        <button onClick={() => setPage(page - 1)} disabled={page === 1}>
          <Icon icon="bxs:left-arrow" tw="h-5 w-5 text-[#CCCCCC] m-0" />
        </button>

        <span tw="text-sm font-semibold">
          {page} / {totalPages}
        </span>

        <button onClick={() => setPage(page + 1)} disabled={page === totalPages}>
          <Icon icon="bxs:right-arrow" tw="h-5 w-5 text-[#CCCCCC] m-0" />
        </button>
      </div>
    </div>
  );
};

export default PagedTableControl;
