import React, { useEffect, useState } from 'react';
import { useApi } from '~/src/contexts/apiContext';
import { ItemType } from '~/src/types';
import SpinLoader from './SpinLoader';
import tw from 'twin.macro';
import Icon from '../primitives/Icon';
import { useModal } from '~/src/contexts/modalContext';
import Grid from '../Grid';
import UserCard from '../User/UserCard';
import Loading from './Loading';

interface Props {
  clapCount?: number;
  postId?: string;
  itemType?: ItemType;
  refresh?: (state?: any) => void;
  fromJOGLDoc?: boolean;
  userReactions: any;
  userId: string;
}
const BtnClap = ({
  clapCount: propsClapCount = 0,
  postId = undefined,
  itemType = undefined,
  fromJOGLDoc = false,
  refresh = undefined,
  userReactions = [],
  userId,
}: Props) => {
  const ogClapState = userReactions?.filter((reaction) => reaction.type === 'clap').length > 0;
  const [clapState, setClapState] = useState(ogClapState);
  const [clapCount, setClapCount] = useState(propsClapCount);
  const [isSending, setIsSending] = useState(false);
  const [action, setAction] = useState<'clap' | 'unclap'>(ogClapState ? 'unclap' : 'clap');
  const api = useApi();

  const { showModal } = useModal();

  if (fromJOGLDoc && ogClapState !== clapState) {
    setClapState(ogClapState);
  }

  const showClapModal = () => {
    api.get(`/feed/contentEntities/${postId}/reactions`).then((res) => {
      const reactions = res.data;
      showModal({
        children: <ClappersModal reactions={reactions?.filter((x) => x.type === 'clap')} />,
        title: 'Likes',
        maxWidth: '40rem',
      });
    });
  };

  useEffect(() => {
    setAction(clapState ? 'unclap' : 'clap');
  }, [clapState]);

  const changeStateClap = () => {
    // toggle display of comments unless we don't want the comments to show at all (in post card)
    if (fromJOGLDoc) {
      const newClapCount = clapState ? clapCount - 1 : clapCount + 1;
      setClapCount(newClapCount);
      setClapState((c) => !c);
      refresh({
        userId: userId,
        clapState: !clapState,
        clapCount: newClapCount,
      });
      return;
    }
    const reactionId = userReactions.length > 0 ? userReactions?.find((reaction) => reaction.type === 'clap').id : '';
    if (postId && itemType) {
      setIsSending(true);
      // snd event to google analytics
      const newClapCount = clapState ? clapCount - 1 : clapCount + 1;
      if (action === 'clap') {
        api
          .post(`/feed/contentEntities/${postId}/reactions`, { type: 'clap' })
          .then(() => {
            setClapCount(newClapCount);
            setClapState(true);
            setIsSending(false);
            refresh();
          })
          .catch(() => {
            setIsSending(false);
          });
      } else {
        api
          .delete(`/feed/contentEntities/${postId}/reactions/${reactionId}`)
          .then(() => {
            setClapCount(newClapCount);
            setClapState(false);
            setIsSending(false);
            refresh();
          })
          .catch(() => {
            setIsSending(false);
          });
      }
    }
  };
  return (
    <div
      className="btn-postcard"
      css={clapState ? tw`flex items-center` : tw`flex items-center text-gray-800`}
      tw="flex cursor-default"
    >
      <button onClick={changeStateClap} css={clapState && tw`text-action`} tw="hover:text-action cursor-pointer">
        {isSending ? <SpinLoader /> : <Icon icon="wpf:like" tw="transform[scaleX(-1)]" />}
      </button>
      <button
        onClick={() => clapCount > 0 && showClapModal()}
        css={clapCount > 0 ? tw`cursor-pointer hover:underline` : tw`cursor-default`}
      >
        {clapCount ?? 0}
      </button>
    </div>
  );
};

const ClappersModal = ({ reactions }) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const fetchDataForId = async (id) => {
        try {
          const response = await fetch(`${process.env.ADDRESS_BACK}/users/${id}`);
          const result = await response.json();
          setData((prevData) => [...prevData, result]);
        } catch (error) {
          console.error(`Error fetching data for ID ${id}:`, error);
        }
      };

      const fetchAllData = async () => {
        if (reactions?.length > 0) {
          const promises = reactions?.map((reaction) => fetchDataForId(reaction.user_id));
          await Promise.all(promises);
        }
      };

      fetchAllData();
    };

    fetchData();
  }, []);

  return (
    <>
      {data.length !== 0 ? (
        <Grid tw="gap-4">
          {data.map((user) => (
            <UserCard key={user.id} user={user} isCompact />
          ))}
        </Grid>
      ) : (
        <Loading />
      )}
    </>
  );
};

export default BtnClap;
