import { useRouter } from 'next/router';
import { FC } from 'react';
import tw from 'twin.macro';
import { Menu, MenuItem, MenuButton, MenuList } from '@reach/menu-button';
import Icon from '../../primitives/Icon';
import Link from 'next/link';
import A from '../../primitives/A';
import { ItemType, UserMini } from '~/src/types';
import useTranslation from 'next-translate/useTranslation';
import { displayObjectDate, entityItem } from '~/src/utils/utils';
import mimetype2fa from '~/src/components/Tools/createDocument/mimetypes';
import { isKnownUrl } from '~/src/utils/isKnownUrl';
import { EntityMini } from '~/src/types/entity';
import { Document } from '~/src/types/document';
import { DocumentType } from '~/__generated__/types';
import { useMutation } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { message } from 'antd';

interface DocumentCardProps {
  id?: string;
  type: 'feed' | 'jogldoc' | 'link' | 'document' | 'resource';
  src?: string;
  title?: string;
  content?: string;
  item?: any;
  user?: UserMini;
  entity?: EntityMini;
  showMenu?: boolean;
  showEdit?: boolean;
  itemType?: ItemType;
  documentData?: string;
  document?: Document;
  onClickTitle?: () => void;
  onClickEdit?: () => void;
  deleteCard?: () => void;
  cardFormat?: string;
  displayType?: 'list' | 'grid';
  folderId?: string;
  showMove?: boolean;
  onClickMove?: () => void;
}

// Matches to translationIds
const typeToTranslationIdDic = {
  feed: 'joglDoc',
  jogldoc: 'joglDoc',
  link: 'link.one',
  document: 'file.one',
  resource: 'resource.one',
};
//TODO check id this component is used
export const DocumentCard: FC<DocumentCardProps> = ({
  id,
  type,
  src,
  title,
  content,
  user,
  entity,
  showMenu = false,
  showEdit = false,
  documentData,
  document,
  item,
  onClickTitle,
  onClickEdit,
  deleteCard,
  cardFormat,
  displayType = 'grid',
  folderId = null,
  showMove = false,
  onClickMove = () => {},
}) => {
  const router = useRouter();
  const { t } = useTranslation('common');

  const knownUrl = type === 'link' ? isKnownUrl(id) : undefined;

  const showFileType = (type) => {
    return type?.replace(/(.*)\//g, '') || 'File';
  };

  const docData = {
    title: t('untitledPage'),
    type: DocumentType.Jogldoc,
    folderId: folderId,
  };

  const mutation = useMutation({
    mutationFn: async () => {
      const response = await api.documents.documentsCreate(entity?.id ?? '', docData);
      return response.data;
    },
    onSuccess: (id) => {
      router.push(`/doc/${id}`);
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const showActionsMenu = () => (
    <>
      {(type === 'jogldoc' || type === 'link' || type === 'document' || type === 'resource') &&
        showMenu &&
        (showEdit || deleteCard) && (
          <div css={displayType === 'grid' && tw`absolute top-2 right-2`}>
            <Menu>
              <MenuButton
                tw="text-[.7rem] w-6 h-6 flex justify-center items-center text-gray-700 rounded-full"
                css={displayType === 'grid' && tw`bg-[rgb(233 233 235)] hover:bg-gray-300`}
              >
                •••
              </MenuButton>
              <MenuList tw="rounded-lg text-center text-sm divide-y divide-gray-300" className="post-manage-dropdown">
                {showEdit && (
                  <MenuItem
                    onSelect={() => {
                      if (onClickEdit) {
                        onClickEdit();
                        return;
                      }
                      if (type === 'jogldoc') {
                        mutation.mutate();
                        return;
                      }
                    }}
                  >
                    <span>{t('action.edit')}</span>
                  </MenuItem>
                )}

                {showMove && (
                  <MenuItem onSelect={() => onClickMove()}>
                    <span>{t('action.move')}</span>
                  </MenuItem>
                )}

                {deleteCard && (
                  <MenuItem onSelect={deleteCard}>
                    <span>{t('action.remove')}</span>
                  </MenuItem>
                )}
                {type === 'document' && (
                  <MenuItem>
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      href={documentData.uri}
                      download={documentData.title}
                      tw="text-gray-700 hover:(no-underline text-white)"
                    >
                      <span>{t('action.download')}</span>
                    </a>
                  </MenuItem>
                )}
              </MenuList>
            </Menu>
          </div>
        )}
    </>
  );

  const showAuthor = () => (
    <>
      {(type === 'jogldoc' || type === 'link' || type === 'document') && (
        <div tw="flex flex-row items-center justify-start text-xs gap-2">
          <img src={user?.logo_url ?? '/images/default/default-user.png'} tw="rounded-full h-7 w-7" />
          <div tw="flex flex-col items-start">
            <A href={`/user/${user?.id}`} passHref noStyle>
              <span tw="font-bold text-xs cursor-pointer hover:underline line-clamp-1 break-all">
                {user?.first_name} {user?.last_name}
              </span>
            </A>
          </div>
        </div>
      )}
    </>
  );

  const showType = () => (
    <>
      {type === 'resource' ? (
        <span
          title={t(typeToTranslationIdDic[type])}
          tw="ml-1 text-xs font-medium text-gray-600 rounded bg-white bg-opacity-80 px-2 py-1 border border-gray-200 first-letter:capitalize line-clamp-1 break-all max-w-32 w-fit leading-normal"
        >
          {t(typeToTranslationIdDic[type])}
        </span>
      ) : (
        <span
          title={type === 'document' ? showFileType(documentData.file_type) : t(typeToTranslationIdDic[type])}
          tw="ml-1 text-xs font-medium text-gray-600 rounded bg-white bg-opacity-80 px-2 py-1 border border-gray-200 first-letter:capitalize line-clamp-1 break-all max-w-32 w-fit leading-normal"
        >
          {type === 'document' ? showFileType(documentData.file_type) : t(typeToTranslationIdDic[type])}
        </span>
      )}
    </>
  );

  return displayType === 'grid' ? (
    <div
      tw="relative flex flex-col shadow-custom rounded-lg w-[240px] justify-center"
      css={cardFormat !== 'inPosts' && tw`h-[330px]`}
    >
      {type === 'link' || type === 'document' ? (
        <div tw="flex justify-center items-center w-full min-w-[240px] min-h-[92px] bg-[#dedede] max-h-[92px] object-cover rounded-t-lg">
          {type === 'document' && mimetype2fa(documentData.file_type)}
          {type === 'link' &&
            (knownUrl ? (
              <img
                src={knownUrl?.image}
                width="40px"
                height="40px"
                tw="text-[rgb(125, 125, 125)]"
                style={{ maxHeight: '40px', maxWidth: '40px' }} //needs for some svg files
                alt={knownUrl?.name}
              />
            ) : (
              <Icon icon="bx:link" tw="w-10 h-10 text-[rgb(125, 125, 125)]" />
            ))}
        </div>
      ) : (
        <img
          src={src ?? '/images/default/default-jogldoc.png'}
          tw="min-h-[92px] max-h-[92px] w-full object-cover rounded-t-lg"
          onClick={(e) => e.preventDefault()}
        />
      )}
      <div
        tw="flex flex-1 flex-col justify-between p-4"
        css={cardFormat !== 'inPosts' && tw`min-h-[238px] max-h-[238px]`}
      >
        {(type === 'document' || type === 'resource') && id ? (
          <p
            tw="text-[16px] font-bold m-0 break-words text-decoration-thickness[2px] line-clamp-3"
            css={type !== 'resource' && tw`cursor-pointer hover:underline`}
            onClick={(e) => {
              e.preventDefault();
              onClickTitle && onClickTitle();
            }}
          >
            {title}
          </p>
        ) : (
          type !== 'document' &&
          id && (
            <Link href={type === 'link' ? id : `/doc/${id}`} passHref target={type === 'link' ? '_blank' : '_self'}>
              <p tw="text-[16px] text-black! font-bold m-0 cursor-pointer break-words line-clamp-3">
                {type === 'link' ? (title?.length > 0 ? title : id) : title}
              </p>
            </Link>
          )
        )}
        {content && (
          <div
            tw="text-sm line-clamp-4 break-words h-20"
            style={{ color: '#868686' }}
            onClick={(e) => {
              e.preventDefault();
            }}
          >
            {(content ?? '').replace(/<[^>]*>?/gm, '')}
          </div>
        )}
        {(type === 'jogldoc' || type === 'link' || type === 'document') && (
          <div tw="flex flex-row items-center justify-start text-xs gap-2">
            <img src={user?.logo_url ?? '/images/default/default-user.png'} tw="rounded-full h-7 w-7" />
            <div tw="flex flex-col items-start">
              <A href={`/user/${user?.id}`} passHref noStyle>
                <span tw="font-bold text-xs cursor-pointer hover:underline line-clamp-1 break-all">
                  {user?.first_name} {user?.last_name}
                </span>
              </A>
            </div>
          </div>
        )}

        {type === 'resource' && (
          <div tw="mt-3 flex flex-col text-[12px] gap-2 pt-[10px] border-top[1px solid rgb(236, 236, 236)]">
            <div tw="flex flex-row justify-between items-center">
              <div tw="flex flex-row items-center">
                <Icon icon="mdi:star-four-points-outline" tw="w-3 h-3 mb-1" />
                {t('type')}
              </div>
              <span
                tw="p-2 rounded-md text-[11px] text-[#868686] font-semibold"
                style={{ background: 'rgb(233 233 235)' }}
              >
                {t(`legacy.typeOptions.${item?.type}`)}
              </span>
            </div>
            <div tw="flex flex-row justify-between items-center">
              <div tw="flex flex-row items-center">
                <Icon icon="akar-icons:location" tw="w-3 h-3 mb-1" />
                {t('condition')}
              </div>
              <span
                tw="p-2 rounded-md text-[11px] first-letter:capitalize text-[#868686] font-semibold"
                style={{ background: 'rgb(233 233 235)' }}
              >
                {item?.condition?.replaceAll('_', ' ')}
              </span>
            </div>
          </div>
        )}
      </div>

      {type === 'feed' && showAuthor()}

      <div
        tw="absolute top-2"
        css={
          type === 'jogldoc' || type === 'link' || type === 'document' || type === 'resource' ? tw`left-2` : tw`right-2`
        }
      >
        {showType()}
      </div>

      {showActionsMenu()}
    </div>
  ) : (
    <tr key={document.title}>
      <td tw="w-full py-4 pl-4 pr-3 text-sm font-medium text-gray-900 max-w-0 sm:w-auto sm:max-w-none sm:pl-0">
        <div tw="inline-flex gap-2 items-center">
          <div tw="min-w-8 text-center">
            {type === 'link' || type === 'document' ? (
              <div>
                {type === 'document' && mimetype2fa(documentData.file_type, true)}
                {type === 'link' &&
                  (knownUrl ? (
                    <img src={knownUrl?.image} tw="size-6 text-[rgb(125, 125, 125)]" alt={knownUrl?.name} />
                  ) : (
                    <Icon icon="bx:link" tw="size-6 mr-0! text-[rgb(125, 125, 125)]" />
                  ))}
              </div>
            ) : (
              <img
                src={src ?? '/images/default/default-jogldoc.png'}
                tw="size-7 object-cover rounded"
                onClick={(e) => e.preventDefault()}
              />
            )}
          </div>
          {(type === 'document' || type === 'resource') && id ? (
            <p
              tw="m-0 break-words text-decoration-thickness[2px] line-clamp-3"
              css={type !== 'resource' && tw`cursor-pointer hover:underline`}
              onClick={(e) => {
                e.preventDefault();
                onClickTitle && onClickTitle();
              }}
            >
              {title}
            </p>
          ) : (
            type !== 'document' &&
            id && (
              <Link href={type === 'link' ? id : `/doc/${id}`} passHref target={type === 'link' ? '_blank' : '_self'}>
                <p tw="text-black! m-0 cursor-pointer break-words line-clamp-3">
                  {type === 'link' ? (title?.length > 0 ? title : id) : title}
                </p>
              </Link>
            )
          )}
        </div>
        <dl tw="font-normal lg:hidden">
          <dt tw="sr-only md:hidden">Type</dt>
          <dd tw="mt-2 -ml-1 text-gray-700 md:hidden">{showType()}</dd>
          <dt tw="sr-only">Author</dt>
          <dd tw="mt-2 text-gray-500">{showAuthor()}</dd>
        </dl>
      </td>
      <td tw="hidden px-3 py-4 text-sm text-gray-500 md:table-cell min-w-[115px]">{showType()}</td>
      <td tw="hidden px-3 py-4 text-sm text-gray-500 lg:table-cell">{showAuthor()}</td>
      <td tw="px-3 py-4 text-sm text-gray-500">{displayObjectDate(document?.created, 'L', true)}</td>
      <td tw="py-4 pl-3 pr-4 text-sm font-medium text-right sm:pr-0">{showActionsMenu()}</td>
    </tr>
  );
};
