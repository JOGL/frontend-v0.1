import React, { useState, FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import SpinLoader from '../SpinLoader';
import { useRouter } from 'next/router';
import DocumentTypeSelector from './documentTypeSelector/documentTypeSelector';
import { DocType } from '~/src/types/docType';
import AddUpdateDocument from './addUpdateDocument/addUpdateDocument';
import { useMutation } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { Button, Flex, message } from 'antd';
import { DocumentType, Permission } from '~/__generated__/types';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import ContainerPicker from '../../Common/container/containerPicker/containerPicker';
import { Container } from '../../Common/container/containerPicker/containerPicker.types';
import { Disabled } from '../../Common/disabled/disabled';

interface PropsModal {
  defaultValue?: any;
  closeModal: () => void;
  refetch?: () => void;
}

export const CreateDocument: FC<PropsModal> = ({ defaultValue, closeModal, refetch }) => {
  const { t } = useTranslation('common');
  const [docForm, setDocForm] = useState<JSX.Element>();
  const [docType, setDocType] = useState<DocType>();
  const [sending] = useState(false);
  const { asPath } = useRouter();
  const router = useRouter();
  const [selectedContainer, setSelectedContainer] = useState<Container | undefined>(defaultValue);
  const { selectedHub } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
  }));
  
  const docData = {
    title: t('untitledPage'),
    type: DocumentType.Jogldoc,
  };

  const mutation = useMutation({
    mutationFn: async () => {
      if(!selectedContainer)
        return;

      const response = await api.documents.documentsCreate(selectedContainer?.id, docData);
      closeModal();
      return response.data;
    },
    onSuccess: (id) => {
      router.push(`/doc/${id}`);
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const handleRefresh = () => {
    //Gets the searchbox button if it exists and clicks it. Can't think of another way to do this from this component
    if (asPath.includes('tab=documents')) {
      const searchBox = document.getElementsByClassName('ais-SearchBox-submit')?.item(0) as HTMLButtonElement;
      searchBox && searchBox.click();
    }
    refetch && refetch();
  };

  const onSubmit = (e) => {
    if (docType === DocType.jogl_doc) {
      mutation.mutate();
    } else {
      selectedContainer &&
        setDocForm(
          <AddUpdateDocument
            feedId={selectedContainer.id}
            choice={docType}
            closeModal={closeModal}
            refresh={handleRefresh}
            goBack={() => setDocForm(undefined)}
          />
        );
    }
  };


  return docForm ? (
    docForm
  ) : (
    <Flex vertical gap="large">
      <ContainerPicker
        permission={Permission.Managedocuments}
        hubId={selectedHub?.id}
        onChange={(container) => setSelectedContainer(container)}
        value={selectedContainer?.id}
      />

      <Disabled disabled={!selectedContainer}>
        <DocumentTypeSelector
          asSelectorOnly={true}
          docType={docType}
          handleSelect={(type: DocType) => setDocType(type)}
        />
      </Disabled>
      <Flex justify="center" align="center">
        <Button type="primary" disabled={sending || !docType || !selectedContainer} onClick={onSubmit}>
          {(sending || mutation.isPending) && <SpinLoader />}
          {t('action.create')}
        </Button>
      </Flex>
    </Flex>
  );
};
