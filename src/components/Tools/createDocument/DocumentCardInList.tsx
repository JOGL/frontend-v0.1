import { FC, useEffect, useState } from 'react';
import { useApi } from '~/src/contexts/apiContext';
import { DocumentCard } from '~/src/components/Tools/createDocument/DocumentCard';
import { ItemType } from '~/src/types';
import DocViewer, { DocViewerRenderers } from '@cyntler/react-doc-viewer';
import { useModal } from '~/src/contexts/modalContext';
import { confAlert, entityItem } from '~/src/utils/utils';
import { Document } from '~/src/types/document';
import { DocumentModel } from '~/__generated__/types';

interface Props {
  document: Document | DocumentModel;
  isAdmin: boolean;
  itemId: string;
  itemType: ItemType;
  onClickEdit?: () => void;
  type?: 'jogldoc' | 'link' | 'document';
  refresh: () => void;
  displayType?: 'grid' | 'list';
  folderId?: string;
  showMove?: boolean;
  onClickMove?: () => void;
}

const DocumentCardInList: FC<Props> = ({
  document,
  itemId,
  itemType,
  isAdmin,
  onClickEdit,
  type,
  refresh,
  displayType = 'grid',
  folderId = null,
  showMove = false,
  onClickMove = () => {},
}) => {
  const api = useApi();
  const { showModal, closeModal } = useModal();
  const [documentData, setDocumentData] = useState<any>({});

  const deleteDoc = () => {
    const route =
      itemType === 'needs'
        ? `feed/contentEntities/${itemId}/documents/${document?.id}`
        : itemType === 'documents' || itemType === 'jogldoc'
        ? `documents/${document?.id}`
        : `/${entityItem[document?.feed_entity?.entity_type].back_path}/${document?.feed_entity?.id}/documents/${
            document?.id
          }`;

    api
      .delete(route)
      .then((res) => {
        if (res.status === 200) {
          refresh();
          confAlert.fire({
            icon: 'success',
            title: `Deleted ${document?.type === 'jogldoc' ? 'JOGL Doc' : document?.type}`,
          });
        }
      })
      .catch((err) => {
        console.error(`Couldn't DELETE ${itemType} with itemId=${itemId}`, err);
      });
  };

  useEffect(() => {
    type === 'document' && fetchAdditionalDocumentData();
  }, []);

  const fetchAdditionalDocumentData = async (i = 0) => {
    try {
      if (i > 2) {
        closeModal();
        return;
      }
      const fetchedDocument = await api.get(
        itemType === 'needs'
          ? `/feed/contentEntities/${itemId}/documents/${document.id}`
          : itemType === 'documents' || itemType === 'jogldoc'
          ? `/documents/${document?.id}`
          : `/${entityItem[document?.feed_entity?.entity_type].back_path}/${document?.feed_entity?.id}/documents/${
              document.id
            }`
      );
      setDocumentData({
        ...fetchedDocument.data,
        uri: fetchedDocument.data.data,
        fileName: fetchedDocument.data.title,
      });
    } catch (_) {
      fetchAdditionalDocumentData(i + 1);
    }
  };

  return (
    <DocumentCard
      type={type}
      id={type === 'link' ? document?.url : document?.id}
      title={document?.title}
      content={document?.description}
      showMenu={isAdmin}
      src={document?.image_url}
      document={document}
      showEdit={isAdmin}
      deleteCard={deleteDoc}
      itemType={itemType}
      onClickEdit={type !== 'jogldoc' ? () => onClickEdit && onClickEdit() : null}
      onClickTitle={
        type === 'document'
          ? () => {
              showModal({
                children: <DocumentViewer document={documentData} />,
                title: 'Document',
                maxWidth: '70rem',
              });
            }
          : null
      }
      user={{
        ...document?.created_by,
        logo_url: document?.created_by?.logo_url_sm ?? '/images/default/default-user.png',
      }}
      documentData={documentData}
      entity={document?.feed_entity}
      displayType={displayType}
      folderId={folderId}
      showMove={showMove}
      onClickMove={onClickMove}
    />
  );
};

interface DocumentViewerProps {
  document: any;
}

export const DocumentViewer: FC<DocumentViewerProps> = ({ document }) => {
  return (
    <div tw="flex flex-col justify-center items-center">
      <p tw="text-lg font-bold m-0 mb-2 word-break[break-all] text-decoration-thickness[2px]">{document.title}</p>
      <p tw="m-0 mb-4 [word-break:break-all]">{document.description}</p>
      <DocViewer
        documents={[document]}
        tw=""
        config={{
          header: {
            disableHeader: true,
            disableFileName: true,
            retainURLParams: true,
          },
        }}
        pluginRenderers={DocViewerRenderers}
      />
    </div>
  );
};

export default DocumentCardInList;
