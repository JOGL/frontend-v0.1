import styled from '@emotion/styled';
import { Button, Flex } from 'antd';

export const OptionButtonStyled = styled(Button)<{ isSelected?: boolean }>`
  display: flex;
  flex-direction: column;
  gap: 8px;
  align-items: center;
  width: 100%;
  height: auto;
  padding: 16px;

  &:hover {
    background-color: ${({ theme }) => theme.token.neutral2};
  }

  ${({ isSelected }) =>
    isSelected &&
    `
    border-color: #643CC6;
    color:  #643CC6;
    background-color: #643CC61A;
  `}
`;

export const OptionsContainerStyled = styled(Flex)`
  justify-content: space-evenly;
  gap: 20px;

  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    flex-wrap: wrap;
  }
`;
