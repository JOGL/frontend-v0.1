import useTranslation from 'next-translate/useTranslation';
import { useEffect, useState } from 'react';
import Icon from '~/src/components/primitives/Icon';
import { Hub, ItemType, Need } from '~/src/types';
import { useRouter } from 'next/router';
import { DocType } from '~/src/types/docType';
import { Event } from '~/src/types/event';
import { Document } from '~/src/types/document';
import { DocumentType } from '~/__generated__/types';
import api from '~/src/utils/api/api';
import { useMutation } from '@tanstack/react-query';
import { message } from 'antd';
import { OptionButtonStyled, OptionsContainerStyled } from './documentTypeSelector.styles';
import AddUpdateDocument from '../addUpdateDocument/addUpdateDocument';

interface Props {
  asSelectorOnly?: boolean;
  docType?: DocType;
  object?: Hub | Need | Document | Event;
  itemType?: ItemType;
  apiRoute?: string;
  handleSelect?: (type: DocType) => void;
  refresh?: () => void;
  closeModal?: () => void;
}

export default function DocumentTypeSelector({
  asSelectorOnly = false,
  docType,
  object,
  itemType,
  apiRoute,
  handleSelect,
  refresh,
  closeModal,
}: Props) {
  const [content, setContent] = useState<JSX.Element>();
  const { t } = useTranslation('common');
  const router = useRouter();

  const renderOption = (title: string, icon: string, type: DocType) => {
    return (
      <OptionButtonStyled
        type="default"
        isSelected={asSelectorOnly && docType === type}
        onClick={() => handleChoice(type)}
      >
        <Icon icon={icon} />
        {title}
      </OptionButtonStyled>
    );
  };

  const docData = {
    title: t('untitledPage'),
    type: DocumentType.Jogldoc,
  };

  const mutation = useMutation({
    mutationFn: async () => {
      const response = await api.documents.documentsCreate(object?.id ?? '', docData);
      return response.data;
    },
    onSuccess: (id) => {
      router.push(`/doc/${id}`);
    },
    onError: () => {
      message.error(t('somethingWentWrong'));
    },
  });

  const setDefaultContent = () => {
    setContent(
      <OptionsContainerStyled>
        {renderOption(t('viaLink'), 'bx:link', 1)}
        {renderOption(t('action.upload'), 'material-symbols:upload', 2)}
        {itemType !== 'needs' &&
          itemType !== 'documents' &&
          itemType !== 'jogldoc' &&
          renderOption(t('page'), 'bx:edit', 3)}
      </OptionsContainerStyled>
    );
  };
  const handleChoice = (choice: DocType) => {
    if (asSelectorOnly) {
      handleSelect && handleSelect(choice);
    } else {
      if (choice === DocType.jogl_doc) {
        mutation.mutate();
      } else {
        itemType &&
          apiRoute &&
          closeModal &&
          refresh &&
          setContent(
            <AddUpdateDocument
              itemType={itemType}
              apiRoute={apiRoute}
              choice={choice}
              closeModal={closeModal}
              refresh={refresh}
              goBack={setDefaultContent}
            />
          );
      }
    }
  };

  useEffect(() => {
    setDefaultContent();
  }, [docType]);

  return content;
}
