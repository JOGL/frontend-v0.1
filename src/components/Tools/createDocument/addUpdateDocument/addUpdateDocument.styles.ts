// addUpdateDocument.styles.ts
import styled from '@emotion/styled';
import { Card, Flex, Typography } from 'antd';

export const MainFlexStyled = styled(Flex)`
  display: flex;
`;

export const UploadCardStyled = styled(Card)`
  width: 100%;
`;

export const UploadWrapperStyled = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
`;
export const IconWrapperStyled = styled.div`
  width: 20px;
  height: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ErrorMessageStyled = styled(Typography.Text)`
  margin-top: 15px;
  display: block;
`;

export const FlexContainerStyled = styled(Flex)`
  width: 100%;
`;

export const ButtonFlexStyled = styled(Flex)`
  display: flex;
`;
