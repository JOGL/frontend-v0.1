import { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from '~/src/contexts/apiContext';
import { DocType } from '~/src/types/docType';
import { isValidHttpUrl, convertBase64 } from '~/src/utils/utils';
import { Button, Upload, UploadFile, UploadProps, Input, Flex, message, Typography } from 'antd';
import { LinkOutlined, UploadOutlined } from '@ant-design/icons';
import { FlexFullWidthStyled } from '../../../Common/common.styles';
import {
  UploadCardStyled,
  ErrorMessageStyled,
  MainFlexStyled,
  ButtonFlexStyled,
  UploadWrapperStyled,
  FlexContainerStyled,
} from './addUpdateDocument.styles';

const { TextArea } = Input;
const MAX_FILES_COUNT = 10;
const MAX_DESCRIPTION_LENGTH = 340;

interface AddUpdateDocumentProps {
  feedId: string;
  document?: any;
  choice?: DocType;
  selectedFolderId?: string;
  refresh: () => void;
  closeModal: () => void;
  goBack?: () => void;
}

const AddUpdateDocument: FC<AddUpdateDocumentProps> = ({
  feedId,
  choice,
  document,
  selectedFolderId = null,
  refresh,
  closeModal,
  goBack,
}) => {
  const [link, setLink] = useState(document?.url ?? '');
  const { t } = useTranslation('common');
  const api = useApi();
  const [fileList, setFileList] = useState<UploadFile[]>([]);
  const [loading, setLoading] = useState(false);
  const [title, setTitle] = useState(document?.title ?? '');
  const [description, setDescription] = useState(document?.description ?? '');
  const [fileListError, setFileListError] = useState('');
  const [titleError, setTitleError] = useState('');

  const itemTranslation = choice === DocType.link ? t('link.one') : t('document.one');
  const btnTxt = document?.id
    ? t('action.updateItem', { item: itemTranslation })
    : t('action.addItem', { item: itemTranslation });

  const uploadProps: UploadProps = {
    fileList,
    maxCount: MAX_FILES_COUNT,
    accept: 'image/*,.pdf,.xls,.xlsx,.doc,.docx',
    multiple: true,
    onRemove: (file) => {
      const index = fileList.indexOf(file);
      const newFileList = fileList.slice();
      newFileList.splice(index, 1);
      setFileList(newFileList);
    },
    beforeUpload: (_, newList) => {
      const updatedList = [...fileList, ...newList].slice(0, MAX_FILES_COUNT);
      setFileList(updatedList);
      setFileListError('');
      return false;
    },
  };

  const validateForm = () => {
    let isValid = true;
    if (choice === DocType.link) {
      if (!title?.trim()) {
        setTitleError(t('error.requiredField'));
        isValid = false;
      } else {
        setTitleError('');
      }
    }

    if (choice === DocType.upload && fileList.length === 0) {
      setFileListError(t('error.requiredField'));
      isValid = false;
    } else {
      setFileListError('');
    }
    return isValid;
  };

  const handleUpload = async () => {
    if (!validateForm()) return;

    try {
      setLoading(true);
      if (choice === DocType.upload && fileList.length > 0) {
        await Promise.all(
          fileList.map(async (file) => {
            const base64 = await convertBase64(file);

            await api.post(`documents/${feedId}/documents`, {
              title: file.name,
              file_name: file.name,
              type: 'document',
              folder_id: selectedFolderId,
              data: base64 as string,
            });
          })
        );
        message.success(t('successfullyAdded'));
        refresh();
        closeModal();
      } else {
        let docJSON: any = {
          title: title,
          description: description,
          folder_id: selectedFolderId,
        };
        if (document?.id) {
          if (document?.type !== 'link') {
            const data = (await api.get(`documents/${document.id}`)).data;
            docJSON = { ...data, ...docJSON };
          } else {
            docJSON = { ...document, ...docJSON, url: link };
          }
          await api.put(`documents/${document.id}`, docJSON);
        } else {
          docJSON = { ...docJSON, url: link, type: 'link' };
          await api.post(`documents/${feedId}/documents`, docJSON);
        }
        message.success(t('successfullyAdded'));
        refresh();
        closeModal();
      }
    } catch (e) {
      message.error(t('someUnknownErrorOccurred'));
    } finally {
      setLoading(false);
    }
  };

  return (
    <MainFlexStyled vertical align="center">
      <FlexFullWidthStyled vertical gap="middle" align="center">
        {choice === DocType.upload && !document?.id && (
          <UploadCardStyled>
            <UploadWrapperStyled>
              <Flex vertical>
                <Upload {...uploadProps}>
                  <Button icon={<UploadOutlined />} disabled={fileList.length >= MAX_FILES_COUNT}>
                    {t('clickToUpload', { maxCount: MAX_FILES_COUNT })}
                  </Button>
                </Upload>
                {fileListError && <ErrorMessageStyled type="danger">{fileListError}</ErrorMessageStyled>}
              </Flex>
            </UploadWrapperStyled>
          </UploadCardStyled>
        )}
        {choice === DocType.link && (
          <>
            <Input
              value={link}
              onChange={(e) => {
                const v = e.target.value;
                setLink(v);
                if (!title) {
                  setTitle(v);
                }
              }}
              placeholder="https://website.com"
              prefix={<LinkOutlined />}
            />
            {link.length > 0 && !isValidHttpUrl(link) && (
              <ErrorMessageStyled type="danger">{t('error.invalidLink')}</ErrorMessageStyled>
            )}

            <FlexContainerStyled vertical>
              <Typography.Text strong>{t('title')}*</Typography.Text>
              <Input
                value={title}
                onChange={(e) => {
                  setTitle(e.target.value);
                  if (titleError) setTitleError('');
                }}
                status={titleError ? 'error' : ''}
                placeholder={t('title')}
              />
              {titleError && <ErrorMessageStyled type="danger">{titleError}</ErrorMessageStyled>}
            </FlexContainerStyled>

            <FlexContainerStyled vertical>
              <Typography.Text strong>{t('shortDescription')}</Typography.Text>
              <TextArea
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                placeholder={t('explainToMembersWhyThisDocumentMatters')}
                rows={3}
                maxLength={MAX_DESCRIPTION_LENGTH}
                showCount
              />
            </FlexContainerStyled>
          </>
        )}
        <ButtonFlexStyled gap="small">
          <Button type="primary" onClick={handleUpload} loading={loading}>
            {btnTxt}
          </Button>
          {!document?.id && <Button onClick={() => goBack && goBack()}>{t('action.back')}</Button>}
        </ButtonFlexStyled>
      </FlexFullWidthStyled>
    </MainFlexStyled>
  );
};

export default AddUpdateDocument;
