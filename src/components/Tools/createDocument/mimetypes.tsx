import tw from 'twin.macro';
import Icon from '~/src/components/primitives/Icon';
const mapping = [
  // Images
  ['fa-regular:file-image', /^image\//],
  // Audio
  ['fa-regular:file-audio', /^audio\//],
  // Video
  ['fa-regular:file-video', /^video\//],
  // Documents
  ['fa-regular:file-pdf', 'application/pdf'],
  ['fa-regular:file-alt', 'text/plain'],
  ['fa-regular:file-code', ['text/html', 'text/javascript']],
  // Archives
  [
    'fa-regular:file-archive',
    [
      /^application\/x-(g?tar|xz|compress|bzip2|g?zip)$/,
      /^application\/x-(7z|rar|zip)-compressed$/,
      /^application\/(zip|gzip|tar)$/,
    ],
  ],
  // Word
  [
    'fa-regular:file-word',
    [
      /ms-?word/,
      'application/vnd.oasis.opendocument.text',
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    ],
  ],
  // Powerpoint
  [
    'fa-regular:file-powerpoint',
    [/ms-?powerpoint/, 'application/vnd.openxmlformats-officedocument.presentationml.presentation'],
  ],
  // Excel
  ['fa-regular:file-excel', [/ms-?excel/, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']],
  // Default, misc
  ['fa-regular:file'],
];

function match(mimetype, cond) {
  if (Array.isArray(cond)) {
    return cond.reduce((v, c) => v || match(mimetype, c), false);
  }
  if (cond instanceof RegExp) {
    return cond.test(mimetype);
  }
  if (cond === undefined) {
    return true;
  }
  return mimetype === cond;
}

const cache = {};

function resolve(mimetype) {
  if (cache[mimetype]) {
    return cache[mimetype];
  }

  for (let i = 0; i < mapping.length; i++) {
    if (match(mimetype, mapping[i][1])) {
      cache[mimetype] = mapping[i][0];
      return mapping[i][0];
    }
  }
}

function mimetype2fa(mimetype: any, isSmall = false, noMargin = false) {
  const icon = resolve(mimetype);
  return (
    <div>
      <Icon icon={icon} css={[isSmall && tw`w-[18px] h-[18px]`, noMargin && tw`m-0!`]} />
    </div>
  );
}

export default mimetype2fa;
