import Select from 'react-select';
import { useState } from 'react';
import { useApi } from '~/src/contexts/apiContext';
import useTranslation from 'next-translate/useTranslation';
import SpinLoader from './SpinLoader';

const DropdownRole = ({
  actualRole = 'member',
  callBack = (key?: any) => {
    console.warn('Missing callback function');
  },
  onRoleChanged = () => {
    console.warn('Missing function');
  },
  itemId = undefined,
  itemType = undefined,
  listRole = [],
  member = undefined,
  isDisabled = false,
  setNewRole,
  showPlaceholder = false,
  submitChanges = true,
}) => {
  const [sending, setSending] = useState(false);
  const api = useApi();
  const { t } = useTranslation('common');

  const handleChange = (key) => {
    if ((itemId || itemType || member.id) && !Array.isArray(member)) {
      setSending(true);
      api
        .put(`/${itemType}/${itemId}/members/${member.id}`, `"${key.value.toLowerCase()}"`)
        .then(() => {
          setSending(false);
          callBack();
          onRoleChanged();
        })
        .catch((err) => {
          console.error(`Couldn't POST ${itemType} with itemId=${itemId} members`, err);
          setSending(false);
        });
    } else if (Array.isArray(member)) {
      setNewRole(key);
    }
  };

  const handleEventChange = (key) => {
    if (member?.id && !Array.isArray(member) && submitChanges) {
      setSending(true);
      api
        .post(
          `/events/attendances/${member?.id}/accessLevel`,
          `"${key.value.toLowerCase() === 'organizer' ? 'admin' : key.value.toLowerCase()}"`
        )
        .then(() => {
          setSending(false);
          callBack(key?.value);
        })
        .catch((err) => {
          console.error(`Couldn't POST events access level with invitation id ${member?.id}`, err);
          setSending(false);
        });
    } else if (Array.isArray(member)) {
      setNewRole(key);
    }

    if (!submitChanges) {
      callBack(key?.value);
    }
  };

  const customStyles = {
    control: (provided) => ({
      ...provided,
      borderRadius: '100px',
    }),
  };

  return (
    <>
      <Select
        options={listRole.map((role) => ({
          value: role,
          label: t(`legacy.role.${role}`),
        }))}
        isSearchable={false}
        isDisabled={isDisabled || sending}
        menuShouldScrollIntoView={true} // force scroll into view
        {...(!showPlaceholder && {
          defaultValue: {
            value: actualRole,
            label: t(`legacy.role.${actualRole}`),
          },
        })}
        styles={customStyles}
        onChange={(key) => {
          itemType === 'event' ? handleEventChange(key) : handleChange(key);
        }}
        placeholder={t('action.changeRole')}
      />
      {sending && <SpinLoader />}
    </>
  );
};
export default DropdownRole;
