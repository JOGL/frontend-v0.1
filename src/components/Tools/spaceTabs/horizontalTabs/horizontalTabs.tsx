import { FC } from 'react';
import Icon from '../../../primitives/Icon';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import { Button, Col, Flex, Grid, Popover, Typography } from 'antd';
import { FolderOutlined } from '@ant-design/icons';
import { VerticalTabs } from '../verticalTabs/verticalTabs';
import { ButtonStyled, FlexStyled, LinkStyled, RowStyled } from './horizontalTabs.styles';
import { SpaceTab, SpaceTabsProps } from '../spaceTabs';

interface HorizontalTabsProps extends SpaceTabsProps {
  verticalTabs: SpaceTab[];
  verticalTabSelected?: boolean;
}

export const HorizontalTabs: FC<HorizontalTabsProps> = ({
  containerType,
  tabs,
  defaultTab,
  selectedTab,
  verticalTabs,
  verticalTabSelected = false,
}) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const { useBreakpoint } = Grid;
  const screens = useBreakpoint();

  return (
    <RowStyled>
      {screens.md && !!verticalTabs.length && (
        <Col span={4}>
          <LinkStyled
            shallow
            selected={verticalTabSelected}
            href={`/${containerType}/${router.query.id}?tab=${verticalTabs[0].value}`}
          >
            <Flex gap="small" align="center">
              <FolderOutlined />
              <Typography.Text>{t('documentation')}</Typography.Text>
            </Flex>
          </LinkStyled>
        </Col>
      )}

      <Col span={screens.md ? 20 : 24}>
        <FlexStyled>
          {!screens.md && !!verticalTabs.length && (
            <Popover
              placement="bottomLeft"
              content={
                <VerticalTabs
                  containerType={containerType}
                  tabs={verticalTabs}
                  selectedTab={selectedTab}
                  defaultTab={defaultTab}
                />
              }
            >
              <ButtonStyled type="text" icon={<FolderOutlined />}>
                <Typography.Text>{t('documentation')}</Typography.Text>
              </ButtonStyled>
            </Popover>
          )}
          {tabs.map((tab) => (
            <LinkStyled
              shallow
              key={tab.value}
              selected={selectedTab === tab.value}
              href={`/${containerType}/${router.query.id}${tab.value === defaultTab ? '' : `?tab=${tab.value}`}`}
            >
              <Flex gap="small" align="center">
                <Icon icon={tab.icon} />
                <Typography.Text> {tab.value === 'members' ? tab.extraLabel : t(tab.translationId)}</Typography.Text>
              </Flex>
            </LinkStyled>
          ))}
        </FlexStyled>
      </Col>
    </RowStyled>
  );
};
