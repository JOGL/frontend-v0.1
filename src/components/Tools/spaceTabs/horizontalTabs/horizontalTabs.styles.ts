import styled from '@emotion/styled';
import { Button, Flex, Row } from 'antd';
import Link from 'next/link';

export const RowStyled = styled(Row)`
  border-bottom: 1px solid ${({ theme }) => theme.token.colorBorder};
  padding: ${({ theme }) => `${theme.space[3]} ${theme.space[3]} ${theme.space[1]}`};
`;

export const FlexStyled = styled(Flex)`
  gap: ${({ theme }) => theme.space[8]};
  justify-content: flex-end;

  @media (max-width: ${({ theme }) => theme.token.screenMDMax + 'px'}) {
    justify-content: start;
    overflow: auto;
  }
`;

export const ButtonStyled = styled(Button)`
  padding: ${({ theme }) => theme.space[5]} 0;
  svg {
    font-size: ${({ theme }) => theme.fontSizes['3xl']};
  }
`;

export const LinkStyled = styled(Link)<{ selected: boolean }>`
  cursor: pointer;
  display: inline-block;
  padding: ${({ theme }) => theme.space[2]} 0;
  border-bottom: 2px solid;
  border-color: ${(props) => (props.selected ? props.theme.token.neutral10 : props.theme.token.neutral1)};

  span {
    color: ${({ theme }) => theme.token.neutral13};
    white-space: nowrap;
  }

  svg {
    font-size: ${({ theme }) => theme.fontSizes['3xl']};
    color: ${({ theme }) => theme.token.neutral8};
  }

  &:hover {
    svg {
      color: ${({ theme }) => theme.token.neutral7};
    }
    span {
      color: ${({ theme }) => theme.token.neutral7};
    }
  }
`;
