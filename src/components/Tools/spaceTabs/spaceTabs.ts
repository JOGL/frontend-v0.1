import { ReactNode } from 'react';

export interface SpaceTab {
  value: string;
  translationId: string;
  //string should be removed after vertical tabs icon change
  icon: string | ReactNode;
  extraLabel?: string;
}

export interface SpaceTabsProps {
  containerType: 'hub' | 'workspace';
  tabs: SpaceTab[];
  defaultTab: string;
  selectedTab: string;
}
