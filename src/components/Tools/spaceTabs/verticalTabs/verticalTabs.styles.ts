import styled from '@emotion/styled';
import { Flex } from 'antd';

export const VerticalTabContainerStyled = styled(Flex)`
  width: 48px;
  align-items: center;
  flex-direction: column;
  padding: ${({ theme }) => theme.space[1]};
  background-color: ${(props) => props.theme.token.neutral2};
  border-right: 1px solid ${(props) => props.theme.token.colorBorder};

  span {
    white-space: nowrap;
  }

  svg {
    font-size: ${({ theme }) => theme.fontSizes['3xl']};
    color: ${({ theme }) => theme.token.neutral8};
    &:hover {
      color: ${({ theme }) => theme.token.neutral7};
    }
  }

  @media (max-width: ${({ theme }) => theme.token.screenMDMax + 'px'}) {
    width: 100px;
    align-items: flex-start;
    border: none;
    background-color: ${(props) => props.theme.token.neutral1};
    svg {
      font-size: ${({ theme }) => theme.fontSizes['sm']};
    }
  }
`;

export const FlexStyled = styled(Flex)<{ selected: boolean }>`
  width: 40px;
  height: 40px;
  margin: ${({ theme }) => theme.space[1]};
  align-items: center;
  justify-content: center;
  border: ${({ selected }) => (selected ? '2px solid' : 'none')};
  border-color: ${(props) => props.theme.token.colorPrimary};
  border-radius: ${(props) => props.theme.token.borderRadiusSM}px;
`;
