import { FC } from 'react';
import { useRouter } from 'next/router';
import { Flex, Grid, Tooltip, Typography } from 'antd';
import Link from 'next/link';
import useTranslation from 'next-translate/useTranslation';
import { SpaceTabsProps } from '../spaceTabs';
import { FlexStyled, VerticalTabContainerStyled } from './verticalTabs.styles';

export const VerticalTabs: FC<SpaceTabsProps> = ({ containerType, tabs, selectedTab }) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const { useBreakpoint } = Grid;
  const screens = useBreakpoint();

  return (
    <VerticalTabContainerStyled>
      {tabs.map((tab) => (
        <Link shallow key={tab.value} href={`/${containerType}/${router.query.id}?tab=${tab.value}`}>
          {screens.md ? (
            <FlexStyled selected={selectedTab === tab.value}>
              <Tooltip placement="rightTop" title={t(tab.translationId)}>
                {tab.icon}
              </Tooltip>
            </FlexStyled>
          ) : (
            <Flex gap="small">
              {tab.icon}
              <Typography.Text>{t(tab.translationId)}</Typography.Text>
            </Flex>
          )}
        </Link>
      ))}
    </VerticalTabContainerStyled>
  );
};
