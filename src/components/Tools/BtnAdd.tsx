import { FC } from 'react';
import { Menu, MenuButton, MenuItem, MenuList } from '@reach/menu-button';
import { ActionRow } from '~/src/types/containerAddMenu';
import { getContainerInfo } from '~/src/utils/getContainerInfo';
import tw from 'twin.macro';
import Icon from '../primitives/Icon';
import useTranslation from 'next-translate/useTranslation';

interface BtnProps {
  container: unknown;
  type: string;
  refresh: () => void;
}

const BtnAdd: FC<BtnProps> = ({ container, type, refresh }) => {
  const { t } = useTranslation('common');
  const actions = getContainerInfo(container, type, refresh);

  const renderAction = (title: string, icon: string, privacy: string, disabled: boolean, handleSelect: () => void) => {
    return (
      <MenuItem
        key={title}
        tw="cursor-pointer flex my-2 justify-between items-center rounded bg-[#F9FCFD] border border-gray-300 border-solid hover:(bg-[#eaedf0] no-underline text-primary)"
        onSelect={handleSelect}
        disabled={disabled}
      >
        <div tw="flex flex-row gap-4 h-full items-center">
          <Icon icon={icon} tw="w-5 h-5" />
          <div tw="border-r border-solid border-[rgb(224, 224, 224)] h-[1px] w-[1px] py-6 hidden sm:block" />
          <div tw="flex flex-col justify-center" css={[type === 'user' ? tw`self-center` : '']}>
            <h5 tw="text-[15px] font-semibold" css={[type === 'user' ? tw`m-0` : '']}>
              {t(title)}
            </h5>
            {type !== 'user' && (
              <span tw="text-gray-500 text-[12px] flex items-center">
                <Icon icon="iconamoon:lock" tw="w-3 h-3" />
                {t(privacy)}
              </span>
            )}
          </div>
        </div>
      </MenuItem>
    );
  };

  return (
    <Menu>
      <MenuButton tw="text-[.8rem] h-fit text-gray-700 rounded-sm mx-0">
        <Icon
          icon="zondicons:add-solid"
          tw="bg-[#CFC5EB] text-primary hocus:text-[#4a3292] rounded-full w-9 h-9 cursor-pointer mr-0"
        />
      </MenuButton>
      <MenuList className="show-scrollbar" tw="p-3 text-primary overflow-auto max-h-[70vh]" hidden={false}>
        {actions?.members && (
          <>
            <div tw="flex flex-row items-center">
              <span tw="text-[rgb(118, 118, 118)] text-[13px]">{t('invite.one')}</span>
              <span tw="ml-[3px] mt-[3px] flex-1 w-full border[0.5px solid rgb(224, 224, 224)] h-0 rounded-lg" />
            </div>
            {renderAction(
              actions?.members.title,
              actions?.members.icon,
              actions?.members.privacy,
              actions?.members.disabled,
              actions?.members.handleSelect
            )}
          </>
        )}
        {actions?.content && (
          <>
            <div tw="flex flex-row items-center">
              <span tw="text-[rgb(118, 118, 118)] text-[13px]">{t('action.addContent')}</span>
              <span tw="ml-[3px] mt-[3px] flex-1 w-full border[0.5px solid rgb(224, 224, 224)] h-0 rounded-lg" />
            </div>

            {actions?.content.map((action: ActionRow) => {
              return renderAction(action?.title, action?.icon, action?.privacy, action?.disabled, action?.handleSelect);
            })}
          </>
        )}
        {actions?.containers && (
          <>
            <div tw="flex flex-row items-center">
              <span tw="text-[rgb(118, 118, 118)] text-[13px]">{t('linkToContainer')}</span>
              <span tw="ml-[3px] mt-[3px] flex-1 w-full border[0.5px solid rgb(224, 224, 224)] h-0 rounded-lg" />
            </div>

            {actions?.containers.map((action: ActionRow) => {
              return renderAction(action?.title, action?.icon, action?.privacy, action?.disabled, action?.handleSelect);
            })}
          </>
        )}
      </MenuList>
    </Menu>
  );
};

export default BtnAdd;
