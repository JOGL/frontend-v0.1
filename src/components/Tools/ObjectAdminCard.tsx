import useTranslation from 'next-translate/useTranslation';
import { FC, useState } from 'react';
import Alert from '~/src/components/Tools/Alert/Alert';
import { useApi } from '~/src/contexts/apiContext';
import { Hub, Organization } from '~/src/types';
import A from '../primitives/A';
import Button from '../primitives/Button';
import SpinLoader from './SpinLoader';
import { entityItem } from '~/src/utils/utils';
import { useRouter } from 'next/router';
import { usePushNotificationStore } from '~/src/store/push-notifications/PushNotificationsStoreProvider';
import { WorkspaceModel } from '~/__generated__/types';

interface Props {
  object: Hub | Organization | WorkspaceModel;
  parentId?: string;
  parentType: 'nodes' | 'organizations';
  type?: string;
  callBack: () => void;
  objUrl?: string;
}

const ObjectAdminCard: FC<Props> = ({ object, parentType, parentId, callBack, type, objUrl }) => {
  const router = useRouter();
  const { showPushNotificationModal } = usePushNotificationStore((s) => ({
    showPushNotificationModal: s.showPushNotificationModal,
  }));
  const [sending, setSending] = useState('');
  const [error, setError] = useState('');
  const api = useApi();
  const { t } = useTranslation('common');

  const onSuccess = () => {
    setSending('');
    callBack();
    showPushNotificationModal(router);
  };

  const onError = (type, err) => {
    console.error(`Couldn't ${type} ${parentType} with parentId=${parentId}`, err);
    setSending('');
    setError(t('error.generic'));
  };

  const acceptObject = () => {
    setSending('accepted');
    api
      .post(`/${parentType}/${parentId}/communityEntityInvites/${object.invitation_id}/accept`)
      .then(() => onSuccess())
      .catch((err) => onError('accept', err));
  };

  const rejectObject = () => {
    setSending('remove');
    api
      .post(`/${parentType}/${parentId}/communityEntityInvites/${object.invitation_id}/reject`)
      .then(() => onSuccess())
      .catch((err) => onError('reject', err));
  };

  const removeObject = () => {
    setSending('remove');
    api
      .post(`/${parentType}/${parentId}/communityEntity/${object.id}/remove`)
      .then(() => onSuccess())
      .catch((err) => onError('reject', err));
  };

  const cancelInvite = () => {
    setSending('remove');
    api
      .post(`/${parentType}/${parentId}/communityEntityInvites/${object.invitation_id}/cancel`)
      .then(() => onSuccess())
      .catch((err) => onError('cancel', err));
  };

  if (object !== undefined) {
    return (
      <div>
        <div tw="flex justify-between" key={object.id}>
          <div tw="flex items-center pb-3 space-x-3 sm:pb-0">
            <div
              style={{
                backgroundImage: `url(${
                  object.logo_url || object.banner_url || entityItem[object.type].default_banner
                })`,
              }}
              tw="bg-cover bg-center h-[50px] w-[50px] rounded-full border border-solid border-[#ced4da]"
            />
            <div tw="flex flex-col">
              <div>
                <A href={objUrl}>{object.title}</A>
                {object.status === 'draft' && ` (${object.status})`}
              </div>
              {object.stats?.members_count > 0 && (
                <div tw="flex flex-col text-[93%] pt-1">
                  {t('membersCount', { count: object.stats?.members_count ?? 0 })}
                </div>
              )}
            </div>
          </div>
          <div tw="flex items-center ml-3">
            {type === 'request' ? ( // if object status is pending, display the accept/reject buttons
              <div tw="flex flex-col space-y-2 xs:(flex-row space-x-2 space-y-0)">
                <Button
                  tw="text-green-500 border-green-500 bg-white hocus:(text-white bg-green-500)"
                  disabled={sending === 'accepted'}
                  onClick={acceptObject}
                >
                  {sending === 'accepted' && <SpinLoader />}
                  {t('action.accept')}
                </Button>
                <Button
                  tw="text-danger border-danger bg-white hocus:(text-white bg-danger)"
                  disabled={sending === 'remove'}
                  onClick={rejectObject}
                >
                  {sending === 'remove' && <SpinLoader />}
                  {t('action.reject')}
                </Button>
              </div>
            ) : type === 'invite' ? (
              <Button
                tw="text-danger border-danger bg-white hocus:(text-white bg-danger)"
                disabled={sending === 'remove'}
                onClick={cancelInvite}
              >
                {sending === 'remove' && <SpinLoader />}
                Cancel {objUrl.includes('/hub') ? 'request' : 'invite'}
              </Button>
            ) : (
              // else display the remove button
              <Button btnType="danger" disabled={sending === 'remove'} onClick={removeObject}>
                {sending === 'remove' && <SpinLoader />}
                {t('action.remove').toLocaleLowerCase()}
              </Button>
            )}
            {error !== '' && <Alert type="danger" message={error} />}
          </div>
        </div>
        <hr />
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default ObjectAdminCard;
