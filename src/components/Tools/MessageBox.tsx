import { FC, ReactNode } from 'react';
import tw from 'twin.macro';

interface Props {
  children: string | ReactNode;
  type?: string;
}

const MessageBox: FC<Props> = ({ children, type }) => (
  <div
    tw="rounded-md p-4 mb-4 text-gray-700"
    role="alert"
    css={[
      type === 'success'
        ? tw`bg-green-100`
        : type === 'danger'
        ? tw`text-red-800 bg-red-100 border border-red-800`
        : type === 'warning'
        ? tw`bg-yellow-100`
        : tw`bg-blue-100`,
    ]}
  >
    {/* <div tw="text-gray-700">{children}</div> */}
    <div>{children}</div>
  </div>
);

export default MessageBox;
