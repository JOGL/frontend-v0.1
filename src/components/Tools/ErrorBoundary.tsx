/* eslint-disable @typescript-eslint/explicit-member-accessibility */
import React from 'react';
import Button from '../primitives/Button';
import styled from '@emotion/styled';

type ErrorBoundaryProps = {
  children: any[];
};

const Header = styled.header`
  display: inline-flex;
  box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px;
  -webkit-box-align: center;
  align-items: center;
  background: white;
  top: 0px;
  z-index: 20;
  height: 4rem;
  padding-left: 0.75rem;
  padding-right: 0.75rem;
`;

export default class ErrorBoundary extends React.Component<ErrorBoundaryProps, any> {
  constructor(props) {
    super(props);

    // Define a state variable to track whether is an error or not
    this.state = { hasError: false };
  }

  static getDerivedStateFromError() {
    // Update state so the next render will show the fallback UI

    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // You can use your own error logging service here
    console.warn({ error, errorInfo });
  }

  render() {
    // Check if the error is thrown
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return (
        <div tw="flex flex-col h-screen w-screen">
          <Header>
            <img tw="h-auto w-24" src="/images/logo.png" alt="JOGL icon" />
          </Header>
          <div tw="flex flex-col justify-center items-center h-full gap-y-14">
            <h1 tw="font-black text-3xl text-primary lg:text-5xl">Unexpected error</h1>
            <img tw="h-1/4 w-1/4" src="/images/unexpected_error.svg" alt="Unexpected error" />
            <Button btnType="secondary" onClick={() => this.setState({ hasError: false })}>
              Reload
            </Button>
          </div>
        </div>
      );
    }

    // Return children components in case of no error
    return this.props.children;
  }
}
