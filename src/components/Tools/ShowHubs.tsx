import useTranslation from 'next-translate/useTranslation';
import { FC, useEffect } from 'react';
import { Hub, ItemType } from '~/src/types';
import Grid from '../Grid';
import Button from '../primitives/Button';
import HubCard from '../Hub/HubCard';
import Loading from './Loading';
import NoResults from './NoResults';
import SpinLoader from './SpinLoader';
import useInfiniteLoading from '~/src/hooks/useInfiniteLoading';
import {
  getContainerAccessLevelChip,
  getContainerJoiningRestrictionChip,
  getContainerVisibilityChip,
} from '~/src/utils/utils';

interface Props {
  objectId: string;
  objectType: ItemType;
  forceRefresh?: boolean;
  isMember?: boolean;
  hasFollowed?: boolean;
}

const ShowHubs: FC<Props> = ({ objectId, objectType, forceRefresh = false, isMember, hasFollowed }) => {
  const hubsPerQuery = 24; // number of needs we get per query calls (make it 3 to test locally)
  const { t } = useTranslation('common');

  const { data: dataHubs, response, error, size, setSize, mutate } = useInfiniteLoading<{
    hubs: Hub[];
  }>((index) => `/${objectType}/${objectId}/nodes?PageSize=${hubsPerQuery}&page=${index + 1}`);
  const hubs = dataHubs ? [].concat(...dataHubs?.map((d) => d)) : [];
  const totalNumber = parseInt(response?.[0].headers['total-count']);
  const isLoadingInitialData = !dataHubs && !error;
  const isLoadingMore = isLoadingInitialData || (size > 0 && dataHubs && typeof dataHubs[size - 1] === 'undefined');
  const isEmpty = dataHubs?.[0]?.length === 0;
  const isReachingEnd = isEmpty || hubs?.length === totalNumber;

  useEffect(() => {
    mutate();
  }, [forceRefresh]);

  // const onFilterChange = (e) => {
  //   const id = e.target.name;
  //   const itemsNb = hubs.length > hubsPerQuery ? hubs.length : hubsPerQuery;
  //   if (id) {
  //     setSelectedProgramFilterId(Number(id));
  //     setHubsEndpoint(`/api/programs/${id}/hubs?items=${itemsNb}`);
  //   } else {
  //     setSelectedProgramFilterId(undefined);
  //     setHubsEndpoint(`/api/organizations/${objectId}/hubs?items=${hubsPerQuery}`);
  //   }
  // };

  return (
    <div>
      <div tw="flex flex-col"></div>
      <div tw="flex flex-col relative">
        {/* <div tw="flex flex-col relative">
          <OverflowGradient display={[undefined, undefined, 'none']} />
          <Filters
            resetButtonLabel="challenge.list_all"
            content={dataPrograms?.program
              // filter to hide draft programs
              ?.filter(({ status }) => status !== 'draft')
              .map(({ title, id }) => ({
                title,
                id,
              }))}
            onChange={(e) => onFilterChange(e)}
            isError={!!dataProgramsError}
            errorMessage="*Could not get programs filters"
            selectedId={selectedProgramFilterId}
          />
        </div> */}
        {!dataHubs ? (
          <Loading />
        ) : hubs?.length === 0 ? (
          <NoResults type="hub" />
        ) : (
          <Grid>
            {hubs.map((hub, index) => (
              <HubCard
                key={index}
                id={hub.id}
                title={hub.title}
                short_title={hub.short_title}
                short_description={hub.short_description}
                members_count={hub.stats.members_count}
                status={hub.status}
                last_activity={hub.updated}
                banner_url={hub.banner_url || hub.banner_url_sm}
                chip={hub?.user_access_level && getContainerAccessLevelChip(hub)}
                imageChips={
                  !hub?.user_access_level
                    ? [getContainerVisibilityChip(hub), getContainerJoiningRestrictionChip(hub)]
                    : [getContainerVisibilityChip(hub)]
                }
              />
            ))}
          </Grid>
        )}
        {
          // show load more button if object has more items than the default items we get from first call, or if we still have not attained last call page
          totalNumber > hubsPerQuery && size <= response?.[0].headers['total-pages'] && (
            <div tw="flex flex-col self-center pt-4">
              <Button onClick={() => setSize(size + 1)} disabled={isLoadingMore || isReachingEnd}>
                {isLoadingMore && <SpinLoader />}
                {isLoadingMore ? t('loadingWithEllipsis') : !isReachingEnd ? t('action.load') : t('noMoreResults')}
              </Button>
            </div>
          )
        }
      </div>
    </div>
  );
};

export default ShowHubs;
