import React, { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from '~/src/contexts/apiContext';
import { ItemType } from '~/src/types';
import useUser from '~/src/hooks/useUser';
import SpinLoader from './SpinLoader';
import Icon from '~/src/components/primitives/Icon';
import { useModal } from '~/src/contexts/modalContext';
import ListFollowers from './ListFollowers';
import tw from 'twin.macro';

interface Props {
  followState: boolean;
  itemType: ItemType;
  itemId: string;
  count?: number;
  hasNoStat?: boolean;
  isDisabled?: boolean;
}

const BtnFollow: FC<Props> = ({
  followState: followStateProp,
  itemType = undefined,
  itemId = undefined,
  count: countProp = 0,
  hasNoStat = false,
  isDisabled = false,
}) => {
  const [followState, setFollowState] = useState(followStateProp);
  const [sending, setSending] = useState(false);
  const [count, setCount] = useState(countProp);
  const api = useApi();
  const { showModal } = useModal();
  const { t } = useTranslation('common');
  const { user } = useUser();

  // useEffect(() => {
  //   // if data also comes from algolia (and not only api), get save state from api (because it's not available in algolia)
  //   const axiosSource = Axios.CancelToken.source();
  //   if (dataComesFromAlgolia) {
  //     if (user) {
  //       const fetchSaves = async () => {
  //         const res = await api
  //           .get(`/${itemType}/${itemId}/follow`, { cancelToken: axiosSource.token })
  //           .catch((err) => {
  //             if (!Axios.isCancel(err)) {
  //               console.error("Couldn't GET saves", err);
  //             }
  //           });
  //         if (res?.data?.user_follows) {
  //           setFollowState(res.data.user_follows);
  //         }
  //       };
  //       fetchSaves();
  //     }
  //     setFollowState(false);
  //   }
  //   return () => {
  //     // This will prevent to setFollowState on unmounted BtnStarIcon
  //     axiosSource.cancel();
  //   };
  // }, [user]);

  const showFollowersModal = (e) => {
    count > 0 && // open modal only if there are followers
    ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) && // if function is launched via keypress, execute only if it's the 'enter' key
      showModal({
        children: <ListFollowers itemId={itemId} itemType={itemType} />,
        title: t('follower.other'),
        maxWidth: '61rem',
      });
  };

  const changeStateFollow = (e) => {
    if (itemId && itemType && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      setSending(true);
      const action = followState ? 'unfollow' : 'follow';
      if (action === 'follow') {
        // if user is following
        api
          .post(`/${itemType}/${itemId}/follow`)
          .then(() => {
            setFollowState((prevState) => !prevState);
            setCount(count + 1);
            setSending(false);
          })
          .catch((err) => {
            console.error(`Couldn't POST /api/${itemType}/${itemId}/follow`, err);
            setSending(false);
          });
      } else {
        // else unfollow
        api
          .delete(`/${itemType}/${itemId}/follow`)
          .then(() => {
            setFollowState((prevState) => !prevState);
            setCount(count - 1);
            setSending(false);
          })
          .catch((err) => {
            console.error(`Couldn't DELETE /api/${itemType}/${itemId}/follow`, err);
            setSending(false);
          });
      }
    }
  };
  const text = followState ? t('action.unfollow') : t('action.follow');
  return (
    <span tw="relative z-0 inline-flex shadow-sm rounded-full" id="followBtn">
      <button
        type="button"
        tw="relative inline-flex items-center p-1 border border-solid bg-white text-xs font-medium hover:bg-gray-100 focus:(z-10 outline-none ring-1 ring-primary border-primary)"
        css={[
          hasNoStat ? tw`rounded-full border-primary text-primary` : tw`text-gray-700 border-gray-300 rounded-l-full`,
          isDisabled && tw`opacity-40`,
        ]}
        onClick={changeStateFollow}
        disabled={sending || isDisabled}
      >
        {sending ? (
          <SpinLoader />
        ) : followState ? (
          <Icon icon="material-symbols:notifications" tw="w-5 h-5" />
        ) : (
          <Icon icon="material-symbols:notifications-outline" tw="w-5 h-5" />
        )}
        {text}
      </button>
      {!hasNoStat && (
        <button
          type="button"
          tw="-ml-px relative inline-flex items-center px-2 py-2 rounded-r-full border border-solid border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-100 focus:(z-10 outline-none ring-1 ring-primary border-primary)"
          onClick={showFollowersModal}
        >
          {count}
        </button>
      )}
    </span>
  );
};
export default BtnFollow;
