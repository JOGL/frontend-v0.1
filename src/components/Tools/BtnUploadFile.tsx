import Icon from '~/src/components/primitives/Icon';
import React, { CSSProperties, FC, ReactNode, forwardRef, useImperativeHandle, useRef, useState } from 'react';
import { useApi } from '~/src/contexts/apiContext';
import { ItemType } from '~/src/types';
import SpinLoader from './SpinLoader';
import useTranslation from 'next-translate/useTranslation';
import { confAlert, convertBase64, entityItem } from '~/src/utils/utils';
import tw from 'twin.macro';
import { MAX_PAYLOAD_SIZE } from '~/src/utils/constants';

interface Props {
  fileTypes?: string[];
  itemId?: string;
  itemType?: ItemType;
  maxSizeFile?: number;
  singleFileOnly?: boolean;
  onChange?: (result: any) => void;
  setListFiles?: (listFiles: FileList) => void;
  refresh?: () => void;
  uploadStarted?: () => void;
  onFileSelected?: (listOfSelectedFiles: any) => void;
  type?: string;
  imageUrl?: string;
  ref?: any;
  text?: string | ReactNode;
  textUploading?: string;
  uploadNow?: boolean;
  userId?: string;
  customCss?: CSSProperties;
  useImageCropper?: boolean;
  uploadIcon?: string;
  selectedFolderId?: string;
  isParentLoading?: boolean;
}

const BtnUploadFile: FC<Props> = forwardRef(
  (
    {
      fileTypes = [''],
      itemId = '',
      itemType = '',
      maxSizeFile = MAX_PAYLOAD_SIZE,
      onChange = (result) => console.warn('Oops, the function onChange is missing !', result),
      setListFiles = (listFiles) => console.warn('Oops, the function setListFiles is missing !', listFiles),
      type = '',
      imageUrl = '',
      uploadNow = false,
      singleFileOnly,
      customCss,
      text,
      useImageCropper = false,
      userId,
      refresh,
      onFileSelected,
      uploadStarted,
      uploadIcon = 'mdi:pencil',
      selectedFolderId = null,
      isParentLoading,
    },
    ref: any
  ) => {
    const [loading, setLoading] = useState(false);
    const { t } = useTranslation('common');
    const api = useApi();
    const inputRef = useRef(null);

    const submit = (file, base64, props = {}) => {
      const route =
        itemType === 'needs'
          ? `/feed/contentEntities/${itemId}/${type}`
          : itemType === 'proposals'
          ? `/proposals/${itemId}/documents`
          : !singleFileOnly || !!(props as any)?.title
          ? `/${entityItem[itemType]?.back_path}/${itemId}/${type}`
          : '/images';
      api
        .post(route, {
          title: (props as any)?.title ?? file.name,
          file_name: (props as any)?.title ?? file.name,
          description: (props as any)?.description ?? '',
          created: new Date(file.lastModified).toISOString(),
          updated: new Date(file.lastModified).toISOString(),
          data: base64,
          created_by_user_id: userId ?? '',
          folder_id: selectedFolderId,
        })
        .then((res) => {
          if (res.status === 200) {
            setLoading(false); // set to false to show user upload has been successful
            // if (itemType === 'needs') alert(t('fileUploadedUpdateTheNeed'));
            if (res.data) {
              onChange({ error: '', img: res.data }); // on success, show new uploading image in the front
              refresh !== undefined && refresh();
            }
          } else {
            setLoading(false); // stop uploading if error
            onChange({
              error: t('error.generic'),
              img: '',
            });
          }
        })
        .catch((err) => {
          console.error(err);
          setLoading(false);
        });
    };

    const uploadFiles = async (files, props = {}) => {
      const acceptMultipleFiles = !singleFileOnly || false;
      let base64;
      if (!itemId || !itemType || !type || !files) {
        onChange({ error: t('error.generic') });
      } else {
        setLoading(true); // setting to true will render a loading wheel + disable the button

        // const bodyFormData = new FormData();
        if (acceptMultipleFiles) {
          for await (const file of files) {
            base64 = await convertBase64(file);
            submit(file, base64);
          }
        } else {
          base64 = await convertBase64(files[0]);
          submit(files[0], base64, props);
        }
        if (document.getElementById('btnUpload')) {
          (document.getElementById('btnUpload') as any).value = '';
        }
      }
    };

    const hasOnlySupportedFileTYpes = (files: FileList): boolean => {
      if (files) {
        for (let index = 0; index < files.length; index++) {
          if (files[index].type === 'video/quicktime') {
            return false;
          }
        }
      }
      return true;
    };

    const validFilesSize = (files) => {
      let nbFilesValid = 0;
      if (files) {
        for (let index = 0; index < files.length; index++) {
          if (files[index].size <= maxSizeFile) {
            nbFilesValid++;
          }
        }
      }
      return nbFilesValid === files.length; // return true or false depending on condition
    };

    const acceptMultipleFiles = !singleFileOnly || false;

    useImperativeHandle(ref, () => ({
      startUpload: (files, props) => {
        try {
          uploadStarted && uploadStarted();
          uploadFiles(files, props);
        } catch (e) {
          console.warn('ERE:: ', e);
        }
      },
      click: () => {
        inputRef.current && inputRef.current.click();
      },
    }));

    const handleChange = (event) => {
      event.preventDefault();

      if (!event.target.files) {
        return;
      }

      if (!hasOnlySupportedFileTYpes(event.target.files)) {
        event.target.value = '';
        confAlert.fire({ icon: 'error', title: t('error.fileTypeNotSupported') });
      } else if (!validFilesSize(event.target.files)) {
        onChange({
          error: `${t('error.fileTooLarge')}. (max: ${maxSizeFile / 1024 / 1024}MB)`,
          url: '',
        });
        event.target.value = '';
      } else if (!uploadNow) {
        // if uploadNow is false, just show list of doc, without attaching them (when attaching files to a post for ex)
        setListFiles(event.target.files);
        if (itemType === 'needs') alert(t('fileUploadedSaveTheNeed'));
        event.target.value = '';
      } else {
        // if, onFileSelected exist call this
        if (onFileSelected) {
          onFileSelected(event.target.files);
        } else {
          // else, attach file directly to object
          uploadFiles(event.target.files);
        }
      }
    };

    return (
      <>
        <div className="upload-btn-wrapper" style={customCss}>
          <div className="upload-btn" tw="cursor-pointer inline-block" style={{ verticalAlign: 'sub' }}>
            {loading || isParentLoading ? ( // render differently depending on uploading state
              <SpinLoader customCSS={tw`m-0`} />
            ) : (
              <Icon icon={uploadIcon} tw="m-0 w-5 h-5" />
            )}
          </div>
          <input
            id={loading || isParentLoading ? '' : 'btnUpload'}
            type="file"
            ref={inputRef}
            accept={fileTypes.join()}
            multiple={acceptMultipleFiles}
            onChange={(e) => handleChange(e)}
            style={{ pointerEvents: loading || isParentLoading ? 'none' : 'auto' }}
          />
        </div>
      </>
    );
  }
);

export default BtnUploadFile;
