import { FC, ReactNode } from 'react';
import tw from 'twin.macro';
import { AlertContainerStyled, AlertMessageStyled } from './Alert.styles';

interface Props {
  message: string | ReactNode;
  type: string;
}

const Alert: FC<Props> = ({ message, type }) => (
  <AlertContainerStyled
    role="alert"
    css={[
      type === 'success'
        ? tw`bg-green-100`
        : type === 'danger'
        ? tw`bg-red-100`
        : type === 'warning'
        ? tw`bg-yellow-100`
        : tw`bg-blue-100`,
    ]}
  >
    <AlertMessageStyled>{message}</AlertMessageStyled>
  </AlertContainerStyled>
);

export default Alert;
