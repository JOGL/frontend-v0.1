import styled from '@emotion/styled';
import tw from 'twin.macro';

export const AlertContainerStyled = styled.div`
  ${tw`rounded-md p-4 mb-4`}
`;

export const AlertMessageStyled = styled.div`
  ${tw`text-gray-700`}
`;

export const AlertDescStyled = styled.p`
  ${tw`mb-0`}
`;
