import useTranslation from 'next-translate/useTranslation';
import React, { FC, useState } from 'react';
import useUserData from '~/src/hooks/useUserData';
import A from '../primitives/A';
import UserCard from '../User/UserCard';
import useInfiniteLoading from '~/src/hooks/useInfiniteLoading';
import { ItemType } from '~/src/types';
import Button from '../primitives/Button';
import SpinLoader from './SpinLoader';
import Loading from './Loading';
import { Empty, Flex } from 'antd';
import { SpaceTabHeader } from '../Common/space/spaceTabHeader/spaceTabHeader';
import { SpaceContentStyled } from '../Common/space/space.styles';
import MembersInvite from '../Members/membersInvite/membersInvite';

interface Props {
  parentId: string;
  parentType: ItemType;
  canCreate: boolean;
}

const ShowMembers: FC<Props> = ({ parentId, parentType, canCreate }) => {
  const [searchText, setSearchText] = useState('');
  const [openCreateModal, setOpenCreateModal] = useState(false);

  const membersPerQuery = 24; // number of members we get per query calls (make it 3 to test locally)

  let membersEndpoints = `/${parentType}/${parentId}/members?Search=${searchText}&PageSize=${membersPerQuery}`;

  const { userData } = useUserData();
  const { t } = useTranslation('common');
  const { data: dataMembers, mutate: refresh, size, setSize, isLoading } = useInfiniteLoading(
    (index) => `${membersEndpoints}&page=${index + 1}`
  );

  const members = dataMembers ? [].concat(...dataMembers) : [];
  const isLoadingMore = isLoading || (size > 0 && dataMembers && typeof dataMembers[size - 1] === 'undefined');
  const isEmpty = dataMembers?.[0]?.length === 0;
  const isReachingEnd = isEmpty || (dataMembers && dataMembers[dataMembers.length - 1]?.length < membersPerQuery);

  return (
    <>
      <SpaceTabHeader
        title={t('person.other')}
        searchText={searchText}
        setSearchText={setSearchText}
        canCreate={canCreate}
        handleClick={() => setOpenCreateModal(true)}
        showSearch={!!searchText || !!members?.length}
      />
      {!isLoading && !members?.length && <Empty />}
      <SpaceContentStyled>
        {!userData && ( // if user is not connected
          <div tw="flex flex-col space-x-2 pb-4">
            <A href="/signin">{t('action.signInToContactMembers')}</A>
          </div>
        )}
        <div tw="flex flex-col relative">
          {isLoading && <Loading tw="self-center" />}
          {members && (
            <>
              <Flex wrap gap="middle">
                {members?.map((member, i) => (
                  <UserCard
                    key={i}
                    user={member}
                    //hide for now
                    showAddContribution={/*parentType === 'workspaces'*/ false}
                    entityId={parentId}
                    refresh={refresh}
                    role={
                      parentType === 'cfps' && member.labels.includes('applicant')
                        ? 'applicant'
                        : parentType === 'cfps' && member.labels.includes('reviewer')
                        ? 'reviewer'
                        : member.access_level
                    }
                  />
                ))}
              </Flex>
              {members?.length !== 0 && !isReachingEnd && (
                <Button
                  btnType="secondary"
                  tw="flex m-auto justify-center items-center pt-4"
                  onClick={() => setSize(size + 1)}
                  disabled={isLoadingMore || isReachingEnd}
                >
                  {isLoadingMore && <SpinLoader />}
                  {isLoadingMore ? t('loadingWithEllipsis') : !isReachingEnd ? t('action.load') : t('noMoreResults')}
                </Button>
              )}
            </>
          )}
        </div>
      </SpaceContentStyled>
      {openCreateModal && (
        <MembersInvite
          refetch={refresh}
          itemId={parentId}
          members={members}
          handleClose={() => setOpenCreateModal(false)}
        />
      )}
    </>
  );
};

export default ShowMembers;
