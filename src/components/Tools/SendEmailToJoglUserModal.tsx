import { FC, useState } from 'react';
import Button from '~/src/components/primitives/Button';
import { useApi } from '~/src/contexts/apiContext';
import SpinLoader from '~/src/components/Tools/SpinLoader';
import { confAlert, entityItem } from '~/src/utils/utils';
import { ItemType, User } from '~/src/types';
import Icon from '../primitives/Icon';
import useTranslation from 'next-translate/useTranslation';

interface Props {
  containerId: string;
  containerType: ItemType;
  recipients: User[];
  disclaimer?: string;
  closeModal: () => void;
}
const SendEmailToJoglUserModal: FC<Props> = ({
  containerId,
  containerType,
  recipients: initialRecipients,
  disclaimer,
  closeModal,
}) => {
  const { t } = useTranslation('common');
  const [subject, setSubject] = useState('');
  const [message, setMessage] = useState('');
  const [recipients, setRecipients] = useState(initialRecipients);
  const [isSending, setIsSending] = useState(false);
  const api = useApi();

  const sendEmail = (e) => {
    e.preventDefault();
    const param = { message, subject, user_ids: recipients.map((r) => r.id) };
    setIsSending(true);
    api
      .post(`/${entityItem[containerType]?.back_path}/${containerId}/message`, param)
      .then(() => {
        setIsSending(false);
        confAlert.fire({ icon: 'success', title: t('emailSentSuccess') });
        closeModal();
      })
      .catch((err) => {
        console.warn(`this is a skilled email sending error: ${err}`);
        confAlert.fire({ icon: 'error', title: t('error.somethingWentWrong') });
        setIsSending(false);
      });
  };

  const handleChange = (e) => {
    if (e.target.name === 'subject') setSubject(e.target.value);
    setMessage(e.target.value);
  };

  const disabledBtns = !message || !subject || recipients.length <= 0;

  const removeRecipient = (id: string) => {
    setRecipients((prevRecipients) => prevRecipients.filter((r) => r.id !== id));
  };

  const renderCards = () => {
    const toRender = recipients.slice(0, 5);
    const more = recipients.slice(5);

    return (
      <div tw="flex flex-wrap">
        {toRender?.map((r) => (
          <div
            key={r.id}
            tw="flex items-center gap-2 m-1 background[white] shadow-custom rounded-lg px-3 py-2 bg-white"
          >
            <div
              style={{
                backgroundImage: `url(${r.logo_url_sm ?? '/images/default/default-user.png'})`,
              }}
              tw="bg-cover bg-center h-6 w-6 rounded-full border border-solid border-[#ced4da] flex-none"
            />
            <span tw="text-xs font-semibold">
              {r.first_name} {r.last_name}
            </span>
            <div onClick={() => removeRecipient(r.id)}>
              <Icon icon="mdi:remove" tw="w-5 h-5 cursor-pointer hover:text-gray-600" />
            </div>
          </div>
        ))}

        {more.length > 0 && (
          <div
            key={`more-${more.length}`}
            tw="flex items-center gap-2 m-1 background[white] shadow-custom rounded-lg px-3 py-2 bg-white"
            title={more.map((r) => `${r.first_name} ${r.last_name}`).join('\n')}
          >
            +{more.length} {t('more')}
          </div>
        )}
      </div>
    );
  };

  return (
    <section>
      <form onSubmit={(e) => sendEmail(e)}>
        <div tw="mb-4">
          <label htmlFor="send_to">{t('action.sendTo')}</label>
          {renderCards()}
        </div>

        <div>
          <label htmlFor="subject">{t('subject')}</label>
          <input
            type="text"
            className="object styledInputTextArea"
            id="subject"
            name="subject"
            onChange={handleChange}
            required
          />
        </div>
        <div>
          <label htmlFor="message">{t('emailMessage')}</label>
          <textarea
            className="message styledInputTextArea"
            style={{ minHeight: '200px' }}
            id="message"
            name="message"
            onChange={handleChange}
            required
          />
        </div>

        {disclaimer && <div tw="text-sm mb-4">{disclaimer}</div>}
        <div tw="text-center">
          <Button type="submit" btnType="primary" disabled={disabledBtns || isSending}>
            {isSending ? <SpinLoader /> : t('action.sendEmail')}
          </Button>
        </div>
      </form>
    </section>
  );
};

export default SendEmailToJoglUserModal;
