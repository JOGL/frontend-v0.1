import useTranslation from 'next-translate/useTranslation';
import Image from 'next/image';
import Link from 'next/link';
import { FC, useMemo, useState } from 'react';
import useBreakpoint from '~/src/hooks/useBreakpoint';
import { Hub, ItemType, Organization } from '~/src/types';
import { confAlert, entityItem, isOwner, linkify } from '~/src/utils/utils';
import H1 from '../primitives/H1';
import InfoHtmlComponent from './Info/InfoHtmlComponent';
import Icon from '../primitives/Icon';
import { Menu, MenuButton, MenuList, MenuItem } from '@reach/menu-button';
import Button from '../primitives/Button';
import ShareBtns from './ShareBtns/ShareBtns';
import { useModal } from '~/src/contexts/modalContext';
import { useApi } from '~/src/contexts/apiContext';
import BtnJoin from './BtnJoin';
import { useRouter } from 'next/router';
import tw from 'twin.macro';
import ShowOnboarding from '../Onboarding/ShowOnboarding';
import ShowLinks from './ShowLinks';
import { ContainerPrivacyAndSecurityInfo } from './ContainerPrivacyAndSecurityInfo';
import ShowOrganizations from './ShowOrganizations';
import ShowSkillSdg from './ShowSkillSdg';
import useUser from '~/src/hooks/useUser';
import useGet from '~/src/hooks/useGet';
import { LinkToHub } from '../Hub/LinkToHub';
import { LinkToWorkspace } from '../workspace/linkToWorkspace/linkToWorkspace';
import { WorkspaceDetailModel } from '~/__generated__/types';

enum ModalTabs {
  About = 'about',
  PrivacyAndSecurity = 'privacyAndSecurity',
  Organizations = 'organizations',
}

interface ModalProps {
  container: Hub | WorkspaceDetailModel;
  containerType: ItemType;
  limitedVisibility: boolean;
  securityOptions: unknown;
  showJoinButton: boolean;
  showRequestToJoinButton: boolean;
  showInviteBox: boolean;
  pendingInvite?: unknown;
  acceptOrRejectInvite?: (e, type) => void;
  setIsOnboardingComplete?: (newState: boolean) => void;
}

export const ContainerMoreModal: FC<ModalProps> = ({
  container,
  containerType,
  limitedVisibility,
  securityOptions,
  showJoinButton,
  showRequestToJoinButton,
  showInviteBox,
  pendingInvite,
  acceptOrRejectInvite = () => {},
  setIsOnboardingComplete = () => {},
}) => {
  return (
    <div tw="flex flex-col gap-4 h-[80vh] overflow-auto">
      <Header
        container={container}
        containerType={containerType}
        showJoinButton={showJoinButton}
        showRequestToJoinButton={showRequestToJoinButton}
        showInviteBox={showInviteBox}
        acceptOrRejectInvite={acceptOrRejectInvite}
        pendingInvite={pendingInvite}
        setIsOnboardingComplete={setIsOnboardingComplete}
      />

      <Tabs
        container={container}
        containerType={containerType}
        limitedVisibility={limitedVisibility}
        securityOptions={securityOptions}
        showJoinButton={showJoinButton}
        showRequestToJoinButton={showRequestToJoinButton}
        showInviteBox={showInviteBox}
        acceptOrRejectInvite={acceptOrRejectInvite}
        pendingInvite={pendingInvite}
        setIsOnboardingComplete={setIsOnboardingComplete}
      />
    </div>
  );
};

interface HeaderAndMembershipButtonsProps {
  container: Hub;
  containerType: ItemType;
  showJoinButton: boolean;
  showRequestToJoinButton: boolean;
  showInviteBox: boolean;
  pendingInvite?: unknown;
  acceptOrRejectInvite?: (e, type) => void;
  setIsOnboardingComplete?: (newState: boolean) => void;
}

const Header: FC<HeaderAndMembershipButtonsProps> = ({
  container,
  containerType,
  showJoinButton,
  showRequestToJoinButton,
  showInviteBox,
  acceptOrRejectInvite,
  pendingInvite,
  setIsOnboardingComplete,
}) => {
  const screenType = useBreakpoint();

  return (
    <div tw="flex flex-col gap-4">
      {screenType !== 'mobile' && (
        <div tw="flex justify-between items-center">
          <RenderCreator container={container} />

          <div tw="flex gap-1 items-center">
            <RenderMembershipButtons
              container={container}
              containerType={containerType}
              showInviteBox={showInviteBox}
              showRequestToJoinButton={showRequestToJoinButton}
              showJoinButton={showJoinButton}
              pendingInvite={pendingInvite}
              acceptOrRejectInvite={acceptOrRejectInvite}
              setIsOnboardingComplete={setIsOnboardingComplete}
            />

            <RenderExtraActions container={container} containerType={containerType} />
          </div>
        </div>
      )}

      <div tw="md:(px-10)">
        <RenderContainerInfo container={container} containerType={containerType} />
      </div>

      {(container.interests?.length > 0 || container.keywords?.length > 0) && (
        <div tw="md:(px-10)">
          <ShowSkillSdg interests={container.interests} skills={container.keywords} noPadding />
        </div>
      )}

      {screenType === 'mobile' && (
        <>
          <RenderCreator container={container} />

          <div tw="flex justify-between items-center">
            <RenderLinks container={container} />
            <RenderExtraActions container={container} containerType={containerType} />
          </div>
        </>
      )}
    </div>
  );
};

const Tabs: FC<ModalProps> = ({
  container,
  containerType,
  limitedVisibility,
  securityOptions,
  showJoinButton,
  showRequestToJoinButton,
  showInviteBox,
  pendingInvite,
  acceptOrRejectInvite,
  setIsOnboardingComplete,
}) => {
  const { t } = useTranslation('common');
  const [currentTab, setCurrentTab] = useState<ModalTabs>(ModalTabs.About);
  const screenType = useBreakpoint();

  const tabs = useMemo(
    () => [
      { value: ModalTabs.About, label: t('about') },
      { value: ModalTabs.PrivacyAndSecurity, label: t('privacyAndSecurity') },
      { value: ModalTabs.Organizations, label: t('organization.other') },
    ],
    [t]
  );

  return (
    <div tw="flex flex-col gap-4">
      {/* TAB SELECT ROW */}
      <div tw="flex items-center justify-between border-t border-b md:(mx-10)">
        <div tw="flex gap-2 items-center text-center p-1 md:(p-6 gap-8 flex-wrap flex-grow)">
          {tabs.map(({ value, label }) => (
            <div
              tw="text-[#191919] text-sm font-medium cursor-pointer hover:opacity-80"
              key={value}
              onClick={() => setCurrentTab(value)}
              css={currentTab === value && tw`underline underline-offset-8 decoration-2 decoration-[#191919]`}
            >
              {label}
            </div>
          ))}
        </div>

        {screenType !== 'mobile' && <RenderLinks container={container} />}
      </div>
      {/* TAB CONTENT */}
      {currentTab === ModalTabs.About && (
        <div tw="flex flex-col gap-2">
          {screenType === 'mobile' && (
            <RenderMembershipButtons
              container={container}
              containerType={containerType}
              showInviteBox={showInviteBox}
              showRequestToJoinButton={showRequestToJoinButton}
              showJoinButton={showJoinButton}
              pendingInvite={pendingInvite}
              acceptOrRejectInvite={acceptOrRejectInvite}
              setIsOnboardingComplete={setIsOnboardingComplete}
            />
          )}

          {/* containshtml and nopadding need to be sent like that to avoid warnings */}
          <InfoHtmlComponent content={container?.description} tw="md:mx-10" />
        </div>
      )}
      {currentTab === ModalTabs.PrivacyAndSecurity && (
        <div tw="md:mx-10">
          <ContainerPrivacyAndSecurityInfo
            object={container}
            limitedVisibility={limitedVisibility}
            securityOptions={securityOptions}
          />
        </div>
      )}
      {currentTab === ModalTabs.Organizations && (
        <OrganizationTab container={container} containerType={containerType} />
      )}
    </div>
  );
};

interface SubcomponentBasicProps {
  container: Hub;
  containerType: ItemType;
}

const OrganizationTab: FC<SubcomponentBasicProps> = ({ container, containerType }) => {
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const { user } = useUser();
  const api = useApi();

  //Needed otherwise we would need to check every possible variation of the same type when selecting which modal to render in the Add button
  const frontendContainerType = entityItem[containerType].front_path;

  const { data: alreadyLinkedOrganizations } = useGet<Organization[]>(
    `/${entityItem[containerType].back_path}/${container?.id}/organizations`
  );

  const linkToContainer = async (containerId: string, containerName: string) => {
    await api
      .post(`/${entityItem[containerType].back_path}/${container?.id}/communityEntity/${containerId}/link`)
      .then(() => {
        confAlert.fire({ icon: 'success', title: `${container.title} is now linked to ${containerName}` });
      })
      .catch((err) => {
        confAlert.fire({ icon: 'error', title: `Couldn't link ${containerName} to hub.` });
        console.error("Couldn't link to container", err);
      });

    closeModal();
  };

  const linkToProps = useMemo(() => {
    return {
      type: 'organization',
      userObjectsUrl: `/users/${user?.id}/organizations?permission=manage`,
      userLogoUrl: user?.logo_url_sm ?? user?.logo_url,
      isAdmin: true,
      alreadyLinked: alreadyLinkedOrganizations,
      onSubmitProps: (containerId, containerName) => {
        linkToContainer(containerId, containerName);
      },
      closeModal: closeModal,
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [alreadyLinkedOrganizations, user]);

  return (
    <div tw="flex flex-col gap-2">
      {container.user_access?.permissions?.includes('manage') && (
        <Button
          btnType="secondary"
          tw="flex items-center gap-2 self-end text-primary text-sm font-semibold rounded-md border-primary md:(mx-10)"
          onClick={() => {
            showModal({
              children:
                frontendContainerType === 'hub' ? (
                  <LinkToHub hub={container as Hub} {...linkToProps} />
                ) : (
                  <LinkToWorkspace workspace={container as WorkspaceDetailModel} {...linkToProps} />
                ),
              title: `Link an organization to ${container?.title}`,
              maxWidth: '50rem',
            });
          }}
        >
          {t('action.add')}
          <Icon icon="iconamoon:sign-plus-fill" tw="w-5 h-5 m-0" />
        </Button>
      )}

      <ShowOrganizations objectId={container.id} objectType={entityItem[containerType].back_path} />
    </div>
  );
};

const RenderCreator: FC<{ container: Hub }> = ({ container }) => {
  const { t } = useTranslation('common');

  return (
    <div tw="flex gap-2 flex-wrap">
      <Image
        src={container?.created_by?.logo_url_sm ?? '/images/default/default-user.png'}
        alt={`${container?.created_by?.first_name} ${container?.created_by?.last_name}`}
        width={25}
        height={25}
      />
      <span tw="text-[#878D97]">{t('creator')}:</span>
      <Link href={`/user/${container?.created_by?.id}`}>
        <span tw="text-primary">{`${container?.created_by?.first_name} ${container?.created_by?.last_name}`}</span>
      </Link>
    </div>
  );
};

const RenderMembershipButtons: FC<HeaderAndMembershipButtonsProps> = ({
  container,
  containerType,
  showInviteBox,
  showRequestToJoinButton,
  showJoinButton,
  pendingInvite,
  acceptOrRejectInvite,
  setIsOnboardingComplete,
}) => {
  const { t } = useTranslation('common');
  const router = useRouter();

  return showInviteBox ? (
    <AcceptRejectInviteBox acceptOrRejectInvite={acceptOrRejectInvite} containerType={containerType} />
  ) : // if onboarding questionnaire is enabled, and multiple other conditions, show button to open onboarding questionnaire modal
  showRequestToJoinButton ? (
    <RequestToJoinButton
      container={container}
      containerType={containerType}
      setIsOnboardingComplete={setIsOnboardingComplete}
    />
  ) : (
    // else show the basic "join" button (can be open or request to join)
    showJoinButton && (
      <BtnJoin
        joinType={container.joining_restriction}
        itemType={containerType}
        itemId={container.id}
        textJoin={container.joining_restriction === 'open' ? t('action.join') : t('action.requestToJoin')}
        count={container.stats.members_count}
        pending_invite={pendingInvite}
        showMembersModal={() =>
          router.push(`/${entityItem[containerType].back_path}/${router.query.id}?tab=members`, undefined, {
            shallow: true,
          })
        }
        hasNoStat
        customCss={[tw`text-primary text-sm font-semibold uppercase rounded-md border-primary w-fit`]}
      />
    )
  );
};

const RenderExtraActions: FC<SubcomponentBasicProps> = ({ container, containerType }) => {
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const api = useApi();

  const leaveObject = () => {
    api.post(`/${entityItem[containerType].back_path}/${container.id}/leave`).then((res) => {
      res.status === 200 && location.reload();
    });
  };

  return (
    <div>
      <Menu>
        <MenuButton tw="text-[.8rem] text-gray-700 hover:bg-gray-200 rounded-full w-8 h-8 mx-auto rotate-90">
          •••
        </MenuButton>
        <MenuList portal={false} className="post-manage-dropdown">
          <ShareBtns type={containerType} specialObjId={container.id} isActionMenu />

          {container.user_access_level !== 'visitor' &&
            container.user_access_level !== 'pending' &&
            !isOwner(container) && (
              <MenuItem
                tw="flex items-center"
                onSelect={() => {
                  showModal({
                    children: (
                      <div tw="inline-flex space-x-3">
                        <Button btnType="danger" onClick={leaveObject}>
                          {t('yes')}
                        </Button>
                        <Button onClick={closeModal}>{t('no')}</Button>
                      </div>
                    ),
                    title: t('areYouSureYouWantToLeaveThisContainer', { container: t('hub.one') }),
                    maxWidth: '30rem',
                  });
                }}
              >
                <Icon icon="pepicons-pop:leave" tw="w-4 h-4" />
                {t('action.leave')}
              </MenuItem>
            )}
        </MenuList>
      </Menu>
    </div>
  );
};

const RenderContainerInfo: FC<SubcomponentBasicProps> = ({ container, containerType }) => {
  const { t } = useTranslation('common');
  const screenType = useBreakpoint();

  return (
    <div tw="flex flex-col gap-1">
      <H1 title={container.title} tw="break-all line-clamp-1 text-[22px] font-bold md:(line-clamp-2 text-[32px])">
        {container.title}
      </H1>

      <span tw="uppercase font-medium text-[#878D97] text-xs">{t(entityItem[containerType]?.translationId)}</span>

      {screenType !== 'mobile' && (
        /* containshtml and nopadding need to be sent like that to avoid warnings */
        <InfoHtmlComponent
          content={linkify(container?.short_description)}
          containshtml="true"
          nopadding="true"
          tw="text-[#4E4E4E]"
        />
      )}
    </div>
  );
};

interface AcceptRejectInviteBoxProps {
  containerType: ItemType;
  acceptOrRejectInvite: (e, type) => void;
}

const AcceptRejectInviteBox: FC<AcceptRejectInviteBoxProps> = ({ containerType, acceptOrRejectInvite }) => {
  const { t } = useTranslation('common');

  return (
    <div tw="flex px-6 py-3 border border-gray-300 border-solid rounded-md text-gray-500 items-center">
      {t('youHaveBeenInvitedToJoinThisContainer', { container: t(entityItem[containerType].translationId) })}
      <div tw="space-x-2 pl-5">
        <Button
          tw="text-green-500 border-green-500 bg-white hover:(text-white bg-green-500)"
          onClick={(e) => acceptOrRejectInvite(e, 'accept')}
        >
          {t('action.accept')}
        </Button>
        <Button
          tw="text-danger border-danger bg-white hover:(text-white bg-danger)"
          onClick={(e) => acceptOrRejectInvite(e, 'reject')}
        >
          {t('action.reject')}
        </Button>
      </div>
    </div>
  );
};

interface RequestToJoinButtonProps extends SubcomponentBasicProps {
  setIsOnboardingComplete: (newState: boolean) => void;
}

const RequestToJoinButton: FC<RequestToJoinButtonProps> = ({ container, containerType, setIsOnboardingComplete }) => {
  const { showModal, closeModal } = useModal();
  const { t } = useTranslation('common');

  return (
    <Button
      btnType="secondary"
      tw="flex items-center text-primary text-sm font-semibold uppercase rounded-md border-primary"
      onClick={() => {
        showModal({
          children: (
            <ShowOnboarding
              object={container}
              itemType={containerType}
              type="first_onboarding"
              setIsOnboardingComplete={(isOnboardingComplete: boolean) => {
                closeModal();
                setIsOnboardingComplete(isOnboardingComplete);
              }}
            />
          ),
          title: t('answeringQuestions'),
          maxWidth: '40rem',
        });
      }}
    >
      <Icon icon="material-symbols:person-add-outline" tw="w-5 h-5" />
      {t('action.requestToJoin')}
    </Button>
  );
};

const RenderLinks: FC<{ container: Hub }> = ({ container }) => {
  const { t } = useTranslation('common');

  return (
    <div tw="flex gap-2 flex-none" css={container?.links?.length > 0 && container?.external_website && tw`divide-x-2`}>
      {/* LINKS */}
      {container?.links?.length > 0 && <ShowLinks links={container?.links ?? []} noText />}

      {/* EXTERNAL WEBSITE */}
      {container?.external_website && (
        <Link
          href={container?.external_website}
          target="_blank"
          rel="noreferrer"
          tw="flex gap-1 items-center text-primary pl-4"
        >
          <Icon icon="iconoir:internet" tw="m-0" />
          <span>{t('website')}</span>
        </Link>
      )}
    </div>
  );
};
