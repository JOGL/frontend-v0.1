import { useQuill } from 'react-quilljs';
import 'quill/dist/quill.snow.css'; // Add css for snow theme
import { QuillOptionsStatic } from 'quill';
import { ImageDrop } from 'quill-image-drop-module';
import ImageResize from 'quill-image-resize-module-react';
import BlotFormatter from 'quill-blot-formatter';
import MagicUrl from 'quill-magic-url';
import 'quill-mention';
import 'quill-mention/dist/quill.mention.css';
import { CSSProperties, Dispatch, SetStateAction, useEffect } from 'react';

interface CustomReactQuillProps extends QuillOptionsStatic {
  onChange: (value: string) => void;
  style?: CSSProperties;
  isExpanded?: boolean;
  value?: string;
  tw?: string;
  id?: string;
  setCharLength: Dispatch<SetStateAction<number | string>>;
}

export default function CustomReactQuill(props: CustomReactQuillProps) {
  const { quill, quillRef, Quill } = useQuill({
    formats: props.formats,
    modules: props.modules,
    theme: props.theme,
    placeholder: props.placeholder,
  });

  // get lenght and set it back to parent component
  useEffect(() => {
    if (quill) props.setCharLength(quill.getText().trim());
  }, [quill?.getText()]);

  useEffect(() => {
    // focus to editor if expanded, meaning user wants to post (in the feed) directly
    if (props.isExpanded && quill) quill.focus();

    // force open mention box when clicking on @ mention icon
    let customButton = document.querySelector(`#postBox${props.id} .ql-mentionUser`);
    customButton?.addEventListener('click', function () {
      quill?.getModule('mention').openMenu('@');
    });
  }, [quill]);

  useEffect(() => {
    if (quill) {
      if (props.value && quill?.root.innerText.trim().length === 0) {
        quill.clipboard.dangerouslyPasteHTML(props.value);
      }

      if (!props.value) {
        quill.setText('');
      }

      /*
       * Workaround for https://github.com/zenoamaro/react-quill/issues/784
       * This takes into consideration if there's a wysiwyg component open in a modal
       * If so it checks for extra toolbars there, else checks the main document body
       */

      let modalOpenElements = document?.getElementsByClassName('ReactModalPortal');
      let extraToolbars;

      if ((modalOpenElements?.length ?? 0) === 3) {
        extraToolbars = modalOpenElements[2].getElementsByClassName('ql-toolbar ql-snow');
      } else {
        extraToolbars = document?.getElementsByClassName('ql-toolbar ql-snow');
      }
      if (extraToolbars && extraToolbars?.length > 1) {
        extraToolbars[1]?.remove();
      }
      //-----

      quill.on('text-change', (delta, oldDelta, source) => {
        if (source !== 'api') {
          props.onChange(quill.root.innerHTML);
        }
      });
    }
  }, [quill, props.value]);

  if (Quill && !quill) {
    // For execute this line only once.
    Quill.register('modules/imageDrop', ImageDrop);
    Quill.register('modules/imageResize', ImageResize);
    Quill.register('modules/blotFormatter', BlotFormatter);
    Quill.register('modules/magicUrl', MagicUrl);
    const Link = Quill.import('formats/link');
    Link.sanitize = function (url) {
      // quill by default creates relative links if scheme is missing.
      if (!url.startsWith('http://') && !url.startsWith('https://')) {
        return `https://${url}`;
      }
      return url;
    };
  }

  return (
    <div style={props.style ?? {}} css={props.tw && props.tw}>
      <div ref={quillRef} />
    </div>
  );
}
