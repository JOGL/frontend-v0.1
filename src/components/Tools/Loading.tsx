import React, { FC } from 'react';
import SpinLoader from './SpinLoader';

interface Props {
  active?: boolean;
  height?: string;
}
const Loading: FC<Props> = ({ active = true, height = '5rem', children }) => {
  if (active) {
    return (
      <div style={{ height, display: 'flex' }}>
        <div tw="flex justify-center m-auto text-gray-500">
          <SpinLoader size="32" />
        </div>
      </div>
    );
  }
  return <>{children}</>;
};
export default Loading;
