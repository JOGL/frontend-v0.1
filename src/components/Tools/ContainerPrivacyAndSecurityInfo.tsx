import useTranslation from 'next-translate/useTranslation';
import { FC, useMemo } from 'react';
import { Cfp, Hub, Organization } from '~/src/types';
import MessageBox from './MessageBox';
import Trans from 'next-translate/Trans';
import Icon from '../primitives/Icon';

interface ContainerPrivacyAndSecurityInfoProps {
  object: Organization | Hub | Cfp;
  limitedVisibility: boolean;
  securityOptions: any;
}

export const ContainerPrivacyAndSecurityInfo: FC<ContainerPrivacyAndSecurityInfoProps> = ({
  object,
  limitedVisibility,
  securityOptions,
}) => {
  const { t } = useTranslation('common');

  const condition = useMemo(() => {
    const contentPrivacy =
      object.content_privacy === 'custom' ? (limitedVisibility ? 'private' : 'public') : object.content_privacy;
    const joiningRestrictionLevel =
      object.joining_restriction === 'custom' && object.user_joining_restriction_level !== 'forbidden'
        ? object.user_joining_restriction_level
        : object.joining_restriction;

    if ('proposal_participation' in object) {
      return {
        listing_privacy: object.listing_privacy,
        content_privacy: contentPrivacy,
        joining_restriction: joiningRestrictionLevel || object.joining_restriction,
        proposal_participiation: (object as Cfp).proposal_participation,
        proposal_privacy: (object as Cfp).proposal_privacy,
        discussion_participation: (object as Cfp).discussion_participation,
      };
    } else {
      return {
        listing_privacy: object.listing_privacy,
        content_privacy: contentPrivacy,
        joining_restriction: joiningRestrictionLevel || object.joining_restriction,
        proposal_participiation: undefined,
        proposal_privacy: undefined,
        discussion_participation: undefined,
      };
    }
  }, [limitedVisibility, object]);

  return (
    <div tw="text-gray-500 mb-6 w-full space-y-7">
      {object.status === 'draft' && (
        <MessageBox>
          {t('thisIsInDraft')}. {t('onlyMembersCanFindThisPageAndSeeItsContent')}.
        </MessageBox>
      )}
      {object.status === 'archived' && (
        <MessageBox>
          {t('thisIsArchived')}. {t('onlyMembersCanFindThisPageAndSeeItsContent')}.
        </MessageBox>
      )}
      {securityOptions.map((secuOption, i) => (
        <div tw="items-center" key={`security-${i}`}>
          <h4 tw="text-[16px] font-bold mb-1">{t(secuOption.header)}</h4>
          <p tw="italic mb-1">{t(secuOption.info)}</p>
          {secuOption.options
            .filter((option) =>
              option.isChecked ? option.isChecked(object?.management) : option.key === condition[secuOption.key]
            )
            .map((option, i) => {
              return (
                <div
                  tw="border border-gray-200 rounded px-3 md:px-4 py-2 flex w-full max-md:flex-col md:items-center"
                  key={`options-${i}`}
                >
                  <div tw="flex items-center gap-2 md:gap-4">
                    <Icon tw="w-6 h-6 flex-none" icon={option.icon} />
                    <span tw="font-medium">{t(option.header)}</span>
                  </div>
                  {option.info &&
                    !['proposal_participiation', 'proposal_privacy', 'discussion_participation'].includes(
                      secuOption.key
                    ) &&
                    option.key !== 'custom' && (
                      <div tw="flex items-center gap-2 md:gap-4 md:ml-4 max-md:pt-1">
                        <Icon icon="material-symbols:circle" tw="h-1 w-1 flex-none max-md:hidden" />
                        <Trans i18nKey={`common:${option.info}`} components={[<b key={option.key} />]} />
                      </div>
                    )}
                </div>
              );
            })}
        </div>
      ))}
    </div>
  );
};
