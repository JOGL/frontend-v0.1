import { FC, useReducer, useRef } from 'react';
import Image from '~/src/components/primitives/Image';
import Alert from '~/src/components/Tools/Alert/Alert';
import BtnUploadFile from '~/src/components/Tools/BtnUploadFile';
import SpinLoader from '../Tools/SpinLoader';
import useTranslation from 'next-translate/useTranslation';
import TitleInfo from '../Tools/TitleInfo';
import Icon from '../primitives/Icon';
import ImageCropper from '../Tools/ImageCropper';
import { useImageCropperModalModal } from '~/src/contexts/imageCropperContext';
import { useApi } from '~/src/contexts/apiContext';

interface ImageUploadComponentProps {
  imageUrl: string;
  itemId: string;
  onChange: (key: any, value: any) => void;
  title?: string;
  tooltipMessage?: string;
  type?: string;
  placeholder?: string;
  useImageCropper?: boolean;
  mandatory?: boolean;
}

const ImageUploadComponent: FC<ImageUploadComponentProps> = (
  { imageUrl, itemId, onChange, title, placeholder, mandatory = false, useImageCropper = false } = {
    imageUrl: '',
    itemId: '',
    onChange: (key, value) => console.warn(`onChange doesn't exist to update ${value}`),
    title: 'Title',
  }
) => {
  const [state, setState] = useReducer(
    (state: any, action: any) => ({
      ...state,
      ...action,
    }),
    {
      error: '',
      uploading: false,
    }
  );

  const { showModal, closeModal, isOpen } = useImageCropperModalModal();

  const inputRef = useRef(null);

  const { t } = useTranslation('common');
  const api = useApi();

  const handleChange = (result: any) => {
    if (result.error !== '') {
      setState({ error: result.error });
    } else {
      setState({ error: '' });
      if (result.url !== '') {
        onChange('image_id', result.url);
      }
    }
  };

  const { error, uploading } = state;

  const submitImage = (file, base64) => {
    const route = '/images';
    const imageInfo = {
      title: file.name ?? file.file_name,
      file_name: file.name ?? file.file_name,
      description: '',
      created: file?.lastModified ? new Date(file.lastModified).toISOString() : file.created,
      updated: file?.lastModified ? new Date(file.lastModified).toISOString() : new Date(),
      data: base64,
      created_by_user_id: itemId ?? '',
    };
    api
      .post(route, imageInfo)
      .then((res) => {
        if (res.status === 200) {
          if (res.data) {
            onChange('image_id', res.data);
          }
        }
      })
      .catch((err) => {
        console.error(err);
      });
    closeModal();
  };

  const removeImage = () => {
    onChange('image_id', null);
    onChange('image_url', null);
    onChange('image_url_sm', null);
  };

  return (
    <div className="formImg">
      <TitleInfo title={title} mandatory={mandatory} />
      <div className="btnUploadZone" tw="mt-1">
        {error !== '' && <Alert type="danger" message={error} />}
        {uploading && <SpinLoader />}

        {!imageUrl && (
          <BtnUploadFile
            uploadNow={false}
            fileTypes={['image/jpeg', 'image/png', 'image/jpg']}
            itemId={itemId}
            type="banner"
            maxSizeFile={4194304}
            singleFileOnly
            useImageCropper={useImageCropper}
            ref={inputRef}
            customCss={!imageUrl ? { display: 'none' } : { display: 'block' }}
            text={t('changeTheImage')}
            onChange={handleChange}
            setListFiles={async (files) => {
              if (typeof files === 'object' && files.length > 0) {
                showModal({
                  children: (
                    <ImageCropper
                      currentImage={imageUrl}
                      handleOnChange={handleChange}
                      maxSizeFile={4194304}
                      aspectRatio={16 / 5}
                      fileTypes={['image/jpeg', 'image/png', 'image/jpg']}
                      openFileDialog={true}
                      noteFiles={[files[0]]}
                      onSave={(file, base64) => submitImage(file, base64)}
                      onRemove={removeImage}
                      onCancel={closeModal}
                      type="banner"
                    />
                  ),
                  showCloseButton: false,
                  showTitle: false,
                  maxWidth: '50rem',
                });
              }
            }}
          />
        )}
      </div>

      <div className="preview banners">
        {imageUrl ? (
          <div tw="relative">
            <div className="upload-btn-wrapper" tw="right-0 z-[1]">
              <div
                className="upload-btn"
                tw="cursor-pointer inline-block"
                style={{ verticalAlign: 'sub' }}
                onClick={(e) => {
                  e.preventDefault();
                  document.getElementById('image-upload-onclick')?.click();
                }}
              >
                <Icon icon="mdi:pencil" tw="m-0" />
              </div>
            </div>

            <Image
              id="image-upload-onclick"
              src={imageUrl}
              priority
              alt="JOGL image"
              tw="rounded-md cursor-pointer [border: 1px solid #ced4da]"
              onClick={(e) => {
                e.preventDefault();
                showModal({
                  children: (
                    <ImageCropper
                      handleOnChange={handleChange}
                      maxSizeFile={4194304}
                      aspectRatio={16 / 5}
                      fileTypes={['image/jpeg', 'image/png', 'image/jpg']}
                      openFileDialog={true}
                      currentImage={imageUrl}
                      onSave={(file, base64) => submitImage(file, base64)}
                      onRemove={removeImage}
                      onCancel={closeModal}
                      type="banner"
                    />
                  ),
                  showCloseButton: false,
                  showTitle: false,
                  maxWidth: '50rem',
                });
              }}
            />
          </div>
        ) : (
          <div
            tw="(w-auto min-h-[250px] min-w-full)! flex justify-center flex-col rounded-md items-center cursor-pointer shadow-sm bg-[#F6F6F6] text-[#8B8B8B] text-[14px]"
            onClick={(e) => {
              e.preventDefault();
              inputRef && inputRef.current && inputRef.current.click();
            }}
          >
            <Icon icon="ph:image-light" style={{ color: '#8B8B8B', width: '120px', height: '120px' }} />
            <span tw="px-4">{placeholder ?? t('considerAddingCoverPhoto')}</span>
          </div>
        )}
      </div>
    </div>
  );
};

export default ImageUploadComponent;
