import { useEffect } from 'react';
// import { onMessage } from 'firebase/messaging';
import { usePushNotificationStore } from '../../store/push-notifications/PushNotificationsStoreProvider';
// import Swal from 'sweetalert2';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { useModal } from '~/src/contexts/modalContext';
import { PushNotificationsModal } from './PushNotificationsModal';
import { useAuth } from '~/auth/auth';

export const PUSH_NOTIFICATIONS_QUERY_PARAM = 'request-push-notifications';
export const LOCAL_STORAGE_PUSH_NOTIFICATIONS_DISABLED = 'disable_push_notifications';

// const pushNotification = Swal.mixin({
//   toast: true,
//   position: 'bottom-start',
//   timer: 10000,
//   timerProgressBar: true,
//   confirmButtonText: 'Open',
// });

const PushNotificationsHandler: React.FC = () => {
  const { isLoggedIn } = useAuth();
  const { query, push, replace } = useRouter();
  const { t } = useTranslation('common');
  const { showModal } = useModal();
  const { messaging, isFirebaseSupported } = usePushNotificationStore((s) => s);

  useEffect(() => {
    if (
      !isLoggedIn ||
      !isFirebaseSupported ||
      !('Notification' in window) ||
      Notification.permission === 'granted' ||
      Notification.permission === 'denied' ||
      localStorage.getItem(LOCAL_STORAGE_PUSH_NOTIFICATIONS_DISABLED) ||
      !query[PUSH_NOTIFICATIONS_QUERY_PARAM]
    ) {
      if (query[PUSH_NOTIFICATIONS_QUERY_PARAM]) {
        delete query[PUSH_NOTIFICATIONS_QUERY_PARAM];
        replace({ query }, undefined, { shallow: true });
      }
      return;
    }

    showModal({
      children: <PushNotificationsModal />,
      title: t('pushNotifications'),
      maxWidth: '40rem',
      showCloseButton: false,
      shouldCloseOnEsc: false,
      shouldCloseOnOverlayClick: false,
      showTitle: false,
    });
  }, [isLoggedIn, isFirebaseSupported, query, replace]);

  useEffect(() => {
    if (!messaging || !isFirebaseSupported) {
      return;
    }

    // onMessage(messaging, async (payload) => {
    //   const { isConfirmed } = await pushNotification.fire({
    //     icon: 'info',
    //     title: payload.notification.title,
    //     text: payload.notification.body,
    //     showConfirmButton: !!payload?.fcmOptions?.link,
    //   });

    //   if (isConfirmed) {
    //     push(payload.fcmOptions.link);
    //   }
    // });
  }, [messaging, isFirebaseSupported, push]);

  return null;
};

export { PushNotificationsHandler };
