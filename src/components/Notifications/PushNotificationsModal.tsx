import { useState, FC } from 'react';
import { Messaging, getToken } from 'firebase/messaging';
import { useRouter } from 'next/router';
import { confAlert } from '~/src/utils/utils';
import { useApi } from '~/src/contexts/apiContext';
import useTranslation from 'next-translate/useTranslation';
import { useModal } from '~/src/contexts/modalContext';
import Image from 'next/image';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';
import { LOCAL_STORAGE_PUSH_NOTIFICATIONS_DISABLED, PUSH_NOTIFICATIONS_QUERY_PARAM } from './PushNotificationsHandler';
import { usePushNotificationStore } from '~/src/store/push-notifications/PushNotificationsStoreProvider';

const requestPermission = async function (messaging: Messaging, serviceWorkerRegistration: ServiceWorkerRegistration) {
  if (!('Notification' in window)) {
    return null;
  }

  try {
    await Notification.requestPermission();
  } catch {
    // If the user closes the permission request we end up here, nothing to do
  }

  if (Notification.permission === 'granted') {
    const token = await getToken(messaging, {
      serviceWorkerRegistration,
      vapidKey: 'BIudcCqeFrLJZ50_BHiBhkHZKcMndYPCmn2HZb-kD0ZpQbGETgBlnZ279KoqFO-y1A8zuo9boNWqqmp7DveUZ3U',
    });

    return token;
  }

  return null;
};

const PushNotificationsModal: React.FC = () => {
  const api = useApi();
  const { query, replace } = useRouter();
  const { t } = useTranslation('common');
  const { closeModal } = useModal();
  const [isLoading, setIsLoading] = useState(false);
  const { messaging, serviceWorkerRegistration } = usePushNotificationStore((s) => s);

  const onSkip = () => {
    delete query[PUSH_NOTIFICATIONS_QUERY_PARAM];
    replace({ query }, undefined, { shallow: true });
    closeModal();
  };

  const onEnable = async () => {
    try {
      setIsLoading(true);
      const token = await requestPermission(messaging, serviceWorkerRegistration);

      if (token) {
        await api.post('/users/push/token', `"${token}"`);
      }
    } catch (err) {
      console.error(err);
      confAlert.fire({ icon: 'error', title: t('error.somethingWentWrong') });
    } finally {
      setIsLoading(false);
      onSkip();
    }
  };

  const onDontEnable = () => {
    window.localStorage.setItem(LOCAL_STORAGE_PUSH_NOTIFICATIONS_DISABLED, 1);
    onSkip();
  };

  return (
    <div tw="flex flex-col items-center m-auto gap-5">
      <Image src="/images/default/toggle-button-colored.png" alt="sleeping-bell-on-clouds" width={94} height={50} />
      <div tw="max-w-[320px] text-xl font-bold text-center">{t('weStronglyRecommendEnablingPushNotifications')}</div>
      <div tw="flex flex-col text-sm gap-2">
        <div tw="flex">
          <Image
            src="/images/default/check-mark-round-colored.png"
            alt="sleeping-bell-on-clouds"
            width={18}
            height={18}
            tw="h-[18px] mr-2"
          />
          {t('stayUpdatedInstantlyOnImportantActivities')}
        </div>
        <div tw="flex">
          <Image
            src="/images/default/check-mark-round-colored.png"
            alt="sleeping-bell-on-clouds"
            width={18}
            height={18}
            tw="h-[18px] mr-2"
          />
          {t('ensureEfficientTeamwork')}
        </div>
        <div tw="flex">
          <Image
            src="/images/default/check-mark-round-colored.png"
            alt="sleeping-bell-on-clouds"
            width={18}
            height={18}
            tw="h-[18px] mr-2"
          />
          {t('fullyCustomizeWhatNotificationsYouReceive')}
        </div>
      </div>
      <div tw="flex gap-5 mt-6">
        <Button btnType="secondary" tw="px-8" onClick={onDontEnable} disabled={isLoading}>
          {isLoading && <SpinLoader />}
          {t('action.dontEnable')}
        </Button>
        <Button tw="px-8" onClick={onEnable} disabled={isLoading}>
          {isLoading && <SpinLoader />}
          {t('action.enable')}
        </Button>
      </div>
      <button tw="text-[#5E3EBF]" onClick={onSkip} disabled={isLoading}>
        {t('remindMeLater')}
      </button>
    </div>
  );
};

export { PushNotificationsModal };
