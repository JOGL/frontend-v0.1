import { Content, EditorProvider } from '@tiptap/react';
import React, { FC } from 'react';
import { getDefaultExtensions } from '../editor.utils';
import { EditorWrapperStyled } from '../editor.styles';
import { AdvancedEditorTopBar } from './advancedEditorTopBar/advancedEditorTopBar';

interface Props {
  content?: Content;
  defaultPlaceholder?: string;
  handleChange: (content: string) => void;
}

export const AdvancedEditor: FC<Props> = ({ content, defaultPlaceholder = '', handleChange }) => {
  return (
    /* Placeholder is sent here because the Placeholder extension "placeholder" attribute crashes crowdin if set to a translatable string  */
    <EditorWrapperStyled placeholder={defaultPlaceholder}>
      <EditorProvider
        content={content}
        slotBefore={<AdvancedEditorTopBar />}
        extensions={getDefaultExtensions()}
        onUpdate={({ editor }) => handleChange(editor.getHTML())}
      />
    </EditorWrapperStyled>
  );
};
