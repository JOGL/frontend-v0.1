import styled from 'styled-components';
import { EditorWrapperStyled } from '../editor.styles';

export const ReadOnlyEditorWrapperStyled = styled(EditorWrapperStyled)`
  .tiptap {
    padding: 0;
  }
`;
