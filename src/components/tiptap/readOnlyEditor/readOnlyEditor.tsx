import React, { useEffect } from 'react';
import { useEditor, EditorContent, ReactNodeViewRenderer } from '@tiptap/react';
import Mention from '@tiptap/extension-mention';
import { MentionLink } from '../editorOptions/mentions/mentionsLink/mentionsLink';
import { getDefaultExtensions } from '../editor.utils';
import { ReadOnlyEditorWrapperStyled } from './readOnlyEditor.styles';

interface ReadOnlyEditorProps {
  content: string;
  focus?: boolean;
}

const ReadOnlyEditor: React.FC<ReadOnlyEditorProps> = ({ content, focus = false }) => {
  const editor = useEditor({
    extensions: [
      ...getDefaultExtensions(),
      Mention.extend({
        addNodeView() {
          return ReactNodeViewRenderer(MentionLink);
        },
      }),
    ],
    editable: false,
    immediatelyRender: false,
    content: content,
  });

  useEffect(() => {
    if (!editor) return;
    if (focus === true) {
      editor
        .chain()
        .focus()
        .setContent(content ?? '')
        .run();
    } else {
      editor
        .chain()
        .setContent(content ?? '')
        .run();
    }
  }, [editor, content]);

  if (editor?.isEmpty) {
    return null;
  }

  return (
    <ReadOnlyEditorWrapperStyled>
      <EditorContent editor={editor} />
    </ReadOnlyEditorWrapperStyled>
  );
};

export default ReadOnlyEditor;
