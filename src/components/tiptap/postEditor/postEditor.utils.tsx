import { mergeAttributes, ReactRenderer } from '@tiptap/react';
import tippy from 'tippy.js';
import { MentionItem } from '../editorOptions/mentions/mentions.types';
import { MentionList } from '../editorOptions/mentions/mentionsList/mentionsList';
import { getDefaultExtensions } from '../editor.utils';
import Mention from '@tiptap/extension-mention';
import HardBreak from '@tiptap/extension-hard-break';
import api from '~/src/utils/api/api';
import { UserMiniModel } from '~/__generated__/types';

const fetchMentionsList = async (
  query: string,
  feedId: string,
  everyoneString: string,
  everyoneEnabled: boolean
): Promise<MentionItem[]> => {
  const resultList: MentionItem[] = await api.feed
    .usersDetail(feedId, { Search: query, Page: 1, PageSize: 10 })
    .then((res) =>
      res?.data?.map((user: UserMiniModel) => ({
        id: user.id,
        label: `${user.first_name} ${user.last_name}`,
        imageURL: user.logo_url_sm || '/images/default/default-user.png',
      }))
    );

  if (everyoneEnabled) {
    resultList.push({
      id: 'Everyone',
      label: everyoneString,
      imageURL: '/images/icons/at_symbol_icon.png',
    });
  }

  return resultList;
};

function startsWithNoAccents(str, prefix) {
  const normalized1 = str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
  const normalized2 = prefix.normalize('NFD').replace(/[\u0300-\u036f]/g, '');

  return normalized1.toLowerCase().startsWith(normalized2.toLowerCase());
}

export const mentionsSuggestionConfig = async (feedId: string, everyoneString: string, everyoneEnabled) => {
  const options = await fetchMentionsList('', feedId, everyoneString, everyoneEnabled);
  return {
    char: '@',
    startOfLine: false,
    items: ({ query }) => {
      return options.filter(
        (o) => !query || query === '' || startsWithNoAccents(o.label.toUpperCase(), query.toUpperCase())
      );
    },
    render: () => {
      let component;
      let popup;

      return {
        onStart: (props) => {
          component = new ReactRenderer(MentionList, {
            props,
            editor: props.editor,
          });

          if (!props.clientRect) {
            return;
          }

          popup = tippy('body', {
            getReferenceClientRect: props.clientRect,
            appendTo: () => document.body,
            content: component.element,
            showOnCreate: true,
            interactive: true,
            trigger: 'manual',
            placement: 'bottom-start',
          })[0];
        },
        onUpdate: (props) => {
          component.updateProps(props);

          if (!props.clientRect) {
            return;
          }

          popup?.setProps({
            getReferenceClientRect: props.clientRect,
          });
        },
        onKeyDown(props) {
          if (props.event.key === 'Escape') {
            popup.hide();
            return true;
          }
          return component?.ref?.onKeyDown(props);
        },
        onExit: () => {
          popup?.destroy();
          component?.destroy();
        },
      };
    },
  };
};

const GetHardBreak = (handleSubmit: (content: string) => void, isMobile: boolean) => {
  return HardBreak.extend({
    addKeyboardShortcuts() {
      let isShiftPressed = false;

      // Add keydown listener to track Shift key
      if (typeof window !== 'undefined') {
        window.addEventListener('keydown', (e) => {
          isShiftPressed = e.shiftKey;
        });
        window.addEventListener('keyup', (e) => {
          isShiftPressed = e.shiftKey;
        });
      }

      return {
        'Shift-Enter': ({ editor }) => {
          const isBulletListActive = editor.isActive('bulletList');
          const isOrderedListActive = editor.isActive('orderedList');

          if (isBulletListActive || isOrderedListActive) {
            editor.commands.enter();
            return true;
          }

          editor.commands.setHardBreak();
          return true;
        },
        Enter: ({ editor }) => {
          if (isMobile) {
            return false;
          }

          if (isShiftPressed) {
            return false;
          }

          const isEmpty = !editor.getText().trim();

          if (isEmpty) {
            if (editor.isActive('bulletList') || editor.isActive('orderedList')) {
              editor.commands.liftListItem('listItem');
            }
            return false;
          }

          handleSubmit(editor?.getHTML() ?? '');
          editor.chain().focus().clearContent().run();
          return true;
        },
      };
    },
  }).configure();
};

const GetMention = async (feedId: string, everyoneString: string, everyoneEnabled) => {
  const suggestionConfig = await mentionsSuggestionConfig(feedId, everyoneString, everyoneEnabled);
  return Mention.configure({
    renderHTML({ options, node }) {
      return [
        'a',
        mergeAttributes({ href: `/user/${node.attrs.id}` }, options.HTMLAttributes),
        `${options.suggestion.char}${node.attrs.label ?? node.attrs.id}`,
      ];
    },
    suggestion: suggestionConfig,
  });
};

export const getPostEditorExtensions = async (
  feedId: string,
  everyoneString: string,
  everyoneEnabled: boolean,
  handleSubmit: (content: string) => void,
  isMobile: boolean
) => {
  return [
    ...getDefaultExtensions(),
    GetHardBreak(handleSubmit, isMobile),
    await GetMention(feedId, everyoneString, everyoneEnabled),
  ];
};
