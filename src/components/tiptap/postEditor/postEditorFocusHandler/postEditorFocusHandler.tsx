import { useCurrentEditor } from '@tiptap/react';
import { FC, useEffect } from 'react';
import { useEditorFocusStore } from '~/src/store/editorOnFocusStore/editorOnFocusStore';

export const EditorFocusHandler: FC = () => {
  const { editor } = useCurrentEditor();
  const { setEditorFocused } = useEditorFocusStore();

  useEffect(() => {
    if (!editor) return;

    const onFocus = () => {
      setEditorFocused(true);
    };

    const onBlur = () => {
      setEditorFocused(false);
    };

    editor.on('focus', onFocus);
    editor.on('blur', onBlur);

    // Set initial state
    if (editor.isFocused) {
      setEditorFocused(true);
    }

    return () => {
      editor.off('focus', onFocus);
      editor.off('blur', onBlur);
    };
  }, [editor, setEditorFocused]);

  return null;
};
