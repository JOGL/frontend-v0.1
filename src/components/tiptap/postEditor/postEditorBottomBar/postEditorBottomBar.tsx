import { Dispatch, FC, SetStateAction, useState } from 'react';
import { Button, Flex, Space } from 'antd';
import { Attachments } from '../../editorOptions/attachments/attachments';
import { AttachmentsCarousel } from '~/src/components/Feed/PostsOld/AttachmentsCarousel';
import PostLinksPreviewer from '~/src/components/Feed/PostsOld/PostLinksPreviewer';
import { useCurrentEditor } from '@tiptap/react';
import { isImageFileType, isVideoFileType } from '~/src/utils/utils';
import { SubmitPost } from '../../editorOptions/submitPost/submitPost';
import { ToggleFormatting } from '../../editorOptions/toggleFormatting/toggleFormatting';
import { MentionButton } from '../../editorOptions/mentions/mentionsButton/mentionsButton';
import { DisplayableAttachment } from '~/src/components/Feed/types';
import { EmojiButton } from '../../editorOptions/emojis/emojiButton/emojiButton';
import useTranslation from 'next-translate/useTranslation';
import { useMutation } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import { useSaveDraft } from '../../editorHooks/saveDraft/useSaveDraft';

interface PostEditorBottomBarProps {
  displayableAttachments: DisplayableAttachment[];
  isEditing: boolean;
  isUploading: boolean;
  formattingVisible: boolean;
  draftEntityId?: string;
  saveDraft: boolean;
  handleChangeDoc: (attachment: DisplayableAttachment[]) => void;
  deleteDocument: (attachmentId: string) => Promise<void>;
  handleSubmit: (content: string) => void;
  handleCancel: () => void;
  toggleVisibility: Dispatch<SetStateAction<boolean>>;
}

export const PostEditorBottomBar: FC<PostEditorBottomBarProps> = ({
  displayableAttachments,
  isEditing,
  isUploading,
  formattingVisible,
  draftEntityId,
  saveDraft,
  handleChangeDoc,
  deleteDocument,
  handleSubmit,
  handleCancel,
  toggleVisibility,
}) => {
  const [uploadFileError, setUploadFileError] = useState<string>('');
  const { t } = useTranslation('common');
  const { editor } = useCurrentEditor();

  const savePostOrReplyDraft = useMutation({
    mutationKey: [MUTATION_KEYS.postOrReplyDraftCreate, draftEntityId],
    mutationFn: async (content: string) => {
      if (draftEntityId) {
        await api.feed.draftCreateDetail(draftEntityId, JSON.stringify(content));
      }
    },
  });
  useSaveDraft({ saveDraft: saveDraft, handleSaveDraft: (content: string) => savePostOrReplyDraft.mutate(content) });

  const postHasImagesOrVideos = !!displayableAttachments.find(
    (a) => isImageFileType(a.file_type) || isVideoFileType(a.file_type)
  );

  const handleDeleteDocument = async (attachmentId: string): Promise<void> => {
    setUploadFileError('');
    deleteDocument(attachmentId);
  };

  return (
    <Flex vertical gap="small">
      <Flex align="center" justify="space-between">
        <Flex align="center">
          <Attachments
            displayableAttachments={displayableAttachments}
            uploadFileError={uploadFileError}
            setUploadFileError={setUploadFileError}
            handleChangeDoc={handleChangeDoc}
          />

          <EmojiButton />

          <MentionButton />

          <ToggleFormatting formattingVisible={formattingVisible} toggleVisibility={toggleVisibility} />
        </Flex>

        {isEditing ? (
          <Space>
            <Button size="small" onClick={handleCancel}>
              {t('action.cancel')}
            </Button>
            <Button type="primary" size="small" onClick={() => handleSubmit(editor?.getHTML() ?? '')}>
              {t('action.save')}
            </Button>
          </Space>
        ) : (
          <SubmitPost
            hasValidAttachments={uploadFileError === '' && displayableAttachments?.length > 0}
            isSubmitting={isUploading}
            handleSubmit={handleSubmit}
          />
        )}
      </Flex>

      {/* Display uploaded attachments */}
      <AttachmentsCarousel
        isEditing
        isUploading={isUploading}
        attachments={displayableAttachments}
        onDeleteAttachment={handleDeleteDocument}
      />
      <PostLinksPreviewer postText={editor?.getHTML() ?? ''} postHasImagesOrVideos={postHasImagesOrVideos} />
    </Flex>
  );
};
