import { EditorProvider } from '@tiptap/react';
import React, { FC, useEffect, useState } from 'react';
import { DisplayableAttachment } from '../../Feed/types';
import { PostEditorTopBar } from './postEditorTopBar/postEditorTopBar';
import { PostEditorBottomBar } from './postEditorBottomBar/postEditorBottomBar';
import useTranslation from 'next-translate/useTranslation';
import { EditorWrapperStyled } from '../editor.styles';
import { getPostEditorExtensions } from './postEditor.utils';
import { Grid, Skeleton } from 'antd';
import { EditorFocusHandler } from './postEditorFocusHandler/postEditorFocusHandler';

const { useBreakpoint } = Grid;

interface Props {
  content?: string;
  feedId: string;
  displayableAttachments: DisplayableAttachment[];
  isEditing?: boolean;
  isUploading: boolean;
  canMentionEveryone: boolean;
  draftEntityId?: string;
  saveDraft?: boolean;
  handleChangeDoc?: (attachment: DisplayableAttachment[]) => void;
  deleteDocument: (attachmentId: string) => Promise<void>;
  handleCancel?: () => void;
  handleSubmit: (content: string) => void;
}

export const PostEditor: FC<Props> = ({
  content,
  feedId,
  displayableAttachments,
  isEditing = false,
  isUploading,
  canMentionEveryone,
  draftEntityId,
  saveDraft = false,
  handleChangeDoc = () => {},
  deleteDocument,
  handleCancel = () => {},
  handleSubmit,
}) => {
  const [formattingVisible, setFormattingVisible] = useState<boolean>(false);
  const { t } = useTranslation('common');
  const [extensions, setExtensions] = useState<any>();
  const screens = useBreakpoint();
  const isMobile = screens.xs;

  const initializeExtensions = async function (isMobile: boolean) {
    const fullExtensions = await getPostEditorExtensions(
      feedId,
      t('everyone'),
      canMentionEveryone,
      handleSubmit,
      isMobile
    );
    setExtensions(fullExtensions);
  };

  useEffect(() => {
    if (isMobile !== undefined) {
      initializeExtensions(isMobile);
    }
  }, [feedId, handleSubmit, isMobile]);

  if (!extensions) return <Skeleton title={false} />;

  return (
    <EditorWrapperStyled placeholder={t('writeMessage')}>
      <EditorProvider
        content={content}
        immediatelyRender={false}
        slotBefore={<PostEditorTopBar formattingVisible={formattingVisible} />}
        slotAfter={
          <PostEditorBottomBar
            displayableAttachments={displayableAttachments}
            isEditing={isEditing}
            isUploading={isUploading}
            formattingVisible={formattingVisible}
            draftEntityId={draftEntityId}
            saveDraft={saveDraft}
            handleChangeDoc={handleChangeDoc}
            deleteDocument={deleteDocument}
            handleSubmit={handleSubmit}
            handleCancel={handleCancel}
            toggleVisibility={setFormattingVisible}
          />
        }
        extensions={extensions}
      >
        <EditorFocusHandler />
      </EditorProvider>
    </EditorWrapperStyled>
  );
};
