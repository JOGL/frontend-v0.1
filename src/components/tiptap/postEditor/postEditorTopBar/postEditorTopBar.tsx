import { Divider, Flex } from 'antd';
import { FC } from 'react';
import { Bold } from '../../editorOptions/bold/bold';
import { Italic } from '../../editorOptions/italic/italic';
import { Underline } from '../../editorOptions/underline/underline';
import { FontSize } from '../../editorOptions/fontSize/fontSize';
import { OrderedList } from '../../editorOptions/orderedList/orderedList';
import { BulletedList } from '../../editorOptions/bulletedList/bulletedList';
import { Blockquote } from '../../editorOptions/blockquote/blockquote';
import { Code } from '../../editorOptions/code/code';
import { Codeblock } from '../../editorOptions/codeblock/codeblock';
import { DefaultMath } from '../../editorOptions/math/defaultMath/defaultMath';
import { Link } from '../../editorOptions/link/link';

interface PostEditorTopBarProps {
  formattingVisible: boolean;
}

export const PostEditorTopBar: FC<PostEditorTopBarProps> = ({ formattingVisible }) => {
  if (!formattingVisible) {
    return null;
  }

  return (
    <Flex align="center" wrap gap="small">
      <Bold />
      <Italic />
      <Underline />
      <Divider type="vertical" />
      <FontSize />
      <Divider type="vertical" />
      <Link />
      <Divider type="vertical" />
      <OrderedList />
      <BulletedList />
      <Divider type="vertical" />
      <Blockquote />
      <Divider type="vertical" />
      <Code />
      <Codeblock />
      <Divider type="vertical" />
      <DefaultMath />
    </Flex>
  );
};
