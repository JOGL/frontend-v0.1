import { BgColorsOutlined, CaretDownOutlined } from '@ant-design/icons';
import { useCurrentEditor } from '@tiptap/react';
import { Button, ColorPicker, Flex, theme, Typography } from 'antd';
import { Color } from 'antd/es/color-picker';
import useTranslation from 'next-translate/useTranslation';
import { FC, useState } from 'react';

export const BackgroundColorPicker: FC = () => {
  const { t } = useTranslation('common');
  const { editor } = useCurrentEditor();
  const { token } = theme.useToken();
  const [color, setColor] = useState<string>(token.colorBgBase);

  if (!editor) {
    return null;
  }

  const onColorChange = (color: Color) => {
    setColor(color.toHexString());
    editor.chain().focus().toggleHighlight({ color: color.toHexString() }).run();
  };

  return (
    <ColorPicker
      onChange={onColorChange}
      value={color}
      panelRender={(panel) => (
        /* onMouseDown stops the default behavior, otherwise the editor would register the click too and the cursor would be moving around */
        <Flex vertical onMouseDown={(e) => e.preventDefault()}>
          <Typography.Text strong>{t('backgroundColor')}</Typography.Text>
          {panel}
        </Flex>
      )}
    >
      <Button type="text" size="small">
        <BgColorsOutlined />
        <CaretDownOutlined />
      </Button>
    </ColorPicker>
  );
};
