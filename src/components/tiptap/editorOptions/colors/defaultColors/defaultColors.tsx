import { FC } from 'react';
import { Button, Dropdown, Flex } from 'antd';
import { CaretDownOutlined, FontColorsOutlined } from '@ant-design/icons';
import { TextColorPicker } from '../colorPickers/textColor/textColorPicker';
import { BackgroundColorPicker } from '../colorPickers/backgroundColor/backgroundColorPicker';

export const DefaultColors: FC = () => {
  return (
    <Dropdown
      dropdownRender={() => (
        <Flex className="ant-dropdown-menu">
          <TextColorPicker />
          <BackgroundColorPicker />
        </Flex>
      )}
      trigger={['click']}
    >
      <Button type="text" size="small">
        <FontColorsOutlined />
        <CaretDownOutlined />
      </Button>
    </Dropdown>
  );
};
