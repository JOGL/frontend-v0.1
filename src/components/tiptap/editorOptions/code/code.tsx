import { useCurrentEditor } from '@tiptap/react';
import { FC } from 'react';
import { EditorButtonStyled } from '../editorOptions.styles';
import { CodeIcon } from '~/src/assets/icons/codeIcon';

export const Code: FC = () => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  return (
    <EditorButtonStyled
      type="text"
      size="small"
      onClick={() => editor.chain().focus().toggleCode().run()}
      isActive={editor.isActive('code')}
      icon={<CodeIcon />}
    />
  );
};
