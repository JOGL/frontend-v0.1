import { UnderlineOutlined } from '@ant-design/icons';
import { useCurrentEditor } from '@tiptap/react';
import { FC } from 'react';
import { EditorButtonStyled } from '../editorOptions.styles';

export const Underline: FC = () => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  return (
    <EditorButtonStyled
      type="text"
      size="small"
      onClick={() => editor.chain().focus().toggleUnderline().run()}
      isActive={editor.isActive('underline')}
      icon={<UnderlineOutlined />}
    />
  );
};
