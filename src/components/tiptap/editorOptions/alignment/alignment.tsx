import { FC } from 'react';
import { Button, Dropdown } from 'antd';
import { AlignCenterOutlined, AlignLeftOutlined, AlignRightOutlined, CaretDownOutlined } from '@ant-design/icons';
import { useCurrentEditor } from '@tiptap/react';
import { ItemType } from 'antd/es/menu/interface';

export const Alignment: FC = () => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  const alignmentMenuItems: ItemType[] = [
    {
      key: '1',
      onClick: () => editor.chain().focus().setTextAlign('left').run(),
      icon: <AlignLeftOutlined />,
    },
    {
      key: '2',
      onClick: () => editor.chain().focus().setTextAlign('center').run(),
      icon: <AlignCenterOutlined />,
    },
    {
      key: '3',
      onClick: () => editor.chain().focus().setTextAlign('right').run(),
      icon: <AlignRightOutlined />,
    },
  ];

  return (
    <Dropdown
      menu={{
        items: alignmentMenuItems,
        selectable: true,
        defaultSelectedKeys: ['1'],
      }}
      trigger={['click']}
    >
      <Button type="text" size="small">
        <AlignLeftOutlined />
        <CaretDownOutlined />
      </Button>
    </Dropdown>
  );
};
