import { UnorderedListOutlined } from '@ant-design/icons';
import { useCurrentEditor } from '@tiptap/react';
import { FC } from 'react';
import { EditorButtonStyled } from '../editorOptions.styles';

export const BulletedList: FC = () => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  const handleBulletedListClick = () => {
    if (editor.isActive('bulletList')) {
      editor.chain().focus().toggleBulletList().run();
      return;
    }

    const endPosition = editor.state.doc.content.size;
    editor.chain().focus().setTextSelection(endPosition).run();

    if (endPosition > 0) {
      editor.chain().focus().setHardBreak().run();
    }

    editor
      .chain()
      .focus()
      .insertContent({
        type: 'bulletList',
        content: [
          {
            type: 'listItem',
            content: [
              {
                type: 'paragraph',
              },
            ],
          },
        ],
      })
      .run();

    editor.chain().focus().run();
  };

  return (
    <EditorButtonStyled
      type="text"
      size="small"
      onClick={handleBulletedListClick}
      isActive={editor.isActive('bulletList')}
      icon={<UnorderedListOutlined />}
    />
  );
};
