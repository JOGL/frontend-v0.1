import { LinkOutlined } from '@ant-design/icons';
import { FC, useState } from 'react';
import { EditorButtonStyled } from '../editorOptions.styles';
import { LinkModal } from './linkModal/linkModal';

export const Link: FC = () => {
  const [modalVisible, setModalVisible] = useState<boolean>(false);

  const handleClick = () => {
    setModalVisible(true);
  };

  const handleClose = () => {
    setModalVisible(false);
  };

  return (
    <>
      <EditorButtonStyled
        type="text"
        size="small"
        onClick={handleClick}
        isActive={modalVisible}
        icon={<LinkOutlined />}
      />
      {modalVisible && <LinkModal onClose={handleClose} />}
    </>
  );
};
