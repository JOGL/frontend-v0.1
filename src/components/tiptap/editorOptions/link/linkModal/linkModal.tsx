import { useCurrentEditor } from '@tiptap/react';
import { Form, Input, Modal } from 'antd';
import { useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';

interface LinkModalProps {
  onClose: () => void;
}

export const LinkModal: React.FC<LinkModalProps> = ({ onClose }) => {
  const { editor } = useCurrentEditor();
  const { t } = useTranslation('common');
  const [form] = Form.useForm();
  const [isOkButtonDisabled, setIsOkButtonDisabled] = useState<boolean>(true);
  const text = Form.useWatch('text', form);
  const url = Form.useWatch('url', form);

  useEffect(() => {
    form
      .validateFields({ validateOnly: true })
      .then(() => setIsOkButtonDisabled(false))
      .catch(() => setIsOkButtonDisabled(true));
  }, [form, url]);

  if (!editor) {
    return null;
  }

  const { from, to } = editor.state.selection;
  const initialText = editor.state.doc.textBetween(from, to, '') || '';
  const linkAttrs = editor.getAttributes('link');

  const handleOk = () => {
    const { from, to } = editor.state.selection;
    const selectedText = editor.state.doc.textBetween(from, to, '');

    editor
      .chain()
      .focus()
      .deleteSelection()
      .insertContent([
        {
          type: 'text',
          marks: [{ type: 'link', attrs: { href: url } }],
          text: text || selectedText || url,
        },
        { type: 'text', text: ' ' }, // Add a space after the link
      ])
      .run();

    onClose();
  };

  return (
    <Modal
      title={t('addLink')}
      open
      onOk={handleOk}
      onCancel={onClose}
      okButtonProps={{ disabled: isOkButtonDisabled }}
    >
      <Form form={form} layout="vertical" initialValues={{ text: initialText, url: linkAttrs.href ?? '' }}>
        <Form.Item name="text" label={t('text.one')}>
          <Input />
        </Form.Item>
        <Form.Item
          name="url"
          label={t('link.one')}
          rules={[
            { required: true, message: t('error.valueIsMissing') },
            { type: 'url', message: t('error.invalidLink') },
          ]}
        >
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  );
};
