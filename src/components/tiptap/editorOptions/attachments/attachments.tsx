import useTranslation from 'next-translate/useTranslation';
import { Dispatch, FC, SetStateAction } from 'react';
import { DisplayableAttachment, isLocalAttachment, mapFileToLocalAttachment } from '~/src/components/Feed/types';
import { useApi } from '~/src/contexts/apiContext';
import { MAX_PAYLOAD_SIZE } from '~/src/utils/constants';
import { Flex, Typography, Upload } from 'antd';
import { EditorButtonStyled } from '../editorOptions.styles';
import { PaperClipOutlined } from '@ant-design/icons';
import { RcFile } from 'antd/es/upload';

interface AttachmentsProps {
  displayableAttachments: DisplayableAttachment[];
  uploadFileError: string;
  setUploadFileError: Dispatch<SetStateAction<string>>;
  handleChangeDoc?: (attachment: DisplayableAttachment[]) => void;
}

export const Attachments: FC<AttachmentsProps> = ({
  displayableAttachments,
  uploadFileError,
  setUploadFileError,
  handleChangeDoc,
}) => {
  const { t } = useTranslation('common');
  const api = useApi();

  const hasValidPayloadSize = (files: { size: number }[]): boolean => {
    const payloadSize = files.reduce((acc, f) => f.size + acc, 0);

    if (payloadSize > MAX_PAYLOAD_SIZE) {
      setUploadFileError(t('error.maxPayloadSizeExceeded'));
      return false;
    }
    return true;
  };

  const attachDocuments = async (newDocuments: RcFile[]) => {
    try {
      const files = newDocuments || [];

      const allFiles = (files as { size: number }[]).concat(displayableAttachments.filter(isLocalAttachment));
      if (!hasValidPayloadSize(allFiles)) {
        setUploadFileError(t('error.maxPayloadSizeExceeded'));
        return;
      }

      const attachments = await Promise.all<DisplayableAttachment>(
        files.map((file) => mapFileToLocalAttachment(file, api))
      );

      setUploadFileError('');
      handleChangeDoc && handleChangeDoc([...displayableAttachments, ...attachments]);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <Flex gap="small" wrap>
      <Upload
        beforeUpload={(_, fileList) => {
          attachDocuments(fileList);
          return false;
        }}
        multiple
        itemRender={() => null}
      >
        <EditorButtonStyled type="text" size="small" isActive={false} icon={<PaperClipOutlined />} />
      </Upload>

      {uploadFileError && <Typography.Text type="danger">{uploadFileError}</Typography.Text>}
    </Flex>
  );
};
