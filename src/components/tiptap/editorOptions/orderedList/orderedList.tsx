import { OrderedListOutlined } from '@ant-design/icons';
import { useCurrentEditor } from '@tiptap/react';
import { FC } from 'react';
import { EditorButtonStyled } from '../editorOptions.styles';

export const OrderedList: FC = () => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  const handleOrderedListClick = () => {
    if (editor.isActive('orderedList')) {
      editor.chain().focus().toggleOrderedList().run();
      return;
    }

    const endPosition = editor.state.doc.content.size;
    editor.chain().focus().setTextSelection(endPosition).run();

    if (endPosition > 0) {
      editor.chain().focus().setHardBreak().run();
    }

    editor
      .chain()
      .focus()
      .insertContent({
        type: 'orderedList',
        content: [
          {
            type: 'listItem',
            content: [
              {
                type: 'paragraph',
              },
            ],
          },
        ],
      })
      .run();

    editor.chain().focus().run();
  };

  return (
    <EditorButtonStyled
      type="text"
      size="small"
      onClick={handleOrderedListClick}
      isActive={editor.isActive('orderedList')}
      icon={<OrderedListOutlined />}
    />
  );
};
