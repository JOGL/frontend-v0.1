import { FC } from 'react';

export const OrderedListButton: FC<{
  onClick: () => void;
  isActive: boolean;
}> = ({ onClick, isActive }) => {
  return (
    <button onClick={onClick} className={`editor-button ${isActive ? 'active' : ''}`}>
      Ordered List
    </button>
  );
};
