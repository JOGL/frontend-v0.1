import { FC } from 'react';
import { Button, Dropdown } from 'antd';
import { CaretDownOutlined, FontSizeOutlined } from '@ant-design/icons';
import { useCurrentEditor } from '@tiptap/react';
import { ItemType } from 'antd/es/menu/interface';
import useTranslation from 'next-translate/useTranslation';

export const FontSize: FC = () => {
  const { editor } = useCurrentEditor();
  const { t } = useTranslation('common');

  if (!editor) {
    return null;
  }

  const fontSizeMenuItems: ItemType[] = [
    {
      key: '1',
      label: t('small'),
      onClick: () => editor.chain().focus().setFontSize('12px').run(),
      style: { fontSize: 12 },
    },
    {
      key: '2',
      label: t('normal'),
      onClick: () => editor.chain().focus().setFontSize('14px').run(),
      style: { fontSize: 14 },
    },
    {
      key: '3',
      label: t('large'),
      onClick: () => editor.chain().focus().setFontSize('18px').run(),
      style: { fontSize: 18 },
    },

    {
      key: '4',
      label: t('huge'),
      onClick: () => editor.chain().focus().setFontSize('22px').run(),
      style: { fontSize: 22 },
    },
  ];

  return (
    <Dropdown
      menu={{
        items: fontSizeMenuItems,
        selectable: true,
        defaultSelectedKeys: ['2'],
      }}
      trigger={['click']}
    >
      <Button type="text" size="small">
        <FontSizeOutlined />
        <CaretDownOutlined />
      </Button>
    </Dropdown>
  );
};
