import { useCurrentEditor } from '@tiptap/react';
import { FC } from 'react';
import { Button } from 'antd';
import { SmileOutlined } from '@ant-design/icons';

export const EmojiButton: FC = () => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  return (
    <Button
      type="text"
      size="small"
      onClick={() => editor.chain().focus().insertContent(':').run()}
      icon={<SmileOutlined />}
    />
  );
};
