import React, { FC } from 'react';
import { ListStyled } from '../../editorOptions.styles';
import { EmojiItem } from '@tiptap-pro/extension-emoji';
import { Button, Flex } from 'antd';

interface EmojiListProps {
  items: EmojiItem[];
  command: (emoji: EmojiItem) => void;
}

export const EmojiList: FC<EmojiListProps> = ({ items, command }) => {
  return (
    <ListStyled
      bordered
      size="small"
      grid={{
        gutter: 0,
        column: items.length > 8 ? 8 : items.length,
      }}
      loading={items?.length === 0}
      dataSource={items}
      renderItem={(item: EmojiItem) => (
        <Flex justify="center" align="center">
          <Button type="text" size="small" onClick={() => command(item)}>
            {item.emoji}
          </Button>
        </Flex>
      )}
    />
  );
};

export default EmojiList;
