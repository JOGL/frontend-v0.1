import useTranslation from 'next-translate/useTranslation';
import { MAX_PAYLOAD_SIZE } from '~/src/utils/constants';
import { Flex, Upload, message } from 'antd';
import { EditorButtonStyled } from '../editorOptions.styles';
import { PaperClipOutlined } from '@ant-design/icons';
import { RcFile } from 'antd/es/upload';
import { useCurrentEditor } from '@tiptap/react';

export const InlineImage = () => {
  const { t } = useTranslation('common');
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  const hasValidPayloadSize = (file: RcFile): boolean => {
    if (file.size > MAX_PAYLOAD_SIZE) {
      message.error(t('error.maxPayloadSizeExceeded'));
      return false;
    }
    return true;
  };

  const beforeUpload = (file: RcFile): boolean => {
    if (hasValidPayloadSize(file)) {
      const reader = new FileReader();
      reader.onload = (e) => {
        const base64 = e.target?.result as string;

        const img = new Image();
        img.onload = () => {
          const maxWidth = Math.min(img.naturalWidth, 500);

          editor
            .chain()
            .focus()
            .setImage({
              src: base64,
            })
            .insertContent({ type: 'paragraph' })
            .run();
        };
        img.src = base64;
      };
      reader.readAsDataURL(file);
    }
    return false;
  };

  return (
    <Flex gap="small" wrap>
      <Upload beforeUpload={beforeUpload} itemRender={() => null}>
        <EditorButtonStyled type="text" size="small" isActive={false} icon={<PaperClipOutlined />} />
      </Upload>
    </Flex>
  );
};
