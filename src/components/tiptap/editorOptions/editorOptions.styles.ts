import { Theme } from '@emotion/react';
import styled from '@emotion/styled';
import { Button } from 'antd';
import { List } from 'antd';

interface EditorButtonStyledProps {
  isActive: boolean;
}

export const EditorButtonStyled = styled(Button)<EditorButtonStyledProps>`
  background: ${({ theme, isActive }: { theme: Theme } & EditorButtonStyledProps) =>
    isActive ? theme.token.neutral5 : 'unset'};
`;

export const ListStyled = styled(List)`
  max-height: 200px;
  background-color: ${({ theme }) => theme.token.colorBgElevated};
  overflow-y: auto;
  overflow-x: hidden;
`;
