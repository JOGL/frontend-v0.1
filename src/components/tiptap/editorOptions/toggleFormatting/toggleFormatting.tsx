import { useCurrentEditor } from '@tiptap/react';
import { Dispatch, FC, SetStateAction } from 'react';
import { Button } from 'antd';
import { TextEditorFormattingVisibleIcon } from '~/src/assets/icons/textEditorFormattingVisibleIcon';
import { TextEditorFormattingHiddenIcon } from '~/src/assets/icons/textEditorFormattingHiddenIcon';

interface ToggleFormattingProps {
  formattingVisible: boolean;
  toggleVisibility: Dispatch<SetStateAction<boolean>>;
}

export const ToggleFormatting: FC<ToggleFormattingProps> = ({ formattingVisible, toggleVisibility }) => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  return (
    <Button
      type="text"
      size="small"
      onClick={() => toggleVisibility(!formattingVisible)}
      icon={formattingVisible ? <TextEditorFormattingVisibleIcon /> : <TextEditorFormattingHiddenIcon />}
    />
  );
};
