import { NodeViewWrapper } from '@tiptap/react';
import Link from 'next/link';

export const MentionLink = ({ node }) => {
  return (
    <NodeViewWrapper>
      <Link href={`/user/${node.attrs.id}`}>@{node.attrs.label}</Link>
    </NodeViewWrapper>
  );
};
