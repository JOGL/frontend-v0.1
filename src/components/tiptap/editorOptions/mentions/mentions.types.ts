export interface MentionItem {
  id: string;
  label: string;
  imageURL: string;
}
