import { useCurrentEditor } from '@tiptap/react';
import { FC } from 'react';
import { Button } from 'antd';
import { AtIcon } from '~/src/assets/icons/atIcon';

export const MentionButton: FC = () => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  return (
    <Button
      type="text"
      size="small"
      onClick={() => editor.chain().focus().insertContent('@').run()}
      icon={<AtIcon />}
    />
  );
};
