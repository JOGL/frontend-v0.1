import styled from '@emotion/styled';
import { List } from 'antd';

export const ListItemStyled = styled(List.Item)`
  cursor: pointer;

  &:hover {
    background-color: ${({ theme }) => theme.token.controlItemBgHover};
  }
`;
