import { FC } from 'react';
import { Flex, Typography } from 'antd';
import Image from 'next/image';
import { MentionItem } from '../mentions.types';
import { ListStyled } from '../../editorOptions.styles';
import { ListItemStyled } from './mentionsList.styles';

interface MentionListProps {
  items: MentionItem[];
  command: (item: MentionItem) => void;
}

export const MentionList: FC<MentionListProps> = ({ items, command }) => {
  return (
    <ListStyled
      bordered
      size="small"
      dataSource={items}
      renderItem={(item: MentionItem) => (
        <ListItemStyled onClick={() => command(item)}>
          <Flex gap="small">
            <Image src={item.imageURL} width={20} height={20} alt={`${item.label}-logo`} />
            <Typography.Text>{item.label}</Typography.Text>
          </Flex>
        </ListItemStyled>
      )}
    />
  );
};
