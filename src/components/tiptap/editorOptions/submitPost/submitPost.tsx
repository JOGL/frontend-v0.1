import { useCurrentEditor } from '@tiptap/react';
import { SendOutlined } from '@ant-design/icons';
import { FC } from 'react';
import { ButtonStyled } from './submitPost.styles';

interface SubmitPostProps {
  isSubmitting: boolean;
  hasValidAttachments?: boolean;
  handleSubmit: (content: string) => void;
}

export const SubmitPost: FC<SubmitPostProps> = ({ isSubmitting, hasValidAttachments, handleSubmit }) => {
  const { editor } = useCurrentEditor();

  const handleSubmitClick = () => {
    handleSubmit(editor?.getHTML() ?? '');
    if (editor) {
      editor.chain().focus().clearContent().run();
      editor.commands.blur();
    }
  };

  return (
    <ButtonStyled
      type="primary"
      ghost
      disabled={isSubmitting || (!hasValidAttachments && editor?.isEmpty)}
      onClick={() => handleSubmitClick()}
      icon={<SendOutlined />}
    />
  );
};
