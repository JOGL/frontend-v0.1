import styled from '@emotion/styled';
import { Button } from 'antd';

export const ButtonStyled = styled(Button)`
  border: 0;
`;
