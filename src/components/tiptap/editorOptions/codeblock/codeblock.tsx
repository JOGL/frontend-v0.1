import { useCurrentEditor } from '@tiptap/react';
import { FC } from 'react';
import { EditorButtonStyled } from '../editorOptions.styles';
import { CodeblockIcon } from '~/src/assets/icons/codeblockIcon';

export const Codeblock: FC = () => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  return (
    <EditorButtonStyled
      type="text"
      size="small"
      onClick={() => editor.chain().focus().toggleCodeBlock().run()}
      isActive={editor.isActive('codeBlock')}
      icon={<CodeblockIcon />}
    />
  );
};
