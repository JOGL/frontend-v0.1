import { BoldOutlined } from '@ant-design/icons';
import { useCurrentEditor } from '@tiptap/react';
import { FC } from 'react';
import { EditorButtonStyled } from '../editorOptions.styles';

export const Bold: FC = () => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  return (
    <EditorButtonStyled
      type="text"
      size="small"
      onClick={() => editor.chain().focus().toggleBold().run()}
      isActive={editor.isActive('bold')}
      icon={<BoldOutlined />}
    />
  );
};
