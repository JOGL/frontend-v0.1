import { CaretDownOutlined } from '@ant-design/icons';
import { useCurrentEditor } from '@tiptap/react';
import { Button, Dropdown } from 'antd';
import { FC } from 'react';
import { DefaultMathMenu } from './defaultMathMenu/defaultMathMenu';
import { PiIcon } from '~/src/assets/icons/piIcon';

export const DefaultMath: FC = () => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  return (
    <Dropdown dropdownRender={() => <DefaultMathMenu />} trigger={['click']}>
      <Button type="text" size="small">
        <PiIcon />
        <CaretDownOutlined />
      </Button>
    </Dropdown>
  );
};
