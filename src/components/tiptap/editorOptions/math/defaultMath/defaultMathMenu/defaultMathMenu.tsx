import { FC } from 'react';
import { Exponentiation } from '../../operations/exponentiation/exponentiation';
import { Addition } from '../../operations/addition/addition';
import { Summation } from '../../operations/summation/summation';
import { Product } from '../../operations/product/product';
import { Subtraction } from '../../operations/subtraction/subtraction';
import { Subscript } from '../../operations/subscript/subscript';
import { FlexRowStyled, FlexStyled } from './defaultMathMenu.styles';
import { AdvancedButton } from '../../advancedButton/advancedButton';

export const DefaultMathMenu: FC = () => {
  return (
    <FlexStyled vertical>
      <FlexRowStyled>
        <Exponentiation />
        <Addition />
        <Summation />
      </FlexRowStyled>
      <FlexRowStyled>
        <Subscript />
        <Subtraction />
        <Product />
      </FlexRowStyled>
      <AdvancedButton />
    </FlexStyled>
  );
};
