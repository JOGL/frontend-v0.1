import styled from '@emotion/styled';
import { Flex } from 'antd';

export const FlexStyled = styled(Flex)`
  background-color: ${({ theme }) => theme.token.colorBgElevated};
  border: 1px solid ${({ theme }) => theme.token.colorBorder};
  border-radius: ${({ theme }) => theme.token.borderRadiusSM}px;

  > :nth-child(even) {
    border-block: 1px solid ${({ theme }) => theme.token.colorBorder};
  }
`;

export const FlexRowStyled = styled(Flex)`
  > :nth-child(even) {
    border-inline: 1px solid ${({ theme }) => theme.token.colorBorder};
  }
`;
