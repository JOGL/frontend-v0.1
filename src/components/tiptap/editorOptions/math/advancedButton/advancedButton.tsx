import { FC } from 'react';
import Link from 'next/link';
import { ButtonStyled } from './advancedButton.styles';
import useTranslation from 'next-translate/useTranslation';

export const AdvancedButton: FC = () => {
  const { t } = useTranslation('common');

  return (
    <Link
      href="https://jogldoc.notion.site/Maths-formulas-a2048d61f70742e095907d10dd09012b?pvs=25"
      target="_blank"
      rel="noopener noreferrer"
    >
      <ButtonStyled size="small" type="link">
        {t('advanced')}
      </ButtonStyled>
    </Link>
  );
};
