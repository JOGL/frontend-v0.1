import { useCurrentEditor } from '@tiptap/react';
import { FC } from 'react';
import { OperationButtonStyled } from '../operations.styles';
import { SummationIcon } from '~/src/assets/icons/summationIcon';

export const Summation: FC = () => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  return (
    <OperationButtonStyled
      type="text"
      size="small"
      onClick={() => editor.chain().focus().insertContent('$\\sum_{i=1}^n $').run()}
      icon={<SummationIcon />}
    />
  );
};
