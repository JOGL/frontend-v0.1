import { useCurrentEditor } from '@tiptap/react';
import { FC } from 'react';
import { OperationButtonStyled } from '../operations.styles';
import { ExponentiationIcon } from '~/src/assets/icons/exponentiationIcon';

export const Exponentiation: FC = () => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  return (
    <OperationButtonStyled
      type="text"
      size="small"
      onClick={() => editor.chain().focus().insertContent('$ x^n $').run()}
      icon={<ExponentiationIcon />}
    />
  );
};
