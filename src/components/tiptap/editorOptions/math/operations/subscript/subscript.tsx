import { useCurrentEditor } from '@tiptap/react';
import { FC } from 'react';
import { OperationButtonStyled } from '../operations.styles';
import { SubscriptIcon } from '~/src/assets/icons/subscriptIcon';

export const Subscript: FC = () => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  return (
    <OperationButtonStyled
      type="text"
      size="small"
      onClick={() => editor.chain().focus().insertContent('$ x_n $').run()}
      icon={<SubscriptIcon />}
    />
  );
};
