import { MinusOutlined } from '@ant-design/icons';
import { useCurrentEditor } from '@tiptap/react';
import { FC } from 'react';
import { OperationButtonStyled } from '../operations.styles';

export const Subtraction: FC = () => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  return (
    <OperationButtonStyled
      type="text"
      size="small"
      onClick={() => editor.chain().focus().insertContent('$ x - y $').run()}
      icon={<MinusOutlined />}
    />
  );
};
