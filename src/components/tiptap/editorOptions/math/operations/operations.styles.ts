import styled from '@emotion/styled';
import { Button } from 'antd';

export const OperationButtonStyled = styled(Button)`
  border: none;
  border-radius: 0;
`;
