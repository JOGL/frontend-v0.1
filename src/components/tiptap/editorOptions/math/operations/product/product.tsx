import { useCurrentEditor } from '@tiptap/react';
import { FC } from 'react';
import { OperationButtonStyled } from '../operations.styles';
import { ProductNotationIcon } from '~/src/assets/icons/productNotationIcon';

export const Product: FC = () => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  return (
    <OperationButtonStyled
      type="text"
      size="small"
      onClick={() => editor.chain().focus().insertContent('$\\prod_{i=1}^n x_i$').run()}
      icon={<ProductNotationIcon />}
    />
  );
};
