import { useCurrentEditor } from '@tiptap/react';
import { FC } from 'react';
import { EditorButtonStyled } from '../editorOptions.styles';
import { BlockquoteIcon } from '~/src/assets/icons/blockquoteIcon';

export const Blockquote: FC = () => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  return (
    <EditorButtonStyled
      type="text"
      size="small"
      onClick={() => editor.chain().focus().toggleBlockquote().run()}
      isActive={editor.isActive('blockquote')}
      icon={<BlockquoteIcon />}
    />
  );
};
