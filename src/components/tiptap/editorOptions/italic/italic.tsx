import { ItalicOutlined } from '@ant-design/icons';
import { useCurrentEditor } from '@tiptap/react';
import { FC } from 'react';
import { EditorButtonStyled } from '../editorOptions.styles';

export const Italic: FC = () => {
  const { editor } = useCurrentEditor();

  if (!editor) {
    return null;
  }

  return (
    <EditorButtonStyled
      type="text"
      size="small"
      onClick={() => editor.chain().focus().toggleItalic().run()}
      isActive={editor.isActive('italic')}
      icon={<ItalicOutlined />}
    />
  );
};
