import styled from '@emotion/styled';

interface Props {
  placeholder?: string;
}

export const EditorWrapperStyled = styled.div<Props>`
  height:100%;
  display: flex;
  flex-direction: column;


  div:has(.tiptap) {
    overflow-y: auto;
  }

  .tiptap {
    padding: ${({ theme }) => theme.token.paddingXS}px;

    p.is-editor-empty:first-child::before {
      color: ${({ theme }) => theme.token.neutral6};
      content: "${({ placeholder }: Props) => placeholder}";
      float: left;
      height: 0;
      pointer-events: none;
    }
  }
  
  p {
    margin: 0;
  }

  blockquote {
    padding-left: ${({ theme }) => theme.token.paddingSM}px;
    border-left: 4px solid ${({ theme }) => theme.token.colorBorder};
  }

  code {
    background-color: ${({ theme }) => theme.token.colorBgSpotlight};
    border-radius: ${({ theme }) => theme.token.borderRadiusSM}px;
    padding: ${({ theme }) => theme.token.paddingXXS}px;
    color: ${({ theme }) => theme.token.orange5};
  }

  pre {
    background: ${({ theme }) => theme.token.colorBgSpotlight};
    color: ${({ theme }) => theme.token.colorTextLightSolid};
    font-family: ${({ theme }) => theme.token.fontFamilyCode};
    padding: ${({ theme }) => theme.token.paddingSM}px;
    border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;

    code {
      color: inherit;
      background: none;
      padding: 0;
    }
  }
`;
