import { ReactRenderer } from '@tiptap/react';
import { Color } from '@tiptap/extension-color';
import StarterKit from '@tiptap/starter-kit';
import ListItem from '@tiptap/extension-list-item';
import TextStyle from '@tiptap/extension-text-style';
import Mathematics from '@tiptap-pro/extension-mathematics';
import tippy from 'tippy.js';
import Link from '@tiptap/extension-link';
import Underline from '@tiptap/extension-underline';
import { FontSize } from './customExtensions/fontSize/fontSize';
import Placeholder from '@tiptap/extension-placeholder';
import Emoji, { emojis } from '@tiptap-pro/extension-emoji';
import EmojiList from './editorOptions/emojis/emojiList/emojiList';
import TextAlign from '@tiptap/extension-text-align';
import Highlight from '@tiptap/extension-highlight';
import Dropcursor from '@tiptap/extension-dropcursor';
import ResizableImageExtension from './customExtensions/imageExtensions/resizableImage/resizableImage';
import { ResizableYouTube } from './customExtensions/youtubeExtensions/resizableYoutube/resizableYoutube';
import { YouTubePasteRule } from './customExtensions/youtubeExtensions/youTubePasteRule';
import { ImagePasteRule } from './customExtensions/imageExtensions/imagePasteRule';

const emojisSuggestionConfig = {
  items: ({ editor, query }) => {
    return (
      editor.storage.emoji.emojis
        //Filter emojis based on user query and return a maximum of 32 at a time
        .filter(({ shortcodes, tags }) => {
          return (
            shortcodes.find((shortcode) => shortcode.startsWith(query.toLowerCase())) ||
            tags.find((tag) => tag.startsWith(query.toLowerCase()))
          );
        })
        .slice(0, 32)
    );
  },

  allowSpaces: false,

  render: () => {
    let component;
    let popup;

    return {
      onStart: (props) => {
        component = new ReactRenderer(EmojiList, {
          props,
          editor: props.editor,
        });

        popup = tippy('body', {
          getReferenceClientRect: props.clientRect,
          appendTo: () => document.body,
          content: component.element,
          showOnCreate: true,
          interactive: true,
          trigger: 'manual',
          placement: 'bottom-start',
        });
      },

      onUpdate(props) {
        component.updateProps(props);

        popup[0].setProps({
          getReferenceClientRect: props.clientRect,
        });
      },

      onKeyDown(props) {
        if (props.event.key === 'Escape') {
          popup[0].hide();
          component.destroy();

          return true;
        }

        return component.ref?.onKeyDown(props);
      },

      onExit() {
        popup[0].destroy();
        component.destroy();
      },
    };
  },
};

export const getDefaultExtensions = () => {
  return [
    StarterKit.configure({
      bulletList: {
        keepMarks: true,
        keepAttributes: true,
      },
      orderedList: {
        keepMarks: true,
        keepAttributes: true,
      },
    }),
    Color.configure({ types: [TextStyle.name, ListItem.name] }),
    Mathematics.configure({
      katexOptions: {
        throwOnError: false,
      },
    }),
    Link,
    Underline,
    TextStyle,
    FontSize.configure({
      defaultSize: '14px',
    }),
    Placeholder,
    Emoji.configure({
      emojis: emojis,
      enableEmoticons: true,
      suggestion: emojisSuggestionConfig,
    }),
    TextAlign.configure({
      types: ['heading', 'paragraph'],
    }),
    Highlight.configure({
      multicolor: true,
    }),
    Dropcursor,
    ResizableImageExtension.configure({
      inline: true,
      allowBase64: true,
    }),
    ImagePasteRule,
    ResizableYouTube.configure({
      HTMLAttributes: {
        class: 'youtube-embed',
      },
    }),
    YouTubePasteRule,
  ];
};
