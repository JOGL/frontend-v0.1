import { Extension } from '@tiptap/core';
import { Plugin, PluginKey } from '@tiptap/pm/state';

export const ImagePasteRule = Extension.create({
  name: 'imagePasteRule',

  addProseMirrorPlugins() {
    return [
      new Plugin({
        key: new PluginKey('imagePasteRule'),
        props: {
          handlePaste: (view, event) => {
            const items = Array.from(event.clipboardData?.items || []);
            const imageItems = items.filter((item) => item.type.indexOf('image') === 0);

            if (imageItems.length === 0) return false;

            event.preventDefault();

            imageItems.forEach((item) => {
              const blob = item.getAsFile();
              if (!blob) return;

              const reader = new FileReader();
              reader.onload = (e) => {
                const base64 = e.target?.result as string;

                const { tr } = view.state;
                const pos = view.state.selection.from;

                tr.replaceWith(pos, pos, view.state.schema.nodes.image.create({ src: base64 }));

                view.dispatch(tr);
              };
              reader.readAsDataURL(blob);
            });

            return true;
          },
        },
      }),
    ];
  },
});
