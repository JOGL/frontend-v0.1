import { NodeViewWrapper, NodeViewProps, ReactNodeViewRenderer } from '@tiptap/react';
import { CSSProperties, useRef, useState } from 'react';
import Image from '@tiptap/extension-image';
import { DragCornerStyled, ImageContainerStyled } from './resizableImage.styles';

const MIN_WIDTH = 60;

const DIRECTIONS = {
  NW: 'nw',
  NE: 'ne',
  SW: 'sw',
  SE: 'se',
};

const ResizableImageTemplate = ({ node, updateAttributes, selected }: NodeViewProps) => {
  const imgRef = useRef<HTMLImageElement>(null);
  const [resizingStyle, setResizingStyle] = useState<Pick<CSSProperties, 'width'> | undefined>();

  const handleMouseDown = (event: React.MouseEvent, direction: string) => {
    if (!imgRef.current) return;

    event.preventDefault();
    const initialXPosition = event.clientX;
    const currentWidth = imgRef.current.width;
    let newWidth = currentWidth;

    const removeListeners = () => {
      window.removeEventListener('mousemove', mouseMoveHandler);
      window.removeEventListener('mouseup', removeListeners);
      updateAttributes({ width: newWidth });
      setResizingStyle(undefined);
    };

    const mouseMoveHandler = (event: MouseEvent) => {
      const delta = event.clientX - initialXPosition;
      newWidth = Math.max(currentWidth + (direction.includes('w') ? -delta : delta), MIN_WIDTH);
      setResizingStyle({ width: newWidth });

      if (!event.buttons) removeListeners();
    };

    window.addEventListener('mousemove', mouseMoveHandler);
    window.addEventListener('mouseup', removeListeners);
  };

  return (
    <NodeViewWrapper>
      <ImageContainerStyled selected={selected}>
        <img
          {...node.attrs}
          ref={imgRef}
          style={{
            width: resizingStyle?.width,
            maxWidth: '100%',
            height: 'auto',
          }}
        />
        {selected && (
          <>
            <DragCornerStyled
              direction={DIRECTIONS.NW}
              top
              left
              onMouseDown={(e) => handleMouseDown(e, DIRECTIONS.NW)}
            />
            <DragCornerStyled direction={DIRECTIONS.NE} top onMouseDown={(e) => handleMouseDown(e, DIRECTIONS.NE)} />
            <DragCornerStyled direction={DIRECTIONS.SW} left onMouseDown={(e) => handleMouseDown(e, DIRECTIONS.SW)} />
            <DragCornerStyled direction={DIRECTIONS.SE} onMouseDown={(e) => handleMouseDown(e, DIRECTIONS.SE)} />
          </>
        )}
      </ImageContainerStyled>
    </NodeViewWrapper>
  );
};

const ResizableImageExtension = Image.extend({
  addAttributes() {
    return {
      ...this.parent?.(),
      width: {
        default: null,
        renderHTML: ({ width }) => ({ width }),
      },
      height: {
        default: null,
        renderHTML: ({ height }) => ({ height }),
      },
    };
  },
  addNodeView() {
    return ReactNodeViewRenderer(ResizableImageTemplate);
  },
});

export default ResizableImageExtension;
