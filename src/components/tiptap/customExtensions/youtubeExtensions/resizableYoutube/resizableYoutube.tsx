import { Node, mergeAttributes } from '@tiptap/core';
import { NodeViewWrapper, NodeViewProps, ReactNodeViewRenderer } from '@tiptap/react';
import { CSSProperties, useRef, useState } from 'react';
import { DragCornerStyled, YouTubeContainerStyled } from './resizableYoutube.styles';

const MIN_WIDTH = 320;
const MIN_HEIGHT = 180;

const DIRECTIONS = {
  NW: 'nw',
  NE: 'ne',
  SW: 'sw',
  SE: 'se',
};

const getYoutubeId = (url: string) => {
  const match = url.match(/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/\s]{11})/);
  return match?.[1] || '';
};

const ResizableYouTubeTemplate = ({ node, updateAttributes, selected }: NodeViewProps) => {
  const containerRef = useRef<HTMLDivElement>(null);
  const [resizingStyle, setResizingStyle] = useState<Pick<CSSProperties, 'width' | 'height'> | undefined>();

  const { url, width = 560, height = 315 } = node.attrs;
  const youtubeId = getYoutubeId(url);

  const handleMouseDown = (event: React.MouseEvent, direction: string) => {
    if (!containerRef.current) return;

    event.preventDefault();
    const initialXPosition = event.clientX;
    const initialYPosition = event.clientY;
    const currentWidth = containerRef.current.offsetWidth;
    const currentHeight = containerRef.current.offsetHeight;
    const aspectRatio = currentWidth / currentHeight;

    let newWidth = currentWidth;
    let newHeight = currentHeight;

    const removeListeners = () => {
      window.removeEventListener('mousemove', mouseMoveHandler);
      window.removeEventListener('mouseup', removeListeners);
      updateAttributes({ width: newWidth, height: newHeight });
      setResizingStyle(undefined);
    };

    const mouseMoveHandler = (event: MouseEvent) => {
      const deltaX = event.clientX - initialXPosition;
      newWidth = Math.max(currentWidth + deltaX, MIN_WIDTH);
      newHeight = Math.round(newWidth / aspectRatio);

      if (newHeight < MIN_HEIGHT) {
        newHeight = MIN_HEIGHT;
        newWidth = Math.round(newHeight * aspectRatio);
      }

      setResizingStyle({ width: newWidth, height: newHeight });

      if (!event.buttons) removeListeners();
    };

    window.addEventListener('mousemove', mouseMoveHandler);
    window.addEventListener('mouseup', removeListeners);
  };

  return (
    <NodeViewWrapper>
      <YouTubeContainerStyled ref={containerRef} selected={selected}>
        <iframe
          width={resizingStyle?.width || width}
          height={resizingStyle?.height || height}
          src={`https://www.youtube.com/embed/${youtubeId}`}
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        />
        {selected && (
          <>
            <DragCornerStyled direction={DIRECTIONS.SE} onMouseDown={(e) => handleMouseDown(e, DIRECTIONS.SE)} />
          </>
        )}
      </YouTubeContainerStyled>
    </NodeViewWrapper>
  );
};

export const ResizableYouTube = Node.create({
  name: 'youtube',
  group: 'block',
  atom: true,

  addAttributes() {
    return {
      url: {
        default: null,
        parseHTML: (element) => element.getAttribute('data-url'),
        renderHTML: (attributes) => ({
          'data-url': attributes.url,
        }),
      },
      width: {
        default: 560,
        parseHTML: (element) => element.getAttribute('width'),
        renderHTML: (attributes) => ({
          width: attributes.width,
        }),
      },
      height: {
        default: 315,
        parseHTML: (element) => element.getAttribute('height'),
        renderHTML: (attributes) => ({
          height: attributes.height,
        }),
      },
    };
  },

  parseHTML() {
    return [
      {
        tag: 'div[data-youtube-embed]',
        getAttrs: (dom) => {
          if (!(dom instanceof HTMLElement)) return false;
          return {
            url: dom.getAttribute('data-url'),
            width: dom.getAttribute('width'),
            height: dom.getAttribute('height'),
          };
        },
      },
    ];
  },

  renderHTML({ HTMLAttributes }) {
    return [
      'div',
      {
        'data-youtube-embed': '',
        ...mergeAttributes(HTMLAttributes),
      },
    ];
  },

  addNodeView() {
    return ReactNodeViewRenderer(ResizableYouTubeTemplate);
  },
});
