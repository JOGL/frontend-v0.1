import styled from '@emotion/styled';

interface DragCornerProps {
  direction: string;
  top?: boolean;
  left?: boolean;
}

export const DragCornerStyled = styled.div<DragCornerProps>`
  position: absolute;
  width: 12px;
  height: 12px;
  background-color: ${({ theme }) => theme.token.colorPrimary};
  border-radius: 3px;
  cursor: nw-resize;
  top: ${(props) => (props.top ? '-6px' : 'auto')};
  bottom: ${(props) => (!props.top ? '-6px' : 'auto')};
  left: ${(props) => (props.left ? '-6px' : 'auto')};
  right: ${(props) => (!props.left ? '-6px' : 'auto')};
`;

export const YouTubeContainerStyled = styled.div<{ selected?: boolean }>`
  position: relative;
  display: inline-block;
  line-height: 0;
  &:hover ${DragCornerStyled} {
    display: block;
  }
  outline-offset: 2px;
  outline: ${(props) => (props.selected ? `1px solid  ${props.theme.token.colorPrimary}` : 'none')};
`;
