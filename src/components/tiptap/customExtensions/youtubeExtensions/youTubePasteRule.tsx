import { Extension } from '@tiptap/core';
import { Plugin, PluginKey } from '@tiptap/pm/state';

const getYoutubeId = (url: string) => {
  const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
  const match = url.match(regExp);
  return match?.[2] || '';
};

const isValidYoutubeUrl = (url: string): boolean => {
  const youtubeId = getYoutubeId(url);
  return youtubeId.length === 11;
};

export const YouTubePasteRule = Extension.create({
  name: 'youtubePasteRule',

  addProseMirrorPlugins() {
    return [
      new Plugin({
        key: new PluginKey('youtubePasteRule'),
        props: {
          handlePaste: (view, event) => {
            const text = event.clipboardData?.getData('text/plain');

            if (!text || !isValidYoutubeUrl(text)) return false;

            const { tr } = view.state;
            const pos = view.state.selection.from;

            tr.replaceWith(pos, pos, view.state.schema.nodes.youtube.create({ url: text }));

            view.dispatch(tr);
            return true;
          },
        },
      }),
    ];
  },
});
