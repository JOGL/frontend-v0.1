import { useCurrentEditor } from '@tiptap/react';
import { useEffect } from 'react';

interface Props {
  saveDraft: boolean;
  handleSaveDraft: (content: string) => void;
}

export const useSaveDraft = ({ saveDraft, handleSaveDraft }: Props) => {
  const { editor } = useCurrentEditor();

  const triggerSave = () => {
    if (editor && saveDraft) {
      handleSaveDraft(!editor?.isEmpty ? editor.getHTML() : '');
    }
  };

  useEffect(() => {
    if (saveDraft) {
      window.addEventListener('beforeunload', triggerSave);

      return () => {
        window.removeEventListener('beforeunload', triggerSave);
        triggerSave();
      };
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [saveDraft]);
};
