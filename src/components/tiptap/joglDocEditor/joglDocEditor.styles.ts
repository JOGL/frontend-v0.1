import styled from '@emotion/styled';
import { EditorWrapperStyled } from '../editor.styles';

export const JoglEditorWrapperStyled = styled(EditorWrapperStyled)`
  border: 1px solid ${({ theme }) => theme.token.colorBorder};
  border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;
  min-height: calc(100vh - 330px);

  .tiptap {
    padding: ${({ theme }) => theme.token.paddingSM}px;
  }

  img {
    max-width: 100%;
  }

  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    iframe[src*='youtube.com'],
    iframe[src*='youtu.be'] {
      aspect-ratio: 16/9;
      width: 100%;
      max-width: 100%;
      height: auto;
      border: none;
    }
  }
`;
