import { Content, EditorProvider, Editor } from '@tiptap/react';
import React, { FC, useCallback } from 'react';
import { getDefaultExtensions } from '../editor.utils';
import { JoglDocEditorTopBar } from './joglDocEditorTopBar/joglDocEditorTopBar';
import { JoglEditorWrapperStyled } from './joglDocEditor.styles';

interface Props {
  content?: Content;
  defaultPlaceholder?: string;
  handleChange: (content: string) => void;
}

export const JoglDocEditor: FC<Props> = ({ content, defaultPlaceholder = '', handleChange }) => {
  const onUpdate = useCallback(
    ({ editor }: { editor: Editor }) => {
      const html = editor.getHTML();
      handleChange(html);
    },
    [handleChange]
  );

  return (
    <JoglEditorWrapperStyled placeholder={defaultPlaceholder}>
      <EditorProvider
        content={content}
        slotBefore={<JoglDocEditorTopBar />}
        extensions={getDefaultExtensions()}
        onUpdate={onUpdate}
        editorProps={{
          handlePaste: (view, event) => {
            const text = event.clipboardData?.getData('text/plain');
            if (!text) return false;

            // Check if it's a YouTube URL
            const youtubeRegex = /^(https?:\/\/)?(www\.)?(youtube\.com|youtu\.be)\/.+$/;
            if (youtubeRegex.test(text)) {
              // Insert YouTube embed
              view.dispatch(view.state.tr.replaceSelectionWith(view.state.schema.nodes.youtube.create({ url: text })));
              return true;
            }
            return false;
          },
        }}
      />
    </JoglEditorWrapperStyled>
  );
};
