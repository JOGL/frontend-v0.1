import { Divider, Flex } from 'antd';
import { Bold } from '../../editorOptions/bold/bold';
import { Italic } from '../../editorOptions/italic/italic';
import { Underline } from '../../editorOptions/underline/underline';
import { FontSize } from '../../editorOptions/fontSize/fontSize';
import { OrderedList } from '../../editorOptions/orderedList/orderedList';
import { BulletedList } from '../../editorOptions/bulletedList/bulletedList';
import { Blockquote } from '../../editorOptions/blockquote/blockquote';
import { Code } from '../../editorOptions/code/code';
import { Codeblock } from '../../editorOptions/codeblock/codeblock';
import { DefaultMath } from '../../editorOptions/math/defaultMath/defaultMath';
import { Link } from '../../editorOptions/link/link';
import { DefaultColors } from '../../editorOptions/colors/defaultColors/defaultColors';
import { Alignment } from '../../editorOptions/alignment/alignment';
import { EmojiButton } from '../../editorOptions/emojis/emojiButton/emojiButton';
import { InlineImage } from '../../editorOptions/inlineImage/inlineImage';
import { ContainerStyled, EditorTopBarStyled } from './joglDocEditorTopBar.styles';

export const JoglDocEditorTopBar = () => {
  return (
    <EditorTopBarStyled align="center" wrap gap="small" justify="space-between">
      <ContainerStyled>
        <Bold />
        <Italic />
        <Underline />
        <Divider type="vertical" />
        <DefaultColors />
        <Divider type="vertical" />
        <FontSize />
        <Divider type="vertical" />
        <Link />
        <Divider type="vertical" />
        <Alignment />
        <Divider type="vertical" />
        <OrderedList />
        <BulletedList />
        <Divider type="vertical" />
        <Blockquote />
        <Divider type="vertical" />
        <Code />
        <Codeblock />
        <Divider type="vertical" />
        <DefaultMath />
      </ContainerStyled>
      <Flex>
        <InlineImage />
        <EmojiButton />
      </Flex>
    </EditorTopBarStyled>
  );
};
