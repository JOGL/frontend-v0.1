import styled from '@emotion/styled';
import { Flex } from 'antd';

export const EditorTopBarStyled = styled(Flex)`
  padding: ${({ theme }) => theme.token.paddingSM}px;
  border-bottom: 1px solid ${({ theme }) => theme.token.colorBorder};
`;

export const ContainerStyled = styled(Flex)`
  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    display: inline;
  }
`;
