import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import { FeedEntityFilter, SortKey } from '~/__generated__/types';

const useUsers = (): [
  (id: string) => void,
  (id: string) => string,
  (key: string) => string
] => {
  const router = useRouter();
  const { t } = useTranslation('common');

  const goToUser = (id: string) => {
    router.push(getUserUrl(id));
  };

  const getUserUrl = (id: string) => {
    return `/user/${id}`;
  };

  const getUserViewTitle = (key: string): string=> {
    switch (key) {
      case 'joined':
        return  t('user.joined');
      case 'all':
      default:
        return t('user.all') ;
    }
  };

  return [goToUser, getUserUrl, getUserViewTitle];
};

export const getUserViewFilter = (key: string): { sortKey: SortKey; filter?: FeedEntityFilter} => {
  switch (key) {
    case 'joined':
      return { sortKey: SortKey.Date };
    case 'all':
    default:
      return { sortKey: SortKey.Alphabetical };
  }
};

export default useUsers;
