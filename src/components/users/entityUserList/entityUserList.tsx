import { Flex, Typography, Input, Button } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useState } from 'react';
import useDebounce from '~/src/hooks/useDebounce';
import { useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import { SortKey } from '~/__generated__/types';
import UserTable from '../userTable/userTable';
import { FlexFullWidthStyled } from '../../Common/common.styles';
import UserInvite from '../userInviteModal/userInviteModal';

interface Props {
  entityId: string;
  canManageUsers: boolean;
}
const EntityUserList = ({ entityId, canManageUsers }: Props) => {
  const [search, setSearch] = useState('');
  const debouncedSearch = useDebounce(search, 300);

  const { data, isLoading, refetch: refetch } = useQuery({
    queryKey: [QUERY_KEYS.entityMemberList, entityId, debouncedSearch],
    queryFn: async () => {
      const response = await api.communityEntities.membersDetail(entityId, {
        Search: debouncedSearch,
        SortKey: SortKey.Alphabetical,
        SortAscending: true,
      });
      return response.data;
    },
  });

  const { t } = useTranslation('common');
  const [showInviteModal, setShowInviteModal] = useState(false);

  return (
    <FlexFullWidthStyled vertical gap="middle">
      <Typography.Title level={3}>{t('user.other')}</Typography.Title>
      <Flex justify="stretch" gap="large">
        <Input.Search
          placeholder={t('action.searchForUsers')}
          onChange={(e) => {
            setSearch(e.target.value);
          }}
          onSearch={(value) => {
            setSearch(value);
          }}
        />
        {canManageUsers && <Button onClick={() => setShowInviteModal(true)}>{t('action.add')}</Button>}
        {showInviteModal && (
          <UserInvite containerId={entityId} refetch={refetch} handleClose={() => setShowInviteModal(false)} />
        )}
      </Flex>
      <UserTable hideColumns={['spaces']} loading={isLoading} users={data ?? []} />
    </FlexFullWidthStyled>
  );
};

export default EntityUserList;
