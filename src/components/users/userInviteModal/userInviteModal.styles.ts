import styled from '@emotion/styled';
import { Divider, Flex, Typography } from 'antd';
import { ScrollbarStyled } from '~/src/assets/styles/Global.styled';

export const UsersScrollbarStyled = styled(ScrollbarStyled)`
  margin: 0 -${({ theme }) => theme.token.paddingXS}px;
`;

export const FlexStyled = styled(Flex)`
  max-height: 155px;
  overflow-y: auto;
  padding: 0 ${({ theme }) => theme.token.paddingXS}px;
`;

export const DividerStyled = styled(Divider)`
  margin: ${({ theme }) => theme.token.paddingXS}px 0 0 0;
`;

export const ParagraphStyled = styled(Typography.Text)`
  margin-bottom: 0;
`;
