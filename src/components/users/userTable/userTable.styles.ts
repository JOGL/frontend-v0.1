import { Flex } from "antd";
import { styled } from "twin.macro";

export const FlexStyled = styled(Flex)`
  flex-wrap: wrap;
`;