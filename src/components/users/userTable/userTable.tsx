import useTranslation from 'next-translate/useTranslation';
import { CommunityEntityMiniModel, UserMiniModel, UserModel } from '~/__generated__/types';
import { AlignType } from 'rc-table/lib/interface';
import { TableStyled } from '../../Common/table.styles';
import Loader from '../../Common/loader/loader';
import useUsers from '../useUsers';
import { ContainerLink } from '../../Common/links/containerLink/containerLink';
import { UserLink } from '../../Common/links/userLink/userLink';
import { FlexStyled } from './userTable.styles';
import { Typography } from 'antd';

interface Props {
  users: UserMiniModel[];
  loading: boolean;
  hideColumns: string[];
}

const UserTable = ({ users, loading = false, hideColumns = [] }: Props) => {
  const { t } = useTranslation('common');
  const [goToUser] = useUsers();

  const columns = [
    {
      dataIndex: 'last_name',
      title: t('name'),
      width: 220,
      ellipsis: true,
      render: (_: string, user: UserMiniModel) => {
        return <UserLink user={user} />;
      },
    },
    {
      dataIndex: 'id',
      title: t('organization.one'),
      width: 200,
      ellipsis: true,
      render: (id: string, item: UserMiniModel) => {
        return <Typography.Text>{[...item.experience.filter(e=>e.current).map((e) => e.company), ...item.education.filter(e=>e.current).map((e) => e.school)].join(', ')}</Typography.Text>;
      },
    },
    {
      dataIndex: 'short_bio',
      title: t('headline'),
      width: 200,
      ellipsis: true,
      render: (short_bio: string) => {
        return <Typography.Text>{short_bio}</Typography.Text>;
      },
    },
    {
      dataIndex: 'spaces',
      title: t('space.other'),
      align: 'left' as AlignType,
      render: (spaces: CommunityEntityMiniModel[], item: UserMiniModel) => {
        return (
          <FlexStyled gap="small">
            {spaces.map((container) => (
              <ContainerLink key={container.id} container={container} />
            ))}
          </FlexStyled>
        );
      },
    },
  ];

  if (loading === true) return <Loader />;

  return (
    <TableStyled
      //rowClassName={(record: UserMiniModel) => (record.is_new ? 'new' : '')}
      dataSource={users}
      columns={columns.filter((col) => !hideColumns.find((hideCol) => col.dataIndex === hideCol))}
      pagination={false}
      onRow={(record: UserMiniModel) => ({
        onClick: () => {
          goToUser(record.id);
        },
      })}
    />
  );
};

export default UserTable;
