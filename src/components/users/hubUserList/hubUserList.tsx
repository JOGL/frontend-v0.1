import { useInfiniteQuery, useQuery } from '@tanstack/react-query';
import { Flex, Input, Typography } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useMemo, useState } from 'react';
import { SortKey, UserMiniModel } from '~/__generated__/types';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import useDebounce from '~/src/hooks/useDebounce';
import { ContainerFilter } from '../../Common/filters/containerFilter/containerFilter';
import UserTable from '../userTable/userTable';
const PAGE_SIZE = 50;

interface Props {
  hubId: string;
  title: string;
  sortKey?: SortKey;
}

const HubUserList = ({ hubId, title, sortKey = SortKey.Updateddate }: Props) => {
  const { t } = useTranslation('common');
  const [search, setSearch] = useState('');
  const debouncedSearch = useDebounce(search, 300);
  const [selectedContainers, setSelectedContainers] = useState<string[]>([]);

  const { data: userData, isLoading } = useInfiniteQuery({
    queryKey: [QUERY_KEYS.papersAggregate, hubId, debouncedSearch, selectedContainers, sortKey],
    queryFn: async ({ pageParam = 1 }) => {
      const response = await api.nodes.usersAggregateDetail(hubId, {
        Search: debouncedSearch,
        communityEntityIds: selectedContainers.join(',') ?? undefined,
        SortKey: sortKey,
        SortAscending: sortKey === SortKey.Alphabetical,
        Page: pageParam,
        PageSize: PAGE_SIZE,
      });
      return response.data;
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItemsCount = allPages.reduce((acc, page) => acc + page.items.length, 0);
      return loadedItemsCount < lastPage.total ? allPages.length + 1 : undefined;
    },
  });

  const { data: containers } = useQuery({
    queryKey: [QUERY_KEYS.usersAggregateContainers, hubId],
    queryFn: async () => {
      const response = await api.nodes.usersAggregateCommunityEntitiesDetail(hubId, {});
      return response.data;
    },
  });

  const flattenedData: UserMiniModel[] = useMemo(() => {
    return userData?.pages.flatMap((page) => page.items) ?? [];
  }, [userData]);

  return (
    <>
      <Flex vertical gap="middle">
        <Typography.Title>{title}</Typography.Title>
        <Flex justify="stretch">
          <Input.Search
            placeholder={t('action.searchForUsers')}
            onChange={(e) => {
              setSearch(e.target.value);
            }}
            onSearch={(value) => {
              setSearch(value);
            }}
          />
        </Flex>
        <Flex justify="stretch">
          {containers && (
            <ContainerFilter
              containers={containers}
              onChange={(communityEntityIds) => setSelectedContainers(communityEntityIds)}
            />
          )}
        </Flex>
        <UserTable users={flattenedData} loading={isLoading} hideColumns={['opened']} />
      </Flex>
    </>
  );
};

export default HubUserList;
