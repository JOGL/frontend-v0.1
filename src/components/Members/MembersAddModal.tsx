// eslint-disable-next-line no-unused-vars
import { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from '~/src/contexts/apiContext';
import { useModal } from '~/src/contexts/modalContext';
import CreatableSelect from 'react-select/creatable';
import Button from '../primitives/Button';
import Alert from '../Tools/Alert/Alert';
import SpinLoader from '../Tools/SpinLoader';
import { ItemType, User } from '~/src/types';
import { copyLink, entityItem } from '~/src/utils/utils';
import { components, ControlProps, Props } from 'react-select';
import Icon from '../primitives/Icon';
import MembersMultiSelect from './MembersMultiSelect';
import useGet from '~/src/hooks/useGet';
import { useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import { CommunityEntityType } from '~/__generated__/types';

interface Props {
  itemType: ItemType;
  itemId: string;
  callBack?: () => void;
  specialTab?: 'reviewers' | 'admins';
  currentMembers?: User[];
}

const MembersAddModal: FC<Props> = ({
  itemType = '',
  itemId,
  callBack = () => console.warn('Missing callback'),
  specialTab,
  currentMembers = undefined,
}) => {
  const [usersList, setUsersList] = useState([]);
  const [emailsList, setEmailsList] = useState([]);
  const [formattedEmailsList, setFormattedEmails] = useState([]);
  const [inviteSend, setInviteSend] = useState(false);
  const [sending, setSending] = useState(false);
  const [error, setError] = useState(false);
  const [selectedRole, setSelectedRole] = useState('member');
  const apiLegacy = useApi();

  const { closeModal } = useModal();
  const { t } = useTranslation('common');
  const objPath = Object.values(entityItem).find((x) => x.back_path === itemType).front_path;

  const { data: containerMembers } = useGet<User[]>(!currentMembers ? `/${itemType}/${itemId}/members` : '');

  const { data: hubDiscussions } = useQuery({
    queryKey: [QUERY_KEYS.feedNodesList],
    queryFn: async () => {
      const response = await api.feed.nodesNewList();
      return response.data;
    },
  });

  const resetState = () => {
    setInviteSend(false);
    setError(false);
  };

  const handleChangeEmail = (content) => {
    const tempEmailsList = [];
    content?.map(function (invitee_mail) {
      if (invitee_mail) {
        let commaSeparatedValues = invitee_mail.value
          .split(',')
          .map((email) => email.trim())
          .filter((email) => email !== '');
        invitee_mail && tempEmailsList.push(...commaSeparatedValues);
      }
    });

    setEmailsList(tempEmailsList);
    setFormattedEmails(
      tempEmailsList.map((email) => {
        return { label: email, value: email };
      })
    );
  };

  const handleChangeUser = (content) => {
    const tempUsersList = [];
    content?.map(function (user) {
      user && tempUsersList.push(user.value);
    });
    setUsersList(tempUsersList);
  };

  const onSuccess = () => {
    setInviteSend(true);
    setSending(false);
    callBack(); // to refresh members list
    setTimeout(() => {
      closeModal(); // close modal after 1,5sec
      resetState();
    }, 1500);
  };

  const onError = () => {
    setError(true);
    setSending(false);
    setTimeout(() => {
      setError(false);
    }, 8000);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setSending(true);
    for (let i = 0; i < usersList?.length ?? 0; i++) {
      apiLegacy
        .post(`/${itemType}/${itemId}/invite/id/${usersList[i]}`, `"${selectedRole}"`)
        .then((res) => {
          onSuccess();
        })
        .catch(() => onError());
    }

    if (emailsList?.length > 0) {
      apiLegacy
        .post(`/${itemType}/${itemId}/invite/email/batch`, emailsList)
        .then((res) => {
          onSuccess();
        })
        .catch(() => onError());
    }
  };

  // show different confirmation message depending on number of users added, and if it's via email or jogl user
  const confMsg =
    usersList.length === 1 // if 1 user has been invited
      ? specialTab
        ? t(`legacy.cfpForm.invite_${specialTab}`)
        : t('userWasInvited', { count: 1 })
      : usersList.length > 1 // if more than 1 user have been invited
      ? specialTab
        ? t(`legacy.cfpForm.invite_${specialTab}_plural`)
        : t('userWasInvited', { count: 2 }) // if we invited people via their email
      : t('invitationSent');

  const ControlEmail = ({ children, ...props }: ControlProps<any, false>) => {
    return (
      <components.Control {...props}>
        <Icon icon="ic:outline-email" tw="h-5 w-5 text-[#5959599e] ml-4 mr-0" />
        {children}
      </components.Control>
    );
  };

  const customEmailStyle = {
    control: (styles) => ({
      ...styles,
      borderRadius: '25px',
    }),
  };

  let channelId = '';
  if (itemType === CommunityEntityType.Node) {
    channelId = hubDiscussions?.find((item) => item.id === itemId)?.home_channel_id || '';
  } else {
    channelId =
      hubDiscussions?.flatMap((hub) => hub.entities)?.find((item) => item.id === itemId)?.home_channel_id || '';
  }

  return (
    <form>
      <div tw="mb-6">
        <label tw="font-bold" htmlFor="joglUser">
          {t('joglUser')}
        </label>
        <MembersMultiSelect
          handleChangeUser={handleChangeUser}
          currentMembers={currentMembers ?? containerMembers}
          setSelectedRole={setSelectedRole}
          autoFocus
        />
      </div>
      <div tw="mb-3">
        <label tw="font-bold" htmlFor="byEmail">
          {t('byEmailAddress')}
        </label>
        <CreatableSelect
          isClearable
          isMulti
          noOptionsMessage={() => null}
          menuShouldScrollIntoView={true} // force scroll into view
          placeholder={t('enterEmails')}
          components={{ DropdownIndicator: null, Control: ControlEmail, IndicatorSeparator: null }}
          formatCreateLabel={(inputValue) => inputValue}
          onChange={handleChangeEmail}
          styles={customEmailStyle}
          value={formattedEmailsList}
          // @TODO check if email is valid before adding it to the list
        />
      </div>
      <div className="text-center btnZone" tw="mb-6">
        {!inviteSend ? (
          <Button onClick={handleSubmit} disabled={(emailsList.length === 0 && usersList.length === 0) || sending}>
            {sending && <SpinLoader />}
            {t('action.invite')}
          </Button>
        ) : (
          <Alert type="success" message={confMsg} />
        )}
      </div>
      {error && (
        <div className="alert alert-danger" role="alert">
          {t('error.generic')}
        </div>
      )}

      <label tw="font-bold" htmlFor="email">
        {t('action.shareLink')}
      </label>
      <div tw="flex space-x-2 relative">
        <div tw="absolute top-0 bg-[#D0D0D0] p-1 rounded-tl-md rounded-bl-md h-[39px] w-[39px] items-center flex justify-center">
          <Icon icon="ph:link" tw="mr-0 h-5 w-5 text-[#5959599e]" />
        </div>
        <input
          type="text"
          name="email"
          id="email"
          tw="flex-1 min-w-0 block w-full px-3 py-[0.4rem] rounded-md border-[#cccccc] pl-10"
          value={`${window.location.origin}/${objPath}/${itemId}?channelId=${channelId}`}
          disabled={true}
        />
        <button
          type="button"
          onClick={() => copyLink(`/${objPath}/${itemId}?channelId=${channelId}`, t)}
          rel="noopener"
          aria-label="Copy link"
        >
          <Icon icon="ic:baseline-content-copy" />
        </button>
      </div>
    </form>
  );
};
export default MembersAddModal;
