import { TabPanels, TabPanel as SpecialTabPanel, Tabs } from '@reach/tabs';
import useTranslation from 'next-translate/useTranslation';
import { useState, FC } from 'react';
import MembersList from '~/src/components/Members/MembersList';
import { ItemType } from '~/src/types';
import { NavTab, TabListStyleNoSticky } from '~/src/components/Tabs/TabsStyles';
import useInfiniteLoading from '~/src/hooks/useInfiniteLoading';

interface Props {
  itemId: string;
  itemType: ItemType;
  isOnboardingActive?: boolean;
  isOwner: boolean;
}
const MembersManage: FC<Props> = ({ itemId, itemType, isOnboardingActive = false, isOwner }) => {
  const { t } = useTranslation('common');
  const [nbOfPendingMembersRequests, setNbOfPendingMembersRequests] = useState(0);
  const [nbOfPendingMembersInvites, setNbOfPendingMembersInvites] = useState(0);

  const membersPerQuery = 50;

  const { data: dataMembers, mutate: membersRevalidate, error, size, setSize } = useInfiniteLoading(
    (index) => `/${itemType}/${itemId}/members?PageSize=${membersPerQuery}&page=${index + 1}&sort=roles`
  );

  const {
    data: pendingDataMembers,
    mutate: pendingMembersRevalidate,
    error: pendingError,
    size: pendingSize,
    setSize: pendingSetSize,
  } = useInfiniteLoading((index) => `/${itemType}/${itemId}/invites?PageSize=${membersPerQuery}&page=${index + 1}`);

  const refresh = () => {
    membersRevalidate();
    pendingMembersRevalidate();
  };

  return (
    <Tabs>
      <TabListStyleNoSticky>
        <NavTab>{t('activeMembers')}</NavTab>
        <NavTab>
          <div tw="inline-flex items-center justify-center">
            {t('outgoingRequests')}
            <span tw="bg-[#E2E2E2] rounded-full w-7 h-7 ml-2">
              <span tw="flex justify-center">{nbOfPendingMembersInvites}</span>
            </span>
          </div>
        </NavTab>
        <NavTab>
          <div tw="inline-flex items-center justify-center">
            {t('incomingRequests')}
            <span tw="bg-[#E2E2E2] rounded-full w-7 h-7 ml-2">
              <span tw="flex justify-center">{nbOfPendingMembersRequests}</span>
            </span>
          </div>
        </NavTab>
      </TabListStyleNoSticky>
      <TabPanels tw="justify-center">
        <SpecialTabPanel>
          {itemId ? (
            <MembersList
              dataMembers={dataMembers}
              membersRevalidate={refresh}
              error={error}
              size={size}
              setSize={setSize}
              itemType={itemType}
              itemId={itemId}
              membersPerQuery={membersPerQuery}
              isOnboardingActive={isOnboardingActive}
              isOwner={isOwner}
            />
          ) : null}
        </SpecialTabPanel>
        {/* Outgoing requests */}
        <SpecialTabPanel>
          {itemId && (
            <MembersList
              dataMembers={pendingDataMembers}
              membersRevalidate={refresh}
              error={pendingError}
              size={pendingSize}
              setSize={pendingSetSize}
              itemType={itemType}
              itemId={itemId}
              type="pendingInvites"
              membersPerQuery={membersPerQuery}
              setNbOfPendingMembersInvites={setNbOfPendingMembersInvites}
              isOnboardingActive={isOnboardingActive}
            />
          )}
        </SpecialTabPanel>
        {/* Incoming requests */}
        <SpecialTabPanel>
          {itemId && (
            <MembersList
              dataMembers={pendingDataMembers}
              membersRevalidate={refresh}
              error={pendingError}
              size={pendingSize}
              setSize={pendingSetSize}
              itemType={itemType}
              itemId={itemId}
              type="pendingRequests"
              membersPerQuery={membersPerQuery}
              setNbOfPendingMembersRequests={setNbOfPendingMembersRequests}
              isOnboardingActive={isOnboardingActive}
            />
          )}
        </SpecialTabPanel>
      </TabPanels>
    </Tabs>
  );
};
export default MembersManage;
