import useTranslation from 'next-translate/useTranslation';
import { useApi } from '~/src/contexts/apiContext';
import AsyncSelect from 'react-select/async';
import Select from 'react-select';
import styled from '~/src/utils/styled';
import { components, ControlProps } from 'react-select';
import Icon from '../primitives/Icon';
import tw from 'twin.macro';

interface Props {
  handleChangeUser?: any;
  content?: any;
  currentMembers?: any;
  autoFocus?: boolean;
  errorCodeMessage?: string;
  customOptions?: any;
  setSelectedRole?: any;
  customPlaceholder?: string;
  showRoleDropdown?: boolean;
  customRoles?: string[];
}

const MembersMultiSelect = ({
  handleChangeUser,
  content,
  currentMembers = [],
  autoFocus = false,
  errorCodeMessage = '',
  customOptions = undefined,
  setSelectedRole,
  customPlaceholder,
  showRoleDropdown = true,
  customRoles = undefined,
}: Props) => {
  const api = useApi();
  const { t } = useTranslation('common');

  const formatOptionLabel = ({ label, logo_url, type: typeProps }) => {
    return (
      <UserLabel tw="flex flex-row items-center">
        <img tw="w-8 h-8 rounded-full" src={logo_url} />
        <div tw="flex flex-col">
          <div css={typeProps ? tw`(text-[12px] font-semibold)!` : tw``}>{label}</div>
          {/* {typeProps && <span tw="text-[#5E5C5C] max-width[fit-content] (text-[12px])!">{typeProps}</span>} */}
        </div>
      </UserLabel>
    );
  };

  const fetchUsers = async (resolve, value) => {
    try {
      let content = (await api.get(`/users?Search=${value}&Page=1&PageSize=10`))?.data?.items ?? [];
      content = content
        // filter users that are already members
        .filter((x) => !currentMembers?.map((m) => m.id).includes(x.id))
        .map((item) => ({
          value: item.id,
          label: `${item.first_name} ${item.last_name}`,
          logo_url: item.logo_url_sm || '/images/default/default-user.png',
        }));
      resolve(content);
    } catch (e) {
      resolve([]);
    }
  };

  const loadOptions = (inputValue) =>
    new Promise((resolve) => {
      fetchUsers(resolve, inputValue);
    });

  const customStyles = {
    container: (provided) => ({
      ...provided,
      width: '100%',
    }),
    multiValue: (provided) => ({
      ...provided,
      // borderRadius: '10px',
      background: 'white',
      boxShadow: '0 3px 10px rgb(0 0 0 / 0.2)',
      borderRadius: '8px',
      padding: '0px',
      margin: '0px',
    }),
    valueContainer: (provided) => ({
      ...provided,
      gap: '4px',
    }),
    multiValueRemove: (provided) => ({
      ...provided,
      background: 'white',
      cursor: 'pointer',
      borderRadius: '8px',
    }),
    control: (provided) => ({
      ...provided,
      // different border style depending if value is empty or not (validation)
      border: errorCodeMessage ? '1px solid #dc3545' : '1px solid lightgrey',
      minHeight: '38px',
      borderRadius: '100px',
    }),
  };

  const ControlUser = ({ children, ...props }: ControlProps<any, false>) => {
    if (!content)
      return (
        <components.Control {...props}>
          <Icon icon="mdi:at" tw="h-5 w-5 text-[#5959599e] ml-4 mr-0" />
          {children}
        </components.Control>
      );
    return <components.Control {...props}>{children}</components.Control>;
  };

  const currentOptions = content?.map((option) => ({
    value: option.id,
    label: option.label,
    logo_url: option.imageUrl,
    ...option,
  }));

  const getRoleOptions = () => {
    const rolesArray = customRoles ?? ['member', 'admin'];
    return rolesArray.map((role) => ({
      value: role,
      label: `as ${t(`legacy.role.${role}`)}`,
    }));
  };

  return (
    <>
      <div tw="flex flex-wrap min-[601px]:flex-nowrap items-center gap-2">
        {customOptions ? (
          <Select
            isMulti
            cacheOptions
            styles={customStyles}
            options={customOptions}
            value={currentOptions}
            onChange={handleChangeUser}
            formatOptionLabel={formatOptionLabel}
            placeholder={customPlaceholder ?? t('selectUsers')}
            noOptionsMessage={() => 'No more members'}
            components={{ DropdownIndicator: null, Control: ControlUser, IndicatorSeparator: null }}
            isClearable
            autoFocus={autoFocus}
          />
        ) : (
          <AsyncSelect
            isMulti
            cacheOptions
            styles={customStyles}
            onChange={handleChangeUser}
            // defaultOptions={true}
            formatOptionLabel={formatOptionLabel}
            placeholder={t('selectUsers')}
            loadOptions={loadOptions}
            noOptionsMessage={() => null}
            components={{ DropdownIndicator: null, Control: ControlUser, IndicatorSeparator: null }}
            isClearable
            // autoFocus={autoFocus}
            autoFocus
            blurInputOnSelect={false} // force keeping focus when selecting option (for mobile)
          />
        )}

        {showRoleDropdown && (
          <div tw="w-[200px]">
            <Select
              options={getRoleOptions()}
              isSearchable={false}
              isDisabled={false}
              menuShouldScrollIntoView={true} // force scroll into view
              defaultValue={{
                value: 'member',
                label: t('asEntity', { entity: t('member', { count: 1 }) }),
              }}
              styles={customStyles}
              onChange={(r) => setSelectedRole(r.value)}
              placeholder={t('action.changeRole')}
            />
          </div>
        )}
      </div>

      {errorCodeMessage && <div className="invalid-feedback">{t(errorCodeMessage)}</div>}
    </>
  );
};

const UserLabel = styled.div`
  div {
    font-size: 15px;
    color: #5c5d5d;
  }
  img {
    width: 28px;
    height: 28px;
    border-radius: 50%;
    object-fit: cover;
    margin-right: 6px;
    flex: none;
  }
`;

export default MembersMultiSelect;
