import { Dispatch, FC, SetStateAction, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import DropdownRole from '../Tools/DropdownRole';
import { useApi } from '~/src/contexts/apiContext';
import Button from '../primitives/Button';
import { useModal } from '~/src/contexts/modalContext';
import ReactTooltip from 'react-tooltip';
import SpinLoader from '../Tools/SpinLoader';
import { Members, ItemType } from '~/src/types';
import tw from 'twin.macro';
import Alert from '../Tools/Alert/Alert';
import Icon from '~/src/components/primitives/Icon';
import { Menu, MenuItem, MenuButton, MenuList } from '@reach/menu-button';
import { confAlert } from '~/src/utils/utils';
import useUser from '~/src/hooks/useUser';
import MembersManageLabels from './MembersManageLabels';
import Link from 'next/link';
import SendEmailToJoglUserModal from '../Tools/SendEmailToJoglUserModal';
import { useRouter } from 'next/router';
import { usePushNotificationStore } from '~/src/store/push-notifications/PushNotificationsStoreProvider';

interface Props {
  member: any;
  itemId: string;
  itemType: ItemType;
  isOwner: boolean;
  callBack: () => void;
  selectedMembers: Members['users'];
  setSelectedMembers: Dispatch<SetStateAction<any>>;
  role: 'pending' | 'member' | 'owner' | 'admin';
  getRole: (member: Members['users']) => void;
  onRoleChanged: (members: Members) => void;
  showChangedMessage: { changing: boolean; ids: number[] };
  isSending: boolean;
  tabType: string;
  isOnboardingActive?: boolean;
}

const MemberCard: FC<Props> = ({
  member,
  itemId,
  itemType,
  isOwner,
  callBack = () => {
    console.warn('Missing callback');
  },
  selectedMembers,
  setSelectedMembers,
  role,
  onRoleChanged,
  showChangedMessage,
  isSending,
  tabType,
  isOnboardingActive,
}) => {
  const api = useApi();
  const router = useRouter();
  const { showPushNotificationModal } = usePushNotificationStore((s) => ({
    showPushNotificationModal: s.showPushNotificationModal,
  }));
  const [sending, setSending] = useState(isSending);
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const { user } = useUser();

  const acceptMember = () => {
    setSending(true);
    api
      .post(`/${itemType}/${itemId}/invites/${member.invitation_id}/accept`)
      .then(() => {
        callBack();
        setSending(false);
        showPushNotificationModal(router);
      })
      .catch((err) => {
        console.error(`Couldn't POST ${itemType} with itemId=${itemId}`, err);
        callBack();
        setSending(false);
      });
  };
  const rejectInvite = (type) => {
    setSending(true);
    api
      .post(`/${itemType}/${itemId}/invites/${member.invitation_id}/${type}`)
      .then(() => {
        callBack();
        setSending(false);
      })
      .catch((err) => {
        console.error(`Couldn't POST ${itemType} with itemId=${itemId}`, err);
        callBack();
        setSending(false);
      });
  };
  const removeMember = () => {
    setSending(true);
    api
      .delete(`/${itemType}/${itemId}/members/${member.id}`)
      .then(() => {
        callBack();
        setSending(false);
        closeModal();
      })
      .catch(() => {
        callBack();
        setSending(false);
        closeModal();
      });
  };

  const resendInvites = () => {
    setSending(true);
    api
      .post(`/${itemType}/invites/${member.invitation_id}/resend`)
      .then(() => {
        callBack();
        setSending(false);
        closeModal();
      })
      .catch(() => {
        callBack();
        setSending(false);
        closeModal();
      });
  };

  const sendMessageToReviewers = (userId) => {
    showModal({
      children: (
        <SendEmailToJoglUserModal
          containerType={itemType}
          containerId={itemId}
          recipients={[member]}
          closeModal={closeModal}
        />
      ),
      title: t('action.sendEmail'),
    });
  };

  const seeOnboardingAnswers = () => {
    api
      .get(`/workspaces/${itemId}/onboardingResponses/${member.id}`)
      .then((res) => {
        showModal({
          children: res.data.items.map((item) => (
            <div key={item.id} tw="mb-4">
              <p tw="font-bold mb-0">{item.question}</p>
              <p>{item.answer}</p>
            </div>
          )),
          title: 'Answers',
        });
      })
      .catch(() => confAlert.fire({ icon: 'error', title: 'No answers for this user' }));
  };

  const handleMembers = (event: { target: HTMLInputElement }) => {
    const id = event.target.id;

    const index = selectedMembers.findIndex((s) => (s?.id !== null ? s?.id === id : s?.invitation_id === id));

    if (index !== -1) {
      setSelectedMembers((members) => members.filter((m) => (m.id !== null ? m.id !== id : m.invitation_id !== id)));
    } else setSelectedMembers((members) => [...members, member]);
  };

  if (member) {
    return (
      <div>
        <div
          tw="flex flex-wrap flex-col gap-2 sm:(flex-row justify-between)"
          css={role !== 'pending' ? tw`justify-start` : tw`justify-between`}
          key={member.id}
        >
          {/* Select box and member name */}
          <div tw="flex items-center pb-3 space-x-3 max-w-[400px] sm:pb-0">
            {/* {itemType !== 'cfps' ? ( */}
            {
              role !== 'owner' ? (
                <input
                  id={member?.id !== null ? member?.id : member?.invitation_id}
                  type="checkbox"
                  tw="hover:cursor-pointer w-[20px]"
                  checked={
                    selectedMembers?.findIndex((m) =>
                      m?.id !== null ? m?.id === member.id : m?.invitation_id === member?.invitation_id
                    ) !== -1
                  }
                  onChange={handleMembers}
                />
              ) : (
                <input type="checkbox" disabled tw="cursor-not-allowed w-[20px]" />
              )
              /* ) : (
              ''
            )} */
            }
            <div tw="h-[50px] w-[50px]">
              <div
                style={{ backgroundImage: `url(${member.logo_url_sm ?? '/images/default/default-user.png'})` }}
                tw="bg-cover bg-center h-[50px] w-[50px] rounded-full border border-solid border-[#ced4da]"
              />
            </div>
            {member?.id ? (
              <Link href={`/user/${member.id}`}>
                {member.first_name} {member.last_name}
              </Link>
            ) : (
              <span>{member?.email}</span>
            )}
            {/* {itemType === 'cfps' && <span tw="italic text-gray-400">({role})</span>} */}
          </div>

          <div tw="inline-flex items-center space-x-4">
            {/* Labels */}
            {itemType === 'cfps' && (
              <div tw="gap-2 flex flex-wrap">
                {member.labels?.map((label, i) =>
                  label === 'lead_applicant' ? (
                    <>
                      <div
                        key={i}
                        tw="px-2 text-sm py-1 rounded-full min-w-[max-content] border border-solid border-gray-200"
                      >
                        {t('containerAdmin', { container: t('workspace.one') })}
                      </div>
                      <div
                        key={i}
                        tw="px-2 text-sm py-1 rounded-full min-w-[max-content] border border-solid border-gray-200"
                      >
                        {t('applicant.one')}
                      </div>
                    </>
                  ) : (
                    <div
                      key={i}
                      tw="px-2 text-sm py-1 rounded-full min-w-[max-content] border border-solid border-gray-200"
                      css={label === 'reviewer' && tw`bg-[#F2F2FC]`}
                    >
                      {label === 'applicant' ? t('applicant.one') : t('reviewer.one')}
                    </div>
                  )
                )}
              </div>
            )}

            {/* Role dropdown */}
            {role === 'pending' && member.invitation_type !== 'invitation' ? (
              // if member role is pending, show buttons to accept or reject the request
              <div tw="flex flex-col space-y-2 xs:(flex-row space-x-2 space-y-0)">
                <Button
                  tw="text-green-500 border-green-500 bg-white hover:(text-white bg-green-500)"
                  disabled={sending}
                  onClick={acceptMember}
                >
                  {sending && <SpinLoader />}
                  {t('action.accept')}
                </Button>
                <Button
                  tw="text-danger border-danger bg-white hover:(text-white bg-danger)"
                  disabled={sending}
                  onClick={() => rejectInvite('reject')}
                >
                  {sending && <SpinLoader />}
                  {t('action.reject')}
                </Button>
              </div>
            ) : member.invitation_type === 'invitation' ? (
              <div tw="inline-flex space-x-4 items-center">
                <Button
                  tw="text-danger border-danger bg-white hover:(text-white bg-danger)"
                  disabled={sending}
                  onClick={() => rejectInvite('cancel')}
                >
                  {sending && <SpinLoader />}
                  {t('action.cancelItem', { item: t('invitation.one') })}
                </Button>
              </div>
            ) : (
              // else show member role dropdown, and more action button
              <div tw="inline-flex w-full items-center sm:w-40">
                <div tw="w-full">
                  <DropdownRole
                    onRoleChanged={onRoleChanged}
                    actualRole={role}
                    callBack={callBack}
                    itemId={itemId}
                    itemType={itemType}
                    // show different list roles depending if user is owner (to prevent admins changing their roles to "owner")
                    listRole={isOwner ? ['owner', 'admin', 'member'] : ['admin', 'member']}
                    member={member}
                    // don't show dropdown of roles if member you want to change is owner (except if you are the owner), to prevent admins changing owner's role.
                    isDisabled={role === 'owner' && !isOwner}
                  />
                </div>
              </div>
            )}

            {/* More actions */}
            <Menu
              // show/hide tooltip on element focus/blur
              onFocus={(e) => ReactTooltip.show(e.target)}
              onBlur={(e) => ReactTooltip.hide(e.target)}
              tabIndex="0"
              data-tip={t('moreActions')}
              data-for="more_actions"
            >
              <MenuButton tw="text-[.8rem] h-[fit-content] text-gray-700 px-1 hover:bg-gray-300 rounded-sm">
                •••
              </MenuButton>
              <MenuList className="post-manage-dropdown">
                {isOnboardingActive && (
                  <MenuItem onSelect={seeOnboardingAnswers}>
                    <Icon icon="ion:eye" tw="w-5 h-5" />
                    {t('action.seeAnswers')}
                  </MenuItem>
                )}
                {itemType === 'cfps' && tabType === 'members' && (
                  <>
                    <MenuItem onSelect={() => sendMessageToReviewers(member.id)}>
                      <Icon icon="fa6-solid:envelope" tw="w-5 h-5" />
                      {t('action.sendEmail')}
                    </MenuItem>
                    <MenuItem
                    // onSelect={() => {
                    //   showModal({
                    //     children: (
                    //       <MembersManageLabels
                    //         itemType={itemType}
                    //         itemId={itemId}
                    //         memberId={member.id}
                    //         currentLabels={member.labels}
                    //         callBack={callBack}
                    //       />
                    //     ),
                    //     title: 'Manage labels',
                    //     maxWidth: '30rem',
                    //   });
                    // }}
                    >
                      {/* Manage labels */}
                      {/* <Icon icon="material-symbols:label-outline" tw="w-5 h-5" /> */}
                      <MembersManageLabels
                        itemType={itemType}
                        itemId={itemId}
                        memberId={member.id}
                        currentLabels={member.labels}
                        callBack={callBack}
                      />
                    </MenuItem>
                  </>
                )}
                {(role !== 'owner' || isOwner) && user?.id !== member.id && role !== 'pending' && (
                  // restriction that prevents 1) admins from removing owners, but allow owners to remove other owners, 2) current user to remove themselves, 3) removing pending members
                  <MenuItem
                    onSelect={() => {
                      showModal({
                        children: (
                          <div tw="inline-flex space-x-3">
                            <Button btnType="danger" onClick={removeMember}>
                              {t('yes')}
                            </Button>
                            <Button onClick={closeModal}>{t('no')}</Button>
                          </div>
                        ),
                        title: 'Are you sure you want to remove this member?',
                        maxWidth: '30rem',
                      });
                    }}
                  >
                    <Icon icon="ic:round-delete" tw="w-5 h-5" />
                    {itemType === 'cfps'
                      ? t('action.remove')
                      : t('action.removeItem', { item: t('member', { count: 1 }) })}
                  </MenuItem>
                )}

                {role === 'pending' && (
                  <MenuItem
                    onSelect={() => {
                      showModal({
                        children: (
                          <div tw="inline-flex space-x-3">
                            <Button btnType="danger" onClick={resendInvites}>
                              {t('yes')}
                            </Button>
                            <Button onClick={closeModal}>{t('no')}</Button>
                          </div>
                        ),
                        title: 'Are you sure you want to resend this invitation?',
                        maxWidth: '30rem',
                      });
                    }}
                  >
                    <Icon icon="ic:round-mail" tw="w-5 h-5" />
                    {itemType === 'cfps'
                      ? t('action.resend')
                      : t('action.resendItem', { item: t('invitation', { count: 1 }) })}
                  </MenuItem>
                )}
              </MenuList>
            </Menu>
            <ReactTooltip id="more_actions" delayHide={300} effect="solid" role="tooltip" />
          </div>
        </div>
        {showChangedMessage?.changing && showChangedMessage.ids.includes(member.id) && (
          <div tw="mt-2">
            <Alert type="success" message={t('userRoleUpdateSuccess')} />
          </div>
        )}
        <hr />
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};

export default MemberCard;
