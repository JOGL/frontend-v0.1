import { FC, useState } from 'react';
import { ItemType } from '~/src/types';
import { useApi } from '~/src/contexts/apiContext';
// import Button from '../primitives/Button';
// import FormDropdownComponent from '../Tools/Forms/FormDropdownComponent';
import useTranslation from 'next-translate/useTranslation';
import { confAlert } from '~/src/utils/utils';
import Icon from '../primitives/Icon';

interface Props {
  itemId: string;
  itemType: ItemType;
  memberId: string;
  currentLabels: string[];
  callBack: () => void;
}

const MembersManageLabels: FC<Props> = ({ itemType, itemId, memberId, currentLabels, callBack }) => {
  const api = useApi();
  const [labels, setLabels] = useState(currentLabels);
  const { t } = useTranslation('common');
  const hasReviewerLabel = labels?.includes('reviewer');

  const changeLabels = () => {
    const newLabels = hasReviewerLabel ? labels.filter((x) => x !== 'reviewer') : [...labels, 'reviewer'];
    setLabels(newLabels);
    api.put(`/${itemType}/${itemId}/members/${memberId}/labels`, newLabels).then((res) => {
      confAlert.fire({
        icon: 'success',
        title: hasReviewerLabel ? t('action.removedReviewerLabel') : t('action.addedReviewerLabel'),
      });
      callBack(); // revalidate members
    });
  };
  return (
    <div onClick={changeLabels}>
      <Icon icon="material-symbols:label-outline" tw="w-5 h-5" />
      {hasReviewerLabel ? t('action.removeReviewerLabel') : t('action.addReviewerLabel')}
    </div>
  );
};

export default MembersManageLabels;
