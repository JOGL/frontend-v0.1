import { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Button, Flex, Modal, message, Typography, Space, AutoComplete, Row, Col, Avatar, Select, Empty } from 'antd';
import { CheckOutlined, LinkOutlined, UserOutlined } from '@ant-design/icons';
import { DividerStyled, FlexStyled, UsersScrollbarStyled } from './membersInvite.styles';
import useDebounce from '~/src/hooks/useDebounce';
import { useMutation, useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import { AccessLevel, InvitationUpsertModel, MemberModel } from '~/__generated__/types';
import EmailTagsSelect from '../../emailTagsSelect/emailTagsSelect';

interface MembersInviteProps {
  members: MemberModel[];
  itemId: string;
  refetch: () => void;
  handleClose: () => void;
}

const MembersInvite: FC<MembersInviteProps> = ({ members, itemId, handleClose, refetch }) => {
  const { t } = useTranslation('common');
  const [userSearch, setUserSearch] = useState('');
  const debouncedSearch = useDebounce(userSearch, 300);
  const [selectedUsers, setSelectedUsers] = useState<MemberModel[]>();
  const [selectedEmails, setSelectedEmails] = useState<string[]>();
  const [hasInvalidEmail, setHesInvalidEmail] = useState(false);

  const { data: users, isFetched } = useQuery({
    queryKey: [QUERY_KEYS.usersSearchAutocompleteList, { search: debouncedSearch }],
    queryFn: async () => {
      const response = await api.users.autocompleteList({
        Search: debouncedSearch,
      });
      return response.data;
    },
    enabled: debouncedSearch.length > 0,
  });

  const { data: invitatonKey } = useQuery({
    queryKey: [QUERY_KEYS.communityEntitiesJoinKey, { id: itemId }],
    queryFn: async () => {
      const response = await api.communityEntities.joinKeyDetail(itemId);
      return response.data;
    },
    enabled: !!itemId,
  });

  const mutation = useMutation({
    mutationFn: async (inviteData: InvitationUpsertModel[]) => {
      await api.communityEntities.inviteBatchCreate(itemId, inviteData);
    },
    onSuccess: () => {
      handleClose();
      message.success(t('invitationSent'));
      refetch();
    },
    onError: () => {
      handleClose();
      message.error(t('error.somethingWentWrong'));
    },
  });

  const handleOk = () => {
    const invitedUsers =
      selectedUsers?.map((user) => {
        return {
          user_id: user.id,
          access_level: user.access_level,
        };
      }) ?? [];
    const invitedEmails =
      selectedEmails?.map((email) => {
        return {
          user_email: email,
          access_level: AccessLevel.Member,
        };
      }) ?? [];
    const invitedData = [...invitedUsers, ...invitedEmails];
    mutation.mutate(invitedData);
  };

  const handleSearch = (value: string) => {
    setUserSearch(value);
  };

  const handleUserSelect = (user: MemberModel) => {
    setSelectedUsers([...(selectedUsers ?? []), user]);
  };

  const handleDeleteUser = (userId: string) => {
    setSelectedUsers(selectedUsers?.filter((user) => user.id !== userId));
  };

  const handleUserChange = (userId: string, accessLevel: AccessLevel) => {
    setSelectedUsers(
      selectedUsers?.map((user) => (user.id === userId ? { ...user, access_level: accessLevel } : user))
    );
  };

  const handleEmailChange = (values: string[]) => {
    !hasInvalidEmail && setSelectedEmails(values);
  };

  const userOptions =
    users
      ?.filter(
        (searchedUsers) =>
          ![...(selectedUsers ?? []), ...(members ?? [])]?.some((selectedUser) => searchedUsers.id === selectedUser.id)
      )
      ?.map((user) => ({
        value: user.id,
        label: (
          <Row>
            <Col span={2}>
              <Avatar size="small" shape="square" icon={<UserOutlined />} src={user?.logo_url} />
            </Col>
            <Col span={12}>
              <Typography.Text type="secondary" ellipsis>{`${user.first_name} ${user.last_name}`}</Typography.Text>
            </Col>
            <Col span={10}>
              <Typography.Text type="secondary" ellipsis>
                @{user.username}
              </Typography.Text>
            </Col>
          </Row>
        ),
        item: user as MemberModel,
      })) || [];
  return (
    <Modal
      open
      onCancel={handleClose}
      onOk={handleOk}
      okText={t('action.invite')}
      title={t('inviteMembers')}
      footer={[
        <Flex justify="space-between">
          <Space align="center">
            <Typography.Text
              copyable={{
                text: `https://${window.location.hostname}/join/${itemId}?key=${invitatonKey}`,
                icon: [<LinkOutlined key="copy-icon" />, <CheckOutlined key="copied-icon" />],
              }}
            >
              {t('action.copyInviteLink')}
            </Typography.Text>
          </Space>

          <Space>
            <Button key="cancel" onClick={handleClose}>
              {t('action.cancel')}
            </Button>
            <Button key="submit" type="primary" onClick={handleOk} disabled={hasInvalidEmail}>
              {t('action.invite')}
            </Button>
          </Space>
        </Flex>,
      ]}
    >
      <Flex vertical gap="middle">
        <Typography.Text> {t('peopleWithAccess')}</Typography.Text>
        <AutoComplete
          value={userSearch}
          options={userOptions}
          onSearch={handleSearch}
          placeholder={t('addPeople')}
          notFoundContent={userSearch.length && isFetched && !users?.length ? <Empty /> : null}
          onSelect={(_, options) => {
            handleUserSelect(options.item);
            setUserSearch('');
          }}
        />
        <UsersScrollbarStyled>
          <FlexStyled vertical gap="small">
            {selectedUsers?.map((user) => (
              <Flex key={user.id} gap="middle" justify="space-between">
                <Space>
                  <Avatar size="large" shape="square" icon={<UserOutlined />} src={user.logo_url} />
                  <Flex vertical>
                    <Typography.Text strong>
                      {user.first_name} {user.last_name}
                    </Typography.Text>
                    <Typography.Text type="secondary">{`@${user?.username}`}</Typography.Text>
                  </Flex>
                </Space>
                <Space>
                  <Select
                    defaultValue={user.access_level || AccessLevel.Member}
                    style={{ width: 150 }}
                    onChange={(val) => {
                      handleUserChange(user.id, val);
                    }}
                    options={[
                      { value: AccessLevel.Admin, label: t('legacy.role.admin') },
                      { value: AccessLevel.Member, label: t('legacy.role.member') },
                    ]}
                    dropdownRender={(menu) => (
                      <>
                        {menu}
                        <DividerStyled />
                        <Button type="text" onClick={() => handleDeleteUser(user.id)}>
                          {t('action.remove')}
                        </Button>
                      </>
                    )}
                  />
                </Space>
              </Flex>
            ))}
          </FlexStyled>
        </UsersScrollbarStyled>
        <Typography.Text> {t('byEmailAddress')}</Typography.Text>
        <EmailTagsSelect invalidEmail={setHesInvalidEmail} onChange={handleEmailChange} />
      </Flex>
    </Modal>
  );
};

export default MembersInvite;
