import useTranslation from 'next-translate/useTranslation';
import React, { FC, useState, Dispatch, SetStateAction } from 'react';
import { useApi } from '~/src/contexts/apiContext';
import { useModal } from '~/src/contexts/modalContext';
import { ItemType } from '~/src/types';
import Button from '../primitives/Button';
import MemberCard from './MemberCard';
import DropdownRole from '../Tools/DropdownRole';
import SpinLoader from '../Tools/SpinLoader';
import tw from 'twin.macro';
import ReactTooltip from 'react-tooltip';
import Icon from '../primitives/Icon';
import BtnUploadCSV from '../Tools/BtnUploadCSV';
import { confAlert } from '~/src/utils/utils';
import SendEmailToJoglUserModal from '../Tools/SendEmailToJoglUserModal';
import { useRouter } from 'next/router';
import { usePushNotificationStore } from '~/src/store/push-notifications/PushNotificationsStoreProvider';
import MembersInvite from './membersInvite/membersInvite';

interface Props {
  dataMembers: unknown[];
  error: any;
  size: any;
  membersRevalidate: () => void;
  setSize: () => void;
  type?: string;
  itemId: string;
  itemType: ItemType;
  membersPerQuery: number;
  setNbOfPendingMembersInvites?: Dispatch<SetStateAction<any>>;
  setNbOfPendingMembersRequests?: Dispatch<SetStateAction<any>>;
  specialTab?: 'reviewers' | 'admins';
  isOnboardingActive?: boolean;
  isOwner?: boolean;
}
const MembersList: FC<Props> = ({
  dataMembers,
  error,
  size,
  membersRevalidate,
  setSize,
  itemId = '0',
  itemType,
  type = 'members',
  membersPerQuery,
  setNbOfPendingMembersInvites,
  setNbOfPendingMembersRequests,
  specialTab,
  isOnboardingActive,
  isOwner = false,
}) => {
  const router = useRouter();
  const [showInviteModal, setShowInviteModal] = useState(false);
  const { showPushNotificationModal } = usePushNotificationStore((s) => ({
    showPushNotificationModal: s.showPushNotificationModal,
  }));
  const [selectedMembers, setSelectedMembers] = useState([]);
  const [showChangedMessage, setShowChangedMessage] = useState({ changing: false, ids: [] });
  const [sending, setSending] = useState(false);
  const [newRole, setNewRole] = useState({});
  const [getDeleted, setGetDeleted] = useState(false);
  const [showMemberType, setShowMemberType] = useState('all');
  const api = useApi();
  const { showModal, closeModal } = useModal();
  const { t } = useTranslation('common');
  const onlyPending = type.includes('pending');

  const inviteType = type === 'pendingRequests' ? 'request' : 'invitation';
  let members =
    dataMembers && !onlyPending
      ? [].concat(...dataMembers?.map((m) => m))
      : dataMembers && onlyPending
      ? [].concat(...dataMembers?.map((m) => m)).filter((m) => m.invitation_type === inviteType)
      : [];

  onlyPending && setNbOfPendingMembersInvites && setNbOfPendingMembersInvites(members.length);
  onlyPending && setNbOfPendingMembersRequests && setNbOfPendingMembersRequests(members.length);

  const isLoadingInitialData = !dataMembers && !error;
  const isLoadingMore =
    isLoadingInitialData || (size > 0 && dataMembers && typeof dataMembers[size - 1] === 'undefined');
  const isEmpty = dataMembers?.[0]?.length === 0;
  const isReachingEnd = isEmpty || (dataMembers && dataMembers[dataMembers.length - 1]?.length < membersPerQuery);

  const handleMembers = () => {
    if (!selectedMembers.length || selectedMembers.length !== nonOwnersMembersLength) {
      const selectedMembersIds = selectedMembers.map((m) => (m.id !== null ? m.id : m.inivation_id));
      for (let i = 0, len = members.length; i < len; i++) {
        if (!selectedMembersIds.includes(members[i].id !== null ? members[i].id : members[i].invitation_id)) {
          if (getRole(members[i]) !== 'owner') setSelectedMembers((old) => [...old, members[i]]);
        }
      }
    } else if (selectedMembers.length === nonOwnersMembersLength) setSelectedMembers([]);
  };

  const getRole = (member) => {
    let actualRole = 'member';
    if (!member.owner && !member.admin && !member.member) actualRole = 'pending';
    if (member.member) actualRole = 'member';
    if (member.owner) actualRole = 'owner';
    else if (member.admin) actualRole = 'admin';
    return actualRole;
  };

  const onRoleChanged = (member) => {
    if (Array.isArray(member)) {
      setShowChangedMessage({ changing: true, ids: member.map((m) => m.id) });
    }
    setShowChangedMessage({ changing: true, ids: [member.id] });
    setTimeout(() => {
      setShowChangedMessage({ changing: false, ids: [] });
    }, 1500);
  };

  const batchReject = async () => {
    setSending(true);
    let promises = [];
    for (let i = 0, len = selectedMembers.length; i < len; i++) {
      promises.push(api.post(`/${itemType}/${itemId}/invites/${selectedMembers[i].invitation_id}/cancel`));
    }

    await Promise.all(promises)
      .then(() => {
        setSelectedMembers([]);
        setGetDeleted(false);
        membersRevalidate();
        setSending(false);
      })
      .catch((err) => {
        console.error(`Couldn't POST ${itemType} with itemId=${itemId}`, err);
        membersRevalidate();
        setSending(false);
      });
  };

  const rejectInvite = (type) => {
    for (let i = 0, len = selectedMembers.length; i < len; i++) {
      setSending(true);
      api
        .post(`/${itemType}/${itemId}/invites/${selectedMembers[i].invitation_id}/${type}`)
        .then(() => {
          setSending(false);
          membersRevalidate();
        })
        .catch((err) => {
          console.error(`Couldn't POST ${itemType} with itemId=${itemId}`, err);
          setSending(false);
          membersRevalidate();
        });
      // setTimeout is used to delay the update of listMembers state after processing loop requests
      if (i === selectedMembers.length - 1) {
        setTimeout(() => membersRevalidate(), 1000);
      }
    }
    setSelectedMembers([]);
    setGetDeleted(false);
  };

  const acceptMember = () => {
    for (let i = 0, len = selectedMembers.length; i < len; i++) {
      setSending(true);
      api
        .post(`/${itemType}/${itemId}/invites/${selectedMembers[i].invitation_id}/accept`)
        .then(() => {
          setSending(false);
          membersRevalidate();
          showPushNotificationModal(router);
        })
        .catch((err) => {
          console.error(`Couldn't POST ${itemType} with itemId=${itemId}`, err);
          setSending(false);
          membersRevalidate();
        });
      // setTimeout is used to delay the update of listMembers state after processing loop requests
      if (i === selectedMembers.length - 1) {
        setTimeout(() => membersRevalidate(), 1000);
      }
    }
    setSelectedMembers([]);
    setGetDeleted(false);
  };

  const removeMember = () => {
    setSending(true);
    for (let i = 0, len = selectedMembers.length; i < len; i++) {
      api
        .delete(`/${itemType}/${itemId}/members/${selectedMembers[i].id}`)
        .then(() => setSending(false))
        .catch(() => setSending(false));
      // setTimeout is used to delay the update of listMembers state after processing loop requests
      if (i === selectedMembers.length - 1) {
        setTimeout(() => membersRevalidate(), 1000);
      }
    }
    setSelectedMembers([]);
    setGetDeleted(false);
  };

  const handleChange = () => {
    if (newRole.value) {
      if (itemId || itemType || selectedMembers.length) {
        for (let i = 0; i < selectedMembers.length; i++) {
          setSending(true);
          api
            .put(`/${itemType}/${itemId}/members/${selectedMembers[i].id}`, `"${newRole.value}"`)
            .then(() => setSending(false))
            .catch((err) => {
              console.error(`Couldn't POST ${itemType} with itemId=${itemId} members`, err);
              setSending(false);
            });
          // setTimeout is used to delay the update of listMembers state after processing loop requests
          if (i === selectedMembers.length - 1) {
            setTimeout(() => membersRevalidate(), 1000);
          }
        }
      }
      setSelectedMembers([]);
      setNewRole({});
    }
  };

  const handleCancel = () => {
    setSelectedMembers([]);
    setNewRole({});
    setGetDeleted(false);
  };

  const handleCSVInvites = (emailList: string[]) => {
    if (itemType === 'cfps' || itemType === 'proposals' || itemType === 'nodes' || itemType === 'organizations') {
      if (itemId) {
        if (emailList?.length > 0) {
          api
            .post(`/${itemType}/${itemId}/invite/email/batch`, emailList)
            .then((res) => {
              membersRevalidate();
              closeModal();
              confAlert.fire({ icon: 'success', title: 'Invitations sent!' });
            })
            .catch(() => confAlert.fire({ icon: 'error', title: 'Could not batch invite' }));
        }
      } else console.warn('itemId is missing');
    } else console.warn('itemType not compatible');
  };

  const memberType = [
    { value: 'all', label: 'All' },
    { value: 'lead_applicant', label: t('containerAdmin', { container: t('workspace.one') }) },
    { value: 'applicant', label: t('applicant.other') },
    { value: 'reviewer', label: t('reviewer.other') },
  ];

  const nonOwnersMembersLength = members.filter((m) => getRole(m) !== 'owner').length;
  return (
    <div>
      <div className="justify-content-end" tw="flex flex-col">
        <div tw="gap-2 inline-flex">
          <Button onClick={() => setShowInviteModal(true)}>
            {specialTab ? t(`legacy.cfpForm.btn_add_${specialTab}`) : t('inviteMembers')}
          </Button>

          <Button
            btnType="secondary"
            onClick={() => {
              showModal({
                children: <BtnUploadCSV handleSubmit={handleCSVInvites} />,
                title: specialTab ? t(`legacy.cfpForm.btn_add_${specialTab}`) : t('importFromCSV'),
              });
            }}
          >
            {specialTab ? t(`legacy.cfpForm.btn_add_${specialTab}`) : t('importFromCSV')}
          </Button>
          <div data-tip={t('inviteMembersByCSV')} data-for="ImportFromCSV">
            <Icon icon="pajamas:question" tw="w-4 h-4" tabIndex={0} />
          </div>

          <ReactTooltip id={`ImportFromCSV`} effect="solid" role="tooltip" type="dark" className="forceTooltipBg" />

          {/* Send message to members depending on active filter (or to selected members via checkbox) */}
          {itemType === 'cfps' && type === 'members' && (
            <Button
              onClick={() => {
                showModal({
                  children: (
                    <SendEmailToJoglUserModal
                      containerId={itemId}
                      containerType={itemType}
                      recipients={
                        selectedMembers?.length !== 0
                          ? selectedMembers
                          : members.filter((m) =>
                              showMemberType !== 'all'
                                ? showMemberType === 'applicant' && m.labels.includes('lead_applicant')
                                  ? true
                                  : m.labels.includes(showMemberType)
                                : true
                            )
                      }
                      closeModal={closeModal}
                    />
                  ),
                  title: t('action.sendMessageTo', {
                    name:
                      selectedMembers?.length !== 0
                        ? t('selectedMembers')
                        : memberType.find((x) => x.value === showMemberType).label,
                  }),
                });
              }}
              btnType="secondary"
            >
              {t('action.sendMessageTo', {
                name:
                  selectedMembers?.length !== 0
                    ? t('selectedMembers')
                    : memberType.find((x) => x.value === showMemberType).label,
              })}
            </Button>
          )}
        </div>

        {/* Cfps members filters */}
        {itemType === 'cfps' && type === 'members' && (
          <div tw="flex gap-3 mt-4 flex-wrap">
            {memberType.map(({ value, label }) => (
              <div
                key={value}
                onClick={() => setShowMemberType(value)}
                tw="cursor-pointer px-2 text-sm py-1 rounded-full hocus:(bg-[#EBF1FA]) min-w-[max-content] border border-solid border-gray-200"
                css={showMemberType === value ? tw`bg-[#EBF1FA]` : tw`bg-white `}
              >
                {label}
              </div>
            ))}
          </div>
        )}

        {/* Special batch action */}
        <div tw="flex flex-col mt-6 space-y-4 sm:space-y-0">
          {!!selectedMembers.length && !onlyPending ? (
            <>
              <div tw="flex w-full flex-wrap gap-2 justify-between">
                <div tw="flex flex-wrap justify-start gap-4 sm:items-center">
                  <div tw="w-48">
                    <DropdownRole
                      onRoleChanged={() => onRoleChanged(selectedMembers)}
                      itemId={itemId}
                      itemType={itemType}
                      // show different list roles depending if user is owner (to prevent admins changing their roles to "owner")
                      listRole={['admin', 'member']}
                      member={selectedMembers}
                      showPlaceholder
                      setNewRole={setNewRole}
                    />
                  </div>
                  <div tw="flex flex-auto justify-start space-x-4">
                    <Button
                      onClick={handleChange}
                      disabled={(newRole.value || getDeleted) && selectedMembers.length ? false : true}
                    >
                      {sending && <SpinLoader />}
                      {t('entity.form.btnApply')}
                    </Button>
                    <Button
                      btnType="secondary"
                      onClick={handleCancel}
                      disabled={newRole.value || getDeleted ? false : true}
                    >
                      {t('action.cancel')}
                    </Button>
                  </div>
                </div>
                <Button onClick={removeMember} btnType="danger">
                  {t('action.remove')}
                </Button>
              </div>
            </>
          ) : !!selectedMembers.length && type === 'pendingRequests' ? (
            <div tw="space-x-2">
              <Button
                tw="text-green-500 border-green-500 bg-white hover:(text-white bg-green-500)"
                disabled={sending}
                onClick={acceptMember}
              >
                {sending && <SpinLoader />}
                {t('action.accept')}
              </Button>
              <Button
                tw="text-danger border-danger bg-white hover:(text-white bg-danger)"
                disabled={sending}
                onClick={() => rejectInvite('reject')}
              >
                {sending && <SpinLoader />}
                {t('action.reject')}
              </Button>
            </div>
          ) : (
            !!selectedMembers.length &&
            type === 'pendingInvites' && (
              <div tw="inline-flex space-x-4 items-center">
                <Button
                  tw="text-danger border-danger bg-white hocus:(text-white bg-danger)"
                  disabled={sending}
                  onClick={() => batchReject()}
                >
                  {sending && <SpinLoader />}
                  {t('action.cancelItem', { item: t('invitation.one') })}
                </Button>
              </div>
            )
          )}
        </div>
        {members?.length !== 0 && (
          <div tw="flex mt-8 items-center">
            <input
              id="members"
              type="checkbox"
              tw="hover:cursor-pointer w-[20px]"
              onChange={handleMembers}
              checked={selectedMembers.length && selectedMembers.length === nonOwnersMembersLength}
            />
            <label htmlFor="members" tw="ml-4 mb-0">
              {specialTab
                ? t('action.selectAllItems', { item: t(specialTab) })
                : t('action.selectAllItems', { item: t('member', { count: 2 }) })}
            </label>
          </div>
        )}
      </div>

      {/* Actual members list */}
      {members?.length !== 0 && (
        <div tw="pt-6">
          {members
            .filter((m) =>
              showMemberType !== 'all'
                ? showMemberType === 'applicant' && m.labels.includes('lead_applicant')
                  ? true
                  : m.labels.includes(showMemberType)
                : true
            )
            .map((member, i) => (
              <MemberCard
                itemId={itemId}
                itemType={itemType}
                member={member}
                key={i}
                callBack={membersRevalidate}
                isOwner={isOwner}
                invitationType={member.invitation_type}
                selectedMembers={selectedMembers}
                setSelectedMembers={setSelectedMembers}
                role={onlyPending ? 'pending' : member.access_level}
                showChangedMessage={showChangedMessage}
                onRoleChanged={() => onRoleChanged(member)}
                isSending={sending}
                tabType={type}
                isOnboardingActive={isOnboardingActive}
              />
            ))}
        </div>
      )}

      {/* Load more button */}
      {members?.length >= membersPerQuery && (
        <div tw="self-center pt-4">
          <Button onClick={() => setSize(size + 1)} disabled={isLoadingMore || isReachingEnd}>
            {isLoadingMore && <SpinLoader />}
            {isLoadingMore ? t('loadingWithEllipsis') : !isReachingEnd ? t('action.load') : t('noMoreResults')}
          </Button>
        </div>
      )}
      {showInviteModal && (
        <MembersInvite
          refetch={membersRevalidate}
          itemId={itemId}
          members={members}
          handleClose={() => setShowInviteModal(false)}
        />
      )}
    </div>
  );
};
export default MembersList;
