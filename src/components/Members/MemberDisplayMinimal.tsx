import { FC } from 'react';
import A from '../primitives/A';
import useGet from '~/src/hooks/useGet';
import { User } from '~/src/types';

interface Props {
  userId?: string;
  user?: User;
}

const fetchUserData = (userId: string): User => {
  const { data } = useGet(`/users/${userId}`);
  return data as User;
};

//Send user prop if info is already available, otherwise send the userId to fetch
const MemberDisplayMinimal: FC<Props> = ({ userId, user }) => {
  const userInfo = user ? user : fetchUserData(userId);

  return (
    userInfo && (
      <A href={`/user/${userInfo.id}`} noStyle>
        <div tw="flex flex-row items-center gap-2 m-1 background[white] shadow-custom cursor-pointer rounded-lg px-3 py-2 bg-white">
          <div
            style={{
              backgroundImage: `url(${userInfo?.logo_url_sm ?? '/images/default/default-user.png'})`,
            }}
            tw="bg-cover bg-center h-6 w-6 rounded-full border border-solid border-[#ced4da] flex-none"
          />
          <span tw="text-xs font-[500] hover:underline">
            {userInfo?.first_name} {userInfo?.last_name}
          </span>
        </div>
      </A>
    )
  );
};

export default MemberDisplayMinimal;
