import { useEffect, useState } from 'react';
import { useApi } from '~/src/contexts/apiContext';
import { useModal } from '~/src/contexts/modalContext';
import { MenuActions, User } from '~/src/types';
import SearchPublications from '../Publications/SearchPublications';
import { confAlert } from '~/src/utils/utils';
import { paperAction } from '~/src/utils/getContainerInfo';

const UserAddActions = (user: User, refresh: () => void): MenuActions => {
  const api = useApi();

  const [alreadyAddedPapers, setAlreadyAddedPapers] = useState<{}>({});
  const [showPapersModal, setShowPapersModal] = useState<boolean>(false);
  const { isOpen: isPapersModalOpen, showModal: showModalPapers, closeModal: closeModalPapers } = useModal();

  const getAlreadyAddedPapers = async () => {
    const newData = {};

    const papers = await api
      .get(`/users/${user?.id}/papers`)
      .then((res) => res.data)
      .catch((err) => {
        console.warn("Couldn't fetch user papers: ", err);
      });

    if (papers) {
      for (const paper of papers) {
        newData[paper.external_id] = paper.id;
      }
    }

    setAlreadyAddedPapers(newData);
  };

  useEffect(() => {
    getAlreadyAddedPapers();
  }, [showPapersModal]);

  useEffect(() => {
    if (!isPapersModalOpen) {
      setShowPapersModal(false);
    }
  }, [isPapersModalOpen]);

  const addPublication = async (data) => {
    try {
      await api.post(`/users/papers/`, {
        ...data,
        title: data?.title,
        summary: data?.abstract?.replaceAll('\n', '<br />'),
        external_id: data?.external_url ?? data?.external_id,
        publication_date: data?.publication_date,
        authors: data?.authors,
        journal: data?.journal,
        open_access_pdf: data?.open_access_pdf,
        type: 'article',
      });

      refresh();
      getAlreadyAddedPapers();

      confAlert.fire({ icon: 'success', title: 'Publication added successfully' });
      return { error: true };
    } catch (err) {
      console.warn(err);
      confAlert.fire({ icon: 'error', title: 'Error while adding publication' });

      return { error: true };
    }
  };

  const removePublication = async (id) => {
    try {
      await api.delete(`/users/papers/${id}`);

      refresh();
      getAlreadyAddedPapers();

      confAlert.fire({ icon: 'success', title: 'Deleted paper' });
      return { error: true };
    } catch (err) {
      console.warn(err);
      confAlert.fire({ icon: 'error', title: 'Error while deleting paper' });
      return { error: true };
    }
  };

  useEffect(() => {
    // eslint-disable-next-line no-unused-expressions
    showPapersModal &&
      showModalPapers({
        children: (
          <SearchPublications
            addPublication={addPublication}
            removePublication={removePublication}
            userDOIs={alreadyAddedPapers}
            user={user}
            hasLinkedOrcid={user.orcid_id !== null}
            callback={() => {
              setShowPapersModal(false);
              // closeModalPapers();
            }}
            fetchPapers={getAlreadyAddedPapers}
            closeModal={closeModalPapers}
          />
        ),
        showCloseButton: false,
        showTitle: false,
        maxWidth: '50rem',
      });
  }, [alreadyAddedPapers, showPapersModal]);

  return {
    members: undefined,
    content: [
      {
        ...paperAction({}),
        privacy: '',
        disabled: false,
        handleSelect: () => setShowPapersModal(true),
      },
    ],
    containers: undefined,
  };
};

export default UserAddActions;
