import Link from 'next/link';
import React, { FC, memo, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import H2 from '~/src/components/primitives/H2';
import { useModal } from '~/src/contexts/modalContext';
import useUserData from '~/src/hooks/useUserData';
import { User } from '~/src/types';
import ObjectCard from '~/src/components/Cards/ObjectCard';
import Title from '../primitives/Title';
import BtnFollow from '../Tools/BtnFollow';
import Image from '~/src/components/primitives/Image';
import Button from '../primitives/Button';
import styles from './UserContribute.module.css';
import AddContribution from './AddContribution';
import Icon from '../primitives/Icon';
import ReactTooltip from 'react-tooltip';
import tw from 'twin.macro';

interface Props {
  user: User;
  entityId?: string;
  role?: string;
  isCompact?: boolean;
  showAddContribution?: boolean;
  refresh?: () => void;
}
const UserCard: FC<Props> = ({ user, role, entityId, showAddContribution, isCompact = false, refresh }) => {
  const { userData } = useUserData();
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const [contribution, setContribution] = useState(user.contribution);

  const showContributionModal = () => {
    showModal({
      children: (
        <AddContribution
          entityId={entityId}
          contribution={contribution}
          memberId={user.id}
          onSubmit={() => {
            closeModal();
            refresh();
          }}
          setContribution={setContribution}
        />
      ),
      title: t('action.addMyContribution'),
      maxWidth: '30rem',
      modalClassName: styles.modalPadding,
    });
  };

  const userUrl = `/user/${user.id}/${user.username}`;
  const maxOrgaShown = 4;
  const extraOrgaNb =
    user.organizations?.length <= maxOrgaShown ? undefined : user.organizations?.length - maxOrgaShown;

  if (!isCompact) {
    return (
      <ObjectCard tw="justify-between h-[350px]">
        <div tw="-mx-4 -mt-4 h-[62px]">
          <img
            src={user.banner_url || user.banner_url_sm || '/images/default/default-user-banner.svg'}
            alt="banner"
            tw="w-full h-full object-cover"
          />
          <div tw="bg-white absolute w-full left-0 h-[2.2rem] top-[2rem] [clip-path: ellipse(55% 65% at 50% 75%)]" />
        </div>
        {role && (
          <div tw="absolute text-sm right-2 text-[#4d4d4d] capitalize rounded top-2 bg-white bg-opacity-80 px-1 [box-shadow:0px 4px 15px rgb(0 0 0 / 9%)]">
            {t(`legacy.role.${role}`)}
          </div>
        )}
        <div tw="flex flex-col justify-between min-h-[315px] -mt-[52px]">
          {/* Profile img */}
          <div tw="z-[3] items-center -ml-[5px] mt-[10px]">
            <Link href={userUrl} tw="w-fit">
              <Image
                src={user.logo_url || user.logo_url_sm || '/images/default/default-user.png'}
                tw="object-cover rounded-full w-20! h-20! border-4 border-white bg-white"
                alt={`${user.first_name || 'First name'} ${user.last_name || 'Last name'}`}
              />
            </Link>
          </div>
          <div tw="absolute left-[5.8rem] mr-4 z-[3] top-[2.8rem]">
            {/* Name */}
            <Link href={userUrl} passHref legacyBehavior>
              <Title>
                <H2 tw="[word-break:break-word] text-[18px] line-clamp-2 [line-height: 19.5px]">
                  {user.first_name || 'First name'} {user.last_name || 'Last name'}
                </H2>
              </Title>
            </Link>
            <p tw="text-gray-500 text-xs line-clamp-1 mb-0 [word-break: break-all]">{`@${user?.username}`}</p>
          </div>
          {/* <Hr tw="mt-2 pt-3" /> */}
          <div tw="flex gap-2 flex-col mt-[0.6rem]">
            {/* Organizations */}
            {user.organizations?.length !== 0 && (
              <div tw="flex flex-wrap gap-2 mx-auto">
                {user.organizations?.slice(0, maxOrgaShown).map((orga, i) => (
                  <Link key={i} href={`/organization/${orga.id}`} target="_blank">
                    <img
                      tw="w-6 h-6 object-cover rounded-full border border-gray-300 hover:opacity-80"
                      src={orga.logo_url_sm || orga.banner_url_sm || '/images/default/default-organization.png'}
                      data-tip={orga.title || t('untitled')}
                      data-for={orga.id}
                    />
                    <ReactTooltip id={orga.id} effect="solid" />
                  </Link>
                ))}
                {extraOrgaNb ? (
                  <Link href={`${userUrl}?tab=organizations`} tw="hover:no-underline!">
                    <div tw="w-6 h-6 object-cover flex items-center justify-center text-sm text-gray-500! rounded-full border border-gray-300 hover:bg-gray-100">
                      +{extraOrgaNb.toString()}
                    </div>
                  </Link>
                ) : (
                  ''
                )}
              </div>
            )}
            {user?.status && (
              <div tw="flex border border-gray-200 rounded-full px-2 py-[2.5px] w-fit mx-auto items-center text-[#E26EAB] text-[13px] font-medium">
                <Icon icon="material-symbols:circle" tw="h-2 w-2 flex-none" />
                <span tw="line-clamp-2 text-center [line-height:15px]" title={user?.status}>
                  {user?.status}
                </span>
              </div>
            )}
          </div>
          {/* Contribution */}
          {showAddContribution && (
            <>
              <div
                className={`content contributionContainer ${styles.contributionContainer}`}
                tw="inline-flex [line-height:17px] justify-between gap-[5px] mt-2"
              >
                {/* if user has contribution, display it */}
                <div tw="w-full">
                  <div className={contribution ? 'row-contributor-title contributor-title' : 'contributor-title'}>
                    <div tw="font-semibold text-sm">{t('workspaceContribution')}:</div>
                    {userData && userData?.id === user.id && showAddContribution && !contribution && (
                      <Button
                        btnType="secondary"
                        tw="text-sm"
                        onClick={() => {
                          showModal({
                            children: (
                              <AddContribution
                                entityId={entityId}
                                contribution={contribution}
                                memberId={user.id}
                                onSubmit={() => {
                                  closeModal();
                                  refresh();
                                }}
                                setContribution={setContribution}
                              />
                            ),
                            title: t('action.addMyContribution'),
                            maxWidth: '30rem',
                            modalClassName: styles.modalPadding,
                          });
                        }}
                      >
                        <Icon icon="bxs:edit" tw="w-4 h-4" />
                        {!contribution ? t('action.addMyContribution') : t('action.manage')}
                      </Button>
                    )}
                    {userData && userData?.id !== user.id && showAddContribution && !contribution && (
                      <p tw="text-gray-800 font-normal leading-5 [line-height:17px] text-[14px]">
                        {t('noContributionYet')}.
                      </p>
                    )}
                  </div>
                  <div
                    // css={[userData?.id === user.id ? tw`line-clamp-4` : tw`line-clamp-2`]}
                    tw="w-full overflow-hidden font-normal leading-5 text-gray-800 [line-height:17px] text-[14px]"
                    css={!user.status || user.organizations?.length === 0 ? tw`line-clamp-3` : tw`line-clamp-2`}
                  >
                    {contribution}
                  </div>
                </div>
                <div tw="flex justify-between w-full mb-2">
                  <span
                    className="see-more"
                    onClick={() => {
                      showModal({
                        children: (
                          <div tw="w-full">
                            <div
                              className={contribution ? 'row-contributor-title contributor-title' : 'contributor-title'}
                            >
                              <div tw="font-semibold text-sm">{t('workspaceContribution')}:</div>
                              {userData && userData?.id !== user.id && showAddContribution && !contribution && (
                                <p tw="text-black font-normal text-sm leading-5">{t('noContributionYet')}.</p>
                              )}
                            </div>
                            <div tw="w-full overflow-hidden text-sm font-normal leading-5 text-black">
                              {contribution}
                            </div>
                            <Hr tw="mt-5 pt-3" />
                            <span tw="font-semibold text-sm leading-5">{t('userHeadline')}:</span>
                            <div tw="my-1 text-sm break-normal">{user.short_bio || '– –'}</div>
                          </div>
                        ),
                        title: t('action.addMyContribution'),
                        maxWidth: '20rem',
                        showCloseButton: false,
                        showTitle: false,
                      });
                    }}
                  >
                    {t('action.seeMore')} +
                  </span>
                  {userData?.id === user.id ? (
                    <span tw="cursor-pointer" onClick={showContributionModal}>
                      <Icon icon="carbon:edit" tw="w-[1.15rem] h-[1.15rem]" />
                    </span>
                  ) : (
                    <span />
                  )}
                </div>
              </div>
            </>
          )}
          {/* Bio */}
          {!showAddContribution && (
            <div css={!user.status || user.organizations?.length === 0 ? tw`h-[5rem]` : tw`h-[4.5rem]`}>
              <div
                tw="my-1 text-sm break-normal text-gray-700 [line-height:19px]"
                css={!user.status || user.organizations?.length === 0 ? tw`line-clamp-4` : tw`line-clamp-3`}
                title={user.short_bio}
              >
                {user.short_bio || '– –'}
              </div>
            </div>
          )}
          {/* <Hr tw="mt-2 pt-3" /> */}
          {/* Shared connections */}
          {/* {mutualCount !== undefined && (
            <div
              tw="flex flex-col items-center text-sm mb-3 cursor-pointer hover:opacity-80"
              // open modal only if count is more than 0, and you're not the user
              onClick={() => mutualCount > 0 && userData?.id !== id && openShareConnectionsModal(id, api, t, showModal)}
            >
              <div tw="inline-flex">
                <div tw="rounded-full h-3 w-3 flex border-gray-600 border-solid border-2"></div>
                <div tw="rounded-full h-3 w-3 flex border-gray-600 border-solid border-2 -ml-1.5"></div>
              </div>
              {userData?.id !== id ? mutualCount : '∞'} <TextWithPlural type="mutualConnection" count={mutualCount} />
            </div>
          )} */}
          {user?.city && (
            <div tw="inline-flex font-medium items-center mx-auto text-gray-700">
              <Icon icon="humbleicons:location" tw="w-4 h-4" />
              <span tw="text-[13px]">{user?.city}</span>
            </div>
          )}
          {/* {(user?.city || user?.country) && (
            <div tw="inline-flex font-medium items-center mx-auto text-gray-700">
              <Icon icon="humbleicons:location" tw="w-4 h-4" />
              <span tw="text-[13px]">
                {user?.city && user?.country ? `${user?.city}, ` : user?.city && user?.city}
                {user?.country}
              </span>
            </div>
          )} */}
          {/* Follow button */}
          <div tw="flex justify-center mt-2">
            <BtnFollow
              followState={user.user_follows}
              itemType="users"
              itemId={user.id}
              isDisabled={user.user_follows !== undefined && userData?.id === user.id}
              hasNoStat
              tw="w-fit"
            />
          </div>
        </div>
      </ObjectCard>
    );
  }
  // Compact card
  return (
    <div tw="flex flex-col shadow-custom rounded-xl p-3 h-[9rem] w-[240px]">
      <div tw="inline-flex items-center gap-2">
        {/* Profile img */}
        <Link href={userUrl} tw="items-center justify-center">
          <Image
            src={user.logo_url || user.logo_url_sm || '/images/default/default-user.png'}
            tw="object-cover rounded-full w-12! h-12! bg-white hover:opacity-90 flex-none"
            alt={`${user.first_name || 'First name'} ${user.last_name || 'Last name'}`}
          />
        </Link>
        <div tw="flex flex-col flex-1 items-start">
          {/* Name */}
          <div tw="flex w-full">
            <Link href={userUrl} passHref legacyBehavior>
              <Title>
                <H2 tw="[word-break:break-word] text-left text-lg line-clamp-2">
                  {user.first_name || 'First name'} {user.last_name || 'Last name'}
                </H2>
              </Title>
            </Link>
          </div>
          {/* Follow button */}
          <div tw="flex h-fit my-1">
            <BtnFollow
              followState={user.user_follows}
              itemType="users"
              itemId={user.id}
              hasNoStat
              isDisabled={user.user_follows !== undefined && userData?.id === user.id}
              tw="w-fit h-fit"
            />
          </div>
        </div>
      </div>
      {/* Bio */}
      <div tw="line-clamp-2 my-1 break-words">{user.short_bio || '– –'}</div>
    </div>
  );
};

const Hr = (props) => <div tw="-mx-4 border-0 border-t border-solid border-color[rgba(0, 0, 0, 0.07)]" {...props} />;

export default memo(UserCard);
