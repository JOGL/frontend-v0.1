import { FC, useState } from 'react';
import Button from '../primitives/Button';
import FormTextAreaComponent from '../Tools/Forms/FormTextAreaComponent';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from '~/src/contexts/apiContext';
import { confAlert } from '~/src/utils/utils';

interface AddContributionProps {
  entityId: string;
  memberId: string;
  contribution?: string;
  onSubmit?: () => void;
  setContribution?: any;
}

const AddContribution: FC<AddContributionProps> = ({
  entityId,
  memberId,
  contribution: previousContribution,
  setContribution,
  onSubmit,
}) => {
  const [contribution, setContribution2] = useState(previousContribution ?? '');
  const { t } = useTranslation('common');
  const api = useApi();

  const submitContribution = async () => {
    try {
      await api.put(`/workspaces/${entityId}/members/${memberId}/contribution`, JSON.stringify(contribution));
      confAlert.fire({ icon: 'success', title: 'Contribution updated!' });
      setTimeout(() => onSubmit(), 500);
      setContribution(contribution);
    } catch (e) {
      confAlert.fire({ icon: 'error', title: 'Error' });
    }
  };

  return (
    <>
      <FormTextAreaComponent
        id="contribution"
        placeholder={t('myContribution')}
        title={t('myContribution')}
        content={contribution}
        rows={3}
        maxChar={340}
        showMaxChar={false}
        showTitle={false}
        supportLinks={false}
        showWordCount={true}
        onChange={(_, value) => {
          setContribution2(value);
        }}
      />
      <Button
        onClick={() => {
          submitContribution();
          setContribution(contribution);
        }}
        tw="self-end"
      >
        {t('action.save')}
      </Button>
    </>
  );
};

export default AddContribution;
