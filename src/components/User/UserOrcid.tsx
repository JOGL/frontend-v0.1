import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useApi } from '~/src/contexts/apiContext';
import { addQueryParams } from '~/src/utils/utils';

const UserOrcid = ({ user, code }) => {
  const api = useApi();
  const router = useRouter();

  const getUserOrcid = async () => {
    const redirect_uri = window?.localStorage?.getItem('redirect_uri');

    api
      .post('/users/orcid', { authorization_code: code })
      .then(() => {
        if (typeof redirect_uri === 'string' && redirect_uri.length > 0) {
          const url = addQueryParams(redirect_uri, {
            code: code,
          });
          router.push({
            pathname: url.pathname,
            query: url.searchParams as any,
          });
          return;
        }
        router.push(`/user/${user.id}/new`);
      })
      .catch((err) => {
        console.error(err);
      });
  };

  useEffect(() => {
    getUserOrcid();
  }, []);

  return (
    <div tw="flex justify-center items-center h-36">
      <img src={`/images/icons/links-orcid.png`} width="28px" />
      &nbsp; Please wait while we are linking your orcid...
    </div>
  );
};

export default UserOrcid;
