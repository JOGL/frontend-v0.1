import { Button, Flex, Typography } from 'antd';
import styled from '@emotion/styled';

export const FilePreviewTitleStyled = styled(Typography.Title)`
  color: ${({ theme }) => theme.token.colorWhite} !important;
  margin-bottom: 0px !important;
`;

export const PreviewFileWrapperStyled = styled.div<{ scrollable: boolean }>`
  max-width: 100vw; // Changed from 80vw to allow full width
  max-height: 85vh;
  overflow: ${(props) => (props.scrollable ? 'auto' : 'hidden')};
`;

export const ButtonStyled = styled(Button)`
  flex-shrink: 0;
`;

export const WrapperStyled = styled(Flex)`
  min-width: 0px;
`;
