import { ReactNode } from 'react';
import Overlay, { OverlayCloseButton } from '../overlay/overlay';
import { Flex, Grid } from 'antd';
import {
  ButtonStyled,
  FilePreviewTitleStyled,
  PreviewFileWrapperStyled,
  WrapperStyled,
} from './filePreviewWrapper.styles';
import useTranslation from 'next-translate/useTranslation';
import { DownloadOutlined } from '@ant-design/icons';
const { useBreakpoint } = Grid;
interface Props {
  children: ReactNode;
  title: string;
  isDownloading?: boolean;
  isScrollable?: boolean;
  onDownload: () => void;
  onClose: () => void;
}

const FilePreviewWrapper = ({ children, title, isDownloading, onDownload, onClose, isScrollable = false }: Props) => {
  const { t } = useTranslation('common');
  const screens = useBreakpoint();
  return (
    <Overlay onClose={onClose}>
      <Flex vertical gap="large">
        <Flex justify="space-between" gap="large" align="center">
          <WrapperStyled align="center" gap="middle">
            <OverlayCloseButton onClose={onClose} />
            <FilePreviewTitleStyled level={screens.xs ? 5 : 4} ellipsis={{ tooltip: title }}>
              {title}
            </FilePreviewTitleStyled>
          </WrapperStyled>
          <ButtonStyled ghost onClick={onDownload} loading={isDownloading} icon={<DownloadOutlined />}>
            {screens.sm && t('action.download')}
          </ButtonStyled>
        </Flex>
        <PreviewFileWrapperStyled scrollable={isScrollable}>{children}</PreviewFileWrapperStyled>
      </Flex>
    </Overlay>
  );
};

export default FilePreviewWrapper;
