import { Flex, Typography, Button, Input, message, Grid } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useState } from 'react';
import NeedTable from '../needTable/needTable';
import useDebounce from '~/src/hooks/useDebounce';
import { useMutation, useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import { Permission, SortKey } from '~/__generated__/types';
import CreateNeed from '../createNeedModal/createNeedModal';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import MobileCardView from '../../Common/mobileCardView/mobileCardView';
import useNeeds from '../useNeeds';
import { useFeedEntity } from '../../Common/feedEntity/feedEntity.hooks';
const { useBreakpoint } = Grid;

interface Props {
  entityId: string;
  canManageNeeds: boolean;
}
const EntityNeedList = ({ entityId, canManageNeeds }: Props) => {
  const [search, setSearch] = useState('');
  const [goToNeed] = useNeeds();
  const screens = useBreakpoint();
  const isMobile = screens.xs;
  const [markFeedAsOpened] = useFeedEntity();

  const debouncedSearch = useDebounce(search, 300);
  const { data, isLoading, refetch } = useQuery({
    queryKey: [QUERY_KEYS.entityNeedList, entityId, debouncedSearch],
    queryFn: async () => {
      const response = await api.needs.needsDetail(entityId, {
        Search: debouncedSearch,
        SortKey: SortKey.Updateddate,
        SortAscending: false,
      });
      return response.data;
    },
  });

  const { selectedHub } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
  }));

  const selectedSpace = selectedHub?.entities.find((item) => item.id === entityId);

  const { t } = useTranslation('common');
  const [showPrivacyModal, setShowPrivacyModal] = useState(false);
  const deleteNeed = useMutation({
    mutationFn: async (id: string) => {
      await api.needs.needsDeleteDetail(id);
    },
    onSuccess: () => {
      message.success(t('needDeleteSuccess'));
      refetch();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const canManage = selectedSpace?.user_access.permissions?.includes(Permission.Postneed);

  return (
    <>
      <Flex vertical gap="middle">
        <Typography.Title level={3}>{t('need.other')}</Typography.Title>
        <Flex justify="stretch" gap="large">
          <Input.Search
            placeholder={t('action.searchForNeeds')}
            onChange={(e) => {
              setSearch(e.target.value);
            }}
            onSearch={(value) => {
              setSearch(value);
            }}
          />
          {canManageNeeds && <Button onClick={() => setShowPrivacyModal(true)}>{t('action.add')}</Button>}
          {showPrivacyModal && (
            <CreateNeed closeModal={() => setShowPrivacyModal(false)} selectedSpace={selectedSpace} />
          )}
        </Flex>
        {isMobile ? (
          <MobileCardView
            data={data ?? []}
            loading={isLoading}
            onClickRow={(need) => {
              goToNeed(need.id);
              markFeedAsOpened(need);
            }}
          />
        ) : (
          <NeedTable
            canManage={canManage}
            onDelete={(need) => {
              deleteNeed.mutate(need.id);
            }}
            hideColumns={canManage ? ['entity'] : ['entity', 'id']}
            loading={isLoading}
            needs={data ?? []}
          />
        )}
      </Flex>
    </>
  );
};

export default EntityNeedList;
