import { useInfiniteQuery, useMutation, useQuery } from '@tanstack/react-query';
import { Flex, Grid, Input, Typography, message } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useMemo, useState } from 'react';
import { FeedEntityFilter, NeedModel, Permission, SortKey } from '~/__generated__/types';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import useDebounce from '~/src/hooks/useDebounce';
import { ContainerFilter } from '../../Common/filters/containerFilter/containerFilter';
import NeedTable from '../needTable/needTable';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import useNeeds from '../useNeeds';
import MobileCardView from '../../Common/mobileCardView/mobileCardView';
import { useFeedEntity } from '../../Common/feedEntity/feedEntity.hooks';
const PAGE_SIZE = 50;
const { useBreakpoint } = Grid;

interface Props {
  hubId: string;
  title: string;
  filter?: FeedEntityFilter;
  sortKey?: SortKey;
}

const HubNeedList = ({ hubId, title, filter, sortKey = SortKey.Updateddate }: Props) => {
  const { t } = useTranslation('common');
  const [search, setSearch] = useState('');
  const debouncedSearch = useDebounce(search, 300);
  const [selectedContainers, setSelectedContainers] = useState<string[]>([]);
  const [goToNeed] = useNeeds();
  const screens = useBreakpoint();
  const isMobile = screens.xs;
  const [markFeedAsOpened] = useFeedEntity();

  const { selectedHub } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
  }));

  const { data: needData, isLoading, refetch } = useInfiniteQuery({
    queryKey: [QUERY_KEYS.needsAggregate, hubId, debouncedSearch, selectedContainers, filter, sortKey],
    queryFn: async ({ pageParam = 1 }) => {
      const response = await api.nodes.needsAggregateDetail(hubId, {
        Search: debouncedSearch,
        communityEntityIds: selectedContainers.join(',') ?? undefined,
        filter: filter,
        SortKey: sortKey,
        SortAscending: undefined,
        Page: pageParam,
        PageSize: PAGE_SIZE,
      });
      return response.data;
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItemsCount = allPages.reduce((acc, page) => acc + page.items.length, 0);
      return loadedItemsCount < lastPage.total ? allPages.length + 1 : undefined;
    },
  });

  const { data: containers } = useQuery({
    queryKey: [QUERY_KEYS.needsAggregateContainers, hubId],
    queryFn: async () => {
      const response = await api.nodes.needsAggregateCommunityEntitiesDetail(hubId, {});
      return response.data;
    },
  });

  const deleteNeed = useMutation({
    mutationFn: async (id: string) => {
      await api.needs.needsDeleteDetail(id);
    },
    onSuccess: () => {
      message.success(t('needDeleteSuccess'));
      refetch();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const flattenedData: NeedModel[] = useMemo(() => {
    return needData?.pages.flatMap((page) => page.items) ?? [];
  }, [needData]);

  const canManage = selectedHub?.user_access.permissions.includes(Permission.Postneed) ? true : false;
  return (
    <>
      <Flex vertical gap="middle">
        <Typography.Title>{title}</Typography.Title>
        <Flex justify="stretch">
          <Input.Search
            placeholder={t('action.searchForNeeds')}
            onChange={(e) => {
              setSearch(e.target.value);
            }}
            onSearch={(value) => {
              setSearch(value);
            }}
          />
        </Flex>
        <Flex justify="stretch">
          {containers && (
            <ContainerFilter
              containers={containers}
              onChange={(communityEntityIds) => setSelectedContainers(communityEntityIds)}
            />
          )}
        </Flex>
        {isMobile ? (
          <MobileCardView
            data={flattenedData}
            loading={isLoading}
            onClickRow={(need) => {
              goToNeed(need.id);
              markFeedAsOpened(need);
            }}
          />
        ) : (
          <NeedTable
            canManage={canManage}
            needs={flattenedData}
            loading={isLoading}
            hideColumns={canManage ? [] : ['id']}
            onDelete={(need) => {
              deleteNeed.mutate(need.id);
            }}
          />
        )}
      </Flex>
    </>
  );
};

export default HubNeedList;
