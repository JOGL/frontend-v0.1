import Link from 'next/link';
import React, { useRef } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import ObjectCard from '~/src/components/Cards/ObjectCard';
import H2 from '~/src/components/primitives/H2';
import Title from '~/src/components/primitives/Title';
import ShareBtns from '~/src/components/Tools/ShareBtns/ShareBtns';
import { Need } from '~/src/types';
import Icon from '~/src/components/primitives/Icon';
import tw from 'twin.macro';
import useUser from '~/src/hooks/useUser';
import { Menu, MenuItem, MenuButton, MenuList } from '@reach/menu-button';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import { entityItem, isAdmin, needUrl } from '~/src/utils/utils';
import { TextChip } from '~/src/types/chip';
import { NeedModel } from '~/__generated__/types';

interface Props {
  need: Need | NeedModel;
  width?: string;
  cardFormat?: string;
  recommendedIds?: number[];
  textChips?: TextChip[];
  onDelete?: (id: any) => void;
}

const NeedCard = ({ need, width, cardFormat, recommendedIds = [], textChips, onDelete }: Props) => {
  const { t } = useTranslation('common');
  const needFrontUrl = needUrl(need.id);
  const router = useRouter();
  const { user } = useUser();
  const shareBtnRef = useRef(null);
  return (
    <ObjectCard
      imgUrl={need?.entity?.banner_url || '/images/default/default-workspace.png'}
      href={needFrontUrl}
      width={width}
      textChips={textChips}
      cardFormat={cardFormat}
      css={[cardFormat !== 'inPosts' && tw`overflow-visible`]}
    >
      {/* Title */}
      <div css={[tw`inline-flex items-center`, cardFormat !== 'compact' && cardFormat !== 'inPosts' && tw`md:h-8`]}>
        <Link href={needFrontUrl} passHref legacyBehavior>
          <Title>
            {/* Add padding-right if card doesn't have workspace (thus its banner), so title doesn't overlap with position absolute's star icon */}
            <H2
              tw="break-words text-[16px] items-center"
              css={cardFormat !== 'inPosts' ? tw`md:line-clamp-2` : tw`md:line-clamp-1`}
              title={need.title}
            >
              {need.title}
            </H2>
          </Title>
        </Link>
      </div>
      {!need.entity && (
        <>
          <Hr css={cardFormat === 'inPosts' ? tw`mt-2` : tw`mt-3`} />
          <InfoHtmlComponent
            className="needCardDescription"
            content={need.description || '&nbsp;'}
            tw="text-sm text-[#5F5D5D] block font-extrabold leading-6 line-clamp-3 h-32"
          />
        </>
      )}
      {/* Show parent */}
      {need.entity && (
        <>
          <Hr tw="pt-3" css={cardFormat === 'inPosts' ? tw`mt-2` : tw`mt-3`} />
          <div tw="h-32">
            <div tw="text-sm line-clamp-2">
              <Icon icon={entityItem[need.entity.type].icon} tw="w-4 h-4" />
              {t(entityItem[need.entity.type].translationId)}:&nbsp;
              <Link
                href={`/${entityItem[need.entity.type].front_path}/${need.entity.id}`}
                passHref
                tw="text-black font-medium hover:(underline text-black)"
              >
                {need.entity.title}
              </Link>
            </div>
          </div>
        </>
      )}
      <div tw="flex-col items-end">
        <div tw="mt-3 flex justify-between text-sm">
          <div tw="flex items-center text-gray-500">
            <Icon icon="material-symbols:extension-outline" tw="w-5 h-5" />
            <span>Type:</span>
          </div>
          <div tw="rounded-md bg-[var(--main-colors-line-border, rgba(209, 209, 209, 0.50))] text-gray-500 px-2 py-1">
            {t(`legacy.needType.${need.type}`)}
          </div>
        </div>
      </div>
      <div tw="hidden">
        <ShareBtns ref={shareBtnRef} type="need" specialObjId={need.id} url={needUrl(need.id)} />
      </div>
      {cardFormat !== 'inPosts' && (
        <div tw="absolute top-2 right-2">
          <Menu>
            <MenuButton tw="rounded-full bg-gray-200 bg-opacity-80 h-6 w-6 p-1 flex justify-center items-center text-[.7rem] text-gray-700">
              •••
            </MenuButton>
            <MenuList
              portal={false}
              className="post-manage-dropdown"
              tw="absolute rounded-lg divide-y divide-gray-300 text-center text-sm"
            >
              {(need.created_by_user_id === user?.id || isAdmin(need.entity)) && (
                <MenuItem onSelect={() => router.push(`${needFrontUrl}?mode=update`)}>{t('action.edit')}</MenuItem>
              )}
              {(need.created_by_user_id === user?.id || isAdmin(need.entity)) && onDelete && (
                <MenuItem onSelect={() => onDelete && onDelete(need)}>{t('action.delete')}</MenuItem>
              )}
              <MenuItem onSelect={() => shareBtnRef?.current?.click()}>{t('action.share')}</MenuItem>
            </MenuList>
          </Menu>
        </div>
      )}
    </ObjectCard>
  );
};

const Hr = (props) => <div tw="-mx-4 border-0 border-t border-solid border-color[rgba(0, 0, 0, 0.07)]" {...props} />;

export default NeedCard;
