import { MessageOutlined, MoreOutlined } from '@ant-design/icons';
import { Badge, Button, Dropdown, Space, MenuProps, Tag, theme, Tooltip } from 'antd';
import dayjs from 'dayjs';
import useTranslation from 'next-translate/useTranslation';
import { FeedStatModel, NeedModel, Permission, UserMiniModel } from '~/__generated__/types';
import { AlignType } from 'rc-table/lib/interface';
import { TableStyled } from '../../Common/table.styles';
import Loader from '../../Common/loader/loader';
import useNeeds from '../useNeeds';
import { ContainerLink } from '../../Common/links/containerLink/containerLink';
import { UserLink } from '../../Common/links/userLink/userLink';
import { useFeedEntity } from '../../Common/feedEntity/feedEntity.hooks';

interface Props {
  needs: NeedModel[];
  loading: boolean;
  hideColumns: string[];
  canManage?: boolean;
  onDelete: (need: NeedModel) => void;
}

const NeedTable = ({ needs, loading = false, hideColumns = [], canManage, onDelete }: Props) => {
  const { t } = useTranslation('common');
  const { token } = theme.useToken();
  const [goToNeed] = useNeeds();
  const [markFeedAsOpened] = useFeedEntity();

  const columns = [
    {
      dataIndex: 'title',
      title: t('title'),
      ellipsis: true,
      render: (title: string) => {
        return (
          <Tooltip title={title} placement="topLeft">
            <Space>{title}</Space>
          </Tooltip>
        );
      },
    },
    {
      dataIndex: 'type',
      title: t('type'),
      width: 120,
      render: (type: string) => {
        return type ? <Tag>{t(`need.type.${type}`)}</Tag> : '';
      },
    },
    {
      dataIndex: 'entity',
      title: t('space.one'),
      ellipsis: true,
      render: (entity: NeedModel['entity']) => {
        return <ContainerLink container={entity} />;
      },
    },
    {
      dataIndex: 'updated',
      title: t('modified'),
      width: 120,
      render: (updated: string) => {
        return updated ? dayjs(updated).format('DD.MM.YYYY') : '';
      },
    },
    {
      dataIndex: 'created_by',
      title: t('createdBy'),
      width: 120,
      render: (created_by: UserMiniModel) => {
        return <UserLink user={created_by} />;
      },
    },
    {
      dataIndex: 'feed_stats',
      align: 'left' as AlignType,
      width: 80,
      render: (feed_stats: FeedStatModel, item: NeedModel) => {
        return (
          <Button onClick={() => goToNeed(item.id, 'discussion')} type="text">
            <Badge count={feed_stats.new_post_count} size="small" offset={[4, -4]} color={token.colorPrimary}>
              <Space>
                <MessageOutlined />
                {feed_stats.post_count}
              </Space>
            </Badge>
          </Button>
        );
      },
    },
    {
      dataIndex: 'id',
      align: 'right' as AlignType,
      width: 80,
      render: (id: string, item: NeedModel) => {
        if (!canManage) return null;
        const menuItems: MenuProps['items'] = [
          ...(item.permissions?.includes(Permission.Manage)
            ? [
                {
                  label: t('action.edit'),
                  key: 'edit',
                  onClick: (e) => {
                    e.domEvent.stopPropagation();
                    goToNeed(item.id);
                  },
                },
              ]
            : []),

          ...(item.permissions?.includes(Permission.Delete)
            ? [
                {
                  label: t('action.delete'),
                  key: 'delete',
                  danger: true,
                  onClick: (e) => {
                    e.domEvent.stopPropagation();
                    onDelete(item);
                  },
                },
              ]
            : []),
        ];

        if (menuItems.length === 0) return <></>;

        return (
          <Dropdown
            trigger={['click']}
            placement="bottomRight"
            menu={{
              onClick: ({ domEvent }) => {
                domEvent.stopPropagation();
              },
              items: menuItems,
            }}
          >
            <Button
              type="text"
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
              }}
            >
              <MoreOutlined />
            </Button>
          </Dropdown>
        );
      },
    },
  ];

  if (loading === true) return <Loader />;

  return (
    <TableStyled
      rowClassName={(record: NeedModel) => (record.is_new ? 'new' : '')}
      dataSource={needs}
      columns={columns.filter((col) => !hideColumns.find((hideCol) => col.dataIndex === hideCol))}
      pagination={false}
      onRow={(record: NeedModel) => ({
        onClick: () => {
          goToNeed(record.id);
          markFeedAsOpened(record);
        },
      })}
    />
  );
};

export default NeedTable;
