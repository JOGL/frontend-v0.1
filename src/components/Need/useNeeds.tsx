import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import { FeedEntityFilter, SortKey } from '~/__generated__/types';

const useNeeds = (): [
  (id: string, tab?: string) => void,
  (key: string) => string
] => {
  const router = useRouter();
  const { t } = useTranslation('common');

  const goToNeed = (id: string, tab?: string) => {
    if (tab) router.push(`/need/${id}?tab=${tab}`);
    else router.push(`/need/${id}`);
  };

  const getNeedViewTitle = (key: string): string => {
    switch (key) {
      case 'shared':
        return t('sharedWithYou');
      case 'created':
        return t('createdByYou');
      case 'recent':
        return t('need.recent');
      case 'all':
      default:
        return t('need.all');
    }
  };

  return [goToNeed, getNeedViewTitle];
};

export const getNeedViewFilter = (key: string): { sortKey: SortKey; filter?: FeedEntityFilter } => {
  switch (key) {
    case 'shared':
      return { sortKey: SortKey.Updateddate, filter: FeedEntityFilter.Sharedwithuser};
    case 'created':
      return { sortKey: SortKey.Updateddate, filter: FeedEntityFilter.Createdbyuser};
    case 'recent':
      return { sortKey: SortKey.Recentlyopened, filter: FeedEntityFilter.Openedbyuser };
    case 'all':
    default:
      return { sortKey: SortKey.Updateddate };
  }
};

export default useNeeds;
