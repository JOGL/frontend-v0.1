import React, { FC, useState } from 'react';
import { Card, Row, Col, Typography, Flex, theme, message, Modal, Space, Form, ConfigProvider } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useMutation } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import {
  CommunityEntityChannelModel,
  EntityMiniModel,
  FeedEntityVisibility,
  NeedType,
  NeedUpsertModel,
  NodeFeedDataModel,
  Permission,
} from '~/__generated__/types';
import { useStonePathStore } from '~/src/contexts/stonepath/StonePathStoreProvider';
import useNeeds from '../useNeeds';
import ContainerPicker from '../../Common/container/containerPicker/containerPicker';
import { Container } from '../../Common/container/containerPicker/containerPicker.types';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import { Disabled } from '../../Common/disabled/disabled';

const CreateNeedModal: FC<{
  selectedSpace?: CommunityEntityChannelModel | NodeFeedDataModel | EntityMiniModel;
  closeModal: () => void;
}> = ({ closeModal, selectedSpace: space }) => {
  const { stonePath } = useStonePathStore();
  const [selectedPrivacy, setSelectedPrivacy] = useState(null);
  const { t } = useTranslation('common');
  const { token } = theme.useToken();
  const [goToNeed] = useNeeds();
  const [selectedContainer, setSelectedContainer] = useState<Container | undefined>(space);
  const { selectedHub } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
  }));

  const mutation = useMutation({
    mutationFn: async (needData: NeedUpsertModel) => {
      if(!selectedContainer)
        return null;

      const response = await api.needs.needsCreate(selectedContainer.id, needData);
      return response.data;
    },
    onSuccess: (id) => {
      id && goToNeed(id);
      closeModal();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
      closeModal();
    },
  });

  const privacyOptions = [
    {
      key: 'public',
      title: t('public'),
      description: t('needPrivacyPublic'),
    },
    {
      key: 'open',
      title: `${t('open')}`,
      description: t('needPrivacyOpen', { entityName: selectedContainer?.title || '' }),
    },
    {
      key: 'private',
      title: t('private'),
      description: t('needPrivacyPrivate'),
    },
  ];

  const handleCardClick = (privacy) => {
    setSelectedPrivacy(privacy);
  };

  const handleNext = () => {
    if (selectedPrivacy) {
      const preSelectedVisibility = selectedPrivacy === 'public' ? FeedEntityVisibility.Comment : undefined;
      const preSelectedCommunityVisibility =
        selectedPrivacy === 'open'
          ? stonePath?.map((item) => {
              return {
                visibility: FeedEntityVisibility.Comment,
                community_entity_id: item.id,
              };
            })
          : null;
      const data = {
        title: t('untitledNeed'),
        type: NeedType.Equipment,
      };

      mutation.mutate({
        ...data,
        default_visibility: preSelectedVisibility,
        communityentity_visibility: preSelectedCommunityVisibility,
      });
    }
  };

  return (
    <Modal
      title={t('action.addItem', { item: t('need.one') })}
      open
      onCancel={closeModal}
      okText={t('action.next')}
      okButtonProps={{
        disabled: !selectedPrivacy || !selectedContainer,
        type: 'primary',
        loading: mutation.isPending,
      }}
      onOk={handleNext}
      centered
      width={800}
      styles={{
        body: {
          padding: token.paddingLG,
        },
        footer: {
          marginTop: token.padding,
          paddingTop: token.padding,
        },
      }}
    >
      <Flex vertical gap="large">
        <ContainerPicker
          permission={Permission.Postneed}
          hubId={selectedHub?.id}
          onChange={(container) => setSelectedContainer(container)}
          value={selectedContainer?.id}
        />
        <Disabled disabled={!selectedContainer}>
          <Row gutter={[16, 16]}>
            {privacyOptions.map((option) => (
              <Col xs={24} sm={24} md={8} key={option.key}>
                <Card
                  hoverable
                  onClick={() => handleCardClick(option.key)}
                  style={{
                    height: 220,
                    ...(selectedPrivacy === option.key && {
                      borderColor: token.colorPrimary,
                    }),
                  }}
                >
                  <Flex vertical gap="small">
                    <Typography.Paragraph strong ellipsis={{ tooltip: option.title, rows: 2 }}>
                      {option.title}
                    </Typography.Paragraph>

                    <Typography.Paragraph type="secondary" ellipsis={{ tooltip: option.description, rows: 5 }}>
                      {option.description}
                    </Typography.Paragraph>
                  </Flex>
                </Card>
              </Col>
            ))}
          </Row>
        </Disabled>
      </Flex>
    </Modal>
  );
};

export default CreateNeedModal;
