import { Form, Input, Modal } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { DocumentOrFolderModel } from '~/__generated__/types';

const CREATE_FOLDER_FORM = 'create-folder-form';

interface Props {
  folder: DocumentOrFolderModel | null;
  onClose: () => void;
  onConfirm: (name: string) => void;
}

const FolderModal = ({ folder, onClose, onConfirm }: Props) => {
  const { t } = useTranslation('common');
  const [form] = Form.useForm<{ name: string }>();
  return (
    <Modal
      open
      title={folder ? t('renameFolder') : t('action.createFolder')}
      okButtonProps={{ htmlType: 'submit', form: CREATE_FOLDER_FORM }}
      okText={folder ? t('action.save') : t('action.create')}
      onCancel={onClose}
    >
      <Form
        form={form}
        layout="vertical"
        id={CREATE_FOLDER_FORM}
        onFinish={({ name }) => onConfirm(name)}
        initialValues={{ name: folder?.title ?? '' }}
      >
        <Form.Item name="name" label={t('folderName')}>
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  );
};
export default FolderModal;
