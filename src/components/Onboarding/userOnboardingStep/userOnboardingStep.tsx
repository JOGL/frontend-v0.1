import { Button, Flex } from 'antd';
import React, { FC, ReactElement } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { FlexFullHeightStyled } from '../../Common/common.styles';
import { OnboardingSubtitleStyled, OnboardingTitleStyled } from '../userOnboarding/userOnboarding.styles';
import { OnboardingData, OnboardingStepProps } from '../userOnboarding/userOnboarding.types';
import { ButtonBackStyled } from './userOnboardingStep.styles';
import { useOnboardingStore } from '../userOnboardingStore/userOnboardingStoreProvider';
import { ArrowLeftOutlined } from '@ant-design/icons';

export interface OnboardingProps {
  onBack?(data: OnboardingData): void;
  onNext?(data: OnboardingData): void;
  onSkip?(data: OnboardingData): void;
  children: ReactElement<OnboardingStepProps>;
  title: string;
  subtitle: string;
  disableNext?: boolean;
}

const UserOnboardingStep: FC<OnboardingProps> = (props) => {
  const { t } = useTranslation('common');
  const data = useOnboardingStore((state) => state.data);

  return (
    <FlexFullHeightStyled vertical gap="large">
      <div>
        {props.onBack && (
          <ButtonBackStyled type="link" onClick={() => props.onBack && props.onBack(data)} icon={<ArrowLeftOutlined />}>
            {t('back')}
          </ButtonBackStyled>
        )}
        <OnboardingTitleStyled>{props.title}</OnboardingTitleStyled>
        <OnboardingSubtitleStyled>{props.subtitle}</OnboardingSubtitleStyled>
      </div>
      {props.children}
      <Flex gap="large">
        {props.onNext && (
          <Button onClick={() => props.onNext && props.onNext(data)} type="primary" disabled={props.disableNext}>
            {t('next')}
          </Button>
        )}
        {props.onSkip && (
          <Button onClick={() => props.onSkip && props.onSkip(data)} type="default">
            {t('skip')}
          </Button>
        )}
      </Flex>
    </FlexFullHeightStyled>
  );
};

export default UserOnboardingStep;
