import useTranslation from 'next-translate/useTranslation';
import Carousel from '~/src/components/Common/Carousel/Carousel';
import { useModal } from '~/src/contexts/modalContext';
import Button from '../primitives/Button';
import FormTextAreaComponent from '../Tools/Forms/FormTextAreaComponent';
import BtnJoin from '../Tools/BtnJoin';
import { useState } from 'react';
import { useApi } from '~/src/contexts/apiContext';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import { confAlert } from '~/src/utils/utils';
import { Flex, Radio } from 'antd';

const ShowOnboarding = ({ object, itemType, type, setIsOnboardingComplete }) => {
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const api = useApi();

  const markOnboardingAsDone = () => {
    api.post(`/communityEntities/${object.id}/onboarding/complete`).then((res) => {
      closeModal();
      confAlert.fire({ icon: 'success', title: `${t('welcome')}!` });
      setIsOnboardingComplete(true);
    });
  };

  // ----- FIRST ONBOARDING -----

  const AnswerQuestions = () => {
    const [answers, setAnswers] = useState([]);
    const handleChange: (key: number, content: string) => void = (key, content) => {
      setAnswers((prevAnswers) => ({ ...prevAnswers, [key]: content })); // update fields as user changes them
    };
    // actually submit the questions
    const handleSubmit = () => {
      const items = [];
      object.onboarding.questionnaire.items.map((question, i) =>
        items.push({ question: question.question, answer: answers?.[i] })
      );
      api.post(`/communityEntities/${object.id}/onboarding`, { items: items }).then(() => {
        setIsOnboardingComplete(false);
        //setTimeout(() => location.reload(), 500);
      });
    };

    return (
      <div>
        <p tw="mb-8">{t('answerQuestionsFromSpaceAdminsToHelp')}</p>
        {object.onboarding.questionnaire.items.map((question, i) => (
          <FormTextAreaComponent
            content={answers?.i}
            id={i}
            placeholder={t('enterYourAnswer')}
            maxChar={340}
            onChange={handleChange}
            rows={5}
            title={question.question}
            key={i}
          />
        ))}
        <BtnJoin
          joinType={object.joining_restriction}
          isPrivate={object.content_privacy === 'private'}
          itemType={itemType as any}
          itemId={object.id}
          textJoin={t('action.submitAndContinue')}
          onJoin={handleSubmit}
          hasNoStat
          isDisabled={
            !answers ||
            Object.keys(answers).length !== object.onboarding.questionnaire.items.length ||
            !Object.values(answers).every((x) => x !== '') // check that all answer fields are filled
          }
        />
      </div>
    );
  };

  // ----- SECOND ONBOARDING -----

  const Slides = () => {
    return (
      <Carousel fullSize noPadding noDotsPadding>
        {object.onboarding.presentation.items.map((slide, i) => (
          <section tw="flex flex-col items-center justify-center mx-10 -mt-1 w-full" key={i}>
            {/* {slide.image_url && (
              <img tw="max-w-xl h-64 mb-8" src={slide.image_url} alt="Carousel slide img" loading="lazy" />
            )} */}
            {/* {slide.image_id && (
              <img
                tw="max-w-xl h-80 mb-8"
                src={`https://jogl-dev-api.azurewebsites.net/images/${slide.image_id}/tn`}
                alt="Carousel slide img"
                loading="lazy"
              />
            )} */}
            <div>
              <p tw="font-bold mt-2 text-[20px] text-center">{slide.title || 'Some tile'}</p>
              <div tw="flex items-center h-[240px] mt-2 mb-8">
                <p tw="text-center">{slide.text}</p>
              </div>
              {i === object.onboarding.presentation.items.length - 1 && (
                <Button
                  tw="flex mx-auto"
                  onClick={() => {
                    type === 'second_onboarding' && !object.onboarding.rules.enabled
                      ? markOnboardingAsDone()
                      : showModal({
                          children: <Rules />,
                          title: t('spaceRules'),
                          maxWidth: '40rem',
                        });
                  }}
                >
                  {t('action.next')}
                </Button>
              )}
            </div>
          </section>
        ))}
      </Carousel>
    );
  };

  const Rules = () => {
    const [hasReadRules, setHasReadRules] = useState(false);
    return (
      <div>
        <p tw="font-bold">{t('makeSureYouHaveTakenIntoAccountTheSpecificRules')}.</p>

        <InfoHtmlComponent content={object.onboarding.rules.text} />
        <Flex gap="small">
          <label htmlFor="read">{t('iHaveReadTheRules')}</label>
          <Radio
            id="read"
            name="read"
            value="read"
            onChange={() => setHasReadRules(!hasReadRules)}
            checked={hasReadRules}
          />
        </Flex>

        <Button tw="mt-6" disabled={!hasReadRules} onClick={markOnboardingAsDone}>
          {t('action.continue')}
        </Button>
      </div>
    );
  };

  return type === 'first_onboarding' ? (
    <AnswerQuestions />
  ) : object.onboarding.presentation?.enabled ? (
    <Slides />
  ) : (
    object.onboarding.rules?.enabled && <Rules />
  );
};

export default ShowOnboarding;
