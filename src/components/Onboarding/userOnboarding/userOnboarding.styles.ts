import styled from '@emotion/styled';
import { Flex, Typography } from 'antd';
import Image from 'next/image';

export const ContentStyled = styled.div<{ isExiting: boolean }>`
  animation: ${(props) => (props.isExiting ? 'slideLeft' : 'slideRight')} 0.2s ease-in forwards;
  width: 100%;
  animation: ${(props) => (props.isExiting ? 'slideLeft' : 'slideRight')} 0.2s ease-in forwards;
  width: 100%;
  @keyframes slideRight {
    from {
      opacity: 0;
      transform: translateX(20px);
    }
    to {
      opacity: 1;
      transform: translateX(0);
    }
  }

  @keyframes slideLeft {
    from {
      opacity: 1;
      transform: translateX(0);
    }
    to {
      opacity: 0;
      transform: translateX(-20px);
    }
  }
`;

export const OnboardingTitleStyled = styled(Typography)`
  padding: 0;
  font-size: ${({ theme }) => theme.token.fontSizeXL}px;
`;

export const OnboardingSubtitleStyled = styled(Typography)`
  padding: 0;
  padding: 0;
  font-size: ${({ theme }) => theme.token.fontSizeXL}px;
  font-weight: bold;
`;

export const LogoStyled = styled(Image)`
  margin: ${({ theme }) => theme.token.margin}px;
`;

export const WelcomeTextStyled = styled(Typography.Title)`
  margin-top: 230px !important;
  margin-left: ${({ theme }) => theme.token.margin}px !important;
  padding: 0;
`;

export const FlexContainerStyled = styled(Flex)`
  min-height: 100vh;
  overflow-y: auto;
`;

export const OnboardingLeftStyled = styled(Flex)`
  width: 460px;
  min-width: 460px;
  background-color: ${({ theme }) => theme.token.neutral4};
  padding: ${({ theme }) => theme.token.padding}px;
  flex-direction: column;
`;

export const OnboardingRightStyled = styled(Flex)`
  flex: 1;
  max-width: 60%;
  margin: 0 auto;
  display: flex;
  align-items: center;
  min-height: 100vh;
  margin-top: ${({ theme }) => theme.token.padding}px;
  padding: ${({ theme }) => theme.token.paddingMD}px 0;

  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    padding: ${({ theme }) => theme.token.paddingXS}px ${({ theme }) => theme.token.padding}px;
    max-width: 100%;
    align-items: baseline;
  }
`;
