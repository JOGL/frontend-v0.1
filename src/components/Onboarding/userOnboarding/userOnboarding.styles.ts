import styled from '@emotion/styled';
import { Flex, Typography } from 'antd';
import Image from 'next/image';

export const ContentStyled = styled.div<{ isExiting: boolean }>`
  animation: ${(props) => (props.isExiting ? 'slideLeft' : 'slideRight')} 0.2s ease-in forwards;
  margin: ${({ theme }) => theme.token.paddingMD}px;
  flex: 1;
  @keyframes slideRight {
    from {
      opacity: 0;
      transform: translateX(20px);
    }
    to {
      opacity: 1;
      transform: translateX(0);
    }
  }

  @keyframes slideLeft {
    from {
      opacity: 1;
      transform: translateX(0);
    }
    to {
      opacity: 0;
      transform: translateX(-20px);
    }
  }
`;

export const OnboardingTitleStyled = styled(Typography)`
  padding: 0;
  font-size: ${({ theme }) => theme.token.fontSizeXL}px;
`;

export const OnboardingSubtitleStyled = styled(Typography)`
  padding: 0;
  padding: 0;
  font-size: ${({ theme }) => theme.token.fontSizeXL}px;
  font-weight: bold;
`;

export const LogoStyled = styled(Image)`
  margin: ${({ theme }) => theme.token.margin}px;
`;

export const WelcomeMobileStyled = styled(Flex)`
  background-color: ${({ theme }) => theme.token.neutral4};
  padding: ${({ theme }) => theme.token.padding}px;
  h2 {
    margin-bottom: 0;
  }
`;

export const WelcomeTextStyled = styled(Typography.Title)`
  margin-top: 230px !important;
  margin-left: ${({ theme }) => theme.token.margin}px !important;
  padding: 0;
`;

export const FlexContainerStyled = styled(Flex)`
  min-height: 100vh;
  height: 100vh;
  overflow: hidden;
  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    min-height: calc(100vh - 70px);
    height: calc(100vh - 70px);
  }
`;

export const OnboardingLeftStyled = styled(Flex)`
  width: 460px;
  min-width: 460px;
  background-color: ${({ theme }) => theme.token.neutral4};
  padding: ${({ theme }) => theme.token.padding}px;
  flex-direction: column;
`;

export const OnboardingRightStyled = styled(Flex)`
  flex: 1;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow-y: auto;

  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    padding: ${({ theme }) => theme.token.paddingXS}px ${({ theme }) => theme.token.padding}px;
    max-width: 100%;
  }
`;
