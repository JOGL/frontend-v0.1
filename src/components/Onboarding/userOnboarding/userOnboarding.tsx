import { Flex, message, Typography } from 'antd';
import React, { useEffect, useMemo, useState } from 'react';
import { useMutation, useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import useUser from '~/src/hooks/useUser';
import { FINISH, OnboardingData, OnboardingSteps } from './userOnboarding.types';
import {
  ContentStyled,
  OnboardingLeftStyled,
  OnboardingRightStyled,
  LogoStyled,
  WelcomeTextStyled,
  FlexContainerStyled,
  WelcomeMobileStyled,
} from './userOnboarding.styles';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import UserOnboardingStep from '../userOnboardingStep/userOnboardingStep';
import { useOnboardingStore } from '../userOnboardingStore/userOnboardingStoreProvider';
import Loader from '../../Common/loader/loader';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import useMobile from '~/src/hooks/useMobile';
import Image from 'next/image';

const UserOnboarding = () => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const [currentStepKey, setCurrentStepKey] = useState(Object.keys(OnboardingSteps)[0]);
  const { user } = useUser();
  const [isExiting, setIsExiting] = useState(false);
  const updateData = useOnboardingStore((state) => state.updateData);
  const data = useOnboardingStore((state) => state.data);
  const commitData = useOnboardingStore((state) => state.commit);
  const rollbackData = useOnboardingStore((state) => state.rollback);
  const isMobile = useMobile();
  const [redirectUrl] = useState(router.query.redirectUrl as string);
  const [showLoader, setShowLoader] = useState(false);

  const goTo = (next: string) => {
    setIsExiting(true);
    setTimeout(() => {
      setCurrentStepKey(next);
      setIsExiting(false);
    }, 200);
  };

  const { data: hasInvites } = useQuery({
    queryKey: [QUERY_KEYS.userHasInvites, user?.id],
    queryFn: async () => {
      const response = await api.users.currentNotificationsPendingList(user.id);
      return response.data;
    },
    enabled: !!user,
  });

  const redirectAfterOnboarding = () => {
    if (redirectUrl) router.push(redirectUrl);
    if (hasInvites === true) router.push(`/actions`);
    else router.push(`/user/${user.id}`);
  };

  const { data: userData } = useQuery({
    queryKey: [QUERY_KEYS.userData, user?.id],
    queryFn: async () => {
      const response = await api.users.usersDetailDetail(user.id);
      return response.data;
    },
    enabled: !!user,
  });

  const storeOnboardingMutation = useMutation({
    mutationKey: [MUTATION_KEYS.userUnarchive],
    mutationFn: async (data: OnboardingData) => {
      setShowLoader(true);
      const response = await api.users.onboardingCreate({
        paperIds: data.papers,
        repos: data.repos,
        education: data.educationRecords,
        experience: data.experienceRecords,
        github_access_token: data.gh_token,
        huggingface_access_token: data.hf_token,
      });
      return { status: response.status };
    },
    onSuccess: () => {
      redirectAfterOnboarding();
    },
    onError: (error: Error) => {
      console.log('error', error);
      message.error(t('error.somethingWentWrong'));
    },
  });

  
  const skipOnboardingMutation = useMutation({
    mutationKey: [MUTATION_KEYS.userUnarchive],
    mutationFn: async (data: OnboardingData) => {
      setShowLoader(true);
      const response = await api.users.onboardingStatusCreate();
      return { status: response.status };
    },
    onSuccess: () => {
      redirectAfterOnboarding();
    },
    onError: (error: Error) => {
      console.log('error', error);
      message.error(t('error.somethingWentWrong'));
    },
  });

  useEffect(() => {
    if (userData) {
      updateData({
        first_name: userData.first_name,
        last_name: userData.last_name,
        educationRecords: userData.education,
        experienceRecords: userData.experience,
      });
    }
  }, [userData, updateData]);

  const currentStep = useMemo(() => {
    return OnboardingSteps[currentStepKey];
  }, [currentStepKey]);

  if (!userData) return <Loader />;
  return (
    <>
      {isMobile && (
        <WelcomeMobileStyled gap="middle" align="center">
          <Image src="/images/JOGL_logo.png" alt="Logo" width={35} height={35} priority />
          <Typography.Title level={2}>{t('welcomeToJogl')}</Typography.Title>
        </WelcomeMobileStyled>
      )}
      <FlexContainerStyled gap={0}>
        {!isMobile && (
          <OnboardingLeftStyled>
            <LogoStyled src="/images/jogl-color-logo-small.png" alt="Logo" width={117} height={40} priority />
            <WelcomeTextStyled level={1}>{t('welcomeToJogl')}</WelcomeTextStyled>
          </OnboardingLeftStyled>
        )}

        <OnboardingRightStyled>
          <ContentStyled isExiting={isExiting}>
            {showLoader ? (
              <Flex vertical gap="middle" justify="center" align="center" style={{ height: '100vh' }}>
                <Loader />
                <Typography.Title level={4}>{t('userOnboarding.preparingYourProfile')}</Typography.Title>
              </Flex>
            ) : (
              <UserOnboardingStep
                title={t(currentStep.title, { name: data.first_name })}
                subtitle={t(currentStep.subtitle)}
                subtitleNotFound={currentStep.subtitleNotFound ? t(currentStep.subtitleNotFound) : undefined}
                showNotFound={data.noWorksFound}
                loading={data.isLoading}
                onBack={
                  currentStep.back
                    ? () => {
                        rollbackData();
                        const previousKey = currentStep.back(data);
                        goTo(previousKey);
                      }
                    : undefined
                }
                disableNext={currentStepKey === 'feats' && (!data.feats || data.feats.length === 0)}
                onNext={
                  currentStep.next
                    ? () => {
                        commitData();
                        const nextKey = currentStep.next(data);
                        if (nextKey === FINISH) {
                          storeOnboardingMutation.mutate(data);
                        } else {
                          goTo(nextKey);
                        }
                      }
                    : undefined
                }
                onSkip={
                  currentStep.skip
                    ? () => {
                        rollbackData();

                        const nextKey = currentStep.skip(data);
                        if (nextKey === FINISH) {
                          skipOnboardingMutation.mutate(data);
                        } else {
                          goTo(nextKey);
                        }
                      }
                    : undefined
                }
              >
                {currentStep.render({ goTo: goTo })}
              </UserOnboardingStep>
            )}
          </ContentStyled>
        </OnboardingRightStyled>
      </FlexContainerStyled>
    </>
  );
};

export default UserOnboarding;
