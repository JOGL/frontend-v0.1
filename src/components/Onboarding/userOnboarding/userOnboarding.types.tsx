import { UserEducationModel, UserExperienceModel } from '~/__generated__/types';
import UserOnboardingCode from '../userOnboardingCode/userOnboardingCode';
import UserOnboardingCodeGithub from '../userOnboardingCodeGithub/userOnboardingCodeGithub';
import UserOnboardingCodeHuggingFace from '../userOnboardingCodeHuggingFace/userOnboardingCodeHuggingFace';
import UserOnboardingFeats from '../userOnboardingFeats/userOnboardingFeats';
import UserOnboardingLinkedin from '../userOnboardingLinkedin/userOnboardingLinkedin';
import UserOnboardingLinkedInExperience from '../userOnboardingLinkedInExperience/userOnboardingLinkedInExperience';
import UserOnboardingLinkedInEducation from '../userOnboardingLinkedInEducation/userOnboardingLinkedInEducation';
import UserOnboardingScienceName from '../userOnboardingScience/userOnboardingScienceName/userOnboardingScienceName';
import UserOnboardingSciencePapers from '../userOnboardingScience/userOnboardingSciencePapers/userOnboardingSciencePapers';
import UserOnboardingScienceAuthor from '../userOnboardingScience/userOnboardingScienceAuthor/userOnboardingScienceAuthor';
export const FINISH = 'FINISH';

export const Feats = [
  {
    key: 'misc',
    translationId: 'userOnboarding.feat.misc',
    translationIdSubtitle: 'userOnboarding.feat.miscSubtitle',
  },
  {
    key: 'science',
    translationId: 'userOnboarding.feat.science',
    translationIdSubtitle: 'userOnboarding.feat.scienceSubtitle',
  },
  {
    key: 'code',
    translationId: 'userOnboarding.feat.code',
    translationIdSubtitle: 'userOnboarding.feat.codeSubtitle',
  },
];

export const OnboardingSteps = {
  feats: {
    title: 'userOnboarding.intro',
    subtitle: 'userOnboarding.introSubtitle',
    render: (props: OnboardingStepProps) => <UserOnboardingFeats goTo={props.goTo} />,
    next: (data: OnboardingData) => {
      if (data.feats.includes('misc')) return 'linkedin_connect';
      if (data.feats.includes('science')) return 'science_name';
      if (data.feats.includes('code')) return 'code_connect';
      return FINISH;
    },
    skip: () => FINISH,
  },
  linkedin_connect: {
    title: 'userOnboarding.linkedin.connect',
    subtitle: 'userOnboarding.linkedin.connectSubtitle',
    render: (props: OnboardingStepProps) => <UserOnboardingLinkedin goTo={props.goTo} />,
    back: () => 'feats',
    next: (data: OnboardingData) => {
      if (data.feats.includes('science')) return 'science_name';
      if (data.feats.includes('code')) return 'code_connect';
      return FINISH;
    },
  },
  linkedin_exp: {
    title: 'userOnboarding.linkedin.exp',
    subtitle: 'userOnboarding.linkedin.expSubtitle',
    render: (props: OnboardingStepProps) => <UserOnboardingLinkedInExperience goTo={props.goTo} />,
    back: () => 'linkedin_connect',
    next: () => 'linkedin_edu',
    skip: () => 'linkedin_edu',
  },
  linkedin_edu: {
    title: 'userOnboarding.linkedin.edu',
    subtitle: 'userOnboarding.linkedin.eduSubtitle',
    render: (props: OnboardingStepProps) => <UserOnboardingLinkedInEducation goTo={props.goTo} />,
    back: () => 'linkedin_connect',
    next: () => 'linkedin_connect',
    skip: () => 'linkedin_connect',
  },

  science_name: {
    title: 'userOnboarding.science.name',
    subtitle: 'userOnboarding.science.nameSubtitle',
    render: (props: OnboardingStepProps) => <UserOnboardingScienceName goTo={props.goTo} />,
    back: (data: OnboardingData) => {
      if (data.feats.includes('misc')) return 'linkedin_connect';
      return 'feats';
    },
    next: () => 'science_author',
    skip: () => 'science_papers',
  },
  science_author: {
    title: 'userOnboarding.science.author',
    subtitle: 'userOnboarding.science.authorSubtitle',
    subtitleNotFound: 'userOnboarding.science.papersNotFound',
    render: (props: OnboardingStepProps) => <UserOnboardingScienceAuthor goTo={props.goTo} />,
    back: () => 'science_name',
    next: (data: OnboardingData) => {
      if (data.author_ids && data.author_ids.length > 0) return 'science_papers';
      if (data.feats.includes('code')) return 'code_connect';
      return FINISH;
    },
    skip: () => 'science_papers',
  },
  science_papers: {
    title: 'userOnboarding.science.papers',
    subtitle: 'userOnboarding.science.papersSubtitle',
    subtitleNotFound: 'userOnboarding.science.papersNotFound',
    render: (props: OnboardingStepProps) => <UserOnboardingSciencePapers goTo={props.goTo} />,
    back: () => 'science_author',
    next: (data: OnboardingData) => {
      if (data.feats.includes('code')) return 'code_connect';
      return FINISH;
    },
  },
  code_connect: {
    title: 'userOnboarding.code.connect',
    subtitle: 'userOnboarding.code.connectSubtitle',
    render: (props: OnboardingStepProps) => <UserOnboardingCode goTo={props.goTo} />,
    back: (data: OnboardingData) => {
      if (data.feats.includes('science') && data.author_ids && data.author_ids.length > 0) return 'science_papers';
      if (data.feats.includes('science')) return 'science_author';
      if (data.feats.includes('misc')) return 'linkedin_connect';
      return 'feats';
    },
    next: () => {
      return FINISH;
    },
    skip: () => {
      return FINISH;
    },
  },
  code_gh: {
    title: 'userOnboarding.code.gh',
    subtitle: 'userOnboarding.code.ghSubtitle',
    render: (props: OnboardingStepProps) => <UserOnboardingCodeGithub goTo={props.goTo} />,
    back: () => 'code_connect',
    next: () => 'code_connect',
    skip: () => 'code_connect',
  },
  code_hf: {
    title: 'userOnboarding.code.hf',
    subtitle: 'userOnboarding.code.hfSubtitle',
    render: (props: OnboardingStepProps) => <UserOnboardingCodeHuggingFace goTo={props.goTo} />,
    back: () => 'code_connect',
    next: () => 'code_connect',
    skip: () => 'code_connect',
  },
};

export interface OnboardingData {
  feats: string[];
  first_name: string;
  last_name: string;
  author_id?: string;
  author_ids?: string[];
  papers: string[];
  gh_token?: string;
  hf_token?: string;
  li_token?: string;
  repos: string[];
  linkedin_url?: string;
  experienceRecords: UserExperienceModel[];
  educationRecords: UserEducationModel[];
  noWorksFound?: boolean;
  isLoading: boolean;
}

export interface OnboardingStepProps {
  goTo(key: string, data?: Partial<OnboardingData>): void;
}
