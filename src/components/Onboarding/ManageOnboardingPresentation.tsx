import React, { FC, FormEvent, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Button from '~/src/components/primitives/Button';
import Card from '~/src/components/Cards/Card';
import FormTextAreaComponent from '../Tools/Forms/FormTextAreaComponent';
import Icon from '../primitives/Icon';
import ReactTooltip from 'react-tooltip';
import FormDefaultComponent from '../Tools/Forms/FormDefaultComponent';

interface Presentation {
  title: string;
  text: string;
  // image_url: string;
  // image_id: string;
}

const ManageOnboardingPresentation = ({ items, handleChange }) => {
  const { t } = useTranslation('common');
  const [showPresentationCreate, setShowPresentationCreate] = useState(false);
  const [presentations, setPresentations] = useState(items);
  const addPresentation = () => {
    setShowPresentationCreate(!showPresentationCreate);
  };
  return (
    <div tw="flex flex-col space-y-3">
      <div tw="flex flex-col space-y-3">
        {presentations &&
          [...presentations]
            .sort(function (a, b) {
              return a.id - b.id; // sort by id (asc)
            })
            .map((value, i) => (
              <div tw="flex flex-col pt-3" key={i}>
                <CardFormEdit
                  value={value}
                  onChange={(originalPresentation: Presentation, updatedPresentation) => {
                    const originalPresentationId = presentations.indexOf(originalPresentation);
                    setPresentations(
                      presentations.map((presentation) =>
                        presentations[originalPresentationId] === presentation ? updatedPresentation : presentation
                      )
                    );
                  }}
                  onSubmit={() => {
                    handleChange('presentation', presentations);
                  }}
                  onDelete={(presentationUrl: number) => {
                    const presentationId = presentations.indexOf(presentationUrl);
                    handleChange(
                      'presentation',
                      [...presentations].filter((presentation) => presentations[presentationId] !== presentation)
                    );
                    setPresentations(
                      [...presentations].filter((presentation) => presentations[presentationId] !== presentation)
                    );
                  }}
                />
              </div>
            ))}
      </div>
      <Button onClick={addPresentation}>{t('action.addItem', { item: t('slide') })}</Button>
      {showPresentationCreate && (
        <CardFormCreate
          onCreate={(newPresentation: Presentation) => {
            handleChange('presentation', [...presentations, newPresentation]);
            setPresentations([...presentations, newPresentation]);
            setShowPresentationCreate(false);
          }}
        />
      )}
    </div>
  );
};
interface ICardFormCreate {
  onCreate?: (newPresentation: Presentation) => void;
}
interface ICardFormEdit {
  value: Presentation;
  onDelete?: (presentation: number) => void;
  onChange?: (originalPresentation: Presentation, updatedPresentation: Presentation) => void;
  onSubmit?: (value: Presentation) => void;
}

const CardFormCreate: FC<ICardFormCreate> = ({ onCreate }) => {
  const { t } = useTranslation('common');
  const [newPresentation, setNewPresentation] = useState<Presentation>({
    title: '',
    text: '',
    // image_url: '',
    // image_id: '',
  });
  const [showSubmitBtn, setShowSubmitBtn] = useState(false);

  // handle every changes to the presentation
  const handleChange: (key: string, content: string) => void = (key, content) => {
    setNewPresentation((prevPresentation) => ({ ...prevPresentation, [key]: content }));
    // if key is image_id, change the image_url key of the presentation, else normal key/content
    // if (key === 'image_id') {
    //   setNewPresentation((prevPresentation) => ({
    //     ...prevPresentation,
    //     image_id: content.id,
    //     image_url: content.url,
    //   }));
    // } else setNewPresentation((prevPresentation) => ({ ...prevPresentation, [key]: content }));
    setShowSubmitBtn(content !== newPresentation.text || content !== newPresentation.image_url);
  };
  // handle when submitting (update, or create), an presentation
  const handleSubmit: (event: FormEvent<HTMLFormElement>) => void = (e) => {
    e.preventDefault();
    if (newPresentation.text !== '' && newPresentation.title !== '' && newPresentation.text.length <= 340) {
      onCreate(newPresentation);
      setShowSubmitBtn(false); // hide update button
    } else {
      newPresentation.text.length > 340 ? alert('Text is too long') : alert('Please fill all fields');
    }
  };

  return (
    <Card>
      <div tw="flex flex-col justify-between md:flex-row">
        <div tw="flex flex-col w-full pr-0 md:pr-5">
          <div tw="inline-flex">
            <FormDefaultComponent
              id="title"
              placeholder={t('yourTitle')}
              title={t('title')}
              content={newPresentation.title}
              maxChar={140}
              mandatory
              onChange={handleChange}
            />
          </div>

          <FormTextAreaComponent
            content={newPresentation.text}
            id="text"
            mandatory
            maxChar={340}
            onChange={handleChange}
            rows={5}
            title={t('slideText')}
          />
        </div>
      </div>
      {showSubmitBtn && (
        <div tw="flex flex-col space-y-2 justify-center items-center pt-4 md:pt-0">
          <Button onClick={handleSubmit} width="100%">
            {t('action.save')}
          </Button>
        </div>
      )}
    </Card>
  );
};

const CardFormEdit: FC<ICardFormEdit> = ({ value, onDelete, onChange, onSubmit }) => {
  const { t } = useTranslation('common');
  const [showSubmitBtn, setShowSubmitBtn] = useState(false);
  // handle every changes to the presentation
  const handleChange: (key: string, content: string) => void = (key, content) => {
    // if (key === 'image_id') {
    //   onChange(value, { ...value, image_id: content.id, image_url: content.url });
    // } else onChange(value, { ...value, [key]: content });
    onChange(value, { ...value, [key]: content });
    setShowSubmitBtn(content !== value.text || content !== value.image_url);
  };
  // handle when submitting (update, or create), an presentation
  const handleSubmit: (event: FormEvent<HTMLFormElement>) => void = (e) => {
    e.preventDefault();
    if (value.text !== '' && value.title !== '' && value.text.length <= 600) {
      onSubmit(value); // call onChange function
      setShowSubmitBtn(false); // hide update button
    } else {
      value.text.length > 600 ? alert('Text is too long') : alert('Please fill all fields');
    }
  };
  // handle when deleting on presentation
  const handleDelete = (presentation) => {
    onDelete(presentation); // call onDelete function
  };

  return (
    <Card overflow="show" tw="relative">
      <div tw="flex flex-col justify-between md:flex-row">
        <div tw="flex flex-col w-full pr-0 md:pr-5">
          <FormDefaultComponent
            id="title"
            placeholder={t('yourTitle')}
            title={t('title')}
            content={value.title}
            maxChar={140}
            mandatory
            onChange={handleChange}
          />
          <FormTextAreaComponent
            content={value.text}
            id="text"
            mandatory
            maxChar={600}
            onChange={handleChange}
            rows={5}
            title={t('slideText')}
          />
        </div>
      </div>
      <div tw="flex flex-col space-y-2 justify-center items-center pt-4 md:pt-0">
        {showSubmitBtn && (
          <Button onClick={handleSubmit} width="100%">
            {t('action.update')}
          </Button>
        )}
      </div>
      <div
        tw="absolute top-2 right-2 cursor-pointer"
        data-tip="delete slide"
        data-for="slideTag"
        onClick={() => handleDelete(value)}
      >
        <Icon icon="mdi:trash" tw="w-5 h-5" />
      </div>
      <ReactTooltip
        id="slideTag"
        effect="solid"
        role="tooltip"
        type="dark"
        className="forceTooltipBg"
        aria-haspopup="true"
      />
    </Card>
  );
};

export default ManageOnboardingPresentation;
