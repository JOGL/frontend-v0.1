import styled from '@emotion/styled';
import { Input, Space } from 'antd';

export const InputStyled = styled(Input)`
  height: ${({ theme }) => theme.token.sizeXL};
`;

export const SpaceFullWidthStyled = styled(Space)`
  min-width: 0;
  width: 75%;

  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    width: 100%;
  }
`;
