import { Button, List, Typography, Alert, Flex } from 'antd';
import React, { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { FlexFullWidthStyled } from '~/src/components/Common/common.styles';
import { useOnboardingStore } from '../userOnboardingStore/userOnboardingStoreProvider';
import { OnboardingStepProps } from '../userOnboarding/userOnboarding.types';
import OAuthLogin from '../../OAuth/oAuthLogin/oAuthLogin';
import { FeedIntegrationType } from '~/__generated__/types';
import { InputStyled, SpaceFullWidthStyled } from './userOnboardingCode.styles';
import { DeleteOutlined } from '@ant-design/icons';
import { useValidateIntegrationUrl } from '~/src/hooks/useValidateIntegrationUrl';
import useMobile from '~/src/hooks/useMobile';

const UserOnboardingCode: FC<OnboardingStepProps> = (props) => {
  const { t } = useTranslation('common');
  const data = useOnboardingStore((state) => state.data);
  const updateData = useOnboardingStore((state) => state.updateData);
  const commitData = useOnboardingStore((state) => state.commit);
  const [repoInput, setRepoInput] = useState<string>();
  const [error, setError] = useState<string>();
  const { parseFeedIntegration } = useValidateIntegrationUrl();
  const isMobile = useMobile();

  const addRepo = () => {
    if (!repoInput) return;

    if (data.repos.find((repo) => repo === repoInput)) return;

    const integration = parseFeedIntegration(repoInput);
    if (!integration) {
      setError(t('error.invalidAppURL'));
      return;
    }
    setError(undefined);

    updateData({ repos: [...data.repos, repoInput] });
    setRepoInput('');
  };

  const removeRepo = (repo: string) => {
    // if (!data.repos.find((r) => r === repoInput)) return;

    updateData({ repos: data.repos.filter((r) => r !== repo) });
  };

  return (
    <SpaceFullWidthStyled direction="vertical" size="middle">
      <Flex gap="small" vertical={isMobile} align="center" justify="center">
        <OAuthLogin
          provider={FeedIntegrationType.Github}
          accessToken={data.gh_token}
          onLogin={(token) => {
            updateData({ gh_token: token });
            commitData();
            props.goTo('code_gh');
          }}
          onClick={() => props.goTo('code_gh')}
        />
        <OAuthLogin
          provider={FeedIntegrationType.Huggingface}
          accessToken={data.hf_token}
          onLogin={(token) => {
            updateData({ hf_token: token });
            commitData();
            props.goTo('code_hf');
          }}
          onClick={() => props.goTo('code_hf')}
        />
      </Flex>

      <FlexFullWidthStyled align="center" gap="middle">
        {t('or')}
        <InputStyled
          value={repoInput}
          onChange={(e) => {
            setRepoInput(e.target.value);
            setError(undefined);
          }}
          onBlur={(e) => {
            if (e.target.value) {
              const integration = parseFeedIntegration(e.target.value);
              setError(integration ? undefined : t('error.invalidAppURL'));
            }
          }}
          placeholder="https://github.com/greatrepo"
          allowClear
          status={error ? 'error' : undefined}
        />
        <Button onClick={addRepo}>{t('action.add')}</Button>
      </FlexFullWidthStyled>
      {error && <Alert type="error" message={error} />}
      <List
        dataSource={data.repos}
        renderItem={(item: string) => {
          return (
            <List.Item>
              <FlexFullWidthStyled justify="space-between" align="center">
                <Typography.Text>{item}</Typography.Text>
                <Button onClick={() => removeRepo(item)}>
                  <DeleteOutlined />
                </Button>
              </FlexFullWidthStyled>
            </List.Item>
          );
        }}
      />
    </SpaceFullWidthStyled>
  );
};

export default UserOnboardingCode;
