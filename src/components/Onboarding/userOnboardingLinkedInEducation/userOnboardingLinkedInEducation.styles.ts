import styled from '@emotion/styled';
import { Flex, Menu } from 'antd';

export const MenuStyled = styled(Menu)`
  && .ant-menu-item {
      padding:0;
  }

  && .ant-menu-item:hover {
      background-color: transparent !important;
      color: inherit !important;
    }

  && .ant-menu-item-selected {
      background-color: transparent !important;
      color: inherit !important;
    }
`;

export const MenuItemStyled = styled(Flex)`
`;