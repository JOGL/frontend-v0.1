import { Checkbox, Empty, Typography } from 'antd';
import React, { FC, useMemo } from 'react';
import { useOnboardingStore } from '../userOnboardingStore/userOnboardingStoreProvider';
import { OnboardingStepProps } from '../userOnboarding/userOnboarding.types';
import { useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import Loader from '../../Common/loader/loader';
import { MenuItemStyled, MenuStyled } from './userOnboardingLinkedInEducation.styles';
import { UserEducationModel } from '~/__generated__/types';
import useTranslation from 'next-translate/useTranslation';

const UserOnboardingLinkedInEducation: FC<OnboardingStepProps> = (props) => {
  const { t } = useTranslation('common');
  const data = useOnboardingStore((state) => state.data);
  const updateData = useOnboardingStore((state) => state.updateData);

  const { data: profile, isFetched } = useQuery({
    queryKey: [QUERY_KEYS.userLinkedInData, data.linkedin_url],
    queryFn: async () => {
      const response = await api.users.linkedinProfileList({ linkedInUrl: data.linkedin_url });
      return response.data;
    },
    enabled: !!data.linkedin_url,
  });

  const areEqual = (model1: UserEducationModel, model2: UserEducationModel): boolean => {
    if (!model1 || !model2) return false;

    return model1.school === model2.school && model1.description === model2.description;
  };

  const allSelected = useMemo(() => {
    if (!profile?.education) return false;
    return profile.education.every((item) => data.educationRecords.some((e) => areEqual(e, item)));
  }, [profile?.education, data.educationRecords]);

  if (!profile) return <Loader />;

  const handleSelectAll = () => {
    if (allSelected) {
      updateData({ educationRecords: [] });
    } else {
      updateData({ educationRecords: profile.education });
    }
  };

  if (isFetched && !profile.education.length) {
    return <Empty />;
  }
  return (
    <>
      {profile && (
        <MenuStyled
          items={[
            ...(profile.education.length > 0
              ? [
                  {
                    key: 'select-all',
                    label: (
                      <MenuItemStyled gap="middle" align="flex-start">
                        <Checkbox checked={allSelected} onChange={handleSelectAll} />
                        <Typography.Text strong>{t('selectAll')}</Typography.Text>
                      </MenuItemStyled>
                    ),
                  },
                ]
              : []),
            ...profile.education.map((item, index) => ({
              key: index,
              label: (
                <MenuItemStyled gap="middle" align="flex-start">
                  <Checkbox checked={!!data.educationRecords.find((e) => areEqual(e, item))} />
                  <Typography.Text>{item.school}</Typography.Text>
                </MenuItemStyled>
              ),
              onClick: () => {
                if (data.educationRecords.find((e) => areEqual(e, item))) {
                  const newEducation = data.educationRecords.filter((e) => !areEqual(e, item));
                  updateData({ educationRecords: newEducation });
                } else {
                  const newEducation = [...data.educationRecords, item];
                  updateData({ educationRecords: newEducation });
                }
              },
            })),
          ]}
        />
      )}
    </>
  );
};

export default UserOnboardingLinkedInEducation;
