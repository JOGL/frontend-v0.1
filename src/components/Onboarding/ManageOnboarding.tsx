import useTranslation from 'next-translate/useTranslation';
import { FC } from 'react';
import { Hub, ItemType } from '~/src/types';
import Icon from '../primitives/Icon';
import FormToggleComponent from '../Tools/Forms/FormToggleComponent';
import { NavTab, TabListStyleNoSticky } from '~/src/components/Tabs/TabsStyles';
import { TabPanels, TabPanel as SpecialTabPanel, Tabs } from '@reach/tabs';
import tw from 'twin.macro';
import H2 from '../primitives/H2';
import FormWysiwygComponent from '../Tools/Forms/FormWysiwygComponent';
import ManageOnboardingQuestions from './ManageOnboardingQuestions';
import ManageOnboardingPresentation from './ManageOnboardingPresentation';
import Alert from '../Tools/Alert/Alert';
import Link from 'next/link';
import { entityItem } from '~/src/utils/utils';
import Trans from 'next-translate/Trans';

interface Props {
  object: Hub;
  objectType?: ItemType;
  handleChange: (key: string, content: string) => void;
}

const ManageOnboarding: FC<Props> = ({ object, objectType, handleChange }) => {
  const { t } = useTranslation('common');
  const onboarding = object?.onboarding;

  const handleChangeOnboarding = (id, content) => {
    // change onboarding object according to id
    if (id === 'use_onboarding') onboarding.enabled = content;
    if (id === 'use_presentation') onboarding.presentation.enabled = content;
    if (id === 'presentation') onboarding.presentation.items = content;
    if (id === 'use_questionnaire') onboarding.questionnaire.enabled = content;
    if (id === 'questionnaire') onboarding.questionnaire.items = content;
    if (id === 'use_rules') onboarding.rules.enabled = content;
    if (id === 'rules') onboarding.rules.text = content;

    // then call handleChange function
    handleChange('onboarding', onboarding);
  };

  const uiTxtSelection = entityItem[objectType].front_path;

  return (
    <>
      <div tw="flex px-6 py-3 border border-gray-300 border-solid rounded-md text-gray-800 items-center space-x-6">
        <Icon icon="mdi:funnel" tw="mr-4 text-[#CBCFD6]" />
        {t('wantToUseAnOnboardingForMyContainerMembers', { container: t(entityItem[objectType].translationId) })}
        <FormToggleComponent
          id="use_onboarding"
          choice1={t('no')}
          choice2={t('yes')}
          isChecked={onboarding.enabled}
          onChange={handleChangeOnboarding}
        />
      </div>
      <div css={!onboarding.enabled && tw`cursor-not-allowed pointer-events-none opacity-40`}>
        <Tabs defaultIndex={0}>
          <TabListStyleNoSticky>
            <NavTab>{t('questionnaire.one')}</NavTab>
            <NavTab>{t('presentation.one')}</NavTab>
            <NavTab>{t('rule.one')}</NavTab>
          </TabListStyleNoSticky>
          <TabPanels tw="justify-center">
            {/* Questionnaire */}
            <SpecialTabPanel>
              {t('youCanRequireUsersToAnswerQuestionnaireBeforeJoining')}.
              <div tw="flex mt-2 mb-5">
                <FormToggleComponent
                  id="use_questionnaire"
                  choice1={t('action.disable')}
                  choice2={t('action.enable')}
                  isChecked={onboarding.questionnaire.enabled}
                  onChange={handleChangeOnboarding}
                />
              </div>
              {object.joining_restriction !== 'request' && (
                <Alert
                  type="info"
                  message={
                    <Trans
                      i18nKey="common:toEditQuestionnairePleaseChangeTheMembershipAccess"
                      components={[
                        <Link key={object.id} href={`/${uiTxtSelection}/${object.id}/edit?tab=privacy_security`} />,
                      ]}
                    />
                  }
                />
              )}
              <div
                tw="flex flex-col"
                css={
                  (!onboarding.questionnaire.enabled || object.joining_restriction !== 'request') &&
                  tw`cursor-not-allowed pointer-events-none opacity-40`
                }
              >
                <H2>{t('question.other')}</H2>
                <ManageOnboardingQuestions
                  handleChange={handleChangeOnboarding}
                  items={onboarding.questionnaire.items}
                />
              </div>
            </SpecialTabPanel>
            {/* Presentation */}
            <SpecialTabPanel>
              {t('canUseSliderToIntroduceYourContainerToYourNewMembers', {
                container: t(entityItem[objectType].translationId),
              })}
              .
              <div tw="flex mt-2 mb-5">
                <FormToggleComponent
                  id="use_presentation"
                  choice1={t('action.disable')}
                  choice2={t('action.enable')}
                  isChecked={onboarding.presentation.enabled}
                  onChange={handleChangeOnboarding}
                />
              </div>
              <div
                tw="flex flex-col"
                css={!onboarding.presentation.enabled && tw`cursor-not-allowed pointer-events-none opacity-40`}
              >
                <ManageOnboardingPresentation
                  handleChange={handleChangeOnboarding}
                  items={onboarding.presentation.items}
                />
              </div>
            </SpecialTabPanel>
            {/* Rules */}
            <SpecialTabPanel>
              {t('canDefineTheRulesOfYourContainer', {
                container: t(entityItem[objectType].translationId),
              })}
              .
              <div tw="flex mt-2 mb-5">
                <FormToggleComponent
                  id="use_rules"
                  choice1={t('action.disable')}
                  choice2={t('action.enable')}
                  isChecked={onboarding.rules.enabled}
                  onChange={handleChangeOnboarding}
                />
              </div>
              <div tw="flex" css={!onboarding.rules.enabled && tw`cursor-not-allowed pointer-events-none opacity-40`}>
                <FormWysiwygComponent
                  title={t('describeTheRules')}
                  id="rules"
                  content={onboarding.rules.text}
                  onChange={handleChangeOnboarding}
                  mandatory
                />
              </div>
            </SpecialTabPanel>
          </TabPanels>
        </Tabs>
      </div>
    </>
  );
};
export default ManageOnboarding;
