import { Checkbox, Empty, Typography } from 'antd';
import React, { FC, useMemo } from 'react';
import { useOnboardingStore } from '../userOnboardingStore/userOnboardingStoreProvider';
import { OnboardingStepProps } from '../userOnboarding/userOnboarding.types';
import { useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import Loader from '../../Common/loader/loader';
import { MenuItemStyled, MenuStyled } from './userOnboardingCodeHuggingFace.styles';
import useTranslation from 'next-translate/useTranslation';

const UserOnboardingCodeHuggingFace: FC<OnboardingStepProps> = (props) => {
  const { t } = useTranslation('common');
  const data = useOnboardingStore((state) => state.data);
  const updateData = useOnboardingStore((state) => state.updateData);

  const { data: repositories, isFetched } = useQuery({
    queryKey: [QUERY_KEYS.userRepositories, data.hf_token],
    queryFn: async () => {
      const response = await api.users.huggingfaceReposList({ accessToken: data.hf_token });
      return response.data;
    },
    enabled: !!data.hf_token,
  });

  const allSelected = useMemo(() => {
    if (!repositories) return false;
    return repositories.every((item) => data.repos.includes(item.url));
  }, [repositories, data.repos]);

  const repos = useMemo(() => {
    if (!repositories) return [];
    return repositories.map((item) => item.url);
  }, [repositories]);
  
  const handleSelectAll = () => {
    if (allSelected) {
      updateData({ repos: data.repos?.filter((url) => !repositories?.find((item) => item.url === url)) || [] });
    } else {
      const selectedRepos = data.repos;
      const displayedRepos = repos;
      const uniqueRepos = Array.from(new Set([...selectedRepos, ...displayedRepos]));
      updateData({ repos: uniqueRepos });
    }
  };
    
  if (!repositories) return <Loader />;
  if (isFetched && !repositories.length) {
    return <Empty />;
  }
  return (
    <MenuStyled
      items={[
        {
          key: 'select-all',
          label: (
            <MenuItemStyled gap="middle" align="flex-start">
              <Checkbox checked={allSelected} onChange={handleSelectAll} />
              <Typography.Text strong>{t('selectAll')}</Typography.Text>
            </MenuItemStyled>
          ),
        },
        ...repositories.map((item) => ({
          key: item.name,
          label: (
            <MenuItemStyled gap="middle" align="flex-start">
              <Checkbox checked={!!data.repos.find((r) => r === item.url)} />
              <Typography.Text>{item.name}</Typography.Text>
            </MenuItemStyled>
          ),
          onClick: () => {
            if (data.repos.find((r) => r === item.url)) {
              const newRepos = data.repos.filter((repo) => repo !== item.url);
              updateData({ repos: newRepos });
            } else {
              const newRepos = [...data.repos, item.url];
              updateData({ repos: newRepos });
            }
          },
        })),
      ]}
    />
  );
};

export default UserOnboardingCodeHuggingFace;
