import styled from '@emotion/styled';
import { Flex, Menu } from 'antd';

export const MenuStyled = styled(Menu)`
  min-width: 420px;
  && .ant-menu-item {
    padding: 0;
  }

  && .ant-menu-item:hover {
    background-color: transparent !important;
    color: inherit !important;
  }

  && .ant-menu-item-selected {
    background-color: transparent !important;
    color: inherit !important;
  }
  &.ant-menu-light.ant-menu-root.ant-menu-vertical {
    border-inline-end: none;
  }
  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    min-width: 100%;
  }
`;

export const MenuItemStyled = styled(Flex)``;
