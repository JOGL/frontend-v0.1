import { createContext } from 'react';
import { StoreApi, UseBoundStore } from 'zustand';
import { OnboardingStore } from './userOnboardingStore';

export type OnboardingStoreType = UseBoundStore<StoreApi<OnboardingStore>>;

export const OnboardingContext = createContext<OnboardingStoreType | null>(null);
