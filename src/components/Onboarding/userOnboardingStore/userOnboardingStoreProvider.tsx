import React, { useContext, useState } from 'react';
import { OnboardingContext } from './userOnboardingStoreContext';
import { createOnboardingStore } from './userOnboardingStore';
import type { OnboardingStore } from './userOnboardingStore';

export const OnboardingStoreProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const [store] = useState(() => createOnboardingStore());

  return <OnboardingContext.Provider value={store}>{children}</OnboardingContext.Provider>;
};

export const useOnboardingStore = <T,>(selector: (state: OnboardingStore) => T): T => {
  const store = useContext(OnboardingContext);
  if (!store) {
    throw new Error('Onboarding store must be used within OnboardingStoreProvider');
  }
  return store(selector);
};
