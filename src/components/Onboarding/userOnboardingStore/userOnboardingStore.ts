import { create } from 'zustand';
import { persist } from 'zustand/middleware';
import { OnboardingData } from '../userOnboarding/userOnboarding.types';

export interface OnboardingStore {
  // Working copy of the data that can be modified
  data: OnboardingData;
  // The last committed version of the data
  committedData: OnboardingData;
  // Flag to track if there are uncommitted changes
  hasUncommittedChanges: boolean;
  // Update the working copy
  updateData: (updates: Partial<OnboardingData>) => void;
  // Commit the current working copy
  commit: () => void;
  // Rollback to the last committed version
  rollback: () => void;
  // Reset everything to initial state
  resetData: () => void;
}

export const createOnboardingStore = () => {
  const initialData: OnboardingData = {
    feats: [],
    first_name: '',
    last_name: '',
    author_id: '',
    author_ids: [],
    papers: [],
    repos: [],
    gh_token: '',
    hf_token: '',
    linkedin_url: '',
    educationRecords: [],
    experienceRecords: [],
    noWorksFound: undefined,
    isLoading: false,
  };

  return create<OnboardingStore>()(
    persist(
      (set) => ({
        data: initialData,
        committedData: initialData,
        hasUncommittedChanges: false,

        updateData: (updates) =>
          set((state) => ({
            data: { ...state.data, ...updates },
            hasUncommittedChanges: true,
          })),

        commit: () =>
          set((state) => ({
            committedData: state.data,
            hasUncommittedChanges: false,
          })),

        rollback: () =>
          set((state) => ({
            data: state.committedData,
            hasUncommittedChanges: false,
          })),

        resetData: () =>
          set({
            data: initialData,
            committedData: initialData,
            hasUncommittedChanges: false,
          }),
      }),
      {
        name: 'onboarding-storage',
      }
    )
  );
};
