import styled from '@emotion/styled';
import { Button } from 'antd';

export const ButtonBackStyled = styled(Button)`
  padding: 0;
`;