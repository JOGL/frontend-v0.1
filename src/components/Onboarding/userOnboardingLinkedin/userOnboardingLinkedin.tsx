import { Button, Col, Flex, Input, List, Row, Typography } from 'antd';
import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { FlexFullWidthStyled, SpaceFullWidthStyled } from '~/src/components/Common/common.styles';
import { useOnboardingStore } from '../userOnboardingStore/userOnboardingStoreProvider';
import { OnboardingStepProps } from '../userOnboarding/userOnboarding.types';
import { UserEducationModel, UserExperienceModel } from '~/__generated__/types';
import useMobile from '~/src/hooks/useMobile';

const UserOnboardingLinkedIn: FC<OnboardingStepProps> = (props) => {
  const { t } = useTranslation('common');
  const data = useOnboardingStore((state) => state.data);
  const updateData = useOnboardingStore((state) => state.updateData);
  const commitData = useOnboardingStore((state) => state.commit);
  const isMobile = useMobile();

  return (
    <SpaceFullWidthStyled direction="vertical" size="middle">
      <Row gutter={[16, 16]}>
        <Col sm={12} xs={24}>
          <Input
            placeholder="https://www.linkedin.com/in/yourprofile"
            value={data.linkedin_url}
            onChange={(e) => {
              updateData({ linkedin_url: e.target.value });
            }}
          />
        </Col>
        <Col sm={12} xs={24}>
          <Button
            type="primary"
            onClick={() => {
              if (!data.linkedin_url) return;
              commitData();
              props.goTo('linkedin_exp');
            }}
          >
            {t('linkedin.load')}
          </Button>
        </Col>
      </Row>
      {data.experienceRecords.length > 0 && (
        <Flex vertical>
          <Typography.Title level={3}>{t('experience.one')}</Typography.Title>
          <List
            dataSource={data.experienceRecords}
            renderItem={(item: UserExperienceModel) => {
              return (
                <List.Item>
                  <FlexFullWidthStyled justify="space-between" align="center">
                    {isMobile ? (
                      <Flex vertical>
                        <Typography.Text>{item.position}</Typography.Text>
                        <Typography.Text>{item.company}</Typography.Text>
                      </Flex>
                    ) : (
                      <Typography.Text>
                        {item.position} | {item.company}
                      </Typography.Text>
                    )}
                  </FlexFullWidthStyled>
                </List.Item>
              );
            }}
          />
        </Flex>
      )}
      {data.educationRecords.length > 0 && (
        <Flex vertical>
          <Typography.Title level={3}>{t('education.one')}</Typography.Title>
          <List
            dataSource={data.educationRecords}
            renderItem={(item: UserEducationModel) => {
              return (
                <List.Item>
                  <FlexFullWidthStyled justify="space-between" align="center">
                    {isMobile ? (
                      <Flex vertical>
                        <Typography.Text>{item.program}</Typography.Text>
                        <Typography.Text>{item.school}</Typography.Text>
                      </Flex>
                    ) : (
                      <Typography.Text>
                        {item.program} | {item.school}
                      </Typography.Text>
                    )}
                  </FlexFullWidthStyled>
                </List.Item>
              );
            }}
          />
        </Flex>
      )}
    </SpaceFullWidthStyled>
  );
};

export default UserOnboardingLinkedIn;