import React, { FC, FormEvent, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Button from '~/src/components/primitives/Button';
import Card from '~/src/components/Cards/Card';
import FormDefaultComponent from '../Tools/Forms/FormDefaultComponent';

interface Question {
  question: string;
}

const ManageOnboardingQuestion = ({ items, handleChange }) => {
  const { t } = useTranslation('common');
  const [showQuestionCreate, setShowQuestionCreate] = useState(false);
  const [questions, setQuestions] = useState(items);
  const addQuestion = () => {
    setShowQuestionCreate(!showQuestionCreate);
  };

  return (
    <div tw="flex flex-col space-y-3">
      <div tw="flex flex-col space-y-3">
        {questions &&
          [...questions]
            .sort(function (a, b) {
              return a.id - b.id; // sort by id (asc)
            })
            .map((value, i) => (
              <div tw="flex flex-col pt-3" key={i}>
                <CardFormEdit
                  value={value}
                  onChange={(originalQuestion: Question, updatedQuestion) => {
                    const originalQuestionId = questions.indexOf(originalQuestion);
                    setQuestions(
                      questions.map((question) =>
                        questions[originalQuestionId] === question ? updatedQuestion : question
                      )
                    );
                  }}
                  onSubmit={() => {
                    handleChange('questionnaire', questions);
                  }}
                  onDelete={(questionUrl: number) => {
                    const questionId = questions.indexOf(questionUrl);
                    handleChange(
                      'questionnaire',
                      [...questions].filter((question) => questions[questionId] !== question)
                    );
                    setQuestions([...questions].filter((question) => questions[questionId] !== question));
                  }}
                />
              </div>
            ))}
      </div>
      <Button onClick={addQuestion}>{t('action.addItem', { item: t('question.one') })}</Button>
      {showQuestionCreate && (
        <CardFormCreate
          onCreate={(newQuestion: Question) => {
            handleChange('questionnaire', [...questions, newQuestion]);
            setQuestions([...questions, newQuestion]);
            setShowQuestionCreate(false);
          }}
        />
      )}
    </div>
  );
};
interface ICardFormCreate {
  onCreate?: (newQuestion: Question) => void;
}
interface ICardFormEdit {
  value: Question;
  onDelete?: (question: number) => void;
  onChange?: (originalQuestion: Question, updatedQuestion: Question) => void;
  onSubmit?: (value: Question) => void;
}

const CardFormCreate: FC<ICardFormCreate> = ({ onCreate }) => {
  const { t } = useTranslation('common');
  const [newQuestion, setNewQuestion] = useState<Question>({
    question: '',
  });

  // handle every changes to the question
  const handleChange: (key: string, content: string) => void = (key, content) => {
    setNewQuestion((prevQuestion) => ({ ...prevQuestion, [key]: content }));
  };
  // handle when submitting (update, or create), an question
  const handleSubmit: (event: FormEvent<HTMLFormElement>) => void = (e) => {
    e.preventDefault();
    if (newQuestion.question !== '') {
      onCreate(newQuestion);
    } else {
      alert('Please fill all fields');
    }
  };

  return (
    <Card tw="py-0">
      <div tw="flex flex-col justify-between md:flex-row">
        <div tw="flex flex-col w-full pr-0 md:pr-5">
          <FormDefaultComponent
            content={newQuestion.question}
            id="question"
            mandatory
            onChange={handleChange}
            title=""
          />
        </div>
        <div tw="flex flex-col space-y-2 justify-center items-end pt-4 md:pt-0">
          <Button onClick={handleSubmit}>{t('action.save')}</Button>
        </div>
      </div>
    </Card>
  );
};

const CardFormEdit: FC<ICardFormEdit> = ({ value, onDelete, onChange, onSubmit }) => {
  const { t } = useTranslation('common');
  const [showSubmitBtn, setShowSubmitBtn] = useState(false);
  // handle every changes to the question
  const handleChange: (key: string, content: string) => void = (key, content) => {
    onChange(value, { ...value, [key]: content });
    setShowSubmitBtn(content !== value.question);
  };
  // handle when submitting (update, or create), an question
  const handleSubmit: (event: FormEvent<HTMLFormElement>) => void = (e) => {
    e.preventDefault();
    if (value.question !== '') {
      onSubmit(value); // call onChange function
      setShowSubmitBtn(false); // hide update button
    } else {
      alert('Please fill all fields');
    }
  };
  // handle when deleting on question
  const handleDelete = (question) => {
    onDelete(question); // call onDelete function
  };

  return (
    <Card overflow="show" tw="py-0">
      <div tw="flex flex-col justify-between md:flex-row">
        <div tw="flex flex-col w-full pr-0 md:pr-5">
          <FormDefaultComponent content={value.question} id="question" mandatory onChange={handleChange} title="" />
        </div>
        <div tw="flex flex-col space-y-2 justify-center items-end pt-4 md:pt-0">
          {showSubmitBtn && (
            <Button onClick={handleSubmit} width="100%">
              {t('action.update')}
            </Button>
          )}
          <Button btnType="danger" onClick={() => handleDelete(value)} width="100%">
            {t('action.delete')}
          </Button>
        </div>
      </div>
      {/* </form> */}
    </Card>
  );
};

export default ManageOnboardingQuestion;
