import styled from '@emotion/styled';
import { Flex, Menu, Typography } from 'antd';

export const MenuStyled = styled(Menu)`
  && .ant-menu-item {
    height: 70px;
    padding: 0;
    margin: 4px 0;
  }

  && .ant-menu-item:hover {
    background-color: transparent !important;
    color: inherit !important;
  }

  && .ant-menu-item-selected {
    background-color: transparent !important;
    color: inherit !important;
  }
`;

export const MenuItemStyled = styled(Flex)`
  .ant-menu-root.ant-menu-vertical {
    border-inline-end: 0;
  }
`;

export const OnboardingFeatTitleStyled = styled(Typography)`
  padding: 0;
  font-size: ${({ theme }) => theme.token.fontSizeLG}px;
`;

export const OnboardingFeatSubtitleStyled = styled(Typography)`
  padding: 0;
  font-size: ${({ theme }) => theme.token.fontSizeSM}px;
  font-weight: 200px;
`;
