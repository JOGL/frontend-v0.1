import { Checkbox, Flex } from 'antd';
import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Feats, OnboardingStepProps } from '../userOnboarding/userOnboarding.types';
import {
  MenuStyled,
  MenuItemStyled,
  OnboardingFeatTitleStyled,
  OnboardingFeatSubtitleStyled,
} from './userOnboardingFeats.styles';
import { useOnboardingStore } from '../userOnboardingStore/userOnboardingStoreProvider';

const UserOnboardingFeats: FC<OnboardingStepProps> = (props) => {
  const { t } = useTranslation('common');
  const data = useOnboardingStore((state) => state.data);
  const updateData = useOnboardingStore((state) => state.updateData);
  const commitData = useOnboardingStore((state) => state.commit);

  return (
    <MenuStyled
      items={Feats.map((item) => ({
        key: item.key,
        label: (
          <MenuItemStyled gap="middle" align="flex-start">
            <Checkbox checked={!!data.feats.find((f) => f === item.key)} />
            <Flex vertical>
              <OnboardingFeatTitleStyled>{t(item.translationId)}</OnboardingFeatTitleStyled>
              <OnboardingFeatSubtitleStyled>{t(item.translationIdSubtitle)}</OnboardingFeatSubtitleStyled>
            </Flex>
          </MenuItemStyled>
        ),
        onClick: () => {
          if (data.feats.find((f) => f === item.key)) {
            const newFeats = data.feats.filter((feat) => feat !== item.key);
            updateData({ feats: newFeats });
          } else {
            const newFeats = [...data.feats, item.key];
            updateData({ feats: newFeats });
          }
          commitData();
        },
      }))}
    />
  );
};

export default UserOnboardingFeats;
