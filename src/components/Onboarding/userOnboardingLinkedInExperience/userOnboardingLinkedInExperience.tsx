import { Checkbox, Empty, Flex, Typography } from 'antd';
import React, { FC, useMemo } from 'react';
import { useOnboardingStore } from '../userOnboardingStore/userOnboardingStoreProvider';
import { OnboardingStepProps } from '../userOnboarding/userOnboarding.types';
import { useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import Loader from '../../Common/loader/loader';
import { MenuStyled } from './userOnboardingLinkedInExperience.styles';
import { UserExperienceModel } from '~/__generated__/types';
import useTranslation from 'next-translate/useTranslation';
import useMobile from '~/src/hooks/useMobile';

const UserOnboardingLinkedInExperience: FC<OnboardingStepProps> = (props) => {
  const { t } = useTranslation('common');
  const data = useOnboardingStore((state) => state.data);
  const updateData = useOnboardingStore((state) => state.updateData);
  const isMobile = useMobile();

  const { data: profile, isFetched } = useQuery({
    queryKey: [QUERY_KEYS.userLinkedInData, data.linkedin_url],
    queryFn: async () => {
      const response = await api.users.linkedinProfileList({ linkedInUrl: data.linkedin_url });
      return response.data;
    },
    enabled: !!data.linkedin_url,
  });

  const areEqual = (model1: UserExperienceModel, model2: UserExperienceModel): boolean => {
    if (!model1 || !model2) return false;

    return model1.company === model2.company && model1.position === model2.position;
  };

  const allSelected = useMemo(() => {
    if (!profile?.experience) return false;
    return profile.experience.every((item) => data.experienceRecords.some((e) => areEqual(e, item)));
  }, [profile?.experience, data.experienceRecords]);

  if (!profile) return <Loader />;

  const handleSelectAll = () => {
    if (allSelected) {
      updateData({ experienceRecords: [] });
    } else {
      updateData({ experienceRecords: profile.experience });
    }
  };
  if (isFetched && !profile.experience.length) {
    return <Empty />;
  }
  return (
    <>
      {profile && (
        <MenuStyled
          isMobile={isMobile}
          items={[
            ...(profile.experience.length > 0
              ? [
                  {
                    key: 'select-all',
                    label: (
                      <Flex gap="middle" align="flex-start">
                        <Checkbox checked={allSelected} onChange={handleSelectAll} />
                        <Typography.Text strong>{t('selectAll')}</Typography.Text>
                      </Flex>
                    ),
                  },
                ]
              : []),
            ...profile.experience.map((item, index) => ({
              key: index,
              label: (
                <Flex gap="middle" align="flex-start">
                  <Checkbox checked={!!data.experienceRecords.find((e) => areEqual(e, item))} />
                  {isMobile ? (
                    <Flex vertical>
                      <Typography.Text ellipsis={{ tooltip: item.position }}>{item.position}</Typography.Text>
                      <Typography.Text ellipsis={{ tooltip: item.company }}>{item.company}</Typography.Text>
                      <Typography.Text type="secondary">
                        {item.date_from} - {item.current ? t('present') : item.date_to}
                      </Typography.Text>
                    </Flex>
                  ) : (
                    <>
                      <Typography.Text>
                        {item.position} | {item.company}
                      </Typography.Text>
                      <Typography.Text type="secondary">
                        {item.date_from} - {item.current ? t('present') : item.date_to}
                      </Typography.Text>
                    </>
                  )}
                </Flex>
              ),
              onClick: () => {
                if (data.experienceRecords.find((e) => areEqual(e, item))) {
                  const newExperience = data.experienceRecords.filter((e) => !areEqual(e, item));
                  updateData({ experienceRecords: newExperience });
                } else {
                  const newExperience = [...data.experienceRecords, item];
                  updateData({ experienceRecords: newExperience });
                }
              },
            })),
          ]}
        />
      )}
    </>
  );
};

export default UserOnboardingLinkedInExperience;
