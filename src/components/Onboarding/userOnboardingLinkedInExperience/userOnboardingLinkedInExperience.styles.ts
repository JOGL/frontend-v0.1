import styled from '@emotion/styled';
import { Menu } from 'antd';

export const MenuStyled = styled(Menu)<{ isMobile?: boolean }>`
  && .ant-menu-item {
    padding: 0;
    ${({ isMobile }) =>
      isMobile &&
      `
      min-height: 80px;
    `}
    .ant-typography {
      display: inline-block;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      -o-text-overflow: ellipsis;
    }
  }

  && .ant-menu-item:first-child {
    min-height: 40px;
  }

  && .ant-menu-item:hover {
    background-color: transparent !important;
    color: inherit !important;
  }

  && .ant-menu-item-selected {
    background-color: transparent !important;
    color: inherit !important;
  }
`;
