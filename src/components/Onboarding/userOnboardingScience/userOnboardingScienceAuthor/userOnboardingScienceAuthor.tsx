import { Button, Card, Col, Empty, Flex, Row, Typography } from 'antd';
import React, { FC, useMemo } from 'react';
import { OnboardingStepProps } from '../../userOnboarding/userOnboarding.types';
import { useOnboardingStore } from '../../userOnboardingStore/userOnboardingStoreProvider';
import useTranslation from 'next-translate/useTranslation';
import { FlexFullWidthStyled, SpaceFullWidthStyled } from '~/src/components/Common/common.styles';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import Loader from '~/src/components/Common/loader/loader';

const UserOnboardingScienceAuthorConfirmation: FC<OnboardingStepProps> = (props) => {
  const { t } = useTranslation('common');
  const data = useOnboardingStore((state) => state.data);
  const updateData = useOnboardingStore((state) => state.updateData);

  const { data: authors, isFetched } = useQuery({
    queryKey: [QUERY_KEYS.openAlexAuthors, data.first_name, data.last_name],
    queryFn: async () => {
      const response = await api.papers.openalexAuthorsList({ Search: `${data.first_name} ${data.last_name}` });
      return response.data;
    },
  });

  const selectedAuthors = useMemo(() => {
    return data.author_ids || [];
  }, [data.author_ids]);

  const handleAuthorSelect = (authorId: string) => {
    const newAuthorIds = selectedAuthors.includes(authorId)
      ? selectedAuthors.filter((id) => id !== authorId)
      : [...selectedAuthors, authorId];
    updateData({ author_ids: newAuthorIds });
  };

  if (!authors) return <Loader />;
  if (isFetched && !authors.length) {
    return <Empty />;
  }
  return (
    <>
      {authors &&
        authors.map((author) => (
          <Card
            key={author.id}
            title={
              <FlexFullWidthStyled gap="large">
                <Typography.Text>
                  {data.first_name} {data.last_name}
                </Typography.Text>
                <Typography.Text type="secondary">{author.orcid_id}</Typography.Text>
              </FlexFullWidthStyled>
            }
            extra={
              <Button
                type={selectedAuthors.includes(author.id) ? 'default' : 'primary'}
                onClick={() => handleAuthorSelect(author.id)}
              >
                {selectedAuthors.includes(author.id)
                  ? t('userOnboarding.science.selected')
                  : t('userOnboarding.science.thisIsMe')}
              </Button>
            }
          >
            <SpaceFullWidthStyled direction="vertical">
              <Row>
                <Col span={12}>
                  <Typography.Title level={5}>{t('institutions')}</Typography.Title>
                </Col>
                <Col span={12}>
                  <Typography.Title level={5}>{t('topics')}</Typography.Title>
                </Col>
              </Row>

              <Row>
                <Col span={12}>
                  <Flex vertical>
                    {author.institutions.map((institution, index) => (
                      <Typography.Text key={institution + index}>{institution}</Typography.Text>
                    ))}
                  </Flex>
                </Col>
                <Col span={12}>
                  <Flex vertical>
                    {author.topics.slice(0, 5).map((topic, index) => (
                      <Typography.Text key={topic + index}>{topic}</Typography.Text>
                    ))}
                  </Flex>
                </Col>
              </Row>

              {/* <Divider />
              <Row>
                <Typography.Title level={5}>Latest Scholarly work</Typography.Title>
              </Row>

              <Row>
                <Col span={12}>
                  <Typography.Text type="secondary">Paper title</Typography.Text>
                </Col>
                <Col span={12}>
                  <Typography.Text type="secondary">Journal name</Typography.Text>
                </Col>
              </Row>
              <Row>
                <Col span={12}>
                  <Typography.Text type="secondary">Author</Typography.Text>
                </Col>
                <Col span={12}>
                  <Typography.Text type="secondary">Publication</Typography.Text>
                </Col>
              </Row>

              <Row>
                <Typography.Title level={5}>Latest Patent</Typography.Title>
              </Row>

              <Row>
                <Col span={8}>
                  <Typography.Text type="secondary">Patent Title</Typography.Text>
                </Col>
                <Col span={8}>
                  <Typography.Text type="secondary">Application Date</Typography.Text>
                </Col>
                <Col span={8}>
                  <Typography.Text type="secondary">Grant date</Typography.Text>
                </Col>
              </Row> */}
            </SpaceFullWidthStyled>
          </Card>
        ))}
    </>
  );
};

export default UserOnboardingScienceAuthorConfirmation;
