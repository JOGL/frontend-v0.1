import { Button, Card, Col, Empty, Flex, Row, Typography } from 'antd';
import React, { FC, useMemo, useEffect } from 'react';
import { OnboardingStepProps } from '../../userOnboarding/userOnboarding.types';
import { useOnboardingStore } from '../../userOnboardingStore/userOnboardingStoreProvider';
import useTranslation from 'next-translate/useTranslation';
import { FlexFullWidthStyled, SpaceFullWidthStyled } from '~/src/components/Common/common.styles';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import Loader from '~/src/components/Common/loader/loader';

const UserOnboardingScienceAuthorConfirmation: FC<OnboardingStepProps> = (props) => {
  const { t } = useTranslation('common');
  const data = useOnboardingStore((state) => state.data);
  const updateData = useOnboardingStore((state) => state.updateData);

  const { data: authors, isFetched, isLoading } = useQuery({
    queryKey: [QUERY_KEYS.openAlexAuthors, data.first_name, data.last_name],
    queryFn: async () => {
      const response = await api.papers.openalexAuthorsList({ Search: `${data.first_name} ${data.last_name}` });
      return response.data;
    },
  });

  const hasAuthors = authors && authors.length > 0;

  useEffect(() => {
    if (!isLoading) {
      updateData({ noWorksFound: !hasAuthors, isLoading: false });
    } else updateData({ isLoading });
  }, [isLoading, hasAuthors, updateData]);

  const selectedAuthors = useMemo(() => {
    return data.author_ids || [];
  }, [data.author_ids]);

  const handleAuthorSelect = (authorId: string) => {
    const newAuthorIds = selectedAuthors.includes(authorId)
      ? selectedAuthors.filter((id) => id !== authorId)
      : [...selectedAuthors, authorId];
    updateData({ author_ids: newAuthorIds });
  };

  if (!authors) return <Loader />;
  if (isFetched && !hasAuthors) {
    return <Empty />;
  }
  return (
    <>
      {authors &&
        authors.map((author) => (
          <Card
            key={author.id}
            title={
              <FlexFullWidthStyled gap="large">
                <Typography.Text>
                  {data.first_name} {data.last_name}
                </Typography.Text>
                <Typography.Text type="secondary">{author.orcid_id}</Typography.Text>
              </FlexFullWidthStyled>
            }
            extra={
              <Button
                type={selectedAuthors.includes(author.id) ? 'default' : 'primary'}
                onClick={() => handleAuthorSelect(author.id)}
              >
                {selectedAuthors.includes(author.id)
                  ? t('userOnboarding.science.selected')
                  : t('userOnboarding.science.thisIsMe')}
              </Button>
            }
          >
            <SpaceFullWidthStyled direction="vertical">
              <FlexFullWidthStyled vertical>
                <Typography.Title level={5}>{t('paper.mostRecent')}</Typography.Title>
                <Typography.Text>{author.newest_work?.title}</Typography.Text>
                <Typography.Text type="secondary">{author.newest_work?.authors.join(', ')}</Typography.Text>
                <Typography.Text type="secondary">{author.newest_work?.date}</Typography.Text>
              </FlexFullWidthStyled>
              <SpaceFullWidthStyled direction="vertical">
                <Typography.Title level={5}>{t('interest.other')}</Typography.Title>
                <Typography.Text>{author.topics.join(', ')}</Typography.Text>
              </SpaceFullWidthStyled>
            </SpaceFullWidthStyled>
          </Card>
        ))}
    </>
  );
};

export default UserOnboardingScienceAuthorConfirmation;
