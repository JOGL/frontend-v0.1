import { Flex } from 'antd';
import React, { FC, useState } from 'react';
import { OnboardingStepProps } from '../../userOnboarding/userOnboarding.types';
import { InputStyled } from './userOnboardingScienceName.styles';
import { useOnboardingStore } from '../../userOnboardingStore/userOnboardingStoreProvider';

const UserOnboardingScienceName: FC<OnboardingStepProps> = (props) => {
  const data = useOnboardingStore((state) => state.data);
  const updateData = useOnboardingStore((state) => state.updateData);

  return (
    <Flex gap="large">
      <InputStyled
        value={data.first_name}
        onChange={(e) => {
          updateData({ first_name: e.target.value});
        }}
      />
      <InputStyled
        value={data.last_name}
        onChange={(e) => {
          updateData({ last_name: e.target.value });
        }}
      />
    </Flex>
  );
};

export default UserOnboardingScienceName;
