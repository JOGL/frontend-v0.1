import styled from '@emotion/styled';
import { Card, Col, Flex, Menu, Typography } from 'antd';

export const MenuStyled = styled(Menu)`
  width: 100%;
  && .ant-menu-item {
    height: auto;
    padding: 0;
    margin: 4px 0;
    width: 100%;
  }

  && .ant-menu-item:hover {
    background-color: transparent !important;
    color: inherit !important;
  }

  && .ant-menu-item-selected {
    background-color: transparent !important;
    color: inherit !important;
  }

  &&.ant-menu-light,
  &&.ant-menu-light.ant-menu-root,
  &&.ant-menu-light > .ant-menu {
    border-inline-end: none;
  }
  .ant-menu-title-content {
    display: flex !important;
  }
`;

export const MenuItemStyled = styled(Flex)`
  width: 100%;
  height: auto;
  min-height: 70px;

  .ant-menu .ant-menu-title-content {
    display: flex;
  }
`;

export const SelectAllStyled = styled(MenuItemStyled)`
  min-height: 50px;
`;

export const InfoContainerStyled = styled(Flex)`
  padding-left: ${({ theme }) => theme.token.paddingSM}px;
  width: calc(100% - 16px);
`;

export const CardStyled = styled(Card)`
  width: 100%;
  height: auto;
  .ant-card-body {
    padding: ${({ theme }) => theme.token.padding}px;
  }
`;

export const TitleContainerStyled = styled(Flex)`
  width: 100%;
  min-width: 0; // This is crucial for text-overflow to work
`;

export const TitleStyled = styled(Typography.Title)`
  && {
    margin: 0;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    .ant-typography {
      width: 100%;
      display: block;
    }
  }
`;

export const DateColStyled = styled(Col)`
  text-align: right;
`;
