import { Checkbox, Col, Empty, Row, Typography } from 'antd';
import React, { FC, useMemo, useEffect } from 'react';
import { OnboardingStepProps } from '../../userOnboarding/userOnboarding.types';
import { useOnboardingStore } from '../../userOnboardingStore/userOnboardingStoreProvider';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import Loader from '~/src/components/Common/loader/loader';
import useTranslation from 'next-translate/useTranslation';
import {
  MenuItemStyled,
  MenuStyled,
  CardStyled,
  TitleStyled,
  TitleContainerStyled,
  InfoContainerStyled,
  DateColStyled,
  SelectAllStyled,
} from './userOnboardingSciencePapers.styles';

const UserOnboardingSciencePapers: FC<OnboardingStepProps> = () => {
  const { t } = useTranslation('common');
  const data = useOnboardingStore((state) => state.data);
  const updateData = useOnboardingStore((state) => state.updateData);

  const { data: worksForAuthorId, isLoading: isLoadingById } = useQuery({
    queryKey: [QUERY_KEYS.openAlexWorks, data.author_ids],
    queryFn: async () => {
      if (!data.author_ids?.length) return [];
      const response = await api.papers.openalexWorksByAuthorIdsList({
        authorIds: data.author_ids.join(','),
      });
      return response.data;
    },
    enabled: !!data.author_ids?.length,
  });

  const { data: worksForAuthorName, isLoading: isLoadingByName } = useQuery({
    queryKey: [QUERY_KEYS.openAlexWorks, data.first_name, data.last_name],
    queryFn: async () => {
      const response = await api.papers.openalexWorksByAuthorNameList({
        Search: `${data.first_name} ${data.last_name}`,
      });
      return response.data;
    },
    enabled: !data.author_ids?.length,
  });

  const works = useMemo(() => {
    return worksForAuthorId ?? worksForAuthorName;
  }, [worksForAuthorId, worksForAuthorName]);

  const isLoading = isLoadingById || isLoadingByName;
  const hasWorks = works && works.length > 0;

  useEffect(() => {
    if (!isLoading) {
      updateData({ noWorksFound: !hasWorks, isLoading: false });
    } else updateData({ isLoading });
  }, [isLoading, hasWorks, updateData]);

  const allSelected = useMemo(() => {
    if (!works) return false;
    return works.every((item) => data.papers.includes(item.id));
  }, [works, data.papers]);

  if (isLoading) return <Loader />;
  if (!hasWorks) return <Empty />;

  const handleSelectAll = () => {
    if (allSelected) {
      updateData({ papers: [] });
    } else {
      updateData({ papers: works.map((item) => item.id) });
    }
  };

  return (
    <MenuStyled
      items={[
        ...(works.length > 0
          ? [
              {
                key: 'select-all',
                label: (
                  <SelectAllStyled gap="middle" align="flex-start">
                    <Checkbox checked={allSelected} onChange={handleSelectAll} />
                    <Typography.Text strong>{t('selectAll')}</Typography.Text>
                  </SelectAllStyled>
                ),
              },
            ]
          : []),
        ...works.map((item) => ({
          key: item.id,
          label: (
            <CardStyled>
              <MenuItemStyled align="flex-start">
                <Checkbox checked={!!data.papers.includes(item.id)} />
                <InfoContainerStyled vertical>
                  <TitleContainerStyled>
                    <TitleStyled level={5} ellipsis={{ rows: 1, tooltip: true }}>
                      {item.title}
                    </TitleStyled>
                  </TitleContainerStyled>
                  <Typography.Text type="secondary" ellipsis={{ tooltip: true }}>
                    {item.authors.join(', ')}
                  </Typography.Text>
                  <Typography.Text type="secondary" ellipsis={{ tooltip: true }}>
                    {item.publication}
                  </Typography.Text>
                  <Typography.Text type="secondary"> {item.date}</Typography.Text>
                </InfoContainerStyled>
              </MenuItemStyled>
            </CardStyled>
          ),
          onClick: () => {
            if (data.papers.find((id) => id === item.id)) {
              const newPapers = data.papers.filter((id) => id !== item.id);
              updateData({ papers: newPapers });
            } else {
              const newPapers = [...data.papers, item.id];
              updateData({ papers: newPapers });
            }
          },
        })),
      ]}
    />
  );
};

export default UserOnboardingSciencePapers;
