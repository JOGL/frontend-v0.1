import { LinkOutlined } from '@ant-design/icons';
import { Form, Input, Modal } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { isValidHttpUrl } from '~/src/utils/utils';
import { RuleObject } from 'rc-field-form/lib/interface';
import { DocumentOrFolderModel } from '~/__generated__/types';

const CREATE_LINK_FORM = 'create-link-form';

export interface LinkFormFields {
  title: string;
  url: string;
  description: string;
}

interface Props {
  link: DocumentOrFolderModel | null;
  onClose: () => void;
  onConfirm: (data: LinkFormFields) => void;
}

const LinkModal = ({ link, onClose, onConfirm }: Props) => {
  const { t } = useTranslation('common');
  const [form] = Form.useForm<LinkFormFields>();

  const requiredRule = {
    required: true,
    message: t('error.requiredField'),
  };

  return (
    <Modal
      open
      title={link ? t('action.editLink') : t('action.createLink')}
      okButtonProps={{ htmlType: 'submit', form: CREATE_LINK_FORM }}
      okText={link ? t('action.save') : t('action.create')}
      onCancel={onClose}
    >
      <Form
        form={form}
        layout="vertical"
        id={CREATE_LINK_FORM}
        onFinish={(data) => onConfirm(data)}
        initialValues={{ title: link?.title ?? '', url: link?.url ?? '', description: link?.description ?? '' }}
      >
        <Form.Item
          name="url"
          label={t('url')}
          required
          rules={[
            requiredRule,
            {
              validator: (_: RuleObject, url: string): Promise<void | string> => {
                if (!url || isValidHttpUrl(url)) {
                  return Promise.resolve();
                }
                return Promise.reject(t('error.invalidLink'));
              },
            },
          ]}
        >
          <Input addonBefore={<LinkOutlined />} placeholder={t('urlPlaceholder')} />
        </Form.Item>
        <Form.Item name="title" label={t('title')} required rules={[requiredRule]}>
          <Input />
        </Form.Item>
        <Form.Item name="description" label={t('shortDescription')}>
          <Input.TextArea placeholder={t('explainToMembersWhyThisDocumentMatters')} rows={4} />
        </Form.Item>
      </Form>
    </Modal>
  );
};
export default LinkModal;
