import React, { useMemo, useState } from 'react';
import { Select } from 'antd';
import { EMAIL_REGEX } from '~/src/utils/utils';
import useTranslation from 'next-translate/useTranslation';

interface EmailTagsSelectProps {
  value?: string[];
  onChange?: (value: string[]) => void;
  placeholder?: string;
  invalidEmail?: (value: boolean) => void;
}

const EmailTagsSelect: React.FC<EmailTagsSelectProps> = ({ value, onChange, placeholder, invalidEmail }) => {
  const [hasInvalidEmail, setHasInvalidEmail] = useState(false);
  const { t } = useTranslation('common');

  const validateEmail = (email: string): boolean => {
    const emailRegex = EMAIL_REGEX;
    return emailRegex.test(email);
  };

  const handleChange = (newValue: string[]) => {
    const hasInvalid = newValue.some((email) => !validateEmail(email));
    setHasInvalidEmail(hasInvalid);
    invalidEmail && invalidEmail(hasInvalidEmail);
    onChange?.(newValue);
  };

  useMemo(() => {
    const hasInvalid = value?.some((email) => !validateEmail(email)) ?? false;
    setHasInvalidEmail(hasInvalid);
    invalidEmail && invalidEmail(hasInvalid);
  }, [value]);

  const selectStatus = hasInvalidEmail ? 'error' : undefined;

  return (
    <Select
      mode="tags"
      value={value}
      onChange={handleChange}
      placeholder={placeholder || t('email')}
      tokenSeparators={[',', ' ', ';']}
      style={{ width: '100%' }}
      status={selectStatus}
      dropdownStyle={{ display: 'none' }}
    />
  );
};

export default EmailTagsSelect;
