import { FolderOutlined, RightOutlined } from '@ant-design/icons';
import { Button, Flex, List, Modal, Space, Tag, Typography } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useState } from 'react';
import { DocumentOrFolderModel } from '~/__generated__/types';
import { BreadcrumbStyled } from './moveToFolderModal.styles';
const ROOT = 'ROOT';

interface DocumentsByFolder {
  [key: string]: DocumentOrFolderModel[];
}

interface Props {
  currentFolder?: DocumentOrFolderModel;
  currentDocument: DocumentOrFolderModel;
  documentsByFolder?: DocumentsByFolder;
  onClose: () => void;
  onMove: (folderId: string | null) => void;
}

const MoveToFolderModal = ({ currentFolder, currentDocument, documentsByFolder, onClose, onMove }: Props) => {
  const { t } = useTranslation('common');
  const [selectedFolder, setSelectedFolder] = useState(ROOT);
  const [path, setPath] = useState<DocumentOrFolderModel[]>([]);
  const availableFolders =
    (documentsByFolder && documentsByFolder[selectedFolder]?.filter(({ is_folder }) => is_folder)) ?? [];

  return (
    <Modal
      open
      title={t('action.moveTo')}
      okText={t('action.move')}
      onCancel={onClose}
      onOk={() => onMove(selectedFolder === ROOT ? null : selectedFolder)}
      okButtonProps={{ disabled: (currentFolder?.id ?? ROOT) === selectedFolder }}
    >
      <Flex vertical gap="middle">
        <Flex gap="small">
          <Typography.Text strong>{t('currentLocation')}:</Typography.Text>
          <Tag>
            <Space>
              <FolderOutlined />
              {currentFolder?.title ?? t('document.other')}
            </Space>
          </Tag>
        </Flex>
        <BreadcrumbStyled
          items={[
            {
              title: t('document.other'),
              onClick: () => {
                setSelectedFolder(ROOT);
                setPath([]);
              },
            },
            ...path.map((folder) => {
              return {
                title: folder?.title,
                onClick: () => {
                  if (folder?.id) {
                    const index = path.map(({ id }) => id).indexOf(folder.id) + 1;
                    setSelectedFolder(folder?.id);
                    setPath((prev) => [...prev.slice(0, index)]);
                  }
                },
              };
            }),
          ]}
        />
        {availableFolders.length ? (
          <List
            size="small"
            bordered
            dataSource={availableFolders}
            renderItem={(folder) => (
              <List.Item>
                <Flex align="center" justify="space-between" gap="small" style={{ width: '100%' }}>
                  <Space>
                    <FolderOutlined />
                    {folder.title}
                  </Space>
                  <Button
                    type="link"
                    disabled={currentDocument.id === folder.id}
                    onClick={() => {
                      setSelectedFolder(folder.id);
                      setPath((prev) => [...prev, folder]);
                    }}
                  >
                    <RightOutlined />
                  </Button>
                </Flex>
              </List.Item>
            )}
          />
        ) : (
          <Typography.Text type="secondary">{t('noAvailableFolders')}</Typography.Text>
        )}
      </Flex>
    </Modal>
  );
};
export default MoveToFolderModal;
