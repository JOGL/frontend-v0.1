import React, { useState, useEffect } from 'react';
import { Grid } from 'antd';
const { useBreakpoint } = Grid;

interface Props {
  url: string;
}

interface PreviewData {
  author?: string;
  date?: string;
  description?: string;
  image?: string;
  logo?: string;
  publisher?: string;
  title?: string;
  url?: string;
}

const ExternalLinkPreviewer: React.FC<Props> = ({ url }) => {
  const screens = useBreakpoint();
  const isMobile = screens.xs;
  const [previewData, setPreviewData] = useState<PreviewData | null>(null);
  const [loading, setLoading] = useState(false);
  const isYouTubeURL = url.includes('youtube.com/watch') || url.includes('youtu.be');

  useEffect(() => {
    if (isYouTubeURL) {
      return;
    }

    const fetchDataAsync = async () => {
      try {
        setLoading(true);
        const res = await fetch(`${process.env.ADDRESS_FRONT}/api/metascraper?targetUrl=${url}`);
        const data = await res.json();
        if (res.status !== 200) {
          throw new Error(data.error);
        }
        setPreviewData(data);
      } catch (error) {
        console.error(error);
        setPreviewData(null);
      } finally {
        setLoading(false);
      }
    };

    fetchDataAsync();
  }, [isYouTubeURL, url]);

  if (loading) {
    return null;
  }

  if (isYouTubeURL) {
    return <YouTubeEmbed url={url} isMobile={isMobile} />;
  }

  if (!previewData) {
    return null;
  }

  return (
    <a href={url} target="_blank" rel="noopener noreferrer" tw="no-underline!">
      <div tw="flex flex-col shadow-custom p-2 rounded-lg hover:bg-gray-50">
        <div tw="flex gap-2 items-center">
          {previewData.logo && <img src={previewData.logo} tw="h-[20px]" alt="Publisher logo" />}
          <p tw="text-gray-800 text-[16px] font-semibold mb-0">{previewData.publisher}</p>
        </div>
        <div>
          <p tw="text-gray-800 text-[14px] mb-0">{previewData.title}</p>
          <p tw="text-gray-600 text-[14px] mb-0">{previewData.description}</p>
          {previewData.image && previewData.image !== previewData.logo && (
            <img tw="h-[80px] w-fit self-center flex" src={previewData.image} alt="Preview image" />
          )}
        </div>
      </div>
    </a>
  );
};

const YouTubeEmbed: React.FC<{ url: string; isMobile?: boolean }> = ({ url, isMobile }) => {
  const embedUrl = getYouTubeEmbedUrl(url);
  if (!embedUrl) return null;

  return (
    <div style={{ position: 'relative', width: '100%', paddingTop: '56.25%' }}>
      <iframe
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
          height: '100%',
        }}
        src={embedUrl}
        title="YouTube video player"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        referrerPolicy="strict-origin-when-cross-origin"
        allowFullScreen
      />
    </div>
  );
};

const getYouTubeEmbedUrl = (url: string): string | null => {
  const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
  const match = url.match(regExp);
  if (match && match[2].length === 11) {
    return `https://www.youtube.com/embed/${match[2]}`;
  }
  return null;
};

export default ExternalLinkPreviewer;
