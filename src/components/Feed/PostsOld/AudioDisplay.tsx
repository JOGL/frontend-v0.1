import { DisplayableAttachment, downloadUrlFromDisplayableAttachment, isRemoteAttachment } from '../types';
import { AttachmentDisplaySmallProps } from './AttachmentDisplay';
import { useState } from 'react';
import axios from 'axios';
import Icon from '~/src/components/primitives/Icon';
import { useAttachmentsCarouselStore } from '~/src/store/attachments-carousel/AttachmentsCarouselStoreProvider';
import SpinLoader from '~/src/components/Tools/SpinLoader';
import useTranslation from 'next-translate/useTranslation';
import { useAuth } from '~/auth/auth';

const AudioDisplaySmall: React.FC<AttachmentDisplaySmallProps> = ({ attachment, onOpenAttachment }) => {
  const { t } = useTranslation('common');

  return (
    <audio controls preload="metadata">
      <source src={downloadUrlFromDisplayableAttachment(attachment)} type={attachment.file_type} />
      {t('browserDoesNotSupportContent', { type: t('audio').toLowerCase() })}.
    </audio>
  );
};

interface AudioDisplayProps {
  attachment: DisplayableAttachment;
}

const AudioDisplay: React.FC<AudioDisplayProps> = ({ attachment }) => {
  const { t } = useTranslation('common');
  const { accessToken } = useAuth();
  const { cachedFile, addFile } = useAttachmentsCarouselStore((s) => ({
    cachedFile: s.cachedFiles[attachment.id],
    addFile: s.addFile,
  }));
  const [isLoading, setIsLoading] = useState(false);
  const [hasError, setHasError] = useState(false);
  const isFirstLoad = !cachedFile && !isLoading;

  const fetchAudio = async () => {
    try {
      let base64: string;

      setIsLoading(true);
      if (isRemoteAttachment(attachment)) {
        const { data } = await axios.get(attachment.document_url, {
          responseType: 'arraybuffer',
          headers: { 'Content-type': 'application/json', Authorization: accessToken },
        });

        const encoding = btoa(new Uint8Array(data).reduce((acc, current) => acc + String.fromCharCode(current), ''));
        base64 = `data:${attachment.file_type};base64,${encoding}`;
      } else {
        base64 = attachment.base64.toString();
      }

      addFile(attachment.id, base64);
    } catch (error) {
      console.error(error);
      setHasError(true);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div>
      {isLoading && <SpinLoader customCSS={{ marginTop: '16px', marginBottom: '16px' }} />}
      {hasError && (
        <div tw="flex flex-col items-center text-sm text-red-500">
          <Icon icon="material-symbols:error-outline-rounded" tw="w-5 h-5" />
          {t('error.somethingWentWrong')}
        </div>
      )}
      {isFirstLoad && (
        <audio controls preload="metadata" autoPlay={false}>
          {/* If the media download service worker isn't working properly this will fail with 401
            and we have to download the full audio manually */}
          <source
            src={downloadUrlFromDisplayableAttachment(attachment)}
            type={attachment.file_type}
            onError={() => fetchAudio()}
          />
          {t('browserDoesNotSupportContent', { type: t('audio').toLowerCase() })}.
        </audio>
      )}
      {cachedFile && (
        <audio preload="metadata" controls autoPlay={false}>
          <source src={cachedFile} type={attachment.file_type} />
          {t('browserDoesNotSupportContent', { type: t('audio').toLowerCase() })}.
        </audio>
      )}
    </div>
  );
};

export { AudioDisplaySmall, AudioDisplay };
