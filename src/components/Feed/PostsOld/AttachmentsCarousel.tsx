import React, { useCallback, useEffect, useState } from 'react';
import Carousel from '~/src/components/Common/Carousel/Carousel';
import { AttachmentDisplaySmall } from './AttachmentDisplay';
import { DisplayableAttachment } from '../types';
import { AttachmentsCarouselStoreProvider } from '~/src/store/attachments-carousel/AttachmentsCarouselStoreProvider';
import MediaPreview from '../../media/mediaPreview/mediaPreview';
import FilePreview from '../../file/filePreview/filePreview';

interface Props {
  isEditing: boolean;
  isUploading: boolean;
  attachments: DisplayableAttachment[];
  onDeleteAttachment: (attachmentId: string) => Promise<void>;
}

export interface ActiveAttachment {
  index: number;
  attachment: DisplayableAttachment;
}

export const AttachmentsCarousel: React.FC<Props> = ({ isEditing, isUploading, attachments, onDeleteAttachment }) => {
  const [activeAttachment, setActiveAttachment] = useState<ActiveAttachment>();
  const hasPrevious = activeAttachment && activeAttachment.index > 0;
  const hasNext = activeAttachment && activeAttachment.index < attachments.length - 1;
  const containsOnlyImagesOrVideos = !attachments.find(
    (a) => !a.file_type.includes('image') && !a.file_type.includes('video')
  );

  const onPrevious = useCallback(
    () =>
      setActiveAttachment((s) => {
        const index = Math.max(0, s.index - 1);
        return { attachment: attachments[index], index };
      }),
    [attachments]
  );

  const onNext = useCallback(
    () =>
      setActiveAttachment((s) => {
        const index = Math.min(s.index + 1, attachments.length - 1);
        return { attachment: attachments[index], index };
      }),
    [attachments]
  );

  useEffect(() => {
    const onKeyDown = (event: KeyboardEvent) => {
      if (event.defaultPrevented) {
        return; // Should do nothing if the default action has been cancelled
      }

      const left = 'ArrowLeft';
      const right = 'ArrowRight';
      if (event.code === left && hasPrevious) {
        onPrevious();
      } else if (event.code === right && hasNext) {
        onNext();
      }
    };

    document.addEventListener('keydown', onKeyDown);

    return () => {
      document.removeEventListener('keydown', onKeyDown);
    };
  }, [hasNext, hasPrevious, onNext, onPrevious]);

  if (attachments.length === 0) {
    return null;
  }
  return (
    <AttachmentsCarouselStoreProvider>
      {!!activeAttachment && activeAttachment.attachment.is_media && (
        <MediaPreview
          media={activeAttachment.attachment}
          onPrevious={hasPrevious ? onPrevious : undefined}
          onNext={hasNext ? onNext : undefined}
          onClose={() => setActiveAttachment(undefined)}
        />
      )}
      {!!activeAttachment && !activeAttachment.attachment.is_media && (
        <FilePreview
          file={activeAttachment.attachment}
          onPrevious={hasPrevious ? onPrevious : undefined}
          onNext={hasNext ? onNext : undefined}
          onClose={() => setActiveAttachment(undefined)}
        />
      )}

      <Carousel showDots={containsOnlyImagesOrVideos}>
        {attachments.map((attachment, index) => (
          <AttachmentDisplaySmall
            size={!isEditing && containsOnlyImagesOrVideos ? 'lg' : 'default'}
            key={attachment.id}
            isEditing={isEditing}
            isUploading={isUploading}
            attachment={attachment}
            onDeleteAttachment={onDeleteAttachment}
            onOpenAttachment={() => setActiveAttachment({ index, attachment })}
          />
        ))}
      </Carousel>
    </AttachmentsCarouselStoreProvider>
  );
};
