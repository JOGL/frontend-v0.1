import { FC } from 'react';
import Icon from '~/src/components/primitives/Icon';

import useTranslation from 'next-translate/useTranslation';
import { useApi } from '~/src/contexts/apiContext';
import { useModal } from '~/src/contexts/modalContext';
import Button from '~/src/components/primitives/Button';
interface Props {
  postId?: string;
  type?: 'post' | 'comment';
  commentId?: string;
  askForConfirmation?: boolean;
  creatorName?: string;
  refresh?: () => void;
}

const PostDelete: FC<Props> = ({ postId, commentId, type, askForConfirmation = false, creatorName, refresh }) => {
  const api = useApi();
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();

  const deletePost = () => {
    if (postId) {
      api.delete(`/feed/contentEntities/${postId}`).then(() => {
        refresh();
      });
    }
  };

  const deleteComment = () => {
    if (postId && commentId) {
      api.delete(`/feed/contentEntities/${postId}/comments/${commentId}`).then(() => {
        refresh();
      });
    }
  };

  const handleDeleteConfirmation = () => {
    deletePost();
    closeModal();
  };

  const handleDeleteClick = () => {
    if (type === 'comment') {
      deleteComment();
    } else {
      if (askForConfirmation) {
        showModal({
          children: (
            <DeleteConfirmationModal
              creatorName={creatorName}
              handleConfirm={handleDeleteConfirmation}
              handleCancel={closeModal}
            />
          ),
          title: t('areYouSure'),
          maxWidth: '30rem',
        });
      } else {
        deletePost();
      }
    }
  };

  return (
    <div onClick={handleDeleteClick}>
      <Icon icon="ic:round-delete" />
      {t('action.delete')}
    </div>
  );
};

export default PostDelete;

interface DeleteConfirmationModalProps {
  creatorName: string;
  handleConfirm: () => void;
  handleCancel: () => void;
}

const DeleteConfirmationModal: FC<DeleteConfirmationModalProps> = ({ creatorName, handleConfirm, handleCancel }) => {
  const { t } = useTranslation('common');

  return (
    <div tw="flex flex-col gap-2">
      <span tw="self-center">{t('deletePostConfirmation', { name: creatorName })}</span>

      <div tw="flex flex-wrap gap-2 justify-center">
        <Button btnType="secondary" onClick={handleCancel}>
          {t('action.cancel')}
        </Button>
        <Button onClick={handleConfirm}>{t('action.continue')}</Button>
      </div>
    </div>
  );
};
