import Icon from '~/src/components/primitives/Icon';
import Link from 'next/link';
import React, { FC, memo, useContext, useMemo, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Card from '~/src/components/Cards/Card';
import CommentDisplay from '~/src/components/Feed/Comments/CommentDisplay';
import BtnClap from '~/src/components/Tools/BtnClap';
import { useApi } from '~/src/contexts/apiContext';
import useGet from '~/src/hooks/useGet';
import { Post, User } from '~/src/types';
import {
  reportContent,
  copyPostLink,
  entityItem,
  displayObjectRelativeDate,
  confAlert,
  isImageFileType,
  isVideoFileType,
} from '~/src/utils/utils';
import PostDelete from './PostDelete';
import { UserContext } from '~/src/contexts/UserProvider';
import tw from 'twin.macro';
import { useRouter } from 'next/router';
import PostLinksPreviewer from './PostLinksPreviewer';
import { AttachmentsCarousel } from './AttachmentsCarousel';
import { mapDocumentModelToRemoteAttachment } from '../types';
import ReadOnlyEditor from '../../tiptap/readOnlyEditor/readOnlyEditor';
import { Button, Flex, Popover } from 'antd';
import { EllipsisOutlined } from '@ant-design/icons';
import PostUpdate from './PostUpdate';

interface Props {
  user: User;
  postId: string;
  refresh?: () => void;
  isAdmin?: boolean;
  width?: string;
  adminsCanDelete?: boolean;
  allowCommenting?: boolean;
  post?: Post;
  showPostParent?: boolean;
  isPreview?: boolean;
}

const PostDisplay: FC<Props> = ({
  postId,
  user,
  isAdmin,
  width,
  adminsCanDelete,
  allowCommenting = false,
  post: postProps,
  refresh,
  showPostParent = false,
  isPreview = false,
}) => {
  const [showComments, setShowComments] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [shouldFetchPost, setShouldFetchPost] = useState(postProps ? false : true);
  const api = useApi();
  const { locale } = useRouter();
  const { t } = useTranslation('common');
  const userContext = useContext(UserContext);

  const { data: post, mutate: revalPosts } = useGet<Post>(
    shouldFetchPost ? `/feed/contentEntities/${postId}?loadDetails=true` : '',
    { fallbackData: postProps }
  );

  const displayableAttachments = useMemo(() => (post?.documents ?? []).map(mapDocumentModelToRemoteAttachment), [
    post?.documents,
  ]);

  const postHasImagesOrVideos = !!displayableAttachments.find(
    (a) => isImageFileType(a.file_type) || isVideoFileType(a.file_type)
  );

  const postParentUrl =
    post && showPostParent
      ? `/${entityItem[post?.feed_entity.entity_type].front_path}/${
          ['need'].includes(post?.feed_entity.entity_type) ? `parent/` : ''
        }${post?.feed_entity.id}`
      : '';

  const revalPostsComments = () => {
    setShouldFetchPost(true);
    revalPosts();
    refresh && refresh();
  };

  const onDeleteAttachment = (attachmentId: string) => {
    return api.delete(`/feed/contentEntities/${postId}/documents/${attachmentId}`).then(() => revalPostsComments());
  };

  if (post === undefined) {
    return null;
  }

  const ownerId = post.created_by_user_id;
  const logoUrl = post.created_by.logo_url_sm ? post.created_by.logo_url_sm : '/images/default/default-user.png';
  const clapsCount = post.reaction_count?.find((x) => x.type === 'clap')?.count;
  const userImgStyle = { backgroundImage: `url(${logoUrl})` };

  // toggle display of comments
  const changeDisplayComments = () => setShowComments((prevState) => !prevState);

  const isEdit = () => {
    setIsEditing((prevState) => !prevState);
  };

  const onPostDelete = () => {
    confAlert.fire({ icon: 'success', title: t('action.deletePost') });
    refresh();
  };

  return (
    <Card width={width}>
      <div className="post postCard">
        <div className="topContent">
          {/* Top bar */}
          <div className="topBar">
            <div className="left">
              <div tw="mr-2">
                <Link href={`/user/${ownerId}`}>
                  <div className="userImg" style={userImgStyle} />
                </Link>
              </div>
              <div className="topInfo">
                <Link href={`/user/${ownerId}`}>
                  <span tw="font-bold">{`${post.created_by.first_name} ${post.created_by.last_name}`}</span>
                </Link>

                <div className="date">
                  <span>{displayObjectRelativeDate(post.created, locale, false, true)}</span>
                </div>
                {showPostParent && (
                  <div className="from">
                    {`${t('from')}: `}
                    <Link href={postParentUrl}>{post.feed_entity?.title}</Link>
                  </div>
                )}
              </div>
            </div>

            {/* Manage dropdown menu */}
            {!isPreview && (
              <Popover
                placement="bottomRight"
                trigger="click"
                content={
                  <Flex vertical>
                    {user && user.id === ownerId && !isEditing && (
                      <Button type="text" onClick={() => setIsEditing((prevState) => !prevState)}>
                        <Icon icon="bxs:edit" />
                        {t('action.edit')}
                      </Button>
                    )}
                    {(adminsCanDelete || user?.id === ownerId) && (
                      <Button type="text" onClick={() => {}}>
                        <PostDelete
                          postId={postId}
                          type="post"
                          askForConfirmation={user?.id !== ownerId}
                          creatorName={`${post?.created_by?.first_name} ${post?.created_by?.last_name}`}
                          refresh={onPostDelete}
                        />
                      </Button>
                    )}
                    {user && user.id !== ownerId && (
                      <Button type="text" onClick={() => reportContent('post', postId, undefined, api, t)}>
                        {/* on click, launch report function (hide if user is owner */}
                        <Icon icon="fa:flag" tw="w-5 h-5" />
                        {t('action.report')}
                      </Button>
                    )}
                    <Button type="text" onClick={() => copyPostLink(postId, 'post', undefined, t)}>
                      {/* on click, launch copyPostLink function */}
                      <Icon icon="ph:link-bold" tw="w-5 h-5" />
                      {t('action.copyPostLink')}
                    </Button>
                  </Flex>
                }
              >
                <Button type="text" icon={<EllipsisOutlined style={{ fontSize: '26px' }} />} />
              </Popover>
            )}
          </div>

          {/* Actual content (+ edit/update mode) */}
          {!isEditing ? ( // if post is not being edited, show post content
            <>
              <ReadOnlyEditor content={post?.text ?? ''} />
              <div css={[!post?.text && tw`py-2`]}>
                <AttachmentsCarousel
                  isEditing={isEditing}
                  attachments={displayableAttachments}
                  onDeleteAttachment={onDeleteAttachment}
                  isUploading={false}
                />
              </div>

              <PostLinksPreviewer postText={post?.text ?? ''} postHasImagesOrVideos={postHasImagesOrVideos} />
            </>
          ) : (
            // else show post edition component
            <PostUpdate
              post={post}
              displayableAttachments={displayableAttachments}
              closeOrCancelEdit={isEdit}
              onDeleteAttachment={onDeleteAttachment}
              refresh={revalPostsComments}
            />
          )}
        </div>

        {/* Action bar */}
        {!isPreview && (
          <div className="actionBar">
            <div tw="flex pt-2 border-0 gap-5">
              <BtnClap
                itemType="posts"
                postId={post.id}
                userReactions={post.user_reactions}
                clapCount={clapsCount}
                refresh={revalPostsComments}
                userId={userContext?.user?.id}
              />
              <div className="btn-postcard" tw="inline-flex items-center gap-1 cursor-default">
                <button onClick={changeDisplayComments} tabIndex={0} tw="hover:text-action cursor-pointer">
                  <Icon icon="fa6-solid:comments" />
                </button>
                <span>{post.comment_count ?? 0}</span>
                {post.new_comment_count > 0 && (
                  <span tw="font-medium">({t('newCount', { count: post.new_comment_count })})</span>
                )}
                {post.user_mentions_in_comments > 0 && (
                  <div tw="inline-flex items-center font-medium">
                    (
                    <div
                      tw="flex items-center mr-1 justify-center p-2 rounded-full text-[13px] text-white font-semibold [background:linear-gradient(253.99deg, #F54799 -2.61%, #7A21B5 48.72%, #02B3FF 107.65%),linear-gradient(0deg, #FFFFFF, #FFFFFF)]"
                      css={post.user_mentions_in_comments < 10 ? tw`w-[18px] h-[18px]` : tw`w-[22px] h-[22px]`}
                    >
                      {post.user_mentions_in_comments}
                    </div>{' '}
                    mention)
                  </div>
                )}
              </div>
              {/* <ShareBtns type="post" specialObjId={post.id} /> */}
            </div>

            {/* Comments */}
            <CommentDisplay
              postId={postId}
              user={user}
              isAdmin={isAdmin}
              canComment={allowCommenting}
              showComments={showComments}
              feedId={post.feed_entity.id}
              refresh={revalPostsComments}
              hasNewComments={post.new_comment_count > 0}
              hasNewMentions={post.user_mentions_in_comments > 0}
            />
          </div>
        )}
      </div>
    </Card>
  );
};

export default memo(PostDisplay);
