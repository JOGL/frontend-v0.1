import tw, { TwStyle } from 'twin.macro';
import React, { useEffect, useState } from 'react';
import Icon from '~/src/components/primitives/Icon';
import axios from 'axios';
import SpinLoader from '~/src/components/Tools/SpinLoader';
import { AttachmentDisplaySmallProps } from './AttachmentDisplay';
import { DisplayableAttachment, isRemoteAttachment } from '../types';
import { useAttachmentsCarouselStore } from '~/src/store/attachments-carousel/AttachmentsCarouselStoreProvider';
import useTranslation from 'next-translate/useTranslation';
import { useAuth } from '~/auth/auth';

const fetchImage = async (attachment: DisplayableAttachment, authorization: string | null) => {
  let base64: string;
  if (isRemoteAttachment(attachment)) {
    const { data } = await axios.get(attachment.document_url, {
      responseType: 'arraybuffer',
      headers: { 'Content-type': attachment.file_type, Authorization: authorization },
    });

    const encoding = btoa(new Uint8Array(data).reduce((acc, current) => acc + String.fromCharCode(current), ''));
    base64 = `data:${attachment.file_type};base64,${encoding}`;
  } else {
    base64 = attachment.base64.toString();
  }
  return base64;
};

const getDimensionsForSize = (size: 'default' | 'lg' | 'fill'): { width: TwStyle; height: TwStyle } => {
  if (size === 'default') {
    return { width: tw`w-[52px]`, height: tw`h-[52px]` };
  } else if (size === 'lg') {
    return { width: tw`max-w-[25rem]`, height: tw`max-h-[18rem]` };
  } else if (size === 'fill') {
    return { width: tw`w-full`, height: tw`h-full` };
  }
};

const ImageDisplaySmall: React.FC<AttachmentDisplaySmallProps> = ({ size, attachment, onOpenAttachment }) => {
  const { t } = useTranslation('common');
  const { accessToken } = useAuth();
  const [isLoading, setIsLoading] = useState(false);
  const [hasError, setHasError] = useState(false);
  const [imgSource, setImgSource] = useState('');
  const { width, height } = getDimensionsForSize(size);

  useEffect(() => {
    const fetchImageAsync = async () => {
      setIsLoading(true);

      try {
        const image = await fetchImage(attachment, accessToken);
        setImgSource(image);
      } catch (error) {
        console.error(error);
        setHasError(true);
      } finally {
        setIsLoading(false);
      }
    };

    fetchImageAsync();
  }, [attachment, accessToken]);

  return (
    <button type="button" tw="relative rounded-md" onClick={onOpenAttachment}>
      {isLoading && !imgSource && <SpinLoader customCSS={{ marginTop: '16px', marginBottom: '16px' }} />}
      {hasError && (
        <div tw="flex flex-col items-center text-sm text-red-500">
          <Icon icon="material-symbols:error-outline-rounded" tw="w-5 h-5" />
          {t('error.one')}
        </div>
      )}
      {imgSource && (
        <img
          src={imgSource}
          alt={attachment.file_name}
          css={[size === 'default' && tw`object-cover`, width, height]}
          tw="rounded-md"
        />
      )}
    </button>
  );
};

interface ImageDisplayProps {
  attachment: DisplayableAttachment;
}

const ImageDisplay: React.FC<ImageDisplayProps> = ({ attachment }) => {
  const { t } = useTranslation('common');
  const { accessToken } = useAuth();
  const { cachedFile, addFile } = useAttachmentsCarouselStore((s) => ({
    cachedFile: s.cachedFiles[attachment.id],
    addFile: s.addFile,
  }));
  const [isLoading, setIsLoading] = useState(false);
  const [hasError, setHasError] = useState(false);

  useEffect(() => {
    const fetchImageAsync = async () => {
      setIsLoading(true);

      try {
        const image = await fetchImage(attachment, accessToken);
        addFile(attachment.id, image);
      } catch (error) {
        console.error(error);
        setHasError(true);
      } finally {
        setIsLoading(false);
      }
    };

    if (!cachedFile) {
      fetchImageAsync();
    }
  }, [attachment, accessToken, cachedFile, addFile]);

  return (
    <>
      {isLoading && <SpinLoader customCSS={{ marginTop: '16px', marginBottom: '16px' }} />}
      {hasError && (
        <div tw="flex flex-col items-center text-sm text-red-500">
          <Icon icon="material-symbols:error-outline-rounded" tw="w-5 h-5" />
          {t('error.somethingWentWrong')}
        </div>
      )}
      {cachedFile && <img src={cachedFile} alt={attachment.file_name} />}
    </>
  );
};

export { ImageDisplaySmall, ImageDisplay };
