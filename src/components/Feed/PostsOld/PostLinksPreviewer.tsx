import React, { useEffect, useState } from 'react';
import { useApi } from '~/src/contexts/apiContext';
import { entityItem } from '~/src/utils/utils';
import ExternalLinkPreviewer from './ExternalLinkPreviewer';
import InternalLinkPreviewer from './InternalLinkPreviewer';

interface Props {
  postText: string;
  postHasImagesOrVideos: boolean;
}

const PostLinksPreviewer = ({ postText = '', postHasImagesOrVideos }: Props) => {
  const api = useApi();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [firstUrl, setFirstUrl] = useState<string>('');
  const [entitiesFromUrls, setEntitiesFromUrls] = useState([]);

  useEffect(() => {
    const snippet = document.createElement('div');
    snippet.innerHTML = postText;
    const links = snippet.getElementsByTagName('a');

    const urls = Array.from(links).map((x) => x.href);
    // Finds first url that is not from our app.
    setFirstUrl(urls.find((url) => !url.startsWith(process.env.ADDRESS_FRONT)));

    const regexId = /[0-9a-f]{24}/;
    const regexType = /\/(?:[a-z]{2}\/)?(workspace|paper|doc|hub|organization|cfp|need|proposal|event|post)/;

    const allEntitiesFound = urls
      .map((url) => {
        const matchId = url.match(regexId);
        const matchType = url.match(regexType);
        if (matchId && matchType) {
          return { type: matchType[1], id: matchId[0] };
        }
        return null;
      })
      // remove undefined elements (generally the mentions)
      .filter((x) => x);

    const newEntities = allEntitiesFound.filter((e) => !entitiesFromUrls.find((v) => v.id === e.id));
    const entitiesToKeep = entitiesFromUrls.filter((e) => allEntitiesFound.find((v) => v.id === e.id));

    if (isLoading || (newEntities.length === 0 && entitiesToKeep.length === entitiesFromUrls.length)) {
      return;
    } else if (newEntities.length === 0 && entitiesToKeep.length !== entitiesFromUrls.length) {
      setEntitiesFromUrls(entitiesToKeep);
      return;
    }

    // If there's a post we preview only the first one we find and no other posts/containers.
    const firstPostFound = allEntitiesFound.find((e) => e.type === 'post');
    if (firstPostFound) {
      if (entitiesFromUrls.length === 1 && entitiesFromUrls[0].id === firstPostFound.id) {
        return;
      }
      return setEntitiesFromUrls([firstPostFound]);
    }

    setEntitiesFromUrls([...entitiesToKeep, ...newEntities]);
    const fetchAllEntities = async () => {
      try {
        setIsLoading(true);
        const promises = newEntities.map(async (entity) => {
          const apiRootUrl = `${entityItem[entity.type]?.back_path}`;
          return await api.get<{ id: string }>(`/${apiRootUrl}/${entity.id}`);
        });

        const entitiesData = (await Promise.all(promises)).map((res) => res.data);
        setEntitiesFromUrls((s) => s.map((o) => ({ ...entitiesData.find((d) => d.id === o.id), ...o })));
      } catch (error) {
        console.error(error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchAllEntities();
  }, [postText, isLoading, entitiesFromUrls]);

  const displayFirstExternalLink = !postHasImagesOrVideos && entitiesFromUrls.length === 0 && !!firstUrl;

  return (
    <>
      {!postHasImagesOrVideos && <InternalLinkPreviewer entitiesFromUrls={entitiesFromUrls} />}
      {displayFirstExternalLink && <ExternalLinkPreviewer url={firstUrl} />}
    </>
  );
};

export default PostLinksPreviewer;
