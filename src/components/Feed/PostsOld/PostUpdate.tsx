import { FC, useState } from 'react';
import { useApi } from '~/src/contexts/apiContext';
import { Post } from '~/src/types';
import { DisplayableAttachment, isLocalAttachment } from '~/src/components/Feed/types';
import { PostEditor } from '../../tiptap/postEditor/postEditor';

interface Props {
  post: Post;
  displayableAttachments: DisplayableAttachment[];
  onDeleteAttachment: (attachmentId: string) => Promise<void>;
  closeOrCancelEdit: () => void;
  refresh: () => void;
}

const PostUpdate: FC<Props> = ({
  post,
  displayableAttachments: existingAttachments,
  onDeleteAttachment,
  closeOrCancelEdit,
  refresh,
}) => {
  const [displayableAttachments, setDisplayableAttachments] = useState<DisplayableAttachment[]>(existingAttachments);
  const [isUploading, setIsUploading] = useState(false);
  const api = useApi();

  const handleChangeDoc = async (attachments: DisplayableAttachment[]) => {
    try {
      setDisplayableAttachments(attachments);
      refresh();
    } catch (err) {
      console.error(err);
    }
  };

  const handleDeleteDocument = async (attachmentId: string): Promise<void> => {
    const indexToDelete = displayableAttachments.findIndex((fileToInspect) => fileToInspect.id === attachmentId);
    if (indexToDelete === undefined) {
      return;
    }

    onDeleteAttachment(attachmentId).then(() => {
      handleChangeDoc([
        ...displayableAttachments.slice(0, indexToDelete),
        ...displayableAttachments.slice(indexToDelete + 1),
      ]);
    });
  };

  const handleSubmit = async (newContent: string) => {
    const updateJson = {
      text: newContent,
      documents_to_add: displayableAttachments.filter(isLocalAttachment).map((a) => ({
        title: a.title,
        file_name: a.file_name,
        description: 'description',
        data: a.base64,
      })),
    };

    try {
      setIsUploading(true);
      await api.patch(`/feed/contentEntities/${post.id}`, updateJson);

      // record event to Google Analytics
      closeOrCancelEdit(); // close update/edit box after update
      refresh();
    } catch (error) {
      console.error(error);
      refresh();
    } finally {
      setIsUploading(false);
    }
  };
  return (
    <PostEditor
      content={post.text}
      feedId={post?.feed_entity?.id ?? ''}
      canMentionEveryone={false}
      displayableAttachments={displayableAttachments}
      isEditing
      isUploading={isUploading}
      handleChangeDoc={handleChangeDoc}
      deleteDocument={handleDeleteDocument}
      handleSubmit={handleSubmit}
      handleCancel={closeOrCancelEdit}
    />
  );
};

export default PostUpdate;
