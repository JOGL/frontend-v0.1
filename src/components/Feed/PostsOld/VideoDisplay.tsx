import tw, { TwStyle } from 'twin.macro';
import Icon from '~/src/components/primitives/Icon';
import { AttachmentDisplaySmallProps } from './AttachmentDisplay';
import { useState } from 'react';
import axios from 'axios';
import { DisplayableAttachment, downloadUrlFromDisplayableAttachment, isRemoteAttachment } from '../types';
import { useAttachmentsCarouselStore } from '~/src/store/attachments-carousel/AttachmentsCarouselStoreProvider';
import SpinLoader from '~/src/components/Tools/SpinLoader';
import useTranslation from 'next-translate/useTranslation';
import { useAuth } from '~/auth/auth';

const getDimensionsForSize = (
  size: 'default' | 'lg' | 'fill'
): { width: TwStyle; height: TwStyle; iconDimensions: TwStyle } => {
  if (size === 'default') {
    return { width: tw`w-[52px]`, height: tw`h-[52px]`, iconDimensions: tw`w-[40%] h-[40%]` };
  } else if (size === 'lg') {
    return { width: tw`w-auto`, height: tw`h-[18rem]`, iconDimensions: tw`w-[30%] h-[30%]` };
  } else if (size === 'fill') {
    return { width: tw`w-auto`, height: tw`h-full`, iconDimensions: tw`w-[30%] h-[30%]` };
  }
};

const VideoDisplaySmall: React.FC<AttachmentDisplaySmallProps> = ({ attachment, size, onOpenAttachment }) => {
  const { t } = useTranslation('common');
  const { width, height, iconDimensions } = getDimensionsForSize(size);

  return (
    <button type="button" tw="relative" onClick={onOpenAttachment}>
      <video
        tw="relative rounded-md bg-[#DBDBDB]"
        preload="metadata"
        autoPlay={false}
        css={[size === 'default' && tw`object-cover`, width, height]}
      >
        {/* By simply adding #t=0.001 at the end of the video file url, we are telling the browser to skip the first millisecond of the video.
            When you do this, even iOS Safari will preload and show that specific frame to the user */}
        <source src={`${downloadUrlFromDisplayableAttachment(attachment)}#t=0.001`} type={attachment.file_type} />
        {t('browserDoesNotSupportContent', { type: t('video.one').toLowerCase() })}.
      </video>
      <Icon
        icon="ph:play-fill"
        tw="absolute top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%]"
        css={[iconDimensions]}
        style={{ color: 'white' }}
      />
    </button>
  );
};

interface VideoDisplayProps {
  attachment: DisplayableAttachment;
}

const VideoDisplay: React.FC<VideoDisplayProps> = ({ attachment }) => {
  const { t } = useTranslation('common');
  const { accessToken } = useAuth();
  const { cachedFile, addFile } = useAttachmentsCarouselStore((s) => ({
    cachedFile: s.cachedFiles[attachment.id],
    addFile: s.addFile,
  }));
  const [isLoading, setIsLoading] = useState(false);
  const [hasError, setHasError] = useState(false);
  const isFirstLoad = !cachedFile && !isLoading;

  const fetchVideoAsync = async () => {
    try {
      let base64: string;

      setIsLoading(true);
      if (isRemoteAttachment(attachment)) {
        const { data } = await axios.get(attachment.document_url, {
          responseType: 'arraybuffer',
          headers: { 'Content-type': attachment.file_type, Authorization: accessToken },
        });

        const encoding = btoa(new Uint8Array(data).reduce((acc, current) => acc + String.fromCharCode(current), ''));
        base64 = `data:${attachment.file_type};base64,${encoding}`;
      } else {
        base64 = attachment.base64.toString();
      }

      addFile(attachment.id, base64);
    } catch (error) {
      console.error(error);
      setHasError(true);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div>
      {isLoading && <SpinLoader customCSS={{ marginTop: '16px', marginBottom: '16px' }} />}
      {hasError && (
        <div tw="flex flex-col items-center text-sm text-red-500">
          <Icon icon="material-symbols:error-outline-rounded" tw="w-5 h-5" />
          {t('error.somethingWentWrong')}
        </div>
      )}
      {isFirstLoad && (
        <video key={attachment.id} preload="metadata" controls autoPlay={true}>
          {/* If the media download service worker isn't working properly this will fail with 401
            and we have to download the full video manually */}
          <source
            src={downloadUrlFromDisplayableAttachment(attachment)}
            type={attachment.file_type}
            onError={() => fetchVideoAsync()}
          />
        </video>
      )}
      {cachedFile && (
        <video key={attachment.id} preload="metadata" controls autoPlay={true}>
          <source src={cachedFile} type={attachment.file_type} />
          {t('browserDoesNotSupportContent', { type: t('video.one').toLowerCase() })}.
        </video>
      )}
    </div>
  );
};

export { VideoDisplaySmall, VideoDisplay };
