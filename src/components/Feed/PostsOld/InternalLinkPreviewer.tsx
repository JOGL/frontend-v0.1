import React from 'react';
import {
  entityItem,
  getContainerAccessLevelChip,
  getContainerJoiningRestrictionChip,
  getContainerVisibilityChip,
} from '~/src/utils/utils';
import HubCard from '~/src/components/Hub/HubCard';
import Carousel from '~/src/components/Common/Carousel/Carousel';
import OrganizationCard from '~/src/components/Organization/OrganizationCard';
import ProposalCard from '~/src/components/Proposal/ProposalCard';
import CfpCard from '~/src/components/Cfp/CfpCard';
import PublicationCard from '~/src/components/Publications/PublicationCard';
import { DocumentCard } from '~/src/components/Tools/createDocument/DocumentCard';
import NeedCard from '~/src/components/Need/NeedCard';
import EventCard from '~/src/components/Event/EventCard';
import PostDisplay from './PostDisplay';
import useUserData from '~/src/hooks/useUserData';
import WorkspaceCard from '../../workspace/workspaceCard/workspaceCard';

interface Props {
  entitiesFromUrls: any[];
}

const InternalLinkPreviewer: React.FC<Props> = ({ entitiesFromUrls }) => {
  const { userData } = useUserData();
  const hasSingleInternalUrl = entitiesFromUrls.length < 2;

  return (
    <CardsWrapper hasSingleChild={hasSingleInternalUrl}>
      {entitiesFromUrls.map((entity) =>
        entity.type === 'workspace' ? (
          <WorkspaceCard
            key={entity.id}
            id={entity.id}
            title={entity.title}
            short_description={entity.short_description}
            banner_url={entity.banner_url || entity.banner_url_sm}
            cardFormat="inPosts"
            chip={entity?.user_access_level && getContainerAccessLevelChip(entity)}
            imageChips={
              !entity?.user_access_level
                ? [getContainerVisibilityChip(entity), getContainerJoiningRestrictionChip(entity)]
                : [getContainerVisibilityChip(entity)]
            }
            textChips={[{ name: 'Workspace' }]}
            {...(hasSingleInternalUrl && { width: '100%' })}
          />
        ) : entity.type === 'hub' ? (
          <HubCard
            key={entity.id}
            id={entity.id}
            title={entity.title}
            short_title={entity.short_title}
            short_description={entity.short_description}
            status={entity.status}
            banner_url={entity.banner_url || entity.banner_url_sm}
            cardFormat="inPosts"
            chip={entity?.user_access_level && getContainerAccessLevelChip(entity)}
            imageChips={
              !entity?.user_access_level
                ? [getContainerVisibilityChip(entity), getContainerJoiningRestrictionChip(entity)]
                : [getContainerVisibilityChip(entity)]
            }
            textChips={[{ name: 'Hub' }]}
            {...(hasSingleInternalUrl && { width: '100%' })}
          />
        ) : entity.type === 'organization' ? (
          <OrganizationCard
            id={entity.id}
            key={entity.id}
            short_title={entity.short_title}
            title={entity.title}
            short_description={entity.short_description}
            status={entity.status}
            banner_url={entity.banner_url || entity.banner_url_sm}
            cardFormat="inPosts"
            chip={entity?.user_access_level && getContainerAccessLevelChip(entity)}
            imageChips={
              !entity?.user_access_level
                ? [getContainerVisibilityChip(entity), getContainerJoiningRestrictionChip(entity)]
                : [getContainerVisibilityChip(entity)]
            }
            textChips={[{ name: 'Organization' }]}
            {...(hasSingleInternalUrl && { width: '100%' })}
          />
        ) : entity.type === 'cfp' ? (
          <CfpCard
            id={entity.id}
            key={entity.id}
            short_title={entity.short_title}
            title={entity.title}
            short_description={entity.short_description}
            status={entity.status}
            banner_url={entity.banner_url || entity.banner_url_sm}
            submissions_from={entity.submissions_from}
            submissions_to={entity.submissions_to}
            cardFormat="inPosts"
            chip={entity?.user_access_level && getContainerAccessLevelChip(entity)}
            imageChips={
              !entity?.user_access_level
                ? [getContainerVisibilityChip(entity), getContainerJoiningRestrictionChip(entity)]
                : [getContainerVisibilityChip(entity)]
            }
            textChips={[{ name: 'Call for proposals' }]}
            {...(hasSingleInternalUrl && { width: '100%' })}
          />
        ) : entity.type === 'proposal' ? (
          <ProposalCard
            key={entity.id}
            proposal={entity}
            cardFormat="inPosts"
            textChips={[{ name: 'Proposal' }]}
            {...(hasSingleInternalUrl && { width: '100%' })}
          />
        ) : entity.type === 'paper' ? (
          <PublicationCard
            key={entity.id}
            item={entity}
            canManageLibrary={false}
            cardFormat="inPosts"
            publicationUrl={`/paper/${entity.id}`}
            {...(hasSingleInternalUrl && { width: '100%' })}
          />
        ) : entity.type === 'doc' ? (
          <DocumentCard
            key={entity.id}
            type="jogldoc"
            title={entity.title}
            content={entity.summary}
            src={entity?.image_url}
            user={{
              first_name: entity?.created_by?.first_name,
              last_name: entity?.created_by?.last_name,
              id: entity?.created_by?.id,
              logoUrl: entity?.created_by?.logo_url_sm,
            }}
            id={entity.id}
            showMenu={false}
            cardFormat="inPosts"
          />
        ) : entity.type?.includes('need') && entity?.entity?.type ? (
          <NeedCard
            key={entity.id}
            need={entity}
            textChips={[{ name: 'Need' }]}
            cardFormat="inPosts"
            {...(hasSingleInternalUrl && { width: '100%' })}
          />
        ) : entity.type === 'event' && entity?.feed_entity ? (
          <EventCard
            key={entity.id}
            sourceType={entityItem[entity.feed_entity.entity_type].back_path}
            sourceId={entity.feed_entity?.id}
            event={entity}
            cardFormat="inPosts"
            {...(hasSingleInternalUrl && { width: '100%' })}
          />
        ) : entity.type === 'post' ? (
          <PostDisplay key={entity.id} user={userData} postId={entity.id} isAdmin={false} isPreview={true} />
        ) : null
      )}
    </CardsWrapper>
  );
};

interface CardsWrapperProps {
  hasSingleChild: boolean;
  children: JSX.Element[];
}

const CardsWrapper: React.FC<CardsWrapperProps> = ({ hasSingleChild, children }) => {
  return hasSingleChild ? <div>{children}</div> : <Carousel showDots={false}>{children}</Carousel>;
};

export default InternalLinkPreviewer;
