import Image from 'next/image';
import { isOfficeSuiteFileType, isPdfFileType } from '~/src/utils/utils';
import { AttachmentDisplaySmallProps } from './AttachmentDisplay';
import { useEffect, useState } from 'react';
import axios from 'axios';
import SpinLoader from '~/src/components/Tools/SpinLoader';
import Icon from '~/src/components/primitives/Icon';
import { DisplayableAttachment, isRemoteAttachment } from '../types';
import { useAttachmentsCarouselStore } from '~/src/store/attachments-carousel/AttachmentsCarouselStoreProvider';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from '~/src/contexts/apiContext';
import { useAuth } from '~/auth/auth';
import { PdfView } from '../../file/pdfView/pdfView';

const DocumentDisplaySmall: React.FC<AttachmentDisplaySmallProps> = ({ attachment, onOpenAttachment }) => {
  const isPdf = isPdfFileType(attachment.file_type);

  return (
    <button
      type="button"
      tw="flex w-[190px] h-[52px] m-0 p-1.5 gap-3 rounded-md bg-[#DBDBDB]"
      onClick={onOpenAttachment}
    >
      <Image
        src={isPdf ? '/images/doc/pdf-logo.svg' : '/images/doc/unknown-file-type-logo.svg'}
        alt="file-type-icon"
        width={36}
        height={36}
      />
      <div tw="text-left text-xs overflow-hidden">
        <div tw="font-bold overflow-hidden text-ellipsis whitespace-nowrap">{attachment.file_name}</div>
        <div tw="text-[#868686] overflow-hidden text-ellipsis whitespace-nowrap">{attachment.file_type}</div>
      </div>
    </button>
  );
};

interface DocumentDisplayProps {
  attachment: DisplayableAttachment;
}

const DocumentDisplay: React.FC<DocumentDisplayProps> = ({ attachment }) => {
  const api = useApi();
  const { t } = useTranslation('common');
  const { accessToken } = useAuth();
  const { cachedFile, addFile } = useAttachmentsCarouselStore((s) => ({
    cachedFile: s.cachedFiles[attachment.id],
    addFile: s.addFile,
  }));
  const [isLoading, setIsLoading] = useState(false);
  const [hasError, setHasError] = useState(false);

  useEffect(() => {
    const fetchPdfAsync = async () => {
      setIsLoading(true);

      try {
        let base64: string;
        if (isRemoteAttachment(attachment)) {
          const { data } = await axios.get(attachment.document_url, {
            responseType: 'arraybuffer',
            headers: { 'Content-type': attachment.file_type, Authorization: accessToken },
          });

          const encoding = btoa(new Uint8Array(data).reduce((acc, current) => acc + String.fromCharCode(current), ''));
          base64 = `data:${attachment.file_type};base64,${encoding}`;

          if (isOfficeSuiteFileType(attachment.file_name, attachment.file_type)) {
            const { data } = await api.post('documents/convert/pdf', {
              from_format: attachment.file_type,
              data: base64,
            });
            base64 = data;
          }
        } else {
          base64 = attachment.base64.toString();
        }

        addFile(attachment.id, base64);
      } catch (error) {
        console.error(error);
        setHasError(true);
      } finally {
        setIsLoading(false);
      }
    };

    if (!cachedFile) {
      fetchPdfAsync();
    }
  }, [attachment, accessToken, cachedFile, addFile]);

  const isRemoteOfficeSuiteFile =
    isOfficeSuiteFileType(attachment.file_name, attachment.file_type) && isRemoteAttachment(attachment);

  if (!isPdfFileType(attachment.file_type) && !isRemoteOfficeSuiteFile) {
    return (
      <div tw="bg-white p-4 rounded-lg w-[60vw]">
        <p tw="font-bold">{attachment.file_name}</p>
        <p tw="text-sm font-[#5F5D5D]">
          {t('type')}:{' '}
          <span tw="rounded-md px-2 py-1 bg-[rgba(209, 209, 209, 0.50)]">{attachment.file_name.split('.')[1]}</span>
        </p>
        <p>{t('noPreviewForThisFileType')}.</p>
      </div>
    );
  }

  return (
    <>
      {isLoading && <SpinLoader customCSS={{ marginTop: '16px', marginBottom: '16px' }} />}
      {hasError && (
        <div tw="flex flex-col items-center text-sm text-red-500">
          <Icon icon="material-symbols:error-outline-rounded" tw="w-5 h-5" />
          {t('error.somethingWentWrong')}
        </div>
      )}
      {cachedFile && <PdfView file={cachedFile} />}
    </>
  );
};

export { DocumentDisplaySmall, DocumentDisplay };
