import React, { useCallback, useEffect, useState } from 'react';
import Icon from '~/src/components/primitives/Icon';
import tw from 'twin.macro';
import { confAlert, isAudioFileType, isImageFileType, isVideoFileType } from '~/src/utils/utils';
import SpinLoader from '~/src/components/Tools/SpinLoader';
import { ImageDisplay, ImageDisplaySmall } from './ImageDisplay';
import { AudioDisplay, AudioDisplaySmall } from './AudioDisplay';
import { VideoDisplay, VideoDisplaySmall } from './VideoDisplay';
import { DocumentDisplay, DocumentDisplaySmall } from './DocumentDisplay';
import useTranslation from 'next-translate/useTranslation';
import { ActiveAttachment } from './AttachmentsCarousel';
import Button from '~/src/components/primitives/Button';
import { DisplayableAttachment, isRemoteAttachment } from '../types';
import { useAuth } from '~/auth/auth';

export interface AttachmentDisplaySmallProps {
  size: 'default' | 'lg' | 'fill';
  isEditing: boolean;
  isUploading: boolean;
  attachment: DisplayableAttachment;
  onOpenAttachment: () => void;
}

interface Props extends AttachmentDisplaySmallProps {
  onDeleteAttachment: (attachmentId: string) => Promise<void>;
}

const AttachmentDisplaySmall: React.FC<Props> = ({
  size,
  isEditing,
  isUploading,
  attachment,
  onDeleteAttachment,
  onOpenAttachment,
}) => {
  const { t } = useTranslation('common');
  const [isLoading, setIsLoading] = useState(false);
  const [isHovering, setIsHovering] = useState(false);

  const onDeleteDocument = () => {
    setIsLoading(true);
    onDeleteAttachment(attachment.id)
      .then(() => {})
      .catch((error) => {
        confAlert.fire({ icon: 'error', title: t('someUnknownErrorOccurred') });
        console.error({ error });
      })
      .finally(() => setIsLoading(false));
  };

  const isLoadingOrUploading = isLoading || isUploading;

  return (
    <div css={[isEditing && tw`pt-4`]}>
      <div
        tw="relative inline-block"
        css={[isHovering && tw`brightness-95`]}
        onFocus={() => setIsHovering(true)}
        onMouseEnter={() => setIsHovering(true)}
        onMouseLeave={() => setIsHovering(false)}
      >
        {isImageFileType(attachment.file_type) ? (
          <ImageDisplaySmall
            size={size}
            isEditing={isEditing}
            isUploading={isUploading}
            attachment={attachment}
            onOpenAttachment={isLoading ? null : onOpenAttachment}
          />
        ) : isAudioFileType(attachment.file_type) ? (
          <AudioDisplaySmall
            size={size}
            isEditing={isEditing}
            isUploading={isUploading}
            attachment={attachment}
            onOpenAttachment={isLoading ? null : onOpenAttachment}
          />
        ) : isVideoFileType(attachment.file_type) ? (
          <VideoDisplaySmall
            size={size}
            isEditing={isEditing}
            isUploading={isUploading}
            attachment={attachment}
            onOpenAttachment={isLoading ? null : onOpenAttachment}
          />
        ) : (
          <DocumentDisplaySmall
            size={size}
            isEditing={isEditing}
            isUploading={isUploading}
            attachment={attachment}
            onOpenAttachment={isLoading ? null : onOpenAttachment}
          />
        )}
        {isEditing && (
          <div
            tw="flex items-center justify-center absolute w-6 h-6 right-0 top-0 translate-x-[30%] translate-y-[-50%] rounded-full bg-[#666] border border-solid border-white overflow-hidden"
            onBlur={() => setIsHovering(false)}
          >
            <button type="button" onClick={onDeleteDocument} disabled={isLoadingOrUploading} tw="w-4 h-4 p-0">
              {isLoadingOrUploading && <SpinLoader customCSS={{ margin: 0, width: 16, height: 16 }} />}
              {!isLoadingOrUploading && <Icon icon="radix-icons:cross-2" tw="text-white m-0 w-4 h-4" />}
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

interface AttachmentDisplayProps {
  attachment: DisplayableAttachment;
  setActiveAttachment: React.Dispatch<React.SetStateAction<ActiveAttachment>>;
}

const AttachmentDisplay: React.FC<AttachmentDisplayProps> = ({ attachment, setActiveAttachment }) => {
  const { t } = useTranslation('common');
  const { accessToken } = useAuth();
  const [isDownloading, setIsDownloading] = useState(false);

  // To prevent users from scrolling the page behind the modal overlay
  useEffect(() => {
    document.body.style.overflow = 'hidden';
    return () => {
      document.body.style.overflow = 'unset';
    };
  }, []);

  const onActiveAttachmentDownload = useCallback(
    (ev) => {
      ev.stopPropagation();

      if (isRemoteAttachment(attachment) && !attachment?.document_url) {
        return null;
      }

      const download = async () => {
        try {
          setIsDownloading(true);
          let downloadUrl: string;
          if (isRemoteAttachment(attachment)) {
            const image = await fetch(attachment.document_url, {
              method: 'GET',
              headers: {
                'Content-Type': attachment.file_type,
                Authorization: accessToken,
              },
            });
            const imageBlog = await image.blob();
            downloadUrl = URL.createObjectURL(imageBlog);
          } else {
            downloadUrl = attachment.base64 as string;
          }

          const link = document?.createElement('a');
          link.href = downloadUrl;
          link.download = attachment.title;
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        } catch (error) {
          confAlert.fire({ icon: 'error', title: t('someUnknownErrorOccurred') });
        } finally {
          setIsDownloading(false);
        }
      };

      download();
    },
    [attachment, accessToken]
  );

  return (
    <div tw="flex flex-col h-full" onClick={() => setActiveAttachment(undefined)}>
      <div tw="flex flex-col gap-4 items-center md:(flex-row justify-between)">
        <button
          type="button"
          tw="flex flex-col items-center text-2xl text-white gap-2 md:(flex-row gap-0)"
          onClick={(ev) => {
            ev.stopPropagation();
            setActiveAttachment(undefined);
          }}
        >
          <Icon icon="formkit:arrowleft" tw="m-0 mr-6 w-12" />
          {attachment?.file_name}
        </button>
        <Button type="button" tw="px-4" onClick={onActiveAttachmentDownload} disabled={isDownloading}>
          {isDownloading && <SpinLoader />}
          {t('action.download')}
        </Button>
      </div>
      <div
        tw="relative mx-auto mt-16 max-w-[90vw] max-h-[85vh] overflow-auto [scrollbar-width: thin]"
        onClick={(ev) => ev.stopPropagation()}
      >
        {isImageFileType(attachment.file_type) ? (
          <ImageDisplay attachment={attachment} />
        ) : isAudioFileType(attachment.file_type) ? (
          <AudioDisplay attachment={attachment} />
        ) : isVideoFileType(attachment.file_type) ? (
          <VideoDisplay attachment={attachment} />
        ) : (
          <DocumentDisplay attachment={attachment} />
        )}
      </div>
    </div>
  );
};

export { AttachmentDisplaySmall, AttachmentDisplay };
