import { AxiosInstance } from 'axios';
import { DocumentModel } from '~/__generated__/types';
import {
  convertBase64,
  isAudioFileType,
  isHeifOrHeicFileType,
  isImageFileType,
  isVideoFileType,
} from '~/src/utils/utils';

export type DisplayableAttachment = LocalAttachment | RemoteAttachment;

export interface LocalAttachment {
  type: 'local';
  id: string;
  title: string;
  file_name: string;
  file_type: string;
  size: number;
  is_media: boolean;
  base64: string | ArrayBuffer;
  document_url: string;
}

export interface RemoteAttachment {
  type: 'remote';
  id: string;
  title: string;
  file_name: string;
  file_type: string;
  document_url: string;
  is_media: boolean;
}

export const isLocalAttachment = (attachment: LocalAttachment | RemoteAttachment): attachment is LocalAttachment =>
  attachment.type === 'local';
export const isRemoteAttachment = (attachment: LocalAttachment | RemoteAttachment): attachment is RemoteAttachment =>
  attachment.type === 'remote';

export const getAttachmentIdFromFile = (file: File): string => `${file.name}-${file.type}-${file.lastModified}`;

export const isMedia = (fileType: string) => {
  return isAudioFileType(fileType) || isImageFileType(fileType) || isVideoFileType(fileType);
};
export const mapFileToLocalAttachment = async (file: File, api: AxiosInstance): Promise<LocalAttachment> => {
  let base64: string = await convertBase64(file);
  let file_name: string = file.name;
  let file_type: string = file.type;
  if (isHeifOrHeicFileType(file.name, file.type)) {
    const format = 'image/jpg';
    const { data } = await api.post('images/convert', {
      to_format: format,
      data: base64,
    });
    file_name = `${file_name.split('.')[0]}.jpg`;
    file_type = format;
    base64 = data;
  }

  return {
    type: 'local',
    is_media: isMedia(file_type),
    id: getAttachmentIdFromFile(file),
    title: file_name,
    file_name,
    file_type,
    size: file.size,
    base64,
    document_url: base64,
  };
};

export const mapDocumentModelToRemoteAttachment = (document: DocumentModel): RemoteAttachment => ({
  type: 'remote',
  id: document.id,
  title: document.title,
  file_name: document.file_name,
  file_type: document.file_type,
  document_url: document.document_url,
  is_media: document.is_media,
});

export const downloadUrlFromDisplayableAttachment = (attachment: DisplayableAttachment): string =>
  isRemoteAttachment(attachment) ? attachment.document_url : attachment.base64.toString();

export const DISCUSSION_SUPPORTED_FILE_TYPES = [
  '.jpg,.jpeg,.png,.gif,.webp,.bmp,.tiff,.pdf,.doc,.docx,.xls,.xlsx,.ppt,.pptx,.rtf,.txt,.mp3,.wav,.aac,.ogg,.flac,.mp4,.webm,.avi,.zip,.rar,.tar,.gz,.7z,.html,.htm,.xml,.json,.csv,.md',
];
