import Button from '~/src/components/primitives/Button';
import SpinLoader from '~/src/components/Tools/SpinLoader';
import { User } from '~/src/types';
import FormWysiwygComponent from '~/src/components/Tools/Forms/FormWysiwygComponent';
import tw from 'twin.macro';
import Icon from '~/src/components/primitives/Icon';
import BtnUploadFile from '~/src/components/Tools/BtnUploadFile';
import { AttachmentsCarousel } from '../PostsOld/AttachmentsCarousel';
import useTranslation from 'next-translate/useTranslation';
import { MouseEventHandler, useState } from 'react';
import { confAlert, isImageFileType, isVideoFileType } from '~/src/utils/utils';
import {
  DISCUSSION_SUPPORTED_FILE_TYPES,
  DisplayableAttachment,
  isLocalAttachment,
  mapFileToLocalAttachment,
} from '../types';
import { MAX_PAYLOAD_SIZE } from '~/src/utils/constants';
import { useApi } from '~/src/contexts/apiContext';
import PostLinksPreviewer from '../PostsOld/PostLinksPreviewer';

interface Props {
  action: 'create' | 'update';
  content?: string;
  handleChange: (content: string) => void;
  handleSubmit: () => void;
  handleChangeDoc?: (attachment: DisplayableAttachment[]) => void;
  onClick?: () => void;
  isUploading?: boolean;
  isExpanded?: boolean;
  user: User;
  postId: string;
  id?: string;
  feedId?: string;
  displayableAttachments: DisplayableAttachment[];
}

const FormComment: React.FC<Props> = ({
  action = 'create',
  content = '',
  user,
  postId,
  id,
  feedId,
  isUploading = false,
  isExpanded = false,
  displayableAttachments,
  handleChange,
  handleSubmit,
  handleChangeDoc,
  onClick,
}) => {
  const api = useApi();
  const { t } = useTranslation('common');
  const postTypeClass = action === 'create' ? 'postCreate commentCreate' : 'commentUpdate';
  const userImg = user?.logo_url_sm ? user.logo_url_sm : '/images/default/default-user.png';
  const userImgStyle = { backgroundImage: `url(${userImg})` };
  const [isProcessing, setIsProcessing] = useState<boolean>(false);
  const [uploadFileError, setUploadFileError] = useState<string>('');

  const commentHasImagesOrVideos = !!displayableAttachments.find(
    (a) => isImageFileType(a.file_type) || isVideoFileType(a.file_type)
  );

  const hasValidPayloadSize = (files: { size: number }[]): boolean => {
    const payloadSize = files.reduce((acc, f) => f.size + acc, 0);

    if (payloadSize > MAX_PAYLOAD_SIZE) {
      setUploadFileError(t('error.maxPayloadSizeExceeded'));
      return false;
    }
    return true;
  };

  const attachDocuments = async (newDocuments: FileList) => {
    try {
      setIsProcessing(true);
      const files = Array.from(newDocuments) || [];

      const allFiles = (files as { size: number }[]).concat(displayableAttachments.filter(isLocalAttachment));
      if (!hasValidPayloadSize(allFiles)) {
        return;
      }

      const attachments = await Promise.all<DisplayableAttachment>(
        files.map((file) => mapFileToLocalAttachment(file, api))
      );
      handleChangeDoc([...displayableAttachments, ...attachments]);
    } catch (error) {
      console.error(error);
      confAlert.fire({ icon: 'error', title: t('error.somethingWentWrong') });
    } finally {
      setIsProcessing(false);
    }
  };

  const deleteDocument = async (attachmentId: string): Promise<void> => {
    const indexToDelete = displayableAttachments.findIndex((fileToInspect) => fileToInspect.id === attachmentId);
    if (indexToDelete === undefined) {
      return;
    }

    setUploadFileError('');
    handleChangeDoc([
      ...displayableAttachments.slice(0, indexToDelete),
      ...displayableAttachments.slice(indexToDelete + 1),
    ]);
  };

  const onSubmit: MouseEventHandler<HTMLButtonElement> = (ev) => {
    ev.stopPropagation();

    if (!hasValidPayloadSize(displayableAttachments.filter(isLocalAttachment))) {
      return;
    }

    handleSubmit();
  };

  const isLoading = isUploading || isProcessing;
  const hasContent = (!!content && content !== '<p><br></p>') || displayableAttachments.length > 0;

  return (
    <div className={postTypeClass} id={`post${postId}`} css={!isExpanded && tw`cursor-pointer`}>
      <form>
        <div className="inputBox" onClick={onClick && onClick}>
          {action === 'create' && ( // if post action is create, display image on the left
            <div className="userImgContainer">
              <div className="userImg" style={userImgStyle} />
            </div>
          )}
          <div tw="w-full min-w-0">
            {(isExpanded || action === 'update') && (
              <div tw="border-[#e5e7eb] border rounded-md w-full">
                <FormWysiwygComponent
                  id={feedId}
                  title=""
                  placeholder={t('action.doActionHere', { action: t('reply.one') })}
                  content={content}
                  onChange={(_, v) => handleChange(v)}
                  specialFormat="postComment"
                  isExpanded={isExpanded}
                />
              </div>
            )}
            {!isExpanded && action !== 'update' && (
              <div tw="text-sm text-[#2125295c] border w-full rounded-lg border-gray-300 p-3">
                {t('action.doActionHere', { action: t('reply.one') })}
              </div>
            )}
            {(isExpanded || action === 'update') && (
              <>
                <div tw="flex py-2 justify-between">
                  <div tw="flex items-center">
                    <div onClick={() => setUploadFileError('')}>
                      <BtnUploadFile
                        customCss={tw`relative bg-white m-0 border h-8 w-8`}
                        setListFiles={attachDocuments}
                        uploadNow={false}
                        fileTypes={DISCUSSION_SUPPORTED_FILE_TYPES}
                        singleFileOnly={false}
                        uploadIcon="uil:paperclip"
                        onChange={(result) => setUploadFileError(result.error ? t('error.maxPayloadSizeExceeded') : '')}
                        isParentLoading={isProcessing}
                      />
                    </div>
                    {uploadFileError && <span tw="ml-2 text-danger">{uploadFileError}</span>}
                  </div>

                  {hasContent && (
                    <Button className="commentBtn" disabled={isLoading} onClick={onSubmit}>
                      {isLoading && <SpinLoader />}
                      {!isLoading && <Icon icon="material-symbols:send" tw="text-primary cursor-pointer" />}
                    </Button>
                  )}
                </div>

                <AttachmentsCarousel
                  isEditing
                  isUploading={isUploading}
                  attachments={displayableAttachments}
                  onDeleteAttachment={deleteDocument}
                />
                <PostLinksPreviewer postText={content} postHasImagesOrVideos={commentHasImagesOrVideos} />
              </>
            )}
          </div>
        </div>
      </form>
    </div>
  );
};

export default FormComment;
