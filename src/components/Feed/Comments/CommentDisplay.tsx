import React, { FC, useState, useEffect, useMemo, useCallback, useRef } from 'react';
import Link from 'next/link';
import useTranslation from 'next-translate/useTranslation';
import CommentUpdate from './CommentUpdate';
import PostDelete from '~/src/components/Feed/PostsOld/PostDelete';
import {
  reportContent,
  copyPostLink,
  displayObjectRelativeDate,
  isImageFileType,
  isVideoFileType,
} from '~/src/utils/utils';
import { useApi } from '~/src/contexts/apiContext';
import { useRouter } from 'next/router';
import { User } from '~/src/types';
import { Menu, MenuItem, MenuButton, MenuList } from '@reach/menu-button';
import Icon from '~/src/components/primitives/Icon';
import useInfiniteLoading from '~/src/hooks/useInfiniteLoading';
import SpinLoader from '~/src/components/Tools/SpinLoader';
import CommentCreate from './CommentCreate';
import InfoHtmlComponent from '~/src/components/Tools/Info/InfoHtmlComponent';
import { Comment } from '~/src/types/comment';
import { AttachmentsCarousel } from '../PostsOld/AttachmentsCarousel';
import { DisplayableAttachment, mapDocumentModelToRemoteAttachment } from '../types';
import tw from 'twin.macro';
import PostLinksPreviewer from '../PostsOld/PostLinksPreviewer';

const PAGE_SIZE = 10;

interface Props {
  user: User;
  isAdmin: boolean;
  postId: string;
  showComments?: boolean;
  canComment?: boolean;
  feedId?: string;
  refresh?: any;
  hasNewComments: boolean;
  hasNewMentions: boolean;
}

interface StateReference {
  wereCommentsEverVisible: boolean;
  comments: Comment[];
  hasNewComments: boolean;
  hasNewMentions: boolean;
  postId: string;
}

const CommentDisplay: FC<Props> = ({
  user,
  isAdmin = false,
  postId,
  showComments,
  canComment,
  feedId,
  refresh,
  hasNewComments,
  hasNewMentions,
}) => {
  const [selectedCommentToEdit, setSelectedCommentToEdit] = useState<string>();
  const [commentIdToHighlight, setCommentIdToHighlight] = useState<string>('');
  const [shouldFetchComments, setShouldFetchComments] = useState<boolean>(showComments);
  const api = useApi();
  const router = useRouter();
  const { t } = useTranslation('common');

  const { data: dataComments, isLoading, size, setSize, mutate: revalComments } = useInfiniteLoading<Comment[]>(
    (index) =>
      shouldFetchComments || showComments
        ? `/feed/contentEntities/${postId}/comments?PageSize=${PAGE_SIZE}&Page=${index + 1}`
        : ''
  );
  const comments: Comment[] = useMemo(() => (dataComments ? [].concat(...dataComments) : []), [dataComments]);

  const stateRef = useRef<StateReference>({
    wereCommentsEverVisible: showComments,
    comments,
    hasNewComments,
    hasNewMentions,
    postId,
  });

  stateRef.current = {
    wereCommentsEverVisible: stateRef.current.wereCommentsEverVisible || showComments,
    comments,
    hasNewComments,
    hasNewMentions,
    postId,
  };

  const isLoadingMore = isLoading || (size > 0 && dataComments && typeof dataComments[size - 1] === 'undefined');
  const isEmpty = dataComments?.[0]?.length === 0;
  const isReachingEnd = isEmpty || (dataComments && dataComments[dataComments.length - 1]?.length < PAGE_SIZE);

  const refreshComments = useCallback(() => {
    setShouldFetchComments(true);
    revalComments();
    refresh(); // mainly to update comments count on the post parent component
  }, [setShouldFetchComments, revalComments, refresh]);

  useEffect(() => {
    const runMarkAsSeenLogic = async () => {
      const { wereCommentsEverVisible, comments, hasNewComments, hasNewMentions, postId } = stateRef.current;
      if (!wereCommentsEverVisible) {
        return;
      }

      const commentsWithMentions = comments.filter((x) => x.user_mentions > 0);
      const markAsSeenRequests = [];

      if (hasNewComments) {
        markAsSeenRequests.push(api.post(`/feed/contentEntities/${postId}/seen`));
      }

      if (hasNewMentions && commentsWithMentions.length !== 0) {
        markAsSeenRequests.push(
          api.post(
            `/feed/contentEntities/comments/seen`,
            commentsWithMentions.map((x) => x.id)
          )
        );
      }

      if (markAsSeenRequests.length > 0) {
        await Promise.all(markAsSeenRequests);
      }
    };

    window.addEventListener('beforeunload', () => {
      runMarkAsSeenLogic();
    });
    return () => {
      runMarkAsSeenLogic();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // if url has a hash (link to a comment), get it
  useEffect(() => {
    setCommentIdToHighlight(
      typeof window !== 'undefined' && router.asPath.match(/#([a-z0-9-]+)/gi)
        ? router.asPath.match(/#([a-z0-9-]+)/gi)[0]
        : window.location.hash
    );
  }, []);

  const onEdit = (commentKey: string) => setSelectedCommentToEdit(commentKey);

  return (
    <>
      {showComments && (
        <>
          {canComment && <CommentCreate postId={postId} refresh={refreshComments} user={user} feedId={feedId} />}
          <div className="commentDisplay">
            <div className="actionBox">
              {comments && (
                <div className="commentList">
                  {comments.map((comment) => {
                    const userImg = comment.created_by?.logo_url_sm
                      ? comment.created_by.logo_url_sm
                      : '/images/default/default-user.png';
                    const userImgStyle = { backgroundImage: 'url(' + userImg + ')' };
                    const ownerId = comment.user_id;

                    // TODO: this needs to be memoized
                    const displayableAttachments: DisplayableAttachment[] = (comment?.documents ?? []).map(
                      mapDocumentModelToRemoteAttachment
                    );
                    // TODO: probably too costly this refresh?
                    const onDeleteAttachment = (attachmentId: string) => {
                      return api
                        .delete(`/feed/contentEntities/${postId}/documents/${attachmentId}`)
                        .then(() => refreshComments());
                    };

                    const commentHasImagesOrVideos = !!displayableAttachments.find(
                      (a) => isImageFileType(a.file_type) || isVideoFileType(a.file_type)
                    );

                    return (
                      <div
                        key={comment.id}
                        className={`comment ${commentIdToHighlight === `#comment-${comment.id}` && 'highlight'}`}
                        id={`comment-${comment.id}`}
                      >
                        <div className="topContent">
                          <div className="topBar" tw="flex-none">
                            <div className="left">
                              <div className="userImgContainer">
                                <Link href={'/user/' + ownerId}>
                                  <div className="userImg" style={userImgStyle} />
                                </Link>
                              </div>
                              <div className="comment-content" tw="(shadow-sm bg-gray-100)!">
                                <div tw="flex justify-between">
                                  <div tw="inline-flex gap-1">
                                    <Link href={'/user/' + ownerId}>
                                      <span tw="text-[14px] font-bold text-black">
                                        {`${comment.created_by?.first_name} ${comment.created_by?.last_name}`}
                                      </span>
                                    </Link>
                                    <span tw="font-semibold text-gray-400">·</span>
                                    <div tw="flex items-center gap-1 text-[#5F5D5D]">
                                      <span tw="text-[12px]">
                                        {displayObjectRelativeDate(comment.created, router?.locale, false, true)}
                                      </span>
                                    </div>
                                  </div>
                                  {/* Manage dropdown menu */}
                                  <Menu>
                                    <MenuButton tw="font-size[.8rem] height[fit-content] text-gray-700 px-1 hover:bg-gray-300 rounded-sm">
                                      •••
                                    </MenuButton>
                                    <MenuList className="post-manage-dropdown">
                                      {user && user.id === ownerId && (
                                        <>
                                          {comment.id === selectedCommentToEdit ? (
                                            !selectedCommentToEdit ? (
                                              <MenuItem onSelect={() => onEdit(comment.id)}>
                                                <Icon icon="bxs:edit" /> {t('action.edit')}
                                              </MenuItem>
                                            ) : (
                                              ''
                                            )
                                          ) : (
                                            <MenuItem onSelect={() => onEdit(comment.id)}>
                                              <Icon icon="bxs:edit" />
                                              {t('action.edit')}
                                            </MenuItem>
                                          )}
                                          <MenuItem onSelect={() => {}}>
                                            <PostDelete
                                              postId={postId}
                                              commentId={comment.id}
                                              type="comment"
                                              refresh={refreshComments}
                                            />
                                          </MenuItem>
                                        </>
                                      )}
                                      {user &&
                                      user.id !== ownerId &&
                                      isAdmin && ( // if user is admin and it's NOT his post
                                          // set origin to other (not your post)
                                          <MenuItem onSelect={() => {}}>
                                            <PostDelete
                                              postId={postId}
                                              commentId={comment.id}
                                              type="comment"
                                              refresh={refreshComments}
                                            />
                                          </MenuItem>
                                        )}
                                      <MenuItem onSelect={() => copyPostLink(postId, 'comment', comment.id, t)}>
                                        {/* on click, launch copyPostLink function */}
                                        <Icon icon="ph:link-bold" tw="w-5 h-5" />
                                        {t('action.copyReplyLink')}
                                      </MenuItem>
                                      {user && user.id !== ownerId && (
                                        <MenuItem onSelect={() => reportContent('comment', postId, comment.id, api, t)}>
                                          {' '}
                                          {/* on click, launch report function (hide if user is owner */}
                                          <Icon icon="fa:flag" tw="w-5 h-5" />
                                          {t('action.report')}
                                        </MenuItem>
                                      )}
                                    </MenuList>
                                  </Menu>
                                </div>
                                {comment.id === selectedCommentToEdit ? ( // if comment is being edited, show comment edition component
                                  <CommentUpdate
                                    postId={postId}
                                    feedId={feedId}
                                    commentId={comment.id}
                                    content={comment.text}
                                    closeOrCancelEdit={() => onEdit('')}
                                    refresh={refreshComments}
                                    user={user}
                                  />
                                ) : (
                                  // else show comment edition component
                                  <div className="commentTextContainer">
                                    {' '}
                                    <InfoHtmlComponent content={comment.text} />
                                  </div>
                                )}
                                <div css={[!comment.text && tw`py-2`]}>
                                  <AttachmentsCarousel
                                    isEditing={comment.id === selectedCommentToEdit}
                                    attachments={displayableAttachments}
                                    onDeleteAttachment={onDeleteAttachment}
                                    isUploading={false}
                                  />
                                </div>
                                {comment.id !== selectedCommentToEdit && (
                                  <PostLinksPreviewer
                                    postText={comment?.text}
                                    postHasImagesOrVideos={commentHasImagesOrVideos}
                                  />
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                  {/* show load more button if object has more items than the default items we get from first call, or if we still have not attained last call page */}
                  {comments?.length !== 0 && !isReachingEnd && (
                    <div
                      tw="mt-4 cursor-pointer underline text-gray-700 hover:text-black"
                      onClick={() => {
                        setSize(size + 1);
                        revalComments();
                      }}
                    >
                      {isLoadingMore && <SpinLoader />}
                      {!isReachingEnd ? t('action.load') : t('noMoreResults')}
                    </div>
                  )}
                </div>
              )}
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default CommentDisplay;
