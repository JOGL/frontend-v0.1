import { FC } from 'react';
import { useState } from 'react';
import FormComment from './FormComment';
import { useApi } from '~/src/contexts/apiContext';
import { User } from '~/src/types';
import { DisplayableAttachment, isLocalAttachment } from '../types';

interface Props {
  content?: string; // but is never passed on when CommentCreate component is called elsewhere in the app
  postId?: string;
  feedId: string;
  user?: User;
  refresh?: (content?: any) => void | Promise<any>;
  fromDiscussion?: boolean;
}

const CommentCreate: FC<Props> = ({
  content: contentProp = '',
  postId = undefined,
  feedId,
  user,
  refresh,
  fromDiscussion = false,
}) => {
  const [content, setContent] = useState(contentProp);
  const [isUploading, setIsUploading] = useState(false);
  const [isExpanded, setIsExpanded] = useState<boolean>(false);
  const [displayableAttachments, setDisplayableAttachments] = useState<DisplayableAttachment[]>([]);
  const api = useApi();

  const handleChange = (newContent) => {
    setContent(newContent);
  };

  const handleChangeDoc = async (attachments: DisplayableAttachment[]) => {
    try {
      setDisplayableAttachments(attachments);
      refresh();
    } catch (err) {
      console.error(err);
    }
  };

  const handleSubmit = () => {
    const commentJson = {
      text: content,
      reply_to_id: postId,
      documents_to_add: displayableAttachments.filter(isLocalAttachment).map((a) => ({
        title: a.title,
        file_name: a.file_name,
        description: 'description',
        data: a.base64,
      })),
    };
    if (fromDiscussion) {
      setIsUploading(true);
      (refresh(content) as Promise<any>)
        .then(() => {
          setContent('');
          const commentBtn = document.querySelector(`.commentCreate#post${postId} .commentBtn`); // select comment button from parent post
          commentBtn.style.display = 'none'; // hide comment button after comment was posted
        })
        .finally(() => setIsUploading(false));
      return;
    }
    setIsUploading(true);
    api
      .post(`/feed/contentEntities/${postId}/comments`, commentJson)
      .then(() => {
        // record event to Google Analytics
        setContent('');
        setDisplayableAttachments([]);
        const commentBtn = document.querySelector(`.commentCreate#post${postId} .commentBtn`); // select comment button from parent post
        commentBtn.style.display = 'none'; // hide comment button after comment was posted
        refresh();
        // send event to google analytics
      })
      .catch((err) => console.error(`Couldn't POST comment in post with id=${postId}`, err))
      .finally(() => setIsUploading(false));
  };

  return (
    <FormComment
      action="create"
      content={content}
      displayableAttachments={displayableAttachments}
      handleChange={handleChange}
      handleChangeDoc={handleChangeDoc}
      handleSubmit={handleSubmit}
      isUploading={isUploading}
      user={user}
      postId={postId}
      feedId={feedId}
      onClick={() => setIsExpanded(true)}
      isExpanded={isExpanded}
    />
  );
};

export default CommentCreate;
