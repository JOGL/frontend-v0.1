import { FC } from 'react';
import { useState } from 'react';
import FormComment from './FormComment';
import { useApi } from '~/src/contexts/apiContext';
import { User } from '~/src/types';
import { DisplayableAttachment, isLocalAttachment } from '../types';

interface Props {
  postId: string;
  feedId: string;
  content: string;
  commentId: string;
  user: User;
  refresh: () => void;
  closeOrCancelEdit: () => void;
}

const CommentUpdate: FC<Props> = ({
  postId,
  feedId,
  content: contentProp = '',
  commentId = '',
  user,
  refresh,
  closeOrCancelEdit,
}) => {
  const [content, setContent] = useState(contentProp);
  const api = useApi();
  const [displayableAttachments, setDisplayableAttachments] = useState<DisplayableAttachment[]>([]);
  const [isUploading, setIsUploading] = useState<boolean>(false);

  const handleChange = (newContent) => {
    setContent(newContent);
  };

  const handleChangeDoc = async (attachments: DisplayableAttachment[]) => {
    try {
      setDisplayableAttachments(attachments);
      refresh();
    } catch (err) {
      console.error(err);
    }
  };

  const handleSubmit = () => {
    const updateJson = {
      text: content,
      documents_to_add: displayableAttachments.filter(isLocalAttachment).map((a) => ({
        title: a.title,
        file_name: a.file_name,
        description: 'description',
        data: a.base64,
      })),
    };

    setIsUploading(true);

    api
      .patch(`/feed/contentEntities/${postId}/comments/${commentId}`, updateJson)
      .then(() => {
        // record event to Google Analytics
        closeOrCancelEdit(); // close comment update box after update
        refresh();
      })
      .catch((err) => console.error(`Couldn't PATCH post comment in post with id=${postId}`, err))
      .finally(() => setIsUploading(false));
  };

  return (
    <FormComment
      action="update"
      content={content}
      displayableAttachments={displayableAttachments}
      handleChange={handleChange}
      handleChangeDoc={handleChangeDoc}
      handleSubmit={handleSubmit}
      id={commentId}
      isUploading={isUploading}
      user={user}
      postId={postId}
      feedId={feedId}
    />
  );
};

export default CommentUpdate;
