import { UserOutlined } from '@ant-design/icons';
import { Avatar, Flex, Space } from 'antd';
import { FC } from 'react';
import { ReactionModel } from '~/__generated__/types';
import useUser from '~/src/hooks/useUser';
import { ListStyled, ReactionItemWrapper, TextStyled } from './reactionTab.styles';
import useTranslation from 'next-translate/useTranslation';
import { NativeEmojiRenderer } from '~/src/components/nativeEmojiRenderer/nativeEmojiRenderer';

interface Props {
  reactions: ReactionModel[];
  handleDeleteReaction: (reactionId: string) => void;
}

export const ReactionTab: FC<Props> = ({ reactions, handleDeleteReaction }) => {
  const { t } = useTranslation('common');
  const { user } = useUser();

  const userReaction = reactions.find((reaction) => reaction.created_by_user_id === user.id);
  return (
    <ListStyled
      dataSource={reactions}
      rowKey="id"
      renderItem={(reaction: ReactionModel) => (
        <ReactionItemWrapper
          key={reaction.id}
          isUserReaction={!!(userReaction && userReaction.id === reaction.id)}
          align="center"
          justify="space-between"
          onClick={() => handleDeleteReaction(reaction.id)}
        >
          <Space>
            <Avatar shape="circle" size="small" src={reaction?.created_by?.logo_url} icon={<UserOutlined />} />
            <Flex vertical>
              <TextStyled>
                {reaction?.created_by?.first_name} {reaction?.created_by?.last_name}
              </TextStyled>

              {userReaction && reaction.id === userReaction.id && (
                <TextStyled type="secondary">{t('clickToRemove')}</TextStyled>
              )}
            </Flex>
          </Space>

          <NativeEmojiRenderer unified={reaction.key} size={14} />
        </ReactionItemWrapper>
      )}
    />
  );
};
