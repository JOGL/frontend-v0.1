import styled from '@emotion/styled';
import { Flex, List, Typography } from 'antd';
import { ReactionModel } from '~/__generated__/types';

export const ListStyled = styled(List<ReactionModel>)`
  height: 150px;
  width: 100%;
  overflow: auto;
`;

export const TextStyled = styled(Typography.Text)`
  font-size: ${({ theme }) => theme.token.fontSizeSM}px;
`;

export const ReactionItemWrapper = styled(Flex)<{ isUserReaction: boolean }>`
  padding-inline: ${({ theme }) => theme.token.paddingXXS}px;;
  margin-bottom: ${({ theme }) => theme.token.marginXXS}px;
  pointer-events: ${({ isUserReaction }) => (isUserReaction ? 'all' : 'none')};
  cursor: ${({ isUserReaction }) => (isUserReaction ? 'pointer' : 'auto')};

  :hover {
    background: ${({ isUserReaction, theme }) => (isUserReaction ? theme.token.colorBgTextHover : 'unset')};
    border-radius: ${({ isUserReaction, theme }) => (isUserReaction ? theme.token.borderRadiusSM : '0')}px;
  }
`;
