import { Space, Spin } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { FC, useMemo } from 'react';
import { ReactionsBaseProps } from '../reactions.types';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { ReactionTab } from './reactionTab/reactionTab';
import { TabsStyled } from './allReactions.styles';
import { NativeEmojiRenderer } from '~/src/components/nativeEmojiRenderer/nativeEmojiRenderer';

interface Props extends ReactionsBaseProps {
  totalCount: number;
}

export const AllReactions: FC<Props> = ({
  type,
  reactedContentId,
  reaction_count,
  totalCount,
  handleDeleteReaction,
}) => {
  const { t } = useTranslation('common');

  const { data: postReactions, isLoading: isLoadingPostReactions } = useQuery({
    queryKey: [QUERY_KEYS.postReactionDetails, reactedContentId],
    queryFn: async () => {
      const response = await api.feed.contentEntitiesReactionsDetail(reactedContentId);
      return response.data;
    },
    enabled: type === 'post',
  });

  const { data: commentReactions, isLoading: isLoadingCommentReactions } = useQuery({
    queryKey: [QUERY_KEYS.commentReactionDetails, reactedContentId],
    queryFn: async () => {
      const response = await api.feed.commentsReactionsDetail(reactedContentId);
      return response.data;
    },
    enabled: type === 'comment',
  });

  const tabs = useMemo(() => {
    const allReactions = (type === 'post' ? postReactions : commentReactions) ?? [];

    const allTab = {
      key: 'all',
      label: (
        <Space align="center" size={4}>
          <>{t('all')}</>
          <>{totalCount}</>
        </Space>
      ),
      children: <ReactionTab reactions={allReactions} handleDeleteReaction={handleDeleteReaction} />,
    };

    const emojiTabs = reaction_count.map((reaction) => ({
      key: reaction.key,
      label: (
        <Space align="center" size={4}>
          <NativeEmojiRenderer unified={reaction.key} size={14} />
          <>{reaction.count}</>
        </Space>
      ),
      children: (
        <ReactionTab
          reactions={allReactions.filter((r) => r.key === reaction.key)}
          handleDeleteReaction={handleDeleteReaction}
        />
      ),
    }));

    return [allTab, ...emojiTabs];
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [commentReactions, postReactions, reaction_count, totalCount, type]);

  if (isLoadingPostReactions || isLoadingCommentReactions) return <Spin />;

  return <TabsStyled defaultActiveKey="all" items={tabs} tabBarGutter={18} />;
};
