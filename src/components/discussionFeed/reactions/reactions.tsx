import { Button, Grid, Popover, Space, Typography } from 'antd';
import React, { FC, useMemo, useState } from 'react';
import { AllReactions } from './allReactions/allReactions';
import { ReactionsBaseProps } from './reactions.types';
import { NativeEmojiRenderer } from '../../nativeEmojiRenderer/nativeEmojiRenderer';

const { useBreakpoint } = Grid;

export const Reactions: FC<ReactionsBaseProps> = ({ type, reactedContentId, reaction_count, handleDeleteReaction }) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const screens = useBreakpoint();

  const { topFourReactions, totalCount } = useMemo(() => {
    return {
      topFourReactions: reaction_count.slice(0, 4).map((r) => r.key),
      totalCount: reaction_count.reduce((sum, r) => sum + r.count, 0),
    };
  }, [reaction_count]);

  const handleDelete = (reactionId: string) => {
    setIsOpen(false);
    handleDeleteReaction(reactionId);
  };

  return (
    <Popover
      content={
        <AllReactions
          type={type}
          reactedContentId={reactedContentId}
          reaction_count={reaction_count}
          totalCount={totalCount}
          handleDeleteReaction={handleDelete}
        />
      }
      trigger="click"
      placement={screens.sm ? 'bottomLeft' : 'bottom'}
      arrow={false}
      overlayStyle={{ width: screens.sm ? 300 : 200 }}
      open={isOpen}
      onOpenChange={(newOpen) => setIsOpen(newOpen)}
    >
      <Button size="small" onClick={() => setIsOpen(!isOpen)}>
        <Space align="center" size={4}>
          {topFourReactions.map((reaction, index) => (
            <NativeEmojiRenderer key={`reaction-${index}`} unified={reaction} size={14} />
          ))}
          <Typography.Text strong>{totalCount}</Typography.Text>
        </Space>
      </Button>
    </Popover>
  );
};
