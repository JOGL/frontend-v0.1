import { ReactionCountModel } from '~/__generated__/types';

export type ReactedContentType = 'post' | 'comment';

export interface ReactionsBaseProps {
  type: ReactedContentType;
  reactedContentId: string;
  reaction_count: ReactionCountModel[];
  handleDeleteReaction: (reactionId: string) => void;
}
