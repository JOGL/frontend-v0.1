import { FC, useMemo, useState } from 'react';
import { MoreOutlined, UserOutlined } from '@ant-design/icons';
import { Avatar, Button, Dropdown, Flex, MenuProps, Modal, Space, Tooltip, Typography } from 'antd';
import { ContentEntityModel, ReactionUpsertModel } from '~/__generated__/types';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { mapDocumentModelToRemoteAttachment } from '~/src/components/Feed/types';
import { copyPostLink, displayObjectRelativeDate, isImageFileType, isVideoFileType } from '~/src/utils/utils';
import ReadOnlyEditor from '~/src/components/tiptap/readOnlyEditor/readOnlyEditor';
import { AttachmentsCarousel } from '~/src/components/Feed/PostsOld/AttachmentsCarousel';
import PostLinksPreviewer from '~/src/components/Feed/PostsOld/PostLinksPreviewer';
import { ButtonStyled } from '../post.styles';
import {
  AuthorButtonStyled,
  AvatarStyled,
  DiscussionItemEditorWrapper,
  DiscussionItemHeaderWrapper,
  DiscussionItemWrapper,
  FlexStyled,
  TagButtonStyled,
  TextStyled,
} from '../../../discussionDetails/discussionFeed.styles';
import { EmojiPicker } from '~/src/components/emojiPicker/emojiPicker';
import { Reactions } from '../../reactions/reactions';
import { useQueryClient } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { useDeletePost, usePostReactions, useReportPost } from '../../chat/chat.hooks';
import useUser from '~/src/hooks/useUser';
import { PostUpdate } from '../postUpdate/postUpdate';

interface Props {
  post: ContentEntityModel;
  canMentionEveryone: boolean;
  canComment: boolean;
  canDeletePosts: boolean;
  handleSelectPost: (post?: ContentEntityModel) => void;
  refreshChatContent: () => void;
}

const { Text } = Typography;

export const PostDisplay: FC<Props> = ({
  post,
  canMentionEveryone,
  canComment,
  canDeletePosts,
  handleSelectPost,
  refreshChatContent,
}) => {
  const [isEditing, setIsEditing] = useState<boolean>(false);
  const [modal, contextHolder] = Modal.useModal();
  const { user } = useUser();
  const { push, locale } = useRouter();
  const { t } = useTranslation('common');
  const queryClient = useQueryClient();
  const deletePost = useDeletePost({ successCallback: refreshChatContent });
  const reportPost = useReportPost();

  const refreshAfterReactionChange = () => {
    queryClient.invalidateQueries({
      queryKey: [QUERY_KEYS.postReactionDetails, post.id],
    });

    refreshChatContent();
  };
  const { reactToPost, deletePostReaction } = usePostReactions({ successCallback: refreshAfterReactionChange });

  const handleSubmitReaction = (emoji: ReactionUpsertModel) => {
    reactToPost.mutate({ contentId: post.id, reaction: emoji });
  };

  const handleDeleteReaction = (reactionId: string) => {
    deletePostReaction.mutate({ contentId: post.id, reactionId });
  };
  const displayableAttachments = useMemo(() => (post?.documents ?? []).map(mapDocumentModelToRemoteAttachment), [
    post?.documents,
  ]);

  const postHasImagesOrVideos = useMemo(
    () => !!displayableAttachments.find((a) => isImageFileType(a.file_type) || isVideoFileType(a.file_type)),
    [displayableAttachments]
  );

  const showDeleteWarning = () => {
    modal.confirm({
      title: t('areYouSure'),
      content: t('thisActionIsIrreversible'),
      onOk: () => deletePost.mutate(post.id),
      okText: t('action.delete'),
      cancelText: t('action.cancel'),
      okButtonProps: { loading: deletePost.isPending },
    });
  };

  const isAuthor = useMemo(() => {
    return user?.id === post.created_by_user_id;
  }, [user, post]);

  const dropdownItems: MenuProps['items'] = useMemo(
    () => [
      ...(!isEditing && isAuthor ? [{ label: t('action.edit'), key: 'edit', onClick: () => setIsEditing(true) }] : []),
      {
        label: t('action.copyPostLink'),
        key: 'copyPostLink',
        onClick: () => copyPostLink(post.id, 'post', undefined, t),
      },
      ...(!isAuthor && user
        ? [
            {
              label: t('action.report'),
              key: 'report',
              danger: true,
              onClick: () => reportPost.mutate(post.id),
            },
          ]
        : []),
      ...(canDeletePosts || isAuthor
        ? [
            {
              label: t('action.delete'),
              key: 'delete',
              danger: true,
              onClick: () => showDeleteWarning(),
            },
          ]
        : []),
    ],

    // eslint-disable-next-line react-hooks/exhaustive-deps
    [post, isAuthor, canDeletePosts, isEditing]
  );
  return (
    <DiscussionItemWrapper gap="small" id={post.id} key={post.id} isNew={post.is_new}>
      <AvatarStyled
        size={36}
        shape="circle"
        src={post?.overrides?.user_image_url ?? post?.created_by?.logo_url}
        icon={<UserOutlined />}
      />
      <FlexStyled flex={1} vertical>
        <DiscussionItemHeaderWrapper wrap gap="small" align="center" justify="space-between">
          <Space>
            <AuthorButtonStyled
              type="link"
              href={post?.overrides?.user_url}
              target="_blank"
              onClick={() => push(`/user/${post.created_by_user_id}`)}
            >
              <Text strong>
                {post?.overrides?.user_name ?? post?.created_by?.first_name + ' ' + post?.created_by?.last_name}
              </Text>
            </AuthorButtonStyled>

            <TextStyled type="secondary">
              {displayObjectRelativeDate(post.created, locale ?? 'en', false, false)}
            </TextStyled>
          </Space>

          <Dropdown menu={{ items: dropdownItems }} trigger={['click']}>
            <Button type="text" size="small" icon={<MoreOutlined />} />
          </Dropdown>
        </DiscussionItemHeaderWrapper>

        {isEditing ? (
          <DiscussionItemEditorWrapper>
            <PostUpdate
              post={post}
              canMentionEveryone={canMentionEveryone}
              displayableAttachments={displayableAttachments}
              refresh={refreshChatContent}
              closeOrCancelEdit={() => setIsEditing(false)}
            />
          </DiscussionItemEditorWrapper>
        ) : (
          <>
            <ReadOnlyEditor content={post?.text ?? ''} />
            <AttachmentsCarousel
              isEditing={false}
              isUploading={false}
              attachments={displayableAttachments}
              onDeleteAttachment={async () => {}}
            />

            <PostLinksPreviewer postText={post?.text ?? ''} postHasImagesOrVideos={postHasImagesOrVideos} />

            {post?.updated && <TextStyled type="secondary">({t('edited').toLocaleLowerCase()})</TextStyled>}
          </>
        )}

        {!isEditing && (
          <Flex wrap align="center" gap="small" justify="space-between">
            <Flex wrap gap="small" align="center">
              {post.users?.length > 0 && (
                <Avatar.Group size="small" shape="circle" max={{ count: 5 }}>
                  {post.users.map((user) => (
                    <Tooltip key={user.id} title={`${user.first_name} ${user.last_name}`} placement="top">
                      <Avatar src={user.logo_url} icon={<UserOutlined />} alt={user.username} />
                    </Tooltip>
                  ))}
                </Avatar.Group>
              )}

              {/* Only show the "Reply" button if user can comment */}
              {(post.comment_count > 0 || canComment) && (
                <ButtonStyled
                  type="link"
                  onClick={() => handleSelectPost(post)}
                  style={post.comment_count === 0 ? { height: 'auto' } : undefined}
                >
                  {post.comment_count > 0 ? (
                    <>
                      {post.comment_count} {t('reply', { count: post.comment_count }).toLocaleLowerCase()}
                    </>
                  ) : (
                    t('reply.one')
                  )}
                </ButtonStyled>
              )}

              {post.new_comment_count > 0 && (
                <TagButtonStyled type="primary" size="small" onClick={() => handleSelectPost(post)}>
                  {post.new_comment_count} {t('new').toLocaleUpperCase()}
                </TagButtonStyled>
              )}

              {post.user_mentions_in_comments > 0 && (
                <TagButtonStyled type="primary" size="small" onClick={() => handleSelectPost(post)}>
                  @{post.user_mentions_in_comments}
                </TagButtonStyled>
              )}

              {post.last_activity && (
                <TextStyled type="secondary">
                  {displayObjectRelativeDate(post.last_activity, locale ?? 'en', false, false)}
                </TextStyled>
              )}
            </Flex>

            <Flex flex={1} gap="small" align="center" justify="flex-end">
              {post.reaction_count?.length > 0 && (
                <Reactions
                  type="post"
                  reactedContentId={post.id}
                  reaction_count={post.reaction_count}
                  handleDeleteReaction={handleDeleteReaction}
                />
              )}

              {user && <EmojiPicker handleEmojiSelect={handleSubmitReaction} />}
            </Flex>
          </Flex>
        )}
      </FlexStyled>

      {contextHolder}
    </DiscussionItemWrapper>
  );
};
