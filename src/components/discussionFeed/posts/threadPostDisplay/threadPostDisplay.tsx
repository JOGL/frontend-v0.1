import { FC, useMemo, useState } from 'react';
import { MoreOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Dropdown, Flex, MenuProps, Modal, Space, Typography } from 'antd';
import { ContentEntityModel, ReactionUpsertModel } from '~/__generated__/types';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { mapDocumentModelToRemoteAttachment } from '~/src/components/Feed/types';
import { copyPostLink, displayObjectRelativeDate, isImageFileType, isVideoFileType } from '~/src/utils/utils';
import ReadOnlyEditor from '~/src/components/tiptap/readOnlyEditor/readOnlyEditor';
import { AttachmentsCarousel } from '~/src/components/Feed/PostsOld/AttachmentsCarousel';
import PostLinksPreviewer from '~/src/components/Feed/PostsOld/PostLinksPreviewer';
import { LoadRepliesButtonStyled } from '../post.styles';
import useUser from '~/src/hooks/useUser';
import { PostUpdate } from '../postUpdate/postUpdate';
import { useQueryClient } from '@tanstack/react-query';
import {
  AuthorButtonStyled,
  AvatarStyled,
  DiscussionItemEditorWrapper,
  DiscussionItemHeaderWrapper,
  DiscussionItemWrapper,
  FlexStyled,
  TextStyled,
} from '../../../discussionDetails/discussionFeed.styles';
import { EmojiPicker } from '~/src/components/emojiPicker/emojiPicker';
import { Reactions } from '../../reactions/reactions';
import { useDeletePost, usePostReactions, useReportPost } from '../../chat/chat.hooks';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';

const { Text } = Typography;

interface Props {
  post: ContentEntityModel;
  canMentionEveryone: boolean;
  canDeletePosts: boolean;
  hasMoreReplies: boolean;
  unfetchedRepliesTotal: number;
  fetchMoreReplies: () => void;
  refreshAfterUpdate: () => void;
  handleDelete: () => void;
}

export const ThreadPostDisplay: FC<Props> = ({
  post,
  canMentionEveryone,
  canDeletePosts,
  hasMoreReplies,
  unfetchedRepliesTotal,
  fetchMoreReplies,
  refreshAfterUpdate,
  handleDelete,
}) => {
  const [isEditing, setIsEditing] = useState<boolean>(false);
  const [modal, contextHolder] = Modal.useModal();
  const { push, locale } = useRouter();
  const { user } = useUser();
  const { t } = useTranslation('common');
  const queryClient = useQueryClient();
  const deletePost = useDeletePost({ successCallback: handleDelete });
  const reportPost = useReportPost();

  const refreshAfterReactionChange = () => {
    queryClient.invalidateQueries({
      queryKey: [QUERY_KEYS.postReactionDetails, post.id],
    });

    refreshAfterUpdate();
  };
  const { reactToPost, deletePostReaction } = usePostReactions({ successCallback: refreshAfterReactionChange });

  const handleSubmitReaction = (emoji: ReactionUpsertModel) => {
    reactToPost.mutate({ contentId: post.id, reaction: emoji });
  };

  const handleDeleteReaction = (reactionId: string) => {
    deletePostReaction.mutate({ contentId: post.id, reactionId });
  };

  const displayableAttachments = useMemo(() => (post?.documents ?? []).map(mapDocumentModelToRemoteAttachment), [
    post?.documents,
  ]);

  const postHasImagesOrVideos = useMemo(
    () => !!displayableAttachments.find((a) => isImageFileType(a.file_type) || isVideoFileType(a.file_type)),
    [displayableAttachments]
  );

  const showDeleteWarning = () => {
    modal.confirm({
      title: t('areYouSure'),
      content: t('thisActionIsIrreversible'),
      onOk: () => deletePost.mutate(post.id),
      okText: t('action.delete'),
      cancelText: t('action.cancel'),
      okButtonProps: { loading: deletePost.isPending },
    });
  };

  const isAuthor = useMemo(() => {
    return user?.id === post.created_by_user_id;
  }, [user, post]);

  const dropdownItems: MenuProps['items'] = useMemo(
    () => [
      ...(!isEditing && isAuthor ? [{ label: t('action.edit'), key: 'edit', onClick: () => setIsEditing(true) }] : []),
      {
        label: t('action.copyPostLink'),
        key: 'copyPostLink',
        onClick: () => copyPostLink(post.id, 'post', undefined, t),
      },
      ...(!isAuthor && user
        ? [
            {
              label: t('action.report'),
              key: 'report',
              danger: true,
              onClick: () => reportPost.mutate(post.id),
            },
          ]
        : []),
      ...(canDeletePosts || isAuthor
        ? [
            {
              label: t('action.delete'),
              key: 'delete',
              danger: true,
              onClick: () => showDeleteWarning(),
            },
          ]
        : []),
    ],

    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isAuthor, canDeletePosts, isEditing]
  );

  return (
    <DiscussionItemWrapper gap="small" id={post.id} key={post.id}>
      <AvatarStyled
        size={36}
        shape="circle"
        src={post?.overrides?.user_image_url ?? post?.created_by?.logo_url}
        icon={<UserOutlined />}
      />

      <FlexStyled flex={1} vertical>
        <DiscussionItemHeaderWrapper wrap gap="small" align="center" justify="space-between">
          <Space>
            <AuthorButtonStyled
              type="link"
              href={post?.overrides?.user_url}
              target="_blank"
              onClick={() => push(`/user/${post.created_by_user_id}`)}
            >
              <Text strong>
                {post?.overrides?.user_name ?? post?.created_by?.first_name + ' ' + post?.created_by?.last_name}
              </Text>
            </AuthorButtonStyled>

            <TextStyled type="secondary">
              {displayObjectRelativeDate(post.created, locale ?? 'en', false, false)}
            </TextStyled>
          </Space>

          <Dropdown menu={{ items: dropdownItems }} trigger={['click']}>
            <Button type="text" size="small" icon={<MoreOutlined />} />
          </Dropdown>
        </DiscussionItemHeaderWrapper>

        {isEditing ? (
          <DiscussionItemEditorWrapper>
            <PostUpdate
              post={post}
              canMentionEveryone={canMentionEveryone}
              displayable
              Attachments={displayableAttachments}
              refresh={refreshAfterUpdate}
              closeOrCancelEdit={() => setIsEditing(false)}
            />
          </DiscussionItemEditorWrapper>
        ) : (
          <>
            <ReadOnlyEditor content={post?.text ?? ''} />
            <AttachmentsCarousel
              isEditing={false}
              isUploading={false}
              attachments={displayableAttachments}
              onDeleteAttachment={async () => {}}
            />

            <PostLinksPreviewer postText={post?.text ?? ''} postHasImagesOrVideos={postHasImagesOrVideos} />

            {post?.updated && <TextStyled type="secondary">({t('edited').toLocaleLowerCase()})</TextStyled>}
          </>
        )}

        {!isEditing && (
          <Flex wrap align="center" justify={hasMoreReplies ? 'space-between' : 'flex-end'}>
            {hasMoreReplies && (
              <LoadRepliesButtonStyled type="link" onClick={fetchMoreReplies}>
                {t('loadMoreReplies')} ({unfetchedRepliesTotal})
              </LoadRepliesButtonStyled>
            )}

            <Flex gap="small">
              {post.reaction_count.length > 0 && (
                <Reactions
                  type="post"
                  reactedContentId={post.id}
                  reaction_count={post.reaction_count}
                  handleDeleteReaction={handleDeleteReaction}
                />
              )}

              {user && <EmojiPicker handleEmojiSelect={handleSubmitReaction} />}
            </Flex>
          </Flex>
        )}
      </FlexStyled>

      {contextHolder}
    </DiscussionItemWrapper>
  );
};
