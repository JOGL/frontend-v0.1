import styled from '@emotion/styled';
import { Button } from 'antd';

export const ButtonStyled = styled(Button)`
  padding: 0;
`;

export const LoadRepliesButtonStyled = styled(ButtonStyled)`
  align-self: start;
  height: auto;
`;
