import { useMutation } from '@tanstack/react-query';
import { message } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { FC, useState } from 'react';
import { ContentEntityModel, ContentEntityUpsertModel, DocumentType } from '~/__generated__/types';
import { DisplayableAttachment, isLocalAttachment } from '~/src/components/Feed/types';
import { PostEditor } from '~/src/components/tiptap/postEditor/postEditor';
import api from '~/src/utils/api/api';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';

interface Props {
  post: ContentEntityModel;
  canMentionEveryone: boolean;
  displayableAttachments: DisplayableAttachment[];
  closeOrCancelEdit: () => void;
  refresh: () => void;
}

export const PostUpdate: FC<Props> = ({
  post,
  canMentionEveryone,
  displayableAttachments: existingAttachments,
  closeOrCancelEdit,
  refresh,
}) => {
  const { t } = useTranslation('common');
  const [displayableAttachments, setDisplayableAttachments] = useState<DisplayableAttachment[]>(existingAttachments);

  const updatePost = useMutation({
    mutationKey: [MUTATION_KEYS.postUpdate, post.id],
    mutationFn: async (newContent: string) => {
      const updatedPost: ContentEntityUpsertModel = {
        type: post.type,
        visibility: post.visibility,
        status: post.status,
        text: newContent,
        documents_to_add: displayableAttachments.filter(isLocalAttachment).map((a) => {
          let attData = a.base64;
          if (attData instanceof ArrayBuffer) {
            // Convert ArrayBuffer to base64 string
            attData = btoa(String.fromCharCode.apply(null, new Uint8Array(attData)));
          }

          return {
            title: a.title,
            file_name: a.file_name,
            description: 'description',
            data: attData,
            type: DocumentType.Document,
          };
        }),
      };

      const response = await api.feed.contentEntitiesUpdateDetail(post.id, updatedPost);
      return response.data;
    },
    onSuccess: () => {
      closeOrCancelEdit();
      refresh();
      message.success(t('postUpdatedSuccessfully'));
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const handleChangeDoc = async (attachments: DisplayableAttachment[]) => {
    setDisplayableAttachments(attachments);
  };

  const deleteAttachment = useMutation({
    mutationKey: [MUTATION_KEYS.postAttachmentDelete, post.id],
    mutationFn: async (attachmentId: string) => {
      const response = await api.feed.contentEntitiesDocumentsDeleteDetail(post.id, attachmentId);
      return { data: response.data, attachmentId };
    },
    onSuccess: (result) => {
      const updatedAttachments = displayableAttachments.filter((attachment) => attachment.id !== result.attachmentId);
      handleChangeDoc(updatedAttachments);
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const handleDeleteDocument = async (attachmentId: string): Promise<void> => {
    const attachmentToDelete = displayableAttachments.find((fileToInspect) => fileToInspect.id === attachmentId);
    if (attachmentToDelete === undefined) {
      return;
    }

    if (attachmentToDelete.type === 'local') {
      const updatedAttachments = displayableAttachments.filter((attachment) => attachment.id !== attachmentId);
      handleChangeDoc(updatedAttachments);
    } else {
      deleteAttachment.mutate(attachmentId);
    }
  };
  return (
    <PostEditor
      content={post.text}
      feedId={post?.feed_entity?.id ?? ''}
      canMentionEveryone={canMentionEveryone}
      displayableAttachments={displayableAttachments}
      isEditing
      isUploading={updatePost.isPending}
      handleChangeDoc={handleChangeDoc}
      deleteDocument={handleDeleteDocument}
      handleSubmit={(content: string) => updatePost.mutate(content)}
      handleCancel={closeOrCancelEdit}
    />
  );
};
