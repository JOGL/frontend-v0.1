import { useState, FC } from 'react';
import { PostEditor } from '../../../tiptap/postEditor/postEditor';
import useUser from '~/src/hooks/useUser';
import { DisplayableAttachment, isLocalAttachment } from '~/src/components/Feed/types';
import { useMutation, useQuery } from '@tanstack/react-query';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import api from '~/src/utils/api/api';
import {
  ContentEntityStatus,
  ContentEntityType,
  ContentEntityUpsertModel,
  ContentEntityVisibility,
  DocumentType,
} from '~/__generated__/types';
import { message, Skeleton } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { DiscussionEditorWrapper } from '~/src/components/discussionDetails/discussionFeed.styles';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';

interface Props {
  feedId: string;
  canMentionEveryone: boolean;
  refresh?: () => void;
  ref: any;
}

export const PostCreate: FC<Props> = ({ ref, feedId, canMentionEveryone, refresh: refreshProp }) => {
  const [displayableAttachments, setDisplayableAttachments] = useState<DisplayableAttachment[]>([]);
  const { t } = useTranslation('common');
  const { user } = useUser();

  const { data: draft, isFetching: isFetchingDraft } = useQuery({
    queryKey: [QUERY_KEYS.feedDraftDetails, feedId],
    queryFn: async () => {
      const response = await api.feed.draftDetailDetail(feedId);
      return response.data;
    },
    refetchOnWindowFocus: false,
  });

  const handleChangeDoc = async (attachments: DisplayableAttachment[]) => {
    setDisplayableAttachments(attachments);
  };

  const deleteDocument = async (attachmentId: string): Promise<void> => {
    const updatedAttachments = displayableAttachments.filter((attachment) => attachment.id !== attachmentId);
    handleChangeDoc(updatedAttachments);
  };

  const refresh = () => {
    refreshProp && refreshProp();
    setDisplayableAttachments([]);
  };

  const createPost = useMutation({
    mutationKey: [MUTATION_KEYS.feedPostCreate, feedId],
    mutationFn: async (content: string, type: ContentEntityType = ContentEntityType.Announcement) => {
      const newPost: ContentEntityUpsertModel = {
        type,
        text: content,
        status: ContentEntityStatus.Active,
        visibility: ContentEntityVisibility.Public,
        user_ids: type === ContentEntityType.Jogldoc ? [user.id] : [],
        documents_to_add: displayableAttachments.filter(isLocalAttachment).map((a) => {
          let attData = a.base64;
          if (attData instanceof ArrayBuffer) {
            // Convert ArrayBuffer to base64 string
            attData = btoa(String.fromCharCode.apply(null, new Uint8Array(attData)));
          }

          return {
            title: a.title,
            file_name: a.file_name,
            description: 'description',
            data: attData,
            type: DocumentType.Document,
          };
        }),
      };

      const response = await api.feed.feedCreateDetail(feedId, newPost);
      return response.data;
    },
    onSuccess: (newPostId) => {
      refresh();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  if (isFetchingDraft) {
    return (
      <DiscussionEditorWrapper>
        <Skeleton title={false} />
      </DiscussionEditorWrapper>
    );
  }

  return (
    <DiscussionEditorWrapper ref={ref}>
      <PostEditor
        content={draft}
        feedId={feedId}
        canMentionEveryone={canMentionEveryone}
        saveDraft
        draftEntityId={feedId}
        displayableAttachments={displayableAttachments}
        isUploading={createPost.isPending}
        handleChangeDoc={handleChangeDoc}
        deleteDocument={deleteDocument}
        handleSubmit={(content: string) => createPost.mutate(content)}
      />
    </DiscussionEditorWrapper>
  );
};
