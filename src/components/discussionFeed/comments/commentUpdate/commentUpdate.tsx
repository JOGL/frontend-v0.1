import { useMutation } from '@tanstack/react-query';
import { message } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { FC, useState } from 'react';
import { CommentModel, CommentUpsertModel, DocumentType } from '~/__generated__/types';
import { DisplayableAttachment, isLocalAttachment } from '~/src/components/Feed/types';
import { PostEditor } from '~/src/components/tiptap/postEditor/postEditor';
import api from '~/src/utils/api/api';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';

interface Props {
  feedId: string;
  postId: string;
  canMentionEveryone: boolean;
  comment: CommentModel;
  displayableAttachments: DisplayableAttachment[];
  closeOrCancelEdit: () => void;
  refresh: () => void;
}

export const CommentUpdate: FC<Props> = ({
  feedId,
  postId,
  canMentionEveryone,
  comment,
  displayableAttachments: existingAttachments,
  closeOrCancelEdit,
  refresh,
}) => {
  const { t } = useTranslation('common');
  const [displayableAttachments, setDisplayableAttachments] = useState<DisplayableAttachment[]>(existingAttachments);

  const updateComment = useMutation({
    mutationKey: [MUTATION_KEYS.commentUpdate, comment.id],
    mutationFn: async (newContent: string) => {
      const updatedComment: CommentUpsertModel = {
        text: newContent,
        documents_to_add: displayableAttachments.filter(isLocalAttachment).map((a) => {
          let attData = a.base64;
          if (attData instanceof ArrayBuffer) {
            // Convert ArrayBuffer to base64 string
            attData = btoa(String.fromCharCode.apply(null, new Uint8Array(attData)));
          }

          return {
            title: a.title,
            file_name: a.file_name,
            description: 'description',
            data: attData,
            type: DocumentType.Document,
          };
        }),
      };

      const response = await api.feed.contentEntitiesCommentsUpdateDetail(postId, comment.id, updatedComment);
      return response.data;
    },
    onSuccess: () => {
      closeOrCancelEdit();
      refresh();
      message.success(t('replyUpdatedSuccessfully'));
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const handleChangeDoc = async (attachments: DisplayableAttachment[]) => {
    setDisplayableAttachments(attachments);
  };

  const deleteAttachment = useMutation({
    mutationKey: [MUTATION_KEYS.commentAttachmentDelete, comment.id],
    mutationFn: async (attachmentId: string) => {
      const response = await api.feed.contentEntitiesCommentsDocumentsDeleteDetail(postId, comment.id, attachmentId);
      return { data: response.data, attachmentId };
    },
    onSuccess: (result) => {
      const updatedAttachments = displayableAttachments.filter((attachment) => attachment.id !== result.attachmentId);
      handleChangeDoc(updatedAttachments);
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const handleDeleteDocument = async (attachmentId: string): Promise<void> => {
    const attachmentToDelete = displayableAttachments.find((fileToInspect) => fileToInspect.id === attachmentId);
    if (attachmentToDelete === undefined) {
      return;
    }

    if (attachmentToDelete.type === 'local') {
      const updatedAttachments = displayableAttachments.filter((attachment) => attachment.id !== attachmentId);
      handleChangeDoc(updatedAttachments);
    } else {
      deleteAttachment.mutate(attachmentId);
    }
  };
  return (
    <PostEditor
      content={comment.text}
      feedId={feedId}
      canMentionEveryone={canMentionEveryone}
      displayableAttachments={displayableAttachments}
      isEditing
      isUploading={updateComment.isPending}
      handleChangeDoc={handleChangeDoc}
      deleteDocument={handleDeleteDocument}
      handleSubmit={(content: string) => updateComment.mutate(content)}
      handleCancel={closeOrCancelEdit}
    />
  );
};
