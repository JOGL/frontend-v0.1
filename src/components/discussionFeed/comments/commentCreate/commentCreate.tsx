import { useState, FC } from 'react';
import { PostEditor } from '../../../tiptap/postEditor/postEditor';
import useUser from '~/src/hooks/useUser';
import { DisplayableAttachment, isLocalAttachment } from '~/src/components/Feed/types';
import { useMutation, useQuery } from '@tanstack/react-query';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import api from '~/src/utils/api/api';
import { CommentUpsertModel, DocumentType } from '~/__generated__/types';
import { message, Skeleton } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { DiscussionEditorWrapper } from '~/src/components/discussionDetails/discussionFeed.styles';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';

interface Props {
  feedId: string;
  postId: string;
  canMentionEveryone: boolean;
  refresh?: () => void;
  ref: any;
}

export const CommentCreate: FC<Props> = ({ ref, feedId, postId, canMentionEveryone, refresh: refreshProp }) => {
  const [displayableAttachments, setDisplayableAttachments] = useState<DisplayableAttachment[]>([]);
  const { t } = useTranslation('common');

  const { data: draft, isFetching: isFetchingDraft } = useQuery({
    queryKey: [QUERY_KEYS.feedDraftDetails, postId],
    queryFn: async () => {
      const response = await api.feed.draftDetailDetail(postId);
      return response.data;
    },
    refetchOnWindowFocus: false,
  });

  const handleChangeDoc = async (attachments: DisplayableAttachment[]) => {
    try {
      setDisplayableAttachments(attachments);
      refreshProp && refreshProp();
    } catch (err) {
      console.error(err);
    }
  };

  const deleteDocument = async (attachmentId: string): Promise<void> => {
    const updatedAttachments = displayableAttachments.filter((attachment) => attachment.id !== attachmentId);
    handleChangeDoc(updatedAttachments);
  };

  const refresh = () => {
    refreshProp && refreshProp();
    setDisplayableAttachments([]);
  };

  const createComment = useMutation({
    mutationKey: [MUTATION_KEYS.postCommentCreate, postId],
    mutationFn: async (content: string) => {
      const newComment: CommentUpsertModel = {
        text: content,
        documents_to_add: displayableAttachments.filter(isLocalAttachment).map((a) => {
          let attData = a.base64;
          if (attData instanceof ArrayBuffer) {
            // Convert ArrayBuffer to base64 string
            attData = btoa(String.fromCharCode.apply(null, new Uint8Array(attData)));
          }

          return {
            title: a.title,
            file_name: a.file_name,
            description: 'description',
            data: attData,
            type: DocumentType.Document,
          };
        }),
      };

      const response = await api.feed.contentEntitiesCommentsCreate(postId, newComment);
      return response.data;
    },
    onSuccess: (newCommentId) => {
      refresh();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  if (isFetchingDraft) {
    return <Skeleton title={false} />;
  }

  return (
    <DiscussionEditorWrapper ref={ref}>
      <PostEditor
        content={draft}
        feedId={feedId}
        canMentionEveryone={canMentionEveryone}
        saveDraft
        draftEntityId={postId}
        displayableAttachments={displayableAttachments}
        isUploading={createComment.isPending}
        handleChangeDoc={handleChangeDoc}
        deleteDocument={deleteDocument}
        handleSubmit={(content: string) => createComment.mutate(content)}
      />
    </DiscussionEditorWrapper>
  );
};
