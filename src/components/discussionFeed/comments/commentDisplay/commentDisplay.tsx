import { MoreOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Dropdown, Flex, MenuProps, message, Modal, Space, Typography } from 'antd';
import { FC, useMemo, useState } from 'react';
import { CommentModel, ReactionUpsertModel } from '~/__generated__/types';
import {
  AuthorButtonStyled,
  AvatarStyled,
  DiscussionItemEditorWrapper,
  DiscussionItemHeaderWrapper,
  DiscussionItemWrapper,
  FlexStyled,
  TextStyled,
} from '~/src/components/discussionDetails/discussionFeed.styles';
import { copyPostLink, displayObjectRelativeDate, isImageFileType, isVideoFileType } from '~/src/utils/utils';
import { useRouter } from 'next/router';
import ReadOnlyEditor from '~/src/components/tiptap/readOnlyEditor/readOnlyEditor';
import { AttachmentsCarousel } from '~/src/components/Feed/PostsOld/AttachmentsCarousel';
import PostLinksPreviewer from '~/src/components/Feed/PostsOld/PostLinksPreviewer';
import { mapDocumentModelToRemoteAttachment } from '~/src/components/Feed/types';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import api from '~/src/utils/api/api';
import useUser from '~/src/hooks/useUser';
import useTranslation from 'next-translate/useTranslation';
import { CommentUpdate } from '../commentUpdate/commentUpdate';
import { EmojiPicker } from '~/src/components/emojiPicker/emojiPicker';
import { Reactions } from '../../reactions/reactions';
import { useCommentReactions } from '../../chat/chat.hooks';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';

const { Text } = Typography;

interface Props {
  feedId: string;
  postId: string;
  comment: CommentModel;
  canDeleteComments: boolean;
  refreshAfterUpdate: () => void;
  handleDelete: () => void;
}

interface DeleteCommentProps {
  postId: string;
  commentId: string;
}

export const CommentDisplay: FC<Props> = ({
  feedId,
  postId,
  comment,
  canDeleteComments,
  refreshAfterUpdate,
  handleDelete,
}) => {
  const [isEditing, setIsEditing] = useState<boolean>(false);
  const { push, locale } = useRouter();
  const { user } = useUser();
  const { t } = useTranslation('common');
  const [modal, contextHolder] = Modal.useModal();
  const queryClient = useQueryClient();

  const refreshAfterReactionChange = () => {
    queryClient.invalidateQueries({
      queryKey: [QUERY_KEYS.commentReactionDetails, comment.id],
    });

    refreshAfterUpdate();
  };
  const { reactToComment, deleteCommentReaction } = useCommentReactions({
    successCallback: refreshAfterReactionChange,
  });

  const handleSubmitReaction = (emoji: ReactionUpsertModel) => {
    reactToComment.mutate({ contentId: comment.id, reaction: emoji });
  };

  const handleDeleteReaction = (reactionId: string) => {
    deleteCommentReaction.mutate({ contentId: comment.id, reactionId });
  };

  const reportComment = useMutation({
    mutationKey: [MUTATION_KEYS.commentReport],
    mutationFn: async (commentId: string) => {
      await api.feed.reportCommentCreateDetail(commentId);
    },
    onSuccess: () => {
      message.success(t('replyReportedSuccessfully'));
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const deleteComment = useMutation({
    mutationKey: [MUTATION_KEYS.commentDelete],
    mutationFn: async ({ postId, commentId }: DeleteCommentProps) => {
      await api.feed.contentEntitiesCommentsDeleteDetail(postId, commentId);
    },
    onSuccess: () => {
      message.success(t('action.deleteReply'));
      handleDelete();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const displayableAttachments = useMemo(() => (comment?.documents ?? []).map(mapDocumentModelToRemoteAttachment), [
    comment?.documents,
  ]);

  const commentHasImagesOrVideos = useMemo(
    () => !!displayableAttachments.find((a) => isImageFileType(a.file_type) || isVideoFileType(a.file_type)),
    [displayableAttachments]
  );

  const showDeleteWarning = () => {
    modal.confirm({
      title: t('areYouSure'),
      content: t('thisActionIsIrreversible'),
      onOk: () => deleteComment.mutate({ postId: postId, commentId: comment.id }),
      okText: t('action.delete'),
      cancelText: t('action.cancel'),
      okButtonProps: { loading: deleteComment.isPending },
    });
  };

  const isAuthor = useMemo(() => {
    return user?.id === comment.created_by_user_id;
  }, [user, comment]);

  const dropdownItems: MenuProps['items'] = useMemo(
    () => [
      ...(!isEditing && isAuthor ? [{ label: t('action.edit'), key: 'edit', onClick: () => setIsEditing(true) }] : []),
      {
        label: t('action.copyReplyLink'),
        key: 'copyReplyLink',
        onClick: () => copyPostLink(postId, 'comment', comment.id, t),
      },
      ...(!isAuthor && user
        ? [
            {
              label: t('action.report'),
              key: 'report',
              danger: true,
              onClick: () => reportComment.mutate(comment.id),
            },
          ]
        : []),
      ...(canDeleteComments || isAuthor
        ? [
            {
              label: t('action.delete'),
              key: 'delete',
              danger: true,
              onClick: () => showDeleteWarning(),
            },
          ]
        : []),
    ],

    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isAuthor, isEditing, canDeleteComments]
  );

  return (
    <DiscussionItemWrapper gap="small" id={comment.id} key={comment.id} isNew={comment.is_new}>
      <AvatarStyled
        size={36}
        shape="circle"
        src={comment?.overrides?.user_image_url ?? comment?.created_by?.logo_url}
        icon={<UserOutlined />}
      />

      <FlexStyled flex={1} vertical>
        <DiscussionItemHeaderWrapper wrap gap="small" align="center" justify="space-between">
          <Space>
            <AuthorButtonStyled
              type="link"
              href={comment?.overrides?.user_url}
              onClick={() => push(`/user/${comment.created_by_user_id}`)}
            >
              <Text strong>
                {comment?.overrides?.user_name ??
                  comment?.created_by?.first_name + ' ' + comment?.created_by?.last_name}
              </Text>
            </AuthorButtonStyled>

            <TextStyled type="secondary">
              {displayObjectRelativeDate(comment.created, locale ?? 'en', false, false)}
            </TextStyled>
          </Space>

          <Dropdown menu={{ items: dropdownItems }} trigger={['click']}>
            <Button type="text" size="small" icon={<MoreOutlined />} />
          </Dropdown>
        </DiscussionItemHeaderWrapper>

        {isEditing ? (
          <DiscussionItemEditorWrapper>
            <CommentUpdate
              feedId={feedId}
              postId={postId}
              comment={comment}
              displayableAttachments={displayableAttachments}
              refresh={refreshAfterUpdate}
              closeOrCancelEdit={() => setIsEditing(false)}
            />
          </DiscussionItemEditorWrapper>
        ) : (
          <>
            <ReadOnlyEditor content={comment?.text ?? ''} />
            <AttachmentsCarousel
              isEditing={false}
              isUploading={false}
              attachments={displayableAttachments}
              onDeleteAttachment={async () => {}}
            />

            <PostLinksPreviewer postText={comment?.text ?? ''} postHasImagesOrVideos={commentHasImagesOrVideos} />

            {comment?.updated && <TextStyled type="secondary">({t('edited').toLocaleLowerCase()})</TextStyled>}
          </>
        )}

        <Flex wrap gap="small" align="center" justify="end">
          {comment.reaction_count.length > 0 && (
            <Reactions
              type="comment"
              reactedContentId={comment.id}
              reaction_count={comment.reaction_count}
              handleDeleteReaction={handleDeleteReaction}
            />
          )}

          {user && <EmojiPicker handleEmojiSelect={handleSubmitReaction} />}
        </Flex>
      </FlexStyled>

      {contextHolder}
    </DiscussionItemWrapper>
  );
};
