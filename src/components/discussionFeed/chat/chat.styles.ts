import styled from '@emotion/styled';
import { Flex } from 'antd';

export const ChatWrapperStyled = styled(Flex)`
  height: 100%;
`;