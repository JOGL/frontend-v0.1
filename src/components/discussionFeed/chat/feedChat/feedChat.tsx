import { useInfiniteQuery, useQueryClient } from '@tanstack/react-query';
import { FC, useEffect, useMemo } from 'react';
import { ContentEntityModel, ContentEntityModelListPage } from '~/__generated__/types';
import { FeedChatListStyled, FeedChatListWrapperStyled, InfiniteScrollStyled } from './feedChat.styles';
import { Skeleton } from 'antd';
import { PostCreate } from '../../posts/postCreate/postCreate';
import { ChatWrapperStyled } from '../chat.styles';
import { useScrollHelper } from '../chat.hooks';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { PostDisplay } from '../../posts/postDisplay/postDisplay';
import { useWebSocket } from '~/src/contexts/websocket/websocketProvider';
import { ScrollbarFullWidthStyled } from '~/src/assets/styles/Global.styled';
import { SearchModel } from '~/src/types/searchModel';
import useFeed from '~/src/components/discussions/useFeed';

interface Props {
  //tabName is needed because for the scroll up to fetch the next page, each scrollable div ID needs to be unique
  tabName?: string;
  queryKey: string;
  feedId: string;
  canMentionEveryone: boolean;
  canPost: boolean;
  canComment: boolean;
  canDeletePosts: boolean;
  loadData: (feedId: string, search: SearchModel) => Promise<ContentEntityModelListPage>;
  handleSelectPost: (post?: ContentEntityModel) => void;
  onRefetch?: () => void;
}

export const FeedChat: FC<Props> = ({
  tabName = 'posts',
  queryKey,
  feedId,
  canMentionEveryone,
  canPost,
  canComment,
  canDeletePosts,
  loadData,
  handleSelectPost,
  onRefetch,
}) => {
  const { scrollRef, scrollToBottom } = useScrollHelper();
  const { subscribe } = useWebSocket();
  const queryClient = useQueryClient();
  useFeed(feedId);

  const refreshContent = () => {
    queryClient.invalidateQueries({
      queryKey: [QUERY_KEYS.feedAllPostsList, feedId],
    });
    queryClient.invalidateQueries({
      queryKey: [QUERY_KEYS.feedThreadsList, feedId],
    });
    queryClient.invalidateQueries({
      queryKey: [QUERY_KEYS.feedMentionsList, feedId],
    });

    onRefetch && onRefetch();
    scrollToBottom();
  };

  useEffect(() => {
    if (feedId) {
      const unsubscribe = subscribe(feedId, ['PostInFeed'], refreshContent);

      //Unsubscribe on unmount
      return () => {
        unsubscribe();
      };
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [subscribe, feedId]);

  const PAGE_SIZE = 20;

  const { data, isLoading, hasNextPage, fetchNextPage } = useInfiniteQuery({
    queryKey: [queryKey, feedId],
    queryFn: async ({ pageParam = 1 }) => {
      const response = await loadData(feedId, { page: pageParam, pageSize: PAGE_SIZE });
      return response;
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItemsCount = allPages.reduce((acc, page) => acc + page.items.length, 0);
      return loadedItemsCount < lastPage.total ? allPages.length + 1 : undefined;
    },
    enabled: !!feedId,
  });

  // Flatten the pages of data into a single array for the Ant List
  const flattenedData: ContentEntityModel[] = useMemo(() => {
    return data?.pages.flatMap((page) => page.items) ?? [];
  }, [data]);

  return (
    <ScrollbarFullWidthStyled>
      <ChatWrapperStyled vertical gap="small">
        <FeedChatListWrapperStyled id={`${feedId}-${tabName}-scrollableDiv`}>
          <InfiniteScrollStyled
            dataLength={flattenedData.length}
            next={fetchNextPage}
            inverse={true}
            hasMore={hasNextPage}
            loader={<Skeleton avatar paragraph={{ rows: 1 }} active={isLoading} />}
            scrollableTarget={`${feedId}-${tabName}-scrollableDiv`}
          >
            <FeedChatListStyled
              dataSource={flattenedData}
              loading={isLoading}
              renderItem={(item) => (
                <PostDisplay
                  post={item}
                  canMentionEveryone={canMentionEveryone}
                  canComment={canComment}
                  canDeletePosts={canDeletePosts}
                  handleSelectPost={handleSelectPost}
                  refreshChatContent={refreshContent}
                />
              )}
            />
          </InfiniteScrollStyled>
        </FeedChatListWrapperStyled>

        {canPost && (
          <PostCreate
            ref={scrollRef}
            feedId={feedId}
            canMentionEveryone={canMentionEveryone}
            refresh={refreshContent}
          />
        )}
      </ChatWrapperStyled>
    </ScrollbarFullWidthStyled>
  );
};
