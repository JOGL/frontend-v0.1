import styled from '@emotion/styled';
import { List } from 'antd';
import InfiniteScroll from 'react-infinite-scroll-component';
import { ContentEntityModel } from '~/__generated__/types';

export const FeedChatListWrapperStyled = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column-reverse;
  overflow-y: auto;
`;

export const FeedChatListStyled = styled(List<ContentEntityModel>)`
  .ant-list-items {
    display: flex;
    flex-direction: column-reverse;
  }
`;

export const InfiniteScrollStyled = styled(InfiniteScroll)`
  display: flex; 
  flex-direction: column-reverse;
`;
