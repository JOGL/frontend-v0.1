import styled from '@emotion/styled';
import { Button, Flex, List } from 'antd';
import { CommentModel } from '~/__generated__/types';

export const ButtonStyled = styled(Button)`
  align-self: start;
`

export const ThreadChatWrapper = styled(Flex)`
  overflow: auto;
  width: 100%;
  display: flex;
  flex-direction: column-reverse;
`;

export const ThreadChatListStyled = styled(List<CommentModel>)`
  flex: 1;

  .ant-list-items {
    display: flex;
    flex-direction: column;
  }

  .ant-list-empty-text {
    display: none;
  }
`;