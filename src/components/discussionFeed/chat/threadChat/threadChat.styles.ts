import styled from '@emotion/styled';
import { Button, Flex, List } from 'antd';
import { CommentModel } from '~/__generated__/types';

export const ButtonStyled = styled(Button)`
  align-self: start;
`

export const ThreadChatWrapper = styled(Flex)`
  overflow: auto;
  width:100%;
`;

export const ThreadChatListStyled = styled(List<CommentModel>)`
  flex: 1;

  .ant-list-empty-text {
    display: none;
  }
`;