import { useInfiniteQuery, useQuery } from '@tanstack/react-query';
import { FC, useEffect, useMemo } from 'react';
import { CommentModel } from '~/__generated__/types';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { ArrowLeftOutlined } from '@ant-design/icons';
import { ChatWrapperStyled } from '../chat.styles';
import { ThreadChatListStyled, ThreadChatWrapper } from './threadChat.styles';
import useTranslation from 'next-translate/useTranslation';
import { useScrollHelper } from '../chat.hooks';
import { CommentCreate } from '../../comments/commentCreate/commentCreate';
import { ThreadPostDisplay } from '../../posts/threadPostDisplay/threadPostDisplay';
import { CommentDisplay } from '../../comments/commentDisplay/commentDisplay';
import { Alert, Flex, Spin, Typography } from 'antd';
import { useWebSocket } from '~/src/contexts/websocket/websocketProvider';
import { ScrollbarFullWidthStyled } from '~/src/assets/styles/Global.styled';
import { LinkStyled } from '../../feed/navigation/navigation.styles';
import useThread from '~/src/components/discussions/useThread';

interface Props {
  postId: string;
  feedId: string;
  canMentionEveryone: boolean;
  canComment: boolean;
  canDeletePosts: boolean;
  canDeleteComments: boolean;
  goBack: () => void;
  showNavigation?: boolean;
  onRefetch?: () => void;
}

const PAGE_SIZE = 20;

export const ThreadChat: FC<Props> = ({
  postId,
  feedId,
  canMentionEveryone,
  canComment,
  canDeletePosts,
  canDeleteComments,
  goBack,
  showNavigation,
  onRefetch,
}) => {
  const { t } = useTranslation('common');
  const { subscribe } = useWebSocket();
  useThread(postId, feedId);

  //Although the parent has the selected post, the post can be updated in the thread, so we fetch the information here to keep it up to date.
  //Once the user goes back to the channel discussion, that query will fetch up to date data.
  //Hence, currently, there is no need to have components higher up in the tree fetching this data and then rely on the post prop to keep data up to date.
  const { data: post, isLoading: isLoadingPost, refetch: refetchPost } = useQuery({
    queryKey: [QUERY_KEYS.postDetails, postId],
    queryFn: async () => {
      const response = await api.feed.contentEntitiesDetailDetail(postId);
      return response.data;
    },
    enabled: !!postId,
  });

  const { scrollRef, scrollToBottom } = useScrollHelper();

  const {
    data: comments,
    isLoading: isLoadingComments,
    isFetchingNextPage: isLoadingMoreComments,
    hasNextPage,
    fetchNextPage,
    refetch: refetchComments,
  } = useInfiniteQuery({
    queryKey: [QUERY_KEYS.postCommentsList, postId],
    queryFn: async ({ pageParam = 1 }) => {
      const response = await api.feed.contentEntitiesCommentsListDetail(postId, {
        Page: pageParam,
        PageSize: PAGE_SIZE,
      });
      return response.data;
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItemsCount = allPages.reduce((acc, page) => acc + page.items.length, 0);
      return loadedItemsCount < lastPage.total ? allPages.length + 1 : undefined;
    },
    enabled: !!post && !isLoadingPost,
  });

  const refreshPostAndComments = async () => {
    await refetchPost();
    await refetchComments();

    onRefetch && onRefetch();
    scrollToBottom();
  };

  //Flatten the pages of data into a single array for the Ant List
  const flattenedData: CommentModel[] = useMemo(() => {
    const commentsArray = comments?.pages.flatMap((page) => page.items) ?? [];
    //Reverse the order of the comments so we have the newest at the end of the list.
    return [...commentsArray.reverse()];
  }, [comments]);

  // Effect to scroll to bottom on initial load and thread changes
  useEffect(() => {
    if (!isLoadingPost && !isLoadingComments && post && comments) {
      // Use setTimeout to ensure content is rendered
      const timer = setTimeout(() => {
        scrollToBottom();
      }, 100);
      return () => clearTimeout(timer);
    }
  }, [isLoadingPost, isLoadingComments, post, comments, scrollToBottom]);

  // Effect to scroll to bottom when new comments are added
  useEffect(() => {
    if (postId) {
      const unsubscribe = subscribe(postId, ['CommentInPost'], () => {
        refetchComments();
        scrollToBottom();

        // Use setTimeout to ensure new comment is rendered
        setTimeout(() => {
          scrollToBottom();
        }, 100);
      });

      return () => {
        unsubscribe();
      };
    }
  }, [subscribe, postId]);

  if (isLoadingPost || isLoadingComments) {
    return (
      <Flex justify="center">
        <Spin />
      </Flex>
    );
  }

  if (!post) {
    return <Alert message={t('error.somethingWentWrong')} />;
  }

  return (
    <>
      <ScrollbarFullWidthStyled>
        <ChatWrapperStyled vertical gap="small">
          {!showNavigation && (
            <LinkStyled onClick={goBack} align="center" gap="small" strong>
              <ArrowLeftOutlined />
              <Typography.Text>{t('backToDiscussion')}</Typography.Text>
            </LinkStyled>
          )}

          <ThreadChatWrapper flex={1} vertical>
            <ThreadChatListStyled
              dataSource={flattenedData}
              loading={isLoadingMoreComments}
              renderItem={(item) => (
                <CommentDisplay
                  feedId={post.feed_entity?.id ?? ''}
                  postId={post.id}
                  comment={item}
                  canDeleteComments={canDeleteComments}
                  refreshAfterUpdate={refetchComments}
                  handleDelete={refreshPostAndComments}
                />
              )}
            />
            <ThreadPostDisplay
              post={post}
              canMentionEveryone={canMentionEveryone}
              canDeletePosts={canDeletePosts}
              hasMoreReplies={hasNextPage}
              unfetchedRepliesTotal={post.comment_count - flattenedData.length}
              fetchMoreReplies={fetchNextPage}
              refreshAfterUpdate={refetchPost}
              handleDelete={goBack}
            />
          </ThreadChatWrapper>
          {canComment && (
            <CommentCreate
              ref={scrollRef}
              feedId={post.feed_entity?.id ?? post.id}
              postId={post.id}
              canMentionEveryone={canMentionEveryone}
              refresh={refreshPostAndComments}
            />
          )}
        </ChatWrapperStyled>
      </ScrollbarFullWidthStyled>
    </>
  );
};
