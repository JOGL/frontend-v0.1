import { useMutation } from '@tanstack/react-query';
import { message } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useCallback, useState } from 'react';
import { ReactionUpsertModel } from '~/__generated__/types';
import api from '~/src/utils/api/api';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';

export const useScrollHelper = () => {
  const [el, setEl] = useState<any>(true);

  const scrollRef = useCallback((element) => {
    setEl(element);
    scrollElementToBottom(element);
  }, []);

  const scrollElementToBottom = (element: any) => {
    if (!element) return;
    setTimeout(() => element.scroll && element.scroll({ behavior: 'smooth' }), 100);
    element.scrollIntoView && element.scrollIntoView();
  };

  const scrollToBottom = () => {
    scrollElementToBottom(el);
  };

  return { scrollRef, scrollToBottom };
};

export const useReportPost = () => {
  const { t } = useTranslation('common');

  const reportPost = useMutation({
    mutationKey: [MUTATION_KEYS.postReport],
    mutationFn: async (postId: string) => {
      await api.feed.reportContentEntityCreateDetail(postId);
    },
    onSuccess: () => {
      message.success(t('postReportedSuccessfully'));
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  return reportPost;
};

interface HookBaseProps {
  successCallback: () => void;
}

export const useDeletePost = ({ successCallback }: HookBaseProps) => {
  const { t } = useTranslation('common');

  const deletePost = useMutation({
    mutationKey: [MUTATION_KEYS.postDelete],
    mutationFn: async (postId: string) => {
      await api.feed.contentEntitiesDeleteDetail(postId);
    },
    onSuccess: () => {
      successCallback();
      message.success(t('action.deletePost'));
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  return deletePost;
};

interface ReactToContentParams {
  contentId: string;
  reaction: ReactionUpsertModel;
}

interface DeleteContentReactionParams {
  contentId: string;
  reactionId: string;
}

export const usePostReactions = ({ successCallback }: HookBaseProps) => {
  const reactToPost = useMutation({
    mutationKey: [MUTATION_KEYS.postReactionCreate],
    mutationFn: async ({ contentId, reaction }: ReactToContentParams) => {
      await api.feed.contentEntitiesReactionsCreate(contentId, reaction);
    },
    onSuccess: () => {
      successCallback();
    },
  });

  const deletePostReaction = useMutation({
    mutationKey: [MUTATION_KEYS.postReactionDelete],
    mutationFn: async ({ contentId, reactionId }: DeleteContentReactionParams) => {
      await api.feed.contentEntitiesReactionsDeleteDetail(contentId, reactionId);
    },
    onSuccess: () => {
      successCallback();
    },
  });

  return { reactToPost, deletePostReaction };
};

export const useCommentReactions = ({ successCallback }: HookBaseProps) => {
  const reactToComment = useMutation({
    mutationKey: [MUTATION_KEYS.commentReactionCreate],
    mutationFn: async ({ contentId, reaction }: ReactToContentParams) => {
      await api.feed.commentsReactionsCreate(contentId, reaction);
    },
    onSuccess: () => {
      successCallback();
    },
  });

  const deleteCommentReaction = useMutation({
    mutationKey: [MUTATION_KEYS.commentReactionDelete],
    mutationFn: async ({ contentId, reactionId }: DeleteContentReactionParams) => {
      await api.feed.commentsReactionsDeleteDetail(contentId, reactionId);
    },
    onSuccess: () => {
      successCallback();
    },
  });

  return { reactToComment, deleteCommentReaction };
};
