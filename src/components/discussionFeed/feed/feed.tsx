import { FC, useEffect, useState } from 'react';
import { ContentEntityModel } from '~/__generated__/types';
import { ThreadChat } from '~/src/components/discussionFeed/chat/threadChat/threadChat';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { FeedChat } from '~/src/components/discussionFeed/chat/feedChat/feedChat';
import { DiscussionTabStyled } from './feed.styles';
import { ScrollbarStyled } from '~/src/assets/styles/Global.styled';
import { useFeedPermissions } from '~/src/components/discussionDetails/discussionFeed.hooks';
import { SearchModel } from '~/src/types/searchModel';
import { useQuery } from '@tanstack/react-query';
import { useRouter } from 'next/router';
import { entityItem } from '~/src/utils/utils';
import { Navigation } from './navigation/navigation';

const Feed: FC<{ feedId: string; onRefetch?: () => void; showNavigation?: boolean; fullHeight?: boolean }> = ({
  feedId,
  showNavigation,
  onRefetch,
  fullHeight,
}) => {
  const [selectedPost, setSelectedPost] = useState<ContentEntityModel | undefined>(undefined);
  const { canPost, canDeletePosts, canComment, canDeleteComments, canMentionEveryone } = useFeedPermissions(feedId);
  const router = useRouter();

  const { data } = useQuery({
    queryKey: [QUERY_KEYS.postDetails, router.query.id, router.query.postId],
    queryFn: async () => {
      if (!router.query.postId) return undefined;
      const response = await api.feed.contentEntitiesDetailDetail(router.query.postId as string);
      setSelectedPost(response.data);
      return response.data;
    },
    enabled: !!router.query.postId,
  });

  useEffect(() => {
    if (!router.query.postId) {
      setSelectedPost(undefined);
    }
  }, [router.query.postId]);

  useEffect(() => {
    if (selectedPost)
      router.replace(
        {
          query: { ...router.query, postId: selectedPost.id },
          hash: window.location.hash
        },
        undefined,
        { shallow: true, scroll: false }
      );
  }, [selectedPost]);

  const goToEntity = () => {
    onRefetch && onRefetch();
    data?.feed_entity?.entity_type &&
      router.replace(
        `/${entityItem[data.feed_entity.entity_type].front_path}/${data.feed_entity.id}?tab=discussion`,
        undefined,
        { shallow: true, scroll: false }
      );
  };

  const goToDiscussion = () => {
    onRefetch && onRefetch();
    data?.feed_entity?.entity_type &&
      router.replace(`/discussion/${data.feed_entity.id}`, undefined, { shallow: true, scroll: false });
  };
  return (
    <DiscussionTabStyled fullHeight={fullHeight}>
      {showNavigation && (
        <Navigation
          feedId={feedId}
          selectedPost={selectedPost}
          goToDiscussion={goToDiscussion}
          goToEntity={goToEntity}
        />
      )}
      <ScrollbarStyled>
        {selectedPost ? (
          <ThreadChat
            showNavigation={showNavigation}
            postId={selectedPost.id}
            feedId={feedId}
            canMentionEveryone={canMentionEveryone}
            canComment={canComment}
            canDeletePosts={canDeletePosts}
            canDeleteComments={canDeleteComments}
            onRefetch={onRefetch}
            goBack={goToEntity}
          />
        ) : (
          <FeedChat
            feedId={feedId}
            canPost={canPost}
            canMentionEveryone={canMentionEveryone}
            canComment={canComment}
            canDeletePosts={canDeletePosts}
            handleSelectPost={setSelectedPost}
            onRefetch={onRefetch}
            queryKey={QUERY_KEYS.feedAllPostsList}
            loadData={async (feedId: string, search: SearchModel) => {
              const response = await api.feed.postsListDetail(feedId, { Page: search.page, PageSize: search.pageSize });
              return response.data;
            }}
          />
        )}
      </ScrollbarStyled>
    </DiscussionTabStyled>
  );
};

export default Feed;
