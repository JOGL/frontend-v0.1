import styled from '@emotion/styled';
import { Flex } from 'antd';

export const LinkStyled = styled(Flex)<{ strong?: boolean }>`
  cursor: pointer;
  padding-bottom: ${({ theme }) => theme.token.paddingMD}px;
  white-space: nowrap;
  max-width: 55%;
  span,
  svg,
  strong {
    color: ${(props) => (props.strong ? props.theme.token.neutral9 : props.theme.token.neutral7)};
  }

  &:hover {
    strong,
    span,
    svg {
      color: ${({ theme }) => theme.token.neutral6};
    }
  }
`;

export const DisabledLinkStyle = styled(Flex)`
  padding-bottom: ${({ theme }) => theme.token.paddingMD}px;
  span,
  svg {
    color: ${({ theme }) => theme.token.neutral7};
  }
`;
