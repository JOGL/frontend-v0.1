import { ArrowLeftOutlined } from '@ant-design/icons';
import { useQuery } from '@tanstack/react-query';
import { Flex, Typography } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { FC } from 'react';
import { ContentEntityModel } from '~/__generated__/types';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { getEntityIcon } from '~/src/utils/utils';
import { DisabledLinkStyle, LinkStyled } from './navigation.styles';

export const Navigation: FC<{
  feedId: string;
  selectedPost?: ContentEntityModel;
  goToEntity?: () => void;
  goToDiscussion?: () => void;
}> = ({ feedId, selectedPost, goToDiscussion, goToEntity }) => {
  const { t } = useTranslation('common');

  const { data } = useQuery({
    queryKey: [QUERY_KEYS.postDetails, feedId],
    queryFn: async () => {
      const response = await api.feed.feedDetailDetail(feedId);
      return response.data;
    },
  });

  const getTitle = () => {
    if (data?.parent_feed_entity) {
      return `${data.feed_entity?.title}, ${data.parent_feed_entity?.title}`;
    }
    return data?.feed_entity?.title;
  };

  const feedData = data?.items[0];
  if (selectedPost) {
    return (
      <Flex gap="small">
        <LinkStyled onClick={goToDiscussion} align="center" gap="small" strong>
          <ArrowLeftOutlined />
          <Typography.Text onClick={goToDiscussion}>{t('threadIn')}</Typography.Text>
        </LinkStyled>
        <LinkStyled onClick={goToEntity} gap="small">
          {selectedPost?.feed_entity && getEntityIcon(selectedPost.feed_entity?.entity_type)}
          <Typography.Text ellipsis={{ tooltip: getTitle() }}>{getTitle()}</Typography.Text>
        </LinkStyled>
      </Flex>
    );
  }
  return (
    <DisabledLinkStyle gap="small">
      {feedData?.feed_entity && getEntityIcon(feedData.feed_entity?.entity_type)}
      <Typography.Text type="secondary"> {getTitle()}</Typography.Text>
    </DisabledLinkStyle>
  );
};
