import styled from '@emotion/styled';
import { Tabs } from 'antd';

export const DiscussionTabStyled = styled.div<{ fullHeight?: boolean }>`
  height: ${({ fullHeight }) => (fullHeight ? '100%' : 'calc(100vh - 330px)')};
  padding-bottom: ${({ fullHeight, theme }) => (fullHeight ? theme.token.paddingLG : 0)}px;
`;

export const TabsStyled = styled(Tabs)`
  overflow: hidden auto;

  border-top: 1px solid ${({ theme }) => theme.token.colorBorderSecondary};
  margin-top: ${({ theme }) => theme.token.paddingMD}px;

  .ant-tabs-nav-wrap {
    justify-content: end;
  }

  .ant-tabs-content-holder {
    margin-top: ${({ theme }) => theme.token.paddingMD}px;
    margin-bottom: ${({ theme }) => theme.token.paddingMD}px;
  }

  .ant-tabs-tab-active .ant-space-item {
    color: ${({ theme }) => theme.token.colorPrimary};
  }
  .ant-tabs-tab-disabled .ant-space-item {
    color: ${({ theme }) => theme.token.colorTextDisabled};
  }

  .ProseMirror {
    font-size: 16px !important; /* Prevents iOS zoom */
    touch-action: manipulation;
    -webkit-tap-highlight-color: transparent;
    -webkit-text-size-adjust: none;
  }

  .tiptap {
    touch-action: manipulation;
    -webkit-tap-highlight-color: transparent;
  }

  .tiptap-editor-wrapper,
  .tiptap-content {
    touch-action: manipulation;
    -webkit-text-size-adjust: none;
  }

  .tiptap-menu-bar,
  .tiptap-floating-menu,
  .tiptap-bubble-menu {
    touch-action: manipulation;
    -webkit-tap-highlight-color: transparent;
  }
`;

export const ScrollbarStyled = styled.div`
  max-height: 70vh;
  max-width: 80vw;
  overflow: auto;

  &::-webkit-scrollbar {
    width: 8px;
  }

  &::-webkit-scrollbar-thumb {
    background: ${({ theme }) => theme.colors.grey};
  }
`;
