import Layout from '~/src/components/Layout/layout/layout';
import { PageWrapperStyled } from '../pages.styles';
import UserHeader from './userHeader/userHeader';
import UserDetails from './userDetails/userDetails';
import { useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

const User = () => {
  const router = useRouter();

  const { data, isPending, isFetched } = useQuery({
    queryKey: [QUERY_KEYS.userProfile, router.query.id],
    queryFn: async () => {
      const response = await api.users.usersDetailDetail(router.query.id as string);
      return response.data;
    },
    enabled: !!router.query.id,
  });

  useEffect(() => {
    if (!data && isFetched) {
      router.push('/search');
    }
  }, [isFetched, data, router]);

  if (isPending && !data) return null;
  if (!data) return null;
  return (
    <Layout
      title={`${data.first_name} ${data.last_name} | JOGL`}
      desc={data.short_bio || data.bio}
      img={data.logo_url}
      noIndex={data.created === undefined} // don't index user if they didn't confirm their account
    >
      <PageWrapperStyled>
        <UserHeader user={data} />
        <UserDetails user={data} />
      </PageWrapperStyled>
    </Layout>
  );
};

export default User;
