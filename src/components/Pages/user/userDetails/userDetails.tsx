import { Tabs } from 'antd';
import type { TabsProps } from 'antd';
import { UserModel } from '~/__generated__/types';

import useTranslation from 'next-translate/useTranslation';
import { ContentWrapperStyled } from './userDetails.styles';
import { About } from './about/about';
import { Portfolio } from './portfolio/portfolio';
import { useState } from 'react';
import { useRouter } from 'next/router';

interface Props {
  user: UserModel;
}

const UserDetails = ({ user }: Props) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const [activeTab, setActiveTab] = useState((router.query.tab as string) ?? 'about');

  const items: TabsProps['items'] = [
    {
      key: 'about',
      label: t('about'),
      children: <About user={user} />,
    },
    {
      key: 'portfolio',
      label: t('portfolio'),
      children: <Portfolio user={user} />,
    },
    // {
    //   key: 'communities',
    //   label: t('community.other'),
    //   children: 'Content of Tab Pane 3',
    // },
  ];

  return (
    <ContentWrapperStyled>
      <Tabs activeKey={activeTab} onChange={(tab) => setActiveTab(tab)} items={items} />
    </ContentWrapperStyled>
  );
};

export default UserDetails;
