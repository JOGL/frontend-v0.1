import styled from '@emotion/styled';
import { Modal, Typography } from 'antd';

const { Text } = Typography;

export const FadeInTextStyled = styled(Text)`
  opacity: 0;
  animation: fadeIn 0.2s ease-in forwards;

  @keyframes fadeIn {
    to {
      opacity: 1;
    }
  }
`;

export const AbstractStyled = styled.div`
  transition: 'opacity 0.5s';
`;

export const ModalStyled = styled(Modal)`
  .ant-modal-content {
    max-height: 90vh;
  }
  .ant-modal-header {
    margin-bottom: ${({ theme }) => theme.token.marginMD}px;
  }
`;
