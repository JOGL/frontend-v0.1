import { Typography, Flex, Divider, Button, Empty } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import React, { FC, useState } from 'react';
import { AbstractStyled, FadeInTextStyled } from './orcidPaper.styled';
import { useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import { ExportOutlined, FileTextOutlined, PlusOutlined } from '@ant-design/icons';
import dayjs from 'dayjs';
import Link from 'next/link';
import { usePaper } from '~/src/components/Publications/createPaper/searchResult/searchResult.hooks';
import Loader from '~/src/components/Common/loader/loader';
const { Text } = Typography;

interface Props {
  refetch: () => void;
}

const OrcidPaper: FC<Props> = ({ refetch }) => {
  const { t } = useTranslation('common');
  const [expandedItems, setExpandedItems] = useState<Record<number, boolean>>({});
  const { addPaper, addingIndexLoader } = usePaper(refetch);
  const { data, isPending, isFetched } = useQuery({
    queryKey: [QUERY_KEYS.papersOrcidList],
    queryFn: async () => {
      const response = await api.users.papersOrcidList();
      return response.data;
    },
  });

  const toggleExpand = (index: number) => {
    setExpandedItems((prev) => ({
      ...prev,
      [index]: !prev[index],
    }));
  };

  return (
    <>
      {isPending && <Loader />}
      {isFetched && !data?.length && <Empty />}
      {data?.map((item, index) => (
        <React.Fragment key={index}>
          <Flex justify="space-between" gap="small">
            <Flex vertical gap="small">
              <Text strong>
                <Flex gap="small" align="baseline">
                  <FileTextOutlined />
                  <span dangerouslySetInnerHTML={{ __html: item.title }} />
                </Flex>
              </Text>
              {item.abstract && (
                <AbstractStyled>
                  <FadeInTextStyled>
                    <span
                      dangerouslySetInnerHTML={{
                        __html: expandedItems[index] ? item.abstract : `${item.abstract?.slice(0, 250)}...`,
                      }}
                    />
                  </FadeInTextStyled>
                  <Typography.Link onClick={() => toggleExpand(index)}>
                    {expandedItems[index] ? t('action.seeLess') : t('action.seeMore')}
                  </Typography.Link>
                </AbstractStyled>
              )}

              <Flex gap="small">
                {item?.publication_date && (
                  <Typography.Text type="secondary">
                    {item?.publication_date
                      ? `${t('publishedOn')}  ${dayjs(item.publication_date).format('MMM D, YYYY')}`
                      : ''}
                    {item.journal_title ? ` - ${item.journal_title}` : ''}
                    {item.authors ? ` - ${item.authors}` : ''}
                  </Typography.Text>
                )}
              </Flex>
              {item.external_id && (
                <Typography.Link>
                  <Link
                    href={item?.external_url ? item.external_url : `https://doi.org/${item?.external_id}`}
                    target="_blank"
                  >
                    {t('externalLink')} <ExportOutlined />
                  </Link>
                </Typography.Link>
              )}
            </Flex>
            <Flex>
              <Button
                type="primary"
                loading={!!addingIndexLoader.length && addingIndexLoader.indexOf(item.external_id) !== -1}
                icon={<PlusOutlined />}
                onClick={() => addPaper(item)}
                size="small"
              />
            </Flex>
          </Flex>
          <Divider />
        </React.Fragment>
      ))}
    </>
  );
};
export default OrcidPaper;
