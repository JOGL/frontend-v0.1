import styled from '@emotion/styled';
export const GridStyled = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: repeat(auto-fill, 350px);
  align-items: center;
  grid-gap: ${({ theme }) => theme.token.sizeMD}px;
`;
