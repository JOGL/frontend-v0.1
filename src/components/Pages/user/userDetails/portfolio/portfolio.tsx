import { DocumentType, FeedEntityVisibility, PortfolioItemType, UserModel } from '~/__generated__/types';
import { Button, Col, Dropdown, Empty, Flex, message, Row } from 'antd';
import { useMutation, useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import Loader from '~/src/components/Common/loader/loader';
import { PortfolioCard } from '~/src/components/portfolioCard/portfolioCard';
import { GridStyled } from './portfolio.styles';
import useTranslation from 'next-translate/useTranslation';
import useUserData from '~/src/hooks/useUserData';
import { useRouter } from 'next/router';
import { useState } from 'react';
import CreatePortfolioPaper from '~/src/components/Publications/createPaper/createPortfolioPaper/createPortfolioPaper';
import CreatePortfolioRepository from '~/src/components/Publications/createPortfolioRepository/createPortfolioRepository';

interface Props {
  user: UserModel;
}

export const Portfolio = ({ user }: Props) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const [showCreateModal, setShowCreateModal] = useState(false);
  const [showCreateRepositoryModal, setShowCreateRepositoryModal] = useState(false);
  const { userData } = useUserData();
  const isSelf = userData?.id === user?.id;

  const { data, isPending, refetch } = useQuery({
    queryKey: [QUERY_KEYS.userPortfolio, user.id],
    queryFn: async () => {
      const response = await api.users.portfolioDetail(user.id);
      return response.data;
    },
  });

  const docData = {
    title: t('untitledPage'),
    type: DocumentType.Jogldoc,
    default_visibility: FeedEntityVisibility.Comment,
  };

  const docMutation = useMutation({
    mutationFn: async () => {
      const response = await api.documents.documentsCreate(user.id, docData);
      return response.data;
    },
    onSuccess: (id) => {
      router.push(`/doc/${id}`);
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const deletePaper = useMutation({
    mutationFn: async (paperId: string) => {
      await api.papers.papersDeleteDetail(paperId);
    },
    onSuccess: () => {
      message.success(t('paperWasRemoved'));
      refetch();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const deleteResource = useMutation({
    mutationFn: async (resourceId: string) => {
      await api.resources.resourcesDeleteDetail(resourceId);
    },
    onSuccess: () => {
      message.success(t('repositoryWasRemoved'));
      refetch();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const deleteJoglDoc = useMutation({
    mutationFn: async (docId: string) => {
      await api.documents.documentsDeleteDetail(docId);
    },
    onSuccess: () => {
      refetch();
      message.success(t('documentWasRemoved'));
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const menuItems = [
    {
      label: t('paper.one'),
      key: 'paper',
      onClick: () => setShowCreateModal(true),
    },
    {
      label: t('page'),
      key: 'joglDoc',
      onClick: () => {
        docMutation.mutate();
      },
    },
    {
      label: t('repository.one'),
      key: 'repository',
      onClick: () => setShowCreateRepositoryModal(true),
    },
  ];
  const addButton = isSelf && (
    <Flex align="center" justify="end" style={{ marginBottom: 16 }}>
      <Dropdown
        placement="bottomRight"
        trigger={['click']}
        menu={{
          items: menuItems,
        }}
      >
        <Button>{t('action.add')}</Button>
      </Dropdown>
    </Flex>
  );
  const getTheLink = (type: PortfolioItemType, id: string, url?: string) => {
    if (url) {
      return url;
    }
    if (type === PortfolioItemType.Paper) {
      return `/paper/${id}`;
    }
    return `/${type}/${id}`;
  };
  if (isPending && !data) return <Loader />;

  return (
    <div>
      {addButton}
      {(!data || !data.length) && (
        <>
          <Row>
            <Col span={24}>
              <Empty description={t('noPortfolioItems')} />
            </Col>
          </Row>
          {showCreateModal && <CreatePortfolioPaper refetch={refetch} handleCancel={() => setShowCreateModal(false)} />}
        </>
      )}
      <GridStyled>
        {data?.map((portfolioItem) => {
          return (
            <PortfolioCard
              key={portfolioItem.id}
              portfolioItem={portfolioItem}
              onDelete={
                isSelf
                  ? () => {
                      if (portfolioItem.type === PortfolioItemType.Paper) {
                        deletePaper.mutate(portfolioItem.id);
                      }
                      if (portfolioItem.type === PortfolioItemType.Jogldoc) {
                        deleteJoglDoc.mutate(portfolioItem.id);
                      }
                      if (portfolioItem.type === PortfolioItemType.Repository) {
                        deleteResource.mutate(portfolioItem.id);
                      }
                    }
                  : undefined
              }
              href={getTheLink(portfolioItem.type, portfolioItem.id, portfolioItem.url)}
            />
          );
        })}
      </GridStyled>
      {showCreateModal && <CreatePortfolioPaper refetch={refetch} handleCancel={() => setShowCreateModal(false)} />}
      {showCreateRepositoryModal && (
        <CreatePortfolioRepository refetch={refetch} handleCancel={() => setShowCreateRepositoryModal(false)} />
      )}
    </div>
  );
};
