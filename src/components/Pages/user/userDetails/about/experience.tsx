import { Flex, Typography, Divider, Grid, Button } from 'antd';
import dayjs from 'dayjs';
import { UserExperienceModel } from '~/__generated__/types';
import { TypographyTextStyled } from './about.styles';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import useTranslation from 'next-translate/useTranslation';

const { useBreakpoint } = Grid;
interface Props {
  experience: UserExperienceModel;
  editEnabled: boolean;
  onEdit: () => void;
  onDelete: () => void;
}

export const Experience = ({ experience, editEnabled, onEdit, onDelete }: Props) => {
  const { t } = useTranslation('common');
  const screens = useBreakpoint();
  return (
    <Flex vertical gap="small">
      <Flex align="start" vertical gap="small">
        <Flex align="center" justify="space-between" style={{ width: '100%' }}>
          <TypographyTextStyled strong>{experience.company}</TypographyTextStyled>
          {editEnabled && (
            <Flex align="center" gap="small">
              <Button type="text" onClick={onEdit}>
                <EditOutlined />
              </Button>
              <Button type="text" onClick={onDelete}>
                <DeleteOutlined />
              </Button>
            </Flex>
          )}
        </Flex>
        <Flex vertical>
          <Typography.Text>{experience.position}</Typography.Text>
          {experience.date_from && (
            <>
              {!screens.xs && <Divider type="vertical" />}
              <Typography.Text type="secondary">
                {`${dayjs(experience.date_from).format('MM-YYYY')} 
                - ${experience.current ? t('present') : dayjs(experience.date_to).format('MM-YYYY')}`}
              </Typography.Text>
            </>
          )}
          {!experience.date_from && (
            <>
              {!screens.xs && <Divider type="vertical" />}
              <Typography.Text type="secondary">
                {`${experience.current ? t('present') : dayjs(experience.date_to).format('MM-YYYY')}`}
              </Typography.Text>
            </>
          )}
        </Flex>
      </Flex>
      {/* <Typography.Paragraph ellipsis={{ rows: 3, expandable: true, symbol: t('more') }}>
        {experience.description}
      </Typography.Paragraph> */}
    </Flex>
  );
};
