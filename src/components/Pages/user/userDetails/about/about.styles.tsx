import styled from '@emotion/styled';
import { Modal, Row, Tag, Typography } from 'antd';
import { getColorToRGB } from '~/src/utils/getColorToRGB';

export const TypographyTextStyled = styled(Typography.Text)`
  color: ${({ theme }) => theme.token.colorPrimary} !important;
`;

export const TagStyled = styled(Tag)`
  margin-bottom: ${({ theme }) => theme.token.sizeSM}px;
  color: ${({ theme }) => theme.token.colorPrimary};
  background-color: ${({ theme }) => getColorToRGB(theme.token.colorPrimary, 0.1)};
`;

export const RowStyled = styled(Row)`
  margin-top: ${({ theme }) => theme.token.sizeXL}px;
`;

export const ModalStyled = styled(Modal)`
  .ant-modal-content {
    max-height: 90vh;
  }
`;
