import { MoreOutlined } from '@ant-design/icons';
import { Dropdown, Flex, MenuProps, Typography, Button } from 'antd';
import { ReactNode } from 'react';

interface Props {
  title: string;
  menuItems?: MenuProps['items'];
  children: ReactNode;
}

export const AboutSection = ({ title, menuItems, children }: Props) => {
  return (
    <Flex vertical gap="middle">
      <Flex align="center" justify="space-between" gap="middle">
        <Typography.Text strong>{title}</Typography.Text>
        {menuItems && (
          <Dropdown
            trigger={['click']}
            placement="bottomRight"
            menu={{
              onClick: ({ domEvent }) => {
                domEvent.stopPropagation();
              },
              items: menuItems,
            }}
          >
            <Button
              type="text"
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
              }}
            >
              <MoreOutlined />
            </Button>
          </Dropdown>
        )}
      </Flex>
      {children}
    </Flex>
  );
};
