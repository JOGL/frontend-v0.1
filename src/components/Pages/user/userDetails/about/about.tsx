import useTranslation from 'next-translate/useTranslation';
import { AboutSection } from './aboutSection';
import { UserEducationModel, UserExperienceModel, UserModel, UserPatchModel } from '~/__generated__/types';
import { Col, Divider, Flex, Typography, Grid, message, Button } from 'antd';
import { Experience } from './experience';
import { Education } from './education';
import { RowStyled, TagStyled } from './about.styles';
import { useState } from 'react';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { EditGeneralModal } from './edit/editGeneral';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { EditInterestsModal } from './edit/editInterests';
import { EditSkillsModal } from './edit/editSkills';
import useUserData from '~/src/hooks/useUserData';
import { ExperienceModal } from './edit/experience';
import { EducationModal } from './edit/education';
import { PlusOutlined } from '@ant-design/icons';
import ORCIDModal from './edit/orcidModal/orcidModal';
const { useBreakpoint } = Grid;

interface Props {
  user: UserModel;
}

export const About = ({ user }: Props) => {
  const { t } = useTranslation('common');
  const { userData } = useUserData();
  const screens = useBreakpoint();
  const isSelf = userData?.id === user?.id;
  const [editGeneral, setEditGeneral] = useState(false);
  const [editInterests, setEditInterests] = useState(false);
  const [editSkills, setEditSkills] = useState(false);
  const [addExperience, setAddExperience] = useState(false);
  const [addORCIDExperience, setAddORCIDExperience] = useState(false);
  const [addORCIDEducation, setAddORCIDEducation] = useState(false);
  const [editExperience, setEditExperience] = useState<number | null>(null);
  const [editEducation, setEditEducation] = useState<number | null>(null);
  const [addEducation, setAddEducation] = useState(false);
  const queryClient = useQueryClient();
  const editProfile = useMutation({
    mutationFn: async (data: UserPatchModel) => {
      await api.users.usersPartialUpdateDetail(user.id, data);
    },
    onSuccess: () => {
      message.success(t('profileWasUpdated'));
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.userProfile],
      });
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });
  const sortedExperience = user.experience.sort((a, b) => {
    const dateToA = new Date(a.date_to ?? '');
    const dateToB = new Date(b.date_to ?? '');
    const dateFromA = new Date(a.date_from ?? '');
    const dateFromB = new Date(b.date_from ?? '');

    // Sort by date_from in descending order (most recent date first)
    if (dateFromA > dateFromB) return -1;
    if (dateFromA < dateFromB) return 1;

    // sort by date_to in descending order (most recent date first)
    if (dateToA > dateToB) return -1;
    if (dateToA < dateToB) return 1;

    return 0;
  });

  const sortedEducation = user.education.sort((a, b) => {
    const dateToA = new Date(a.date_to ?? '');
    const dateToB = new Date(b.date_to ?? '');
    const dateFromA = new Date(a.date_from ?? '');
    const dateFromB = new Date(b.date_from ?? '');

    // Sort by date_from in descending order (most recent date first)
    if (dateFromA > dateFromB) return -1;
    if (dateFromA < dateFromB) return 1;

    // sort by date_to in descending order (most recent date first)
    if (dateToA > dateToB) return -1;
    if (dateToA < dateToB) return 1;

    return 0;
  });
  return (
    <>
      <RowStyled gutter={32}>
        <Col span={screens.xs ? 24 : 12}>
          <Flex vertical gap="middle">
            {user.bio ? (
              <>
                <AboutSection
                  title={t('general')}
                  menuItems={
                    isSelf
                      ? [
                          {
                            label: t('action.edit'),
                            key: 'edit',
                            onClick: (e) => {
                              setEditGeneral(true);
                              e.domEvent.stopPropagation();
                            },
                          },
                        ]
                      : undefined
                  }
                >
                  <Typography.Text>
                    <span dangerouslySetInnerHTML={{ __html: user.bio }} />
                  </Typography.Text>
                </AboutSection>
                <Divider />
              </>
            ) : isSelf ? (
              <>
                <Flex align="center" justify="space-between" gap="middle">
                  <Typography.Text strong>{t('general')}</Typography.Text>
                  <Button icon={<PlusOutlined />} onClick={() => setEditGeneral(true)}>
                    {t('action.add')}
                  </Button>
                </Flex>
                <Divider />
              </>
            ) : null}
            <AboutSection
              title={t('experience.one')}
              menuItems={
                isSelf
                  ? [
                      {
                        label: t('action.add'),
                        key: 'add',
                        onClick: (e) => {
                          setAddExperience(true);
                          e.domEvent.stopPropagation();
                        },
                      },
                      {
                        label: t('addWithORCID'),
                        key: 'addWithORCID',
                        onClick: (e) => {
                          setAddORCIDExperience(true);
                          e.domEvent.stopPropagation();
                        },
                      },
                    ]
                  : undefined
              }
            >
              <Flex vertical gap="large">
                {sortedExperience.map((experience, idx) => {
                  return (
                    <Experience
                      key={`${experience.company}-${experience.position}`}
                      experience={experience}
                      editEnabled={isSelf}
                      onEdit={() => setEditExperience(idx)}
                      onDelete={() => editProfile.mutate({ experience: sortedExperience.filter((e, i) => idx !== i) })}
                    />
                  );
                })}
              </Flex>
            </AboutSection>
            <Divider />
          </Flex>
        </Col>
        <Col span={screens.xs ? 24 : 12}>
          <Flex vertical gap="middle">
            {sortedEducation.length ? (
              <AboutSection
                title={t('education.one')}
                menuItems={
                  isSelf
                    ? [
                        {
                          label: t('action.add'),
                          key: 'add',
                          onClick: (e) => {
                            setAddEducation(true);
                            e.domEvent.stopPropagation();
                          },
                        },
                        {
                          label: t('addWithORCID'),
                          key: 'addWithORCID',
                          onClick: (e) => {
                            setAddORCIDEducation(true);
                            e.domEvent.stopPropagation();
                          },
                        },
                      ]
                    : undefined
                }
              >
                <Flex vertical gap="large">
                  {sortedEducation.map((education, idx) => {
                    return (
                      <Education
                        key={`${education.school}-${education.program}`}
                        education={education}
                        editEnabled={isSelf}
                        onEdit={() => setEditEducation(idx)}
                        onDelete={() => editProfile.mutate({ education: sortedEducation.filter((e, i) => idx !== i) })}
                      />
                    );
                  })}
                </Flex>
              </AboutSection>
            ) : isSelf ? (
              <>
                <Flex align="center" justify="space-between" gap="middle">
                  <Typography.Text strong>{t('education.one')}</Typography.Text>
                  <Button icon={<PlusOutlined />} onClick={() => setAddEducation(true)}>
                    {t('action.add')}
                  </Button>
                </Flex>
              </>
            ) : null}
            <Divider />
            {user.skills.length ? (
              <>
                <AboutSection
                  title={t('skill.other')}
                  menuItems={
                    isSelf
                      ? [
                          {
                            label: t('action.edit'),
                            key: 'edit',
                            onClick: (e) => {
                              setEditSkills(true);
                              e.domEvent.stopPropagation();
                            },
                          },
                        ]
                      : undefined
                  }
                >
                  <Flex wrap>
                    {user.skills.map((skill) => (
                      <TagStyled key={skill}>{skill}</TagStyled>
                    ))}
                  </Flex>
                </AboutSection>
                <Divider />
              </>
            ) : isSelf ? (
              <>
                <Flex align="center" justify="space-between" gap="middle">
                  <Typography.Text strong>{t('skill.one')}</Typography.Text>
                  <Button icon={<PlusOutlined />} onClick={() => setEditSkills(true)}>
                    {t('action.add')}
                  </Button>
                </Flex>
                <Divider />
              </>
            ) : null}
            {user.interests.length ? (
              <AboutSection
                title={t('interest.other')}
                menuItems={
                  isSelf
                    ? [
                        {
                          label: t('action.edit'),
                          key: 'edit',
                          onClick: (e) => {
                            setEditInterests(true);
                            e.domEvent.stopPropagation();
                          },
                        },
                      ]
                    : undefined
                }
              >
                <Flex wrap>
                  {user.interests.map((interest) => (
                    <TagStyled key={interest}>{interest}</TagStyled>
                  ))}
                </Flex>
              </AboutSection>
            ) : isSelf ? (
              <>
                <Flex align="center" justify="space-between" gap="middle">
                  <Typography.Text strong>{t('interest.one')}</Typography.Text>
                  <Button icon={<PlusOutlined />} onClick={() => setEditInterests(true)}>
                    {t('action.add')}
                  </Button>
                </Flex>
                <Divider />
              </>
            ) : null}
          </Flex>
        </Col>
      </RowStyled>
      {editGeneral && (
        <EditGeneralModal
          user={user}
          onConfirm={(data: UserPatchModel) => {
            editProfile.mutate(data);
            setEditGeneral(false);
          }}
          onClose={() => setEditGeneral(false)}
        />
      )}
      {editInterests && (
        <EditInterestsModal
          user={user}
          onConfirm={(data: UserPatchModel) => {
            editProfile.mutate(data);
            setEditInterests(false);
          }}
          onClose={() => setEditInterests(false)}
        />
      )}
      {editSkills && (
        <EditSkillsModal
          user={user}
          onConfirm={(data: UserPatchModel) => {
            editProfile.mutate(data);
            setEditSkills(false);
          }}
          onClose={() => setEditSkills(false)}
        />
      )}
      {addExperience && (
        <ExperienceModal
          onConfirm={(experience: UserExperienceModel) => {
            editProfile.mutate({ experience: [experience, ...(user.experience || [])] });
            setAddExperience(false);
          }}
          onClose={() => setAddExperience(false)}
        />
      )}
      {addORCIDExperience && (
        <ORCIDModal
          type="employments"
          onConfirm={(experience: UserExperienceModel[]) => {
            editProfile.mutate({ experience: [...experience, ...(user.experience || [])] });
            setAddORCIDExperience(false);
          }}
          onClose={() => setAddORCIDExperience(false)}
        />
      )}
      {editExperience !== null && (
        <ExperienceModal
          experience={user.experience[editExperience]}
          onConfirm={(updatedExperience: UserExperienceModel) => {
            editProfile.mutate({
              experience: user.experience.map((experience, idx) => {
                if (idx === editExperience) {
                  return updatedExperience;
                }
                return experience;
              }),
            });
            setEditExperience(null);
          }}
          onClose={() => setEditExperience(null)}
        />
      )}
      {addEducation && (
        <EducationModal
          onConfirm={(education: UserEducationModel) => {
            editProfile.mutate({ education: [education, ...(user.education || [])] });
            setAddEducation(false);
          }}
          onClose={() => setAddEducation(false)}
        />
      )}
      {addORCIDEducation && (
        <ORCIDModal
          type="education"
          onConfirm={(education: UserEducationModel[]) => {
            editProfile.mutate({ education: [...education, ...(user.education || [])] });
            setAddORCIDEducation(false);
          }}
          onClose={() => setAddORCIDEducation(false)}
        />
      )}
      {editEducation !== null && (
        <EducationModal
          education={user.education[editEducation]}
          onConfirm={(updatedEducation: UserEducationModel) => {
            editProfile.mutate({
              education: user.education.map((education, idx) => {
                if (idx === editEducation) {
                  return updatedEducation;
                }
                return education;
              }),
            });
            setEditEducation(null);
          }}
          onClose={() => setEditEducation(null)}
        />
      )}
    </>
  );
};
