import styled from '@emotion/styled';
import { getColorToRGB } from '~/src/utils/getColorToRGB';

export const ExperienceItemStyled = styled.div<{ selected: boolean }>`
  padding: ${({ theme }) => theme.token.sizeSM}px;
  border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;
  cursor: pointer;
  transition: all 0.3s ease;
  background-color: ${(props) => (props.selected ? getColorToRGB(props.theme.token.colorPrimary, 0.2) : 'transparent')};
  margin-top: ${({ theme }) => theme.token.sizeSM}px;
  &:hover {
    background-color: ${(props) =>
      props.selected
        ? getColorToRGB(props.theme.token.colorPrimary, 0.1)
        : getColorToRGB(props.theme.token.neutral8, 0.1)};
  }
`;
