import { Typography, Flex } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useState } from 'react';
import { ModalStyled } from '../../about.styles';
import { ScrollbarFullWidthStyled } from '~/src/assets/styles/Global.styled';
import { Employment, UserExperienceModel } from '~/__generated__/types';
import { useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import Loader from '~/src/components/Common/loader/loader';
import { ExperienceItemStyled } from './orcidModal.styles';

interface Props {
  onClose: () => void;
  onConfirm: (data: UserExperienceModel) => void;
  type: 'employments' | 'educations';
}

const ORCIDModal = ({ onClose, onConfirm, type }) => {
  const { t } = useTranslation('common');
  const [selectedItems, setSelectedItems] = useState<number[]>([]);

  const { data, isFetching } = useQuery({
    queryKey: [QUERY_KEYS.documentDetails],
    queryFn: async () => {
      const response = await api.users.orcidUserdataList();
      return response.data;
    },
  });
  const handleItemClick = (index: number) => {
    setSelectedItems((prev) => (prev.includes(index) ? prev.filter((i) => i !== index) : [...prev, index]));
  };

  const handleAdd = () => {
    const selectedExperiences = selectedItems.map((index) =>
      type === 'employments'
        ? {
            company: data?.employments[index].company,
            position: data?.employments[index].position,
            description: '',
            date_from: data?.employments[index].date_from && new Date(data?.employments[index].date_from),
            date_to: data?.employments[index].date_to && new Date(data?.employments[index].date_to),
          }
        : {
            school: data?.educations[index].school,
            dateFrom: data?.educations[index].dateFrom && new Date(data?.educations[index].dateFrom),
            dateTo: data?.educations[index].dateTo && new Date(data?.educations[index].dateTo),
            description: `${data?.educations[index].degree_name} ${data?.educations[index].department_name}`,
          }
    );
    onConfirm(selectedExperiences);
  };

  return (
    <ModalStyled
      onCancel={onClose}
      open
      title={t('action.importFromOrcid')}
      okText={t('action.add')}
      onOk={handleAdd}
      cancelButtonProps={{ style: { display: 'none' } }}
      styles={{
        footer: {
          textAlign: 'center',
        },
      }}
    >
      {isFetching && <Loader />}
      <ScrollbarFullWidthStyled>
        {type === 'employments'
          ? data?.employments.map((item: Employment, index) => (
              <ExperienceItemStyled
                key={index}
                selected={selectedItems.includes(index)}
                onClick={() => handleItemClick(index)}
              >
                <Flex vertical gap="small">
                  <Typography.Text strong>{item.position}</Typography.Text>
                  <Typography.Text>
                    {item.company} {item.department_name} {item.date_from ? `${item.date_from} - ${item.date_to}` : ''}
                  </Typography.Text>
                </Flex>
              </ExperienceItemStyled>
            ))
          : data?.educations.map((item, index) => (
              <ExperienceItemStyled
                key={index}
                selected={selectedItems.includes(index)}
                onClick={() => handleItemClick(index)}
              >
                <Flex vertical gap="small">
                  <Typography.Text strong>{item.school}</Typography.Text>
                  <Typography.Text>
                    {item.degree_name} {item.department_name} {item.dateFrom ? `${item.dateFrom} - ${item.dateTo}` : ''}
                  </Typography.Text>
                </Flex>
              </ExperienceItemStyled>
            ))}
      </ScrollbarFullWidthStyled>
    </ModalStyled>
  );
};

export default ORCIDModal;
