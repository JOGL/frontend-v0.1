import { useQuery } from '@tanstack/react-query';
import { Form, Modal, Select } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useState } from 'react';
import { UserModel, UserPatchModel } from '~/__generated__/types';
import useDebounce from '~/src/hooks/useDebounce';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';

const EDIT_SKILLS = 'edit-skills-form';

interface Props {
  user: UserModel;
  onClose: () => void;
  onConfirm: (data: UserPatchModel) => void;
}

export const EditSkillsModal = ({ user, onClose, onConfirm }: Props) => {
  const { t } = useTranslation('common');
  const [form] = Form.useForm();
  const [search, setSearch] = useState('');
  const debouncedSearch = useDebounce(search, 300);

  const { data, isPending } = useQuery({
    queryKey: [QUERY_KEYS.usersSkillsList, debouncedSearch],
    queryFn: async () => {
      const response = await api.users.skillsList({ Search: debouncedSearch });
      return response.data;
    },
  });

  const selectedValues = Form.useWatch('skills', form) || [];

  const options = search
    ? [...(data || []).map(({ value }) => ({ value, label: value })), { value: search, label: search }].filter(
        (option) => !selectedValues.includes(option.value)
      )
    : [];

  return (
    <Modal
      open
      title={t('editSkills')}
      okText={t('action.save')}
      onCancel={onClose}
      okButtonProps={{ htmlType: 'submit', form: EDIT_SKILLS }}
    >
      <Form form={form} layout="vertical" initialValues={{ skills: user.skills }} id={EDIT_SKILLS} onFinish={onConfirm}>
        <Form.Item name="skills" label={t('legacy.form.skills')}>
          <Select
            showSearch
            loading={isPending}
            mode="multiple"
            style={{ width: '100%' }}
            searchValue={search}
            onSearch={setSearch}
            onSelect={() => setSearch('')}
            placeholder={t('searchToSelect')}
            optionFilterProp="label"
            options={options}
            open={!!search}
          />
        </Form.Item>
      </Form>
    </Modal>
  );
};
