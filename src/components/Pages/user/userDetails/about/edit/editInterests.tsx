import { useQuery } from '@tanstack/react-query';
import { Form, Modal, Select } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useState } from 'react';
import { UserModel, UserPatchModel } from '~/__generated__/types';
import useDebounce from '~/src/hooks/useDebounce';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';

const EDIT_INTERESTS = 'edit-interests-form';

interface Props {
  user: UserModel;
  onClose: () => void;
  onConfirm: (data: UserPatchModel) => void;
}
export const EditInterestsModal = ({ user, onClose, onConfirm }: Props) => {
  const { t } = useTranslation('common');
  const [form] = Form.useForm();
  const [search, setSearch] = useState('');
  const debouncedSearch = useDebounce(search, 300);

  const { data, isPending } = useQuery({
    queryKey: [QUERY_KEYS.usersInterestsList, debouncedSearch],
    queryFn: async () => {
      const response = await api.users.interestsList({ Search: debouncedSearch });
      return response.data;
    },
  });

  const selectedValues = Form.useWatch('interests', form) || [];

  const options = search
    ? [...(data || []).map(({ value }) => ({ value, label: value })), { value: search, label: search }].filter(
        (option) => !selectedValues.includes(option.value)
      )
    : [];

  return (
    <Modal
      open
      title={t('editInterests')}
      okText={t('action.save')}
      onCancel={onClose}
      okButtonProps={{ htmlType: 'submit', form: EDIT_INTERESTS }}
    >
      <Form
        form={form}
        layout="vertical"
        initialValues={{ interests: user.interests }}
        id={EDIT_INTERESTS}
        onFinish={onConfirm}
      >
        <Form.Item name="interests" label={t('legacy.form.interests')}>
          <Select
            showSearch
            loading={isPending}
            mode="multiple"
            style={{ width: '100%' }}
            searchValue={search}
            onSearch={setSearch}
            onSelect={() => setSearch('')}
            placeholder={t('searchToSelect')}
            optionFilterProp="label"
            options={options}
            open={!!search}
          />
        </Form.Item>
      </Form>
    </Modal>
  );
};
