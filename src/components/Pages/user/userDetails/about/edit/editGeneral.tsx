import { Form, Input, Modal } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { UserModel, UserPatchModel } from '~/__generated__/types';

const EDIT_GENERAL_FORM = 'edit-general-form';

interface Props {
  user: UserModel;
  onClose: () => void;
  onConfirm: (data: UserPatchModel) => void;
}

export const EditGeneralModal = ({ user, onClose, onConfirm }: Props) => {
  const { t } = useTranslation('common');
  const [form] = Form.useForm();

  return (
    <Modal
      open
      title={t('editGeneralInfo')}
      okText={t('action.save')}
      onCancel={onClose}
      okButtonProps={{ htmlType: 'submit', form: EDIT_GENERAL_FORM }}
    >
      <Form form={form} layout="vertical" initialValues={{ bio: user.bio }} id={EDIT_GENERAL_FORM} onFinish={onConfirm}>
        <Form.Item name="bio" label={t('general')}>
          <Input.TextArea rows={10} />
        </Form.Item>
      </Form>
    </Modal>
  );
};
