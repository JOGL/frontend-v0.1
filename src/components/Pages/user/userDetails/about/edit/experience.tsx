import { Checkbox, DatePicker, Form, Input, Modal } from 'antd';
import dayjs from 'dayjs';
import useTranslation from 'next-translate/useTranslation';
import { UserExperienceModel } from '~/__generated__/types';

const ADD_EXPERIENCE_FORM = 'add-experience-form';

interface Props {
  experience?: UserExperienceModel;
  onClose: () => void;
  onConfirm: (data: UserExperienceModel) => void;
}

export const ExperienceModal = ({ experience, onClose, onConfirm }: Props) => {
  const { t } = useTranslation('common');
  const [form] = Form.useForm();
  const isCurrent = Form.useWatch('current', form);
  const requiredRule = {
    required: true,
    message: t('error.requiredField'),
  };
  return (
    <Modal
      open
      title={t('addExperience')}
      okText={t('action.save')}
      onCancel={onClose}
      okButtonProps={{ htmlType: 'submit', form: ADD_EXPERIENCE_FORM }}
    >
      <Form
        form={form}
        layout="vertical"
        initialValues={{
          company: experience?.company,
          position: experience?.position,
          description: experience?.description,
          date_from: experience?.date_from ? dayjs(experience.date_from) : undefined,
          date_to: experience?.date_to ? dayjs(experience.date_to) : undefined,
          current: experience?.current,
        }}
        id={ADD_EXPERIENCE_FORM}
        onFinish={(values) => {
          // Clear the dateTo value if current is checked
          if (values.current) {
            values.dateTo = undefined;
          }
          onConfirm(values);
        }}
      >
        <Form.Item name="position" label={t('position')} required rules={[requiredRule]}>
          <Input />
        </Form.Item>
        <Form.Item name="company" label={t('company.one')} required rules={[requiredRule]}>
          <Input />
        </Form.Item>
        <Form.Item name="date_from" label={t('legacy.formValidator.start')} required rules={[requiredRule]}>
          <DatePicker picker='month' style={{ width: '100%' }} />
        </Form.Item>
        <Form.Item name="date_to" label={t('legacy.formValidator.end')} rules={isCurrent ? [] : [requiredRule]}>
          <DatePicker picker='month' style={{ width: '100%' }} disabled={isCurrent} />
        </Form.Item>
        <Form.Item name="current" valuePropName="checked">
          <Checkbox>{t('currentlyWorkingHere')}</Checkbox>
        </Form.Item>
        <Form.Item name="description" label={t('description')}>
          <Input.TextArea rows={5} maxLength={500} showCount />
        </Form.Item>
      </Form>
    </Modal>
  );
};
