import { Checkbox, DatePicker, Form, Input, Modal } from 'antd';
import dayjs from 'dayjs';
import useTranslation from 'next-translate/useTranslation';
import { UserEducationModel } from '~/__generated__/types';

const ADD_EDUCATION_FORM = 'add-education-form';
const MAX_DESCRIPTION_LENGTH = 500;

interface Props {
  education?: UserEducationModel;
  onClose: () => void;
  onConfirm: (data: UserEducationModel) => void;
}

export const EducationModal = ({ education, onClose, onConfirm }: Props) => {
  const { t } = useTranslation('common');
  const [form] = Form.useForm();
  const isCurrent = Form.useWatch('current', form);
  const requiredRule = {
    required: true,
    message: t('error.requiredField'),
  };

  return (
    <Modal
      open
      title={t('addEducation')}
      okText={t('action.save')}
      onCancel={onClose}
      okButtonProps={{ htmlType: 'submit', form: ADD_EDUCATION_FORM }}
    >
      <Form
        form={form}
        layout="vertical"
        initialValues={{
          school: education?.school,
          program: education?.program,
          description: education?.description,
          date_from: education?.date_from ? dayjs(education.date_from) : undefined,
          date_to: education?.date_to ? dayjs(education.date_to) : undefined,
          current: education?.current,
        }}
        id={ADD_EDUCATION_FORM}
        onFinish={(values) => {
          // Clear the date_to value if current is checked
          if (values.current) {
            values.date_to = undefined;
          }
          onConfirm(values);
        }}
      >
        <Form.Item name="school" label={t('school.one')} required rules={[requiredRule]}>
          <Input />
        </Form.Item>
        <Form.Item name="program" label={t('program.one')} required rules={[requiredRule]}>
          <Input />
        </Form.Item>
        <Form.Item name="date_from" label={t('legacy.formValidator.start')}>
          <DatePicker picker='year' style={{ width: '100%' }} />
        </Form.Item>
        <Form.Item name="date_to" label={t('legacy.formValidator.end')} rules={isCurrent ? [] : [requiredRule]}>
          <DatePicker picker='year' style={{ width: '100%' }} disabled={isCurrent} />
        </Form.Item>
        <Form.Item name="current" valuePropName="checked">
          <Checkbox>{t('currentlyStudyingHere')}</Checkbox>
        </Form.Item>
        <Form.Item
          name="description"
          label={t('description')}
          rules={[
            {
              max: MAX_DESCRIPTION_LENGTH,
              message: t('error.maxLength', { max: MAX_DESCRIPTION_LENGTH }),
            },
          ]}
        >
          <Input.TextArea
            rows={5}
            maxLength={MAX_DESCRIPTION_LENGTH}
            showCount={{
              formatter: ({ count, maxLength }) => `${count}/${maxLength}`,
            }}
          />
        </Form.Item>
      </Form>
    </Modal>
  );
};
