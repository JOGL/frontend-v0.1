import { Flex, Typography, Button, Grid, Divider } from 'antd';
import dayjs from 'dayjs';
import { UserEducationModel } from '~/__generated__/types';
import { TypographyTextStyled } from './about.styles';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import useTranslation from 'next-translate/useTranslation';
const { useBreakpoint } = Grid;

interface Props {
  education: UserEducationModel;
  editEnabled: boolean;
  onEdit: () => void;
  onDelete: () => void;
}

export const Education = ({ education, editEnabled, onEdit, onDelete }: Props) => {
  const { t } = useTranslation('common');
  const screens = useBreakpoint();
  return (
    <Flex vertical gap="small">
    <Flex align="start" vertical gap="small">
      <Flex align="center" justify="space-between" style={{ width: '100%' }}>
        <TypographyTextStyled strong>{education.school}</TypographyTextStyled>
        {editEnabled && (
          <Flex align="center" gap="small">
            <Button type="text" onClick={onEdit}>
              <EditOutlined />
            </Button>
            <Button type="text" onClick={onDelete}>
              <DeleteOutlined />
            </Button>
          </Flex>
        )}
      </Flex>
      <Flex vertical>
        <Typography.Text>{education.program}</Typography.Text>
        {education.date_from && (
          <>
            {!screens.xs && <Divider type="vertical" />}
            <Typography.Text type="secondary">
              {`${dayjs(education.date_from).format('YYYY')} 
              - ${
                education.current
                  ? t('present')
                  : dayjs(education.date_to).format('YYYY')
              }`}
            </Typography.Text>
          </>
        )}
         {!education.date_from && (
                    <>
                      {!screens.xs && <Divider type="vertical" />}
                      <Typography.Text type="secondary">
                        {`${education.current ? t('present') : dayjs(education.date_to).format('YYYY')}`}
                      </Typography.Text>
                    </>
                  )}
      </Flex>
    </Flex>
    {/* <Typography.Paragraph ellipsis={{ rows: 3, expandable: true, symbol: t('more') }}>
      {education.description}
    </Typography.Paragraph> */}
  </Flex>
  );
};
