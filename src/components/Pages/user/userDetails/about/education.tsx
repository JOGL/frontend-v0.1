import { Flex, Typography, Button } from 'antd';
import dayjs from 'dayjs';
import { UserEducationModel } from '~/__generated__/types';
import { TypographyTextStyled } from './about.styles';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import useTranslation from 'next-translate/useTranslation';

interface Props {
  education: UserEducationModel;
  editEnabled: boolean;
  onEdit: () => void;
  onDelete: () => void;
}

export const Education = ({ education, editEnabled, onEdit, onDelete }: Props) => {
  const { t } = useTranslation('common');
  return (
    <Flex vertical gap="small">
      <Flex align="center" justify="space-between" gap="small" style={{ width: '100%' }}>
        <TypographyTextStyled strong>{education.school}</TypographyTextStyled>
        {editEnabled && (
          <Flex align="center" gap="small">
            <Button type="text" onClick={onEdit}>
              <EditOutlined />
            </Button>
            <Button type="text" onClick={onDelete}>
              <DeleteOutlined />
            </Button>
          </Flex>
        )}
      </Flex>

      {education.date_from && (
        <Typography.Text type="secondary">
          {`${dayjs(education.date_from).format('MM-YYYY')} 
                       - ${education.current ? t('present') : dayjs(education.date_to).format('MM-YYYY')}`}
        </Typography.Text>
      )}
      <Typography.Text>{education.description}</Typography.Text>
    </Flex>
  );
};
