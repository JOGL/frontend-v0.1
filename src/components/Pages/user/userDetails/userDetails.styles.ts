import styled from '@emotion/styled';

export const ContentWrapperStyled = styled.div`
  position: relative;
  padding: ${({ theme }) => theme.token.sizeLG}px;
`;
