import { DeleteOutlined, EditOutlined, UserOutlined } from '@ant-design/icons';
import { Grid, Divider, Flex, Typography, Button, Upload, message } from 'antd';
import Image from 'next/image';
import { ImageInsertModel, UserModel, UserPatchModel } from '~/__generated__/types';
import {
  AvatarStyled,
  AvatarWrapper,
  ButtonStyled,
  ContentWrapper,
  ExternalLinksContainer,
  FlexStyled,
  ImageWrapper,
  TitleStyled,
  UploadWrapperStyled,
} from './userHeader.styles';
import useTranslation from 'next-translate/useTranslation';
import useUserData from '~/src/hooks/useUserData';
import type { UploadFile } from 'antd';
import { UserLink } from './userlink';
import { useState } from 'react';
import { UserEditProfileModal } from './userEditProfile/userEditProfile';
import ImgCrop from 'antd-img-crop';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
const { useBreakpoint } = Grid;

interface Props {
  user: UserModel;
}

const UserHeader = ({ user }: Props) => {
  const { t } = useTranslation('common');
  const queryClient = useQueryClient();

  const { userData, isLoading } = useUserData();
  const screens = useBreakpoint();
  const isSelf = userData?.id === user?.id;
  const isMobile = screens.xs;
  const [showEditModal, setShowEditModal] = useState(false);
  const updateProfile = useMutation({
    mutationFn: async (userData: UserPatchModel) => {
      await api.users.usersPartialUpdateDetail(user.id, userData);
    },
    onSuccess: () => {
      message.success(t('successfullyUpdated'));
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.userProfile],
      });
      queryClient.invalidateQueries({ queryKey: [QUERY_KEYS.userData, user.id] });
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const uploadImage = useMutation({
    mutationFn: async ({ imageInfo, id }: { imageInfo: ImageInsertModel; id: string }) => {
      const response = await api.images.imagesCreate(imageInfo);
      return { [id]: response.data.id };
    },
    onSuccess: (data) => {
      updateProfile.mutate(data);
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const onChange = ({ file, id }: { file: UploadFile; id: string }) => {
    if (file.status === 'done') {
      const reader = new FileReader();
      reader.onload = (e) => {
        const base64 = e.target?.result as string;
        const imageInfo = {
          title: file.name,
          file_name: file.name,
          description: '',
          created: file?.lastModified ? new Date(file.lastModified).toISOString() : new Date().toISOString(),
          updated: file?.lastModified ? new Date(file.lastModified).toISOString() : new Date().toISOString(),
          last_activity: file?.lastModified ? new Date(file.lastModified).toISOString() : new Date().toISOString(),
          data: base64,
          created_by_user_id: user.id,
        };
        uploadImage.mutate({ imageInfo, id });
      };
      if (file.originFileObj) {
        reader.readAsDataURL(file.originFileObj);
      }
    }
  };

  const onDelete = () => {
    updateProfile.mutate({ banner_id: null });
  };
  return (
    <FlexStyled vertical>
      <ImageWrapper>
        <Image src={user.banner_url || '/images/default/default-user-banner.svg'} alt="banner" fill objectFit="cover" />
        {isSelf && (
          <UploadWrapperStyled>
            <Flex gap="small">
              <ImgCrop rotationSlider aspect={16 / 5}>
                <Upload
                  accept="image/jpeg, image/png, image/jpg"
                  onChange={({ file }) => onChange({ file, id: 'banner_id' })}
                  showUploadList={false}
                  multiple={false}
                >
                  <Button icon={<EditOutlined />} shape="circle" />
                </Upload>
              </ImgCrop>
              <Button icon={<DeleteOutlined />} shape="circle" onClick={onDelete} />
            </Flex>
          </UploadWrapperStyled>
        )}
      </ImageWrapper>
      {!isLoading && (
        <>
          {isSelf ? (
            <ButtonStyled onClick={() => setShowEditModal(true)}>{t('editProfile')}</ButtonStyled>
          ) : (
            // <ButtonStyled type="primary">{t('message')}</ButtonStyled>
            <></>
          )}
        </>
      )}

      <ContentWrapper>
        <AvatarWrapper>
          <AvatarStyled src={user.logo_url} icon={<UserOutlined />} size={isMobile ? 120 : 160} />
          {isSelf && (
            <UploadWrapperStyled>
              <ImgCrop rotationSlider cropShape="round">
                <Upload
                  accept="image/jpeg, image/png, image/jpg"
                  onChange={({ file }) => onChange({ file, id: 'logo_id' })}
                  showUploadList={false}
                  multiple={false}
                >
                  <Button icon={<EditOutlined />} shape="circle" />
                </Upload>
              </ImgCrop>
            </UploadWrapperStyled>
          )}
        </AvatarWrapper>

        <Flex vertical gap="small">
          <div>
            <TitleStyled level={3}>{`${user.first_name} ${user.last_name}`}</TitleStyled>
            <Typography.Text type="secondary">@{user.username}</Typography.Text>
          </div>
          {user.short_bio && <Typography.Text>{user.short_bio}</Typography.Text>}

          <Flex gap="small" align={isMobile ? 'start' : 'center'} vertical={isMobile}>
            {(user.city || user.country) && (
              <Typography.Text type="secondary">{`${user.city ?? ''} ${user.country ?? ''}`}</Typography.Text>
            )}
            {(user.city || user.country) && !!user.links.length && !isMobile && <Divider type="vertical" />}
            <ExternalLinksContainer>
              {user.links.map((link) => (
                <UserLink key={link.url} link={link} />
              ))}
            </ExternalLinksContainer>
          </Flex>
        </Flex>
      </ContentWrapper>
      {showEditModal && <UserEditProfileModal onClose={() => setShowEditModal(false)} user={user} />}
    </FlexStyled>
  );
};

export default UserHeader;
