import {
  DropboxOutlined,
  FacebookOutlined,
  GithubOutlined,
  GitlabOutlined,
  GoogleOutlined,
  InstagramOutlined,
  LinkedinOutlined,
  LinkOutlined,
  SkypeOutlined,
  XOutlined,
  YoutubeOutlined,
} from '@ant-design/icons';
import { Button } from 'antd';
import { UserModel } from '~/__generated__/types';

const getIcon = (url: string) => {
  if (url.includes('website')) return <LinkOutlined />;
  if (url.includes('github')) return <GithubOutlined />;
  if (url.includes('gitlab')) return <GitlabOutlined />;
  if (url.includes('linkedin')) return <LinkedinOutlined />;
  if (url.includes('twitter')) return <XOutlined />;
  if (url.includes('instagram')) return <InstagramOutlined />;
  if (url.includes('facebook')) return <FacebookOutlined />;
  if (url.includes('youtube')) return <YoutubeOutlined />;
  if (url.includes('dropbox')) return <DropboxOutlined />;
  if (url.includes('google')) return <GoogleOutlined />;
  if (url.includes('skype')) return <SkypeOutlined />;
  return <LinkOutlined />;
};

interface UserLinkProps {
  link: UserModel['links'][number];
}

export const UserLink = ({ link }: UserLinkProps) => {
  const { url } = link;

  return (
    <Button onClick={() => window.open(url)} type="link">
      {getIcon(url)}
    </Button>
  );
};
