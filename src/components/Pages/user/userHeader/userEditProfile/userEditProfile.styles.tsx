import styled from '@emotion/styled';
import { Flex } from 'antd';

export const FlexStyled = styled(Flex)`
  border: 1px solid ${({ theme }) => theme.token.colorBorder};
  border-radius: ${({ theme }) => theme.token.borderRadius}px;
`;
