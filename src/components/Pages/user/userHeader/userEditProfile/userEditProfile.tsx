import { Button, Flex, Form, Input, Modal, Typography, message } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useState } from 'react';
import { UserModel, UserPatchModel, Link } from '~/__generated__/types';
import { LocationSearch } from '~/src/components/Tools/Forms/FormLocationComponent';
import api from '~/src/utils/api/api';
import { UserLink } from '../userlink';
import { CloseOutlined, PlusOutlined } from '@ant-design/icons';
import { FlexStyled } from './userEditProfile.styles';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { deepEqual } from '~/src/utils/utils';

const { TextArea } = Input;
const EDIT_USER_PROFILE_FORM = 'edit-user-profile-form';

interface Props {
  user: UserModel;
  onClose: () => void;
}

export const UserEditProfileModal = ({ user, onClose }: Props) => {
  const { t } = useTranslation('common');
  const [form] = Form.useForm();
  const [isUsernameTaken, setIsUsernameTaken] = useState(false);
  const [isFormChanged, setIsFormChanged] = useState(false);
  const [externalWebsiteLink, setExternalWebsiteLink] = useState<Link>({
    title: '',
    url: '',
  });
  const [externalWebsiteLinks, setExternalWebsiteLinks] = useState<Link[]>(user.links || []);
  const queryClient = useQueryClient();

  const mutation = useMutation({
    mutationFn: async (userData: UserPatchModel) => {
      await api.users.usersPartialUpdateDetail(user.id, userData);
    },
    onSuccess: () => {
      message.success(t('successfullyUpdated'));
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.userProfile],
      });
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const checkIfFormChanged = (changedValues: any, allValues: any) => {
    const initialValues = {
      first_name: user.first_name,
      last_name: user.last_name,
      username: user.username,
      short_bio: user.short_bio,
      city: user.city,
      links: user.links || [],
    };

    const currentValues = {
      ...allValues,
      city: allValues.city || '',
      links: externalWebsiteLinks,
    };

    const hasChanges = !deepEqual(initialValues, currentValues);
    setIsFormChanged(hasChanges);
  };

  const handleFinish = async (values: any) => {
    const updatedData = { ...values, links: externalWebsiteLinks };
    mutation.mutate(updatedData);
    onClose();
  };

  const handleUsernameChange = async () => {
    const newUserName = form.getFieldValue('username');

    if (newUserName === user.username) {
      return;
    }

    try {
      await api.users.existsUsernameDetailDetail(newUserName);
      setIsUsernameTaken(false);
    } catch {
      setIsUsernameTaken(true);
    }
  };

  const handleExternalWebsiteLinkAdd = () => {
    const updatedLinks = [...externalWebsiteLinks, externalWebsiteLink];
    setExternalWebsiteLinks(updatedLinks);
    form.setFieldsValue({
      links: updatedLinks,
      externalWebsiteUrl: '',
    });
    setExternalWebsiteLink({ title: '', url: '' });
    checkIfFormChanged({}, form.getFieldsValue());
  };

  const handleExternalWebsiteLinkRemove = (index: number) => {
    const updatedLinks = [...externalWebsiteLinks];
    updatedLinks.splice(index, 1);
    setExternalWebsiteLinks(updatedLinks);
    form.setFieldsValue({ links: updatedLinks });
    checkIfFormChanged({}, form.getFieldsValue());
  };

  return (
    <Modal
      open
      title={t('editProfile')}
      okText={t('action.save')}
      onCancel={onClose}
      okButtonProps={{
        htmlType: 'submit',
        form: EDIT_USER_PROFILE_FORM,
        disabled: !isFormChanged || isUsernameTaken,
      }}
    >
      <Form
        form={form}
        layout="vertical"
        initialValues={{
          first_name: user.first_name,
          last_name: user.last_name,
          username: user.username,
          short_bio: user.short_bio,
          city: user.city,
          links: user.links,
        }}
        id={EDIT_USER_PROFILE_FORM}
        onFinish={handleFinish}
        onValuesChange={checkIfFormChanged}
      >
        <Form.Item
          name="first_name"
          label={t('firstName')}
          rules={[{ required: true, message: t('error.requiredField') }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="last_name"
          label={t('lastName')}
          rules={[{ required: true, message: t('error.requiredField') }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="username"
          label={t('userName')}
          rules={[
            { required: true, message: t('error.requiredField') },
            { max: 30, message: t('error.valueIsTooLong') },
          ]}
          validateStatus={isUsernameTaken ? 'error' : undefined}
          help={isUsernameTaken ? t('error.usernameIsTaken') : undefined}
          initialValue={user.username}
        >
          <Input
            prefix="@"
            onKeyDown={(e) => {
              if (e.key === ' ') {
                e.preventDefault();
              }
            }}
            onChange={() => setIsUsernameTaken(false)}
            onBlur={handleUsernameChange}
          />
        </Form.Item>
        <Form.Item name="short_bio" label={t('headline')}>
          <TextArea rows={4} maxLength={150} />
        </Form.Item>
        <Form.Item name="city" label={t('location')}>
          <LocationSearch
            onLocationSelect={(place) => {
              const newPlace = place || '';
              form.setFieldsValue({ city: newPlace });
              checkIfFormChanged({}, { ...form.getFieldsValue(), city: newPlace });
            }}
            existingLocation={user.city}
          />
        </Form.Item>

        <Form.Item
          label={t('link.other')}
          name="externalWebsiteUrl"
          rules={[
            {
              pattern: /^(https?:\/\/)/,
              message: t('error.invalidLink'),
            },
          ]}
        >
          <Flex gap="small" justify="space-between">
            <Input
              width={'100%'}
              placeholder="https://www.facebook.com/justonegiantlab/"
              onChange={(e) => {
                const value = e.target.value;
                setExternalWebsiteLink({ ...externalWebsiteLink, url: value });
                form.setFieldsValue({ externalWebsiteUrl: value });
                form.validateFields(['externalWebsiteUrl']);
              }}
              value={externalWebsiteLink.url}
            />
            <Button
              onClick={handleExternalWebsiteLinkAdd}
              icon={<PlusOutlined />}
              disabled={!externalWebsiteLink.url || !!form.getFieldError('externalWebsiteUrl').length}
            />
          </Flex>
        </Form.Item>

        <Flex vertical gap="small">
          {externalWebsiteLinks.map((link, index) => (
            <FlexStyled gap="middle" justify="space-between" key={`${link.url}${index}`}>
              <Flex style={{ width: '80%' }} align="center">
                <UserLink link={link} />
                <Typography.Text ellipsis={{ tooltip: link.url }}>{link.url}</Typography.Text>
              </Flex>
              <Button type="text" onClick={() => handleExternalWebsiteLinkRemove(index)} icon={<CloseOutlined />} />
            </FlexStyled>
          ))}
        </Flex>
      </Form>
    </Modal>
  );
};
