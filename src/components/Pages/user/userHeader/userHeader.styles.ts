import { Avatar, Button, Flex, Typography } from 'antd';
import styled from '@emotion/styled';

export const FlexStyled = styled(Flex)`
  position: relative;
`;

export const ImageWrapper = styled.div`
  position: relative;
  width: 100%;
  height: 236px;
`;

export const ContentWrapper = styled.div`
  position: relative;
  padding: ${({ theme }) => theme.token.sizeLG}px;
  padding-top: ${({ theme }) => theme.token.sizeXXL}px;
  margin-top: ${({ theme }) => theme.token.sizeXXL}px;
  padding-bottom: 0;
  @media screen and (min-width: ${({ theme }) => theme.token.screenMDMax}px) {
    max-width: 50%;
  }
`;

export const AvatarWrapper = styled.div`
  position: absolute;
  top: -130px;
  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    top: -110px;
  }
`;

export const AvatarStyled = styled(Avatar)`
  background-color: ${({ theme }) => theme.token.neutral6};
`;

export const TitleStyled = styled(Typography.Title)`
  margin-bottom: ${({ theme }) => theme.token.sizeXXS}px !important;
`;

export const ButtonStyled = styled(Button)`
  position: absolute;
  top: 270px;
  right: 8px;
`;

export const UploadWrapperStyled = styled.span`
  position: absolute;
  bottom: 8px;
  right: 8px;
`;

export const ExternalLinksContainer = styled.div`
  button:first-of-type {
    padding-left: 0;
  }
`;
