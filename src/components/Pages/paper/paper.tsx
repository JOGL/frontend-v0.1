import useTranslation from 'next-translate/useTranslation';
import React, { useEffect, useState } from 'react';
import { PaperModel, PaperUpsertModel } from '~/__generated__/types';
import Layout from '~/src/components/Layout/layout/layout';
import {
  BadgeStyled,
  ContainerStyled,
  FlexStyled,
  LinkButtonStyled,
  ReadOnlyWrapperStyled,
  TagStyled,
  TypographyTitleStyled,
} from './paper.styled';
import { Flex, Space, Tag, Typography, TabsProps, theme, message, Tooltip, Grid } from 'antd';
import { displayObjectDate, isValidHttpUrl } from '~/src/utils/utils';
import { useRouter } from 'next/router';
import { CommentOutlined, GlobalOutlined, ReadOutlined } from '@ant-design/icons';
import ReadOnlyEditor from '../../tiptap/readOnlyEditor/readOnlyEditor';
import Feed from '~/src/components/discussionFeed/feed/feed';
import Share from '../../Common/shareModal/share';
import { useMutation, useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { AxiosError } from 'axios';
import Loading from '../../Tools/Loading';
import { useFeedEntity } from '../../Common/feedEntity/feedEntity.hooks';
import useLogin from '~/src/hooks/useLogin';
const { useBreakpoint } = Grid;
import { TabsStyled } from '~/src/components/discussionFeed/feed/feed.styles';

const Paper = () => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const [, , handle403] = useLogin();
  const paperId = router.query.id as string;
  const { token } = theme.useToken();
  const [tab, setTab] = useState<string>((router.query.tab as string) ?? 'page');
  const [markFeedAsOpened] = useFeedEntity();
  const screens = useBreakpoint();

  const { data: paper, refetch, error } = useQuery({
    queryKey: [QUERY_KEYS.papersDetailDetail, paperId],
    queryFn: async () => {
      const response = await api.papers.papersDetailDetail(paperId);
      return response.data;
    },
    retry: (failureCount, error: AxiosError) => {
      if (error.response?.status === 404 || error.response?.status === 403) {
        return false;
      }

      return failureCount < 3;
    },

    enabled: !!paperId,
  });

  useEffect(() => {
    if (paper) markFeedAsOpened(paper);
  }, [paper]);

  const mutation = useMutation({
    mutationFn: async (paperData: PaperUpsertModel) => {
      return await api.papers.papersUpdateDetail(paperId, paperData);
    },
    onSuccess: () => {
      refetch();
      message.success(t('paperWasSaved'));
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  if (error?.response?.status === 403) {
    handle403();
    return <></>;
  }

  if (!paper) {
    return <Loading />;
  }

  const authors = paper.authors.split(',');
  const moreAuthors = authors.slice(3);
  const moreTags = paper.tags.slice(3);

  const handleTabChange = (tab: string) => {
    setTab(tab);
    router.replace(
      {
        query: { ...router.query, tab: tab },
      },
      undefined,
      { shallow: true }
    );
  };

  const badgeCount = (paper?.feed_stats?.new_thread_activity_count ?? 0) + (paper?.feed_stats?.new_mention_count ?? 0);
  const items: TabsProps['items'] = [
    {
      key: 'page',
      label: (
        <Flex gap="small" align="center">
          <ReadOutlined />
          {t('abstract')}
        </Flex>
      ),
      children: (
        <ReadOnlyWrapperStyled>
          <ReadOnlyEditor content={paper.summary ?? ''} />
        </ReadOnlyWrapperStyled>
      ),
    },

    {
      key: 'discussion',
      label: (
        <Flex gap="small" align="center">
          <BadgeStyled count={badgeCount} size="small" offset={[4, -4]} color={token.colorPrimary}>
            <Space>
              <CommentOutlined />
              {t('discussion.one')}
            </Space>
          </BadgeStyled>
        </Flex>
      ),
      children: <Feed feedId={paper?.id} />,
    },
  ];

  const formatData = (data: PaperModel) => {
    const userVisibility = data.user_visibility?.map((item) => {
      return {
        visibility: item.visibility,
        user_id: item.user.id,
      };
    });

    const communityEntityVisibility = data.communityentity_visibility?.map((item) => {
      return {
        visibility: item.visibility,
        community_entity_id: item.community_entity?.id,
      };
    });

    return {
      user_visibility: userVisibility,
      communityentity_visibility: communityEntityVisibility,
      default_visibility: data.default_visibility,
    };
  };

  const handleShare = (data: PaperModel) => {
    mutation.mutate({
      ...paper,
      ...formatData(data),
    });
  };

  const handleAccessClick = () => {
    if (paper?.open_access_pdf) {
      window.open(paper.open_access_pdf, '_blank', 'noopener,noreferrer');
    }
  };

  return (
    <Layout title={paper?.title && `${paper?.title} | JOGL`}>
      <ContainerStyled>
        <FlexStyled justify="space-between">
          {paper?.open_access_pdf && (
            <LinkButtonStyled type="link" onClick={handleAccessClick} icon={<GlobalOutlined />}>
              {t('openAccess')}
            </LinkButtonStyled>
          )}
          <Space>{screens.xs && <Share data={paper} handleShare={handleShare} />}</Space>
        </FlexStyled>

        <TypographyTitleStyled level={3}>
          <span dangerouslySetInnerHTML={{ __html: paper.title }} />
        </TypographyTitleStyled>
        <Flex gap="middle" justify="space-between">
          <Flex gap="small" align="center">
            <Typography.Text strong>
              {authors?.slice(0, 3)?.map((author, index) => {
                return (
                  <React.Fragment key={index} strong>
                    {author}
                    {index < 2 ? ', ' : ' '}
                  </React.Fragment>
                );
              })}
              {!!moreAuthors.length && (
                <Tooltip placement="top" title={moreAuthors.join(', ')}>
                  <Tag> +{moreAuthors.length}</Tag>
                </Tooltip>
              )}
            </Typography.Text>
          </Flex>
          <Space>{!screens.xs && <Share data={paper} handleShare={handleShare} />}</Space>
        </Flex>

        <FlexStyled vertical gap="small">
          {paper?.journal && <Typography.Text>{paper?.journal}</Typography.Text>}
          <a
            href={isValidHttpUrl(paper?.external_id) ? paper?.external_id : `https://doi.org/${paper.external_id}`}
            target="_blank"
            rel="noreferrer"
          >
            {isValidHttpUrl(paper?.external_id) ? paper?.external_id : `https://doi.org/${paper.external_id}`}
          </a>

          {!!paper?.publication_date && (
            <Typography.Text type="secondary">{displayObjectDate(paper?.publication_date, 'LL', true)}</Typography.Text>
          )}
        </FlexStyled>
        {paper.tags?.slice(0, 3)?.map((tag, index) => {
          return <TagStyled key={index}> {tag}</TagStyled>;
        })}

        {!!moreTags.length && (
          <Tooltip placement="top" title={moreTags.join(', ')}>
            <TagStyled> +{moreTags.length}</TagStyled>
          </Tooltip>
        )}
        <TabsStyled activeKey={tab} items={items} onChange={handleTabChange} />
      </ContainerStyled>
    </Layout>
  );
};

export default Paper;
