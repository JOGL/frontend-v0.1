import styled from '@emotion/styled';
import { Badge, Flex, Tag, Typography } from 'antd';
import { Button } from 'antd';

export const ContainerStyled = styled.div`
  padding: ${({ theme }) => theme.token.paddingXL}px;
  height: 100%;
  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    padding: ${({ theme }) => theme.token.paddingXXS}px;
  }
`;

export const FlexStyled = styled(Flex)`
  margin-top: ${({ theme }) => theme.token.paddingXS}px;
  margin-bottom: ${({ theme }) => theme.token.paddingXS}px;
`;

export const ReadOnlyWrapperStyled = styled.div`
  border: 1px solid ${({ theme }) => theme.token.colorBorder};
  border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;
  height: calc(100vh - 330px);
  padding: ${({ theme }) => theme.token.paddingLG}px;
  background-color: ${({ theme }) => theme.token.neutral2};
  img {
    max-width: 100%;
  }
`;

export const TypographyTitleStyled = styled(Typography.Title)`
  cursor: pointer;
  &:hover {
    color: ${({ theme }) => theme.token.colorTextLabel};
  }
`;

export const BadgeStyled = styled(Badge)`
  margin-right: ${({ theme }) => theme.token.paddingMD}px;
`;

export const LinkButtonStyled = styled(Button)`
  padding: 0;
`;
export const TagStyled = styled(Tag)`
  margin-top: ${({ theme }) => theme.token.marginXS}px;
`;
