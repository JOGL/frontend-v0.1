import { Button, Empty, Spin } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { ButtonLoadMoreWrapper, GridStyled } from '../searchResults.styles';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { useInfiniteQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import HubCard from '~/src/components/Hub/HubCard';

import {
  getContainerAccessLevelChip,
  getContainerJoiningRestrictionChip,
  getContainerVisibilityChip,
} from '~/src/utils/utils';
import Loader from '~/src/components/Common/loader/loader';
import { SortKey } from '~/__generated__/types';

const PAGE_SIZE = 20;

interface Props {
  search: string;
}

const Hubs = ({ search }: Props) => {
  const { t } = useTranslation('common');

  const { data, isPending, hasNextPage, fetchNextPage, isFetchingNextPage } = useInfiniteQuery({
    queryKey: [QUERY_KEYS.globalNodesSearch, { search }],
    queryFn: async ({ pageParam = 1 }) => {
      const response = await api.nodes.nodesList({
        Search: search,
        PageSize: PAGE_SIZE,
        Page: pageParam,
        SortKey: SortKey.Relevance,
      });
      return response.data;
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItemsCount = allPages.reduce((acc, page) => acc + page.items.length, 0);
      return loadedItemsCount < lastPage.total ? allPages.length + 1 : undefined;
    },
  });
  const hubs =
    data?.pages.flatMap((page) => {
      return page.items;
    }) || [];

  if (isPending && !data) return <Loader />;
  if (!hubs || !hubs.length) return <Empty />;

  return (
    <Spin spinning={isPending}>
      <GridStyled>
        {hubs.map((hub) => (
          <HubCard
            key={`hubs-${hub.id}`}
            hub={hub}
            id={hub.id}
            title={hub.title}
            short_title={hub.short_title}
            short_description={hub.short_description}
            members_count={hub.stats.members_count}
            banner_url={hub.banner_url || hub.banner_url_sm}
            status={hub.status}
            chip={hub.user_access_level ? getContainerAccessLevelChip(hub) : undefined}
            imageChips={
              !hub?.user_access_level
                ? [getContainerVisibilityChip(hub), getContainerJoiningRestrictionChip(hub)]
                : [getContainerVisibilityChip(hub)]
            }
            last_activity={hub.updated || ''}
          />
        ))}
        {hasNextPage && (
          <ButtonLoadMoreWrapper>
            <Button type="primary" loading={isFetchingNextPage} onClick={() => fetchNextPage()}>
              {t('action.load')}
            </Button>
          </ButtonLoadMoreWrapper>
        )}
      </GridStyled>
    </Spin>
  );
};

export default Hubs;
