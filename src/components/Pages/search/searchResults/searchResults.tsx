import {
  AppstoreAddOutlined,
  BankOutlined,
  CalendarOutlined,
  CoffeeOutlined,
  UsergroupAddOutlined,
} from '@ant-design/icons';
import { Empty, Space } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import type { TabsProps } from 'antd';
import { useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import People from './people/people';
import { TabsStyled } from './searchResults.styles';
import Events from './events/events';
import Needs from './needs/needs';
import Hubs from './hubs/hubs';
import Organizations from './organizations/organizations';

interface Props {
  search: string;
}

const SearchResults = ({ search }: Props) => {
  const { t } = useTranslation('common');

  const { data, isPending } = useQuery({
    queryKey: [QUERY_KEYS.globalEntitySearchCount, { search }],
    queryFn: async () => {
      const response = await api.entities.searchTotalsList({
        Search: search,
      });
      return response.data;
    },
  });

  const items: TabsProps['items'] = [
    {
      key: 'people',
      label: (
        <Space>
          <UsergroupAddOutlined />
          {t('person.other')}
          {!isPending && `(${data?.user_count})`}
        </Space>
      ),
      children: data?.user_count ? <People search={search} /> : <Empty />,
    },
    {
      key: 'hubs',
      label: (
        <Space>
          <BankOutlined />
          {t('hub.other')}
          {!isPending && `(${data?.node_count})`}
        </Space>
      ),
      children: data?.node_count ? <Hubs search={search} /> : <Empty />,
    },
    {
      key: 'events',
      label: (
        <Space>
          <CalendarOutlined />
          {t('event.other')}
          {!isPending && `(${data?.event_count})`}
        </Space>
      ),
      children: data?.event_count ? <Events search={search} /> : <Empty />,
    },

    {
      key: 'needs',
      label: (
        <Space>
          <AppstoreAddOutlined />
          {t('need.other')}
          {!isPending && `(${data?.need_count})`}
        </Space>
      ),
      children: data?.need_count ? <Needs search={search} /> : <Empty />,
    },
    // {
    //   key: 'organizations',
    //   label: (
    //     <Space>
    //       <CoffeeOutlined />
    //       {t('organization.other')}
    //       {!isPending && `(${data?.org_count})`}
    //     </Space>
    //   ),
    //   children: data?.org_count ? <Organizations search={search} /> : <Empty />,
    // },
  ];

  return <TabsStyled defaultActiveKey="1" items={items} />;
};

export default SearchResults;
