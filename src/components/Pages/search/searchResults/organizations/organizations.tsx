import { Button, Empty, Spin } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { ButtonLoadMoreWrapper, GridStyled } from '../searchResults.styles';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { useInfiniteQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import OrganizationCard from '~/src/components/Organization/OrganizationCard';
import {
  getContainerAccessLevelChip,
  getContainerJoiningRestrictionChip,
  getContainerVisibilityChip,
} from '~/src/utils/utils';
import Loader from '~/src/components/Common/loader/loader';
import { SortKey } from '~/__generated__/types';

const PAGE_SIZE = 20;

interface Props {
  search: string;
}

const Organizations = ({ search }: Props) => {
  const { t } = useTranslation('common');

  const { data, isPending, hasNextPage, fetchNextPage, isFetchingNextPage } = useInfiniteQuery({
    queryKey: [QUERY_KEYS.globalOrganizationsSearch, { search }],
    queryFn: async ({ pageParam = 1 }) => {
      const response = await api.organizations.organizationsList({
        Search: search,
        PageSize: PAGE_SIZE,
        Page: pageParam,
        SortKey: SortKey.Relevance,
      });
      return response.data;
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItemsCount = allPages.reduce((acc, page) => acc + page.items.length, 0);
      return loadedItemsCount < lastPage.total ? allPages.length + 1 : undefined;
    },
  });
  const organizations =
    data?.pages.flatMap((page) => {
      return page.items;
    }) || [];

  if (isPending && !data) return <Loader />;
  if (!organizations || !organizations.length) return <Empty />;

  return (
    <Spin spinning={isPending}>
      <GridStyled>
        {organizations.map((organization) => (
          <OrganizationCard
            key={`organizations-${organization.id}`}
            organization={organization}
            id={organization.id}
            title={organization.title}
            short_title={organization.short_title}
            short_description={organization.short_description}
            members_count={organization.stats.members_count}
            banner_url={organization.banner_url || organization.banner_url_sm}
            status={organization.status}
            chip={organization.user_access_level ? getContainerAccessLevelChip(organization) : undefined}
            imageChips={
              !organization?.user_access_level
                ? [getContainerVisibilityChip(organization), getContainerJoiningRestrictionChip(organization)]
                : [getContainerVisibilityChip(organization)]
            }
            last_activity={organization.updated || ''}
          />
        ))}
        {hasNextPage && (
          <ButtonLoadMoreWrapper>
            <Button type="primary" loading={isFetchingNextPage} onClick={() => fetchNextPage()}>
              {t('action.load')}
            </Button>
          </ButtonLoadMoreWrapper>
        )}
      </GridStyled>
    </Spin>
  );
};

export default Organizations;
