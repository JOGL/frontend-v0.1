import useTranslation from 'next-translate/useTranslation';
import Layout from '~/src/components/Layout/layout/layout';
import SearchResults from './searchResults/searchResults';
import { useState } from 'react';
import useDebounce from '~/src/hooks/useDebounce';
import { SearchStyled, WrapperStyled, SearchWrapperStyled } from './search.styles';
import { SearchOutlined } from '@ant-design/icons';
import { Typography } from 'antd';
import { useRouter } from 'next/router';

const Search = () => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const [search, setSearch] = useState<string>(router.query.search as string);
  const debouncedSearch = useDebounce(search, 300);

  return (
    <Layout title={'Search'} desc={'Search on JOGL'}>
      <WrapperStyled vertical gap="middle" align="center">
        <SearchStyled
          value={search}
          placeholder={t('action.search')}
          onChange={(e) => {
            setSearch(e.target.value);
          }}
          onSearch={(value) => {
            setSearch(value);
          }}
        />
        {!debouncedSearch ? (
          <SearchWrapperStyled vertical gap="large" align="center">
            <SearchOutlined />
            <Typography.Text type="secondary">{t('searchGlobalPlaceholder')}</Typography.Text>
          </SearchWrapperStyled>
        ) : (
          <SearchResults search={debouncedSearch} />
        )}
      </WrapperStyled>
    </Layout>
  );
};

export default Search;
