import styled from '@emotion/styled';
import { Flex, Input } from 'antd';

export const WrapperStyled = styled(Flex)`
  padding: ${({ theme }) => theme.token.sizeLG}px;
  max-width: calc(4 * 240px + 3 * 20px + 2 * 24px); // max-width fits 4 cards with 20px gap and 24px ppadding
  margin: auto;
`;

export const SearchStyled = styled(Input.Search)`
  max-width: 500px;
`;

export const SearchWrapperStyled = styled(Flex)`
  padding: ${({ theme }) => theme.token.sizeLG}px;
  text-align: center;
  & svg {
    font-size: 56px;
    color: ${({ theme }) => theme.token.colorTextSecondary};
  }
`;
