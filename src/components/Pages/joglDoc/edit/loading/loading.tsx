import { Flex, Spin } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import Layout from '~/src/components/Layout/layout/layout';

const Loading = () => {
  const { t } = useTranslation('common');
  return (
    <Layout title={t('page')}>
      <Flex style={{ width: '100%', height: '100%' }} justify="center" align="center">
        <Spin />
      </Flex>
    </Layout>
  );
};

export default Loading;
