import { useEffect } from 'react';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { useMutation, useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { message } from 'antd';
import { useStonePathStore } from '~/src/contexts/stonepath/StonePathStoreProvider';
import JoglDocContent from '../joglDocContent/joglDocContent';
import { DocumentModel, DocumentUpdateModel, Permission } from '~/__generated__/types';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import Loading from './loading/loading';
import { AxiosError } from 'axios';
import useLogin from '~/src/hooks/useLogin';

const JoglDocEdit = () => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const [, , handle403] = useLogin();
  const docId = router.query.id as string;

  const { data: doc, error, refetch } = useQuery({
    queryKey: [QUERY_KEYS.documentDetails, docId],
    queryFn: async () => {
      const response = await api.documents.documentsDetailDetail(docId);
      return response.data;
    },
    retry: (failureCount, error: AxiosError) => {
      if (error.response?.status === 404 || error.response?.status === 403) {
        return false;
      }

      return failureCount < 3;
    },

    enabled: !!docId,
  });

  const { setStonePath } = useStonePathStore();
  useEffect(() => {
    doc && setStonePath(doc.path);
    return () => setStonePath([]);
  }, [doc, setStonePath]);

  const mutation = useMutation({
    mutationFn: async (docData: DocumentUpdateModel) => {
      await api.documents.documentsUpdateDetail(docId, docData);
    },
    onSuccess: () => {
      refetch();
      message.success(t('documentWasSaved'));
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  if (error?.response?.status === 403) {
    handle403();
    return <></>;
  }

  if (!doc) {
    return <Loading />;
  }

  const formatData = (data: DocumentModel | DocumentUpdateModel) => {
    const userVisibility = data.user_visibility?.map((item) => {
      return {
        visibility: item.visibility,
        user_id: item.user.id,
      };
    });

    const communityEntityVisibility = data.communityentity_visibility?.map((item) => {
      return {
        visibility: item.visibility,
        community_entity_id: item.community_entity.id,
      };
    });

    return {
      user_visibility: userVisibility,
      communityentity_visibility: communityEntityVisibility,
      default_visibility: data.default_visibility,
    };
  };

  const saveShare = (data: DocumentModel) => {
    mutation.mutate({ ...doc, ...formatData(data) });
  };

  const saveChanges = (data: DocumentUpdateModel) => {
    if (
      doc.title !== data?.title ||
      doc.description !== data.description ||
      doc.keywords?.join() !== data.keywords?.join()
    ) {
      mutation.mutate({
        ...doc,
        ...data,
        ...formatData(doc),
      });
    }
  };

  const canEdit = doc?.permissions?.includes(Permission.Manage);

  return <JoglDocContent canEdit={canEdit} doc={doc} handleChanges={saveChanges} handleShare={saveShare} />;
};
export default JoglDocEdit;
