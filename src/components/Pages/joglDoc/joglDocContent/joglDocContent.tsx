import { FC, useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { Flex, Space, TabsProps, Typography, theme } from 'antd';
import Layout from '~/src/components/Layout/layout/layout';
import { CommentOutlined, FileTextOutlined, FolderOutlined } from '@ant-design/icons';
import { BadgeStyled, ContainerStyled, InputStyled, TypographyTitleStyled } from './joglDocContent.styles';
import useOnClickOutside from '~/src/hooks/useOnClickOutside';
import { displayObjectRelativeDate } from '~/src/utils/utils';
import { DocumentModel, DocumentType, DocumentUpdateModel, Permission } from '~/__generated__/types';
import Feed from '~/src/components/discussionFeed/feed/feed';
import Share from '~/src/components/Common/shareModal/share';
import KeyWordsModal from '../../../Common/keyWordsModal/keyWordsModal';
import DescriptionEditor from '~/src/components/Common/descriptionEditor/descriptionEditor';
import { useFeedEntity } from '~/src/components/Common/feedEntity/feedEntity.hooks';
import EntityDocumentListSimple from '~/src/components/documents/entityDocumentListSimple/entityDocumentListSimple';
import { TabsStyled } from '~/src/components/discussionFeed/feed/feed.styles';

interface JoglDocContentProps {
  canEdit: boolean;
  doc?: DocumentModel;
  handleChanges: (data: DocumentUpdateModel) => void;
  handleShare?: (data: DocumentModel) => void;
}

const JoglDocContent: FC<JoglDocContentProps> = ({ canEdit, doc, handleChanges, handleShare }) => {
  const { t } = useTranslation('common');
  const { token } = theme.useToken();
  const { locale } = useRouter();
  const inputRef = useRef(null);
  const [showTitleEditor, setShowTitleEditor] = useState(false);
  const router = useRouter();
  const [tab, setTab] = useState<string>((router.query.tab as string) ?? 'page');
  const [markFeedAsOpened] = useFeedEntity();

  const [docData, setData] = useState<DocumentUpdateModel>({
    title: doc?.title || t('untitledPage'),
    type: DocumentType.Jogldoc,
    description: doc?.description,
    keywords: doc?.keywords,
  });

  useEffect(() => {
    if (doc) markFeedAsOpened(doc);
  }, [doc]);

  const handleTabChange = (tab: string) => {
    setTab(tab);
    router.replace(
      {
        query: { ...router.query, tab: tab },
      },
      undefined,
      { shallow: true }
    );
  };

  const handleTitleChange = (e) => {
    setData({ ...docData, title: e.target.value });
  };

  const handleClickOutsideTitle = () => {
    setShowTitleEditor(false);
  };

  useOnClickOutside(inputRef, handleClickOutsideTitle);

  const handleKeywordChange = (keywords: string[]) => {
    const updateData = { ...docData, keywords };
    setData(updateData);
    handleChanges(updateData);
  };

  const badgeCount = (doc?.feed_stats?.new_thread_activity_count ?? 0) + (doc?.feed_stats?.new_mention_count ?? 0);
  const items: TabsProps['items'] = [
    {
      key: 'page',
      label: (
        <Flex gap="small" align="center">
          <FileTextOutlined />
          {t('page')}
        </Flex>
      ),
      children: (
        <DescriptionEditor
          data={docData}
          setData={setData}
          handleClickOutside={() => handleChanges(docData)}
          canEdit={canEdit}
        />
      ),
    },
    {
      key: 'files',
      disabled: !doc?.id,
      label: (
        <Flex gap="small" align="center">
          <FolderOutlined />
          {t('file.other')}
        </Flex>
      ),
      children: doc ? (
        <EntityDocumentListSimple
          entityId={doc.id}
          canManageDocuments={doc.permissions.includes(Permission.Managedocuments)}
        />
      ) : null,
    },
    {
      key: 'discussion',
      disabled: !doc?.id,
      label: (
        <Flex gap="small" align="center">
          <BadgeStyled count={badgeCount} size="small" offset={[4, -4]} color={token.colorPrimary}>
            <Space>
              <CommentOutlined />
              {t('discussion.one')}
            </Space>
          </BadgeStyled>
        </Flex>
      ),
      children: doc ? <Feed feedId={doc?.id} /> : null,
    },
  ];

  const isShareActive = handleShare && doc;
  return (
    <Layout title={doc?.title ? `${doc.title} | JOGL Doc` : 'JOGL Doc'}>
      <ContainerStyled>
        {!showTitleEditor ? (
          <TypographyTitleStyled level={3} onClick={() => canEdit && setShowTitleEditor(true)}>
            {docData.title}
          </TypographyTitleStyled>
        ) : (
          <div ref={inputRef}>
            <InputStyled size="large" value={docData.title} onChange={handleTitleChange} />
          </div>
        )}

        <Flex gap="middle" justify="space-between">
          <Flex vertical gap="small">
            <KeyWordsModal keywords={docData?.keywords || []} canEdit={canEdit} saveChanges={handleKeywordChange} />

            <Typography.Text type="secondary">
              {!!doc?.last_activity &&
                `${t('lastEdited')}: ${displayObjectRelativeDate(doc?.last_activity, locale ?? 'en', false, false)}`}
            </Typography.Text>
          </Flex>
          <Space>{isShareActive && <Share data={doc} handleShare={handleShare} />}</Space>
        </Flex>

        <TabsStyled activeKey={tab} items={items} onChange={handleTabChange} />
      </ContainerStyled>
    </Layout>
  );
};
export default JoglDocContent;
