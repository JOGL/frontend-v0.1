import {
  AppstoreAddOutlined,
  FileTextOutlined,
  CalendarOutlined,
  UsergroupAddOutlined,
  FilePdfOutlined,
} from '@ant-design/icons';
import { Empty, Space } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import type { TabsProps } from 'antd';
import { useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import People from './people/people';
import { TabsStyled } from './searchResults.styles';
import Events from './events/events';
import Needs from './needs/needs';
import Papers from './papers/papers';
import Documents from './documents/documents';

interface Props {
  hubId: string;
  search: string;
}

const SearchResults = ({ hubId, search }: Props) => {
  const { t } = useTranslation('common');

  const { data, isPending } = useQuery({
    queryKey: [QUERY_KEYS.nodeEntitySearchCount, { search }],
    queryFn: async () => {
      const response = await api.nodes.searchTotalsDetail(hubId, {
        Search: search,
      });
      return response.data;
    },
  });

  const items: TabsProps['items'] = [
    {
      key: 'people',
      label: (
        <Space>
          <UsergroupAddOutlined />
          {t('person.other')}
          {!isPending && `(${data?.user_count})`}
        </Space>
      ),
      children: data?.user_count ? <People search={search} hubId={hubId} /> : <Empty />,
    },
    {
      key: 'events',
      label: (
        <Space>
          <CalendarOutlined />
          {t('event.other')}
          {!isPending && `(${data?.event_count})`}
        </Space>
      ),
      children: data?.event_count ? <Events search={search} hubId={hubId} /> : <Empty />,
    },
    {
      key: 'papers',
      label: (
        <Space>
          <FileTextOutlined />
          {t('paper.other')}
          {!isPending && `(${data?.paper_count})`}
        </Space>
      ),
      children: data?.paper_count ? <Papers search={search} hubId={hubId} /> : <Empty />,
    },
    {
      key: 'documents',
      label: (
        <Space>
          <FilePdfOutlined />
          {t('document.other')}
          {!isPending && `(${data?.doc_count})`}
        </Space>
      ),
      children: data?.doc_count ? <Documents search={search} hubId={hubId} /> : <Empty />,
    },
    {
      key: 'needs',
      label: (
        <Space>
          <AppstoreAddOutlined />
          {t('need.other')}
          {!isPending && `(${data?.need_count})`}
        </Space>
      ),
      children: data?.need_count ? <Needs search={search} hubId={hubId} /> : <Empty />,
    },
  ];

  return <TabsStyled defaultActiveKey="1" items={items} />;
};

export default SearchResults;
