import { Button, Empty, Spin } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import UserCard from '~/src/components/User/UserCard';
import { ButtonLoadMoreWrapper, GridStyled } from '../searchResults.styles';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { useInfiniteQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import Loader from '~/src/components/Common/loader/loader';
import { SortKey } from '~/__generated__/types';

const PAGE_SIZE = 20;

interface Props {
  hubId: string;
  search: string;
}

const People = ({ hubId, search }: Props) => {
  const { t } = useTranslation('common');

  const { data, isPending, hasNextPage, fetchNextPage, isFetchingNextPage } = useInfiniteQuery({
    queryKey: [QUERY_KEYS.nodeUsersSearch, { search }],
    queryFn: async ({ pageParam = 1 }) => {
      const response = await api.nodes.usersAggregateDetail(hubId, {
        Search: search,
        PageSize: PAGE_SIZE,
        Page: pageParam,
        SortKey: SortKey.Relevance,
      });
      return response.data;
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItemsCount = allPages.reduce((acc, page) => acc + page.items.length, 0);
      return loadedItemsCount < lastPage.total ? allPages.length + 1 : undefined;
    },
  });

  const people =
    data?.pages.flatMap((page) => {
      return page.items;
    }) || [];

  if (isPending && !data) return <Loader />;
  if (!people || !people.length) return <Empty />;

  return (
    <Spin spinning={isPending}>
      <GridStyled>
        {people.map((user) => (
          <UserCard key={`users-${user.id}`} user={user} />
        ))}
      </GridStyled>
      {hasNextPage && (
        <ButtonLoadMoreWrapper>
          <Button type="primary" loading={isFetchingNextPage} onClick={() => fetchNextPage()}>
            {t('action.load')}
          </Button>
        </ButtonLoadMoreWrapper>
      )}
    </Spin>
  );
};

export default People;
