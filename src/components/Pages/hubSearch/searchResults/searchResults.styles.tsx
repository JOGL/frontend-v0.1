import styled from '@emotion/styled';
import { Spin, Tabs, Flex } from 'antd';

export const TabsStyled = styled(Tabs)`
  width: 100%;
`;

export const SpinStyled = styled(Spin)`
  .ant-spin-nested-loading {
    width: 100% !important;
  }
`;

export const GridStyled = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: repeat(auto-fill, 240px);
  align-items: center;
  grid-gap: ${({ theme }) => theme.token.sizeMD}px;
`;

export const ButtonLoadMoreWrapper = styled(Flex)`
  padding: ${({ theme }) => theme.token.sizeMD}px;
  justify-content: center;
`;
