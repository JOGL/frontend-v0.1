import { Button, Empty, Flex, Select, Spin } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { ButtonLoadMoreWrapper, GridStyled } from '../searchResults.styles';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { useInfiniteQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import NeedCard from '~/src/components/Need/NeedCard';
import Loader from '~/src/components/Common/loader/loader';
import { useState } from 'react';
import { SortKey } from '~/__generated__/types';

const PAGE_SIZE = 20;

interface Props {
  hubId: string;
  search: string;
}

const Needs = ({ hubId, search }: Props) => {
  const { t } = useTranslation('common');
  const [sortKey, setSortKey] = useState(SortKey.Relevance);

  const { data, isPending, hasNextPage, fetchNextPage, isFetchingNextPage } = useInfiniteQuery({
    queryKey: [QUERY_KEYS.nodeNeedsSearch, { search, sortKey }],
    queryFn: async ({ pageParam = 1 }) => {
      const response = await api.nodes.needsAggregateDetail(hubId, {
        Search: search,
        PageSize: PAGE_SIZE,
        Page: pageParam,
        SortKey: sortKey,
      });
      return response.data;
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItemsCount = allPages.reduce((acc, page) => acc + page.items.length, 0);
      return loadedItemsCount < lastPage.total ? allPages.length + 1 : undefined;
    },
  });

  const needs =
    data?.pages.flatMap((page) => {
      return page.items;
    }) || [];

  if (isPending && !data) return <Loader />;
  if (!needs || !needs.length) return <Empty />;

  return (
    <Spin spinning={isPending}>
      <Flex vertical gap="middle">
        {needs.length > 1 && (
          <Select
            value={sortKey}
            style={{ width: 150 }}
            options={[
              { label: t('mostRelevant'), value: SortKey.Relevance },
              { label: t('lastAdded'), value: SortKey.Createddate },
              { label: t('dueDate'), value: SortKey.Date },
            ]}
            onChange={(key) => {
              setSortKey(key);
            }}
          />
        )}
        <GridStyled>
          {needs.map((need) => (
            <NeedCard key={`needs-${need.id}`} need={need} />
          ))}
        </GridStyled>
      </Flex>
      {hasNextPage && (
        <ButtonLoadMoreWrapper>
          <Button type="primary" loading={isFetchingNextPage} onClick={() => fetchNextPage()}>
            {t('action.load')}
          </Button>
        </ButtonLoadMoreWrapper>
      )}
    </Spin>
  );
};

export default Needs;
