import { Button, Empty, Flex, Select, Spin } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { ButtonLoadMoreWrapper, GridStyled } from '../searchResults.styles';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { useInfiniteQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';

import Loader from '~/src/components/Common/loader/loader';
import { SortKey } from '~/__generated__/types';
import { useState } from 'react';
import DocumentCardInList from '~/src/components/Tools/createDocument/DocumentCardInList';

const PAGE_SIZE = 20;

interface Props {
  hubId: string;
  search: string;
}

const Documents = ({ hubId, search }: Props) => {
  const { t } = useTranslation('common');
  const [sortKey, setSortKey] = useState(SortKey.Relevance);

  const { data, isPending, hasNextPage, fetchNextPage, isFetchingNextPage } = useInfiniteQuery({
    queryKey: [QUERY_KEYS.nodeDocumentsSearch, { search, sortKey }],
    queryFn: async ({ pageParam = 1 }) => {
      const response = await api.nodes.documentsAggregateDetail(hubId, {
        Search: search,
        PageSize: PAGE_SIZE,
        Page: pageParam,
        SortKey: sortKey,
      });
      return response.data;
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItemsCount = allPages.reduce((acc, page) => acc + page.items.length, 0);
      return loadedItemsCount < lastPage.total ? allPages.length + 1 : undefined;
    },
  });
  const documents =
    data?.pages.flatMap((page) => {
      return page.items;
    }) || [];

  if (isPending && !data) return <Loader />;
  if (!documents || !documents.length) return <Empty />;

  return (
    <Spin spinning={isPending}>
      <Flex vertical gap="middle">
        {documents.length > 1 && (
          <Select
            value={sortKey}
            style={{ width: 150 }}
            options={[
              { label: t('mostRelevant'), value: SortKey.Relevance },
              { label: t('lastAdded'), value: SortKey.Createddate },
              { label: t('publicationDate'), value: SortKey.Date },
            ]}
            onChange={(key) => {
              setSortKey(key);
            }}
          />
        )}
        <GridStyled>
          {documents.map((document) => (
            <DocumentCardInList
              key={`documents-${document.id}`}
              document={document}
              type={document?.type}
              itemType={document.type}
            />
          ))}
        </GridStyled>
      </Flex>

      {hasNextPage && (
        <ButtonLoadMoreWrapper>
          <Button type="primary" loading={isFetchingNextPage} onClick={() => fetchNextPage()}>
            {t('action.load')}
          </Button>
        </ButtonLoadMoreWrapper>
      )}
    </Spin>
  );
};

export default Documents;
