import useTranslation from 'next-translate/useTranslation';
import { NodeDetailModel } from '~/__generated__/types';
import Layout from '~/src/components/Layout/layout/layout';
import SearchResults from './searchResults/searchResults';
import { useEffect, useState } from 'react';
import useDebounce from '~/src/hooks/useDebounce';
import { SearchStyled, SearchWrapperStyled, WrapperStyled } from './hubSearch.styles';
import { useStonePathStore } from '~/src/contexts/stonepath/StonePathStoreProvider';
import { SearchOutlined } from '@ant-design/icons';
import { Typography } from 'antd';

interface Props {
  hub: NodeDetailModel;
  initial: string;
}

const HubSearch = ({ hub, initial }: Props) => {
  const { t } = useTranslation('common');
  const [search, setSearch] = useState<string>(initial);
  const debouncedSearch = useDebounce(search, 300);
  const { setStonePath } = useStonePathStore();

  useEffect(() => {
    setStonePath(hub.path);

    return () => setStonePath([]);
  }, [hub, setStonePath]);

  return (
    <Layout
      title={hub?.title && `${hub.title} | JOGL`}
      desc={hub?.short_description}
      img={hub?.banner_url || '/images/default/default-workspace.png'}
      noIndex={hub?.status === 'draft' || hub?.listing_privacy !== 'public'}
    >
      {initial}
      <WrapperStyled vertical gap="middle" align="center">
        <SearchStyled
          value={search}
          placeholder={t('searchHub', { hubName: hub.title })}
          onChange={(e) => {
            setSearch(e.target.value);
          }}
          onSearch={(value) => {
            setSearch(value);
          }}
        />
        {!debouncedSearch ? (
          <SearchWrapperStyled vertical gap="large" align="center">
            <SearchOutlined />
            <Typography.Text type="secondary">{t('searchHubDescription')}</Typography.Text>
          </SearchWrapperStyled>
        ) : (
          <SearchResults search={debouncedSearch} hubId={hub.id} />
        )}
      </WrapperStyled>
    </Layout>
  );
};

export default HubSearch;
