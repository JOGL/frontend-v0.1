import styled from '@emotion/styled';
import { Badge, Input, Typography } from 'antd';

export const ContainerStyled = styled.div`
  padding: ${({ theme }) => theme.token.paddingXL}px;
  height: 100%;
  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    padding: ${({ theme }) => theme.token.paddingXXS}px;
  }
`;

export const InputStyled = styled(Input)`
  margin-bottom: ${({ theme }) => theme.token.paddingSM}px;
`;

export const TypographyTitleStyled = styled(Typography.Title)`
  cursor: pointer;
  &:hover {
    color: ${({ theme }) => theme.token.colorTextLabel};
  }
`;

export const BadgeStyled = styled(Badge)`
  margin-right: ${({ theme }) => theme.token.paddingMD}px;
`;
