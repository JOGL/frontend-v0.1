import { FC, useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { Avatar, DatePicker, Flex, Select, Space, TabsProps, Typography, theme, Grid } from 'antd';
import Layout from '~/src/components/Layout/layout/layout';
import { AppstoreAddOutlined, CommentOutlined, FolderOutlined, UserOutlined } from '@ant-design/icons';
import { BadgeStyled, ContainerStyled, InputStyled, TypographyTitleStyled } from './needContent.styles';
import useOnClickOutside from '~/src/hooks/useOnClickOutside';
import { NeedModel, NeedType, NeedUpsertModel, Permission } from '~/__generated__/types';
import Share from '../../../Common/shareModal/share';
import dayjs from 'dayjs';
import KeyWordsModal from '../../../Common/keyWordsModal/keyWordsModal';
import DescriptionEditor from '../../../Common/descriptionEditor/descriptionEditor';
import Feed from '~/src/components/discussionFeed/feed/feed';
import { useFeedEntity } from '~/src/components/Common/feedEntity/feedEntity.hooks';
import EntityDocumentListSimple from '~/src/components/documents/entityDocumentListSimple/entityDocumentListSimple';
const { useBreakpoint } = Grid;
import { TabsStyled } from '~/src/components/discussionFeed/feed/feed.styles';

interface NeedContentProps {
  canEdit: boolean;
  need?: NeedModel;
  handleChanges: (data: NeedUpsertModel) => void;
  handleShare?: (data: NeedModel) => void;
}

const NeedContent: FC<NeedContentProps> = ({ canEdit, need, handleChanges, handleShare }) => {
  const { t } = useTranslation('common');
  const { token } = theme.useToken();
  const inputRef = useRef(null);
  const [showTitleEditor, setShowTitleEditor] = useState(false);
  const router = useRouter();
  const [tab, setTab] = useState<string>((router.query.tab as string) ?? 'need');
  const [markFeedAsOpened] = useFeedEntity();
  const screens = useBreakpoint();
  const isMobile = screens.xs;

  const [needData, setData] = useState<NeedUpsertModel>({
    title: need?.title ?? t('untitledNeed'),
    type: need?.type ?? NeedType.Equipment,
    description: need?.description,
    keywords: need?.keywords,
    end_date: need?.end_date,
  });

  useEffect(() => {
    if (need) markFeedAsOpened(need);
  }, [need]);

  const handleTabChange = (tab: string) => {
    setTab(tab);
    router.replace(
      {
        query: { ...router.query, tab: tab },
      },
      undefined,
      { shallow: true }
    );
  };

  const handleTitleChange = (e) => {
    setData({ ...needData, title: e.target.value });
  };

  const handleClickOutsideTitle = () => {
    setShowTitleEditor(false);
  };

  useOnClickOutside(inputRef, handleClickOutsideTitle, ['.ant-select']);

  const handleKeywordChange = (keywords: string[]) => {
    const updateData = { ...needData, keywords };
    setData(updateData);
    handleChanges(updateData);
  };

  const badgeCount = (need?.feed_stats?.new_thread_activity_count ?? 0) + (need?.feed_stats?.new_mention_count ?? 0);
  const items: TabsProps['items'] = [
    {
      key: 'need',
      label: (
        <Flex gap="small" align="center">
          <AppstoreAddOutlined />
          {t('need.one')}
        </Flex>
      ),
      children: (
        <DescriptionEditor
          data={needData}
          setData={setData}
          handleClickOutside={() => handleChanges(needData)}
          canEdit={canEdit}
        />
      ),
    },
    {
      key: 'files',
      disabled: !need?.id,
      label: (
        <Flex gap="small" align="center">
          <FolderOutlined />
          {t('file.other')}
        </Flex>
      ),
      children: need ? (
        <EntityDocumentListSimple
          canManageDocuments={need.permissions.includes(Permission.Managedocuments)}
          entityId={need.id}
        />
      ) : null,
    },
    {
      key: 'discussion',
      disabled: !need?.id,
      label: (
        <Flex gap="small" align="center">
          <BadgeStyled count={badgeCount} size="small" offset={[4, -4]} color={token.colorPrimary}>
            <Space>
              <CommentOutlined />
              {t('discussion.one')}
            </Space>
          </BadgeStyled>
        </Flex>
      ),
      children: need ? <Feed feedId={need?.id} /> : null,
    },
  ];

  const isShareActive = handleShare && need;
  const needTypeOptions = Object.values(NeedType).map((type) => ({
    value: type,
    label: t(`legacy.needType.${type}`),
  }));

  return (
    <Layout title={need?.title ? `${need.title} | JOGL Need` : 'JOGL Need'}>
      <ContainerStyled>
        {!showTitleEditor ? (
          <TypographyTitleStyled level={3} onClick={() => canEdit && setShowTitleEditor(true)}>
            {needData.title}
          </TypographyTitleStyled>
        ) : (
          <div ref={inputRef}>
            <InputStyled size="large" value={needData.title} onChange={handleTitleChange} />
          </div>
        )}
        <Flex vertical gap="middle">
          <Flex gap="small" align="center">
            <Typography.Text type="secondary"> {t('type')}:</Typography.Text>
            <Select
              size="middle"
              value={needData?.type}
              style={{ width: 150 }}
              onChange={(value) => setData({ ...needData, type: value })}
              options={needTypeOptions}
            />
          </Flex>
          <KeyWordsModal keywords={needData?.keywords || []} canEdit={canEdit} saveChanges={handleKeywordChange} />
          <Flex gap="middle" vertical={isMobile} justify={need?.created_by ? 'space-between' : 'end'}>
            {need?.created_by && (
              <Space>
                <Avatar size="large" shape="square" icon={<UserOutlined />} src={need?.created_by?.logo_url} />
                <Flex vertical>
                  <Typography.Text strong>
                    {need.created_by?.first_name} {need.created_by?.last_name}
                  </Typography.Text>
                  <Typography.Text type="secondary">{`@${need.created_by?.username}`}</Typography.Text>
                </Flex>
              </Space>
            )}

            {
              <Space size="middle">
                {canEdit ? (
                  <Space size="small">
                    <Typography.Text type="secondary">{t('due')}:</Typography.Text>
                    <DatePicker
                      value={need?.end_date ? dayjs(need?.end_date) : undefined}
                      onChange={(value) =>
                        setData({
                          ...needData,
                          end_date: value ? value.toISOString() : null,
                        })
                      }
                    />
                  </Space>
                ) : (
                  need?.end_date && (
                    <Space size="small">
                      <Typography.Text type="secondary">{t('due')}:</Typography.Text>
                      <Typography.Text>{dayjs(need?.end_date).format('YYYY-MM-DD')}</Typography.Text>
                    </Space>
                  )
                )}

                {isShareActive && <Share data={need} handleShare={handleShare} />}
              </Space>
            }
          </Flex>
        </Flex>
        <TabsStyled activeKey={tab} items={items} onChange={handleTabChange} />
      </ContainerStyled>
    </Layout>
  );
};

export default NeedContent;
