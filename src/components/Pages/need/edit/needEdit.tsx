import { useEffect } from 'react';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { useMutation, useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { message } from 'antd';
import { useStonePathStore } from '~/src/contexts/stonepath/StonePathStoreProvider';
import { NeedModel, NeedUpsertModel, Permission } from '~/__generated__/types';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import Loading from './loading/loading';
import { AxiosError } from 'axios';
import NeedContent from '../needContent/needContent';
import dayjs from 'dayjs';
import useLogin from '~/src/hooks/useLogin';

const NeedEdit = () => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const [, , handle403] = useLogin();
  const needId = router.query.id as string;

  const { data: need, error, refetch } = useQuery({
    queryKey: [QUERY_KEYS.needsDetailDetail, needId],
    queryFn: async () => {
      const response = await api.needs.needsDetailDetail(needId);
      return response.data;
    },
    retry: (failureCount, error: AxiosError) => {
      if (error.response?.status === 404 || error.response?.status === 403) {
        return false;
      }

      return failureCount < 3;
    },

    enabled: !!needId,
  });

  const { setStonePath } = useStonePathStore();
  useEffect(() => {
    need && setStonePath(need.path);
    return () => setStonePath([]);
  }, [need, setStonePath]);

  const mutation = useMutation({
    mutationFn: async (needData: NeedUpsertModel) => {
      return await api.needs.needsUpdateDetail(needId, needData);
    },
    onSuccess: () => {
      refetch();
      message.success(t('needWasSaved'));
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  if (error?.response?.status === 403) {
    handle403();
    return <></>;
  }

  if (!need) {
    return <Loading />;
  }

  const formatData = (data: NeedModel | NeedUpsertModel) => {
    const userVisibility = data.user_visibility?.map((item) => {
      return {
        visibility: item.visibility,
        user_id: item.user.id,
      };
    });

    const communityEntityVisibility = data.communityentity_visibility?.map((item) => {
      return {
        visibility: item.visibility,
        community_entity_id: item.community_entity.id,
      };
    });

    return {
      user_visibility: userVisibility,
      communityentity_visibility: communityEntityVisibility,
      default_visibility: data.default_visibility,
    };
  };

  const saveShare = (need: NeedModel) => {
    mutation.mutate({ ...need, ...formatData(need) });
  };

  const saveChanges = (data: NeedUpsertModel) => {
    if (
      need.title !== data?.title ||
      need.description !== data.description ||
      need.keywords?.join() !== data.keywords?.join() ||
      need.type !== data.type ||
      dayjs(need?.end_date).format('YYYY-MM-DD') !== dayjs(data.end_date).format('YYYY-MM-DD')
    ) {
      mutation.mutate({
        ...need,
        ...data,
        ...formatData(need),
      });
    }
  };

  const canEdit = need?.permissions?.includes(Permission.Manage);

  return <NeedContent canEdit={canEdit} need={need} handleChanges={saveChanges} handleShare={saveShare} />;
};
export default NeedEdit;
