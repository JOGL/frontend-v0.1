import Layout from '~/src/components/Layout/layout/layout';
import { getNotificationDescription } from '~/src/components/Header/NotificationUtils';
import ActionCard from '~/src/components/Header/ActionCard';
import useTranslation from 'next-translate/useTranslation';
import { Flex } from 'antd';
import { useInfiniteQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import { ButtonStyled, FlexStyled } from './actions.styles';
const PAGE_SIZE = 30;

const Actions = () => {
  const { data, hasNextPage, fetchNextPage, isFetchingNextPage, refetch } = useInfiniteQuery({
    queryKey: [QUERY_KEYS.notificationsList],
    queryFn: async ({ pageParam = 1 }) => {
      const response = await api.users.currentNotificationsList({ Page: pageParam, PageSize: PAGE_SIZE });
      return response.data;
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItemsCount = allPages.reduce((acc, page) => acc + page.items.length, 0);
      return loadedItemsCount < lastPage.total ? allPages.length + 1 : undefined;
    },
  });

  const rawNotifications =
    data?.pages.flatMap((page) => {
      return page.items;
    }) || [];

  const { t } = useTranslation('common');
  const notifications = rawNotifications.map((n) => getNotificationDescription(n, t));

  return (
    <Layout title="Actions | JOGL" noIndex>
      <FlexStyled vertical gap="middle">
        {notifications?.map((item, i) => (
          <ActionCard notification={item} key={item.id} reload={refetch} />
        ))}
      </FlexStyled>
      <Flex justify="center">
        {hasNextPage && (
          <ButtonStyled type="primary" loading={isFetchingNextPage} onClick={() => fetchNextPage()}>
            {t('action.load')}
          </ButtonStyled>
        )}
      </Flex>
    </Layout>
  );
};

export default Actions;
