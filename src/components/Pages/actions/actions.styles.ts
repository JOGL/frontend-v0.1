import styled from '@emotion/styled';
import { Button, Flex } from 'antd';

export const FlexStyled = styled(Flex)`
  padding: 0 100px;
  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    padding: 0 ${({ theme }) => theme.token.paddingContentHorizontalLG}px;
  }
`;

export const ButtonStyled = styled(Button)`
  width: fit-content;
  margin: ${({ theme }) => theme.token.paddingContentHorizontalLG}px;
`;
