import { useRouter } from 'next/router';
import Feed from '../../discussionFeed/feed/feed';
import Layout from '~/src/components/Layout/layout/layout';
import useTranslation from 'next-translate/useTranslation';
import { useQueryClient } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';

export const Discussion = () => {
  const router = useRouter();
  const docId = router.query.id as string;
  const { t } = useTranslation('common');
  const queryClient = useQueryClient();

  const onRefetch = () => {
    switch (router.query.inboxTab) {
      case 'mentions': {
        queryClient.invalidateQueries({
          queryKey: [QUERY_KEYS.nodeMentionContentEntityList],
        });
        return;
      }
      case 'thread': {
        queryClient.invalidateQueries({
          queryKey: [QUERY_KEYS.nodeThreadContentEntityList],
        });
        return;
      }

      default: {
        queryClient.invalidateQueries({
          queryKey: [QUERY_KEYS.nodeContentEntityList],
        });
        return;
      }
    }
  };

  return (
    <Layout title={t('discussion.one')}>
      <Feed feedId={docId} fullHeight showNavigation onRefetch={onRefetch} />
    </Layout>
  );
};
