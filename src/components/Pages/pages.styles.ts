import styled from '@emotion/styled';

export const PageWrapperStyled = styled.div`
  box-shadow: ${({ theme }) => theme.token.boxShadow};
`;
