import { useEffect } from 'react';
import { NodeDetailModel } from '~/__generated__/types';
import { useStonePathStore } from '~/src/contexts/stonepath/StonePathStoreProvider';
import Layout from '~/src/components/Layout/layout/layout';
import HubWorkspaceList from '~/src/components/workspace/hubWorkspaceList/hubWorkspaceList';
import useWorkspaces, { getHubWorkspacesViewFilter } from '~/src/components/workspace/useWorkspaces';

interface Props {
  node: NodeDetailModel;
  filterKey: string;
}

const HubWorkspaces = ({ node, filterKey }: Props) => {
  const { setStonePath } = useStonePathStore();
  const [, getHubWorkspacesViewTitle] = useWorkspaces();

  useEffect(() => {
    setStonePath(node?.path);

    return () => setStonePath([]);
  }, [node, setStonePath]);

  const viewTitle = getHubWorkspacesViewTitle(filterKey);
  const filterData = getHubWorkspacesViewFilter(filterKey);

  return (
    <Layout title="Home">
      <HubWorkspaceList
        hubId={node?.id}
        title={viewTitle}
        permission={filterData.permission}
        sortKey={filterData.sortKey}
        sortAscending={filterData.sortAscending}
      />
    </Layout>
  );
};

export default HubWorkspaces;
