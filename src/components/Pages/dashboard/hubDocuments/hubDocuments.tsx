import { useEffect } from 'react';
import { NodeDetailModel } from '~/__generated__/types';
import { useStonePathStore } from '~/src/contexts/stonepath/StonePathStoreProvider';
import HubDocumentList from '~/src/components/documents/hubDocumentList/hubDocumentList';
import Layout from '~/src/components/Layout/layout/layout';
import useDocuments, { getDocumentViewFilter } from '~/src/components/documents/useDocuments';

interface Props {
  node: NodeDetailModel;
  filterKey: string;
}

const HubDocuments = ({ node, filterKey }: Props) => {
  const { setStonePath } = useStonePathStore();
  const [, getDocumentViewTitle] = useDocuments();
  useEffect(() => {
    setStonePath(node?.path);

    return () => setStonePath([]);
  }, [node, setStonePath]);

  const viewTitle = getDocumentViewTitle(filterKey);
  const filterData = getDocumentViewFilter(filterKey);
  return (
    <Layout title="Documents">
      <HubDocumentList
        hubId={node?.id}
        title={viewTitle}
        filter={filterData.filter}
        sortKey={filterData.sortKey}
      />
    </Layout>
  );
};

export default HubDocuments;
