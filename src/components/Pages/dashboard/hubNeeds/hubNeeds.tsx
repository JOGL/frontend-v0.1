import { useEffect } from 'react';
import { NodeDetailModel } from '~/__generated__/types';
import { useStonePathStore } from '~/src/contexts/stonepath/StonePathStoreProvider';
import HubNeedList from '~/src/components/Need/hubNeedList/hubNeedList';
import Layout from '~/src/components/Layout/layout/layout';
import useNeeds, { getNeedViewFilter } from '~/src/components/Need/useNeeds';

interface Props {
  node: NodeDetailModel;
  filterKey: string;
}

const HubNeeds = ({ node, filterKey }: Props) => {
  const { setStonePath } = useStonePathStore();
  const [, getNeedViewTitle] = useNeeds();

  useEffect(() => {
    setStonePath(node?.path);

    return () => setStonePath([]);
  }, [node, setStonePath]);

  const viewTitle = getNeedViewTitle(filterKey);
  const filterData = getNeedViewFilter(filterKey);
  return (
    <Layout title="Needs">
      <HubNeedList hubId={node?.id} title={viewTitle} filter={filterData.filter} sortKey={filterData.sortKey} />
    </Layout>
  );
};

export default HubNeeds;
