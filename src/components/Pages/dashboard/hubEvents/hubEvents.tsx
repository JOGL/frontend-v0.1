import { useEffect } from 'react';
import { NodeDetailModel } from '~/__generated__/types';
import { useStonePathStore } from '~/src/contexts/stonepath/StonePathStoreProvider';
import Layout from '~/src/components/Layout/layout/layout';
import HubEventList from '~/src/components/Event/hubEventList/hubEventList';
import useEvents, { getEventViewFilter } from '~/src/components/Event/useEvents';

interface Props {
  node: NodeDetailModel;
  filterKey: string;
}

const HubEvents = ({ node, filterKey }: Props) => {
  const { setStonePath } = useStonePathStore();
  const [, getEventViewTitle] = useEvents();

  useEffect(() => {
    setStonePath(node?.path);

    return () => setStonePath([]);
  }, [node, setStonePath]);

  const viewTitle = getEventViewTitle(filterKey);
  const filterData = getEventViewFilter(filterKey);
  return (
    <Layout title="Events">
      <HubEventList
        hubId={node?.id}
        title={viewTitle}
        filter={filterData.filter}
        sortKey={filterData.sortKey}
        allowPast={filterData.allowPast}
      />
    </Layout>
  );
};

export default HubEvents;
