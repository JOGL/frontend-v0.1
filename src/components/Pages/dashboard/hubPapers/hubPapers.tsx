import { useEffect } from 'react';
import { NodeDetailModel } from '~/__generated__/types';
import { useStonePathStore } from '~/src/contexts/stonepath/StonePathStoreProvider';
import HubPaperList from '~/src/components/papers/hubPaperList/hubPaperList';
import Layout from '~/src/components/Layout/layout/layout';
import usePapers, { getPaperViewFilter } from '~/src/components/papers/usePapers';

interface Props {
  node: NodeDetailModel;
  filterKey: string;
}

const HubPapers = ({ node, filterKey }: Props) => {
  const { setStonePath } = useStonePathStore();
  const [, getPaperViewTitle] = usePapers();

  useEffect(() => {
    setStonePath(node?.path);

    return () => setStonePath([]);
  }, [node, setStonePath]);

  const viewTitle = getPaperViewTitle(filterKey);
  const filterData = getPaperViewFilter(filterKey);
  return (
    <Layout title="Papers">
      <HubPaperList hubId={node?.id} title={viewTitle} filter={filterData.filter} sortKey={filterData.sortKey} />
    </Layout>
  );
};

export default HubPapers;
