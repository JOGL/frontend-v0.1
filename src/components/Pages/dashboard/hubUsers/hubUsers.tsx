import { useEffect } from 'react';
import { NodeDetailModel } from '~/__generated__/types';
import { useStonePathStore } from '~/src/contexts/stonepath/StonePathStoreProvider';
import Layout from '~/src/components/Layout/layout/layout';
import HubUserList from '~/src/components/users/hubUserList/hubUserList';
import useUsers, { getUserViewFilter } from '~/src/components/users/useUsers';

interface Props {
  node: NodeDetailModel;
  filterKey: string;
}

const HubUsers = ({ node, filterKey }: Props) => {
  const { setStonePath } = useStonePathStore();
  const [, , getUserViewTitle] = useUsers();

  useEffect(() => {
    setStonePath(node?.path);

    return () => setStonePath([]);
  }, [node, setStonePath]);

  const viewTitle = getUserViewTitle(filterKey);
  const filterData = getUserViewFilter(filterKey);
  return (
    <Layout title="Users">
      <HubUserList hubId={node?.id} title={viewTitle} sortKey={filterData.sortKey} />
    </Layout>
  );
};

export default HubUsers;
