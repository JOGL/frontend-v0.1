import { Empty, Flex } from 'antd';
import Layout from '~/src/components/Layout/layout/layout';

const EmptyComponent = () => {
  return (
    <Layout title="Inbox">
      <Flex vertical align="center" justify="space-around" style={{ height: '100%' }}>
        <Empty />
      </Flex>
    </Layout>
  );
};
export default EmptyComponent;
