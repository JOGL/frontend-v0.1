import { SignUpRightSection } from './SignUpRightSection/SignUpRightSection';
import SignInUpContainer from '../Common/SignInUpContainer/SignInUpContainer';
import { GoogleReCaptchaProvider } from 'react-google-recaptcha-v3';

/**
 * Renders the SignUp page, which includes a left section with the logo and carousel,
 * and a right section with the sign-up form.
 *
 * @returns {JSX.Element} The rendered SignUp page component.
 */
export const SignUp = (): JSX.Element => {
  return (
    <GoogleReCaptchaProvider reCaptchaKey={process.env.NEXT_PUBLIC_RECAPTCHA_KEY ?? 'NOT DEFINED'}>
      <SignInUpContainer>
        <SignUpRightSection />
      </SignInUpContainer>
    </GoogleReCaptchaProvider>
  );
};
