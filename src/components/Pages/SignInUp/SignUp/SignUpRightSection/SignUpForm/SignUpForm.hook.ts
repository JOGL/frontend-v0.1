import { useState } from 'react';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import useUser from '~/src/hooks/useUser';
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';
/**
 * Custom hook to handle the sign-up form logic.
 *
 * @returns {object} The state and functions to manage the sign-up form.
 */

const useSignUpForm = () => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const [sending, setSending] = useState(false);
  const [error, setError] = useState('');
  const userContext = useUser();
  const { executeRecaptcha } = useGoogleReCaptcha();

  const onSubmit = async (data: any) => {
    setSending(true);
    setError('');

    const randomNumber = Math.floor(Math.random() * 12) + 1; // with 12 = max, and 1 = min
    const logoUrl = `/images/default/default-user-${randomNumber}.png`;

    if (!executeRecaptcha) {
      setError(t('reCaptchaCouldNotBeExecuted'));
      return;
    }

    const captcha_verification_token = await executeRecaptcha('registration');
    const user = {
      ...data,
      username: '',
      email: (router.query.email as string)?.toLowerCase(),
      logo_url: logoUrl,
      verification_code: (router.query.verification_code as string) || '',
      captcha_verification_token,
      redirect_url: (router.query.redirectUrl as string) || '',
    };

    userContext &&
      userContext
        .signUp(user)
        .then((res) => {
          setSending(false);
          const redirectUrl = router.query.redirectUrl;
          if (router.query.verification_code) {
            router.push(
              { pathname: '/signin', query: { email: user.email, confirmed: true, redirectUrl } },
              undefined,
              {
                shallow: true,
              }
            );
            return;
          }
          router.push({ pathname: '/signup', query: { success: 1, email: user.email, redirectUrl } }, undefined, {
            shallow: true,
          });
          typeof window !== 'undefined' && window.scrollTo(0, 0);
        })
        .catch((err) => {
          setSending(false);
          if (err?.response?.status) {
            let errorTranslationId = 'someUnknownErrorOccurred';
            if (err.response.data.error === 'username') {
              errorTranslationId = 'error.usernameIsAlreadyTaken';
            } else if (err.response.data.error === 'email') {
              errorTranslationId = 'error.emailAlreadyExists';
            }
            setError(t(errorTranslationId));
          }
        });
  };

  return {
    sending,
    error,
    onSubmit,
  };
};

export default useSignUpForm;
