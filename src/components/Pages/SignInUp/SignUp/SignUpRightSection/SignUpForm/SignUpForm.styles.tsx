import styled from '@emotion/styled';

export const LinkButtonContainerStyled = styled.div`
  button {
    padding: 0;
  }
`;

export const PolicyContainerStyled = styled.div`
  margin-bottom: ${({ theme }) => theme.space[6]};
`;
