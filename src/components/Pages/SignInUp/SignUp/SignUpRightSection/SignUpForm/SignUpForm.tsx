import { useState } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { LeftOutlined } from '@ant-design/icons';
import { Form, Input, Button, Flex, Typography, Space, Row, Col, Alert, Checkbox } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import Trans from 'next-translate/Trans';
import { LinkButtonContainerStyled, PolicyContainerStyled } from './SignUpForm.styles';
import useSignUpForm from './SignUpForm.hook';
import { EMAIL_REGEX } from '~/src/utils/utils';

/**
 * Renders the sign-up form with validation and step functionality.
 *
 * @returns {JSX.Element} The sign-up form component.
 */
const SignUpForm = (): JSX.Element => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const { sending, error, onSubmit } = useSignUpForm();
  const [form] = Form.useForm();
  const [formStep, setFormStep] = useState(0);
  const initialEmail = router.query.email ?? '';
  const email = Form.useWatch('email', form);
  const verification_code = router.query.verification_code ?? '';
  const redirectUrl = router.query.redirectUrl;

  const handleNextStep = () => {
    form
      .validateFields(['email'])
      .then(() => {
        setFormStep(1);
        router.push({ pathname: '/signup', query: { formStep: 1, email, verification_code, redirectUrl } }, undefined, {
          shallow: true,
        });
      })
      .catch(() => {});
  };

  const handlePreviousStep = () => {
    setFormStep(0);
    router.push({ pathname: '/signup', query: { email: initialEmail, verification_code, redirectUrl } }, undefined, {
      shallow: true,
    });
  };
  return (
    <Form
      form={form}
      onFinish={onSubmit}
      requiredMark={false}
      layout="vertical"
      initialValues={{ email: initialEmail }}
    >
      {formStep === 0 && (
        <>
          <Form.Item
            label={t('email')}
            name="email"
            rules={[
              { required: true, message: t('error.valueIsMissing') },
              {
                type: 'string',
                pattern: EMAIL_REGEX,
                message: t('error.invalidMailAddress'),
              },
            ]}
          >
            <Input size="large" placeholder={t('emailPlaceholder')} />
          </Form.Item>
          <Flex vertical justify="center" align="center" gap="middle">
            <Button type="primary" loading={sending} size="large" htmlType="button" onClick={handleNextStep}>
              {t('action.createMyAccount')}
            </Button>

            <Space>
              <Typography.Text>{t('alreadyHaveAnAccount')}</Typography.Text>
              <Typography.Text underline={true}>
                <Link href="/signin">{t('action.signIn')}</Link>
              </Typography.Text>
            </Space>
          </Flex>
        </>
      )}
      {formStep === 1 && (
        <>
          <LinkButtonContainerStyled>
            <Button type="link" icon={<LeftOutlined />} onClick={handlePreviousStep}>
              {t('action.return')}
            </Button>
          </LinkButtonContainerStyled>
          <Row gutter={12} justify="center">
            <Col span={12}>
              <Form.Item
                label={t('firstName')}
                name="first_name"
                rules={[
                  { required: true, message: t('error.valueIsMissing') },
                  { type: 'string', max: 60, message: t('error.valueIsTooLong') },
                ]}
              >
                <Input size="large" placeholder={t('john')} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label={t('lastName')}
                name="last_name"
                rules={[
                  { required: true, message: t('error.valueIsMissing') },
                  { type: 'string', max: 60, message: t('error.valueIsTooLong') },
                ]}
              >
                <Input size="large" placeholder={t('doe')} />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                label={t('password')}
                name="password"
                rules={[
                  { required: true, message: t('error.valueIsMissing') },
                  { type: 'string', min: 8, message: t('error.passwordToShort') },
                  { type: 'string', max: 128, message: t('error.valueIsTooLong') },
                ]}
              >
                <Input size="large" placeholder={t('yourPassword')} type="password" />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                label={t('passwordConfirmation')}
                name="password_confirmation"
                rules={[
                  { required: true, message: t('error.valueIsMissing') },
                  { type: 'string', min: 8, message: t('error.passwordToShort') },
                  { type: 'string', max: 128, message: t('error.valueIsTooLong') },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || value.length < 8) {
                        return Promise.resolve();
                      }
                      if (!value || getFieldValue('password') === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(new Error(t('error.wrongPasswordConfirmation')));
                    },
                  }),
                ]}
              >
                <Input size="large" placeholder={t('action.confirmPassword')} type="password" />
              </Form.Item>
            </Col>
          </Row>
          {error && <Alert message={error} type="error" />}
          <PolicyContainerStyled>
            <Form.Item
              name="agreement"
              valuePropName="checked"
              rules={[
                {
                  validator: (_, value) =>
                    value ? Promise.resolve() : Promise.reject(new Error(t('error.mustHaveReadAndAccepted'))),
                },
              ]}
            >
              <Checkbox>
                <Typography.Text>
                  <Trans
                    i18nKey="common:termsOfServiceAgreementTemplate"
                    components={[
                      <div className="hookExplain" />,
                      <a href="/terms" target="_blank" />,
                      <a href="/data" target="_blank" />,
                      <a href="/ethics-pledge" target="_blank" />,
                    ]}
                    values={{
                      termsOfService: t('termsOfService'),
                      dataPolicy: t('userDataPolicy'),
                      ethicsPledge: t('ethicsPledge'),
                    }}
                  />
                </Typography.Text>
              </Checkbox>
            </Form.Item>
          </PolicyContainerStyled>
          <Flex vertical justify="center" align="center" gap="middle">
            <Button htmlType="submit" type="primary" size="large" loading={sending}>
              {t('action.createMyAccount')}
            </Button>
          </Flex>
        </>
      )}
    </Form>
  );
};

export default SignUpForm;
