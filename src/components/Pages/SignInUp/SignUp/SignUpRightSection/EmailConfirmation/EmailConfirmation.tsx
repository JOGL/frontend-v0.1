import { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from '~/src/contexts/apiContext';
import { confAlert } from '~/src/utils/utils';
import { Button, Flex, Space, Typography } from 'antd';

/**
 * EmailConfirmation component renders a section for email confirmation.
 * It allows the user to resend the confirmation email.
 *
 * @param {string} email - The email address to which the confirmation email will be sent.
 * @returns {JSX.Element} The rendered email confirmation section.
 */

const EmailConfirmation: FC<{ email: string }> = ({ email }) => {
  const { t } = useTranslation('common');
  const [sending, setSending] = useState(false);
  const api = useApi();

  const sendConfirmationEmail = async () => {
    try {
      setSending(true);
      await api?.post(`/auth/verification`, { email: email.toLowerCase() });
      confAlert.fire({ icon: 'success', title: t('emailSentSuccess') });
    } catch (e) {
      confAlert.fire({ icon: 'error', title: t('someUnknownErrorOccurred') });
    }
    setSending(false);
  };

  return (
    <Flex vertical justify="center">
      <Flex vertical justify="center" align="center">
        <Typography.Text> {t('confirmYourEmail')}</Typography.Text>
        <Typography.Text underline={true} strong={true}>
          {email.toLowerCase()}.
        </Typography.Text>
      </Flex>

      <Flex vertical justify="center" align="center" gap="middle">
        <Space>
          <Typography.Text>{t('haventRecivedConfirmation')}</Typography.Text>
        </Space>
        <Button
          htmlType="submit"
          type="primary"
          size="large"
          loading={sending}
          onClick={(e) => {
            e.preventDefault();
            sendConfirmationEmail();
          }}
        >
          {t('resendMailConfirmation')}
        </Button>
      </Flex>
    </Flex>
  );
};

export default EmailConfirmation;
