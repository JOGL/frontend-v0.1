import { useRouter } from 'next/router';
import QuickSignInUp from '../../Common/QuickSignInUp/QuickSignInUp';
import EmailConfirmation from './EmailConfirmation/EmailConfirmation';
import SignUpForm from './SignUpForm/SignUpForm';

/**
 * Renders the right section of the SignUp page.
 *
 * @returns {JSX.Element} The rendered right panel of the Sign Up page.
 */

export const SignUpRightSection = (): JSX.Element => {
  const router = useRouter();
  const success = router.query.success === '1';
  const formStep = router.query.formStep === '1' ? 1 : 0;
  const email = (router.query.email as string) ?? '';

  return (
    <>
      {success && email && <EmailConfirmation email={email} />}

      {!success && (
        <>
          {!success && formStep === 0 && <QuickSignInUp />}
          {!success && <SignUpForm />}
        </>
      )}
    </>
  );
};
