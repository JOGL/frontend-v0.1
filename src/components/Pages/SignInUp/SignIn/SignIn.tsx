import SignInRightSection from './SignInRightSection/SignInRightSection';
import SignInUpContainer from '../Common/SignInUpContainer/SignInUpContainer';

const SignIn = (): JSX.Element => {
  return (
    <SignInUpContainer>
      <SignInRightSection />
    </SignInUpContainer>
  );
};

export default SignIn;