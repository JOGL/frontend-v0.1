import Link from 'next/link';
import axios from 'axios';
import { Button, Form, Input, Flex, Typography, Space } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import { AlertStyled, LabelWrapper } from './SignInForm.styles';
import { useSignIn, useResendVerifyEmail } from './SignInForm.hook';
import { EMAIL_REGEX } from '~/src/utils/utils';
import { Auth } from '~/__generated__/types';

const SignInForm = () => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const signIn = useSignIn();
  const resendVerifyEmail = useResendVerifyEmail();
  const [form] = Form.useForm<Auth.LoginCreate.RequestBody>();
  const initialEmail = router.query.email ?? '';
  const email = Form.useWatch('email', form);
  const isAccountNotValidated = !!(
    router.query.confirmed === 'false' ||
    (signIn.error && axios.isAxiosError(signIn.error) && signIn.error.response?.status === 403)
  );

  const handleSubmit = ({ email, password }: Auth.LoginCreate.RequestBody) => {
    signIn.mutate({ email, password });
  };

  return (
    <Form
      form={form}
      layout="vertical"
      initialValues={{ email: initialEmail }}
      onFinish={handleSubmit}
      requiredMark={false}
    >
      <Form.Item
        label={t('email')}
        name="email"
        rules={[
          { required: true, message: t('error.valueIsMissing') },
          {
            type: 'string',
            pattern: EMAIL_REGEX,
            message: t('error.invalidMailAddress'),
          },
        ]}
      >
        <Input size="large" placeholder={t('emailPlaceholder')} type="email" />
      </Form.Item>
      <LabelWrapper>
        <label htmlFor="password">{t('password')}</label>
        <Link href="/auth/forgot-password">{t('forgotYourPassword')}</Link>
      </LabelWrapper>
      <Form.Item
        name="password"
        id="password"
        rules={[
          { required: true, message: t('error.valueIsMissing') },
          { type: 'string', min: 8, message: t('error.passwordToShort') },
          { type: 'string', max: 128, message: t('error.valueIsTooLong') },
        ]}
      >
        <Input size="large" placeholder={t('yourPassword')} type="password" />
      </Form.Item>

      {isAccountNotValidated && (
        <AlertStyled
          message={t('youDidntValidateYourAccount')}
          type="error"
          action={<Button onClick={() => resendVerifyEmail.mutate(email.toLowerCase())}>{t('resendEmail')}</Button>}
        />
      )}

      <Flex vertical justify="center" align="center" gap="middle">
        <Space>
          <Typography.Text>{t('dontHaveAnAccount')}</Typography.Text>
          <Link href={router.query.redirectUrl ? `/signup?redirectUrl=${router.query.redirectUrl}` : '/signup/'}>
            {t('action.signUp')}
          </Link>
        </Space>
        <Button htmlType="submit" type="primary" size="large" loading={signIn.isPending}>
          {t('action.signIn')}
        </Button>
      </Flex>
    </Form>
  );
};

export default SignInForm;
