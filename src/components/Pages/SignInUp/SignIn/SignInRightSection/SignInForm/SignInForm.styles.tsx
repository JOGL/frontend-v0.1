import styled from '@emotion/styled';
import { Alert } from 'antd';

export const LabelWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: ${({ theme }) => theme.space[2]};
`;

export const AlertStyled = styled(Alert)`
  margin-bottom: ${({ theme }) => theme.space[4]};
`;
