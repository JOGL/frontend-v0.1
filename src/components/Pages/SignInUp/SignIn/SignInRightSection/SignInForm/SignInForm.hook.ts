import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import { message } from 'antd';
import { useMutation } from '@tanstack/react-query';
import axios from 'axios';
import { useAuth } from '~/auth/auth';
import api from '~/src/utils/api/api';
import { Auth } from '~/__generated__/types';

export function useSignIn() {
  const { t } = useTranslation('common');
  const { onLoginUser } = useAuth();
  const router = useRouter();

  const signIn = useMutation({
    mutationFn: async ({ email, password }: Auth.LoginCreate.RequestBody) => {
      const response = await api.auth.loginCreate({ email: email.toLowerCase(), password });
      return response.data;
    },
    onSuccess: ({ userId, token }) => {
      onLoginUser({ userId, token });
      if (router.query.confirmed !== 'true') {
        router.replace(
          !router.query.invited || router.query.redirectUrl
            ? (router.query.redirectUrl as string) || `/user/${userId}`
            : `/action`
        );
      }
    },
    onError: (error) => {
      if (axios.isAxiosError(error)) {
        if (error.response?.status === 401) {
          message.error(t('error.invalidEmailOrPassword'));
        }
      } else {
        console.log({ error });
        message.error('Something went wrong. Please try again later.');
      }
    },
  });

  return signIn;
}

export function useResendVerifyEmail() {
  const { t } = useTranslation('common');

  const resendVerifyEmail = useMutation({
    mutationFn: async (email: string) => {
      await api.auth.verificationCreate({ email: email.toLowerCase() });
    },
    onSuccess: () => {
      message.success(t('emailSentSuccess'));
    },
    onError: () => {
      message.success(t('someUnknownErrorOccurred'));
    },
  });

  return resendVerifyEmail;
}
