import styled from '@emotion/styled';
import { Alert } from 'antd';

export const AlertStyled = styled(Alert)`
  margin-bottom: ${({ theme }) => theme.space[4]};
`;

export const FullWidthStyled = styled.div`
  width: 100%;
`;
