import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import QuickSignInUp from '../../Common/QuickSignInUp/QuickSignInUp';
import { AlertStyled, FullWidthStyled } from './SignInRightSection.styles';
import SignInForm from './SignInForm/SignInForm';

/**
 * Renders the right section of the SignIn page, including alerts and sign-in forms.
 *
 * @returns {JSX.Element} The rendered right panel of the SignIn page.
 */

const SignInRightSection = () => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const showEventMessage = router.query.redirectUrl?.includes('/event/');

  return (
    <>
      {router.query?.confirmed === 'true' && (
        <AlertStyled type="success" message={t('congratulations')} description={t('joglAccountIsActivated')} />
      )}
      {showEventMessage && <AlertStyled type="warning" message={t('signInEventMessage')} />}

      <AlertStyled type="warning" message={t('signInDisclaimer')} />
      <FullWidthStyled>
        <QuickSignInUp />
        <SignInForm />
      </FullWidthStyled>
    </>
  );
};
export default SignInRightSection;
