import { useContext } from 'react';
import { UserContext } from '~/src/contexts/UserProvider';

const SignOut = () => {
  const userContext = useContext(UserContext);
  userContext?.logout();
};

export default SignOut;
