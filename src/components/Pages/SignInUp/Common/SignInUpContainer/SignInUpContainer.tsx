import { FC } from 'react';
import { ContainerProps } from './SignInUpContainer.types';
import { ColRightStyled, RightPanelStyled, RowStyled } from './SignInUpContainer.styles';

const SignInUpContainer: FC<ContainerProps> = ({ children }) => {
  return (
    <RowStyled>
      <ColRightStyled xs={24} lg={{ span: 12, offset: 6 }}>
        <RightPanelStyled>{children}</RightPanelStyled>
      </ColRightStyled>
    </RowStyled>
  );
};

export default SignInUpContainer;
