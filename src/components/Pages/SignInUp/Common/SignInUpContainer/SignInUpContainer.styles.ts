import styled from '@emotion/styled';
import { Col, Row } from 'antd';

export const RowStyled = styled(Row)`
  margin: 0 100px;
  height: 100vh;
  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    margin: 0;
  }
`;

export const ColRightStyled = styled(Col)`
  padding: ${({ theme }) => theme.space[8]};
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export const RightPanelStyled = styled.div`
  max-width: 650px;
`;
