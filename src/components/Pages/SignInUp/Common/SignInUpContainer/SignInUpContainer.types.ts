import { ReactNode } from 'react';

export interface ContainerProps {
  children: ReactNode;
  showHeaderFooter?: boolean;
}
