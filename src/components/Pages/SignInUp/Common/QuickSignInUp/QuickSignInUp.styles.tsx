import styled from '@emotion/styled';

export const FormContainerStyled = styled.div`
  button {
    width: 100%;
    margin-bottom: ${({ theme }) => theme.space[3]};
  }
  margin-bottom: ${({ theme }) => theme.space[8]};
  .ant-btn-icon {
    min-width: 35px;
  }
  span {
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
  }
`;
