import { Translate } from 'next-translate';

type AuthProvider = 'ORCID' | 'Google' | 'LinkedIn';
export interface HandleErrorParams {
  err: any;
  type: AuthProvider;
  t: Translate;
}

export interface RedirectURlParams {
  created: boolean;
  userId: string;
}
