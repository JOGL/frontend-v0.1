import { useRouter } from 'next/router';
import { useGoogleLogin } from '@react-oauth/google';
import useTranslation from 'next-translate/useTranslation';
import { useLinkedIn } from 'react-linkedin-login-oauth2';
import useUser from '~/src/hooks/useUser';
import { addQueryParams } from '~/src/utils/utils';
import { handleError } from './QuickSignUp.helpers';
import { RedirectURlParams } from './QuickSignUp.types';
import { useEffect, useState } from 'react';
import { message } from 'antd';

/**
 * Custom hook to handle social login functionalities (Google, ORCID, LinkedIn).
 *
 * @returns {Object} An object containing methods for social logins.
 */
const useQuickLogins = () => {
  const router = useRouter();
  const userContext = useUser();
  const isSignIn = router.pathname.toLowerCase() === '/signin';
  const { t } = useTranslation('common');

  const [alertDialogLoadingMsg, setAlertDialogLoadingMsg] = useState('');
  const [alertDialogErrorMsg, setAlertDialogErrorMsg] = useState('');

  useEffect(() => {
    if (router.query.code) {
      loginWithOrcid(`${router.query.code}`);
    }
  }, [router.query.code]);

  useEffect(() => {
    if (alertDialogLoadingMsg && !alertDialogErrorMsg) {
      const hideLoading = message.loading(alertDialogLoadingMsg, 0);
      return () => hideLoading();
    }
    if (alertDialogErrorMsg) {
      message.error({
        content: alertDialogErrorMsg,
        onClose: () => setAlertDialogErrorMsg(''),
        duration: 3,
      });
    }
  }, [alertDialogErrorMsg, alertDialogLoadingMsg]);

  const openLinkMyOrcidModal = () => {
    const orcidUrl = new URL(`${process.env.ORCID_URL}/oauth/authorize`);
    orcidUrl.searchParams.append('client_id', process.env.ORCID_CLIENT_ID || '');
    orcidUrl.searchParams.append('response_type', 'code');
    orcidUrl.searchParams.append('scope', '/read-limited');
    orcidUrl.searchParams.append('redirect_uri', `${process.env.ADDRESS_FRONT}/signin`);

    router.push(orcidUrl.toString());
  };

  const setRedirectUrlAfterAuthentication = ({ created, userId }: RedirectURlParams) => {
    if (created) {
      router.replace({ pathname: `/onboarding`, query: router.query });
    } else {
      router.replace((router.query.redirectUrl as string) || `/user/${userId}`);
    }
  };

  const loginWithOrcid = (code: string) => {
    setAlertDialogLoadingMsg(t('loggingYouIn'));
    userContext &&
      userContext
        .authOrcid(code, isSignIn ? 'signin' : 'signup')
        .then(async (res) => {
          setAlertDialogLoadingMsg('');
          if (isSignIn) {
            const redirect_uri = window?.localStorage?.getItem('redirect_uri');
            if (typeof redirect_uri === 'string' && redirect_uri.length > 0) {
              const url = addQueryParams(redirect_uri, { code: code });
              router.replace({ pathname: url.pathname, query: `${url.searchParams}` });
              return;
            }
          }

          if (res.userId) {
            const { userId, created } = res;
            setRedirectUrlAfterAuthentication({ userId, created });
          }
        })
        .catch((err) => {
          setAlertDialogLoadingMsg('');
          const errorMsg = handleError({ err, type: 'ORCID', t });
          errorMsg && setAlertDialogErrorMsg(errorMsg);
        });
  };

  const loginWithGoogle = useGoogleLogin({
    onSuccess: (codeResponse) => {
      setAlertDialogLoadingMsg(isSignIn ? t('loggingYouIn') : t('signingYouUp'));
      userContext &&
        userContext
          .authGoogle(codeResponse.access_token)
          .then(async (res) => {
            if (res.userId) {
              setAlertDialogLoadingMsg('');
              const { userId, created } = res;
              setRedirectUrlAfterAuthentication({ userId, created });
            }
          })
          .catch((err) => {
            setAlertDialogLoadingMsg('');
            const errorMsg = handleError({ err, type: 'Google', t });
            errorMsg && setAlertDialogErrorMsg(errorMsg);
          });
    },
  });

  const { linkedInLogin } = useLinkedIn({
    clientId: process.env.LINKEDIN_CLIENT_ID || '',
    redirectUri: `${typeof window === 'object' && window.location.origin}/linkedin`,
    scope: 'email openid profile',
    onSuccess: (authCode) => {
      setAlertDialogLoadingMsg(isSignIn ? t('loggingYouIn') : t('signingYouUp'));

      userContext
        .authLinkedIn(authCode)
        .then(async (res) => {
          setAlertDialogLoadingMsg('');
          if (res.userId) {
            const { userId, created } = res;
            setRedirectUrlAfterAuthentication({ userId, created });
          }
        })
        .catch((err) => {
          setAlertDialogLoadingMsg('');
          const errorMsg = handleError({ err, type: 'LinkedIn', t });
          errorMsg && setAlertDialogErrorMsg(errorMsg);
        });
    },
    onError: (error) => console.warn(error),
  });

  return {
    loginWithGoogle,
    linkedInLogin,
    openLinkMyOrcidModal,
  };
};

export default useQuickLogins;
