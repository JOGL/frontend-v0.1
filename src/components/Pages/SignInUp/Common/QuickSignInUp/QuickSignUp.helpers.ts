import { HandleErrorParams } from './QuickSignUp.types';

export const handleError = ({ err, type, t }: HandleErrorParams): string | undefined => {
  const quikSingUpText = {
    alreadySignUp: t('alreadySignUp'),
    alreadySignMsg: t('alreadySignMsg'),
    somethingWrongMsg: t('somethingWrongMsg'),
    unableToFetchMsg: t('unableToFetchMsg', { type }),
    ORCID: {
      emailNotFound: t('emailNotFoundORCID'),
      emailNotFoundMsg: t('emailNotFoundMsgORCID'),
    },
  };

  if (err && err.response) {
    const { data, status } = err.response;
    if (status === 404) {
      return quikSingUpText.unableToFetchMsg;
    } else if (status === 400) {
      if (type === 'ORCID' && data === quikSingUpText[type]?.emailNotFound) {
        return quikSingUpText[type]?.emailNotFoundMsg;
      } else if (data === quikSingUpText.alreadySignUp) {
        return quikSingUpText.alreadySignMsg;
      } else {
        console.warn(err.response);
        return quikSingUpText.somethingWrongMsg;
      }
    } else {
      return quikSingUpText.somethingWrongMsg;
    }
  }
};
