import React from 'react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import useQuickLogins from './QuickSignInUp.hook';
import useTranslation from 'next-translate/useTranslation';
import { Button, Col, Row } from 'antd';
import { FormContainerStyled } from './QuickSignInUp.styles';

/**
 * Renders the Quick Sign-In/Sign-Up component with social login options (Google, ORCID, LinkedIn).
 *
 * @returns {JSX.Element} The Quick Sign-In/Sign-Up component.
 */

const QuickSignInUp = (): JSX.Element => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const isSignIn = router.pathname.toLowerCase() === '/signin';
  const { loginWithGoogle, linkedInLogin, openLinkMyOrcidModal } = useQuickLogins();

  return (
    <FormContainerStyled>
      <Button
        type="default"
        size="large"
        icon={<Image src={`/images/icons/sign-in-google.png`} width={35} height={35} alt="Google logo" />}
        onClick={() => loginWithGoogle()}
      >
        {`${isSignIn ? t('action.signInWith', { type: 'Google' }) : t('action.signUpWith', { type: 'Google' })} `}
      </Button>

      {/* <Row justify="center" gutter={12}>
        <Col lg={12} xs={24}>
          <Button
            type="default"
            shape="round"
            size="large"
            icon={<Image src={`/images/icons/links-orcid.png`} width={35} height={35} alt="Orcid logo" />}
            onClick={() => openLinkMyOrcidModal()}
          >
            {`${isSignIn ? t('action.signInWith', { type: 'ORCID' }) : t('action.signUpWith', { type: 'ORCID' })} `}
          </Button>
        </Col>
        <Col lg={12} xs={24}> */}
      <Button
        type="default"
        size="large"
        icon={<Image src={`/images/icons/sign-in-linkedIn.png`} width={35} height={35} alt="LinkedIn logo" />}
        onClick={() => linkedInLogin()}
      >
        {`${isSignIn ? t('action.signInWith', { type: 'LinkedIn' }) : t('action.signUpWith', { type: 'LinkedIn' })} `}
      </Button>
      {/* </Col>
      </Row> */}
    </FormContainerStyled>
  );
};

export default QuickSignInUp;
