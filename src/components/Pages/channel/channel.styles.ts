import styled from '@emotion/styled';

export const ContainerStyled = styled.div`
  height: 100%;
  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    padding: ${({ theme }) => theme.token.paddingXXS}px;
  }
`;
