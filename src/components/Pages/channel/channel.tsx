import { ChannelDetailModel } from '~/__generated__/types';
import Layout from '~/src/components/Layout/layout/layout';
import { useStonePathStore } from '~/src/contexts/stonepath/StonePathStoreProvider';
import DiscussionDetails from '../../discussionDetails/discussionDetails';
import { useEffect } from 'react';
import { ContainerStyled } from './channel.styles';

interface Props {
  channel: ChannelDetailModel;
}
const Channel = ({ channel }: Props) => {
  const { setStonePath } = useStonePathStore();

  useEffect(() => {
    setStonePath(channel.path);

    return () => setStonePath([]);
  }, [channel, setStonePath]);

  return (
    <Layout title={channel?.title && `${channel.title} | JOGL`} desc={channel.description ?? ''}>
      <ContainerStyled>
        <DiscussionDetails discussion={channel} />
      </ContainerStyled>
    </Layout>
  );
};

export default Channel;
