import { useQuery, keepPreviousData, useQueryClient } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import useTranslation from 'next-translate/useTranslation';
import { useState } from 'react';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { SpaceTabHeader } from '../../Common/space/spaceTabHeader/spaceTabHeader';
import { SpaceContentStyled } from '../../Common/space/space.styles';
import useDebounce from '~/src/hooks/useDebounce';
import { useRouter } from 'next/router';
import Loader from '../../Common/loader/loader';
import { DiscussionsList } from '../../discussions/list/list';
import Layout from '~/src/components/Layout/layout/layout';

export const Browse = () => {
  const { t } = useTranslation('common');
  const [searchText, setSearchText] = useState('');
  const debouncedSearch = useDebounce(searchText, 300);
  const router = useRouter();
  const spaceId = router.query.id as string;
  const queryClient = useQueryClient();

  const { data, isLoading, refetch, isPlaceholderData } = useQuery({
    queryKey: [QUERY_KEYS.nodeChannelsList, debouncedSearch, spaceId],
    queryFn: async () => {
      const response = await api.channels.channelsDetail(spaceId, { Search: debouncedSearch });
      return response.data;
    },
    placeholderData: keepPreviousData,
  });

  const refreshData = () => {
    queryClient.invalidateQueries({
      queryKey: [QUERY_KEYS.nodesDetailDetail],
    });

    refetch();
  };

  if (isLoading && !data && !isPlaceholderData) {
    return <Loader />;
  }
  return (
    <Layout title="Needs">
      <SpaceTabHeader
        setSearchText={setSearchText}
        searchText={searchText}
        title={t('channel.all')}
        showSearch={!!searchText || !!data?.length}
      />

      <SpaceContentStyled>
        <DiscussionsList discussions={data} onJoin={() => refreshData()} />
      </SpaceContentStyled>
    </Layout>
  );
};
