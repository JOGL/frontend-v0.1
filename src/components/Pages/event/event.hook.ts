import { EventModel } from '~/__generated__/types';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';
import durationPlugin from 'dayjs/plugin/duration';
import useTranslation from 'next-translate/useTranslation';

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.extend(durationPlugin);

export function useEventDateInfo(event: EventModel) {
  const { t } = useTranslation('common');
  // Get the user's local timezone
  const userTimezone = dayjs.tz.guess();

  // Get current time in user's timezone
  const now = dayjs().tz(userTimezone);

  const startDate = dayjs(event.start).tz(userTimezone);
  const endDate = dayjs(event.end).tz(userTimezone);

  const sameDay = startDate.isSame(endDate, 'day');

  const formatDate = (date: dayjs.Dayjs, format: string) => {
    return date.format(format);
  };

  // Calculate duration
  const durationMs = endDate.diff(startDate);
  const durationObj = dayjs.duration(durationMs);
  const hours = Math.floor(durationObj.asHours());
  const minutes = durationObj.minutes();

  let durationString = '';
  if (sameDay) {
    if (hours > 0) {
      durationString = t('nHours', { count: hours });
    }
    if (minutes > 0) {
      if (durationString) durationString += ' ';
      durationString += t('nMinutes', { count: minutes });
    }
  } else {
    durationString = t('nHours', { count: Math.floor(durationObj.asHours()) });
  }

  let status: 'upcoming' | 'ongoing' | 'finished';
  if (now.isBefore(startDate)) {
    status = 'upcoming';
  } else if (now.isAfter(endDate)) {
    status = 'finished';
  } else {
    status = 'ongoing';
  }

  const result = {
    startDate: formatDate(startDate, 'ddd DD MMM, YYYY | HH:mm'),
    endDate: sameDay ? formatDate(endDate, 'HH:mm') : formatDate(endDate, 'ddd DD MMM, YYYY | HH:mm'),
    isUpcoming: status === 'upcoming',
    isOngoing: status === 'ongoing',
    isFinished: status === 'finished',
    duration: durationString,
  };

  return result;
}
