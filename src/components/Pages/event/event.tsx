import React, { useEffect } from 'react';
import Layout from '~/src/components/Layout/layout/layout';
import EventHeader from './eventHeader/eventHeader';
import { useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import Loading from './loading/loading';
import { useRouter } from 'next/router';
import { AxiosError } from 'axios';
import useUser from '~/src/hooks/useUser';
import EventTabs from './eventTabs/eventTabs';
import { PageWrapperStyled } from '../pages.styles';
import { useFeedEntity } from '../../Common/feedEntity/feedEntity.hooks';

const Event = () => {
  const router = useRouter();
  const eventId = router.query.id as string;
  const { user } = useUser();
  const [markFeedAsOpened] = useFeedEntity();

  const { data: event, isFetched, isLoading } = useQuery({
    queryKey: [QUERY_KEYS.eventData, eventId],
    queryFn: async () => {
      const response = await api.events.eventsDetailDetail(eventId);
      return response.data;
    },
    retry: (failureCount, error: AxiosError) => {
      if (error.response?.status === 404 || error.response?.status === 403) {
        return false;
      }

      return failureCount < 3;
    },
    enabled: !!eventId,
  });

  useEffect(() => {
    if (event) markFeedAsOpened(event);
  }, [event]);

  if (!event && isFetched) {
    !user ? router.push(`/signin?redirectUrl=/event/${router.query.id}`) : router.push('/');
    return;
  }

  if (isLoading) {
    return <Loading />;
  }

  return (
    <Layout title={event?.title && `${event.title} | JOGL`} desc={event?.description} img={event?.banner_url}>
      <PageWrapperStyled>
        <EventHeader event={event} />
        <EventTabs event={event} />
      </PageWrapperStyled>
    </Layout>
  );
};

export default Event;
