import styled from '@emotion/styled';
import { Badge, Flex, Space, Table, Tag, Typography } from 'antd';
import { AttendanceStatus } from '~/__generated__/types';
import { getColorToRGB } from '~/src/utils/getColorToRGB';

export const TableStyled = styled(Table)`
  cursor: pointer;

  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    overflow-x: auto;
    table {
      width: unset;
    }
  }
`;

export const TypographyTextStyled = styled(Typography.Text)`
  color: ${({ theme }) => theme.token.colorPrimary} !important;
`;

export const TagStyled = styled(Tag)<{ secondary?: boolean }>`
  text-align: center;
  margin-bottom: ${({ theme }) => theme.token.sizeXXS}px;
  padding: ${({ theme }) => theme.token.paddingXS}px;
  background-color: ${(props) =>
    props.secondary
      ? getColorToRGB(props.theme.token.neutral11, 0.15)
      : getColorToRGB(props.theme.token.colorPrimary, 0.2)};
  span {
    color: ${(props) => (props.secondary ? props.theme.token.neutral11 : props.theme.token.colorPrimary)};
  }
`;

export const BadgeStyled = styled(Badge)<{ type: string }>`
  border-radius: 20px;
  padding: 5px;
  left: -8px;
  bottom: -15px;
  background-color: ${(prop) =>
    prop.type === AttendanceStatus.Yes
      ? prop.theme.token.colorSuccessBg
      : prop.type === AttendanceStatus.No
      ? prop.theme.token.colorErrorBg
      : prop.theme.token.neutral5};
  svg {
    color: ${(prop) =>
      prop.type === AttendanceStatus.Yes
        ? prop.theme.token.colorSuccess
        : prop.type === AttendanceStatus.No
        ? prop.theme.token.colorError
        : prop.theme.token.neutral10};
  }
`;

export const SpaceStyled = styled(Space)`
  margin-left: auto;
`;
