import { useQuery } from '@tanstack/react-query';
import { FC, useState } from 'react';
import {
  AttendanceAccessLevel,
  AttendanceStatus,
  EventAttendanceModel,
  EventModel,
  UserMiniModel,
} from '~/__generated__/types';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { BadgeStyled, SpaceStyled, TableStyled, TagStyled, TypographyTextStyled } from './participants.styles';
import { Avatar, Button, Dropdown, Flex, Input, MenuProps, Spin, Typography } from 'antd';
import {
  CheckOutlined,
  CloseOutlined,
  MailOutlined,
  MoreOutlined,
  PlusOutlined,
  QuestionOutlined,
  UserOutlined,
} from '@ant-design/icons';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import useDebounce from '~/src/hooks/useDebounce';
import useUser from '~/src/hooks/useUser';
import EventInvite from './evnetInvite';
import { AlignType } from 'rc-table/lib/interface';
import { useManageParticipants } from './participants.hook';

const Participants: FC<{ event: EventModel }> = ({ event }) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const [showInviteModal, setShowInviteModal] = useState(false);
  const [search, setSearch] = useState('');
  const debouncedSearch = useDebounce(search, 300);
  const { user } = useUser();
  const { deleteParticipants, editAccessLevel, editLabel } = useManageParticipants();

  const { data: participants, isLoading } = useQuery({
    queryKey: [QUERY_KEYS.eventParticipants, event.id, debouncedSearch],
    queryFn: async () => {
      const response = await api.events.attendancesDetail(event.id, { Search: debouncedSearch });
      return response.data;
    },

    enabled: !!event.id,
  });

  if (isLoading) {
    return (
      <Flex style={{ width: '100%', height: '100%' }} justify="center" align="center">
        <Spin />
      </Flex>
    );
  }

  const canManage =
    participants?.find((participant) => participant.user?.id === user?.id)?.access_level ===
    AttendanceAccessLevel.Admin;

  const columns = [
    {
      dataIndex: 'user',
      ellipsis: true,
      render: (user: UserMiniModel, item: EventAttendanceModel) => {
        return (
          <>
            {user ? (
              <Flex gap="small">
                <div>
                  <Avatar size="large" src={user?.logo_url} icon={<UserOutlined />} alt={user.username} />
                  <BadgeStyled type={item.status}>
                    {item.status === AttendanceStatus.Yes ? (
                      <CheckOutlined />
                    ) : item.status === AttendanceStatus.No ? (
                      <CloseOutlined />
                    ) : (
                      <QuestionOutlined />
                    )}
                  </BadgeStyled>
                </div>

                <Flex vertical gap={0}>
                  <TypographyTextStyled strong>
                    {user.first_name} {user.last_name}
                  </TypographyTextStyled>
                  <Typography.Text type="secondary">@{user.username}</Typography.Text>
                </Flex>
              </Flex>
            ) : (
              <Flex gap="small" align="center">
                <div>
                  <Avatar size="large" icon={<MailOutlined />} alt={item.user_email} />
                  <BadgeStyled type={item.status}>
                    {item.status === AttendanceStatus.Yes ? (
                      <CheckOutlined />
                    ) : item.status === AttendanceStatus.No ? (
                      <CloseOutlined />
                    ) : (
                      <QuestionOutlined />
                    )}
                  </BadgeStyled>
                </div>
                <Typography.Text>{item.user_email}</Typography.Text>
              </Flex>
            )}
          </>
        );
      },
    },
    {
      dataIndex: 'type',
      render: (id: string, item: EventAttendanceModel) => {
        return (
          <Flex gap="small">
            {item.access_level === AttendanceAccessLevel.Admin && (
              <TagStyled secondary={true}>
                <Typography.Text ellipsis={{ tooltip: t('organizer.one') }}>{t('organizer.one')}</Typography.Text>
              </TagStyled>
            )}
            {item.labels?.includes('speaker') && (
              <TagStyled>
                <Typography.Text ellipsis={{ tooltip: t('speaker.one') }}>{t('speaker.one')}</Typography.Text>
              </TagStyled>
            )}
          </Flex>
        );
      },
    },
    {
      dataIndex: 'id',
      align: 'right' as AlignType,
      render: (id: string, item: EventAttendanceModel) => {
        if (!canManage) return null;

        const menuItems = [
          item.access_level !== AttendanceAccessLevel.Admin && {
            label: t('addAsOrganizer'),
            key: 'addAsOrganizer',
            onClick: (e) => {
              e.domEvent.stopPropagation();
              editAccessLevel.mutate({ id: item.id, type: { access_level: AttendanceAccessLevel.Admin } });
            },
          },
          !item.labels?.includes('speaker') && {
            label: t('addAsSpeaker'),
            key: 'addAsSpeaker',
            onClick: (e) => {
              e.domEvent.stopPropagation();
              editLabel.mutate({ id: item.id, type: ['speaker'] });
            },
          },

          item.access_level == AttendanceAccessLevel.Admin &&
            item.created_by_user_id !== item.user?.id && {
              label: t('removeOrganizerRole'),
              key: 'removeOrganizerRole',
              danger: true,
              onClick: (e) => {
                e.domEvent.stopPropagation();
                editAccessLevel.mutate({ id: item.id, type: { access_level: AttendanceAccessLevel.Member } });
              },
            },

          item.labels?.includes('speaker') && {
            label: t('removeSpeakerTag'),
            key: 'removeSpeakerTag',
            danger: true,
            onClick: (e) => {
              e.domEvent.stopPropagation();
              editLabel.mutate({ id: item.id, type: [] });
            },
          },

          ...(item.created_by_user_id !== item.user?.id
            ? [
                {
                  label: t('removeParticipant'),
                  key: 'delete',
                  danger: true,
                  onClick: (e) => {
                    e.domEvent.stopPropagation();
                    deleteParticipants.mutate(item.id);
                  },
                },
              ]
            : []),
        ];

        return (
          <Dropdown
            trigger={['click']}
            placement="bottomRight"
            menu={{
              onClick: ({ domEvent }) => {
                domEvent.stopPropagation();
              },
              items: menuItems as MenuProps['items'],
            }}
          >
            <Button
              type="text"
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
              }}
            >
              <MoreOutlined />
            </Button>
          </Dropdown>
        );
      },
    },
  ];

  return (
    <>
      <Flex gap="small" align="flex-end">
        <SpaceStyled>
          <Input.Search
            placeholder={t('action.search')}
            onChange={(e) => {
              setSearch(e.target.value);
            }}
            onSearch={(value) => {
              setSearch(value);
            }}
            value={search}
          />
        </SpaceStyled>

        {canManage && (
          <Button icon={<PlusOutlined />} onClick={() => setShowInviteModal(true)}>
            {t('action.add')}
          </Button>
        )}
      </Flex>
      <TableStyled
        showHeader={false}
        dataSource={participants}
        columns={columns}
        pagination={false}
        onRow={(record: EventAttendanceModel) => {
          return {
            onClick() {
              record.user && router.push(`/user/${record.user.id}`);
            },
          };
        }}
      />
      {showInviteModal && (
        <EventInvite participants={participants} event={event} onClose={() => setShowInviteModal(false)} />
      )}
    </>
  );
};

export default Participants;
