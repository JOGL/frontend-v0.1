import { useMutation, useQueryClient } from '@tanstack/react-query';
import { message } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { AccessLevel, AttendanceAccessLevel, EventAttendanceAccessLevelModel } from '~/__generated__/types';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';

export function useManageParticipants() {
  const { t } = useTranslation('common');
  const queryClient = useQueryClient();

  const deleteParticipants = useMutation({
    mutationFn: async (id: string) => {
      await api.events.attendancesDeleteDetail(id);
    },
    onSuccess: () => {
      message.success(t('inviteRemovedSuccess'));

      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.eventParticipants],
      });
    },
    onError: () => {
      message.error(t('inviteRemovedError'));
    },
  });

  const editAccessLevel = useMutation({
    mutationFn: async (data: { id: string; type: EventAttendanceAccessLevelModel }) => {
      await api.events.attendancesAccessLevelCreate(data.id, data.type);
    },
    onSuccess: () => {
      message.success(t('userRoleUpdateSuccess'));
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.eventParticipants],
      });
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const editLabel = useMutation({
    mutationFn: async (data: { id: string; type: string[] }) => {
      await api.events.attendancesLabelsCreate(data.id, data.type);
    },
    onSuccess: () => {
      message.success(t('speakersChangedSuccess'));
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.eventParticipants],
      });
    },
    onError: () => {
      message.error(t('speakersChangedError'));
    },
  });

  return {
    deleteParticipants,
    editAccessLevel,
    editLabel,
  };
}
