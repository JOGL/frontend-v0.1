import { UserOutlined } from '@ant-design/icons';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { Avatar, Col, Empty, Form, Modal, Row, Select, Typography, message } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { FC, useState } from 'react';
import { EventAttendanceModel, EventAttendanceUpsertModel, EventModel, UserMiniModel } from '~/__generated__/types';
import useDebounce from '~/src/hooks/useDebounce';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';

const { Option } = Select;
const FORM_ID = 'event-invite-form';

const EventInvite: FC<{ event: EventModel; participants?: EventAttendanceModel[]; onClose: () => void }> = ({
  event,
  participants,
  onClose,
}) => {
  const { t } = useTranslation('common');
  const [form] = Form.useForm();
  const [userSearch, setUserSearch] = useState('');
  const [selectedUsers, setSelectedUsers] = useState<UserMiniModel[]>([]);
  const [selectedEmails, setSelectedEmails] = useState<string[]>([]);
  const [inviteRole, setInviteRole] = useState('member');
  const debouncedSearch = useDebounce(userSearch, 300);
  const queryClient = useQueryClient();

  const { data: users, isFetched } = useQuery({
    queryKey: [QUERY_KEYS.usersSearchAutocompleteList, { search: debouncedSearch }],
    queryFn: async () => {
      const response = await api.users.autocompleteList({
        Search: debouncedSearch,
      });
      return response.data;
    },
    enabled: debouncedSearch.length > 0,
  });

  const userOptions =
    users?.filter((searchedUser) => {
      const isAlreadySelected = selectedUsers.some((selectedUser) => selectedUser.id === searchedUser.id);
      const isAlreadyParticipant = participants?.some((participant) => participant.user?.id === searchedUser.id);

      return !isAlreadySelected && !isAlreadyParticipant;
    }) || [];

  const handleCancel = () => {
    form.resetFields();
    onClose();
  };
  const mutation = useMutation({
    mutationFn: async (data: EventAttendanceUpsertModel[]) => {
      const response = await api.events.inviteBatchCreate(event.id, data);
      return response.data;
    },
    onSuccess: (id) => {
      message.success(t('invitesSent'));
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.eventParticipants],
      });
      onClose();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
      onClose();
    },
  });

  const onFinish = (values) => {
    const invitedUsers =
      selectedUsers.map((selectedUser) => {
        return {
          access_level: inviteRole,
          community_entity_id: null,
          labels: [],
          origin_community_entity_id: null,
          user_email: null,
          user_id: selectedUser.id,
        };
      }) || [];
    const invitedEmails =
      values.userEmail?.map((userEmail) => {
        return {
          access_level: inviteRole,
          community_entity_id: null,
          labels: [],
          origin_community_entity_id: null,
          user_email: userEmail,
          user_id: null,
        };
      }) || [];
    mutation.mutate([...invitedUsers, ...invitedEmails]);
  };

  const handleSearch = (value: string) => {
    setUserSearch(value);
  };

  const handleUserSelect = (value: string, option: any) => {
    setSelectedUsers((prev) => [...prev, option.item]);
  };

  const handleUserDeselect = (item) => {
    setSelectedUsers((prev) => prev.filter((user) => user.id !== item.value));
  };

  const handleEmailChange = (emails: string[]) => {
    setSelectedEmails(emails);
  };

  return (
    <Modal
      open
      title={t('invite.other')}
      okButtonProps={{
        htmlType: 'submit',
        form: FORM_ID,
        disabled: !selectedUsers.length && !selectedEmails.length,
      }}
      onCancel={handleCancel}
      okText={t('invite.one')}
    >
      <Form layout="vertical" form={form} id={FORM_ID} onFinish={onFinish}>
        <Row gutter={16}>
          <Col span={18}>
            <Form.Item name="invitedUsers" label={t('inviteToJoin')}>
              <Select
                mode="multiple"
                style={{ width: '100%' }}
                placeholder={t('addPeople')}
                onSearch={handleSearch}
                onSelect={handleUserSelect}
                onDeselect={handleUserDeselect}
                notFoundContent={userSearch.length && isFetched && !userOptions.length ? <Empty /> : null}
                filterOption={false}
                value={selectedUsers.map((user) => user.id)}
                labelInValue
                optionLabelProp="label"
                size="large"
              >
                {userOptions.map((user) => (
                  <Option key={user.id} value={user.id} item={user} label={`${user.first_name} ${user.last_name}`}>
                    <Row>
                      <Col span={4}>
                        <Avatar size="small" shape="square" icon={<UserOutlined />} src={user?.logo_url} />
                      </Col>
                      <Col span={10}>
                        <Typography.Text
                          type="secondary"
                          ellipsis
                        >{`${user.first_name} ${user.last_name}`}</Typography.Text>
                      </Col>
                      <Col span={10}>
                        <Typography.Text type="secondary" ellipsis>
                          @{user.username}
                        </Typography.Text>
                      </Col>
                    </Row>
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item name="inviteRole" label={t('role.one')}>
              <Select
                defaultValue="member"
                onChange={(value) => setInviteRole(value)}
                style={{ width: '100%' }}
                size="large"
              >
                <Option value="member">{t('member.one')}</Option>
                <Option value="admin">{t('organizer.one')}</Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Form.Item name="userEmail" label={t('byEmailAddress')}>
          <Select
            placeholder={t('enterEmails')}
            style={{ width: '100%' }}
            mode="tags"
            size="large"
            onChange={handleEmailChange}
          />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default EventInvite;
