import styled from '@emotion/styled';
import { Tabs } from 'antd';

export const ContentWrapperStyled = styled.div`
  position: relative;
  padding: ${({ theme }) => theme.token.sizeLG}px;
`;
