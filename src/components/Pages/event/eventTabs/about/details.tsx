import { FC } from 'react';
import { EventModel, EventVisibility } from '~/__generated__/types';
import { Flex, Typography, Tooltip } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { EnvironmentOutlined, VideoCameraOutlined } from '@ant-design/icons';
import { useEventDateInfo } from '../../event.hook';
import Link from 'next/link';
import { entityItem } from '~/src/utils/utils';
import { LinkStyled, TypographyTextNoWrapStyled } from './about.styles';

const Details: FC<{ event: EventModel }> = ({ event }) => {
  const { t } = useTranslation('common');
  const eventDate = useEventDateInfo(event);
  const meetingUrl = event.meeting_url || event.generated_meeting_url;
  return (
    <Flex vertical gap="middle">
      <Typography.Text strong>{t('details')}</Typography.Text>
      <Flex vertical gap="small">
        {event.location && (
          <Flex gap="small">
            <TypographyTextNoWrapStyled>{t('location')}</TypographyTextNoWrapStyled>
            <Tooltip title={event.location.name}>
              <LinkStyled href={event.location.url}>
                <EnvironmentOutlined />
                {event.location.name}
              </LinkStyled>
            </Tooltip>
          </Flex>
        )}
        {meetingUrl && (
          <Flex gap="small">
            <TypographyTextNoWrapStyled>{t('meetingLink')}</TypographyTextNoWrapStyled>
            <LinkStyled href={meetingUrl}>
              <Flex>
                <VideoCameraOutlined />
                <Typography.Text ellipsis={{ tooltip: meetingUrl }}>
                  {meetingUrl.includes('meet.google.com') ? 'Google.meet' : meetingUrl}
                </Typography.Text>
              </Flex>
            </LinkStyled>
          </Flex>
        )}

        <Flex gap="small">
          <TypographyTextNoWrapStyled>{t('duration')}</TypographyTextNoWrapStyled>
          <Typography.Text>{eventDate.duration}</Typography.Text>
        </Flex>

        <Flex gap="small">
          {event.visibility === EventVisibility.Container ? (
            <Flex gap="small">
              <TypographyTextNoWrapStyled>{t('openToAllMembersOf')}</TypographyTextNoWrapStyled>
              {event?.feed_entity?.entity_type && (
                <Link href={`/${entityItem[event?.feed_entity?.entity_type].front_path}/${event?.feed_entity?.id}`}>
                  {event?.feed_entity?.title}
                </Link>
              )}
            </Flex>
          ) : (
            <TypographyTextNoWrapStyled>{t(event.visibility)}</TypographyTextNoWrapStyled>
          )}
        </Flex>
      </Flex>
    </Flex>
  );
};

export default Details;
