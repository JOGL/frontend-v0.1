import { UserOutlined } from '@ant-design/icons';
import { Avatar, Flex, Typography } from 'antd';
import { FC } from 'react';
import { UserMiniModel } from '~/__generated__/types';
import { TypographyTextStyled } from './about.styles';

const UserInfo: FC<{ user: UserMiniModel }> = ({ user }) => {
  return (
    <Flex vertical gap="middle">
      <Flex gap="small">
        <Avatar size="large" src={user.logo_url} icon={<UserOutlined />} alt={user.username} />
        <Flex vertical gap={0}>
          <TypographyTextStyled strong>
            {user.first_name} {user.last_name}
          </TypographyTextStyled>
          <Typography.Text type="secondary">@{user.username}</Typography.Text>
        </Flex>
      </Flex>
      {user.short_bio && <Typography.Text>{user.short_bio}</Typography.Text>}
    </Flex>
  );
};

export default UserInfo;
