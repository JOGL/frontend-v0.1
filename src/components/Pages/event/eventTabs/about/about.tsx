import { FC } from 'react';
import { AttendanceAccessLevel, EventModel } from '~/__generated__/types';
import { Col, Divider, Flex, Typography, Grid } from 'antd';
import { RowStyled, TagStyled } from './about.styles';
import useTranslation from 'next-translate/useTranslation';
import Details from './details';
import { useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import UserInfo from './userInfo';
import ReadOnlyEditor from '~/src/components/tiptap/readOnlyEditor/readOnlyEditor';
const { useBreakpoint } = Grid;

const About: FC<{ event: EventModel }> = ({ event }) => {
  const { t } = useTranslation('common');
  const screens = useBreakpoint();

  const { data: attendances } = useQuery({
    queryKey: [QUERY_KEYS.eventParticipants, event.id],
    queryFn: async () => {
      const response = await api.events.attendancesDetail(event.id);
      return response.data;
    },

    enabled: !!event.id,
  });
  const speakers = attendances?.filter((user) => user.labels?.includes('speaker'));
  const organizers = attendances?.filter((user) => user.access_level === AttendanceAccessLevel.Admin);

  return (
    <RowStyled gutter={32}>
      <Col span={screens.xs ? 24 : 12}>
        <Flex vertical gap="large">
          <Details event={event} />
          <Divider />
          {event.description && (
            <Flex vertical gap="large">
              <Typography.Text strong>{t('description')}</Typography.Text>
              <Typography.Text>
                <ReadOnlyEditor content={event.description ?? ''} />
              </Typography.Text>
              <Divider />
            </Flex>
          )}
        </Flex>
      </Col>
      <Col span={screens.xs ? 24 : 12}>
        {!!organizers?.length && (
          <Flex vertical gap="large">
            <Typography.Text strong>{t('organizer.other')}</Typography.Text>
            {organizers
              .filter((item) => item.user)
              .map((item) => (
                <UserInfo key={item.user.id} user={item.user} />
              ))}
            <Divider />
          </Flex>
        )}

        {!!speakers?.length && (
          <Flex vertical gap="large">
            <Typography.Text strong>{t('speaker.other')}</Typography.Text>
            {speakers
              .filter((item) => item.user)
              .map((item) => (
                <UserInfo key={item.user.id} user={item.user} />
              ))}
            <Divider />
          </Flex>
        )}

        {!!event.keywords?.length && (
          <Flex vertical gap="large">
            <Typography.Text strong>{t('keywords')}</Typography.Text>
            <Flex wrap>
              {event.keywords.map((keyword, index) => (
                <TagStyled key={index}>
                  <Typography.Text>{keyword}</Typography.Text>
                </TagStyled>
              ))}
            </Flex>
            <Divider />
          </Flex>
        )}
      </Col>
    </RowStyled>
  );
};

export default About;
