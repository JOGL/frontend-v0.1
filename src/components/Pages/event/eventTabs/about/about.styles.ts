import styled from '@emotion/styled';
import { Row, Tag, Typography } from 'antd';
import Link from 'next/link';
import { getColorToRGB } from '~/src/utils/getColorToRGB';

export const TypographyTextStyled = styled(Typography.Text)`
  color: ${({ theme }) => theme.token.colorPrimary} !important;
`;

export const TagStyled = styled(Tag)`
  margin-bottom: ${({ theme }) => theme.token.sizeSM}px;
  background-color: ${({ theme }) => getColorToRGB(theme.token.colorPrimary, 0.1)};
  span {
    color: ${({ theme }) => theme.token.colorPrimary};
  }
`;

export const RowStyled = styled(Row)`
  margin-top: ${({ theme }) => theme.token.sizeXL}px;
`;

export const TypographyTextNoWrapStyled = styled(Typography.Text)`
  white-space: nowrap;
`;

export const LinkStyled = styled(Link)`
  overflow: hidden;
  text-overflow: ellipsis;
  text-align: left;
  span {
    color: ${({ theme }) => theme.token.colorPrimary};
  }
  svg {
    margin-right: ${({ theme }) => theme.token.marginXS}px;
  }
`;
