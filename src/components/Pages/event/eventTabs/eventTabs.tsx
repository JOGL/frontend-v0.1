import { FC, useEffect, useState } from 'react';
import { EventModel } from '~/__generated__/types';
import { ContentWrapperStyled } from './eventTabs.styles';
import { Badge, Tabs, TabsProps, theme } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import About from './about/about';
import Participants from './participants/participants';
import { CommentOutlined } from '@ant-design/icons';
import Feed from '~/src/components/discussionFeed/feed/feed';
import { useRouter } from 'next/router';
import useUser from '~/src/hooks/useUser';
import { TabsStyled } from '~/src/components/discussionFeed/feed/feed.styles';

const EventTabs: FC<{ event: EventModel }> = ({ event }) => {
  const { t } = useTranslation('common');
  const { token } = theme.useToken();
  const badgeCount = (event?.feed_stats?.new_thread_activity_count ?? 0) + (event?.feed_stats?.new_mention_count ?? 0);
  const router = useRouter();
  const [tab, setTab] = useState<string>((router.query.tab as string) ?? 'about');
  const { user } = useUser();

  const handleTabChange = (tab: string) => {
    setTab(tab);
    router.replace(
      {
        query: { ...router.query, tab: tab },
      },
      undefined,
      { shallow: true, scroll: false }
    );
  };

  const items: TabsProps['items'] = [
    {
      key: 'about',
      label: t('about'),
      children: <About event={event} />,
    },
    {
      key: 'participants',
      label: t('participant.other'),
      children: <Participants event={event} />,
    },
    ...(!event?.feed_stats.post_count && !user
      ? []
      : [
          {
            key: 'discussion',
            label: (
              <Badge count={badgeCount} size="small" offset={[4, -4]} color={token.colorPrimary}>
                <CommentOutlined /> {t('discussion.one')}
              </Badge>
            ),
            children: <Feed feedId={event?.id} />,
          },
        ]),
  ];
  return (
    <ContentWrapperStyled>
      <TabsStyled activeKey={tab} items={items} onChange={handleTabChange} />
    </ContentWrapperStyled>
  );
};

export default EventTabs;
