import React, { FC, useState } from 'react';
import { AttendanceStatus, EventModel } from '~/__generated__/types';
import { ContainerStyled, DateStyled, EventBannerStyled, EventTitleStyled } from './eventHeader.styles';
import { Avatar, Dropdown, Flex, Tooltip, Typography, Button, message } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import { entityItem } from '~/src/utils/utils';
import { useEventDateInfo } from '../event.hook';
import { useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import { LinkOutlined, ShareAltOutlined, UserOutlined } from '@ant-design/icons';
import EventType from '../eventType/eventType';
import EventAttendanceStatus from './attendanceStatus/attendanceStatus';

const EventHeader: FC<{ event: EventModel }> = ({ event }) => {
  const { t } = useTranslation('common');
  const [copying, setCopying] = useState(false);
  const eventDate = useEventDateInfo(event);

  const { data: attendances } = useQuery({
    queryKey: [QUERY_KEYS.eventAttendances, event.id],
    queryFn: async () => {
      const response = await api.events.attendancesDetail(event.id, { status: AttendanceStatus.Yes });
      return response.data;
    },

    enabled: !!event.id,
  });

  const copyToClipboard = async () => {
    setCopying(true);
    try {
      const currentUrl = window.location.href;
      await navigator.clipboard.writeText(currentUrl);
      message.success(t('linkCopied'));
    } catch (err) {
      message.error(t('failedToCopyLink'));
    } finally {
      setCopying(false);
    }
  };

  return (
    <>
      {event.banner_url && <EventBannerStyled src={event.banner_url} />}
      <EventTitleStyled>
        <Typography.Text>{t('eventIn')} </Typography.Text>
        {event?.feed_entity?.entity_type && (
          <Link href={`/${entityItem[event?.feed_entity?.entity_type].front_path}/${event?.feed_entity?.id}`}>
            <Typography.Text strong>{event?.feed_entity?.title}</Typography.Text>
          </Link>
        )}
      </EventTitleStyled>
      <ContainerStyled>
        <Flex justify="space-between">
          <DateStyled vertical gap={0}>
            <Typography.Title level={5}>
              {eventDate.startDate} - {eventDate.endDate}
            </Typography.Title>
            <Typography.Title level={4}>{event.title}</Typography.Title>
          </DateStyled>
          <Dropdown
            menu={{
              items: [
                {
                  label: (
                    <Flex gap="small">
                      <LinkOutlined />
                      <Typography.Text> {t('action.copyLink')}</Typography.Text>
                    </Flex>
                  ),
                  key: '1',
                },
              ],
              onClick: copyToClipboard,
            }}
            trigger={['click']}
          >
            <Button icon={<ShareAltOutlined />} />
          </Dropdown>
        </Flex>

        <Flex gap="middle" vertical>
          <EventType event={event} />
          {!!attendances?.length && (
            <Avatar.Group max={{ count: 10 }}>
              {attendances.map((attendance) => (
                <Tooltip
                  key={attendance.id}
                  title={
                    attendance.user?.first_name
                      ? `${attendance.user?.first_name} ${attendance.user?.last_name}`
                      : attendance.user_email
                  }
                  placement="top"
                >
                  <Avatar
                    src={attendance.user?.logo_url}
                    icon={<UserOutlined />}
                    alt={attendance.user?.username || attendance.user_email}
                  />
                </Tooltip>
              ))}
            </Avatar.Group>
          )}
          <EventAttendanceStatus event={event} />
        </Flex>
      </ContainerStyled>
    </>
  );
};

export default EventHeader;
