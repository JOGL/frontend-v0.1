import styled from '@emotion/styled';
import { Button, Flex } from 'antd';

export const FlexStyled = styled(Flex)`
  background-color: ${({ theme }) => theme.token['purple-2']};
  border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;
  padding: ${({ theme }) => theme.token.paddingSM}px;
`;

export const ButtonStyled = styled(Button)`
  width: fit-content;
`;
