import { Button, Flex, Form, Input, Modal, Typography, message } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import React, { FC, useState } from 'react';
import { ButtonStyled } from '../attendanceStatus.styles';
import Link from 'next/link';
import { EMAIL_REGEX } from '~/src/utils/utils';
import { useMutation } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { useRouter } from 'next/router';
import { AxiosError } from 'axios';

const AttendanceRSVP: FC<{ eventId: string }> = ({ eventId }) => {
  const { t } = useTranslation('common');
  const [showRSVPModal, setShowRSVPModal] = useState(false);
  const [form] = Form.useForm();
  const router = useRouter();

  const mutation = useMutation({
    mutationFn: async (email: string) => {
      const response = await api.events.attendEmailCreate(eventId, { email });
      return response.data;
    },
    onSuccess: () => {
      setShowRSVPModal(false);
      message.success(t('invitesSent'));
    },
    onError: (error: AxiosError) => {
      if (error.response?.status === 409) {
        message.error(t('error.thisEmailIsAlreadyInTheParticipantsList'));
      } else {
        message.error(t('error.somethingWentWrong'));
      }
      setShowRSVPModal(false);
    },
  });

  const onFinish = ({ email }) => {
    mutation.mutate(email);
  };

  const validateEmailConfirmation = (_: any, value: string) => {
    const email = form.getFieldValue('email');
    if (email && value && email !== value) {
      return Promise.reject(new Error(t('theTwoEmailsDoNotMatch')));
    }
    return Promise.resolve();
  };
  return (
    <>
      <ButtonStyled type="primary" onClick={() => setShowRSVPModal(true)}>
        {t('RSVP')}
      </ButtonStyled>
      {showRSVPModal && (
        <Modal open onCancel={() => setShowRSVPModal(false)} footer={null}>
          <Flex vertical gap="large">
            <Flex vertical gap="small">
              <Typography.Text>
                {t('toCreateAnAccount')}
                <Link href={`/signup?redirectUrl=${router.asPath}`}>{t('action.signUp')}</Link>
              </Typography.Text>
              <Typography.Text>{t('registerToTheEventWithoutCreatingAnAccount')}</Typography.Text>
            </Flex>

            <Form form={form} name="email_form" onFinish={onFinish} layout="vertical">
              <Flex gap="large">
                <Form.Item
                  name="email"
                  rules={[
                    { required: true, message: t('error.valueIsMissing') },
                    {
                      type: 'string',
                      pattern: EMAIL_REGEX,
                      message: t('error.invalidMailAddress'),
                    },
                  ]}
                >
                  <Input placeholder={t('email')} />
                </Form.Item>

                <Form.Item
                  name="confirmEmail"
                  dependencies={['email']}
                  rules={[
                    { required: true, message: t('error.valueIsMissing') },
                    { validator: validateEmailConfirmation },
                  ]}
                >
                  <Input placeholder={t('confirmYourMail')} />
                </Form.Item>
              </Flex>

              <Form.Item>
                <Flex gap="large" align="center">
                  <Button type="primary" htmlType="submit">
                    {t('action.register')}
                  </Button>
                  {t('aCalendarInviteWillBeSentToYourEmail')}
                </Flex>
              </Form.Item>
            </Form>
          </Flex>
        </Modal>
      )}
    </>
  );
};

export default AttendanceRSVP;
