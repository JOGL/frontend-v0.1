import React, { FC } from 'react';
import { AttendanceStatus, EventModel, Permission } from '~/__generated__/types';
import { Button, Dropdown, Flex, Space, Typography, message } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { ButtonStyled, FlexStyled } from './attendanceStatus.styles';
import { DownOutlined, EditOutlined } from '@ant-design/icons';
import { useEventDateInfo } from '../../event.hook';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import useUser from '~/src/hooks/useUser';
import AttendanceRSVP from './attendanceRSVP/attendanceRSVP';
import { useRouter } from 'next/router';

const EventAttendanceStatus: FC<{ event: EventModel }> = ({ event }) => {
  const { t } = useTranslation('common');
  const { user } = useUser();
  const router = useRouter();
  const eventDate = useEventDateInfo(event);
  const userAttendance = event?.user_attendance;
  const queryClient = useQueryClient();

  const mutationAttend = useMutation({
    mutationFn: async () => {
      const response = await api.events.attendCreate(event.id);
      return response.data;
    },
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.eventData],
      });
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.eventAttendances],
      });
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.eventParticipants],
      });
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });
  const { mutate: mutateAttendance, isPending: isAttendancePending } = useMutation({
    mutationFn: async (type: AttendanceStatus) => {
      if (type === AttendanceStatus.Yes) {
        const response = await api.events.attendancesAcceptCreate(userAttendance.id);
        return response.data;
      }
      const response = await api.events.attendancesRejectCreate(userAttendance.id);
      return response.data;
    },
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.eventData],
      });
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.eventAttendances],
      });
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.eventParticipants],
      });
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const menuProps = {
    items: [
      {
        label: t('going'),
        key: AttendanceStatus.Yes,
      },
      {
        label: t('notGoing'),
        key: AttendanceStatus.No,
      },
    ],
    onClick: ({ key }: { key: string }) => {
      mutateAttendance(key as AttendanceStatus);
    },
  };

  if (eventDate.isOngoing) {
    return (
      <Flex vertical gap="middle">
        {event.permissions.includes(Permission.Manage) && (
          <ButtonStyled
            icon={<EditOutlined />}
            onClick={() =>
              router.push(
                `/event/create?id=${event?.id}&parentId=${event?.feed_entity?.id}&parentType=${event?.feed_entity?.entity_type}`
              )
            }
          >
            {t('action.manage')}
          </ButtonStyled>
        )}
        <FlexStyled gap="small" align="center" justify="space-between">
          <Typography.Text>{t('thisEventHasStarted')}</Typography.Text>
          {(event.meeting_url || event.generated_meeting_url) && (
            <Button
              type="primary"
              target="_blank"
              onClick={() => router.push(event.meeting_url || event.generated_meeting_url)}
              rel="noopener noreferrer"
            >
              {t('joinEvent')}
            </Button>
          )}
        </FlexStyled>
      </Flex>
    );
  }

  if (eventDate.isFinished) {
    return (
      <Flex vertical gap="middle">
        {event.permissions.includes(Permission.Manage) && (
          <ButtonStyled
            icon={<EditOutlined />}
            onClick={() =>
              router.push(
                `/event/create?id=${event?.id}&parentId=${event?.feed_entity?.id}&parentType=${event?.feed_entity?.entity_type}`
              )
            }
          >
            {t('action.manage')}
          </ButtonStyled>
        )}
        <FlexStyled gap="small" align="center" justify="space-between">
          <Typography.Text>{t('thisEventHasPassed')}</Typography.Text>
        </FlexStyled>
      </Flex>
    );
  }

  if (user && !userAttendance) {
    return (
      <ButtonStyled type="primary" onClick={() => mutationAttend.mutate()}>
        {t('attend')}
      </ButtonStyled>
    );
  }

  if (!userAttendance) {
    return <AttendanceRSVP eventId={event.id} />;
  }

  if (userAttendance.status === AttendanceStatus.Yes || userAttendance.status === AttendanceStatus.No) {
    return (
      <Flex gap="small">
        <Dropdown menu={menuProps} trigger={['click']}>
          <ButtonStyled loading={isAttendancePending}>
            <Space>
              {userAttendance.status === AttendanceStatus.Yes ? t('going') : t('notGoing')}
              <DownOutlined />
            </Space>
          </ButtonStyled>
        </Dropdown>
        {event.permissions.includes(Permission.Manage) && (
          <ButtonStyled
            icon={<EditOutlined />}
            onClick={() =>
              router.push(
                `/event/create?id=${event?.id}&parentId=${event?.feed_entity?.id}&parentType=${event?.feed_entity?.entity_type}`
              )
            }
          >
            {t('action.manage')}
          </ButtonStyled>
        )}
      </Flex>
    );
  }

  return (
    <Flex vertical gap="middle">
      {event.permissions.includes(Permission.Manage) && (
        <ButtonStyled
          icon={<EditOutlined />}
          onClick={() =>
            router.push(
              `/event/create?id=${event?.id}&parentId=${event?.feed_entity?.id}&parentType=${event?.feed_entity?.entity_type}`
            )
          }
        >
          {t('action.manage')}
        </ButtonStyled>
      )}
      <FlexStyled gap="small" align="center" justify="space-between">
        {userAttendance.created_by && (
          <Typography.Text>{`${t('youHaveBeenInvitedToThisEventBy')} ${userAttendance.created_by.first_name} ${
            userAttendance.created_by.last_name
          }`}</Typography.Text>
        )}
        <Space size="small">
          <Button type="primary" onClick={() => mutateAttendance(AttendanceStatus.Yes)}>
            {t('going')}
          </Button>
          <Button onClick={() => mutateAttendance(AttendanceStatus.No)}>{t('notGoing')}</Button>
        </Space>
      </FlexStyled>
    </Flex>
  );
};

export default EventAttendanceStatus;
