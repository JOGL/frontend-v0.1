import styled from '@emotion/styled';
import { Flex } from 'antd';

export const EventBannerStyled = styled.div<{ src: string }>`
  height: 236px;
  background-image: url(${({ src }) => src});
  background-size: cover;
  background-position: center;
  border-radius: ${({ theme }) => theme.token.borderRadiusLG}px ${({ theme }) => theme.token.borderRadiusLG}px 0 0;
  overflow: hidden;
`;

export const EventTitleStyled = styled.div`
  background-color: ${({ theme }) => theme.token.colorPrimary};
  padding: ${({ theme }) => theme.token.sizeMS}px ${({ theme }) => theme.token.sizeLG}px;
  span {
    color: ${({ theme }) => theme.token.neutral1};
  }
  a {
    color: ${({ theme }) => theme.token.neutral1};
    text-decoration: none;
  }
`;

export const ContainerStyled = styled.div`
  padding: ${({ theme }) => theme.token.sizeLG}px;
  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    padding: ${({ theme }) => theme.token.paddingXXS}px;
  }
`;

export const DateStyled = styled(Flex)`
  margin-bottom: ${({ theme }) => theme.token.sizeMD}px;
  h5 {
    color: ${({ theme }) => theme.token.colorPrimary};
  }
  h4 {
    margin-top: 0px !important;
  }
`;
