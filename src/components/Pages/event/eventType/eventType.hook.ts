import { EventVisibility, GeolocationModel } from '~/__generated__/types';
import useTranslation from 'next-translate/useTranslation';

export const useGetEventType = (location: GeolocationModel | null, link: string | null) => {
  const { t } = useTranslation('common');
  if (location && link) {
    return t('eventType.onlineEvent');
  }
  if (!link && location) {
    return t('eventType.inPersonEvent');
  }
  return t('eventType.hybridEvent');
};

export const useGetPrivacy = (visibility: EventVisibility, space?: string) => {
  const { t } = useTranslation('common');
  if (visibility === EventVisibility.Container) {
    return `${t('openToAllMembersOf')} ${space}`;
  }
  return t(visibility);
};
