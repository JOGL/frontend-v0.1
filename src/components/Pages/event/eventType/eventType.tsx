import React, { FC } from 'react';
import { EventModel } from '~/__generated__/types';
import { Flex, Typography, Grid } from 'antd';
import { PrimaryTextStyled } from './eventType.styles';
import { useGetEventType, useGetPrivacy } from './eventType.hook';
const { useBreakpoint } = Grid;

const EventType: FC<{ event: EventModel }> = ({ event }) => {
  const screens = useBreakpoint();
  const isMobile = screens.xs;
  const privacy = useGetPrivacy(event.visibility, event.feed_entity?.title);

  return (
    <Flex gap="small" vertical={isMobile}>
      <PrimaryTextStyled strong>{useGetEventType(event.location, event.meeting_url)}</PrimaryTextStyled>{' '}
      {!isMobile ? ' | ' : ''}
      <Typography.Text ellipsis={{ tooltip: privacy }}>{privacy}</Typography.Text>
    </Flex>
  );
};

export default EventType;
