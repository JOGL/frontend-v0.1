import { Typography } from 'antd';
import styled from '@emotion/styled';

export const PrimaryTextStyled = styled(Typography.Text)`
  color: ${({ theme }) => theme.token.colorPrimary} !important;
`;
