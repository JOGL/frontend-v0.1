import styled from '@emotion/styled';
import { Flex } from 'antd';

export const PageWrapperStyled = styled(Flex)`
  min-height: 80vh;
`;

export const TabContentStyled = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  padding: ${({ theme }) => theme.token.sizeLG}px ${({ theme }) => theme.token.sizeXL}px;
`;
