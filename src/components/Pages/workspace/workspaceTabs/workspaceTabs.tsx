import React, { useEffect, useMemo, useState } from 'react';
import { FeedType, Permission, WorkspaceDetailModel } from '~/__generated__/types';
import { SpaceTab } from '~/src/components/Tools/spaceTabs/spaceTabs';
import useTranslation from 'next-translate/useTranslation';
import { PageWrapperStyled, TabContentStyled } from './workspaceTabs.styles';
import { useRouter } from 'next/router';
import 'intro.js/introjs.css';
import { isMember } from '~/src/utils/utils';
import ResourceList from '~/src/components/Resources/ResourceList';
import ShowCfps from '~/src/components/Tools/ShowCfps';
import useUser from '~/src/hooks/useUser';
import { useModal } from '~/src/contexts/modalContext';
import { HorizontalTabs } from '~/src/components/Tools/spaceTabs/horizontalTabs/horizontalTabs';
import { DiscussionsListWorkspace } from '~/src/components/discussions/workspace/workspace';
import ShowMembers from '~/src/components/Tools/ShowMembers';
import { VerticalTabs } from '~/src/components/Tools/spaceTabs/verticalTabs/verticalTabs';
import { AppstoreAddOutlined, CompassOutlined, FileTextOutlined, ReadOutlined } from '@ant-design/icons';
import { Grid } from 'antd';
import { ShowWorkspaces } from '~/src/components/workspace/showWorkspaces/showWorkspaces';
import EntityDocumentList from '~/src/components/documents/entityDocumentList/entityDocumentList';
import EntityNeedList from '~/src/components/Need/entityNeedList/entityNeedList';
import EntityPaperList from '~/src/components/papers/entityPaperList/entityPaperList';
import EntityEventList from '~/src/components/Event/entityEventList/entityEventList';

const { useBreakpoint } = Grid;

const DEFAULT_TAB = 'feed';
const verticalTabsList = ['documents', 'library', 'needs', 'resources'];

interface Props {
  workspace: WorkspaceDetailModel;
}

export const WorkspaceTabs = ({ workspace }: Props) => {
  const { t } = useTranslation('common');
  const screens = useBreakpoint();
  const router = useRouter();
  const { user } = useUser();
  const { showModal } = useModal();
  const isSubworkspace = workspace.path.some(
    ({ id, entity_type }) => entity_type === FeedType.Workspace && id !== workspace.id
  );
  const limitedVisibility = workspace.content_privacy === 'private' && !isMember(workspace);

  const isAdmin = workspace.user_access?.permissions?.includes(Permission.Manage);
  const hasPapers = workspace.stats?.papers_count_aggregate && workspace.stats.papers_count_aggregate > 0;
  const hasNeeds = workspace.stats?.needs_count_aggregate && workspace.stats.needs_count_aggregate > 0;
  const hasResources = workspace.stats?.resources_count && workspace.stats.resources_count > 0;

  const showLibraryTab = !!(workspace.management?.includes('library_managed_by_any_member') || hasPapers || isAdmin);
  const showDocumentsTab =
    workspace.tabs?.includes('documents') && workspace?.user_access?.permissions.includes(Permission.Read);
  const showNeedsTab = !!(workspace.management?.includes('needs_created_by_any_member') || hasNeeds || isAdmin);
  const showResourcesTab = !!(
    workspace.management?.includes('resources_created_by_any_member') ||
    hasResources ||
    isAdmin
  );

  const showResourcesTabDisclaimerToAdmin = isAdmin && !hasResources;

  const horizontalTabs: SpaceTab[] = useMemo(() => {
    return [
      { value: 'feed', translationId: 'discussion.one', icon: 'material-symbols:chat-outline' },
      ...(!isSubworkspace
        ? [{ value: 'workspaces', translationId: 'workspace.other', icon: 'gravity-ui:nodes-down' }]
        : []),
      { value: 'events', translationId: 'calendar', icon: 'material-symbols:event-note-outline' },
      ...(workspace.stats?.cfp_count > 0
        ? [{ value: 'cfps', translationId: 'callForProposals.other', icon: 'bx:medal' }]
        : []),
      {
        value: 'members',
        translationId: 'person.other',
        icon: 'mdi:people-check',
        extraLabel: `${workspace.stats?.members_count}`,
      },
    ];
  }, [workspace, isSubworkspace]);

  const verticalTabs: SpaceTab[] = useMemo(() => {
    return [
      ...(showDocumentsTab
        ? [{ value: 'documents', translationId: 'document.other', icon: <FileTextOutlined /> }]
        : []),
      ...(showLibraryTab ? [{ value: 'library', translationId: 'library', icon: <ReadOutlined /> }] : []),
      ...(showNeedsTab ? [{ value: 'needs', translationId: 'need.other', icon: <AppstoreAddOutlined /> }] : []),
      ...(showResourcesTab ? [{ value: 'resources', translationId: 'resource.other', icon: <CompassOutlined /> }] : []),
    ];
  }, [showDocumentsTab, showLibraryTab, showNeedsTab, showResourcesTab]);

  const allTabs = [...horizontalTabs, ...verticalTabs];
  const isAllowedToViewSelectedTab = allTabs.some((tab) => tab.value === router.query.tab);
  const [selectedTab, setSelectedTab] = useState(
    isAllowedToViewSelectedTab ? (router.query.tab as string) : DEFAULT_TAB
  );

  // function when changing tab (or when router change in general)
  useEffect(() => {
    if (allTabs.map((tab: SpaceTab) => tab.value).includes(router.query.tab as string)) {
      if (router.query.tab === DEFAULT_TAB) {
        router.replace(`/workspace/${router.query.id}`, undefined, {
          shallow: true,
        });
      }
      setSelectedTab(router.query.tab as string);
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router]);

  const showVerticalTab = verticalTabsList.includes(selectedTab);
  return (
    <>
      {!limitedVisibility && (
        <div>
          {
            <HorizontalTabs
              containerType="workspace"
              tabs={horizontalTabs}
              verticalTabs={verticalTabs}
              defaultTab={DEFAULT_TAB}
              selectedTab={selectedTab}
              verticalTabSelected={showVerticalTab}
            />
          }

          <PageWrapperStyled gap="small">
            {screens.md && showVerticalTab && (
              <VerticalTabs
                containerType="workspace"
                tabs={verticalTabs}
                defaultTab={DEFAULT_TAB}
                selectedTab={selectedTab}
              />
            )}

            {selectedTab === 'feed' && (
              <TabContentStyled>
                <DiscussionsListWorkspace
                  workspaceId={workspace.id}
                  canCreate={!!workspace?.user_access?.permissions?.includes(Permission.Createchannels)}
                />
              </TabContentStyled>
            )}

            {selectedTab === 'workspaces' && (
              <TabContentStyled>
                <ShowWorkspaces workspace={workspace} />
              </TabContentStyled>
            )}

            {selectedTab === 'events' && (
              <TabContentStyled>
                <EntityEventList
                  entityId={workspace.id}
                  canManageEvents={workspace?.user_access?.permissions?.includes(Permission.Createevents)}
                />
              </TabContentStyled>
            )}

            {selectedTab === 'cfps' && (
              <TabContentStyled>
                <ShowCfps parentId={workspace.id} parentType="workspaces" isAdmin={isAdmin} />
              </TabContentStyled>
            )}

            {selectedTab === 'members' && (
              <TabContentStyled>
                <ShowMembers parentType="workspaces" parentId={workspace.id} canCreate={isAdmin} />
              </TabContentStyled>
            )}

            {selectedTab === 'documents' && (
              <TabContentStyled id="documents">
                <EntityDocumentList
                  entityId={workspace.id}
                  canManageDocuments={workspace.user_access.permissions.includes(Permission.Managedocuments)}
                />
              </TabContentStyled>
            )}

            {selectedTab === 'library' && (
              <TabContentStyled id="library">
                <EntityPaperList
                  entityId={workspace.id}
                  canManagePapers={workspace.user_access.permissions.includes(Permission.Managelibrary)}
                />
              </TabContentStyled>
            )}

            {selectedTab === 'needs' && (
              <TabContentStyled id="needs">
                <EntityNeedList
                  entityId={workspace.id}
                  canManageNeeds={workspace.user_access.permissions.includes(Permission.Postneed)}
                />
              </TabContentStyled>
            )}

            {selectedTab === 'resources' && (
              <TabContentStyled id="resources">
                <ResourceList
                  itemType="workspaces"
                  entity={workspace}
                  isAdmin={isAdmin}
                  showResourcesTabDisclaimerToAdmin={showResourcesTabDisclaimerToAdmin}
                />
              </TabContentStyled>
            )}
          </PageWrapperStyled>
        </div>
      )}
    </>
  );
};
