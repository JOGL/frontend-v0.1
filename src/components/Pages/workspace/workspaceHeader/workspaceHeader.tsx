import { WorkspaceDetailModel } from '~/__generated__/types';
import { HeaderStyled, HubDescriptionStyled, InvitationActionWrapper } from './workspaceHeader.styles';
import { Alert, Avatar, Flex, Grid, Space, Tag, Typography, Button, message, Modal } from 'antd';
import { EditOutlined, ShareAltOutlined, UserAddOutlined, UserOutlined } from '@ant-design/icons';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import { addressFront, isAdmin, isMember, linkify, toInitials } from '~/src/utils/utils';
import useUser from '~/src/hooks/useUser';
import { useEffect, useMemo, useState } from 'react';
import ShareModal from '../../../shareModal/shareModal';
import { useMutation, useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import { usePushNotificationStore } from '~/src/store/push-notifications/PushNotificationsStoreProvider';
import ShowOnboarding from '~/src/components/Onboarding/ShowOnboarding';
import BtnJoin from '~/src/components/Tools/BtnJoin';
import tw from 'twin.macro';
import { ContainerMoreModal } from '~/src/components/Tools/ContainerMoreModal';
import { securityOptionsWorkspace } from '~/src/utils/security_options';

const { useBreakpoint } = Grid;

interface Props {
  workspace: WorkspaceDetailModel;
}
const WorkspaceHeader = ({ workspace }: Props) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const screens = useBreakpoint();
  const [showShareModal, setShowShareModal] = useState(false);
  const [showMoreModal, setShowMoreModal] = useState(false);
  const workspaceDescritption = linkify(workspace.short_description);
  const [isOnboardingComplete, setIsOnboardingComplete] = useState(workspace.user_onboarded);
  const [showOnboardingModalType, setShowOnboardingModalType] = useState<'first_onboarding' | 'second_onboarding'>();
  const { user } = useUser();
  const { showPushNotificationModal } = usePushNotificationStore((s) => ({
    showPushNotificationModal: s.showPushNotificationModal,
  }));

  const { data: pending_invite, refetch: refreshInvites } = useQuery({
    queryKey: [QUERY_KEYS.workspaceInvitationDetails, workspace.id],
    queryFn: async () => {
      const response = await api.nodes.invitationDetail(workspace.id);
      return response.data;
    },
    enabled: workspace.user_joining_restriction_level === 'visitor' || workspace.user_access_level === 'pending',
  });

  const showInviteBox = !!(pending_invite && pending_invite?.invitation_type !== 'request');
  const limitedVisibility = workspace.content_privacy === 'private' && !isMember(workspace);
  const { showRequestToJoinButton, showJoinButton } = useMemo(() => {
    return {
      showRequestToJoinButton:
        workspace.onboarding?.enabled &&
        workspace.user_access_level === 'visitor' &&
        workspace.user_joining_restriction_level !== 'forbidden' &&
        workspace.joining_restriction === 'request' &&
        workspace.onboarding?.questionnaire.enabled &&
        workspace.onboarding?.questionnaire.items.length > 0,

      showJoinButton:
        (workspace.user_access_level === 'visitor' || workspace.user_access_level === 'pending') &&
        workspace.joining_restriction !== 'invite' &&
        workspace.user_joining_restriction_level !== null,
    };
  }, [workspace]);

  const acceptInvite = useMutation({
    mutationKey: [MUTATION_KEYS.workspaceInviteAccept],
    mutationFn: async (invitationId?: string) => {
      if (!invitationId) return;
      const response = await api.workspaces.invitesAcceptCreate(workspace.id, invitationId);
      return response.data;
    },
    onSuccess: () => {
      refreshInvites();
      const channelId = workspace?.home_channel_id as string;
      showPushNotificationModal({ ...router, query: { ...router.query, channelId: channelId } }).then(() => {
        router.reload();
      });
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const rejectInvite = useMutation({
    mutationKey: [MUTATION_KEYS.workspaceInviteReject],
    mutationFn: async (invitationId?: string) => {
      if (!invitationId) return;
      const response = await api.workspaces.invitesRejectCreate(workspace.id, invitationId);
      return response.data;
    },
    onSuccess: (data) => {
      refreshInvites();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const acceptOrRejectInvite = (e, type) => {
    // if function is launched via keypress, execute only if it's the 'enter' key
    if ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) {
      if (type === 'accept') {
        acceptInvite.mutate(pending_invite?.invitation_id);
      }
      if (type === 'reject') {
        rejectInvite.mutate(pending_invite?.invitation_id);
      }
    }
  };

  const handleShareButtonClick = () => {
    // check if browser/phone support the native share api (meaning we are in a mobile)
    if (navigator.share && window?.innerWidth < 700) {
      navigator
        .share({
          title: t('workspace.one'),
          text: addressFront + router.asPath,
          url: addressFront + router.asPath,
        })
        .catch(console.error);
    } else {
      setShowShareModal(true);
    }
  };

  const showOnboardingModal =
    !isAdmin(workspace) &&
    workspace?.status !== 'draft' &&
    isMember(workspace) &&
    workspace.onboarding?.enabled &&
    !isOnboardingComplete &&
    (workspace.onboarding.rules.enabled || workspace.onboarding.presentation.enabled);

  // Launch show onboarding modal with intro and/or rules if:
  // 1) onboarding is enabled 2) user is member 3) user didn't complete onboarding yet
  useEffect(() => {
    if (showOnboardingModal) {
      setShowOnboardingModalType('second_onboarding');
    }
  }, [showOnboardingModal]);

  return (
    <>
      <HeaderStyled>
        <Flex gap="middle">
          {screens.md && (
            <Flex flex="none">
              <Avatar src={workspace.logo_url || '/images/default/default-workspace-logo.png'} size={160}>
                {toInitials(workspace.title || t('untitled'))}
              </Avatar>
            </Flex>
          )}
          <Flex vertical gap="small" flex="1">
            <Flex align="center" justify="space-between" gap="middle">
              <Flex align="center" gap="middle">
                {!screens.md && (
                  <Avatar src={workspace.logo_url} size={80}>
                    {toInitials(workspace.title || t('untitled'))}
                  </Avatar>
                )}
                <Flex vertical>
                  <Typography.Title level={2}>{workspace.title}</Typography.Title>
                  <Flex gap="small">
                    <Tag>{workspace.label.toUpperCase()}</Tag>
                    {workspace.status === 'draft' && <Tag>{t('draft').toLowerCase()}</Tag>}
                    <Tag>
                      <Space>
                        <UserOutlined />
                        {t(`legacy.role.${workspace.user_access_level}`)}
                      </Space>
                    </Tag>
                  </Flex>
                </Flex>
              </Flex>

              {screens.md && <Button icon={<ShareAltOutlined />} onClick={handleShareButtonClick} />}
            </Flex>

            {workspaceDescritption && (
              <HubDescriptionStyled>
                <div dangerouslySetInnerHTML={{ __html: workspaceDescritption }} />
              </HubDescriptionStyled>
            )}

            <Flex gap="small" wrap>
              <Button type="primary" onClick={() => setShowMoreModal(true)}>
                {t('more')}
              </Button>

              {isAdmin(workspace) && (
                <Button icon={<EditOutlined />} onClick={() => router.push(`/workspace/${workspace.id}/edit`)}>
                  {t('action.edit')}
                </Button>
              )}

              {/* Join/request btn, or onboarding modal, or accept/reject buttons */}
              {showInviteBox ? (
                <Alert
                  description={t('youHaveBeenInvitedToJoinThisContainer', { container: t('workspace.one') })}
                  type="info"
                  action={
                    <InvitationActionWrapper>
                      <Button
                        size="small"
                        type="primary"
                        loading={acceptInvite.isPending}
                        onClick={() => acceptInvite.mutate(pending_invite?.invitation_id)}
                      >
                        {t('action.accept')}
                      </Button>
                      <Button
                        size="small"
                        danger
                        ghost
                        loading={rejectInvite.isPending}
                        onClick={() => rejectInvite.mutate(pending_invite?.invitation_id)}
                      >
                        {t('action.reject')}
                      </Button>
                    </InvitationActionWrapper>
                  }
                />
              ) : // if onboarding questionnaire is enabled, and multiple other conditions, show button to open onboarding questionnaire modal
              showRequestToJoinButton ? (
                <Button
                  icon={<UserAddOutlined />}
                  type="primary"
                  ghost
                  onClick={() => {
                    setShowOnboardingModalType('first_onboarding');
                  }}
                >
                  {t('action.requestToJoin')}
                </Button>
              ) : (
                // else show the basic "join" button (can be open or request to join)
                showJoinButton && (
                  <BtnJoin
                    joinState={workspace.user_access_level}
                    joinType={workspace.joining_restriction}
                    itemType="workspaces"
                    itemId={workspace.id}
                    textJoin={workspace.joining_restriction === 'open' ? t('action.join') : t('action.requestToJoin')}
                    count={workspace.stats.members_count}
                    pending_invite={pending_invite}
                    showMembersModal={() =>
                      router.push(`/workspace/${router.query.id}?tab=members`, undefined, { shallow: true })
                    }
                    hasNoStat
                    customCss={[tw`text-primary text-sm font-semibold uppercase rounded-md border-primary`]}
                  />
                )
              )}
              {!screens.md && <Button icon={<ShareAltOutlined />} onClick={handleShareButtonClick} />}
            </Flex>
          </Flex>
        </Flex>
      </HeaderStyled>
      {showShareModal && (
        <ShareModal
          id={workspace.id}
          type="workspace"
          title={t('action.shareItem', {
            item: t('workspace.one'),
          })}
          onClose={() => setShowShareModal(false)}
        />
      )}
      {showOnboardingModalType && (
        <Modal
          open
          title={showOnboardingModalType === 'first_onboarding' ? t('answeringQuestions') : t('welcome')}
          onCancel={() => setShowOnboardingModalType(undefined)}
          closable={!(showOnboardingModalType === 'second_onboarding')}
          footer={null}
        >
          <ShowOnboarding
            object={workspace}
            itemType="workspace"
            type={showOnboardingModalType}
            setIsOnboardingComplete={(isOnboardingComplete: boolean) => {
              setShowOnboardingModalType(undefined);
              setIsOnboardingComplete(isOnboardingComplete);
            }}
          />
        </Modal>
      )}
      {showMoreModal && (
        <Modal open onCancel={() => setShowMoreModal(false)} closable={false} footer={null} width={890}>
          <ContainerMoreModal
            container={workspace}
            containerType="workspace"
            limitedVisibility={limitedVisibility}
            securityOptions={securityOptionsWorkspace}
            showJoinButton={showJoinButton}
            showRequestToJoinButton={showRequestToJoinButton}
            showInviteBox={showInviteBox}
            pendingInvite={pending_invite}
            acceptOrRejectInvite={acceptOrRejectInvite}
            setIsOnboardingComplete={setIsOnboardingComplete}
          />
        </Modal>
      )}
    </>
  );
};

export default WorkspaceHeader;
