import styled from '@emotion/styled';
import { Space, Typography } from 'antd';

export const InvitationActionWrapper = styled(Space)`
  margin-left: ${({ theme }) => theme.token.sizeMS}px;
`;

export const HeaderStyled = styled.header`
  padding: ${({ theme }) => theme.token.sizeLG}px;
  border-bottom: solid 1px ${({ theme }) => theme.token.colorBorder};
`;

export const HubDescriptionStyled = styled(Typography.Text)`
  white-space: normal;
  margin-bottom: ${({ theme }) => theme.token.sizeLG}px;
`;
