import { WorkspaceDetailModel } from '~/__generated__/types';
import Layout from '~/src/components/Layout/layout/layout';
import WorkspaceHeader from './workspaceHeader/workspaceHeader';
import { WorkspaceTabs } from './workspaceTabs/workspaceTabs';
import { useStonePathStore } from '~/src/contexts/stonepath/StonePathStoreProvider';
import { useEffect } from 'react';
import { PageWrapperStyled } from '../pages.styles';

interface Props {
  workspace: WorkspaceDetailModel;
}
const Workspace = ({ workspace }: Props) => {
  const { setStonePath } = useStonePathStore();

  useEffect(() => {
    setStonePath(workspace.path);

    return () => setStonePath([]);
  }, [workspace, setStonePath]);

  return (
    <Layout
      title={workspace?.title && `${workspace.title} | JOGL`}
      desc={workspace?.short_description}
      img={workspace?.banner_url || '/images/default/default-workspace.png'}
      noIndex={workspace?.status === 'draft' || workspace?.listing_privacy !== 'public'}
    >
      <PageWrapperStyled>
        <WorkspaceHeader workspace={workspace} />
        <WorkspaceTabs workspace={workspace} />
      </PageWrapperStyled>
    </Layout>
  );
};

export default Workspace;
