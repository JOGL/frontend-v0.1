import styled from '@emotion/styled';
import { Alert } from 'antd';

export const AlertStyled = styled(Alert)`
  margin-bottom: ${({ theme }) => theme.space[4]};
`;

export const FormContainerStyled = styled.div`
  margin-top: ${({ theme }) => theme.space[8]};
`;

export const SuccessMessageStyled = styled.div`
  margin: auto;
  text-align: center;

  img {
    max-width: 400px;
    width: 100%;
    margin: auto;
  }
`;
