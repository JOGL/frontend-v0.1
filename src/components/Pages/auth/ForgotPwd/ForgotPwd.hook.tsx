import { useState, ReactNode } from 'react';
import { useRouter } from 'next/router';
import { useApi } from '~/src/contexts/apiContext';

/**
 * Custom hook to manage the state and logic for the Forgot Password feature.
 *
 * @returns {object} - Returns the state and handlers for the Forgot Password form.
 */
const useForgotPwd = () => {
  const [error, setError] = useState<string | ReactNode>('');
  const [sending, setSending] = useState(false);
  const [success, setSuccess] = useState(false);
  const router = useRouter();
  const api = useApi();

  const onSubmit = (data: { email: string }) => {
    setSending(true);
    api
      .get(`/auth/reset?email=${data.email.toLowerCase()}`)
      .then(() => {
        setSending(false);
        setSuccess(true);
        setTimeout(() => {
          router.push('/signin');
        }, 10000);
      })
      .catch((err) => {
        setSending(false);
        setError(err.response.data.error);
      });
  };

  return {
    error,
    sending,
    success,
    onSubmit,
  };
};

export default useForgotPwd;
