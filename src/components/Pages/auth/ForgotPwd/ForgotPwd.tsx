import Image from 'next/image';
import useTranslation from 'next-translate/useTranslation';
import useForgotPwd from './ForgotPwd.hook';
import { Button, Flex, Form, Input, Typography } from 'antd';
import { AlertStyled, FormContainerStyled, SuccessMessageStyled } from './ForgotPwd.styles';
import { EMAIL_REGEX } from '~/src/utils/utils';
import AuthContainer from '../AuthContainer/AuthContainer';

const ForgotPwdForm = () => {
  const { success, error, sending, onSubmit } = useForgotPwd();
  const { t } = useTranslation('common');
  const [form] = Form.useForm();

  return (
    <AuthContainer>
      <Flex vertical justify="center" align="center">
        <Typography.Title>{t(success ? 'anEmailHasBeenSentToYou' : 'action.resetPassword')}</Typography.Title>
        <Typography.Text>
          {t(
            success
              ? 'passwordResetInstructionsWereSentToYourEmailAddress'
              : 'passwordResetInstructionsWillBeSentToYourEmailAddress'
          )}
        </Typography.Text>
      </Flex>
      <FormContainerStyled>
        {!success ? (
          <Form form={form} onFinish={onSubmit} layout="vertical" requiredMark={false}>
            <Form.Item
              label={t('email')}
              name="email"
              rules={[
                { required: true, message: t('error.valueIsMissing') },
                {
                  type: 'string',
                  pattern: EMAIL_REGEX,
                  message: t('error.invalidMailAddress'),
                },
              ]}
            >
              <Input size="large" placeholder={t('emailPlaceholder')} />
            </Form.Item>

            {error && <AlertStyled message={error} type="error" />}

            <Flex vertical justify="center" align="center" gap="middle">
              <Button htmlType="submit" type="primary" size="large" loading={sending}>
                {t('sendMeInstructions')}
              </Button>
            </Flex>
          </Form>
        ) : (
          <SuccessMessageStyled>
            <Image src="/images/envelope.svg" alt="Message sent envelope" fill={true} />
          </SuccessMessageStyled>
        )}
      </FormContainerStyled>
    </AuthContainer>
  );
};

export default ForgotPwdForm;
