import { FC, ReactNode } from 'react';
import Link from 'next/link';
import useTranslation from 'next-translate/useTranslation';
import Layout from '~/src/components/Layout/layout/layout';
import Image from 'next/image';
import { ColLeftStyled, ColRightStyled, RightPanelStyled, RowStyled, FullWidthStyled } from './AuthContainer.styles';

const AuthContainer: FC<{ children: ReactNode }> = ({ children }) => {
  const { t } = useTranslation('common');

  return (
    <Layout className="no-margin" title={`${t('action.resetPassword')} | JOGL`} noIndex fullWidth>
      <RowStyled>
        <ColLeftStyled xs={24} lg={12}>
          <Link href="/">
            <Image src="/images/logo_single.svg" className="logo" alt="JOGL icon" width={80} height={102} />
          </Link>
        </ColLeftStyled>
        <ColRightStyled xs={24} lg={12}>
          <FullWidthStyled>
            <RightPanelStyled>{children} </RightPanelStyled>
          </FullWidthStyled>
        </ColRightStyled>
      </RowStyled>
    </Layout>
  );
};
export default AuthContainer;
