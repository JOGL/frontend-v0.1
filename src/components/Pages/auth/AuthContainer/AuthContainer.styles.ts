import styled from '@emotion/styled';

import { Col, Row } from 'antd';

export const RowStyled = styled(Row)`
  height: 100%;
`;

export const ColLeftStyled = styled(Col)`
  background-color: ${({ theme }) => theme.colors.carouselBg};
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ColRightStyled = styled(Col)`
  padding: ${({ theme }) => theme.space[10]};
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;
export const RightPanelStyled = styled.div`
  max-width: 750px;
`;

export const FullWidthStyled = styled.div`
  width: 100%;
`;
