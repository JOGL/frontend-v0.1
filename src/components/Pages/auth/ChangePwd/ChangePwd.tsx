import useTranslation from 'next-translate/useTranslation';
import useChangePwdForm from './ChangePwd.hook';
import { Button, Col, Flex, Form, Input, Row, Typography } from 'antd';
import { AlertStyled, FormContainerStyled } from './ChangePwd.styles';
import AuthContainer from '~/src/components/Pages/auth/AuthContainer/AuthContainer';

const ChangePwdForm = () => {
  const { errorMessage, sending, success, onSubmit, isReset } = useChangePwdForm();
  const { t } = useTranslation('common');
  const [form] = Form.useForm();

  return (
    <AuthContainer>
      <Flex vertical justify="center" align="center">
        <Typography.Title>{t('passwordReset')}</Typography.Title>
        <Typography.Text>{t('enterNewPassword')}</Typography.Text>
      </Flex>

      <FormContainerStyled>
        <Form onFinish={onSubmit} form={form} requiredMark={false} layout="vertical">
          <Row justify="center" gutter={12}>
            {!isReset && (
              <Col span={12}>
                <Form.Item
                  label={t('currentPassword')}
                  name="password"
                  rules={[
                    { required: isReset ? false : true, message: t('error.valueIsMissing') },
                    { type: 'string', min: 8, message: t('error.passwordToShort') },
                    { type: 'string', max: 128, message: t('error.valueIsTooLong') },
                  ]}
                >
                  <Input size="large" placeholder={t('yourPassword')} type="password" />
                </Form.Item>
              </Col>
            )}

            <Col span={isReset ? 24 : 12}>
              <Form.Item
                label={t('newPassword')}
                name="new_password"
                rules={[
                  { required: true, message: t('error.valueIsMissing') },
                  { type: 'string', min: 8, message: t('error.passwordToShort') },
                  { type: 'string', max: 128, message: t('error.valueIsTooLong') },
                ]}
              >
                <Input size="large" placeholder={t('password')} type="password" />
              </Form.Item>
            </Col>
          </Row>

          {errorMessage && <AlertStyled type="error" message={errorMessage} />}
          {success && <AlertStyled type="success" message={t('newPasswordSaved')} />}

          <Flex vertical justify="center" align="center" gap="middle">
            <Button htmlType="submit" type="primary" size="large" loading={sending}>
              {t('action.changePassword')}
            </Button>
          </Flex>
        </Form>
      </FormContainerStyled>
    </AuthContainer>
  );
};

export default ChangePwdForm;
