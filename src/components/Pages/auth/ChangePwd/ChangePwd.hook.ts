import { useState, useEffect, ReactNode, FormEvent } from 'react';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from '~/src/contexts/apiContext';

const useChangePwdForm = () => {
  const [error, setError] = useState('');
  const [sending, setSending] = useState(false);
  const [success, setSuccess] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string | ReactNode>(error);
  const { t } = useTranslation('common');
  const router = useRouter();
  const api = useApi();

  const onSubmit = (data: any) => {
    setError('');
    setSending(true);

    if (router.query.token && router.query.email) {
      api
        ?.post(`/auth/resetConfirm`, {
          email: router.query.email,
          code: router.query.token,
          new_password: data.new_password,
        })
        .then(() => {
          setSending(false);
          setSuccess(true);
          setTimeout(() => {
            router.push('/signin');
          }, 2500);
        })
        .catch((err) => {
          setError(err.response.data.error || 'Invalid token');
          setSending(false);
        });
    } else {
      api
        ?.post(`/auth/password`, { old_password: data.password, new_password: data.new_password })
        .then(() => {
          setSending(false);
          setSuccess(true);
        })
        .catch((err) => {
          setError(err.response.data.error || 'Invalid token');
          setSending(false);
        });
    }
  };

  useEffect(() => {
    setErrorMessage(error.includes('error.') ? t(error) : error);
  }, [error, t]);

  return {
    errorMessage,
    sending,
    success,
    onSubmit,
    isReset: Boolean(router.query.email),
  };
};

export default useChangePwdForm;
