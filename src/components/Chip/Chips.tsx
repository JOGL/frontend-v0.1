import React, { FC, Fragment, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Chip from '~/src/components/Chip/Chip';
import A from '../primitives/A';

interface Props {
  data: any;
  overflowLink?: string;
  overflowText?: string;
  type: 'skills' | 'resources' | 'interests';
  showCount?: number;
  smallChips?: boolean;
  hideOverFlow?: boolean;
  onOverflowClick?: () => void;
}

const Chips: FC<Props> = ({
  data,
  overflowLink,
  overflowText,
  type,
  showCount = 100,
  smallChips = false,
  hideOverFlow = false,
  onOverflowClick,
}) => {
  const overFlowingChipsLength = data.length <= showCount ? undefined : data.length - showCount;
  const [showCount2, setShowCount2] = useState(showCount);
  const { t } = useTranslation('common');

  return (
    <div tw="flex flex-wrap gap-1">
      {/* chips list */}
      {[...data].splice(0, showCount2).map((item, i) => (
        <Fragment key={i}>
          <div tw="flex flex-col">
            {/* <A href={item.href} key={i} noStyle> */}
            {/* set different maxLength depending on if chip is small (in cards) or other factors */}
            {item.title && (
              <Chip
                type={type}
                maxLength={smallChips && showCount === 2 ? 15 : overflowLink ? 23 : 40}
                smallChips={smallChips}
              >
                {item.title}
              </Chip>
            )}
            {/* </A> */}
          </div>
        </Fragment>
      ))}
      {/* moreChips links to an object */}
      {overFlowingChipsLength && overflowLink && !hideOverFlow && (
        <A href={overflowLink} noStyle>
          <div tw="flex flex-col cursor-pointer">
            <Chip type={type} smallChips={smallChips} hasOpacity>
              + {overFlowingChipsLength.toString()}
            </Chip>
          </div>
        </A>
      )}
      {/* if moreChips allow you to see more/less, or jumps you to another div */}
      {overFlowingChipsLength && showCount === showCount2 && overflowText && !hideOverFlow && (
        <div
          tw="flex flex-col cursor-pointer"
          onClick={() => overflowText === 'seeMore' && setShowCount2(data.length)}
          className={overflowText === 'userSkill' ? 'moreSkills' : 'userResource' && 'moreResources'}
        >
          <Chip type={type} smallChips={smallChips} hasOpacity>
            + {overFlowingChipsLength.toString()}
          </Chip>
        </div>
      )}
      {/* show "see less" if user has clicked "see more" */}
      {overFlowingChipsLength && showCount !== showCount2 && !hideOverFlow && (
        <div tw="flex flex-col cursor-pointer" onClick={() => setShowCount2(showCount)}>
          <Chip type={type} smallChips={smallChips} hasOpacity>
            {t('action.showLess')}
          </Chip>
        </div>
      )}
      {overFlowingChipsLength && overflowLink && hideOverFlow && (
        <div tw="flex flex-col cursor-pointer" onClick={onOverflowClick}>
          <Chip type={type} smallChips={smallChips} hasOpacity>
            + {overFlowingChipsLength.toString()}
          </Chip>
        </div>
      )}
    </div>
  );
};

export default Chips;
