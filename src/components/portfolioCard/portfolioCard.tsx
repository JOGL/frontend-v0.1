import { PortfolioItemModel, PortfolioItemType } from '~/__generated__/types';
import { Button, Dropdown, Flex, Tag, theme, Typography } from 'antd';
import { CardStyled, SummaryWrapperStyled, TagStyled, TextStyled } from './portfolioCard.styles';
import { MessageOutlined, MoreOutlined } from '@ant-design/icons';
import dayjs from 'dayjs';
import useTranslation from 'next-translate/useTranslation';
import ReadOnlyEditor from '../tiptap/readOnlyEditor/readOnlyEditor';
import Link from 'next/link';

interface Props {
  portfolioItem: PortfolioItemModel;
  href: string;
  onDelete?: () => void;
}

export const PortfolioCard = ({ portfolioItem, href, onDelete }: Props) => {
  const { t } = useTranslation('common');
  const { token } = theme.useToken();
  const { type, keywords, title, summary, publication_date, feed_stats, authors, journal } = portfolioItem;
  const authorsList = authors?.split(',');
  const displayAuthors = authorsList
    ? authorsList.length > 1
      ? `${authorsList[0]}, ${authorsList[1]}`
      : authorsList[0]
    : '';
  return (
    <CardStyled>
      <Flex vertical gap="small" style={{ height: '100%', overflow: 'hidden' }}>
        <Flex align="center" justify={!keywords?.length && type !== PortfolioItemType.Paper ? 'end' : 'space-between'}>
          <Flex>
            {type === PortfolioItemType.Paper && <Tag color={token.colorPrimary}>{t('paper.one')}</Tag>}
            {!!keywords?.length && (
              <>
                <TagStyled>{keywords[0]}</TagStyled>
                {keywords.length > 1 && <Tag>{`+${keywords.length - 1}`}</Tag>}
              </>
            )}
          </Flex>
          {
            <Dropdown
              trigger={['click']}
              placement="bottomRight"
              menu={{
                onClick: ({ domEvent }) => {
                  domEvent.stopPropagation();
                },
                items: [
                  ...(onDelete
                    ? [
                        {
                          label: t('action.delete'),
                          key: 'delete',
                          danger: true,
                          onClick: (e) => {
                            e.domEvent.stopPropagation();
                            onDelete();
                          },
                        },
                      ]
                    : []),
                ],
              }}
            >
              <Button
                type="text"
                onClick={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                }}
              >
                <MoreOutlined />
              </Button>
            </Dropdown>
          }
        </Flex>
        <Flex vertical gap="small" style={{ flexGrow: 1 }}>
          <Link href={href}>
            <Typography.Paragraph strong ellipsis={{ tooltip: title, rows: 3 }}>
              {title}
            </Typography.Paragraph>
          </Link>

          <SummaryWrapperStyled
            showAuthorsAndJournal={!!displayAuthors && !!journal}
            paper={type === PortfolioItemType.Paper}
          >
            <ReadOnlyEditor focus={false} content={summary ?? ''} />
          </SummaryWrapperStyled>
        </Flex>

        <Flex vertical>
          {displayAuthors && (
            <TextStyled type="secondary" ellipsis={{ tooltip: displayAuthors }}>
              {displayAuthors}
            </TextStyled>
          )}
          {journal && (
            <TextStyled type="secondary" ellipsis={{ tooltip: journal }}>
              {journal}
            </TextStyled>
          )}
          <Flex justify="space-between" align="center">
            <Flex gap="small">
              <MessageOutlined />
              {feed_stats.post_count}
            </Flex>
            {publication_date && (
              <Typography.Text type="secondary">{dayjs(publication_date).format('DD MMM, YYYY')}</Typography.Text>
            )}
          </Flex>
        </Flex>
      </Flex>
    </CardStyled>
  );
};
