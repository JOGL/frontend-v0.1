import styled from '@emotion/styled';
import { Card, Tag, Typography } from 'antd';
import { getColorToRGB } from '~/src/utils/getColorToRGB';

export const CardStyled = styled(Card)`
  min-height: 350px;
  .ant-card-body {
    height: 350px;
  }
`;

export const TagStyled = styled(Tag)`
  color: ${({ theme }) => theme.token.colorPrimary};
  background-color: ${({ theme }) => getColorToRGB(theme.token.colorPrimary, 0.1)};
`;

export const SummaryWrapperStyled = styled.div<{ paper: boolean; showAuthorsAndJournal: boolean }>`
  height: ${(props) => (props.paper ? (props.showAuthorsAndJournal ? 85 : 110) : 160)}px;
  overflow: hidden;
`;

export const TextStyled = styled(Typography.Text)`
  border-bottom: 1px solid ${({ theme }) => theme.token.colorBorderSecondary};
  padding-bottom: 4px;
  &:last-of-type {
    margin-bottom: 4px;
  }
`;
