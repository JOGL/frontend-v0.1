import styled from '@emotion/styled';

export const PickerWrapper = styled.div`
  .EmojiPickerReact {
    --epr-emoji-size: ${({ theme }) => theme.token.fontSizeLG}px;
    --epr-picker-border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;
  }
`;
