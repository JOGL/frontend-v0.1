import React, { useState } from 'react';
import { Button, Grid, Popover } from 'antd';
import { SmileOutlined } from '@ant-design/icons';
import { ReactionUpsertModel } from '~/__generated__/types';
import Picker, { EmojiClickData, EmojiStyle, SuggestionMode, Theme } from 'emoji-picker-react';
import useTranslation from 'next-translate/useTranslation';
import { PickerWrapper } from './emojiPicker.styles';

interface EmojiPickerProps {
  handleEmojiSelect: (emoji: ReactionUpsertModel) => void;
}

const { useBreakpoint } = Grid;

const DEFAULT_REACTIONS = ['1f44d', '2764-fe0f', '1f602', '1f62e', '1f622', '1f64f'];

export const EmojiPicker: React.FC<EmojiPickerProps> = ({ handleEmojiSelect }) => {
  const { t } = useTranslation('common');
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const screens = useBreakpoint();

  const handlePickedEmoji = (pickedEmoji: EmojiClickData) => {
    setIsOpen(false);
    handleEmojiSelect({ key: pickedEmoji.unified });
  };

  return (
    <Popover
      content={
        <PickerWrapper>
          <Picker
            emojiStyle={EmojiStyle.NATIVE}
            suggestedEmojisMode={SuggestionMode.FREQUENT}
            theme={Theme.LIGHT}
            searchPlaceHolder={t('action.searchHere')}
            width={screens.sm ? '350px' : '100%'}
            reactions={DEFAULT_REACTIONS}
            reactionsDefaultOpen={true}
            onEmojiClick={handlePickedEmoji}
            onReactionClick={handlePickedEmoji}
            previewConfig={{ defaultCaption: t('pickAnEmoji') }}
          />
        </PickerWrapper>
      }
      trigger="click"
      placement={screens.sm ? 'bottomLeft' : 'bottom'}
      arrow={false}
      open={isOpen}
      onOpenChange={(newOpen) => setIsOpen(newOpen)}
      overlayInnerStyle={{ padding: 0 }}
    >
      <Button size="small" icon={<SmileOutlined />} onClick={() => setIsOpen(!isOpen)} />
    </Popover>
  );
};
