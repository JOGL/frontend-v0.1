import { Form, Select, Space, message } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import Trans from 'next-translate/Trans';
import { PrivacySettingsForm } from '../privacySettingsForm/privacySettingsForm';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import { useMutation } from '@tanstack/react-query';
import { NodeModel, NodePatchModel, PrivacyLevel } from '~/__generated__/types';
import api from '~/src/utils/api/api';
import { useRouter } from 'next/router';

interface Props {
  hub: NodeModel;
}

export const PrivacySettingsHub = ({ hub }: Props) => {
  const router = useRouter();
  const { t } = useTranslation('common');

  const getPrivacyOptions = (type: string) => {
    return [
      { label: t(`privacyOptions.hub.${type}Open`), value: 'public' },
      { label: t(`privacyOptions.hub.${type}Private`, { entityName: hub.title }), value: 'private' },
    ];
  };

  const updateHub = useMutation({
    mutationKey: [MUTATION_KEYS.nodePartialUpdate, hub.id],
    mutationFn: async (data: NodePatchModel) => {
      await api.nodes.nodesPartialUpdateDetail(hub.id, data);
    },
    onSuccess: () => {
      message.success(t('hubWasUpdated'));
      router.replace(router.asPath);
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  return (
    <PrivacySettingsForm
      onUpdate={(data) => updateHub.mutate(data)}
      initialValues={{
        listing_privacy: hub.listing_privacy === PrivacyLevel.Ecosystem ? PrivacyLevel.Public : hub.listing_privacy,
        content_privacy: hub.content_privacy === PrivacyLevel.Ecosystem ? PrivacyLevel.Public : hub.content_privacy,
        joining_restriction: hub.joining_restriction,
        management: hub.management,
      }}
      privacyFormItems={
        <>
          <Form.Item
            label={<Trans i18nKey={`common:privacySettingsLabel.discoverability`} components={{ em: <em /> }} />}
            name="listing_privacy"
            tooltip={
              <Space direction="vertical">
                <Trans i18nKey={`common:privacySettingsTooltip.publicDiscover`} components={{ b: <b /> }} />
                <Trans
                  i18nKey={`common:privacySettingsTooltip.private`}
                  components={{ b: <b /> }}
                  values={{ entityName: hub.title }}
                />
              </Space>
            }
          >
            <Select options={getPrivacyOptions('discoverability')} />
          </Form.Item>
          <Form.Item
            label={<Trans i18nKey={`common:privacySettingsLabel.visibility`} components={{ em: <em /> }} />}
            name="content_privacy"
            tooltip={
              <Space direction="vertical">
                <Trans i18nKey={`common:privacySettingsTooltip.publicView`} components={{ b: <b /> }} />
                <Trans
                  i18nKey={`common:privacySettingsTooltip.private`}
                  components={{ b: <b /> }}
                  values={{ entityName: hub.title }}
                />
              </Space>
            }
          >
            <Select options={getPrivacyOptions('visibility')} />
          </Form.Item>
          <Form.Item
            label={<Trans i18nKey={`common:privacySettingsLabel.membership`} components={{ em: <em /> }} />}
            name="joining_restriction"
            tooltip={
              <Space direction="vertical">
                <Trans i18nKey={`common:privacySettingsMembershipTooltip.openToPublic`} components={{ b: <b /> }} />
                <Trans i18nKey={`common:privacySettingsMembershipTooltip.invitationOnly`} components={{ b: <b /> }} />
                <Trans i18nKey={`common:privacySettingsMembershipTooltip.request`} components={{ b: <b /> }} />
              </Space>
            }
          >
            <Select
              options={[
                { label: t('privacySettingsMembershipOptions.hub.openToPublic'), value: 'open' },
                { label: t('privacySettingsMembershipOptions.hub.invitationOnly'), value: 'invite' },
                { label: t('privacySettingsMembershipOptions.hub.request'), value: 'request' },
              ]}
            />
          </Form.Item>
        </>
      }
    />
  );
};
