import styled from '@emotion/styled';
import { Flex } from 'antd';
import { ScrollbarStyled } from '~/src/assets/styles/Global.styled';

export const FlexStyled = styled(Flex)`
  max-height: 155px;
  overflow-y: auto;
  padding: 0 ${({ theme }) => theme.token.paddingXS}px;
`;

export const UsersScrollbarStyled = styled(ScrollbarStyled)`
  margin: 0 -${({ theme }) => theme.token.paddingXS}px;
`;
