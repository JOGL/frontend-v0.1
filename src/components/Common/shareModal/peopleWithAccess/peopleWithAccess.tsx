import { Dispatch, FC, SetStateAction, useState } from 'react';
import { useQuery } from '@tanstack/react-query';
import { UserOutlined } from '@ant-design/icons';
import useTranslation from 'next-translate/useTranslation';
import { AutoComplete, Avatar, Button, Col, Empty, Flex, Row, Select, Space, Typography } from 'antd';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import useDebounce from '~/src/hooks/useDebounce';
import {
  DocumentModel,
  FeedEntityUserVisibilityModel,
  FeedEntityVisibility,
  NeedModel,
  PaperModel,
  Permission,
  UserMiniModel,
} from '~/__generated__/types';
import { FlexStyled, UsersScrollbarStyled } from './peopleWithAccess.styles';
import { DividerStyled } from '../share.styles';

interface ShareProps {
  data: NeedModel | DocumentModel | PaperModel;
  setData: Dispatch<SetStateAction<NeedModel | DocumentModel | PaperModel>>;
}

const PeopleWithAccess: FC<ShareProps> = ({ data, setData }) => {
  const { t } = useTranslation('common');
  const [userSearch, setUserSearch] = useState('');
  const debouncedSearch = useDebounce(userSearch, 300);
  const canManage = data?.permissions.includes(Permission.Manage);

  const { data: users, isFetched } = useQuery({
    queryKey: [QUERY_KEYS.usersSearchAutocompleteList, { search: debouncedSearch }],
    queryFn: async () => {
      const response = await api.users.autocompleteList({
        Search: debouncedSearch,
      });
      return response.data;
    },
    enabled: debouncedSearch.length > 0,
  });

  const handleUserSelect = (userVisibilityData: FeedEntityUserVisibilityModel) => {
    const userVisibility = data.user_visibility?.length
      ? [...data?.user_visibility, userVisibilityData]
      : [userVisibilityData];
    setUserSearch('');
    setData({ ...data, user_visibility: userVisibility });
  };

  const handleUserChange = (userVisibilityData: FeedEntityUserVisibilityModel) => {
    const userVisibility = data.user_visibility?.map((item) => {
      if (item.user.id === userVisibilityData.user.id) {
        return userVisibilityData;
      }
      return item;
    });
    setData({ ...data, user_visibility: userVisibility });
  };

  const handleDeleteUser = (userId: string) => {
    const userVisibility = data.user_visibility?.filter((item) => item.user.id !== userId);
    setData({ ...data, user_visibility: userVisibility });
  };

  const handleSearch = (value: string) => {
    setUserSearch(value);
  };

  const selectedUsers = data.user_visibility?.flatMap((item) => item.user) || [];
  if (data.created_by) {
    selectedUsers.push(data.created_by);
  }

  const userOptions =
    users
      ?.filter((searchedUsers) => !selectedUsers.some((selectedUser) => searchedUsers.id === selectedUser.id))
      ?.map((user) => ({
        value: user.id,
        label: (
          <Row>
            <Col span={2}>
              <Avatar size="small" shape="square" icon={<UserOutlined />} src={user?.logo_url} />
            </Col>
            <Col span={12}>
              <Typography.Text type="secondary" ellipsis>{`${user.first_name} ${user.last_name}`}</Typography.Text>
            </Col>
            <Col span={10}>
              <Typography.Text type="secondary" ellipsis>
                @{user.username}
              </Typography.Text>
            </Col>
          </Row>
        ),
        item: user as UserMiniModel,
      })) || [];

  return (
    <Flex vertical gap="middle">
      <Typography.Title level={5}> {t('peopleWithAccess')}</Typography.Title>
      <AutoComplete
        disabled={!canManage}
        value={userSearch}
        options={userOptions}
        onSearch={handleSearch}
        placeholder={t('addPeople')}
        notFoundContent={userSearch.length && isFetched && !users?.length ? <Empty /> : null}
        onSelect={(_, options) => {
          handleUserSelect({ user: options.item, visibility: FeedEntityVisibility.View });
        }}
      />
      <UsersScrollbarStyled>
        <FlexStyled vertical gap="small">
          <Flex gap="middle" justify="space-between">
            <Space>
              <Avatar size="large" shape="square" icon={<UserOutlined />} src={data.created_by?.logo_url} />
              <Flex vertical>
                <Typography.Text strong>
                  {data.created_by?.first_name} {data.created_by?.last_name}
                </Typography.Text>
                <Typography.Text type="secondary">{`@${data.created_by?.username}`}</Typography.Text>
              </Flex>
            </Space>

            <Typography.Text type="secondary">{t('owner')}</Typography.Text>
          </Flex>

          {data?.user_visibility?.map((item) => (
            <Flex key={item.user.id} gap="middle" justify="space-between">
              <Space>
                <Avatar size="large" shape="square" icon={<UserOutlined />} src={item.user?.logo_url} />
                <Flex vertical>
                  <Typography.Text strong>
                    {item.user?.first_name} {item.user?.last_name}
                  </Typography.Text>
                  <Typography.Text type="secondary">{`@${item.user?.username}`}</Typography.Text>
                </Flex>
              </Space>

              <Select
                disabled={!canManage}
                defaultValue={item.visibility}
                style={{ width: 150 }}
                onChange={(val) => {
                  handleUserChange({ ...item, visibility: val });
                }}
                options={[
                  { value: FeedEntityVisibility.Edit, label: t('entityVisibility.canEdit') },
                  { value: FeedEntityVisibility.Comment, label: t('entityVisibility.canComment') },
                  { value: FeedEntityVisibility.View, label: t('entityVisibility.canView') },
                ]}
                dropdownRender={(menu) => (
                  <>
                    {menu}
                    <DividerStyled />
                    <Button type="text" onClick={() => handleDeleteUser(item.user.id)}>
                      {t('action.remove')}
                    </Button>
                  </>
                )}
              />
            </Flex>
          ))}
        </FlexStyled>
      </UsersScrollbarStyled>
    </Flex>
  );
};

export default PeopleWithAccess;
