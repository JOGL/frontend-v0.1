import { Dispatch, FC, SetStateAction, useState } from 'react';
import Image from 'next/image';
import useTranslation from 'next-translate/useTranslation';
import { useQuery } from '@tanstack/react-query';
import { AutoComplete, Button, Col, Empty, Flex, Row, Select, Typography } from 'antd';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import useDebounce from '~/src/hooks/useDebounce';
import { capitalize, toInitials } from '~/src/utils/utils';
import {
  CommunityEntityType,
  DocumentModel,
  FeedEntityCommunityEntityVisibilityModel,
  FeedEntityVisibility,
  NeedModel,
  PaperModel,
  Permission,
} from '~/__generated__/types';
import {
  BorderStyled,
  FlexStyled,
  SpaceNameStyled,
  SpaceScrollbarStyled,
  SpaceInfoStyled,
} from './spacesWithAccess.styles';
import { DividerStyled } from '../share.styles';

interface ShareProps {
  data: NeedModel | DocumentModel | PaperModel;
  setData: Dispatch<SetStateAction<NeedModel | DocumentModel | PaperModel>>;
}

const SpacesWithAccess: FC<ShareProps> = ({ data, setData }) => {
  const { t } = useTranslation('common');
  const [spaceSearch, setSpaceSearch] = useState('');
  const debouncedSpaceSearch = useDebounce(spaceSearch, 300);
  const canManage = data?.permissions.includes(Permission.Manage);

  const { data: spaces, isFetched } = useQuery({
    queryKey: [QUERY_KEYS.communityEntitiesList, { search: debouncedSpaceSearch }],
    queryFn: async () => {
      const response = await api.entities.communityEntitiesList({
        Search: debouncedSpaceSearch,
        permission: Permission.Manage,
      });
      return response.data;
    },
    enabled: debouncedSpaceSearch.length > 0,
  });

  const handleSearch = (value: string) => {
    setSpaceSearch(value);
  };

  const handleDeleteSpace = (entityId: string) => {
    const communityEntityVisibility = data.communityentity_visibility?.filter(
      (item) => item.community_entity?.id !== entityId
    );
    setData({ ...data, communityentity_visibility: communityEntityVisibility });
  };

  const handleSelect = (entityVisibilityData: FeedEntityCommunityEntityVisibilityModel) => {
    const communityEntityVisibility = data.communityentity_visibility?.length
      ? [...data?.communityentity_visibility, entityVisibilityData]
      : [entityVisibilityData];
    setSpaceSearch('');
    setData({ ...data, communityentity_visibility: communityEntityVisibility });
  };

  const getSpaceType = (label?: string, type?: CommunityEntityType) => {
    if (label) {
      return label;
    }
    if (type === CommunityEntityType.Node) {
      return t('hub.one');
    }
    return t('workspace.one');
  };

  const handleSpaceChange = (spaceVisibilityData: FeedEntityCommunityEntityVisibilityModel) => {
    const spaceVisibility = data.communityentity_visibility?.map((item) => {
      if (item.community_entity?.id === spaceVisibilityData.community_entity.id) {
        return spaceVisibilityData;
      }
      return item;
    });
    setData({ ...data, communityentity_visibility: spaceVisibility });
  };

  const selectedSpaces = data.communityentity_visibility?.flatMap((item) => item.community_entity) || [];
  const spaceOptions =
    spaces
      ?.filter((searchedSpace) => !selectedSpaces.some((selectedSpace) => searchedSpace.id === selectedSpace.id))
      ?.map((space) => ({
        value: `${space.id}`,
        label: (
          <Row gutter={[8, 8]}>
            <Col span={2}>
              <BorderStyled size={26}>
                {space.logo_url ? (
                  <Image src={space.logo_url} width={26} height={26} alt={toInitials(space.title || '')} />
                ) : (
                  <Typography.Text>{toInitials(space.title || '')} </Typography.Text>
                )}
              </BorderStyled>
            </Col>
            <Col span={12}>
              <Typography.Text type="secondary" ellipsis>
                {space.title}
              </Typography.Text>
            </Col>

            <Col span={10}>
              <Typography.Text type="secondary" ellipsis>
                {capitalize(getSpaceType(space.label, space.type))}
              </Typography.Text>
            </Col>
          </Row>
        ),
        item: space,
      })) || [];

  return (
    <Flex vertical gap="middle">
      <Typography.Title level={5}>{t('spacesWithAccess')}</Typography.Title>
      <AutoComplete
        disabled={!canManage}
        value={spaceSearch}
        options={spaceOptions}
        onSearch={handleSearch}
        onSelect={(_, options) => {
          handleSelect({ community_entity: options.item, visibility: FeedEntityVisibility.View });
        }}
        placeholder={t('addSpaces')}
        notFoundContent={spaceSearch.length && isFetched && !spaces?.length ? <Empty /> : null}
      />
      <SpaceScrollbarStyled>
        <FlexStyled vertical gap="small">
          {data?.communityentity_visibility?.map((item) => {
            const initials = toInitials(item.community_entity?.title || '');
            return (
              <Flex vertical gap="middle" key={item.community_entity?.id}>
                <Flex gap="middle" justify="space-between">
                  <SpaceInfoStyled gap="small">
                    <BorderStyled>
                      {item.community_entity?.logo_url ? (
                        <Image src={item.community_entity.logo_url} width={36} height={36} alt={initials} />
                      ) : (
                        <Typography.Text>{initials} </Typography.Text>
                      )}
                    </BorderStyled>
                    <SpaceNameStyled vertical>
                      <Typography.Text strong ellipsis={{ tooltip: item.community_entity?.title }}>
                        {item.community_entity?.title}
                      </Typography.Text>
                      <Typography.Text type="secondary">
                        {capitalize(getSpaceType(item.community_entity?.label, item.community_entity?.type))}
                      </Typography.Text>
                    </SpaceNameStyled>
                  </SpaceInfoStyled>

                  <Select
                    disabled={!canManage}
                    defaultValue={item.visibility}
                    style={{ width: 150 }}
                    onChange={(val) => {
                      handleSpaceChange({ ...item, visibility: val });
                    }}
                    options={[
                      { value: FeedEntityVisibility.Edit, label: t('entityVisibility.canEdit') },
                      { value: FeedEntityVisibility.Comment, label: t('entityVisibility.canComment') },
                      { value: FeedEntityVisibility.View, label: t('entityVisibility.canView') },
                    ]}
                    dropdownRender={(menu) => (
                      <>
                        {menu}
                        <DividerStyled />
                        <Button type="text" onClick={() => handleDeleteSpace(item.community_entity?.id)}>
                          {t('action.remove')}
                        </Button>
                      </>
                    )}
                  />
                </Flex>
              </Flex>
            );
          })}
        </FlexStyled>
      </SpaceScrollbarStyled>
    </Flex>
  );
};

export default SpacesWithAccess;
