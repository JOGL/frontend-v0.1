import styled from '@emotion/styled';
import { Flex } from 'antd';
import { ScrollbarStyled } from '~/src/assets/styles/Global.styled';

export const BorderStyled = styled.div<{ size?: number }>`
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;

  img {
    border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;
    border: 2px solid ${({ theme }) => theme.token.neutral5};
  }
  span {
    width: ${({ size = 36 }) => size}px;
    height: ${({ size = 36 }) => size}px;
    line-height: ${({ size = 36 }) => size - 3}px;
    display: inline-block;
    text-transform: uppercase;
    text-align: center;
    font-size: ${({ size }) => (size ? 8 : 10)}px;
    border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;
    border: 2px solid ${({ theme }) => theme.token.neutral5};
    background: ${({ theme }) => theme.token.neutral1};
  }
`;

export const SpaceScrollbarStyled = styled(ScrollbarStyled)`
  margin: 0 -${({ theme }) => theme.token.paddingXS}px;
`;

export const FlexStyled = styled(Flex)`
  max-height: 155px;
  overflow-y: auto;
  padding: 0 ${({ theme }) => theme.token.paddingXS}px;
`;

export const SpaceInfoStyled = styled(Flex)`
  max-width: calc(100% - 200px);
`;

export const SpaceNameStyled = styled(Flex)`
  width: 100%;
`;
