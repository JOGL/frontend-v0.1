import { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Button, Flex, Modal, message, Typography, Space } from 'antd';
import PeopleWithAccess from './peopleWithAccess/peopleWithAccess';
import SpacesWithAccess from './spacesWithAccess/spacesWithAccess';
import { GlobalOutlined, LinkOutlined, LockOutlined } from '@ant-design/icons';
import { DocumentModel, NeedModel, PaperModel, Permission } from '~/__generated__/types';
import GeneralAccess from './generalAccess/generalAccess';
import { ButtonStyled } from './share.styles';

interface ShareProps {
  data: NeedModel | DocumentModel | PaperModel;
  handleShare: (data: NeedModel | DocumentModel | PaperModel) => void;
}

const Share: FC<ShareProps> = ({ data: shareData, handleShare }) => {
  const { t } = useTranslation('common');
  const [showModal, setShowModal] = useState(false);
  const [data, setData] = useState(shareData);
  const [copying, setCopying] = useState(false);
  const canManage = data?.permissions.includes(Permission.Manage);

  const handleCancel = () => {
    setData(shareData);
    setShowModal(false);
  };

  const handleOk = () => {
    handleShare(data);
    setShowModal(false);
  };

  const copyToClipboard = async () => {
    setCopying(true);
    try {
      const currentUrl = window.location.href;
      await navigator.clipboard.writeText(currentUrl);
      message.success(t('linkCopied'));
    } catch (err) {
      message.error(t('failedToCopyLink'));
    } finally {
      setCopying(false);
    }
  };

  return (
    <>
      <Button
        type="primary"
        icon={shareData.default_visibility ? <GlobalOutlined /> : <LockOutlined />}
        onClick={() => setShowModal(true)}
      >
        {t('action.share')}
      </Button>
      <Modal
        open={showModal}
        onCancel={handleCancel}
        onOk={handleOk}
        okText={t('done')}
        footer={[
          <Flex justify="space-between">
            <ButtonStyled
              key="copyLink"
              icon={<LinkOutlined />}
              onClick={copyToClipboard}
              loading={copying}
              type="text"
            >
              <Typography.Text strong> {t('action.copyLink')}</Typography.Text>
            </ButtonStyled>
            {canManage && (
              <Space>
                <Button key="cancel" onClick={handleCancel}>
                  {t('action.cancel')}
                </Button>
                <Button key="submit" type="primary" onClick={handleOk}>
                  {t('action.save')}
                </Button>
              </Space>
            )}
          </Flex>,
        ]}
      >
        <Flex vertical gap="large">
          <PeopleWithAccess data={data} setData={setData} />
          <SpacesWithAccess data={data} setData={setData} />
          <GeneralAccess data={data} setData={setData} />
        </Flex>
      </Modal>
    </>
  );
};

export default Share;
