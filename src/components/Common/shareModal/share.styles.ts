import styled from '@emotion/styled';
import { Button, Divider } from 'antd';

export const ButtonStyled = styled(Button)`
  width: fit-content;
  svg {
    color: ${({ theme }) => theme.token.colorPrimary};
    font-weight: bold;
    font-size: 18px;
  }
`;

export const DividerStyled = styled(Divider)`
  margin: ${({ theme }) => theme.token.paddingXS}px 0 0 0;
`;
