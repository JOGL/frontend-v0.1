import { Dispatch, FC, SetStateAction } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Flex, Select, Typography } from 'antd';
import { DocumentModel, FeedEntityVisibility, NeedModel, PaperModel, Permission } from '~/__generated__/types';

interface GeneralAccessProps {
  data: NeedModel | DocumentModel | PaperModel;
  setData: Dispatch<SetStateAction<NeedModel | DocumentModel | PaperModel>>;
}

const GeneralAccess: FC<GeneralAccessProps> = ({ data, setData }) => {
  const { t } = useTranslation('common');
  const canManage = data?.permissions.includes(Permission.Manage);

  const handleAccessChange = (value: string) => {
    if (value === 'restricted') {
      setData({ ...data, default_visibility: undefined });
    } else {
      setData({ ...data, default_visibility: FeedEntityVisibility.View });
    }
  };

  const handleVisibilityChange = (value: FeedEntityVisibility) => {
    setData({ ...data, default_visibility: value });
  };

  return (
    <Flex vertical gap="middle">
      <Typography.Title level={5}>{t('generalAccess')}</Typography.Title>
      <Flex gap="middle" justify="space-between">
        <Select
          disabled={!canManage}
          value={data.default_visibility ? 'anyone' : 'restricted'}
          style={{ width: 200 }}
          onChange={handleAccessChange}
          options={[
            { value: 'restricted', label: t('restricted') },
            { value: 'anyone', label: t('anyoneWithTheLink') },
          ]}
        />

        {data.default_visibility && (
          <Select
            disabled={!canManage}
            value={data.default_visibility}
            style={{ width: 150 }}
            onChange={handleVisibilityChange}
            options={[
              { value: FeedEntityVisibility.Edit, label: t('entityVisibility.canEdit') },
              { value: FeedEntityVisibility.Comment, label: t('entityVisibility.canComment') },
              { value: FeedEntityVisibility.View, label: t('entityVisibility.canView') },
            ]}
          />
        )}
      </Flex>
      {!data.default_visibility && (
        <Typography.Text type="secondary">{t('onlyPeopleWithAccessCanOpenWithTheLink')}</Typography.Text>
      )}
    </Flex>
  );
};

export default GeneralAccess;
