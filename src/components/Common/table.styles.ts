import styled from '@emotion/styled';
import { Table } from 'antd';

export const TableStyled = styled(Table)`
  cursor: pointer;
  
  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    overflow-x: auto;
    table {
      width: unset;
    }
  }

  && .new {
    background-color: ${({ theme }) => theme.colors.new};
  }
`;