import styled from '@emotion/styled';
import { DotProps } from './Carousel.types';
import { SnapItem } from 'react-snaplist-carousel';

export const CarouselContainer = styled.div<{ fullSize: boolean }>`
  width: ${(props) => (props.fullSize ? '100%' : 'auto')};
  height: ${(props) => (props.fullSize ? '100%' : 'auto')};
  position: relative;
`;

export const ArrowLeft = styled.button`
  background: none;
  color: inherit;
  border: none;
  padding: 0;
  font: inherit;
  cursor: pointer;
  outline: inherit;
  position: absolute;
  bottom: 0;
  left: 0;
  top: calc(50% - 45px);
  height: 2rem;
  width: 2rem;
  border-radius: 50%;
  box-shadow: ${({ theme }) => theme.shadows.default};
  background-color: white;
  z-index: 8;
`;

export const ArrowRight = styled.button`
  background: none;
  color: inherit;
  border: none;
  padding: 0;
  font: inherit;
  cursor: pointer;
  outline: inherit;
  position: absolute;
  bottom: 0;
  right: 0;
  top: calc(50% - 45px);
  height: 2rem;
  width: 2rem;
  border-radius: 50%;
  box-shadow: ${({ theme }) => theme.shadows.default};
  background-color: white;
  z-index: 8;
`;

export const Dot = styled.button<DotProps>`
  background: none;
  color: inherit;
  border: none;
  padding: 0;
  font: inherit;
  cursor: pointer;
  outline: inherit;
  border-radius: 50%;
  width: 10px;
  height: 10px;
  background-color: ${(props) => (props.active ? props.theme.colors.greys['800'] : props.theme.colors.greys['500'])};
  &:hover {
    background-color: ${({ theme }) => theme.colors.greys['800']};
  }
`;

export const SnapItemContainer = styled(SnapItem)<{ fullSize: boolean; spaceX: string }>`
  margin-left: ${({ spaceX, fullSize }) => (!fullSize ? spaceX : '0')};
  margin-right: ${({ spaceX }) => spaceX};
  margin-top: 0;
  margin-bottom: 0.5rem;
  width: ${(props) => (props.fullSize ? '100%' : 'auto')};
  height: ${(props) => (props.fullSize ? '100%' : 'auto')};
  padding-left: ${(props) => (props.fullSize ? '5px' : '0')};
  padding-top: ${(props) => (props.fullSize ? '5px' : '0')};
`;

export const DotsContainer = styled.div<{ fullSize: boolean; noDotsPadding: boolean }>`
  display: flex;
  justify-content: center;
  gap: 8px;

  position: ${(props) => (props.fullSize ? 'relative' : 'static')};
  bottom: ${(props) => (props.fullSize ? '4rem' : 0)};
  padding-top: ${(props) => (props.noDotsPadding ? 0 : '1rem')};
`;
