import React, { FC, useEffect, useRef, useState } from 'react';
import { SnapList, useDragToScroll, useScroll, useVisibleElements } from 'react-snaplist-carousel';
import Icon from '~/src/components/primitives/Icon';
import { CarouselProps } from './Carousel.types';
import { ArrowLeft, ArrowRight, CarouselContainer, Dot, DotsContainer, SnapItemContainer } from './Carousel.styles';

const Carousel: FC<CarouselProps> = ({
  noPadding = false,
  children,
  showDots = true,
  fullSize = false,
  noArrows = false,
  noDotsPadding = false,
}) => {
  const snapList = useRef(null);
  const [showControls, setShowControls] = useState(true);
  const [hideArrow, setHideArrow] = useState<undefined | 'left' | 'right'>('left');
  const selectItems = useVisibleElements({ debounce: 10, ref: snapList }, (elements) => elements);
  const spaceX = noPadding ? 0 : '3px';

  useEffect(() => {
    // Hide controls when there's no invisible elements aka the children don't overflow
    if (selectItems.length === children.length) {
      setShowControls(false);
    } else {
      setShowControls(true);
    }
    // Hide right arrow when last selectItems is visible
    if (children.length - 1 === selectItems[selectItems.length - 1]) {
      setHideArrow('right');
    } else if (selectItems[0] === 0) {
      // Hide left arrow when selectItems first visible element is the first children.
      setHideArrow('left');
    } else {
      setHideArrow(undefined);
    }
  }, [selectItems]);

  const goToElement = useScroll({ ref: snapList });
  useDragToScroll({ ref: snapList });

  return (
    <CarouselContainer>
      {showControls && !noArrows && (
        <>
          {hideArrow !== 'left' && (
            <ArrowLeft type="button" onClick={() => goToElement(selectItems[0] - 1)}>
              <Icon icon="formkit:arrowleft" tw="w-4 h-4" />
            </ArrowLeft>
          )}
          {hideArrow !== 'right' && (
            <ArrowRight type="button" onClick={() => goToElement(selectItems[selectItems.length - 1] + 1)}>
              <Icon icon="formkit:arrowright" tw="w-4 h-4" />
            </ArrowRight>
          )}
        </>
      )}

      <SnapList ref={snapList} width="100%" direction="horizontal" height={fullSize && '100%'}>
        {children.map((child, i) => (
          <SnapItemContainer key={i} spaceX={spaceX} fullSize={fullSize} snapAlign={i === 0 ? 'start' : 'center'}>
            {child}
          </SnapItemContainer>
        ))}
      </SnapList>

      {showDots && showControls && (
        <DotsContainer fullSize={fullSize} noDotsPadding={noDotsPadding}>
          {children.map((_, index) => (
            <Dot key={index} active={selectItems.includes(index)} type="button" onClick={() => goToElement(index)} />
          ))}
        </DotsContainer>
      )}
    </CarouselContainer>
  );
};

export default Carousel;
