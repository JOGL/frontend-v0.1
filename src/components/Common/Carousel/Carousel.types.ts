export interface CarouselProps {
  noPadding?: boolean;
  showDots?: boolean;
  children: JSX.Element[];
  fullSize?: boolean;
  noArrows?: boolean;
  noDotsPadding?: boolean;
}

export interface DotProps {
  active: boolean;
}
