import { FC } from "react";

export const Disabled: FC<{
  disabled?: boolean;
  children: React.ReactNode;
}> = ({ disabled = false, children }) => {
  return (
    <div
      style={{
        opacity: disabled ? 0.5 : 1,
        pointerEvents: disabled ? 'none' : 'auto',
        cursor: disabled ? 'not-allowed' : 'auto',
      }}
    >
      {children}
    </div>
  );
};