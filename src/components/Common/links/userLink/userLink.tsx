import { Avatar, Typography } from 'antd';
import { FC } from 'react';
import { UserMiniModel } from '~/__generated__/types';
import { useRouter } from 'next/router';
import useUsers from '~/src/components/users/useUsers';
import { ButtonLinkStyled } from '../../common.styles';
import { UserOutlined } from '@ant-design/icons';

interface UserLinkProps {
  user?: UserMiniModel;
}

export const UserLink: FC<UserLinkProps> = ({ user }) => {
  const router = useRouter();
  const [, getUserUrl] = useUsers();

  if (!user) return <></>;

  return (
    <ButtonLinkStyled
      size="small"
      type="link"
      href={getUserUrl(user.id)}
      onClick={(e) => {
        e.preventDefault();
        e.stopPropagation();
        router.push(getUserUrl(user.id));
      }}
    >
      <Avatar size="small" shape="square" icon={<UserOutlined />} src={user.logo_url} />
      <Typography.Text>{`${user.first_name} ${user.last_name}`}</Typography.Text>
    </ButtonLinkStyled>
  );
};
