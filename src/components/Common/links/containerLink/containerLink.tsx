import { Avatar, Image, Typography } from 'antd';
import { FC } from 'react';
import { CommunityEntityMiniModel } from '~/__generated__/types';
import { useRouter } from 'next/router';
import { entityItem } from '~/src/utils/utils';
import { ButtonLinkStyled } from '../../common.styles';

interface ContainerLinkProps {
  container?: CommunityEntityMiniModel;
}

export const ContainerLink: FC<ContainerLinkProps> = ({ container }) => {
  const router = useRouter();

  if (!container) return <></>;

  return (
    <ButtonLinkStyled
      size="small"
      type="link"
      href={`/${entityItem[container.type]?.front_path}/${container.id}`}
      onClick={(e) => {
        e.preventDefault();
        e.stopPropagation();
        router.push(`/${entityItem[container.type]?.front_path}/${container.id}`);
      }}
    >
      <Avatar size="small" shape="square" src={container.logo_url} />
      <Typography.Text ellipsis={{ tooltip: container.title }}>{container.title}</Typography.Text>
    </ButtonLinkStyled>
  );
};
