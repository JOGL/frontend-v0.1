import { Avatar, Typography } from 'antd';
import { FC } from 'react';
import { EntityMiniModel } from '~/__generated__/types';
import { useRouter } from 'next/router';
import { entityItem } from '~/src/utils/utils';
import { ButtonLinkStyled } from '../../common.styles';

interface FeedEntityLinkProps {
  entity?: EntityMiniModel;
}

export const FeedEntityLink: FC<FeedEntityLinkProps> = ({ entity }) => {
  const router = useRouter();

  if (!entity) return <></>;

  return (
    <ButtonLinkStyled
      size="small"
      type="link"
      href={`/${entityItem[entity.entity_type]?.front_path}/${entity.id}`}
      onClick={(e) => {
        e.preventDefault();
        e.stopPropagation();
        router.push(`/${entityItem[entity.entity_type]?.front_path}/${entity.id}`);
      }}
    >
      <Avatar size="small" shape="square" src={entity.logo_url} />
      <Typography.Text ellipsis={{ tooltip: entity.title }}> {entity.title}</Typography.Text>
    </ButtonLinkStyled>
  );
};
