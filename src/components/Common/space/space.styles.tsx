import styled from '@emotion/styled';

export const SpaceContentStyled = styled.div`
  width: 80%;
  min-width: 80%;
  max-width: 914px;
  margin: 0 auto;
`;
