import styled from '@emotion/styled';
import { Space } from 'antd';

export const WrapperStyled = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
`;

export const HeaderStyled = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  margin-bottom: ${({ theme }) => theme.space[7]};

  @media screen and (min-width: ${({ theme }) => theme.token.screenSM}px) {
    flex-direction: row;
    justify-content: space-between;
    align-items: start;
  }
`;

export const SpaceStyled = styled(Space)`
  margin-left: auto;
`;
