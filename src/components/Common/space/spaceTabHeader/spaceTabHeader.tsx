import { FC } from 'react';
import { Button, Input, Typography } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { HeaderStyled, SpaceStyled, WrapperStyled } from './spaceTabHeader.styles';
import { PlusOutlined } from '@ant-design/icons';

const { Search } = Input;

interface SpaceHeaderProps {
  title?: string;
  searchText: string;
  setSearchText: (searchText: string) => void;
  canCreate?: boolean;
  handleClick?: () => void;
  showSearch?: boolean;
}

export const SpaceTabHeader: FC<SpaceHeaderProps> = ({
  title,
  searchText,
  setSearchText,
  canCreate,
  handleClick,
  showSearch,
}) => {
  const { t } = useTranslation('common');

  return (
    <WrapperStyled>
      <HeaderStyled>
        <Typography.Title level={3}>{title}</Typography.Title>

        <SpaceStyled>
          {showSearch && (
            <Search
              placeholder={t('action.searchHere')}
              value={searchText}
              onChange={(e) => setSearchText(e.target.value)}
              onSearch={(value) => setSearchText(value)}
            />
          )}
          {canCreate && (
            <Button icon={<PlusOutlined />} onClick={handleClick}>
              {t('action.add')}
            </Button>
          )}
        </SpaceStyled>
      </HeaderStyled>
    </WrapperStyled>
  );
};
