import styled from '@emotion/styled';

export const ReadOnlyWrapperStyled = styled.div`
  border: 1px solid ${({ theme }) => theme.token.colorBorder};
  border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;
  min-height: calc(100vh - 330px);
  padding: ${({ theme }) => theme.token.paddingLG}px;
  background-color: ${({ theme }) => theme.token.neutral2};
  img {
    max-width: 100%;
  }

  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    iframe[src*='youtube.com'],
    iframe[src*='youtu.be'] {
      aspect-ratio: 16/9;
      width: 100%;
      max-width: 100%;
      height: auto;
      border: none;
    }
  }
`;
