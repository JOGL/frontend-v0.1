import { Dispatch, FC, SetStateAction, useRef, useState } from 'react';
import { DocumentInsertModel, DocumentUpdateModel, NeedUpsertModel } from '~/__generated__/types';
import useTranslation from 'next-translate/useTranslation';
import ReadOnlyEditor from '~/src/components/tiptap/readOnlyEditor/readOnlyEditor';
import { JoglDocEditor } from '~/src/components/tiptap/joglDocEditor/joglDocEditor';
import useOnClickOutside from '~/src/hooks/useOnClickOutside';
import { ReadOnlyWrapperStyled } from './descriptionEditor.styles';

interface DescriptionEditorProps {
  canEdit: boolean;
  data: NeedUpsertModel | DocumentInsertModel | DocumentUpdateModel;
  setData: Dispatch<SetStateAction<NeedUpsertModel | DocumentInsertModel | DocumentUpdateModel>>;
  handleClickOutside: () => void;
}

const DescriptionEditor: FC<DescriptionEditorProps> = ({ canEdit, data, setData, handleClickOutside }) => {
  const ref = useRef(null);
  const { t } = useTranslation('common');
  const [showContentEditor, setShowContentEditor] = useState(canEdit);

  const handleChange = (content: string) => {
    // Ensure content is properly stringified with YouTube embeds
    setData({
      ...data,
      description: content,
    });
  };

  const handleClickOutsideContent = () => {
    // Only hide editor if there's content
    if (data.description) {
      setShowContentEditor(false);
    }
    handleClickOutside();
  };

  useOnClickOutside(ref, handleClickOutsideContent, ['.ant-dropdown', '.ant-popover', '.ant-modal']);

  return (
    <div ref={ref}>
      {!showContentEditor ? (
        <ReadOnlyWrapperStyled onClick={() => canEdit && setShowContentEditor(true)}>
          <ReadOnlyEditor content={data.description ?? ''} focus={false} />
        </ReadOnlyWrapperStyled>
      ) : (
        <JoglDocEditor content={data.description} defaultPlaceholder={t('typeHere')} handleChange={handleChange} />
      )}
    </div>
  );
};
export default DescriptionEditor;
