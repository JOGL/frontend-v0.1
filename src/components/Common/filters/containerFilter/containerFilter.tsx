import { Button, Checkbox, Dropdown, MenuProps, DropdownProps } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { FC, ReactElement, useState } from 'react';
import { CommunityEntityMiniModel } from '~/__generated__/types';

interface KeyWordsProps {
  icon?: ReactElement;
  containers: CommunityEntityMiniModel[];
  onChange: (communityEntityIds: string[]) => void;
}

export const ContainerFilter: FC<KeyWordsProps> = ({ icon = null, containers, onChange }) => {
  const [open, setOpen] = useState(false);
  const [selectedItems, setSelectedItems] = useState<CommunityEntityMiniModel[]>([]);
  const { t } = useTranslation('common');

  const handleMenuClick: MenuProps['onClick'] = (e) => {
    if (e.key === '3') {
      setOpen(false);
    }
  };

  const handleOpenChange: DropdownProps['onOpenChange'] = (nextOpen, info) => {
    if (info.source === 'trigger' || nextOpen) {
      setOpen(nextOpen);
      if (nextOpen === false) {
        onChange(selectedItems.map((item) => item.id));
      }
    }
  };

  const menu = {
    items: containers.map((item) => ({
      key: item.id,
      label: (
        <Checkbox
          checked={selectedItems.includes(item)}
          onChange={(e) => {
            if (e.target.checked) {
              setSelectedItems([...selectedItems, item]);
            } else {
              setSelectedItems(selectedItems.filter((i) => i !== item));
            }
          }}
        >
          {item.title !== '' ? item.title : t('untitled')}
        </Checkbox>
      ),
    })),
    onClick: handleMenuClick,
  };

  return (
    <Dropdown menu={menu} onOpenChange={handleOpenChange} open={open}>
      <Button>
        {selectedItems.length === 0 || selectedItems.length === containers.length
          ? t('workspace.all')
          : t('selected', { count: selectedItems.length })}
      </Button>
    </Dropdown>
  );
};
