import styled from '@emotion/styled';
import { Select } from 'antd';

export const SelectStyled = styled(Select)`
  && .ant-select-selection-placeholder {
    color: black !important;
  }
`;
