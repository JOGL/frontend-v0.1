import { Radio } from 'antd';
import { FC } from 'react';
import { SelectStyled } from './filter.styles';

interface FilterValue {
  value: string;
  label: string;
}

interface Props {
  value?: string;
  placeholder?: string;
  items: FilterValue[];
  onChange: (value: string) => void;
  mode?: 'radio' | 'select';
}

export const Filter: FC<Props> = ({ value, placeholder, items, onChange, mode }) => {
  switch (mode) {
    case 'radio':
      return (
        <Radio.Group buttonStyle="solid" onChange={(e) => onChange(e.target.value)} value={value}>
          {items.map((item) => (
            <Radio.Button key={item.value} value={item.value}>
              {item.label}
            </Radio.Button>
          ))}
        </Radio.Group>
      );
    default:
      return (
        <SelectStyled allowClear defaultValue={value} onChange={onChange} options={items} placeholder={placeholder} />
      );
  }
};
