import { Flex, Spin } from 'antd';

const Loader = () => {
  return (
    <Flex vertical align="center" justify="center">
      <Spin />
    </Flex>
  );
};

export default Loader;
