import { FC } from 'react';
import { Typography } from 'antd';
import Image from 'next/image';
import { entityItem, toInitials } from '~/src/utils/utils';
import { BorderStyled } from './containerLogo.styles';

import { CommunityEntityChannelModel } from '~/__generated__/types';

const ContainerLogo: FC<{ container: CommunityEntityChannelModel }> = ({ container }) => {
  const src = container.logo_url_sm || container.logo_url;
  const alt = container.short_title || '';
  const bannerSrc = container.type !== 'node' ? entityItem[container.type]?.default_banner : '';

  const initials = toInitials(container.title || '');

  if (src || bannerSrc) {
    return (
      <BorderStyled>
        <Image src={src || bannerSrc} width={27} height={27} alt={alt} />
      </BorderStyled>
    );
  }

  return (
    <BorderStyled>
      <Typography.Text>{initials} </Typography.Text>
    </BorderStyled>
  );
};

export default ContainerLogo;
