import styled from '@emotion/styled';

export const BorderStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;

  img {
    border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;
    border: 2px solid ${({ theme }) => theme.token.neutral5};
  }
  span {
    width: 27px;
    height: 27px;
    line-height: 24px;
    display: inline-block;
    text-transform: uppercase;
    text-align: center;
    font-size: 10px;
    border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;
    border: 2px solid ${({ theme }) => theme.token.neutral5};
    background: ${({ theme }) => theme.token.neutral1};
  }
`;
