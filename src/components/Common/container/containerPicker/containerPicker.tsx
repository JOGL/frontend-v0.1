import React, { FC, useEffect, useState } from 'react';
import { Row, Col, Typography, Flex, AutoComplete, Image, Empty } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import useDebounce from '~/src/hooks/useDebounce';
import { useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import api from '~/src/utils/api/api';
import { capitalize, toInitials } from '~/src/utils/utils';
import { CommunityEntityMiniModel, CommunityEntityType, Permission } from '~/__generated__/types';
import { BorderStyled } from './containerPicker.styles';

const ContainerPicker: FC<{
  hubId?: string;
  value?: string;
  permission?: Permission;
  onChange: (container?: CommunityEntityMiniModel) => void;
}> = ({ hubId, value, permission = Permission.Manage,  onChange }) => {
  const { t } = useTranslation('common');
  const [spaceSearch, setSpaceSearch] = useState<string>();
  const debouncedSpaceSearch = useDebounce(spaceSearch, 300);

  const { data: spaces, isFetched } = useQuery({
    queryKey: [QUERY_KEYS.communityEntitiesList, { search: debouncedSpaceSearch }],
    queryFn: async () => {
      const response = await api.entities.communityEntitiesList({
        id: hubId,
        Search: debouncedSpaceSearch,
        permission: permission
      });
      return response.data;
    },
  });

  useEffect(() => {
    if (isFetched && spaces) {
      const space = spaces.find((sp) => sp.id === value);
      space && setSpaceSearch(space.title);
      onChange(space);
    }
  }, [isFetched, spaces]); 

  const handleSearch = (value: string) => {
    setSpaceSearch(value);
    onChange(undefined);
  };

  const getSpaceType = (label?: string, type?: CommunityEntityType) => {
    if (label) {
      return label;
    }
    if (type === CommunityEntityType.Node) {
      return t('hub.one');
    }
    return t('workspace.one');
  };

  const spaceOptions =
    spaces?.map((space) => ({
      value: `${space.id}`,
      label: (
        <Row gutter={[8, 8]}>
          <Col span={2}>
            <BorderStyled size={26}>
              {space.logo_url ? (
                <Image src={space.logo_url} width={26} height={26} alt={toInitials(space.title || '')} />
              ) : (
                <Typography.Text>{toInitials(space.title || '')} </Typography.Text>
              )}
            </BorderStyled>
          </Col>
          <Col span={12}>
            <Typography.Text type="secondary" ellipsis>
              {space.title}
            </Typography.Text>
          </Col>

          <Col span={10}>
            <Typography.Text type="secondary" ellipsis>
              {capitalize(getSpaceType(space.label, space.type))}
            </Typography.Text>
          </Col>
        </Row>
      ),
      item: space,
    })) || [];

  return (
    <Flex vertical gap="large">
      <Flex vertical>
        <Typography.Text> {t('selectASpace')}</Typography.Text>
        <AutoComplete
          value={spaceSearch}
          options={spaceOptions}
          onSearch={handleSearch}
          onSelect={(_, options) => {
            setSpaceSearch(options.item.title);
            onChange(options.item);
          }}
          onDeselect={(_, options) => {
            setSpaceSearch('');
            onChange(undefined);
          }}
          placeholder={t('addSpaces')}
          notFoundContent={spaceSearch?.length && isFetched && !spaces?.length ? <Empty /> : null}
        />
      </Flex>
    </Flex>
  );
};

export default ContainerPicker;
