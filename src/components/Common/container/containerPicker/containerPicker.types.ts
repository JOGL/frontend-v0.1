export type Container = {
  id: string;
  title: string;
};