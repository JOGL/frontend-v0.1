import { FC, useState } from 'react';
import { Typography, Flex, Button } from 'antd';
import { DeleteOutlined, LoadingOutlined, UploadOutlined } from '@ant-design/icons';
import { RcFile } from 'antd/es/upload';
import Image from 'next/image';
import { MAX_PAYLOAD_SIZE } from '~/src/utils/constants';
import { useMutation } from '@tanstack/react-query';
import useUser from '~/src/hooks/useUser';
import api from '~/src/utils/api/api';
import { ImageInsertModel, ImageInsertResultModel } from '~/__generated__/types';
import useTranslation from 'next-translate/useTranslation';
import { DeleteStyled, UploadStyled } from './imageUpload.styles';

const { Text } = Typography;

interface ImageUploadProps {
  handleFileUpload: (data: ImageInsertResultModel | undefined) => void;
  width?: number;
  height?: number;
  imageSrc?: string;
}

const ImageUpload: FC<ImageUploadProps> = ({ handleFileUpload, imageSrc, width = 90, height = 90 }) => {
  const { t } = useTranslation('common');
  const [uploadFileError, setUploadFileError] = useState<string>('');
  const [uploadedImage, setUploadedImage] = useState<string | undefined>(imageSrc);
  const [isLoading, setLoading] = useState(false);
  const { user } = useUser();

  const mutation = useMutation({
    mutationFn: async (imageInfo: ImageInsertModel) => {
      const response = await api.images.imagesCreate(imageInfo);
      return response.data;
    },
    onSuccess: (data) => {
      setLoading(false);
      handleFileUpload && handleFileUpload(data as any);
    },
    onError: (error: Error) => {
      setLoading(false);
      setUploadFileError(error.message);
    },
  });

  const hasValidPayloadSize = (file: RcFile): boolean => {
    if (file.size > MAX_PAYLOAD_SIZE) {
      setLoading(false);
      setUploadFileError(t('error.maxPayloadSizeExceeded'));
      return false;
    }
    setUploadFileError('');
    return true;
  };

  const beforeUpload = (file: RcFile): boolean => {
    setLoading(true);

    if (hasValidPayloadSize(file)) {
      const reader = new FileReader();
      reader.onload = (e) => {
        const base64 = e.target?.result as string;

        const imageInfo = {
          title: file.name,
          file_name: file.name,
          description: '',
          created: file?.lastModified ? new Date(file.lastModified).toISOString() : new Date().toISOString(),
          updated: file?.lastModified ? new Date(file.lastModified).toISOString() : new Date().toISOString(),
          last_activity: file?.lastModified ? new Date(file.lastModified).toISOString() : new Date().toISOString(),
          data: base64,
          created_by_user_id: user.id,
        };

        setUploadedImage(base64);
        mutation.mutate(imageInfo);
      };
      reader.readAsDataURL(file);
    }
    return false;
  };

  return (
    <>
      <UploadStyled
        name="logo"
        listType="picture-card"
        accept="image/jpeg, image/png, image/jpg"
        beforeUpload={beforeUpload}
        showUploadList={false}
        multiple={false}
        width={width}
        height={height}
      >
        <>
          {uploadedImage && !isLoading ? (
            <>
              <Image src={uploadedImage} alt="Uploaded" fill objectFit="cover" />
              <DeleteStyled>
                <Button
                  size="small"
                  type="primary"
                  icon={<DeleteOutlined />}
                  onClick={(e) => {
                    e.stopPropagation();
                    setUploadedImage(undefined);
                    handleFileUpload(undefined);
                  }}
                />
              </DeleteStyled>
            </>
          ) : (
            <Flex vertical justify="center" align="center">
              {isLoading ? <LoadingOutlined /> : <UploadOutlined />}
              <Text> {t('action.upload')}</Text>
            </Flex>
          )}
        </>
      </UploadStyled>
      {uploadFileError && <Text type="danger">{uploadFileError}</Text>}
    </>
  );
};

export default ImageUpload;
