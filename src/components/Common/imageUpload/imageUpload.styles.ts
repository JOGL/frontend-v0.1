import styled from '@emotion/styled';
import { Upload } from 'antd';

export const UploadStyled = styled(Upload)<{ width: number; height: number }>`
  width: ${(props) => props.width}px;
  height: ${(props) => props.height}px;

  .ant-upload {
    width: 100% !important;
    height: 100% !important;
    position: relative;
  }
`;

export const DeleteStyled = styled.div`
  position: absolute;
  right: 5px;
  bottom: 5px;
`;
