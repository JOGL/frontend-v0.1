import styled from '@emotion/styled';

export const BorderStyled = styled.div<{ selected: boolean }>`
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  border-radius: ${({ theme }) => `${theme.token.borderRadiusLG}px`};
  box-shadow: ${(props) =>
    props.selected
      ? `0 0 0 2px ${props.theme.token.neutral1}, 0 0 0 calc(2px + 2px) ${props.theme.token.colorPrimary}, 0 0 ${props.theme.token.neutral13}`
      : 'none'};

  img {
    border-radius: ${({ theme }) => `${theme.token.borderRadiusLG}px`};
    border: ${(props) =>
      props.selected ? `2px solid ${props.theme.token.colorPrimary}` : `2px solid ${props.theme.token.neutral5}`};
    &:hover {
      border-color: ${({ theme }) => theme.token.colorPrimary};
    }
  }
`;
