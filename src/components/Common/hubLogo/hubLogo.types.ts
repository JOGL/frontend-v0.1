export interface HubLogoProps {
  selected?: boolean;
  src: string;
  alt: string;
}
