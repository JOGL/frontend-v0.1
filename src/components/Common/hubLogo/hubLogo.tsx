import { FC } from 'react';
import { HubLogoProps } from './hubLogo.types';
import { BorderStyled } from './hubLogo.styles';
import Image from 'next/image';

const HubLogo: FC<HubLogoProps> = ({ selected, src, alt }) => {
  return (
    <BorderStyled selected={!!selected}>
      <Image src={src} width={40} height={40} alt={alt} />
    </BorderStyled>
  );
};

export default HubLogo;
