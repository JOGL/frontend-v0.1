import styled from '@emotion/styled';
import { Badge, Button, Flex, Space } from 'antd';

export const FlexFullWidthStyled = styled(Flex)`
  min-width: 0;
  width: 100%;
`;

export const FlexFullHeightStyled = styled(Flex)`
  height: 100%;
`;

export const SpaceFullWidthStyled = styled(Space)`
  min-width: 0;
  width: 100%;
`;

export const SpaceFullHeightStyled = styled(Space)`
  height: 100%;
`;
export const ButtonLinkStyled = styled(Button)`
  padding: 0;
  display: flex;
  justify-content: start;
  .ant-typography {
    display: inline-block;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    -o-text-overflow: ellipsis;
    max-width: 100%;
  }
`;

export const BadgeStyled = styled(Badge)`
  && .ant-badge-dot {
    background: ${({ theme }) => theme.colors.badge};
  }
`;
