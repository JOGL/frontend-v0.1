import styled from '@emotion/styled';
import { Typography } from 'antd';

export const TypographyStyled = styled(Typography.Text)`
  margin-right: ${({ theme }) => theme.token.paddingXS}px;
`;
