import { EditOutlined, PlusOutlined } from '@ant-design/icons';
import { Tag, Typography, Button, Modal, Select, Form, Tooltip, Flex, Grid } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { FC, useState } from 'react';
import { TypographyStyled } from './keyWordsModal.styles';
const { useBreakpoint } = Grid;

interface KeyWordsProps {
  canEdit: boolean;
  keywords: string[];
  saveChanges: (data: string[]) => void;
}
const FORM_ID = 'keywords-form';

const KeyWordsModal: FC<KeyWordsProps> = ({ canEdit, keywords, saveChanges }) => {
  const { t } = useTranslation('common');
  const [showModal, setShowModal] = useState(false);
  const [form] = Form.useForm();
  const screens = useBreakpoint();
  const isMobile = screens.xs;

  const handleOk = ({ keywords }) => {
    setShowModal(false);
    saveChanges(keywords);
  };

  const handleCancel = () => {
    setShowModal(false);
    form.resetFields();
  };

  const moreKeywords = keywords.slice(3);

  return (
    <>
      <Flex align="center">
        {!isMobile && <TypographyStyled type="secondary">{t('keywords')}:</TypographyStyled>}
        {keywords?.slice(0, 3)?.map((keyWord, index) => {
          return <Tag key={index + keyWord}> {keyWord}</Tag>;
        })}

        {!!moreKeywords.length && (
          <Tooltip placement="top" title={moreKeywords.join(', ')}>
            <Tag> +{moreKeywords.length}</Tag>
          </Tooltip>
        )}
        {canEdit && (
          <Button
            icon={!keywords?.length ? <PlusOutlined /> : <EditOutlined />}
            size="small"
            onClick={() => setShowModal(true)}
          />
        )}
      </Flex>
      <Modal
        title={t('keyword.other')}
        open={showModal}
        onCancel={handleCancel}
        okButtonProps={{ htmlType: 'submit', form: FORM_ID }}
      >
        <Form layout="vertical" form={form} onFinish={handleOk} initialValues={{ keywords: keywords }} id={FORM_ID}>
          <Form.Item name="keywords">
            <Select style={{ width: '100%' }} dropdownStyle={{ display: 'none' }} mode="tags" />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
export default KeyWordsModal;
