import { useMutation } from '@tanstack/react-query';
import { useAuth } from '~/auth/auth';
import api from '~/src/utils/api/api';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';

interface Openable {
  id: string;
  is_new: boolean;
}

export const useFeedEntity = (): [(entity: Openable) => void] => {
  const {isLoggedIn} = useAuth()
  const markFeed = useMutation({
    mutationKey: [MUTATION_KEYS.feedOpened],
    mutationFn: async (feedId: string) => {
      const response = await api.feed.openedCreate(feedId);
      return { status: response.status, data: response.data, feedId };
    },
  });

  const markFeedAsOpened = (entity: Openable): void => {
    if(!isLoggedIn)
      return;
    
    markFeed.mutate(entity.id);
    entity.is_new = false;
  };

  return [markFeedAsOpened];
};
