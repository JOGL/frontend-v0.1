import styled from '@emotion/styled';
import { Button, Flex } from 'antd';

export const CardViewWrapperStyled = styled.div`
  margin: ${({ theme }) => `${theme.token.paddingXXS}px -${theme.token.paddingXXS}px`};
`;

export const FlexStyled = styled(Flex)<{ new?: boolean }>`
  height: 110px;
  padding: ${({ theme }) => theme.token.paddingSM}px;
  border-top: 1px solid ${({ theme }) => theme.token.colorBorder};
  background-color: ${(props) => (props.new ? props.theme.colors.new : 'none')};
`;

export const ButtonLinkStyled = styled(Button)`
  padding: 0;
  display: flex;
  justify-content: start;
  max-width: 90px;
  .ant-typography {
    display: inline-block;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    -o-text-overflow: ellipsis;
    max-width: 100%;
    color: ${({ theme }) => theme.token.colorLink};
  }
`;

export const AdditionalInfoStyled = styled(Flex)`
  span {
    font-size: 10px;
    white-space: nowrap;
  }
`;

export const FooterStyled = styled(Flex)`
  svg {
    font-size: 16px;
  }
  span {
    font-size: 12px;
  }
`;

export const NewStyled = styled(Flex)`
  background-color: ${({ theme }) => theme.token.colorPrimary};
  border-radius: ${({ theme }) => theme.token.borderRadiusOuter}px;
  span {
    padding: 0px ${({ theme }) => theme.token.paddingXXS}px;
    color: #fff;
  }
`;

export const TagWrapperStyled = styled.div`
  min-width: 90px;
`;
export const UserLinkWrapperStyled = styled(Flex)`
  .ant-typography {
    max-width: 80px !important;
  }
`;
