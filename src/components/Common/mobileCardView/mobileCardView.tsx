import { Empty, Flex, Tag, Typography } from 'antd';
import { DocumentModel, DocumentOrFolderModel, NeedModel, PaperModel, DocumentType } from '~/__generated__/types';
import {
  AdditionalInfoStyled,
  ButtonLinkStyled,
  CardViewWrapperStyled,
  FlexStyled,
  FooterStyled,
  NewStyled,
  TagWrapperStyled,
  UserLinkWrapperStyled,
} from './mobileCardView.styles';
import useTranslation from 'next-translate/useTranslation';
import Loader from '../loader/loader';
import { FrontPath } from '../../Layout/menuItems/menuItems';
import { MessageOutlined } from '@ant-design/icons';
import { stripHtml } from 'string-strip-html';
import { FlexFullWidthStyled } from '../common.styles';
import { UserLink } from '../links/userLink/userLink';

interface Props {
  data: DocumentModel[] | DocumentOrFolderModel[] | NeedModel[] | PaperModel[];
  loading: boolean;
  onClickRow: (document: DocumentModel | NeedModel | PaperModel) => void;
}

const MobileCardView = ({ data, loading = false, onClickRow }: Props) => {
  const { t } = useTranslation('common');
  if (loading === true) return <Loader />;

  return (
    <CardViewWrapperStyled>
      {!data.length && (
        <FlexFullWidthStyled align="center" justify="center">
          <Empty />
        </FlexFullWidthStyled>
      )}
      {data.map((item) => {
        const hasMentionsOrThreadActivity = !!(
          item.feed_stats.new_mention_count || item.feed_stats.new_thread_activity_count
        );
        const hasNewPosts = !!item.feed_stats.new_post_count;
        const entity = item.entity ?? item.feed_entity;
        const title = stripHtml(item.title).result;
        const type = item.type === DocumentType.Jogldoc ? 'page' : item.type;
        return (
          <FlexStyled key={item.id} new={item.is_new} onClick={() => onClickRow(item)} vertical gap="small">
            <Typography.Text ellipsis={{ tooltip: title }}>{title}</Typography.Text>
            <AdditionalInfoStyled align="center">
              <TagWrapperStyled>
                <Tag>{type}</Tag>
              </TagWrapperStyled>

              <Flex gap={4} justify="center" align="center">
                <Typography.Text>{t('addedTo')}</Typography.Text>
                <ButtonLinkStyled size="small" type="link" href={`/${FrontPath.CommunityEntity}/${entity?.id}`}>
                  <Typography.Text ellipsis={{ tooltip: entity?.title }}>{entity?.title}</Typography.Text>
                </ButtonLinkStyled>
                <Typography.Text>{t('by')}</Typography.Text>
                <UserLinkWrapperStyled>
                  <UserLink user={item.created_by} />
                </UserLinkWrapperStyled>
              </Flex>
            </AdditionalInfoStyled>

            <FooterStyled gap="small" align="center">
              <MessageOutlined />
              <Typography.Text>{item.feed_stats.post_count}</Typography.Text>
              {hasNewPosts && (
                <NewStyled>
                  <Typography.Text>{t('nNewPost', { count: item.feed_stats.new_post_count })}</Typography.Text>
                </NewStyled>
              )}

              {hasMentionsOrThreadActivity && (
                <NewStyled>
                  <Typography.Text>@</Typography.Text>
                </NewStyled>
              )}
            </FooterStyled>
          </FlexStyled>
        );
      })}
    </CardViewWrapperStyled>
  );
};

export default MobileCardView;
