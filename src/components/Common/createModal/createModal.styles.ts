import styled from '@emotion/styled';
import { Modal } from 'antd';

export const ModalStyled = styled(Modal)`
  .ant-modal-body {
    margin-top: ${({ theme }) => theme.token.paddingSM}px;
  }
`;
