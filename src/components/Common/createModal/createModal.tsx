import { useQueryClient } from '@tanstack/react-query';
import CreateNeedModal from '~/src/components/Need/createNeedModal/createNeedModal';
import CreatePaperModal from '~/src/components/Publications/createPaper/createPaperModal';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import useTranslation from 'next-translate/useTranslation';
import { EventCreationStep } from '~/src/types/event';
import { ModalStyled } from './createModal.styles';
import { CreateModalProps } from './createModal.types';
import { FrontPath } from '../../Layout/menuItems/menuItems';
import { Permission } from '~/__generated__/types';
import { CreateDocument } from '../../Tools/createDocument/createDocument';
import { CreateEventModal } from '../../Event/createEventModal';

const CreateModal = ({ modalType, handleClose, selectedSpace }: CreateModalProps) => {
  const queryClient = useQueryClient();
  const { t } = useTranslation('common');

  const permission = selectedSpace.user_access.permissions;
  return (
    <>
      {modalType === FrontPath.Need && (
        <CreateNeedModal
          closeModal={handleClose}
          selectedSpace={permission.includes(Permission.Postneed) ? selectedSpace : undefined}
        />
      )}
      {modalType === FrontPath.Paper && (
        <CreatePaperModal
          selectedSpace={permission.includes(Permission.Managelibrary) ? selectedSpace : undefined}
          refetch={() => {
            queryClient.invalidateQueries({
              queryKey: [QUERY_KEYS.papersAggregate],
            });
          }}
          handleCancel={handleClose}
        />
      )}
      {modalType === FrontPath.Event && (
        <ModalStyled
          width={800}
          open
          title={t('action.createItem', { item: t('event', { count: 1 }) })}
          footer={false}
          onCancel={handleClose}
        >
          <CreateEventModal
            defaultContainer={permission.includes(Permission.Createevents) ? selectedSpace : undefined}
            defaultCreationStep={EventCreationStep.PickPrivacy}
          />
        </ModalStyled>
      )}
      {modalType === FrontPath.Document && (
        <ModalStyled
          width={800}
          open
          title={t('action.createItem', { item: t('document', { count: 1 }) })}
          footer={false}
          onCancel={handleClose}
        >
          <CreateDocument
            closeModal={handleClose}
            defaultValue={permission.includes(Permission.Managedocuments) ? selectedSpace : undefined}
            refetch={() => {
              queryClient.invalidateQueries({
                queryKey: [QUERY_KEYS.documentsAggregate],
              });
              queryClient.invalidateQueries({
                queryKey: [QUERY_KEYS.entityDocumentList],
              });
            }}
          />
        </ModalStyled>
      )}
    </>
  );
};

export default CreateModal;
