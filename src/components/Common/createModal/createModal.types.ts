import { CommunityEntityChannelModel, NodeFeedDataModel } from '~/__generated__/types';
import { FrontPath } from '../../Layout/menuItems/menuItems';

export interface CreateModalProps {
  modalType: FrontPath;
  handleClose: () => void;
  selectedSpace: CommunityEntityChannelModel | NodeFeedDataModel;
}
