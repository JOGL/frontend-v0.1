import { SOCIAL_MEDIA } from './shareModal.utils';
export type SocialMedia = typeof SOCIAL_MEDIA[keyof typeof SOCIAL_MEDIA];
export type ObjectType = 'node' | 'workspace';

export interface ShareUrlParams {
  socialMedia: SocialMedia;
  objUrl: string;
  type: ObjectType;
}
