import styled from '@emotion/styled';
import { Button } from 'antd';
import Icon from '~/src/components/primitives/Icon';

export const SocialMediaButtonStyled = styled(Button)`
  all: unset;
  cursor: pointer;
  height: 22px;
`;

export const IconStyled = styled(Icon)`
  width: 12px;
  height: 20px;
`;
