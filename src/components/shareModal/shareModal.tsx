import { LinkOutlined, MailOutlined } from '@ant-design/icons';
import { Flex, Input, Modal, Tag, Typography } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { addressFront } from '~/src/utils/utils';
import { SocialMediaButtonStyled } from './shareModal.styles';
import Code from '~/src/components/code/code';
import useUser from '~/src/hooks/useUser';
import { useRouter } from 'next/router';
import { ObjectType, SocialMedia } from './shareModal.types';
import { generateEmbedCode, SOCIAL_MEDIA_LIST, getShareUrl } from './shareModal.utils';

interface Props {
  id: string;
  title: string;
  type: ObjectType;
  onClose: () => void;
}

const ShareModal = ({ id, title, type, onClose }: Props) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const objectUrlPath = `${window.location.origin}${router.asPath}`;
  const objUrl = addressFront + encodeURIComponent(router.asPath);

  const openWindow = (socialMedia: SocialMedia) => {
    const shareUrl = getShareUrl({ socialMedia, objUrl, type });
    window.open(shareUrl, 'share-dialog', 'width=626,height=436');
  };

  return (
    <Modal open title={title} footer={null} onCancel={onClose}>
      <Flex vertical gap="large">
        <Flex vertical gap="small" align="start">
          <Typography.Text strong>{t('url')}</Typography.Text>
          <Input
            disabled
            value={objectUrlPath}
            addonBefore={<LinkOutlined />}
            addonAfter={<Typography.Text copyable={{ text: objectUrlPath }} />}
          />
        </Flex>
        <Flex vertical gap="small" align="start">
          <Typography.Text strong>{t('socialMedia')}</Typography.Text>
          <Flex wrap gap="small" align="center">
            {SOCIAL_MEDIA_LIST.map((media) => {
              return (
                <SocialMediaButtonStyled key={media.key} onClick={() => openWindow(media.key)}>
                  <Tag color={media.color}>
                    <Flex gap="small">
                      {media.icon} {media.key}
                    </Flex>
                  </Tag>
                </SocialMediaButtonStyled>
              );
            })}
            <SocialMediaButtonStyled
              type="link"
              href={`mailto:?subject=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform&body=${objUrl}`}
              target="_self"
            >
              <Tag>
                <Flex gap="small">
                  <MailOutlined />
                  {t('email')}
                </Flex>
              </Tag>
            </SocialMediaButtonStyled>
          </Flex>
        </Flex>
        <Flex vertical gap="small" align="start">
          <Typography.Text strong copyable={{ text: generateEmbedCode(id, type) }}>
            {t('action.embed')}
          </Typography.Text>
          <Code>{generateEmbedCode(id, type)}</Code>
        </Flex>
      </Flex>
    </Modal>
  );
};

export default ShareModal;
