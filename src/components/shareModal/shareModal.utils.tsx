import { FacebookOutlined, LinkedinOutlined, RedditOutlined, WhatsAppOutlined, XOutlined } from '@ant-design/icons';

import { entityItem } from '~/src/utils/utils';
import { IconStyled } from './shareModal.styles';
import { ShareUrlParams } from './shareModal.types';
export const generateEmbedCode = (objectId: string, objectType: 'node' | 'workspace') => {
  const link = `${window.location.origin}/embedded/${entityItem[objectType].back_path}?id=${objectId}`;
  return `<iframe src="${link}" height="450" width="305" frameborder="0" allowfullscreen="" title="JOGL ${objectType} ${objectId}"></iframe>`;
};

export const SOCIAL_MEDIA = {
  facebook: 'Facebook',
  twitter: 'Twitter',
  linkedin: 'LinkedIn',
  reddit: 'Reddit',
  whatsapp: 'WhatsApp',
  telegram: 'Telegram',
} as const;

export const SOCIAL_MEDIA_LIST = [
  {
    key: SOCIAL_MEDIA.facebook,
    icon: <FacebookOutlined />,
    color: '#1877f2',
  },
  {
    key: SOCIAL_MEDIA.twitter,
    icon: <XOutlined />,
    color: '#000000',
  },
  {
    key: SOCIAL_MEDIA.linkedin,
    icon: <LinkedinOutlined />,
    color: '#55acee',
  },
  {
    key: SOCIAL_MEDIA.reddit,
    icon: <RedditOutlined />,
    color: '#ff4500',
  },
  {
    key: SOCIAL_MEDIA.whatsapp,
    icon: <WhatsAppOutlined />,
    color: '#25d366',
  },
  {
    key: SOCIAL_MEDIA.telegram,
    icon: <IconStyled icon="ic:baseline-telegram" />,
    color: '#0088cc',
  },
];

export const getShareUrl = ({ socialMedia, objUrl, type }: ShareUrlParams) => {
  const objectType = type === 'node' ? 'hub' : type;
  const text = `Check%20out%20this%20${objectType}%20on%20the%20JOGL%20platform`;
  switch (socialMedia) {
    case SOCIAL_MEDIA.facebook:
      return `https://www.facebook.com/sharer/sharer.php?u=${objUrl}`;
    case SOCIAL_MEDIA.twitter:
      return `https://twitter.com/intent/tweet/?text=${text}:&hashtags=JOGL&url=${objUrl}`;
    case SOCIAL_MEDIA.linkedin:
      return `https://www.linkedin.com/shareArticle?mini=true&url=${objUrl}&title=${text}&amp;&source=${objUrl}`;
    case SOCIAL_MEDIA.reddit:
      return `https://reddit.com/submit/?url=${objUrl}&resubmit=true&amp;title=${text}:&amp;`;
    case SOCIAL_MEDIA.whatsapp:
      return `https://api.whatsapp.com/send?text=${text}: ${objUrl}&preview_url=true`;
    case SOCIAL_MEDIA.telegram:
      return `https://telegram.me/share/url?text=${text}:&amp;url=${objUrl}`;
  }
};
