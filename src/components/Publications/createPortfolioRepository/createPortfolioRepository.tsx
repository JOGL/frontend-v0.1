import { Modal, message } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { FC, useEffect, useMemo, useState } from 'react';
import UserOnboardingStep from '../../Onboarding/userOnboardingStep/userOnboardingStep';
import { FINISH, OnboardingData, OnboardingSteps } from '../../Onboarding/userOnboarding/userOnboarding.types';
import { useOnboardingStore } from '../../Onboarding/userOnboardingStore/userOnboardingStoreProvider';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import { useMutation } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { OnboardingStoreProvider } from '../../Onboarding/userOnboardingStore/userOnboardingStoreProvider';

interface CreatePortfolioRepositoryProps {
  handleCancel: () => void;
  refetch: () => void;
}

const CreatePortfolioRepositoryContent: FC<CreatePortfolioRepositoryProps> = ({ handleCancel, refetch }) => {
  const { t } = useTranslation('common');
  const [currentStepKey, setCurrentStepKey] = useState('code_connect');
  const data = useOnboardingStore((state) => state.data);
  const commitData = useOnboardingStore((state) => state.commit);
  const rollbackData = useOnboardingStore((state) => state.rollback);
  const resetData = useOnboardingStore((state) => state.resetData);

  useEffect(() => {
    resetData();
  }, [resetData]);

  const goTo = (next: string) => {
    setTimeout(() => {
      setCurrentStepKey(next);
    }, 200);
  };

  const storeOnboardingMutation = useMutation({
    mutationKey: [MUTATION_KEYS.userUnarchive],
    mutationFn: async (data: OnboardingData) => {
      const response = await api.users.onboardingCreate({
        paperIds: data.papers,
        repos: data.repos,
        education: data.educationRecords,
        experience: data.experienceRecords,
        github_access_token: data.gh_token,
        huggingface_access_token: data.hf_token,
      });
      return { status: response.status };
    },
    onSuccess: () => {
      message.success(t('repositoryWasAdded'));
      refetch();
      handleCancel();
    },
    onError: (error: Error) => {
      console.log('error', error);
      message.error(t('error.somethingWentWrong'));
    },
  });

  const currentStep = useMemo(() => {
    return OnboardingSteps[currentStepKey];
  }, [currentStepKey]);

  return (
    <Modal onCancel={handleCancel} open footer={null} width={800}>
      <UserOnboardingStep
        title={''}
        subtitle={t(currentStep.subtitle)}
        subtitleNotFound={currentStep.subtitleNotFound ? t(currentStep.subtitleNotFound) : undefined}
        showNotFound={data.noWorksFound}
        loading={data.isLoading}
        align={currentStepKey === 'code_connect' ? 'center' : 'start'}
        onBack={
          currentStep.back && currentStepKey !== 'code_connect'
            ? () => {
                rollbackData();
                const previousKey = currentStep.back(data);
                goTo(previousKey);
              }
            : undefined
        }
        onNext={
          currentStep.next
            ? () => {
                commitData();
                const nextKey = currentStep.next(data);
                if (nextKey === FINISH) {
                  storeOnboardingMutation.mutate(data);
                } else {
                  goTo(nextKey);
                }
              }
            : undefined
        }
        onSkip={
          currentStep.skip && currentStepKey !== 'code_connect'
            ? () => {
                rollbackData();
                const nextKey = currentStep.skip(data);
                if (nextKey === 'user_page') {
                  handleCancel();
                } else {
                  goTo(nextKey);
                }
              }
            : undefined
        }
      >
        {currentStep.render({ goTo: goTo })}
      </UserOnboardingStep>
    </Modal>
  );
};

const CreatePortfolioRepository: FC<CreatePortfolioRepositoryProps> = (props) => {
  return (
    <OnboardingStoreProvider>
      <CreatePortfolioRepositoryContent {...props} />
    </OnboardingStoreProvider>
  );
};

export default CreatePortfolioRepository;
