import { useQuery } from '@tanstack/react-query';
import { Empty } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { FC, useState } from 'react';
import useDebounce from '~/src/hooks/useDebounce';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import SearchResult from '../searchResult/searchResult';
import { SearchStyled } from '../createPaperModal.styles';

const SearchByDOI: FC<{ refetch: () => void }> = ({ refetch }) => {
  const { t } = useTranslation('common');
  const [search, setSearch] = useState<string>();
  const debouncedSearch = useDebounce(search, 300);

  const { data, isFetched } = useQuery({
    queryKey: [QUERY_KEYS.papersDoiList, debouncedSearch],
    queryFn: async () => {
      const response = await api.users.papersDoiList({ id: debouncedSearch });
      return response.data;
    },
  });

  return (
    <>
      <SearchStyled
        placeholder={t('action.searchHere')}
        onChange={(e) => {
          setSearch(e.target.value);
        }}
        onSearch={(value) => {
          setSearch(value);
        }}
      />
      {isFetched && debouncedSearch && !data && <Empty />}
      {data && <SearchResult refetch={refetch} data={[data]} />}
    </>
  );
};
export default SearchByDOI;
