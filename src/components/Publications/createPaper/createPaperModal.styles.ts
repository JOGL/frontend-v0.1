import styled from '@emotion/styled';
import { Input, Modal } from 'antd';

export const SearchStyled = styled(Input.Search)`
  margin-bottom: ${({ theme }) => theme.token.marginXL}px;
`;

export const ModalStyled = styled(Modal)`
  .ant-modal-content {
    max-height: 90vh;
  }
  .ant-modal-body {
    padding-top: ${({ theme }) => theme.token.paddingLG}px;
    padding-bottom: ${({ theme }) => theme.token.paddingLG}px;
  }
`;
