import { TabsProps, Tabs, Typography, Button } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { FC, useState } from 'react';
import Trans from 'next-translate/Trans';
import useUserData from '~/src/hooks/useUserData';
import { useRouter } from 'next/router';
import AddPaper from '../addPaper/addPaper';
import SearchByDOI from '../searchByDOI/searchByDOI';
import OrcidPaper from '~/src/components/Pages/user/userDetails/portfolio/orcidModal/orcidPaper';
import { ModalStyled } from '../createPaperModal.styles';

interface CreatePaperProps {
  handleCancel: () => void;
  refetch: () => void;
}

const CreatePortfolioPaper: FC<CreatePaperProps> = ({ handleCancel, refetch }) => {
  const { t } = useTranslation('common');
  const [activeTab, setActiveTab] = useState('paper');
  const { userData } = useUserData();
  const router = useRouter();
  const isUserPage = router.pathname.includes('/user/');

  const openLinkMyOrcidModal = () => {
    window.location.href = `${process.env.ORCID_URL}/oauth/authorize?client_id=${process.env.ORCID_CLIENT_ID}&response_type=code&scope=/read-limited&redirect_uri=${process.env.ORCID_REDIRECT_URL}`;
  };

  const items: TabsProps['items'] = [
    {
      key: 'paper',
      label: t('action.addItem', { item: t('paper.one') }),
      children: <AddPaper refetch={refetch} />,
    },
    {
      key: 'DOI',
      label: t('action.searchByDOI'),
      children: <SearchByDOI refetch={refetch} />,
    },
    ...(!isUserPage
      ? []
      : [
          {
            key: 'ORCID',
            label: t('action.importFromOrcid'),
            children: userData?.orcid_id ? (
              <OrcidPaper refetch={refetch} />
            ) : (
              <Button onClick={openLinkMyOrcidModal}>
                <img src={`/images/icons/links-orcid.png`} style={{ width: 24, height: 24 }} />
                {t('action.linkMyOrcid')}
              </Button>
            ),
          },
        ]),
  ];

  return (
    <ModalStyled onCancel={handleCancel} open footer={null} width={800}>
      <Tabs items={items} activeKey={activeTab} onChange={(tab) => setActiveTab(tab)} />
      {activeTab === 'paper' && (
        <Trans
          i18nKey="common:cantFindThePaperYouAreLookingForTryWithDoi"
          components={[
            <Typography.Text key="cantFindThePaperYouAreLookingForTryWithDoi-0" />,
            <Typography.Link key="cantFindThePaperYouAreLookingForTryWithDoi-1" onClick={() => setActiveTab('DOI')} />,
          ]}
        />
      )}
    </ModalStyled>
  );
};
export default CreatePortfolioPaper;
