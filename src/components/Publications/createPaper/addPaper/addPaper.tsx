import { useInfiniteQuery } from '@tanstack/react-query';
import { TabsProps, Flex, Tabs, Typography, Spin } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { FC, useState } from 'react';
import useDebounce from '~/src/hooks/useDebounce';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { ButtonStyled, ContainerStyled, PrimaryTextStyled } from './addPaper.styles';
import { LoadingOutlined } from '@ant-design/icons';
import SearchResult from '../searchResult/searchResult';

import { SearchStyled } from '../createPaperModal.styles';
import { ScrollbarFullWidthStyled } from '~/src/assets/styles/Global.styled';

const PAGE_SIZE = 20;

const AddPaper: FC<{ refetch: () => void; selectedSpaceId?: string }> = ({ refetch, selectedSpaceId }) => {
  const { t } = useTranslation('common');
  const [search, setSearch] = useState<string>();
  const debouncedSearch = useDebounce(search, 300);

  const {
    data: semanticList,
    isFetching: isSmFetching,
    hasNextPage: semanticListNext,
    isFetchingNextPage: isFetchingSmNext,
    fetchNextPage: fetchNextSmPage,
  } = useInfiniteQuery({
    queryKey: [QUERY_KEYS.papersS2PaperList, { search: debouncedSearch, PageSize: PAGE_SIZE }],
    queryFn: async ({ pageParam = 1 }) => {
      const response = await api.users.papersS2PaperList({
        Search: debouncedSearch,
        PageSize: PAGE_SIZE,
        Page: pageParam,
        SortAscending: false,
      });
      return response.data;
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItemsCount = allPages.reduce((acc, page) => acc + page.items.length, 0);
      return loadedItemsCount < lastPage.total ? allPages.length + 1 : undefined;
    },
    enabled: !!debouncedSearch && debouncedSearch.length > 0,
    refetchOnMount: false,
  });

  const {
    data: openAlexList,
    isFetching: isOaFetching,
    hasNextPage: openAlexListNext,
    isFetchingNextPage: isFetchingOaNext,
    fetchNextPage: fetchNextOaPage,
  } = useInfiniteQuery({
    queryKey: [QUERY_KEYS.openAlexList, { search: debouncedSearch, PageSize: PAGE_SIZE }],
    queryFn: async ({ pageParam = 1 }) => {
      const response = await api.users.papersOaList({
        Search: debouncedSearch,
        PageSize: PAGE_SIZE,
        Page: pageParam,
        SortAscending: false,
      });
      return response.data;
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItemsCount = allPages.reduce((acc, page) => acc + page.items.length, 0);
      return loadedItemsCount < lastPage.total ? allPages.length + 1 : undefined;
    },
    enabled: !!debouncedSearch && debouncedSearch.length > 0,
    refetchOnMount: false,
  });

  const {
    data: pubMedList,
    isFetching: isPmFetching,
    hasNextPage: pubMedListNext,
    isFetchingNextPage: isFetchingPmNext,
    fetchNextPage: fetchPmNextPage,
  } = useInfiniteQuery({
    queryKey: [QUERY_KEYS.papersPmList, { search: debouncedSearch, PageSize: PAGE_SIZE }],
    queryFn: async ({ pageParam = 1 }) => {
      const response = await api.users.papersPmList({
        Search: debouncedSearch,
        PageSize: PAGE_SIZE,
        Page: pageParam,
        SortAscending: false,
      });
      return response.data;
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItemsCount = allPages.reduce((acc, page) => acc + page.items.length, 0);
      return loadedItemsCount < lastPage.total ? allPages.length + 1 : undefined;
    },
    enabled: !!debouncedSearch && debouncedSearch.length > 0,
    refetchOnMount: false,
  });

  const hasNextPage = pubMedListNext || openAlexListNext || semanticListNext;
  const isFetchingNextPage = isFetchingPmNext || isFetchingOaNext || isFetchingSmNext;

  const fetchNextPage = () => {
    pubMedListNext && fetchPmNextPage();
    openAlexListNext && fetchNextOaPage();
    semanticListNext && fetchNextSmPage();
  };

  const semanticListData =
    semanticList?.pages.flatMap((page) => {
      return page.items;
    }) || [];
  const semanticListTotal = semanticList?.pages[0].total;

  const openAlexListData =
    openAlexList?.pages.flatMap((page) => {
      return page.items;
    }) || [];
  const openAlexListTotal = openAlexList?.pages[0].total;

  const pubMedListData =
    pubMedList?.pages.flatMap((page) => {
      return page.items;
    }) || [];
  const pubMedListTotal = pubMedList?.pages[0].total;

  const items: TabsProps['items'] = [
    {
      key: 'openAlex',
      label: (
        <Flex gap="small" align="center">
          <Typography.Text>{t('openAlex')}</Typography.Text>
          {isOaFetching && <Spin indicator={<LoadingOutlined spin />} size="small" />}
          {!isOaFetching && !!openAlexListTotal && (
            <PrimaryTextStyled>{openAlexListTotal > 99 ? '99+' : openAlexListTotal}</PrimaryTextStyled>
          )}
        </Flex>
      ),
      children: <SearchResult refetch={refetch} data={openAlexListData} selectedSpaceId={selectedSpaceId} />,
    },
    {
      key: 'semanticScholar',
      label: (
        <Flex gap="small" align="center">
          <Typography.Text>{t('semanticScholar')}</Typography.Text>
          {isSmFetching && <Spin indicator={<LoadingOutlined spin />} size="small" />}
          {!isSmFetching && !!semanticListTotal && (
            <PrimaryTextStyled>{semanticListTotal > 99 ? '99+' : semanticListTotal}</PrimaryTextStyled>
          )}
        </Flex>
      ),
      children: <SearchResult refetch={refetch} data={semanticListData} selectedSpaceId={selectedSpaceId} />,
    },
    {
      key: 'pubMed',
      label: (
        <Flex gap="small" align="center">
          <Typography.Text>{t('pubMed')}</Typography.Text>
          {isPmFetching && <Spin indicator={<LoadingOutlined spin />} size="small" />}
          {!isPmFetching && !!pubMedListTotal && (
            <PrimaryTextStyled>{pubMedListTotal > 99 ? '99+' : pubMedListTotal}</PrimaryTextStyled>
          )}
        </Flex>
      ),
      children: <SearchResult refetch={refetch} data={pubMedListData} selectedSpaceId={selectedSpaceId} />,
    },
  ];

  return (
    <ContainerStyled>
      <SearchStyled
        placeholder={t('action.searchHere')}
        onChange={(e) => {
          setSearch(e.target.value);
        }}
        onSearch={(value) => {
          setSearch(value);
        }}
      />
      {debouncedSearch && (
        <ScrollbarFullWidthStyled>
          <Tabs items={items} />
        </ScrollbarFullWidthStyled>
      )}

      {hasNextPage && (
        <Flex justify="center">
          <ButtonStyled loading={isFetchingNextPage} onClick={() => fetchNextPage()}>
            {t('action.load')}
          </ButtonStyled>
        </Flex>
      )}
    </ContainerStyled>
  );
};
export default AddPaper;
