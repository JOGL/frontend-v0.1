import styled from '@emotion/styled';
import { Typography, Button } from 'antd';

export const PrimaryTextStyled = styled(Typography.Text)`
  color: ${({ theme }) => theme.token.colorPrimary};
`;

export const ContainerStyled = styled.div`
  margin-bottom: ${({ theme }) => theme.token.marginMD}px;
  .ant-tabs-content {
    max-height: calc(90vh - 520px);
    overflow: auto;
  }
`;
export const ButtonStyled = styled(Button)`
  margin-top: ${({ theme }) => theme.token.marginMD}px;
`;
