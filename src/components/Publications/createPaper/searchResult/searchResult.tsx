import { Typography, Flex, Divider, Button } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import React, { FC, useState } from 'react';
import { PaperModelOrcid, PaperModelPM, PaperModelS2 } from '~/__generated__/types';
import { ExportOutlined, FileTextOutlined, PlusOutlined } from '@ant-design/icons';
import { AbstractStyled, FadeInTextStyled } from './searchResult.styles';
import dayjs from 'dayjs';
import Link from 'next/link';
import { usePaper } from './searchResult.hooks';

const { Text } = Typography;

const SearchResult: FC<{
  data: PaperModelS2[] | PaperModelPM[] | PaperModelOrcid[];
  refetch: () => void;
  selectedSpaceId?: string;
}> = ({ data, refetch, selectedSpaceId }) => {
  const { t } = useTranslation('common');
  const [expandedItems, setExpandedItems] = useState<Record<number, boolean>>({});

  const { addPaper, addingIndexLoader } = usePaper(refetch, selectedSpaceId);

  const toggleExpand = (index: number) => {
    setExpandedItems((prev) => ({
      ...prev,
      [index]: !prev[index],
    }));
  };

  return (
    <Flex vertical gap="small">
      {data.map((item, index) => (
        <React.Fragment key={index}>
          <Flex justify="space-between" gap="small">
            <Flex vertical gap="small">
              <Text strong>
                <Flex gap="small" align="baseline">
                  <FileTextOutlined />
                  <span dangerouslySetInnerHTML={{ __html: item.title }} />
                </Flex>
              </Text>
              {item.abstract && (
                <AbstractStyled>
                  <FadeInTextStyled>
                    <span
                      dangerouslySetInnerHTML={{
                        __html: expandedItems[index] ? item.abstract : `${item.abstract?.slice(0, 250)}...`,
                      }}
                    />
                  </FadeInTextStyled>
                  <Typography.Link onClick={() => toggleExpand(index)}>
                    {expandedItems[index] ? t('action.seeLess') : t('action.seeMore')}
                  </Typography.Link>
                </AbstractStyled>
              )}

              <Flex gap="small">
                {item?.publication_date && (
                  <Typography.Text type="secondary">
                    {item?.publication_date
                      ? `${t('publishedOn')}  ${dayjs(item.publication_date).format('MMM D, YYYY')}`
                      : ''}
                    {item.journal ? ` - ${item.journal}` : ''}
                    {item.authors ? ` - ${item.authors}` : ''}
                  </Typography.Text>
                )}
              </Flex>
              {item.external_id && (
                <Typography.Link>
                  <Link
                    href={item?.external_id_url ? item.external_id_url : `https://doi.org/${item?.external_id}`}
                    target="_blank"
                  >
                    {t('externalLink')} <ExportOutlined />
                  </Link>
                </Typography.Link>
              )}
            </Flex>
            <Flex>
              <Button
                type="primary"
                loading={!!addingIndexLoader.length && addingIndexLoader.indexOf(item.external_id) !== -1}
                icon={<PlusOutlined />}
                onClick={() => addPaper(item)}
                size="small"
              />
            </Flex>
          </Flex>
          <Divider />
        </React.Fragment>
      ))}
    </Flex>
  );
};

export default SearchResult;
