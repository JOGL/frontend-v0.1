import { useMutation } from '@tanstack/react-query';
import { message } from 'antd';
import { useRouter } from 'next/router';
import { useState } from 'react';
import { FeedEntityVisibility, PaperModelOrcid, PaperUpsertModel } from '~/__generated__/types';
import api from '~/src/utils/api/api';
import useTranslation from 'next-translate/useTranslation';
import { useStonePathStore } from '~/src/contexts/stonepath/StonePathStoreProvider';

const isEncoded = (uri: string) => {
  uri = uri || '';
  return uri !== decodeURIComponent(uri);
};

const fullyDecodeURI = (uri: string) => {
  while (isEncoded(uri)) {
    uri = decodeURIComponent(uri);
  }
  return uri;
};

const hasExternalUrl = (data: any): data is { external_url: string } => {
  return 'external_url' in data;
};

export const usePaper = (refetch: () => void, selectedSpaceId?: string) => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const { stonePath } = useStonePathStore();
  const [deletingIndexLoader, setDeletingIndexLoader] = useState<string[]>([]);
  const [addingIndexLoader, setAddingIndexLoader] = useState<string[]>([]);
  const entityId = selectedSpaceId ?? (router.query.id as string);

  const addPaperMutation = useMutation({
    mutationFn: async (data: PaperUpsertModel) => {
      const paperData = {
        ...data,
        default_visibility: FeedEntityVisibility.Comment,
        communityentity_visibility: stonePath?.map((item) => {
          return {
            visibility: FeedEntityVisibility.Comment,
            community_entity_id: item.id,
          };
        }),
      };
      data.external_id && setAddingIndexLoader([data.external_id]);
      const response = await api.papers.papersCreate(entityId, paperData);
      data.external_id && setAddingIndexLoader([]);
      return response.data;
    },
    onSuccess: () => {
      message.success(t('paperWasAdded'));

      refetch();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const addPaper = (data: PaperUpsertModel | PaperModelOrcid) => {
    if (!data.external_id && hasExternalUrl(data) && data.external_url) {
      data.external_id = fullyDecodeURI(data.external_url).split('doi.org/')[1].trim();
    }
    data.external_id && setAddingIndexLoader([...addingIndexLoader, data.external_id]);

    addPaperMutation.mutate(data as PaperUpsertModel);
  };

  return {
    addPaper,
    addingIndexLoader,
    deletingIndexLoader,
  };
};
