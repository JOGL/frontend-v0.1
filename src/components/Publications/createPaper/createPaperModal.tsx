import { TabsProps, Tabs, Typography, Flex } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { FC, useState } from 'react';
import Trans from 'next-translate/Trans';
import AddPaper from './addPaper/addPaper';
import SearchByDOI from './searchByDOI/searchByDOI';
import { ModalStyled } from './createPaperModal.styles';
import { CommunityEntityChannelModel, NodeFeedDataModel, Permission } from '~/__generated__/types';
import ContainerPicker from '../../Common/container/containerPicker/containerPicker';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import { Container } from '../../Common/container/containerPicker/containerPicker.types';
import { Disabled } from '../../Common/disabled/disabled';

interface CreatePaperProps {
  handleCancel: () => void;
  refetch: () => void;
  selectedSpace?: CommunityEntityChannelModel | NodeFeedDataModel;
}

const CreatePaperModal: FC<CreatePaperProps> = ({ handleCancel, refetch, selectedSpace: space }) => {
  const { t } = useTranslation('common');
  const [activeTab, setActiveTab] = useState('paper');
  const [selectedContainer, setSelectedContainer] = useState<Container | undefined>(space);
  const { selectedHub } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
  }));
  
  const items: TabsProps['items'] = [
    {
      key: 'paper',
      label: t('action.addItem', { item: t('paper.one') }),
      children: <AddPaper refetch={refetch} selectedSpaceId={selectedContainer?.id} />,
    },
    {
      key: 'DOI',
      label: t('action.searchByDOI'),
      children: <SearchByDOI refetch={refetch} />,
    },
  ];

  return (
    <ModalStyled
      title={t('action.addItem', { item: t('paper.one') })}
      onCancel={handleCancel}
      open
      footer={null}
      width={800}
    >
      <Flex vertical gap="large">
        <ContainerPicker
          permission={Permission.Managelibrary}
          hubId={selectedHub?.id}
          onChange={(container) => setSelectedContainer(container)}
          value={selectedContainer?.id}
        />
        <Disabled disabled={!selectedContainer}>
          <Tabs items={items} activeKey={activeTab} onChange={(tab) => setActiveTab(tab)} />
          <Flex gap="small">
            {activeTab === 'paper' && (
              <Trans
                i18nKey="common:cantFindThePaperYouAreLookingForTryWithDoi"
                components={[
                  <Typography.Text key="cantFindThePaperYouAreLookingForTryWithDoi-0" />,
                  <Typography.Link
                    key="cantFindThePaperYouAreLookingForTryWithDoi-1"
                    onClick={() => setActiveTab('DOI')}
                  />,
                ]}
              />
            )}
          </Flex>
        </Disabled>
      </Flex>
    </ModalStyled>
  );
};
export default CreatePaperModal;
