import { FC } from 'react';
import ObjectCard from '../Cards/ObjectCard';
import { Menu, MenuButton, MenuList, MenuItem } from '@reach/menu-button';
import SpinLoader from '../Tools/SpinLoader';
import { displayObjectDate, entityItem } from '~/src/utils/utils';
import { TextChip } from '~/src/types/chip';
import Link from 'next/link';
import H2 from '../primitives/H2';
import useTranslation from 'next-translate/useTranslation';

interface PublicationCardProps {
  item: any;
  isLoading?: boolean;
  publicationUrl: string;
  onDelete?: (id: string, index: number) => any;
  index?: number;
  showAggregate?: boolean;
  textChips?: TextChip[];
  showAggregateSource?: boolean;
  canManageLibrary?: boolean;
  cardFormat?: string;
  width?: string;
  displayType?: 'grid' | 'list';
}

const PublicationCard: FC<PublicationCardProps> = ({
  item,
  publicationUrl,
  isLoading,
  onDelete,
  index,
  showAggregate = false,
  textChips = [],
  showAggregateSource = false,
  canManageLibrary = false,
  cardFormat,
  width,
  displayType = 'grid',
}) => {
  const { t } = useTranslation('common');

  return displayType === 'grid' ? (
    <ObjectCard
      hrefNewTab
      imgUrl="/images/default/default-paper.png"
      href={publicationUrl}
      chip={
        !showAggregateSource
          ? { name: t('paper.one') }
          : {
              name: t('containerLibrary', {
                container: t(
                  entityItem[item.tags.filter((x) => x !== 'aggregated' && x !== 'authoredbyme')[0]]?.translationId
                ),
              }),
            }
      }
      textChips={textChips}
      aggregatedChip={showAggregate}
      cardFormat={cardFormat}
      width={width}
      tw="overflow-visible"
    >
      {canManageLibrary && (
        <>
          {isLoading ? (
            <div tw="absolute top-3 right-3 h-6 w-6">
              <SpinLoader />
            </div>
          ) : (
            <>
              {!showAggregate && (
                <div tw="absolute top-2 right-2">
                  <Menu>
                    <MenuButton tw="rounded-full bg-gray-200 bg-opacity-80 h-6 w-6 p-1 flex justify-center items-center font-size[.7rem] text-gray-700">
                      •••
                    </MenuButton>
                    <MenuList
                      portal={false}
                      className="post-manage-dropdown"
                      tw="absolute rounded-lg divide-y divide-gray-300 text-center text-sm"
                    >
                      <MenuItem tw="flex items-center  text-center" onSelect={() => onDelete(item.id, index)}>
                        {t('action.remove')}
                      </MenuItem>
                    </MenuList>
                  </Menu>
                </div>
              )}
            </>
          )}
        </>
      )}
      <Link href={publicationUrl}>
        <H2 tw="font-bold leading-5 line-clamp-3 text-[16px] text-black" title={item.title}>
          {item.title}
        </H2>
      </Link>
      {cardFormat !== 'inPosts' && (
        <>
          {item?.journal && (
            <div tw="flex flex-col mt-4">
              <span tw="font-normal text-xs leading-4">{t('journal')}:</span>
              <span tw="font-medium text-[14px] line-clamp-1">{item?.journal}</span>
            </div>
          )}
          <div tw="mt-4 flex flex-col">
            <span tw="font-normal text-xs leading-4">{t('author.other')}:</span>
            <span tw="font-medium text-[14px] line-clamp-2">{item.authors}</span>
          </div>
          <span tw="font-normal text-xs leading-4 text-right mt-2 mb-1 italic">
            {t('published')}: {displayObjectDate(item.publication_date, 'LL')}
          </span>
        </>
      )}
    </ObjectCard>
  ) : (
    <tr key={item.title}>
      <td tw="w-full py-4 pl-4 pr-3 text-sm font-medium text-gray-900 max-w-0 sm:w-auto sm:max-w-none sm:pl-0">
        <Link href={publicationUrl}>
          <H2 tw="font-medium leading-5 line-clamp-3 text-[15px] text-black" title={item.title}>
            {item.title}
          </H2>
        </Link>
        <dl tw="font-normal lg:hidden">
          <dt tw="sr-only md:hidden">{t('journal')}</dt>
          <dd tw="mt-1 text-gray-700 truncate md:hidden">
            <span tw="underline">{t('journal')}:</span> {item.journal || 'No journal'}
          </dd>
          <dt tw="sr-only">{t('author.other')}</dt>
          <dd tw="mt-1 text-gray-500 line-clamp-2">
            <span tw="underline">{t('author.other')}:</span> {item.authors}
          </dd>
        </dl>
      </td>
      <td tw="hidden px-3 py-4 text-sm text-gray-500 md:table-cell">{item.journal || 'No journal'}</td>
      <td tw="hidden px-3 py-4 text-sm text-gray-500 lg:table-cell">{item.authors}</td>
      <td tw="px-3 py-4 text-sm text-gray-500">{displayObjectDate(item.publication_date, 'LL')}</td>
      <td tw="py-4 pl-3 pr-4 text-sm font-medium text-right sm:pr-0">
        {canManageLibrary && !showAggregate && (
          <>
            {isLoading ? (
              <div tw="h-6 w-6">
                <SpinLoader />
              </div>
            ) : (
              <Menu>
                <MenuButton tw="rounded-full bg-gray-200 bg-opacity-80 h-6 w-6 p-1 flex justify-center items-center font-size[.7rem] text-gray-700">
                  •••
                </MenuButton>
                <MenuList
                  portal={false}
                  className="post-manage-dropdown"
                  tw="rounded-lg divide-y divide-gray-300 text-center text-sm"
                >
                  <MenuItem tw="flex items-center  text-center" onSelect={() => onDelete(item.id, index)}>
                    {t('action.remove')}
                  </MenuItem>
                </MenuList>
              </Menu>
            )}
          </>
        )}
      </td>
    </tr>
  );
};

export default PublicationCard;
