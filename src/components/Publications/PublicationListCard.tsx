import Link from 'next/link';
import React from 'react';
import useTranslation from 'next-translate/useTranslation';
import H2 from '~/src/components/primitives/H2';
import Title from '~/src/components/primitives/Title';
import { Publication, PublicationDisplayMode } from '~/src/types';
import Icon from '~/src/components/primitives/Icon';
import { displayObjectDate } from '~/src/utils/utils';
import tw from 'twin.macro';
import { useRouter } from 'next/router';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';

interface Props {
  publication: Publication;
  displayMode?: PublicationDisplayMode;
  onSelectedPublication?: (publication: Publication) => void;
}

const PublicationListCard = ({ publication, displayMode = 'default', onSelectedPublication }: Props) => {
  const { t } = useTranslation('common');

  const router = useRouter();
  const newActivityCount =
    publication?.feed_stats?.new_mention_count + publication?.feed_stats?.new_thread_activity_count;

  const handlePublicationClick = () => {
    if (displayMode === 'hub_discussions') {
      onSelectedPublication(publication);
    } else {
      router.push(`/paper/${publication?.id}`);
    }
  };

  const handlePublicationTitleClick = (e) => {
    e.stopPropagation();
  };

  return (
    <div
      tw="flex shadow-custom rounded-lg cursor-pointer h-[120px] md:(h-[100px])"
      onClick={handlePublicationClick}
      css={displayMode === 'default' && tw`self-center w-full md:w-3/4`}
    >
      <div tw="flex flex-col flex-auto border-r-2 p-2 gap-2 justify-around">
        <div tw="flex items-center gap-1">
          {/* Title */}
          <div tw="flex-auto" onClick={(e) => handlePublicationTitleClick(e)}>
            <Link href={`/paper/${publication.id}`} passHref legacyBehavior>
              <Title>
                <H2
                  tw="break-all text-[14px] items-center line-clamp-2 font-medium"
                  title={publication?.title}
                  css={publication?.feed_stats?.new_post_count > 0 && tw`font-bold`}
                >
                  {publication?.title}
                </H2>
              </Title>
            </Link>
          </div>

          <div
            css={
              publication?.is_new &&
              tw`flex-none [background:linear-gradient(253.99deg, #F54799 -2.61%, #7A21B5 48.72%, #02B3FF 107.65%),linear-gradient(0deg, #FFFFFF, #FFFFFF)] rounded-full border`
            }
          >
            {publication?.is_new && (
              <div tw="bg-white rounded-full m-[1px]">
                <span tw="flex text-[10px] px-2 [background:linear-gradient(253.99deg, #F54799 -2.61%, #7A21B5 48.72%, #02B3FF 107.65%),linear-gradient(0deg, #FFFFFF, #FFFFFF)] bg-clip-text! text-transparent">
                  {t('new')}
                </span>
              </div>
            )}
          </div>
        </div>

        {/* Authors */}
        {publication?.authors && (
          <div tw="flex flex-none items-center text-xs gap-1 w-full">
            <span tw="text-[#5F5D5D] capitalize">{t('by')}:</span>
            <span title={publication.authors} tw="line-clamp-1">
              {publication.authors}
            </span>
          </div>
        )}

        <div tw="flex flex-wrap gap-2 items-center w-full md:(flex-nowrap gap-1)" onClick={(e) => e.stopPropagation()}>
          {/* Journal */}
          {/* Always render the div even if the Journal is null to keep formatting consistent */}
          <div tw="flex flex-auto items-center text-xs line-clamp-1 gap-1">
            {publication?.journal && (
              <>
                <span tw="text-[#5F5D5D]">{t('journal')}:</span>
                <span title={publication?.journal} tw="break-all line-clamp-1">
                  {publication?.journal}
                </span>
              </>
            )}
          </div>

          {/* Published date */}
          {publication?.publication_date && (
            <div tw="flex items-center text-xs gap-1 md:flex-none">
              <span tw="text-[#5F5D5D]">{t('published')}:</span>
              <span tw="text-[#8B8B8B] break-all line-clamp-1">
                {displayObjectDate(publication.publication_date, 'll', true)}
              </span>
            </div>
          )}
        </div>
      </div>

      <div tw="flex-none w-[60px] text-primary font-semibold text-xs" onClick={(e) => e.stopPropagation()}>
        <div
          tw="flex gap-2 h-full w-full items-center justify-center hover:(bg-[#EBEBEB])"
          onClick={handlePublicationClick}
        >
          {newActivityCount > 0 ? (
            <div tw="flex-none w-[15px] h-[15px] [background:linear-gradient(253.99deg, #F54799 -2.61%, #7A21B5 48.72%, #02B3FF 107.65%), linear-gradient(0deg, #FFFFFF, #FFFFFF)] self-center rounded-full text-[8px] text-white text-center content-center font-semibold">
              {newActivityCount}
            </div>
          ) : (
            <span>{publication?.feed_stats?.post_count > 0 ? publication?.feed_stats?.post_count : ''}</span>
          )}
          <Icon
            icon="fluent:chat-28-filled"
            tw="w-4 h-4"
            css={publication?.feed_stats?.post_count === 0 && tw`text-[#868686]`}
          />
        </div>
      </div>
    </div>
  );
};

export default PublicationListCard;
