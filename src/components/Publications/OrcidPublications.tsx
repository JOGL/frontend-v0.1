import Button from '~/src/components/primitives/Button';
import { useApi } from '~/src/contexts/apiContext';
import useTranslation from 'next-translate/useTranslation';
import { useEffect, useState } from 'react';
import SpinLoader from '../Tools/SpinLoader';
import Icon from '../primitives/Icon';
import { confAlert } from '~/src/utils/utils';
import Trans from 'next-translate/Trans';

const OrcidPublications = ({ user, fetchUserPapers, userDOIs, callback }) => {
  const api = useApi();
  const { t } = useTranslation('common');
  const [loading, setLoading] = useState(false);
  const [addingPubs, setAddingPubs] = useState(false);
  const [orcidList, setOrcidList] = useState([]);
  const [addingOrDeletingIndex, setAddingOrDeletingIndex] = useState(undefined);
  const fetchOrcidPapers = async () => {
    try {
      setLoading(true);
      const { data } = await api.get('/users/papers/orcid');
      setOrcidList(data);
    } catch (err) {
      console.warn(err);
    }
    setLoading(false);
  };

  useEffect(() => {
    setAddingOrDeletingIndex(undefined);
  }, [userDOIs]);

  useEffect(() => {
    fetchOrcidPapers();
  }, []);

  const removePaper = async (id) => {
    try {
      await api.delete(`/users/papers/${id}`);
      fetchUserPapers();
      confAlert.fire({ icon: 'success', title: 'Deleted paper from your profile' });
    } catch (err) {
      console.warn(err);
      confAlert.fire({ icon: 'error', title: 'Error while deleting paper from your profile' });
    }
  };

  const addPublications = async () => {
    try {
      setAddingPubs(true);
      const promises = [];
      for (const paper of orcidList) {
        if (!userDOIs[paper.external_id]) {
          promises.push(api.post('/users/papers', { ...paper, user_ids: [user?.id] }));
        }
      }

      if (promises.length) {
        await Promise.all(promises);
        confAlert.fire({ icon: 'success', title: 'All papers added successfully to your profile' });
        fetchUserPapers();
      } else {
        confAlert.fire({ icon: 'info', title: 'All papers already added to your profile' });
      }
      callback();
    } catch (err) {
      console.warn(err);
      confAlert.fire({ icon: 'error', title: 'Error while adding papers to your profile' });
    }
    setAddingPubs(false);
  };

  const addPublication = async (paper) => {
    try {
      await api.post('/users/papers', { ...paper, user_ids: [user?.id] });
      fetchUserPapers();
      confAlert.fire({ icon: 'success', title: 'Paper added successfully to your profile' });
    } catch (err) {
      console.warn(err);
      confAlert.fire({ icon: 'error', title: 'Error while adding paper to your profile' });
    }
  };

  return (
    <div tw="flex flex-col">
      <div tw="mt-5 flex justify-between flex-wrap gap-2">
        <div tw="flex flex-col">
          <span tw="text-black text-base font-medium leading-4">{t('myOrcidId')}</span>
          <div tw="flex mt-3 items-center">
            <img src={`/images/icons/links-orcid.png`} width="28px" />
            <span tw="ml-2">{user.orcid_id}</span>
          </div>
        </div>
        {!loading && orcidList.length > 0 && (
          <div>
            {addingPubs ? (
              <div tw="mt-4 flex justify-center items-center">
                <SpinLoader />
              </div>
            ) : (
              <Button tw="mt-1 mr-4 rounded-md font-medium" onClick={addPublications}>
                {t('action.addAllPapersToMyProfile')}
              </Button>
            )}
          </div>
        )}
      </div>
      {!loading ? (
        <>
          <div tw="my-14 text-center">
            <span tw="text-black text-center text-base italic font-normal leading-7">
              <Trans
                i18nKey="common:weFoundItemsOnYourOrcid"
                components={[<span key="common:weFoundItemsOnYourOrcid" tw="font-bold" />]}
                values={{
                  item: `${orcidList?.length ?? 0} ${t('paper', { count: orcidList?.length ?? 0 })}`,
                }}
              />
            </span>
          </div>
          <div>
            {orcidList?.map((ele, index) => (
              <>
                <div tw="flex flex-col">
                  <div tw="flex items-center">
                    <span tw="text-black  text-xl font-bold leading-normal">{ele.title}</span>
                  </div>
                  <div tw="flex my-2 items-center">
                    <span tw="text-black text-base font-semibold leading-normal">{t('type')}</span>
                    <div tw="ml-2.5 capitalize text-xs font-medium text-gray-600 rounded bg-gray-200 bg-opacity-80 px-1 box-shadow[0px 4px 15px rgb(0 0 0 / 9%)]">
                      {ele.type}
                    </div>
                  </div>
                  <div tw="flex justify-between">
                    <div tw="flex items-center">
                      <img src={`/images/icons/links-doi.png`} width="28px" />
                      <span tw="ml-2.5">{ele.external_id}</span>
                    </div>
                    <div>
                      {ele.on_jogl && (
                        <div tw="flex py-1 px-2 justify-center items-center bg-yellow-400 bg-opacity-60 rounded">
                          <span tw="text-yellow-800 text-xs italic font-bold leading-6">{t('onJogl')}</span>
                        </div>
                      )}
                      {userDOIs[ele.external_id] ? (
                        <div
                          tw="cursor-pointer"
                          onClick={() => {
                            setAddingOrDeletingIndex(index);
                            removePaper(userDOIs[ele.external_id]);
                          }}
                        >
                          {index === addingOrDeletingIndex ? <SpinLoader /> : <Icon icon="mdi:bin-circle" />}
                        </div>
                      ) : (
                        <>
                          {index === addingOrDeletingIndex ? (
                            <SpinLoader />
                          ) : (
                            <Icon
                              icon="icon-park-solid:add"
                              tw="cursor-pointer"
                              onClick={() => {
                                setAddingOrDeletingIndex(index);
                                addPublication(ele);
                              }}
                            />
                          )}
                        </>
                      )}
                    </div>
                  </div>
                </div>
                <div tw="my-3 bg-gray-300 bg-opacity-50 h-0.5" />
              </>
            ))}
          </div>
        </>
      ) : (
        <div tw="mt-4 flex justify-center items-center">
          <SpinLoader />
        </div>
      )}
    </div>
  );
};

export default OrcidPublications;
