import Icon from '../primitives/Icon';
import { TabPanels, TabPanel as SpecialTabPanel, Tabs } from '@reach/tabs';
import { FC, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Tab } from '@reach/tabs';
import '@reach/tabs/styles.css';
import tw from 'twin.macro';
import Button from '~/src/components/primitives/Button';
import { useApi } from '~/src/contexts/apiContext';
import SpinLoader from '../Tools/SpinLoader';
import useSemantiScholarSearch from '~/src/hooks/useSemanticScholarSearch';
import usePubMedOpenAlexSearch from '~/src/hooks/usePubMedOpenAlexSearch';
import { Publication } from '~/src/types';
import Link from 'next/link';
import OrcidPublications from './OrcidPublications';
import Trans from 'next-translate/Trans';
import useBreakpoint from '~/src/hooks/useBreakpoint';

const NavTab = tw(Tab)`border-b-2 border-indigo-600 hover:border-gray-500 px-4 font-bold`;

const getBreaker = () => <div tw="w-full border-b-2 border-gray-200 mt-3 mb-4" />;

const DateFormatOptions: Intl.DateTimeFormatOptions = {
  year: 'numeric',
  month: 'long',
  day: 'numeric',
};

const RenderPublication = ({ article, userDOIs, addPaper, removePaper, index, addingOrDeletingIndex }) => {
  const alreadyAdded = userDOIs[article.external_id];
  return (
    <>
      <div tw="flex justify-between">
        <div tw="flex flex-col w-[90%] gap-4" css={alreadyAdded && tw`text-[#8B8B8B]`}>
          <div tw="flex justify-between items-start">
            <Icon icon="ri:article-line" tw="mr-2" />
            <span tw="w-full block font-bold text-base leading-6 line-clamp-2">{article.title}</span>
          </div>
          {article.abstract && (
            <div tw="flex flex-wrap">
              <div tw="block font-normal text-sm leading-5 ">
                <span tw="line-clamp-3">{article.abstract}&nbsp;</span>
                <span
                  tw="text-primary font-bold cursor-pointer"
                  onClick={(e) => {
                    e.preventDefault();
                    const link =
                      article.open_access_pdf ?? article.external_url
                        ? article.external_url
                        : `https://doi.org/${article.external_id}`;
                    window && window.open(link, '_blank');
                  }}
                >
                  Show more +
                </span>
              </div>
            </div>
          )}
          <div tw="line-clamp-2 font-normal text-sm text-gray-500">
            {article?.publication_date && (
              <>Published on {new Date(article?.publication_date).toLocaleDateString('en-US', DateFormatOptions)} -</>
            )}{' '}
            {article?.journal && <>{article?.journal} - </>}
            <>{article.authors}</>
          </div>

          <div tw="flex gap-2">
            <Link
              href={article?.external_id_url ? article.external_id_url : `https://doi.org/${article?.external_id}`}
              target="_blank"
              style={{ textDecoration: 'none' }}
            >
              <Button btnType="secondary" tw="flex items-center font-bold text-primary text-sm ">
                External link&nbsp;
                <Icon icon="iconoir:open-new-window" tw="hover:text-black h-4! w-4!" />
              </Button>
            </Link>
            {article.jogl_id && (
              <Link href={`/paper/${article.jogl_id}`} target={'_blank'} style={{ textDecoration: 'none' }}>
                <Button tw="flex items-center bg-[#CFC5EB] border-none text-primary font-bold text-sm hover:bg-[#b7a8e3]">
                  Open on&nbsp;
                  <img src="/images/logo_single.svg" alt="JOGL" tw="h-4! w-4!" />
                </Button>
              </Link>
            )}
          </div>
        </div>

        <div tw="flex flex-col ml-4 justify-center items-center gap-4 w-[10%]">
          {!alreadyAdded ? (
            <>
              {index === addingOrDeletingIndex ? (
                <div tw="cursor-pointer">
                  <SpinLoader />
                </div>
              ) : (
                <div tw="cursor-pointer">
                  <Icon
                    icon="icon-park-solid:add"
                    tw="cursor-pointer text-primary hocus:text-[#4a3292]"
                    onClick={() => addPaper(article, index)}
                  />
                </div>
              )}
            </>
          ) : (
            <>
              {index === addingOrDeletingIndex ? (
                <div tw="cursor-pointer">
                  <SpinLoader />
                </div>
              ) : (
                <>
                  <span tw="text-xs font-medium text-gray-600 rounded bg-white bg-opacity-80 px-2 py-1 box-shadow[0px 4px 15px rgb(0 0 0 / 9%)] first-letter:capitalize">
                    In library
                  </span>
                  <div
                    tw="cursor-pointer"
                    onClick={() => {
                      removePaper(userDOIs[article.external_id], index);
                    }}
                  >
                    <Icon icon="mdi:bin-circle" />
                  </div>
                </>
              )}
            </>
          )}
        </div>
      </div>
      {getBreaker()}
    </>
  );
};

type RenderTabProps = {
  title: string;
  total: number;
  showTotal: boolean;
  loading: boolean;
};

const RenderTabs = ({ title, total, showTotal, loading }: RenderTabProps) => {
  return (
    <div tw="flex gap-2 items-center">
      <span>{title}</span>
      {loading && <SpinLoader customCSS={tw`text-[#5E3EBF] text-xs w-8 h-8`} />}
      {!loading && showTotal && (
        <div tw="flex justify-center items-center rounded-full bg-[#CFC5EB] w-8 h-8">
          <span tw="text-primary text-xs">{total > 99 ? `99+` : `${total}`}</span>
        </div>
      )}
    </div>
  );
};

type ResultTabProps = {
  loadingMore: boolean;
  showLoadMoreButton: boolean;
  results: Publication[];
  addingOrDeletingIndex: any;
  userDOIs: any;
  addPaper: (data: any, index: any) => Promise<void>;
  removePaper: (id: any, index: any) => Promise<void>;
  loadMore: () => Promise<void>;
};

const RenderResultsTabs = ({
  loadingMore,
  showLoadMoreButton,
  results,
  addingOrDeletingIndex,
  userDOIs,
  addPaper,
  removePaper,
  loadMore,
}: ResultTabProps) => {
  return (
    <SpecialTabPanel>
      {results
        .filter((e) => e.external_id)
        .map((e, index) => (
          <RenderPublication
            addingOrDeletingIndex={addingOrDeletingIndex}
            index={index}
            article={e}
            key={`${index}-${e.external_id}`}
            userDOIs={userDOIs}
            removePaper={removePaper}
            addPaper={addPaper}
          />
        ))}
      <div tw="flex justify-center items-center w-full">
        {loadingMore ? (
          <SpinLoader />
        ) : (
          <>
            {showLoadMoreButton && (
              <Button tw="ml-4 rounded-lg font-medium" onClick={loadMore}>
                Load more
              </Button>
            )}
          </>
        )}
      </div>
    </SpecialTabPanel>
  );
};

type SearchPublicationsProps = {
  removePublication: any;
  addPublication: any;
  userDOIs: Object;
  callback: any;
  user?: any;
  hasLinkedOrcid?: boolean;
  fromOnboarding?: boolean;
  fetchPapers?: any;
  closeModal?: any;
};

const SearchPublications: FC<SearchPublicationsProps> = ({
  removePublication,
  addPublication,
  userDOIs,
  callback,
  user,
  hasLinkedOrcid = false,
  fromOnboarding = false,
  closeModal,
  fetchPapers,
}) => {
  const [loadingDOI, setLoadingDOI] = useState(false);
  const [tab, setTab] = useState(0);
  const [resultSourceTab, setResultSourceTab] = useState<number>(0);
  const [searchText, setSearchState] = useState('');
  const [doi, setDOI] = useState('');
  const [doiList, setDOIList] = useState([]);
  const [addingOrDeletingIndex, setAddingOrDeletingIndex] = useState(undefined);
  const { t } = useTranslation('common');
  const api = useApi();
  const screenType = useBreakpoint();
  const {
    loading: loadingSemantic,
    loadingMore: loadingMoreSemantic,
    showLoadMoreBtn: semanticShowLoadMoreBtn,
    results: semanticList,
    total: semanticTotal,
    loadMore: loadMoreSemantic,
  } = useSemantiScholarSearch(searchText);
  const {
    loading: loadingOpenAlex,
    loadingMore: loadingMoreOpenAlex,
    showLoadMoreBtn: openAlexShowLoadMoreBtn,
    results: openAlexList,
    total: openAlexTotal,
    loadMore: loadMoreOpenAlex,
  } = usePubMedOpenAlexSearch('oa', searchText);
  const {
    loading: loadingPubMed,
    loadingMore: loadingMorePubMed,
    showLoadMoreBtn: pubMedShowLoadMoreBtn,
    results: pubMedList,
    total: pubMedTotal,
    loadMore: loadMorePubMed,
  } = usePubMedOpenAlexSearch('pm', searchText);

  useEffect(() => {
    setAddingOrDeletingIndex(undefined);
  }, [userDOIs]);

  function isEncoded(uri) {
    uri = uri || '';
    return uri !== decodeURIComponent(uri);
  }

  function fullyDecodeURI(uri) {
    while (isEncoded(uri)) {
      uri = decodeURIComponent(uri);
    }
    return uri;
  }

  const fetchDOIData = async () => {
    try {
      let doiNum = doi;

      if (doi.includes('doi.org')) {
        doiNum = fullyDecodeURI(doi).split('doi.org/')[1].trim();
      }

      setLoadingDOI(true);
      const { data } = await api.get('/users/papers/doi', { params: { id: doiNum } });

      if (data) {
        setDOIList([data]);
      }
    } catch (e) {
      console.warn('Err While Searching With API:: ', e);
    }
    setLoadingDOI(false);
  };

  const addPaper = async (data, index) => {
    if (!data.external_id && data?.external_url) {
      data.external_id = fullyDecodeURI(data?.external_url).split('doi.org/')[1].trim();
    }

    setAddingOrDeletingIndex(index);
    const res = await addPublication(data);

    if (res && res.error) {
      setAddingOrDeletingIndex(undefined);
    }
  };

  const removePaper = async (id, index) => {
    setAddingOrDeletingIndex(index);
    const res = await removePublication(id);

    if (res && res.error) {
      setAddingOrDeletingIndex(undefined);
    }
  };

  const getTabStyle = (index) => ({ borderBottom: `3px solid ${tab === index ? '#5E3EBF' : 'white'}` });

  const getResultsTabStyle = (index) => ({
    borderBottom: `3px solid ${resultSourceTab === index ? '#5E3EBF' : 'white'}`,
  });

  return (
    <div tw="flex w-full" css={fromOnboarding && tw`pt-2`}>
      <Tabs style={{ width: '100%' }} defaultIndex={0} index={tab} onChange={(id) => setTab(id)}>
        <div tw="flex justify-between w-full border-b-2 border-gray-200 mb-6 p-0">
          <div>
            {hasLinkedOrcid && <NavTab style={getTabStyle(0)}>{t('fromItem', { item: 'ORCID' })}</NavTab>}
            <NavTab style={getTabStyle(hasLinkedOrcid ? 1 : 0)}>{t('action.addItem', { item: t('paper.one') })}</NavTab>
            <NavTab style={getTabStyle(hasLinkedOrcid ? 2 : 1)}>{t('action.searchByDOI')}</NavTab>
          </div>
          {!fromOnboarding && (
            <button tw="flex flex-col pt-1" onClick={closeModal}>
              <Icon icon="material-symbols:close" tw="text-gray-400" />
            </button>
          )}
        </div>

        <TabPanels>
          {hasLinkedOrcid && (
            <SpecialTabPanel>
              <OrcidPublications user={user} fetchUserPapers={fetchPapers} userDOIs={userDOIs} callback={callback} />
            </SpecialTabPanel>
          )}

          <SpecialTabPanel>
            <div tw="mb-8" css={screenType === 'mobile' && tw`mb-16`}>
              <input
                type="search"
                placeholder={t('action.searchPapers')}
                autoComplete="off"
                autoCorrect="off"
                autoCapitalize="off"
                spellCheck="false"
                required={true}
                maxLength={512}
                onChange={(e) => {
                  e.preventDefault();
                  setSearchState(e.target.value);
                }}
                className="ais-SearchBox-input"
                tw="rounded-full h-12 border border-gray-300 bg-white px-6"
              />

              <Tabs
                style={{ width: '100%' }}
                defaultIndex={0}
                index={resultSourceTab}
                onChange={(id) => setResultSourceTab(id)}
              >
                <div tw="flex flex-wrap w-full border-b-2 border-gray-200 mt-3 mb-3 p-0">
                  <NavTab style={getResultsTabStyle(0)}>
                    <RenderTabs
                      title="OpenAlex"
                      total={openAlexTotal}
                      showTotal={!loadingOpenAlex && openAlexList?.length > 0}
                      loading={loadingOpenAlex}
                    />
                  </NavTab>
                  <NavTab style={getResultsTabStyle(1)}>
                    <RenderTabs
                      title="SemanticScholar"
                      total={semanticTotal}
                      showTotal={!loadingSemantic && semanticList?.length > 0}
                      loading={loadingSemantic}
                    />
                  </NavTab>
                  <NavTab style={getResultsTabStyle(2)}>
                    <RenderTabs
                      title="PubMed"
                      total={pubMedTotal}
                      showTotal={!loadingPubMed && pubMedList?.length > 0}
                      loading={loadingPubMed}
                    />
                  </NavTab>
                </div>

                <TabPanels>
                  <RenderResultsTabs
                    loadingMore={loadingMoreOpenAlex}
                    showLoadMoreButton={openAlexShowLoadMoreBtn}
                    results={openAlexList}
                    addingOrDeletingIndex={addingOrDeletingIndex}
                    userDOIs={userDOIs}
                    addPaper={addPaper}
                    removePaper={removePaper}
                    loadMore={loadMoreOpenAlex}
                  />

                  <RenderResultsTabs
                    loadingMore={loadingMoreSemantic}
                    showLoadMoreButton={semanticShowLoadMoreBtn}
                    results={semanticList}
                    addingOrDeletingIndex={addingOrDeletingIndex}
                    userDOIs={userDOIs}
                    addPaper={addPaper}
                    removePaper={removePaper}
                    loadMore={loadMoreSemantic}
                  />

                  <RenderResultsTabs
                    loadingMore={loadingMorePubMed}
                    showLoadMoreButton={pubMedShowLoadMoreBtn}
                    results={pubMedList}
                    addingOrDeletingIndex={addingOrDeletingIndex}
                    userDOIs={userDOIs}
                    addPaper={addPaper}
                    removePaper={removePaper}
                    loadMore={loadMorePubMed}
                  />
                </TabPanels>
              </Tabs>
            </div>

            <div tw="fixed w-[calc(100% - 2rem)] bottom-0 bg-white p-3">
              <Icon icon="octicon:question-16" tw="w-4 h-4" />
              <Trans
                i18nKey="common:cantFindThePaperYouAreLookingForTryWithDoi"
                components={[
                  <span key="cantFindThePaperYouAreLookingForTryWithDoi-0" tw="ml-2 text-base font-normal" />,
                  <span
                    key="cantFindThePaperYouAreLookingForTryWithDoi-1"
                    tw="text-base font-normal underline cursor-pointer"
                    onClick={() => setTab(2)}
                  />,
                ]}
              />
            </div>
          </SpecialTabPanel>
          <SpecialTabPanel>
            <div tw="flex">
              <input
                type="search"
                placeholder={t('enterDoiIdOrFullUrl')}
                autoComplete="off"
                autoCorrect="off"
                autoCapitalize="off"
                spellCheck="false"
                required={true}
                maxLength={512}
                value={doi}
                onChange={(e) => {
                  e.preventDefault();
                  setDOI(e.target.value);
                }}
                className="ais-SearchBox-input"
                tw="rounded-lg w-[350px] h-12 border border-gray-300 bg-white px-6"
              />
              {!loadingDOI ? (
                <Button tw="ml-4 rounded-lg font-medium" onClick={fetchDOIData}>
                  {t('action.search')}
                </Button>
              ) : (
                <div tw="ml-4 flex items-center">
                  <SpinLoader />
                </div>
              )}
            </div>
            <div tw="mt-4 flex justify-between mb-1">
              <span tw="text-base text-gray-500 font-bold">{t('nResults', { count: doiList.length })}</span>
            </div>
            {getBreaker()}
            {doiList.map((e, index) => (
              <RenderPublication
                addingOrDeletingIndex={addingOrDeletingIndex}
                index={index}
                userDOIs={userDOIs}
                key={e.external_id}
                article={e}
                removePaper={removePaper}
                addPaper={addPaper}
              />
            ))}
          </SpecialTabPanel>
        </TabPanels>
      </Tabs>
    </div>
  );
};

export default SearchPublications;
