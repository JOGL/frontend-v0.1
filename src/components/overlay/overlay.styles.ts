import styled from '@emotion/styled';

export const OverlayWrapperStyled = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 5001;
  height: 100%;
  padding: ${({ theme }) => theme.space[8]};
  background-color: ${({ theme }) => theme.token.colorBgSpotlight};
  color: ${({ theme }) => theme.token.colorWhite};
  @media screen and (max-width: ${({ theme }) => theme.token.screenLG}px) {
    padding: ${({ theme }) => theme.token.paddingXXS}px;
  }
`;
