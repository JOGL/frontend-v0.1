import { createPortal } from 'react-dom';
import { OverlayWrapperStyled } from './overlay.styles';
import { MouseEvent, ReactNode, useEffect, useRef } from 'react';
import { LeftOutlined } from '@ant-design/icons';
import { Button } from 'antd';

export const OverlayCloseButton = ({ onClose }: { onClose: () => void }) => {
  return (
    <Button ghost onClick={onClose}>
      <LeftOutlined />
    </Button>
  );
};
const createOverlayContainer = () => {
  const container = document.createElement('div');
  container.setAttribute('id', `overlay-container-${Date.now()}`);
  return container;
};

interface Props {
  children: ReactNode;
  disableCloseOnClickOutside?: boolean;
  onClose?: () => void;
}

const Overlay = ({ children, disableCloseOnClickOutside = false, onClose }: Props) => {
  const portalRef = useRef<HTMLDivElement>(document && createOverlayContainer()).current;
  const onClickOverlay = (e: MouseEvent) => {
    if (e.target === e.currentTarget && !disableCloseOnClickOutside) {
      onClose && onClose();
    }
  };

  useEffect(() => {
    document.body.appendChild(portalRef);
    return () => {
      document.body.removeChild(portalRef);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return createPortal(<OverlayWrapperStyled onMouseDown={onClickOverlay}>{children}</OverlayWrapperStyled>, portalRef);
};

export default Overlay;
