import React, { useEffect, ReactElement } from 'react';
import { notification } from 'antd';
import { BellOutlined } from '@ant-design/icons';
import { useRouter } from 'next/router';

const PushNotification = () => {
  const [api, contextHolder] = notification.useNotification();
  const { router } = useRouter();

  useEffect(() => {
    const openNotification = (icon: ReactElement, message: string, description: string, url: string) => {
      api.info({
        message: message,
        description: description,
        placement: 'bottomRight',
        duration: 4.5,
        icon: icon,
        onClick: () => router.push(url),
      });
    };

    // Listen for messages from service worker
    navigator.serviceWorker.addEventListener('message', (event) => {
      if (event.data.type === 'PUSH_RECEIVED') {
        const { notification } = event.data;
        openNotification(<BellOutlined />, notification.title, notification.body, notification.URL);
      }
    });
  });

  return <div>{contextHolder}</div>;
};

export default PushNotification;
