import tw from 'twin.macro';
import { TabList, Tab, TabPanel } from '@reach/tabs';
import '@reach/tabs/styles.css';
import styled from '~/src/utils/styled';

// For several index&edit pages Tabs

export const TabListStyle = styled(TabList)`
  ${tw`bg-white text-gray-500 font-sans font-semibold text-lg justify-center flex-wrap my-6 border-0 border-b-2 border-solid border-gray-200 sticky`}
  top: 4.1rem;
  z-index: 9;
`;

export const TabListStyleNoSticky = styled(TabList)`
  ${tw`bg-white text-gray-500 font-sans font-semibold text-lg justify-center flex-wrap my-6 border-0 border-b-2 border-solid border-gray-200`}
  top: 4.1rem;
  z-index: 9;
`;

export const TabListStyleNoBorder = styled(TabList)`
  ${tw`bg-white text-gray-500 font-sans font-semibold text-lg justify-center flex-wrap my-6 border-0 border-b-2 border-solid border-gray-200`}
  top: 4.1rem;
  z-index: 9;
  border: none;
  justify-content: flex-start;
  gap: 15px;
`;

export const NavTab = tw(Tab)`border-b-2 hover:border-gray-500 px-4`;

export const ChipTab = styled(Tab)`
  border: 1px solid rgba(209, 209, 209, 0.5) !important;
  border-radius: 12px;
  color: #5e5c5c;
  font-size: 17px;
  font-weight: 400;
  tab-size: 0;
  padding: 0px 12px;
  background: ${(props) => (props['isSelected'] ? 'rgba(209, 209, 209, 0.5);' : '')};
`;

export const TabPanelFeed = styled(TabPanel)`
  .feed {
    margin: auto;
  }
`;

export const TabPanelAbout = styled(TabPanel)`
  .interests {
    @media (min-width: 900px) {
      grid-template-columns: repeat(auto-fill, minmax(110px, 1fr)) !important;
    }
  }
`;

export const TabPanelResp = tw(TabPanel)`px-4`;

// For UserMenu component Tabs

export const TabListUser = tw(
  TabList
)`mt-0 mb-4 bg-white text-lg text-gray-600 font-sans font-medium justify-start border-b border-t-0 border-r-0 border-l-0 border-solid border-gray-200 sticky -top-4 z-10`;

export const TabUser = tw(
  Tab
)`text-lg text-gray-600 font-sans font-medium border-0 mb-0 min-w-max hover:border-gray-500 border-b-2`;

// For Home page Tabs
// TODO: find these specific styles
// export const TabListHome = tw(
//   TabList
// )`mt-0 mb-4 bg-white text-lg text-gray-600 font-sans font-medium justify-start border-b border-t-0 border-r-0 border-l-0 border-solid border-gray-200 sticky -top-4 z-10`;

// export const TabHome = tw(
//   Tab
// )`text-lg text-gray-600 font-sans font-medium border-0 border-b mb-0 min-w-max hover:border-blue-600 border-b-2`;
