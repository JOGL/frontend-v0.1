import { CSSProperties, FC, useState } from 'react';
import Icon from '../../../primitives/Icon';
import Button from '../../../primitives/Button';
import { confAlert } from '~/src/utils/utils';
import { useApi } from '~/src/contexts/apiContext';
import AsyncSelect from 'react-select/async';
import { components, ControlProps } from 'react-select';
import styled from '~/src/utils/styled';
import useTranslation from 'next-translate/useTranslation';
import { WorkspaceDetailModel } from '~/__generated__/types';

interface WorkspaceRequestOrInviteProps {
  itemId: string;
  itemType: string;
  alreadyAttachedWorkspaces: WorkspaceDetailModel[];
  callBack: () => void;
}

const WorkspaceRequestOrInvite: FC<WorkspaceRequestOrInviteProps> = ({
  itemId,
  itemType,
  alreadyAttachedWorkspaces,
  callBack,
}) => {
  const api = useApi();
  const { t } = useTranslation('common');
  const [workspacesList, setWorkspacesList] = useState([]);
  const [sending, setSending] = useState(false);
  const action = t('action.invite');

  const searchWorkspace = async (resolve: (value: unknown) => void, value: string) => {
    try {
      let content = (await api.get(`/workspaces?Search=${value}&Page=1&PageSize=5`))?.data?.items ?? [];
      content = content
        // filter workspaces that were already added
        .filter((x) => !alreadyAttachedWorkspaces?.map((x) => x.id).includes(x.id))
        .map((workspace) => {
          return {
            value: workspace.id,
            label: workspace.title,
            banner_url: workspace.banner_url_sm || '/images/default/default-workspace.png',
          };
        });
      resolve(content);
    } catch (e) {
      resolve([]);
    }
  };

  const loadOptions = (inputValue) =>
    new Promise((resolve) => {
      searchWorkspace(resolve, inputValue);
    });

  const inviteWorkspace = async (workspaceId: string) => {
    await api.post(`/${itemType}/${itemId}/communityEntityInvite/${workspaceId}`);
  };

  const handleChangeWorkspace = (content) => {
    const tempWorkspacesList = [];
    content?.map(function (workspace) {
      workspace && tempWorkspacesList.push(workspace.value);
    });
    setWorkspacesList(tempWorkspacesList);
  };

  const handleSubmit = () => {
    const sentInvitation = [];
    setSending(true);
    for (const item of workspacesList) {
      sentInvitation.push(inviteWorkspace(item));
    }
    Promise.all(sentInvitation)
      .then(() => {
        confAlert.fire({ icon: 'success', title: `${action} sent` });
        callBack();
      })
      .catch((err) => {
        confAlert.fire({
          icon: 'error',
          title:
            err.response.status === 409
              ? t('thisItemIsPendingAdminApproval', { item: t('workspace.one') })
              : t('someUnknownErrorOccurred'),
        });
        console.warn(err);
      })
      .finally(() => {
        setSending(false);
      });
  };

  const formatOptionLabel = ({ label, banner_url }) => (
    <WorkspaceLabel tw="inline-flex items-center">
      <img src={banner_url} />
      <div>{label}</div>
    </WorkspaceLabel>
  );

  const customStyles: Record<string, (css: CSSProperties) => CSSProperties> = {
    container: (provided) => ({
      ...provided,
      width: '100%',
    }),
    control: (css) => ({
      ...css,
      position: 'relative',
      borderRadius: '4px',
      minHeight: '10px',
    }),
    valueContainer: (css) => ({
      ...css,
    }),
    multiValue: (css) => ({
      ...css,
      '> div': {
        display: 'flex',
        alignItems: 'center',
      },
    }),
  };

  const Control = ({ children, ...props }: ControlProps<any, false>) => {
    return (
      <components.Control {...props}>
        <div tw="bg-[#D0D0D0] p-1 rounded-tl-sm rounded-bl-sm items-center flex justify-center h-[38px] w-[38px]">
          <Icon icon="mdi:at" tw="h-5 w-5 text-[#5959599e] mr-0" />
        </div>
        {children}
      </components.Control>
    );
  };

  return (
    <div>
      <div tw="mb-4">
        <label tw="font-bold" htmlFor="joglUser">
          {t('joglContainer', { container: t('workspace.other') })}
        </label>
        <AsyncSelect
          isMulti
          cacheOptions
          styles={customStyles}
          onChange={handleChangeWorkspace}
          formatOptionLabel={formatOptionLabel}
          placeholder={t('action.search')}
          loadOptions={loadOptions}
          noOptionsMessage={() => null}
          components={{ DropdownIndicator: null, Control, IndicatorSeparator: null }}
          isClearable
          autoFocus
        />
      </div>
      <Button onClick={handleSubmit} disabled={workspacesList.length === 0 || sending} className="invite-btn">
        {action}
      </Button>
    </div>
  );
};

const WorkspaceLabel = styled.div`
  div {
    font-size: 15px;
    color: #5c5d5d;
  }
  img {
    width: 28px;
    height: 28px;
    border-radius: 50%;
    object-fit: cover;
    margin-right: 6px;
    border: 1px solid lightgray;
  }
`;

export default WorkspaceRequestOrInvite;
