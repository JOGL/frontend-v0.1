import { FC } from 'react';
import { Menu, MenuItem, MenuButton, MenuList } from '@reach/menu-button';
import Icon from '../../../primitives/Icon';
import useTranslation from 'next-translate/useTranslation';

interface MoreActionsProps {
  onClick?: () => void;
}

export const MoreActions: FC<MoreActionsProps> = ({ onClick }) => {
  const { t } = useTranslation('common');

  return (
    <Menu>
      <MenuButton tw="font-size[.8rem] height[fit-content] text-gray-700 px-1 hover:bg-gray-300 rounded-sm">
        •••
      </MenuButton>
      <MenuList className="post-manage-dropdown">
        <MenuItem onSelect={onClick}>
          <Icon icon="ic:round-delete" />
          {t('action.remove')}
        </MenuItem>
      </MenuList>
    </Menu>
  );
};
