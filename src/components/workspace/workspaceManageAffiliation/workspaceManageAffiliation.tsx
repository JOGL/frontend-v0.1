import { FunctionComponent, useEffect, useState } from 'react';
import { TabPanels, Tabs, TabPanel } from '@reach/tabs';
import { NavTab, TabListStyleNoSticky } from '~/src/components/Tabs/TabsStyles';
import styles from './workspaceManageAffiliation.module.css';
import SpinLoader from '../../Tools/SpinLoader';
import { confAlert } from '~/src/utils/utils';
import Button from '../../primitives/Button';
import useTranslation from 'next-translate/useTranslation';
import FilterTable from './filterTable/filterTable';
import { useModal } from '~/src/contexts/modalContext';
import { MoreActions } from './moreActions/moreActions';
import { useApi } from '~/src/contexts/apiContext';
import Loading from '../../Tools/Loading';
import { Hub, ItemType, Organization } from '~/src/types';
import useGet from '~/src/hooks/useGet';
import A from '../../primitives/A';
import FormDropdownComponent from '../../Tools/Forms/FormDropdownComponent';
import ObjectAdminCard from '../../Tools/ObjectAdminCard';
import NoResults from '../../Tools/NoResults';
import WorkspaceRequestOrInvite from './workspaceRequestOrInvite/WorkspaceRequestOrInvite';

const WorkspacesColumns = {
  workspace: '',
  status: '',
  content_privacy_custom_settings: '',
  joining_restriction_custom_settings: '',
  menu: '',
};

const pendingWorkspaceColumns = {
  workspace: '',
};

const options = {
  content_privacy_custom_settings: ['Yes', 'No'],
  joining_restriction_custom_settings: ['invite', 'request', 'open'],
};

interface WorkspacesProps {
  object: Hub | Organization;
  objectType: ItemType;
}

interface WorkspaceAdminCardProps {
  data: {
    title: string;
    id: string;
    stats: number;
    logo_url?: string;
    banner_url?: string;
  };
}

const WorkspaceAdminCard: FunctionComponent<WorkspaceAdminCardProps> = ({
  data: { title, id, stats, logo_url, banner_url },
}) => {
  const { t } = useTranslation('common');

  return (
    <div tw="inline-flex space-x-3">
      <div
        style={{ backgroundImage: `url(${logo_url || banner_url || '/images/default/default-workspace.png'})` }}
        tw="bg-cover bg-center h-[50px] w-[50px] rounded-full border border-solid border-[#ced4da]"
      />
      <div>
        <A href={`/workspace/${id}`}>{title}</A>
        <div>
          <span>
            {stats?.members_count ?? 0}&nbsp;{t('member', { count: stats?.members_count ?? 0 })}
          </span>
        </div>
      </div>
    </div>
  );
};

const WorkspaceManageAffiliation: FunctionComponent<WorkspacesProps> = ({ object, objectType }) => {
  const [objectInfo, setObjectInfo] = useState({
    content_privacy_custom_settings: object.content_privacy_custom_settings || [],
    joining_restriction_custom_settings: object.joining_restriction_custom_settings || [],
  });

  const [isFetching, setIsFetching] = useState(false);

  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const api = useApi();

  const { data: workspacesData, mutate: workspacesRevalidate } = useGet(`/${objectType}/${object.id}/workspaces`);
  const { data: incomingWorkspaceInvites, mutate: incomingWorkspaceRevalidate } = useGet(
    `/${objectType}/${object.id}/communityEntityInvites/incoming`
  );
  const { data: outgoingWorkspaceInvitesProps, mutate: outgoingWorkspaceRevalidate } = useGet(
    `/${objectType}/${object.id}/communityEntityInvites/outgoing`
  );

  // Format data for tables
  const workspaces = workspacesData?.map((item) => ({
    workspace: item,
    status: item.type,
    content_privacy_custom_settings: objectInfo?.content_privacy_custom_settings[item.id] === true ? 'Yes' : 'No',
    joining_restriction_custom_settings: objectInfo?.joining_restriction_custom_settings[item.id],
  }));
  const outgoingWorkspaceInvites = outgoingWorkspaceInvitesProps?.map((item) => ({
    workspace: item,
  }));

  const fetchObjectDetails = async () => {
    let { content_privacy_custom_settings, joining_restriction_custom_settings } = object;
    content_privacy_custom_settings = content_privacy_custom_settings?.reduce((a, b) => {
      return { ...a, [b.communityEntityId]: b.allowed };
    }, {});

    joining_restriction_custom_settings = joining_restriction_custom_settings?.reduce((a, b) => {
      return { ...a, [b.communityEntityId]: b.level };
    }, {});

    setObjectInfo({
      content_privacy_custom_settings,
      joining_restriction_custom_settings,
    });
  };

  useEffect(() => {
    fetchObjectDetails();
  }, []);

  const cancelInvite = async (itemAId = object.id, communityInviteId: string) => {
    try {
      await api.post(`/${objectType}/${object.id}/communityEntityInvites/${communityInviteId}/cancel`);
      confAlert.fire({ icon: 'success', title: 'Cancelled request' });
      outgoingWorkspaceRevalidate();
    } catch (e) {
      confAlert.fire({ icon: 'error', title: t('someUnknownErrorOccurred') });
    }
  };

  const removeWorkspace = (workspaceId) => {
    api
      .post(`/${objectType}/${object.id}/communityEntity/${workspaceId}/remove`)
      .then(() => {
        confAlert.fire({ icon: 'success', title: t('removedLinkToWorkspace') });
        workspacesRevalidate();
      })
      .catch((err) => confAlert.fire({ icon: 'error', title: `Error: ${err})` }));
  };

  const WorkspaceRequestOrInviteButton = () => {
    const title = t('inviteWorkspace');
    return (
      <Button
        onClick={() => {
          showModal({
            children: (
              <WorkspaceRequestOrInvite
                alreadyAttachedWorkspaces={workspacesData}
                itemType={objectType}
                itemId={object.id}
                callBack={() => {
                  closeModal();
                  outgoingWorkspaceRevalidate();
                }}
              />
            ),
            title: title,
          });
        }}
      >
        {title}
      </Button>
    );
  };

  return (
    <div className={styles.workspacesContainer}>
      <p>{t('inThisTabYouCanManageTheWorkspaces')}.</p>
      <Tabs defaultIndex={0}>
        <TabListStyleNoSticky>
          <NavTab>{t('affiliatedWorkspaces')}</NavTab>
          <NavTab>
            <div tw="inline-flex items-center justify-center">
              {t('outgoingRequests')}
              <span tw="bg-[#E2E2E2] rounded-full w-7 h-7 ml-2">
                <span tw="flex justify-center">
                  {outgoingWorkspaceInvitesProps?.filter((x) => x.type === 'workspace').length}
                </span>
              </span>
            </div>
          </NavTab>

          <NavTab>
            <div tw="inline-flex items-center justify-center">
              {t('incomingRequests')}
              <span tw="bg-[#E2E2E2] rounded-full w-7 h-7 ml-2">
                <span tw="flex justify-center">
                  {incomingWorkspaceInvites?.filter((x) => x.type === 'workspace').length}
                </span>
              </span>
            </div>
          </NavTab>
        </TabListStyleNoSticky>
        <TabPanels tw="justify-center">
          {/* Workspaces */}
          <TabPanel>
            <div className={`${styles.tabContainer} ${styles.rowContainer}`}>
              <WorkspaceRequestOrInviteButton />
              <div className="loader">
                {isFetching && <SpinLoader size="40" />}
                {workspaces?.length === 0 && !isFetching && <span>{t('noItems', { item: t('workspace.one') })}</span>}
              </div>
              <div className="row-gap-15">
                {!isFetching && workspaces?.length > 0 && (
                  <FilterTable
                    columns={WorkspacesColumns}
                    rows={workspaces}
                    tbodyRender={(item, rowItem, col, row) => {
                      if (col === 'workspace') {
                        return <WorkspaceAdminCard data={item} />;
                      }
                      if (col === 'menu') {
                        return <MoreActions onClick={() => removeWorkspace(rowItem.workspace.id)} />;
                      }
                      if (col === 'content_privacy_custom_settings') {
                        return (
                          <FormDropdownComponent
                            id="type"
                            isDisabled={object.content_privacy !== 'custom'}
                            type="noTranslation"
                            title={t('contentVisibility')}
                            content={
                              objectInfo[col][rowItem.workspace.id] === true
                                ? 'Yes'
                                : objectInfo[col][rowItem.workspace.id] === false
                                ? 'No'
                                : 'No'
                            }
                            options={options[col]}
                            onChange={(key, content) => {
                              objectInfo[col][rowItem.workspace.id] =
                                content === 'Yes' ? true : content === 'No' && false;
                              setObjectInfo({ ...objectInfo });
                            }}
                          />
                        );
                      }
                      if (col === 'joining_restriction_custom_settings') {
                        return (
                          <FormDropdownComponent
                            isDisabled={object.joining_restriction !== 'custom'}
                            id="type"
                            type="noTranslation"
                            title={t('membershipAccess')}
                            content={objectInfo[col][rowItem.workspace.id] || 'invite'}
                            options={options[col]}
                            onChange={(key, content) => {
                              objectInfo[col][rowItem.workspace.id] = content;
                              setObjectInfo({ ...objectInfo });
                            }}
                          />
                        );
                      }
                    }}
                  />
                )}
              </div>
            </div>
          </TabPanel>
          {/* Outgoing requests */}
          <TabPanel>
            <div className={styles.tabContainer}>
              <WorkspaceRequestOrInviteButton />
              <div className="loader">
                {isFetching && <SpinLoader size="40" />}
                {outgoingWorkspaceInvitesProps?.filter((x) => x.type === 'workspace')?.length === 0 && !isFetching && (
                  <NoResults />
                )}
              </div>
              <div className="row-gap-15">
                {!isFetching && outgoingWorkspaceInvitesProps?.filter((x) => x.type === 'workspace')?.length > 0 && (
                  <FilterTable
                    columns={pendingWorkspaceColumns}
                    rows={outgoingWorkspaceInvites?.filter((x) => x.workspace.type === 'workspace')}
                    tbodyRender={(item, rowItem, col, row) => {
                      return (
                        <div tw="inline-flex w-full justify-between">
                          {col === 'workspace' && <WorkspaceAdminCard data={item} />}
                          <div className={styles.rowContainer}>
                            <div className="row-gap-15 justify-content-end">
                              <Button
                                tw="text-danger border-danger bg-white hocus:(text-white bg-danger)"
                                onClick={() => {
                                  cancelInvite(rowItem.workspace.id, rowItem.workspace.invitation_id);
                                }}
                              >
                                {t('action.cancelItem', { item: t('request') })}
                              </Button>
                            </div>
                          </div>
                        </div>
                      );
                    }}
                  />
                )}
              </div>
            </div>
          </TabPanel>
          {/* Incoming requests */}
          <TabPanel>
            <div className={styles.tabContainer}>
              {incomingWorkspaceInvites?.filter((x) => x.type === 'workspace').length === 0 && <NoResults />}
              {incomingWorkspaceInvites?.filter((x) => x.type === 'workspace') ? (
                <>
                  {incomingWorkspaceInvites
                    ?.filter((x) => x.type === 'workspace')
                    .map((workspace, i) => (
                      <ObjectAdminCard
                        key={i}
                        object={workspace}
                        objUrl={`/workspace/${workspace.id}`}
                        parentType={objectType}
                        parentId={object.id}
                        callBack={incomingWorkspaceRevalidate}
                        type="request"
                      />
                    ))}
                </>
              ) : (
                <Loading />
              )}
            </div>
          </TabPanel>
        </TabPanels>
      </Tabs>
    </div>
  );
};

export default WorkspaceManageAffiliation;
