import React, { FC, useState } from 'react';
import styles from './filterTable.module.css';

interface FilterTableProps {
  columns: Record<string, string>;
  rows: Partial<
    {
      [K in keyof FilterTableProps['columns']]: any;
    }
  >[];
  checkBox?: boolean;
  renderHeader?: () => JSX.Element;
  tbodyRender: (columnItem: any, rowItem: any, colName: string, row: number, col: number) => JSX.Element;
}

const FilterTable: FC<FilterTableProps> = ({ checkBox, renderHeader, tbodyRender, rows, columns }) => {
  const [selectedAll, setSelectedAll] = useState(Array(Object.keys(columns).length + 1).fill(false));

  return (
    <table className={styles.responsiveTable}>
      <thead>
        <tr>
          {checkBox && (
            <td colSpan={0} style={{ width: '0px', paddingLeft: '0px' }}>
              <input
                type="checkbox"
                checked={selectedAll[0]}
                onChange={(val) => {
                  setSelectedAll((c) => {
                    c[0] = !c[0];
                    return [...c];
                  });
                }}
                name="All"
              />
            </td>
          )}
          {!selectedAll[0] && Object.entries(columns).map((tuple, i) => <td key={tuple[0]}>{tuple[1]}</td>)}
          {selectedAll[0] && <td colSpan={Object.keys(columns).length}>{renderHeader && renderHeader()}</td>}
        </tr>
      </thead>
      <tbody>
        {rows.map((row, index) => (
          <tr key={index}>
            {checkBox && (
              <td colSpan={0} style={{ width: '0px', paddingLeft: '0px' }}>
                <input
                  type="checkbox"
                  checked={selectedAll[0] || selectedAll[index]}
                  onChange={(val) => {
                    setSelectedAll((c) => {
                      c[index] = !c[index];
                      return [...c];
                    });
                  }}
                  name={`${row}${index}`}
                />
              </td>
            )}
            {Object.entries(columns).map((tuple, i) => (
              <td key={tuple[0]} data-label={tuple[1]} colSpan={1}>
                {tbodyRender(row[tuple[0]], row, tuple[0], index, i)}
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default FilterTable;
