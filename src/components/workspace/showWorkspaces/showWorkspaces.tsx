import { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import {
  getContainerAccessLevelChip,
  getContainerJoiningRestrictionChip,
  getContainerVisibilityChip,
} from '~/src/utils/utils';
import Loading from '../../Tools/Loading';
import { useRouter } from 'next/router';
import api from '~/src/utils/api/api';
import { useQuery, keepPreviousData } from '@tanstack/react-query';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { SpaceTabHeader } from '../../Common/space/spaceTabHeader/spaceTabHeader';
import { Empty, Flex } from 'antd';
import { SpaceContentStyled } from '../../Common/space/space.styles';
import { Permission, WorkspaceDetailModel, WorkspaceModel } from '~/__generated__/types';
import useDebounce from '~/src/hooks/useDebounce';
import WorkspaceCard from '../workspaceCard/workspaceCard';

interface Props {
  workspace: WorkspaceDetailModel;
}

export const ShowWorkspaces: FC<Props> = ({ workspace }) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const [searchText, setSearchText] = useState('');
  const debouncedSearch = useDebounce(searchText, 300);

  const fetchWorkspaces = async () => {
    const response = await api.workspaces.workspacesDetail(workspace.id, { Search: debouncedSearch });
    return response.data;
  };

  const { data, isLoading, isPlaceholderData } = useQuery({
    queryKey: [QUERY_KEYS.workspaceWorkspacesList, debouncedSearch],
    queryFn: fetchWorkspaces,
    placeholderData: keepPreviousData,
  });

  const handleAddClick = () => {
    router.push(`/workspace/create?entityId=${workspace.id}`);
  };

  if (isLoading && !isPlaceholderData) {
    return <Loading />;
  }

  return (
    <>
      <SpaceTabHeader
        setSearchText={setSearchText}
        canCreate={workspace.user_access?.permissions?.includes(Permission.Createworkspaces)}
        searchText={searchText}
        handleClick={handleAddClick}
        title={t('workspace.other')}
        showSearch={!!searchText || !!data?.length}
      />

      {!isLoading && !data?.length && <Empty />}

      <SpaceContentStyled>
        <Flex wrap gap="middle">
          {data?.map((workspace: WorkspaceModel) => (
            <WorkspaceCard
              id={workspace.id}
              key={workspace.id}
              title={workspace.title}
              last_activity={workspace?.updated?.toString() ?? ''}
              short_description={workspace.short_description}
              membersCount={workspace.stats?.members_count}
              status={workspace.status}
              banner_url={workspace.banner_url || workspace.banner_url_sm}
              chip={getContainerAccessLevelChip(workspace) ?? ''}
              imageChips={
                !workspace?.user_access_level
                  ? [getContainerVisibilityChip(workspace), getContainerJoiningRestrictionChip(workspace)]
                  : [getContainerVisibilityChip(workspace)]
              }
            />
          ))}
        </Flex>
      </SpaceContentStyled>
    </>
  );
};
