import { Form, Select, Space, message } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import Trans from 'next-translate/Trans';
import { PrivacySettingsForm } from '../../privacySettingsForm/privacySettingsForm';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Permission, PrivacyLevel, WorkspaceModel, WorkspacePatchModel } from '~/__generated__/types';
import api from '~/src/utils/api/api';
import { useRouter } from 'next/router';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { useMemo } from 'react';

interface Props {
  workspace: WorkspaceModel;
}

export const PrivacySettingsWorkspace = ({ workspace }: Props) => {
  const router = useRouter();
  const { t } = useTranslation('common');

  const updatedWorkspace = useMutation({
    mutationKey: [MUTATION_KEYS.workspacePartialUpdate, workspace.id],
    mutationFn: async (data: WorkspacePatchModel) => {
      await api.workspaces.workspacesPartialUpdateDetail(workspace.id, data);
    },
    onSuccess: () => {
      message.success(t('workspaceWasUpdated'));
      router.replace(router.asPath);
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const { data: workspaceDetails } = useQuery({
    queryKey: [QUERY_KEYS.workspaceDetails, workspace.id],
    queryFn: async () => {
      const response = await api.workspaces.detailDetail(workspace.id);
      return response.data;
    },
  });
  const parentEntityName = useMemo(
    () => workspaceDetails?.path.find(({ entity_type }) => entity_type === 'node')?.title ?? '',
    [workspaceDetails]
  );

  const privacyOptions = [
    { label: t(`privacyOptions.workspace.open`, { entityName: parentEntityName }), value: 'public' },
    { label: t(`privacyOptions.workspace.private`, { entityName: workspace.title }), value: 'private' },
  ];

  return (
    <PrivacySettingsForm
      onUpdate={(data) => updatedWorkspace.mutate(data)}
      showWorkspaceCreationPermission={workspace.user_access.permissions.includes(Permission.Createworkspaces)}
      initialValues={{
        listing_privacy:
          workspace.listing_privacy === PrivacyLevel.Ecosystem ? PrivacyLevel.Public : workspace.listing_privacy,
        content_privacy:
          workspace.content_privacy === PrivacyLevel.Ecosystem ? PrivacyLevel.Public : workspace.content_privacy,
        joining_restriction: workspace.joining_restriction,
        management: workspace.management,
      }}
      privacyFormItems={
        <>
          <Form.Item
            label={<Trans i18nKey={`common:privacySettingsLabel.discoverability`} components={{ em: <em /> }} />}
            name="listing_privacy"
            tooltip={
              <Space direction="vertical">
                <Trans
                  i18nKey={`common:privacySettingsTooltip.open`}
                  components={{ b: <b /> }}
                  values={{ entityName: parentEntityName }}
                />
                <Trans
                  i18nKey={`common:privacySettingsTooltip.private`}
                  components={{ b: <b /> }}
                  values={{ entityName: workspace.title }}
                />
              </Space>
            }
          >
            <Select options={privacyOptions} />
          </Form.Item>
          <Form.Item
            label={<Trans i18nKey={`common:privacySettingsLabel.visibility`} components={{ em: <em /> }} />}
            name="content_privacy"
            tooltip={
              <Space direction="vertical">
                <Trans
                  i18nKey={`common:privacySettingsTooltip.open`}
                  components={{ b: <b /> }}
                  values={{ entityName: parentEntityName }}
                />
                <Trans
                  i18nKey={`common:privacySettingsTooltip.private`}
                  components={{ b: <b /> }}
                  values={{ entityName: workspace.title }}
                />
              </Space>
            }
          >
            <Select options={privacyOptions} />
          </Form.Item>
          <Form.Item
            label={<Trans i18nKey={`common:privacySettingsLabel.membership`} components={{ em: <em /> }} />}
            name="joining_restriction"
            tooltip={
              <Space direction="vertical">
                <Trans
                  i18nKey={`common:privacySettingsMembershipTooltip.open`}
                  components={{ b: <b /> }}
                  values={{ entityName: parentEntityName }}
                />
                <Trans i18nKey={`common:privacySettingsMembershipTooltip.invitationOnly`} components={{ b: <b /> }} />
                <Trans i18nKey={`common:privacySettingsMembershipTooltip.request`} components={{ b: <b /> }} />
              </Space>
            }
          >
            <Select
              options={[
                {
                  label: t('privacySettingsMembershipOptions.workspace.open', { entityName: parentEntityName }),
                  value: 'open',
                },
                { label: t('privacySettingsMembershipOptions.workspace.invitationOnly'), value: 'invite' },
                { label: t('privacySettingsMembershipOptions.workspace.request'), value: 'request' },
              ]}
            />
          </Form.Item>
        </>
      }
    />
  );
};
