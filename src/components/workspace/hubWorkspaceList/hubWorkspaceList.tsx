import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { Divider, Empty, Flex, Input, List, Modal, Typography, message } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useState } from 'react';
import { JoiningRestrictionLevel, Permission, SortKey } from '~/__generated__/types';
import useDebounce from '~/src/hooks/useDebounce';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { ButtonStyled, FlexStyled } from './hubWorkspaceList.styles';
import { ContainerLink } from '../../Common/links/containerLink/containerLink';
import { FlexFullWidthStyled } from '../../Common/common.styles';
import Loader from '../../Common/loader/loader';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import useWorkspaces from '../useWorkspaces';

const PAGE_SIZE = 60;

interface Props {
  hubId: string;
  title: string;
  permission?: Permission;
  sortKey?: SortKey;
  sortAscending?: boolean;
}

const HubWorkspaceList = ({ hubId, title, permission, sortKey = SortKey.Updateddate, sortAscending }: Props) => {
  const { t } = useTranslation('common');
  const queryClient = useQueryClient();
  const [modal, contextHolder] = Modal.useModal();
  const [search, setSearch] = useState('');
  const debouncedSearch = useDebounce(search, 300);
  const [, , goToWorkspaceEdit] = useWorkspaces();

  const { initialize } = useHubDiscussionsStore((store) => ({
    initialize: store.initialize,
  }));

  const { data: spaces, isFetching, isFetched } = useQuery({
    queryKey: [QUERY_KEYS.communityEntitiesList, hubId, debouncedSearch, sortKey, permission],
    queryFn: async () => {
      const response = await api.entities.communityEntitiesList({
        id: hubId,
        SortKey: sortKey,
        Search: debouncedSearch,
        PageSize: PAGE_SIZE,
        permission: permission,
        SortAscending: sortAscending,
      });
      return response.data;
    },
  });

  const leaveWorkspace = useMutation({
    mutationKey: [MUTATION_KEYS.communityEntitiesList],
    mutationFn: async (id: string) => {
      await api.communityEntities.leaveCreate(id);
    },
    onSuccess: () => {
      message.success(t('youLeftWorkspace'));
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.communityEntitiesList],
      });
      initialize();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const showLeaveWarning = (id: string) => {
    modal.confirm({
      title: t('areYouSure'),
      content: t('thisActionIsIrreversible'),
      onOk: () => leaveWorkspace.mutate(id),
      okText: t('action.leave'),
      cancelText: t('action.cancel'),
      okButtonProps: { loading: leaveWorkspace.isPending },
    });
  };

  const joinWorkspace = useMutation({
    mutationKey: [MUTATION_KEYS.communityEntitiesList],
    mutationFn: async (id: string) => {
      await api.communityEntities.joinCreate(id);
    },
    onSuccess: () => {
      message.success(t('youJoinedNewWorkspace'));
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.communityEntitiesList],
      });
      initialize();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const requestToJoinWorkspace = useMutation({
    mutationKey: [MUTATION_KEYS.communityEntitiesList],
    mutationFn: async (id: string) => {
      await api.communityEntities.requestCreate(id);
    },
    onSuccess: () => {
      message.success(t('youRequestedToJoinNewWorkspace'));
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.communityEntitiesList],
      });
      initialize();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  if (isFetching) {
    <Loader />;
  }
  return (
    <Flex vertical gap="middle">
      <Typography.Title>{title}</Typography.Title>
      <Flex justify="stretch">
        <Input.Search
          placeholder={t('action.searchForDocuments')}
          onChange={(e) => {
            setSearch(e.target.value);
          }}
          onSearch={(value) => {
            setSearch(value);
          }}
        />
      </Flex>
      {spaces && !!spaces?.length ? (
        <List
          bordered
          dataSource={spaces}
          renderItem={(space) => (
            <List.Item>
              <FlexFullWidthStyled justify="space-between" align="center">
                <FlexStyled vertical gap="large">
                  <ContainerLink container={space} />
                  <Flex align="center">
                    <Typography.Text>
                      {`${space.stats.members_count} ${t('member', { count: space.stats.members_count })}`}
                    </Typography.Text>
                    {space.short_description && (
                      <>
                        <Divider type="vertical" />
                        <Typography.Text ellipsis={{ tooltip: space.short_description }}>
                          {space.short_description}
                        </Typography.Text>
                      </>
                    )}
                  </Flex>
                </FlexStyled>
                <Flex vertical align="center">
                  {space.user_access_level ? (
                    <>
                      {space.user_access.permissions.includes(Permission.Manage) && (
                        <ButtonStyled type="dashed" onClick={() => goToWorkspaceEdit(space.id, space.type)}>
                          {t('action.manage')}
                        </ButtonStyled>
                      )}
                      <ButtonStyled type="text" onClick={() => showLeaveWarning(space.id)}>
                        {t('action.leave')}
                      </ButtonStyled>
                    </>
                  ) : (
                    <>
                      {(() => {
                        switch (space.joining_restriction) {
                          case JoiningRestrictionLevel.Open:
                            return (
                              space.user_access.permissions.includes(Permission.Join) && (
                                <ButtonStyled type="primary" onClick={() => joinWorkspace.mutate(space.id)}>
                                  {t('action.join')}
                                </ButtonStyled>
                              )
                            );
                          case JoiningRestrictionLevel.Request:
                              if (!space.user_invitation) {
                              return (
                                space.user_access.permissions.includes(Permission.Request) && (
                                  <ButtonStyled type="primary" onClick={() => requestToJoinWorkspace.mutate(space.id)}>
                                    {t('action.requestToJoin')}
                                  </ButtonStyled>
                                )
                              );
                            } else {
                              return <Typography.Text type="secondary">{t('requestSent')}</Typography.Text>;
                            }
                          case JoiningRestrictionLevel.Invite:
                            return <Typography.Text type="secondary">{t('inviteOnly')}</Typography.Text>;
                          default:
                            return <></>;
                        }
                      })()}
                    </>
                  )}
                </Flex>
              </FlexFullWidthStyled>
            </List.Item>
          )}
        />
      ) : (
        <Empty />
      )}

      {contextHolder}
    </Flex>
  );
};

export default HubWorkspaceList;
