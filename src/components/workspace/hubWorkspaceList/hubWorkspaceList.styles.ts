import styled from '@emotion/styled';
import { Button, Flex } from 'antd';

export const FlexStyled = styled(Flex)`
  width: calc(100% - 100px);
  .ant-typography {
    white-space: nowrap;
  }
  .ant-divider-vertical {
    border-inline-start: 1px solid black;
  }
`;

export const ButtonStyled = styled(Button)`
  display: inline-block;
`;
