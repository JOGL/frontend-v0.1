import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import FormDefaultComponent from '~/src/components/Tools/Forms/FormDefaultComponent';
import FormImgComponent from '~/src/components/Tools/Forms/FormImgComponent';
import FormSkillsComponent from '../../Tools/Forms/FormSkillsComponent';
import { generateSlug } from '~/src/utils/utils';
import FormInterestsComponent from '../../Tools/Forms/FormInterestsComponent';
import isMandatoryField from '~/src/utils/isMandatoryField';
import FormTextAreaComponent from '../../Tools/Forms/FormTextAreaComponent';
import FormMissingFieldsList from '../../Tools/Forms/FormMissingFieldsList';
import { WorkspaceModel } from '~/__generated__/types';
import Alert from '../../Tools/Alert/Alert';

interface Props {
  workspace: WorkspaceModel;
  stateValidation: any;
  showErrorList: boolean;
  handleChange: (key: any, content: any) => void;
}

const WorkspaceEditForm: FC<Props> = ({ workspace, stateValidation, showErrorList, handleChange }) => {
  const { t } = useTranslation('common');
  const handleChangeWorkspace = (key, content) => {
    if (key === 'title') {
      handleChange('short_title', generateSlug(content));
    }
    handleChange(key, content);
  };

  const handleChangeLogo = (key, content) => {
    handleChange('logo_url', content?.id ? `${process.env.ADDRESS_BACK}/images/${content?.id}/full` : null);
    handleChange('logo_url_sm', content?.id ? `${process.env.ADDRESS_BACK}/images/${content?.id}/tn` : null);
    handleChange('logo_id', content?.id ?? null);
  };

  const { valid_title, valid_short_description } = stateValidation || {};

  return (
    <form className="WorkspaceForm">
      {showErrorList && <FormMissingFieldsList stateValidation={stateValidation} />}

      {/* Show warning msg if workspace status is draft */}
      {workspace.status === 'draft' && <Alert type="warning" message={t('draftWarningMessage')} />}

      {/* Basic fields */}
      <div tw="flex gap-1">
        <div tw="mr-4">
          <FormImgComponent
            itemId={workspace?.id}
            maxSizeFile={4194304}
            itemType="users"
            type="avatar"
            id="logo_id"
            imageUrl={workspace?.logo_url}
            defaultImg="/images/default/default-workspace-logo.png"
            onChange={handleChangeLogo}
            aspectRatio={1}
            isRounded
          />
        </div>
        <div tw="flex-1">
          <FormDefaultComponent
            id="title"
            content={workspace.title}
            title={t('title')}
            placeholder={t('aGreatWorkspace')}
            onChange={handleChangeWorkspace}
            mandatory={isMandatoryField('workspace', 'title')}
            errorCodeMessage={valid_title ? valid_title.message : ''}
            isValid={valid_title ? !valid_title.isInvalid : undefined}
            maxChar={120}
          />
          <FormDefaultComponent
            id="label"
            content={workspace.label}
            title={t('primaryLabel')}
            placeholder={t('primaryLabel')}
            onChange={handleChangeWorkspace}
            maxChar={120}
          />
        </div>
      </div>
      <FormImgComponent
        id="banner_id"
        type="banner"
        title={t('coverImage')}
        itemType="workspaces"
        itemId={workspace.id}
        imageUrl={workspace.banner_url}
        showAddButton={workspace?.banner_url === null}
        defaultImg="/images/default/default-workspace.png"
        onChange={handleChange}
      />

      <FormTextAreaComponent
        content={workspace.short_description}
        id="short_description"
        maxChar={500}
        onChange={handleChange}
        rows={3}
        title={t('shortDescription')}
        placeholder={t('describeYourWorkspaceInShort')}
        errorCodeMessage={valid_short_description ? valid_short_description.message : ''}
        isValid={valid_short_description ? !valid_short_description.isInvalid : undefined}
        mandatory={isMandatoryField('workspace', 'short_description')}
      />
      <FormSkillsComponent
        id="keywords"
        type="keywords"
        placeholder={t('skillsPlaceholder')}
        title={t('keyword.other')}
        showHelp
        content={workspace?.keywords}
        mandatory={false}
        onChange={handleChange}
      />

      <FormInterestsComponent
        id="sdgs"
        content={workspace.interests}
        title={t('sdgs')}
        showHelp={false}
        onChange={handleChange}
      />
    </form>
  );
};
export default WorkspaceEditForm;
