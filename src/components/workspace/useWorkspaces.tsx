import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import { CommunityEntityType, Permission, SortKey } from '~/__generated__/types';
import { FrontPath } from '../Layout/menuItems/menuItems';

const useWorkspaces = (): [
  (id: string, tab?: string) => void,
  (key: string) => string,
  (id: string, type: CommunityEntityType) => void
] => {
  const router = useRouter();
  const { t } = useTranslation('common');

  const goToWorkspace = (id: string, tab?: string) => {
    if (tab) router.push(`/${FrontPath.CommunityEntity}/${id}?tab=${tab}`);
    else router.push(`/${FrontPath.CommunityEntity}/${id}`);
  };

  const getHubWorkspacesViewTitle = (key: string): string => {
    switch (key) {
      case 'created':
        return t('workspace.my');
      case 'recent':
        return t('workspace.recent');
      case 'all':
      default:
        return t('workspace.all');
    }
  };

  const goToWorkspaceEdit = (id: string, type: CommunityEntityType) => {
    switch (type) {
      case CommunityEntityType.Node:
        router.push(`/hub/${id}/edit`);
        break;
      default:
        router.push(`/workspace/${id}/edit`);
    }
  };

  return [goToWorkspace, getHubWorkspacesViewTitle, goToWorkspaceEdit];
};

export const getHubWorkspacesViewFilter = (
  key: string
): { sortKey: SortKey; permission?: Permission; sortAscending?: boolean } => {
  switch (key) {
    case 'created':
      return { sortKey: SortKey.Updateddate, permission: Permission.Membership };
    case 'recent':
      return { sortKey: SortKey.Createddate, sortAscending: false };
    case 'all':
    default:
      return { sortKey: SortKey.Updateddate };
  }
};

export default useWorkspaces;
