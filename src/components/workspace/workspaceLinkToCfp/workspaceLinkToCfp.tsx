import React, { useState, useMemo, FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from '~/src/contexts/apiContext';
import useGet from '~/src/hooks/useGet';
import Alert from '../../Tools/Alert/Alert';
import Button from '../../primitives/Button';
import Loading from '../../Tools/Loading';
import SpinLoader from '../../Tools/SpinLoader';
import Select from 'react-select';
import { useRouter } from 'next/router';
import { confAlert } from '~/src/utils/utils';

interface PropsModal {
  userId: string;
  cfpId: string;
}

export const WorkspaceLinkToCfp: FC<PropsModal> = ({ cfpId, userId }) => {
  const { data: dataWorkspacesMine } = useGet(`/users/${userId}/workspaces?permission=manage`);
  const [selectedWorkspaceId, setSelectedWorkspaceId] = useState(null);
  const [sending, setSending] = useState(false);
  const [requestSent, setRequestSent] = useState(false);
  const [hasChecked, setHasChecked] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const api = useApi();
  const { t } = useTranslation('common');
  const router = useRouter();
  const { data: proposals } = useGet(`/cfps/${cfpId}/proposals`);

  const filteredWorkspaces = useMemo(() => {
    return (dataWorkspacesMine as any)?.filter((x) => x.title !== '');
  }, [dataWorkspacesMine]);

  const onSubmit = (e) => {
    e.preventDefault();
    setSending(true);
    // Create proposal that is linked to the selected workspace
    if (selectedWorkspaceId) {
      api
        .post('/proposals', { call_for_proposal_id: cfpId, project_id: selectedWorkspaceId })
        .then((res) => {
          setSending(false);
          setRequestSent(true);
          setIsButtonDisabled(true);
          confAlert.fire({ icon: 'success', title: t('createdSuccess') });
          // Redirect to proposal edit page
          router.push(`/proposal/${res?.data}/edit`);
        })
        .catch(() => {
          confAlert.fire({ icon: 'error', title: 'Error' });
          console.error(
            `Could not POST cfp with id=${cfpId} with the linked workspace with workspaceId=${selectedWorkspaceId}`
          );
          setSending(false);
        });
    }
  };

  const onWorkspaceSelect = (workspace) => {
    setSelectedWorkspaceId(filteredWorkspaces.find((item) => item.title === workspace.value).id);
    hasChecked && setIsButtonDisabled(false);
  };

  const handleCheckBoxChange = (e) => {
    setHasChecked(true);
    selectedWorkspaceId && setIsButtonDisabled(false);
  };

  const optionsList = filteredWorkspaces?.map((option) => {
    return {
      value: option.title,
      label: `${option.title} ${proposals?.map((x) => x.project.id).includes(option.id) ? '(already applied)' : ''}`,
      isDisabled: proposals?.map((x) => x.project.id).includes(option.id),
    };
  });

  return (
    <div>
      {!filteredWorkspaces ? (
        <Loading />
      ) : filteredWorkspaces.length > 0 ? (
        <>
          <Select
            options={optionsList}
            menuShouldScrollIntoView={true} // force scroll into view
            noOptionsMessage={() => null}
            placeholder={t('selectWorkspaceToApply')}
            onChange={onWorkspaceSelect}
          />
          <div tw="mt-4">
            <input
              type="radio"
              id="read"
              name="read"
              value="read"
              onChange={handleCheckBoxChange}
              checked={hasChecked}
            />
            <label htmlFor="read">
              {t('certifyThatReadAndAcceptedCfpRules')}&nbsp;
              <a target="_blank" href={`/cfp/${cfpId}?tab=rules`}>
                {t('rule', { count: 2 })}
              </a>
            </label>
          </div>
          <div className="btnZone">
            <Button type="submit" disabled={isButtonDisabled || sending} onClick={onSubmit} tw="mb-4">
              <>
                {sending && <SpinLoader />}
                {t('action.continue')}
              </>
            </Button>
            {requestSent && <Alert type="success" message={t('workspaceSubmissionSuccess')} />}
          </div>
        </>
      ) : (
        <div>{t('noWorkspaceToAdd')}</div>
      )}
    </div>
  );
};
