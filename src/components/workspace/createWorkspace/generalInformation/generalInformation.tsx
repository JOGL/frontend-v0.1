import { FC, useState } from 'react';
import { Button, Col, Flex, Form, Input, Row, Select, Typography } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { LeftOutlined, RightOutlined } from '@ant-design/icons';
import { BaseFormProps } from '../createWorkspace.types';

const { TextArea } = Input;
const { Title } = Typography;

const GeneralInformation: FC<BaseFormProps> = ({ formData, onFinish, onSave, onBack }) => {
  const [form] = Form.useForm();
  const { t } = useTranslation('common');
  const [isAnyFieldEmpty, setIsAnyFieldEmpty] = useState(
    !formData.title || !formData.label || !formData.short_description
  );

  const getFormState = () => {
    const values = form.getFieldsValue();
    return {
      ...formData,
      ...values,
      short_title: values.title,
    };
  };

  const handleValuesChange = () => {
    const values = form.getFieldsValue();
    setIsAnyFieldEmpty(!values.title || !values.label || !values.short_description);
  };

  return (
    <Flex vertical gap="large">
      <Title level={2}>{t('generalInformation')}</Title>
      <Form
        layout="vertical"
        onFinish={() => onFinish(getFormState())}
        onValuesChange={handleValuesChange}
        requiredMark={true}
        form={form}
        initialValues={{
          title: formData.title,
          label: formData.label,
          short_description: formData.short_description,
          keywords: formData.keywords,
        }}
      >
        <Form.Item
          name="title"
          label={t('nameYourWorkspace')}
          rules={[{ required: true, message: t('error.requiredField') }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="label"
          label={t('primaryLabel')}
          rules={[{ required: true, message: t('error.requiredField') }]}
        >
          <Input placeholder={t('workspace.one')} />
        </Form.Item>

        <Form.Item
          name="short_description"
          label={t('describeYourWorkspaceInShort')}
          rules={[{ required: true, message: t('error.requiredField') }]}
        >
          <TextArea
            showCount
            maxLength={150}
            placeholder={`150 ${t('charactersMax')}`}
            style={{ height: 70, resize: 'none' }}
          />
        </Form.Item>

        <Form.Item name="keywords" label={t('keyword.other')}>
          <Select mode="tags" dropdownStyle={{ display: 'none' }} />
        </Form.Item>

        <Row justify="start" gutter={12}>
          <Col>
            <Button type="default" icon={<LeftOutlined />} onClick={() => onBack(getFormState())}>
              {t('action.back')}
            </Button>
          </Col>
          <Col>
            <Button type="primary" icon={<RightOutlined />} htmlType="submit" iconPosition={'end'}>
              {t('action.next')}
            </Button>
          </Col>
          <Col>
            <Button type="link" onClick={() => onSave(getFormState())} disabled={isAnyFieldEmpty}>
              {t('action.saveAsDraft')}
            </Button>
          </Col>
        </Row>
      </Form>
    </Flex>
  );
};

export default GeneralInformation;
