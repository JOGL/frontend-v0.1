import { FC } from 'react';
import { Button, Col, Flex, Form, Row, Space, Switch, Typography } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { LeftOutlined, RightOutlined } from '@ant-design/icons';
import { BaseFormProps } from '../createWorkspace.types';
import { FormStyled } from './functionalities.styles';

const { Title, Text } = Typography;

const Functionalities: FC<BaseFormProps> = ({ formData, onBack, onFinish, onSave }) => {
  const [form] = Form.useForm();
  const { t } = useTranslation('common');

  const getFormState = () => {
    const values = form.getFieldsValue();
    const tabs = Object.keys(values).filter((key) => values[key]);
    return {
      ...formData,
      tabs,
    };
  };

  const initialValues = formData.tabs?.reduce((acc, item) => {
    acc[item] = true;
    return acc;
  }, {});

  return (
    <Flex gap="large" vertical>
      <Title level={2}>{t('functionalities')}</Title>
      <Space direction="vertical" size={0}>
        <Text> {t('selectTheFunctionalities')}</Text>
        <Text type="secondary">{t('thisCanBeChangedLater')} </Text>
      </Space>

      <FormStyled colon={false} form={form} initialValues={initialValues} onFinish={() => onFinish(getFormState())}>
        <Form.Item name="documents" label={t('document.other')} valuePropName="checked">
          <Switch />
        </Form.Item>

        <Form.Item name="library" label={t('library')} valuePropName="checked">
          <Switch />
        </Form.Item>

        <Form.Item name="needs" label={t('need.other')} valuePropName="checked">
          <Switch />
        </Form.Item>

        <Form.Item name="resources" label={t('resource.other')} valuePropName="checked">
          <Switch />
        </Form.Item>

        <Row justify="start" gutter={12}>
          <Col>
            <Button type="default" icon={<LeftOutlined />} onClick={() => onBack(getFormState())}>
              {t('action.back')}
            </Button>
          </Col>
          <Col>
            <Button type="primary" icon={<RightOutlined />} htmlType="submit" iconPosition={'end'}>
              {t('action.next')}
            </Button>
          </Col>
          <Col>
            <Button type="link" onClick={() => onSave(getFormState())}>
              {t('action.saveAsDraft')}
            </Button>
          </Col>
        </Row>
      </FormStyled>
    </Flex>
  );
};

export default Functionalities;
