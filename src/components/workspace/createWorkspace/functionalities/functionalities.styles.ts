import styled from '@emotion/styled';
import { Form } from 'antd';

export const FormStyled = styled(Form)`
  .ant-form-item-row {
    flex-direction: row-reverse;
    justify-content: start;
  }

  .ant-form-item-control {
    width: 64px;
    flex: initial;
  }
`;
