import { WorkspaceUpsertModel } from '~/__generated__/types';

export interface BaseFormProps {
  formData: WorkspaceUpsertModel;
  onBack: (data: WorkspaceUpsertModel) => void;
  onFinish: (data: WorkspaceUpsertModel) => void;
  onSave: (data: WorkspaceUpsertModel) => void;
}
