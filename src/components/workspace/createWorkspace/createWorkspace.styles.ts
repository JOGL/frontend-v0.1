import styled from '@emotion/styled';

export const ContainerStyled = styled.div`
  padding: ${({ theme }) => theme.token.paddingXL}px;
  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    padding: ${({ theme }) => theme.token.paddingXXS}px;
  }
`;

export const StepsContainerStyled = styled.div`
  margin-top: ${({ theme }) => theme.token.paddingXL}px;
  margin-bottom: ${({ theme }) => theme.token.paddingXL}px;
`;
export const ContentStyled = styled.div`
  width: 550px;
  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    width: 100%;
  }
`;
