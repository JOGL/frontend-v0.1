import { FC } from 'react';
import { Button, Col, Flex, Form, Row, Typography } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { CommunityEntityMiniModel, WorkspaceUpsertModel } from '~/__generated__/types';
import { useRouter } from 'next/router';
import { RightOutlined } from '@ant-design/icons';
import ContainerPicker from '~/src/components/Common/container/containerPicker/containerPicker';

const { Title } = Typography;

interface ChooseWorkspaceProps {
  formData: WorkspaceUpsertModel;
  onFinish: (value: { parent_id: string }) => void;
}

const ChooseWorkspace: FC<ChooseWorkspaceProps> = ({ formData, onFinish }) => {
  const [form] = Form.useForm();
  const router = useRouter();
  const { t } = useTranslation('common');

  const onCancel = () => {
    form.resetFields();
    router.push('/');
  };

  return (
    <Flex gap="large" vertical>
      <Title level={2}>{t('chooseWhereToCreateYourWorkspace')}</Title>
      <Form
        layout="vertical"
        onFinish={(data) => onFinish({ ...formData, ...data })}
        requiredMark={true}
        form={form}
        initialValues={{ parent_id: formData.parent_id }}
      >
        <Form.Item
          name="parent_id"
          label={t('space.one')}
          rules={[{ required: true, message: t('error.requiredField') }]}
        >
          <ContainerPicker
            onChange={(container?: CommunityEntityMiniModel) => {
              form.setFieldValue('parent_id', container?.id);
            }}
          />
        </Form.Item>
        <Row justify="start" gutter={16}>
          <Col>
            <Button type="default" onClick={onCancel}>
              {t('action.cancel')}
            </Button>
          </Col>
          <Col>
            <Button type="primary" htmlType="submit" icon={<RightOutlined />} iconPosition={'end'}>
              {t('action.next')}
            </Button>
          </Col>
        </Row>
      </Form>
    </Flex>
  );
};

export default ChooseWorkspace;
