import { FC } from 'react';
import { Button, Col, Flex, Form, Popover, Radio, Row, Space, Typography } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { WorkspaceModel, JoiningRestrictionLevel, PrivacyLevel } from '~/__generated__/types';
import HubLogo from '~/src/components/Common/hubLogo/hubLogo';
import { InfoCircleOutlined, LeftOutlined, RightOutlined } from '@ant-design/icons';
import Trans from 'next-translate/Trans';
import { BorderStyled, RadioGroupStyled } from './hostAndPrivacy.styles';
import { toInitials } from '~/src/utils/utils';
import { BaseFormProps } from '../createWorkspace.types';

const { Title, Text } = Typography;

interface HostAndPrivacyProps extends Omit<BaseFormProps, 'onSave'> {
  workspaces: WorkspaceModel[];
}

const HostAndPrivacy: FC<HostAndPrivacyProps> = ({ workspaces, formData, onBack, onFinish }) => {
  const [form] = Form.useForm();
  const { t } = useTranslation('common');
  const workspace = workspaces.find((item) => item.id === formData.parent_id);

  const getFormState = () => {
    const values = form.getFieldsValue();
    return {
      ...formData,
      listing_privacy: values.visibility,
      content_privacy: values.visibility,
      joining_restriction:
        values.visibility === PrivacyLevel.Private ? JoiningRestrictionLevel.Invite : JoiningRestrictionLevel.Open,
    };
  };

  if (!workspace) {
    return;
  }

  return (
    <Flex gap="large" vertical>
      <Title level={2}>{t('hostAndPrivacy')}</Title>
      <Text strong> {t('hostSpace')}</Text>
      <Flex gap="small" align="center">
        {workspace.logo_url_sm ? (
          <HubLogo src={workspace.logo_url_sm} alt={workspace.title} />
        ) : (
          <BorderStyled>
            <Flex justify="center" align="center">
              <Text>{toInitials(workspace.title)}</Text>
            </Flex>
          </BorderStyled>
        )}
        <Text type="secondary"> {workspace.title}</Text>
      </Flex>

      <Space direction="vertical" size={0}>
        <Flex gap="small">
          <Text strong> {t('visibilitySettingsForWorkspace')}</Text>
          <Popover
            placement="right"
            content={
              <Space direction="vertical">
                <Flex vertical>
                  <Text type="secondary">
                    <Trans
                      i18nKey={`common:privacySettingsLabelWorkspace.discoverability`}
                      components={{ b: <Text /> }}
                    />
                  </Text>
                  <Text type="secondary">
                    <Trans i18nKey={`common:privacySettingsLabelWorkspace.visibility`} components={{ b: <Text /> }} />
                  </Text>
                  <Text type="secondary">
                    <Trans i18nKey={`common:privacySettingsLabelWorkspace.membership`} components={{ b: <Text /> }} />
                  </Text>
                </Flex>
              </Space>
            }
          >
            <InfoCircleOutlined />
          </Popover>
        </Flex>
        <Text type="secondary">{t('thisCanBeChangedLater')}</Text>
      </Space>
      <Form
        layout="vertical"
        onFinish={() => onFinish(getFormState())}
        requiredMark={true}
        form={form}
        initialValues={{ visibility: formData.content_privacy }}
      >
        <Form.Item name="visibility">
          <RadioGroupStyled defaultValue={PrivacyLevel.Private}>
            <Space direction="vertical">
              <Radio value={PrivacyLevel.Private}>
                <Flex vertical>
                  <Text>{t('private')}</Text>

                  <Text type="secondary">
                    {' • '} {t('discoverabilityMembersOnly')}
                  </Text>
                  <Text type="secondary">
                    {' • '} {t('visibilityMembersOnly')}
                  </Text>
                  <Text type="secondary">
                    {' • '} {t('membershipInviteOnly')}
                  </Text>
                </Flex>
              </Radio>
              <Radio value={PrivacyLevel.Public}>
                <Flex vertical>
                  <Text>{t('open')}</Text>
                  <Text type="secondary">
                    {' • '} {t('discoverabilityOpenToMembersOf', { workspace: workspace.title })}
                  </Text>
                  <Text type="secondary">
                    {' • '}
                    {t('visibilityOpenToMembersOf', { workspace: workspace.title })}
                  </Text>
                  <Text type="secondary">
                    {' • '}
                    {t('membershipOpenToMembersOf', { workspace: workspace.title })}
                  </Text>
                </Flex>
              </Radio>
            </Space>
          </RadioGroupStyled>
        </Form.Item>
        <Row justify="start" gutter={16}>
          <Col>
            <Button type="default" icon={<LeftOutlined />} onClick={() => onBack(getFormState())}>
              {t('action.back')}
            </Button>
          </Col>
          <Col>
            <Button type="primary" icon={<RightOutlined />} htmlType="submit" iconPosition={'end'}>
              {t('action.next')}
            </Button>
          </Col>
        </Row>
      </Form>
    </Flex>
  );
};

export default HostAndPrivacy;
