import styled from '@emotion/styled';
import { Radio } from 'antd';

export const RadioGroupStyled = styled(Radio.Group)`
  .ant-radio {
    align-self: flex-start;
  }
`;
export const BorderStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;

  div {
    width: 40px;
    height: 40px;
    padding: 4px;
    cursor: pointer;
    text-transform: uppercase;
    border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;
    border: 2px solid ${(props) => props.theme.token.neutral5};

    span {
      font-size: ${({ theme }) => theme.fontSizes['xs']};
      white-space: nowrap;
    }
    &:hover {
      border-color: ${({ theme }) => theme.token.colorPrimary};
    }
  }
`;
