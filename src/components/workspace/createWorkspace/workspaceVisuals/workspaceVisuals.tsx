import { Dispatch, FC, SetStateAction } from 'react';
import { Typography, Flex, Space, Row, Col, Button, Grid } from 'antd';
import { LeftOutlined } from '@ant-design/icons';
import ImageUpload from '~/src/components/Common/imageUpload/imageUpload';
import { BaseFormProps } from '../createWorkspace.types';
import useTranslation from 'next-translate/useTranslation';
import { ImageInsertResultModel } from '~/__generated__/types';

const { Text, Title } = Typography;

interface WorkspaceVisualsProps extends BaseFormProps {
  logo: ImageInsertResultModel | undefined;
  setLogo: Dispatch<SetStateAction<ImageInsertResultModel | undefined>>;
  banner: ImageInsertResultModel | undefined;
  setBanner: Dispatch<SetStateAction<ImageInsertResultModel | undefined>>;
}

const WorkspaceVisuals: FC<WorkspaceVisualsProps> = ({
  formData,
  onBack,
  onFinish,
  onSave,
  logo,
  setLogo,
  banner,
  setBanner,
}) => {
  const { t } = useTranslation('common');
  const { useBreakpoint } = Grid;
  const screens = useBreakpoint();

  const getFormState = () => {
    return {
      ...formData,
      logo_id: logo?.id,
      banner_id: banner?.id,
    };
  };

  return (
    <Flex vertical gap="large">
      <Title level={2}>{t('action.createWorkspace')}</Title>
      <Space size={0} direction="vertical">
        <Text>{t('addVisualsToYourWorkspace')}</Text>
        <Text type="secondary">{t('youCanAlsoSkipThisStepAndDoItLater')}</Text>
      </Space>

      <Space size="small" direction="vertical">
        <Text>{t('profileImage')}</Text>
        <ImageUpload
          handleFileUpload={(data) => {
            setLogo(data);
          }}
          imageSrc={logo?.url}
        />
      </Space>

      <Space size="small" direction="vertical">
        <Text>{t('coverImage')}</Text>
        <ImageUpload
          width={screens.md ? 500 : 300}
          height={156}
          handleFileUpload={(data) => {
            setBanner(data);
          }}
          imageSrc={banner?.url}
        />
      </Space>
      <Row justify="start" gutter={12}>
        <Col>
          <Button type="default" icon={<LeftOutlined />} onClick={() => onBack(getFormState())}>
            {t('action.back')}
          </Button>
        </Col>
        <Col>
          <Button type="primary" iconPosition={'end'} onClick={() => onFinish(getFormState())}>
            {t('action.create')}
          </Button>
        </Col>
        <Col>
          <Button type="link" onClick={() => onSave(getFormState())}>
            {t('action.saveAsDraft')}
          </Button>
        </Col>
      </Row>
    </Flex>
  );
};

export default WorkspaceVisuals;
