import { Steps, message } from 'antd';
import React, { FC, useState } from 'react';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import {
  WorkspaceModel,
  ImageInsertResultModel,
  JoiningRestrictionLevel,
  PrivacyLevel,
  WorkspaceUpsertModel,
} from '~/__generated__/types';
import Layout from '~/src/components/Layout/layout/layout';
import { ContainerStyled, ContentStyled, StepsContainerStyled } from './createWorkspace.styles';
import ChooseWorkspace from './chooseWorkspace/chooseWorkspace';
import HostAndPrivacy from './hostAndPrivacy/hostAndPrivacy';
import GeneralInformation from './generalInformation/generalInformation';
import Functionalities from './functionalities/functionalities';
import WorkspaceVisuals from './workspaceVisuals/workspaceVisuals';
import api from '~/src/utils/api/api';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';

const { Step } = Steps;

const DEFAULT_INITIAL_VALUES = {
  title: '',
  short_title: '',
  short_description: '',
  status: '',
  label: '',
  parent_id: '',
  listing_privacy: PrivacyLevel.Private,
  content_privacy: PrivacyLevel.Private,
  joining_restriction: JoiningRestrictionLevel.Invite,
  tabs: ['documents', 'library', 'needs', 'resources'],
};

const CreateWorkspace: FC<{ workspaces: WorkspaceModel[] }> = ({ workspaces }) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const [currentStep, setCurrentStep] = useState(0);
  const [logo, setLogo] = useState<ImageInsertResultModel>();
  const [banner, setBanner] = useState<ImageInsertResultModel>();
  const [draftId, setDraftId] = useState('');
  const queryClient = useQueryClient();
  const parentId = workspaces.find((item) => item.id === router.query.entityId)?.id ?? '';
  const [formData, setFormData] = useState<WorkspaceUpsertModel>({
    ...DEFAULT_INITIAL_VALUES,
    parent_id: parentId,
  });

  const mutation = useMutation({
    mutationFn: async (data: WorkspaceUpsertModel) => {
      if (draftId) {
        const response = await api.workspaces.workspacesUpdateDetail(draftId, data);
        return { id: response.data, status: data.status };
      }
      const response = await api.workspaces.workspacesCreate(data);
      return { id: response.data, status: data.status };
    },
    onSuccess: ({ id, status }) => {
      if (currentStep === 4 && status !== 'draft') {
        message.success(t('workspaceWasCreated'));
        router.push(`/workspace/${id}`);
      } else {
        setDraftId(id);
        queryClient.invalidateQueries({
          queryKey: [QUERY_KEYS.feedNodesList],
        });
        message.success(t('workspaceWasSaved'));
      }
    },
    onError: (error: Error) => {
      console.log('error', error);
      message.error(t('error.somethingWentWrong'));
    },
  });

  const handleNext = (data: WorkspaceUpsertModel) => {
    setCurrentStep(currentStep + 1);
    setFormData(data);
  };

  const handleBack = (data: WorkspaceUpsertModel) => {
    setCurrentStep(currentStep - 1);
    setFormData(data);
  };

  const handleSave = (data: WorkspaceUpsertModel) => {
    mutation.mutate({ ...data, status: 'draft' });
  };

  const handleCreate = (data: WorkspaceUpsertModel) => {
    mutation.mutate({ ...data, status: 'active' });
  };

  return (
    <Layout title={t('createWorkspace')}>
      <ContainerStyled>
        <StepsContainerStyled>
          <Steps progressDot current={currentStep}>
            {Array.from({ length: 5 }).map((_, index) => (
              <Step key={index} />
            ))}
          </Steps>
        </StepsContainerStyled>

        <ContentStyled>
          {currentStep === 0 && <ChooseWorkspace formData={formData} onFinish={handleNext} />}
          {currentStep === 1 && (
            <HostAndPrivacy workspaces={workspaces} formData={formData} onBack={handleBack} onFinish={handleNext} />
          )}

          {currentStep === 2 && (
            <GeneralInformation formData={formData} onFinish={handleNext} onBack={handleBack} onSave={handleSave} />
          )}
          {currentStep === 3 && (
            <Functionalities formData={formData} onFinish={handleNext} onBack={handleBack} onSave={handleSave} />
          )}
          {currentStep === 4 && (
            <WorkspaceVisuals
              logo={logo}
              banner={banner}
              setLogo={setLogo}
              setBanner={setBanner}
              formData={formData}
              onFinish={handleCreate}
              onBack={handleBack}
              onSave={handleSave}
            />
          )}
        </ContentStyled>
      </ContainerStyled>
    </Layout>
  );
};

export default CreateWorkspace;
