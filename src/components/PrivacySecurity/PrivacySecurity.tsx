import { FunctionComponent, useEffect, useState } from 'react';
import styles from './PrivacySecurity.module.css';
import { securityOptionsOrganization, securityOptionsCfp } from '~/src/utils/security_options';
import { Cfp, Organization } from '~/src/types';
import Alert from '../Tools/Alert/Alert';
import tw from 'twin.macro';
import Icon from '../primitives/Icon';
import useTranslation from 'next-translate/useTranslation';
import Trans from 'next-translate/Trans';

interface PrivacySecurityProps {
  object: Organization | Cfp;
  type: 'organization' | 'cfp';
  handleChange: (key, content, remove?: boolean) => void;
}

const PrivacySecurity: FunctionComponent<PrivacySecurityProps> = ({ object, type, handleChange }) => {
  const TypeMatch = {
    organization: { securityOptions: securityOptionsOrganization, noteTranslationId: '' },
    cfp: {
      securityOptions: securityOptionsCfp,
      noteTranslationId: 'ecosystemRepresentationForCallForProposals',
    },
  };

  const { t } = useTranslation('common');
  const [objectInfo, setObjectInfo] = useState<any>({});

  useEffect(() => {
    fetchObjectDetails();
  }, []);

  const fetchObjectDetails = async () => {
    try {
      const {
        listing_privacy,
        content_privacy,
        joining_restriction,
        management,
        proposal_participiation,
        proposal_privacy,
        discussion_participation,
      } = object;
      setObjectInfo({
        listing_privacy,
        content_privacy,
        joining_restriction,
        management,
        proposal_participiation,
        proposal_privacy,
        discussion_participation,
      });
    } catch (e) {}
  };

  const updateObjectInfo = (item, option) => {
    if (item === 'management') {
      const management = objectInfo.management;
      const newArr = management.includes(option) ? management.filter((x) => x !== option) : [...management, option];
      setObjectInfo({ ...objectInfo, [item]: newArr });
      handleChange([item], newArr);
    } else {
      setObjectInfo({ ...objectInfo, [item]: option });
      handleChange([item], option);
    }
  };
  const isDisabled = (item) => item.isDisabled && item.isDisabled(objectInfo?.management);

  return (
    <div className={styles.frameParent}>
      {TypeMatch[type].securityOptions.map((item, i) => (
        <div
          className={styles.contentGroupContainer}
          key={`${item.key}-${i}`}
          css={isDisabled(item) && tw`cursor-not-allowed pointer-events-none opacity-40`}
        >
          <h3>{t(item.header)}</h3>
          {item.info && <p tw="mb-1">{t(item.info)}</p>}
          {object.status === 'draft' && (item.key === 'listing_privacy' || item.key === 'content_privacy') && (
            <Alert type="warning" message={t('theStatusFieldIsSetToDraft')} />
          )}
          <div
            className={styles.radioSelectContainer}
            css={
              object.status === 'draft' &&
              (item.key === 'listing_privacy' || item.key === 'content_privacy') &&
              tw`cursor-not-allowed pointer-events-none opacity-40`
            }
          >
            {item.options.map((option) => {
              let isChecked = objectInfo[item.key] === option.key;

              if (item.key === 'management') {
                isChecked = option.isChecked(objectInfo?.management || []);
              }

              return (
                <div
                  className={
                    isChecked
                      ? `${styles.radioSelectUI} ${styles.selected}`
                      : option.notNeeded
                      ? `${styles.radioSelectUI} ${styles.notNeeded}`
                      : `${styles.radioSelectUI}`
                  }
                  css={option.info && tw`h-[220px]`}
                  onClick={(e) => {
                    e.preventDefault();
                    updateObjectInfo(item.key, option.key);
                  }}
                  key={`${option.header}-${i}`}
                >
                  <input
                    type="radio"
                    key={objectInfo[item.key]}
                    checked={isChecked}
                    value={option.key}
                    onChange={(e) => e.preventDefault()}
                  />
                  <div tw="w-full">
                    <Icon icon={option.icon} tw="w-8 h-8" />
                    <p className="header">{t(option.header)}</p>
                    <p className="info">
                      {option.info && <Trans i18nKey={`common:${option.info}`} components={[<b key={option.key} />]} />}
                    </p>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      ))}
      {TypeMatch[type].noteTranslationId && (
        <div tw="mt-4 italic text-sm">
          <b>*{t('ecosystem')}:</b> {t(TypeMatch[type].noteTranslationId)}
        </div>
      )}
    </div>
  );
};

export default PrivacySecurity;
