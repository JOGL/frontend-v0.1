import React, { FC, ReactNode } from 'react';

interface CardProps {
  children: ReactNode;
  width?: string;
  onClick?: (e: any) => void;
}

const Card: FC<CardProps> = ({ children, width, ...props }) => {
  return (
    <div tw="flex flex-col shadow-custom rounded-xl bg-white p-4" style={{ width }} {...props}>
      {children}
    </div>
  );
};
export default Card;
