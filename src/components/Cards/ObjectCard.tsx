import Link from 'next/link';
import React, { FC, ReactNode } from 'react';
import Image from '~/src/components/primitives/Image';
import tw from 'twin.macro';
import ReactTooltip from 'react-tooltip';
import Icon from '../primitives/Icon';
import { ImageChip, TextChip } from '~/src/types/chip';

interface CardProps {
  imgUrl?: string;
  href?: string;
  children: ReactNode;
  containerStyle?: any;
  width?: string;
  height?: string;
  chip?: TextChip;
  hrefNewTab?: boolean;
  cardFormat?: string;
  aggregatedChip?: boolean;
  textChips?: TextChip[];
  imageChips?: ImageChip[];
}

const ObjectCard: FC<CardProps> = ({
  imgUrl,
  children,
  href,
  width = '240px',
  height = '330px',
  chip,
  aggregatedChip,
  hrefNewTab = false,
  cardFormat,
  textChips = [],
  imageChips = [],
  ...props
}) => {
  const cardHeight = cardFormat === 'inPosts' ? '190px' : height;
  return (
    <div
      style={{ width, height: cardHeight }}
      tw="relative flex flex-col overflow-hidden bg-white shadow-custom rounded-xl"
      {...props.containerStyle}
      {...props}
    >
      {/* if card has image and image is also a link */}
      {imgUrl && href && (
        <div tw="relative">
          <Link href={href} target={hrefNewTab ? '_blank' : undefined}>
            <div tw="relative">
              <div tw="bg-gray-200 w-full h-full rounded-t-2xl absolute" />
              <Image
                src={imgUrl}
                // priority
                blurDataURL="data:image/jpeg;base64,/9j/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAGAAoDASIAAhEBAxEB/8QAFgABAQEAAAAAAAAAAAAAAAAAAAYH/8QAHhAAAQMFAQEAAAAAAAAAAAAAAQMFEQACBAYhE0L/xAAVAQEBAAAAAAAAAAAAAAAAAAAAAf/EABoRAAEFAQAAAAAAAAAAAAAAAAABAhEiYTH/2gAMAwEAAhEDEQA/AN12fX9ldHcZjc/luSxVE78XHSJ81QIN9q4+hd0ciBVmJilKruxgalZ1T//Z"
                placeholder="blur"
                tw="w-full object-cover rounded-tl-xl rounded-tr-xl bg-white hover:opacity-95 h-[92px]!"
                alt={href}
              />
            </div>
          </Link>
          <div tw="absolute top-2 left-1 flex">
            {/* Use for general image chips -> item === icon name*/}
            {imageChips?.length > 0 &&
              imageChips?.map((item, i) => (
                <React.Fragment key={i}>
                  <div
                    tw="ml-1 text-xs font-medium text-gray-600 rounded bg-white bg-opacity-80 px-2 py-1 [box-shadow:0px 4px 15px rgb(0 0 0 / 9%)] first-letter:capitalize"
                    {...(item?.tooltip && {
                      'data-tip': `${item?.tooltip}`,
                      'data-for': `imageChip-${i}`,
                    })}
                  >
                    <Icon icon={item?.icon} tw="w-4 h-4 m-0" />
                  </div>
                  <ReactTooltip
                    id={`imageChip-${i}`}
                    effect="solid"
                    role="tooltip"
                    type="dark"
                    className="forceTooltipBg"
                    aria-haspopup="true"
                  />
                </React.Fragment>
              ))}

            {/* Main chip - usually the user_access_level*/}
            {chip && (
              <>
                <div
                  tw="ml-1 text-xs font-medium text-gray-600 rounded bg-white bg-opacity-80 px-2 py-1 [box-shadow:0px 4px 15px rgb(0 0 0 / 9%)] first-letter:capitalize"
                  {...(chip?.tooltip && { 'data-tip': `${chip?.tooltip}`, 'data-for': `accessChip` })}
                >
                  {chip?.name}
                </div>
                <ReactTooltip id={`accessChip`} effect="solid" role="tooltip" type="dark" className="forceTooltipBg" />

                {aggregatedChip && (
                  <div
                    tw="ml-1 text-xs font-medium text-gray-600 rounded bg-white bg-opacity-80 px-2 py-1 [box-shadow:0px 4px 15px rgb(0 0 0 / 9%)] first-letter:capitalize"
                    {...(cardFormat === 'showObjType' && {
                      'data-tip': `I'm ${chip} of`,
                      'data-for': `roleChip${href}`,
                    })}
                  >
                    {/* {t('publications.aggregated')} */}
                    Aggregated
                  </div>
                )}
                <ReactTooltip
                  id={`roleChip${href}`}
                  effect="solid"
                  role="tooltip"
                  type="dark"
                  className="forceTooltipBg"
                />
              </>
            )}

            {/* Use for general text chips*/}
            {textChips?.length > 0 &&
              textChips?.map((item, i) => (
                <React.Fragment key={i}>
                  <div
                    tw="ml-1 text-xs font-medium text-gray-600 rounded bg-white bg-opacity-80 px-2 py-1 [box-shadow:0px 4px 15px rgb(0 0 0 / 9%)] first-letter:capitalize"
                    {...(item?.tooltip && {
                      'data-tip': `${item?.tooltip}`,
                      'data-for': `textChip-${i}`,
                    })}
                  >
                    {item?.name}
                  </div>
                  <ReactTooltip
                    id={`textChip-${i}`}
                    effect="solid"
                    role="tooltip"
                    type="dark"
                    className="forceTooltipBg"
                  />
                </React.Fragment>
              ))}
          </div>
        </div>
      )}
      {/* Card content */}
      <div
        css={[
          tw`flex flex-col flex-1 h-[238px]`,
          !props.noPadding && tw`p-3`,
          imgUrl && tw`justify-between border-0 border-t border-gray-300 border-solid`,
        ]}
        {...props}
      >
        {children}
      </div>
    </div>
  );
};
export default ObjectCard;
