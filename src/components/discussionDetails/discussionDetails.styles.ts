import styled from '@emotion/styled';
import { Flex } from 'antd';

export const ContainerStyled = styled(Flex)`
  position: relative;
  height: 100%;

  @media screen and (min-width: ${({ theme }) => theme.token.screenSM}px) {
    height: calc(100vh - 120px);
  }
`;
