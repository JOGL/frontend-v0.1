import { ChannelVisibility } from '~/__generated__/types';

export interface FormValues {
  icon_key: string;
  title: string;
  description: string;
  visibility: ChannelVisibility;
  auto_join: boolean;
  content_entities_created_by_any_member: boolean;
  comments_created_by_any_member: boolean;
  mention_everyone_by_any_member: boolean;
  members_invited_by_any_member: boolean;
}
