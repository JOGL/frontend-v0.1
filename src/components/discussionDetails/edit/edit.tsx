import useTranslation from 'next-translate/useTranslation';
import { Form, Row, Col, Select, Input, Typography, Button, Radio, Flex, Space, Checkbox } from 'antd';
import { ChannelExtendedModel, ChannelModel, ChannelUpsertModel } from '~/__generated__/types';
import { ICONS_LIST, MANAGEMENT_OPTIONS, MANAGEMENT_PROPERTIES } from '../../discussions/createForm/createForm.utils';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import api from '~/src/utils/api/api';
import { message } from 'antd';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { getDiscussionIcon } from '../../discussions/discussionItem/discussionItem.utils';
import { FormValues } from './edit.types';

interface Props {
  discussion: ChannelExtendedModel | ChannelModel;
  onClose: () => void;
}

const DiscussionEditView = ({ discussion, onClose }: Props) => {
  const { t } = useTranslation('common');
  const queryClient = useQueryClient();
  const [form] = Form.useForm();

  const visibility = Form.useWatch('visibility', form);

  const managementInitialValues = MANAGEMENT_PROPERTIES.reduce((acc, option) => {
    if (discussion.management.includes(option)) {
      acc[option] = true;
    } else {
      acc[option] = false;
    }
    return acc;
  }, {});

  const managementOptions = MANAGEMENT_OPTIONS.map((option) => ({
    label: t(`${option}`),
    value: option === 'everyone',
  }));

  const updateDiscussion = useMutation({
    mutationKey: [MUTATION_KEYS.channelUpdate],
    mutationFn: async (data: ChannelUpsertModel) => {
      const response = await api.channels.channelsUpdateDetail(discussion.id, data);
      return response.data;
    },
    onSuccess: (data) => {
      message.success(t('channelWasUpdated'));
      queryClient.invalidateQueries({ queryKey: [QUERY_KEYS.feedNodesList] });
      queryClient.invalidateQueries({ queryKey: [QUERY_KEYS.channelDetails] });
      onClose();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const onFinish = (values: FormValues) => {
    const {
      content_entities_created_by_any_member,
      comments_created_by_any_member,
      mention_everyone_by_any_member,
      members_invited_by_any_member,
      ...rest
    } = values;

    const managementValues = {
      content_entities_created_by_any_member,
      comments_created_by_any_member,
      mention_everyone_by_any_member,
      members_invited_by_any_member,
    };

    const management = Object.keys(managementValues).filter((key) => managementValues[key]);
    updateDiscussion.mutate({ ...rest, management });
  };

  return (
    <Form
      form={form}
      layout="vertical"
      onFinish={onFinish}
      requiredMark="optional"
      initialValues={{
        icon_key: discussion.icon_key,
        title: discussion.title,
        description: discussion.description,
        visibility: discussion.visibility,
        auto_join: discussion.auto_join,
        ...managementInitialValues,
      }}
    >
      <Row gutter={16}>
        <Col xs={{ span: 5 }} sm={{ span: 4 }}>
          <Form.Item name="icon_key" label={t('icon')} rules={[{ required: true }]}>
            <Select
              options={ICONS_LIST.map((icon) => ({ value: icon, label: getDiscussionIcon(icon) }))}
              optionRender={(option) => getDiscussionIcon(option.value as string)}
            />
          </Form.Item>
        </Col>
        <Col xs={{ span: 19 }} sm={{ span: 20 }}>
          <Form.Item name="title" label={t('name')} rules={[{ required: true, message: t('error.requiredField') }]}>
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Form.Item name="description" label={t('description')}>
        <Input />
      </Form.Item>
      <Form.Item
        name="visibility"
        label={t('privacySettings')}
        rules={[{ required: true, message: t('error.requiredField') }]}
      >
        <Radio.Group>
          <Space direction="vertical">
            <Radio value="private">
              <Flex vertical>
                <Typography.Text>{t('private')}</Typography.Text>
                <Typography.Text type="secondary">{t('privateDiscussionDescription')}</Typography.Text>
              </Flex>
            </Radio>
            <Radio value="open">
              <Flex vertical>
                <Typography.Text>{t('open')}</Typography.Text>
                <Typography.Text type="secondary">{t('openDiscussionDescription')}</Typography.Text>
              </Flex>
            </Radio>
          </Space>
        </Radio.Group>
      </Form.Item>
      {visibility === 'open' && (
        <Row>
          <Col offset={2}>
            <Form.Item name="auto_join" valuePropName="checked">
              <Checkbox>
                <Flex vertical>
                  <Typography.Text>{t('enableAutoJoin')}</Typography.Text>
                  <Typography.Text type="secondary">{t('enableAutoJoinDescription')}</Typography.Text>
                </Flex>
              </Checkbox>
            </Form.Item>
          </Col>
        </Row>
      )}
      <Form.Item name="content_entities_created_by_any_member" label={t('whoCanPost')} required>
        <Select options={managementOptions} />
      </Form.Item>
      <Form.Item name="comments_created_by_any_member" label={t('whoCanReply')} required>
        <Select options={managementOptions} />
      </Form.Item>
      <Form.Item name="mention_everyone_by_any_member" label={t('whoCanMentionEveryone')} required>
        <Select options={managementOptions} />
      </Form.Item>
      <Form.Item name="members_invited_by_any_member" label={t('whoCanAddPeople')} required>
        <Select options={managementOptions} />
      </Form.Item>
      <Row justify="end" gutter={16}>
        <Col>
          <Button type="default" onClick={onClose}>
            {t('action.cancel')}
          </Button>
        </Col>
        <Col>
          <Button type="primary" htmlType="submit">
            {t('action.save')}
          </Button>
        </Col>
      </Row>
    </Form>
  );
};

export default DiscussionEditView;
