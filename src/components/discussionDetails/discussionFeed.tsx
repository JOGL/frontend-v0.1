import { Flex, TabsProps, theme } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { FC, useState } from 'react';
import { ChannelDetailModel, ContentEntityModel } from '~/__generated__/types';
import { BadgeStyled, TabsStyled } from './discussionFeed.styles';
import { useFeedPermissions } from '../discussionDetails/discussionFeed.hooks';
import DiscussionFiles from './files/files';
import { ThreadChat } from '../discussionFeed/chat/threadChat/threadChat';
import { useRouter } from 'next/router';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { usePathname } from 'next/navigation';
import { FeedChat } from '../discussionFeed/chat/feedChat/feedChat';
import { SearchModel } from '~/src/types/searchModel';
import DiscussionMenu from './discussionMenu';

interface Props {
  discussion: ChannelDetailModel;
  unreadThreads: number;
  unreadMentions: number;
}

const { useToken } = theme;

export const DiscussionFeed: FC<Props> = ({ discussion, unreadThreads, unreadMentions }) => {
  const { t } = useTranslation('common');
  const { token } = useToken();
  const [selectedTab, setSelectedTab] = useState<string>('all');
  const { canPost, canDeletePosts, canComment, canDeleteComments, canMentionEveryone } = useFeedPermissions(
    discussion.id
  );
  const router = useRouter();
  const pathname = usePathname();

  const { data: selectedPost } = useQuery({
    queryKey: [QUERY_KEYS.postDetails, router.query.id, router.query.postId],
    queryFn: async () => {
      if (!router.query.postId) return undefined;

      const response = await api.feed.contentEntitiesDetailDetail(router.query.postId as string);
      return response.data;
    },
    enabled: !!router.query.postId,
  });

  const handleSelectPost = (post: ContentEntityModel | undefined) => {
    if (post)
      router.replace(
        {
          query: { ...router.query, postId: post.id },
        },
        undefined,
        { shallow: true, scroll: false }
      );
    else
      router.replace(
        {
          pathname,
        },
        undefined,
        { shallow: true, scroll: false }
      );
  };

  const items: TabsProps['items'] = [
    {
      key: 'all',
      label: t('allItems', { item: t('post.other') }),
      children: (
        <FeedChat
          feedId={discussion.id}
          canMentionEveryone={canMentionEveryone}
          canComment={canComment}
          canDeletePosts={canDeletePosts}
          handleSelectPost={handleSelectPost}
          canPost={canPost}
          queryKey={QUERY_KEYS.feedAllPostsList}
          loadData={async (feedId: string, search: SearchModel) => {
            const response = await api.feed.postsListDetail(feedId, { Page: search.page, PageSize: search.pageSize });
            return response.data;
          }}
        />
      ),
    },
    {
      key: 'threads',
      label: (
        <Flex gap="small" align="center">
          {t('reply.other')}
          {unreadThreads > 0 && <BadgeStyled count={unreadThreads} color={token.colorPrimary} />}
        </Flex>
      ),
      children: (
        <FeedChat
          feedId={discussion.id}
          canMentionEveryone={canMentionEveryone}
          canComment={canComment}
          canDeletePosts={canDeletePosts}
          handleSelectPost={handleSelectPost}
          canPost={false}
          tabName="threads"
          queryKey={QUERY_KEYS.feedThreadsList}
          loadData={async (feedId: string, search: SearchModel) => {
            const response = await api.feed.threadsListDetail(feedId, { Page: search.page, PageSize: search.pageSize });
            return response.data;
          }}
        />
      ),
    },
    {
      key: 'mentions',

      label: (
        <Flex gap="small" align="center">
          {t('mention.other')}
          {unreadMentions > 0 && <BadgeStyled count={unreadMentions} color={token.colorPrimary} />}
        </Flex>
      ),
      children: (
        <FeedChat
          feedId={discussion.id}
          canMentionEveryone={canMentionEveryone}
          canComment={canComment}
          canDeletePosts={canDeletePosts}
          handleSelectPost={handleSelectPost}
          canPost={false}
          tabName="mentions"
          queryKey={QUERY_KEYS.feedMentionsList}
          loadData={async (feedId: string, search: SearchModel) => {
            const response = await api.feed.mentionsListDetail(feedId, {
              Page: search.page,
              PageSize: search.pageSize,
            });
            return response.data;
          }}
        />
      ),
    },
    { key: 'files', label: t('file.other'), children: <DiscussionFiles discussion={discussion} /> },
  ];

  if (selectedPost) {
    return (
      <ThreadChat
        postId={selectedPost.id}
        feedId={discussion.id}
        canMentionEveryone={canMentionEveryone}
        canComment={canComment}
        canDeletePosts={canDeletePosts}
        canDeleteComments={canDeleteComments}
        goBack={() => handleSelectPost(undefined)}
      />
    );
  }

  return (
    <TabsStyled
      activeKey={selectedTab}
      onTabClick={(key) => setSelectedTab(key)}
      items={items}
      tabBarExtraContent={<DiscussionMenu discussion={discussion} />}
    />
  );
};
