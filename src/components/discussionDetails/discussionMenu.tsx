import { Button, Dropdown, Flex, Modal, message } from 'antd';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import useTranslation from 'next-translate/useTranslation';
import { MoreOutlined, SettingOutlined } from '@ant-design/icons';
import { ChannelDetailModel, Permission } from '~/__generated__/types';
import type { MenuProps } from 'antd';
import { useState } from 'react';
import api from '~/src/utils/api/api';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import Settings from './settings/settings';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { useRouter } from 'next/router';

interface Props {
  discussion: ChannelDetailModel;
}

const DiscussionMenu = ({ discussion }: Props) => {
  const { t } = useTranslation('common');
  const queryClient = useQueryClient();

  const [modal, contextHolder] = Modal.useModal();
  const [showSettingsModal, setShowSettingsModal] = useState(false);
  const canManage = !!discussion?.permissions.includes(Permission.Manage);
  const router = useRouter();

  const leaveDiscussion = useMutation({
    mutationKey: [MUTATION_KEYS.channelMemberLeave],
    mutationFn: async (id: string) => {
      await api.channels.membersLeaveCreate(id);
    },
    onSuccess: () => {
      message.success(t('youLeftDiscussion'));
      router.push('/');
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.feedNodesList],
      });
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.nodesDetailDetail],
      });
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.workspaceChannelsList],
      });
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.nodeChannelsList],
      });
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const deleteDiscussion = useMutation({
    mutationKey: [MUTATION_KEYS.channelDelete],
    mutationFn: async (id: string) => {
      await api.channels.channelsDeleteDetail(id);
    },
    onSuccess: () => {
      message.success(t('discussionWasDeleted'));
      router.push('/');
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.feedNodesList],
      });
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.workspaceChannelsList],
      });
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.nodeChannelsList],
      });
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const showLeaveWarning = () => {
    modal.confirm({
      title: t('areYouSure'),
      content: t('thisActionIsIrreversible'),
      onOk: () => leaveDiscussion.mutate(discussion.id),
      okText: t('action.leave'),
      cancelText: t('action.cancel'),
      okButtonProps: { loading: leaveDiscussion.isPending },
    });
  };

  const showDeleteWarning = () => {
    modal.warning({
      title: t('areYouSure'),
      content: t('deleteDiscussionWarning'),
      onOk: () => deleteDiscussion.mutate(discussion.id),
      okButtonProps: { loading: deleteDiscussion.isPending },
      closable: true,
    });
  };

  const menuItems: MenuProps['items'] = [
    { label: t('settings'), key: 'settings', onClick: () => setShowSettingsModal(true) },
    {
      label: 'Leave discussion',
      key: 'leave-discussion',
      danger: true,
      onClick: () => showLeaveWarning(),
    },
    ...(canManage
      ? [{ label: t('deleteDiscussion'), key: 'delete', danger: true, onClick: () => showDeleteWarning() }]
      : []),
  ];

  const menuProps = {
    items: menuItems,
  };

  return (
    <div>
      <Flex gap="small">
        <Dropdown menu={menuProps}>
          <Button icon={<MoreOutlined />} />
        </Dropdown>
      </Flex>
      {showSettingsModal && (
        <Modal open title={t('addApp')} onCancel={() => setShowSettingsModal(false)} footer={null}>
          <Settings discussion={discussion} onClose={() => setShowSettingsModal(false)} />
        </Modal>
      )}
      {contextHolder}
    </div>
  );
};

export default DiscussionMenu;
