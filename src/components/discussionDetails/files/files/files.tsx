import { Typography, Flex, Spin } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { DocumentFilter, SortKey } from '~/__generated__/types';
import { useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { RightOutlined } from '@ant-design/icons';
import FilesGrid from '../filesGrid/filesGrid';
import { LinkButtonStyled } from '../files.styles';

const PAGE_SIZE = 6;

interface Props {
  discussionId: string;
  search: string;
  onSeeAll: () => void;
}

const DiscussionFilesFiles = ({ discussionId, search, onSeeAll }: Props) => {
  const { t } = useTranslation('common');

  const { data: files, isPending } = useQuery({
    queryKey: [
      QUERY_KEYS.channelDocumentsList,
      discussionId,
      { type: DocumentFilter.File, search, PageSize: PAGE_SIZE },
    ],
    queryFn: async () => {
      const response = await api.channels.documentsDetail(discussionId, {
        type: DocumentFilter.File,
        Search: search,
        PageSize: PAGE_SIZE,
        SortKey: SortKey.Date,
        SortAscending: false,
      });
      return response.data;
    },
  });

  if (isPending && !files && !search) return <Spin />;

  return (
    <Flex vertical gap="middle">
      <Flex justify="space-between" gap="large">
        <Typography.Text strong>{t('file.other')}</Typography.Text>
        {Number(files?.total) > PAGE_SIZE && (
          <LinkButtonStyled type="link" icon={<RightOutlined />} iconPosition="end" onClick={onSeeAll}>
            {t('action.seeAll')}
          </LinkButtonStyled>
        )}
      </Flex>
      <FilesGrid files={files?.items} isLoading={isPending} />
    </Flex>
  );
};

export default DiscussionFilesFiles;
