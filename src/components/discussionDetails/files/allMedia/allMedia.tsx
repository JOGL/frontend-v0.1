import { Flex, Spin } from 'antd';
import { LeftOutlined } from '@ant-design/icons';
import useTranslation from 'next-translate/useTranslation';
import { DocumentFilter, SortKey } from '~/__generated__/types';
import { useInfiniteQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import MediaGrid from '../mediaGrid/mediaGrid';
import { AllWrapperStyled, ButtonLoadMoreStyled, LinkButtonStyled } from '../files.styles';

const PAGE_SIZE = 32;

interface Props {
  discussionId: string;
  search: string;
  onBack: () => void;
}

const DiscussionFilesAllMedia = ({ discussionId, search, onBack }: Props) => {
  const { t } = useTranslation('common');

  const { data, isPending, hasNextPage, fetchNextPage, isFetchingNextPage } = useInfiniteQuery({
    queryKey: [
      QUERY_KEYS.channelDocumentsList,
      discussionId,
      { type: DocumentFilter.Media, search, PageSize: PAGE_SIZE },
    ],
    queryFn: async ({ pageParam = 1 }) => {
      const response = await api.channels.documentsDetail(discussionId, {
        type: DocumentFilter.Media,
        Search: search,
        PageSize: PAGE_SIZE,
        Page: pageParam,
        SortKey: SortKey.Date,
        SortAscending: false,
      });
      return response.data;
    },
    initialPageParam: 1,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItemsCount = allPages.reduce((acc, page) => acc + page.items.length, 0);
      return loadedItemsCount < lastPage.total ? allPages.length + 1 : undefined;
    },
  });

  if (isPending && !data) return <Spin />;
  const media =
    data?.pages.flatMap((page) => {
      return page.items;
    }) || [];

  return (
    <Flex vertical gap="large">
      <LinkButtonStyled type="link" icon={<LeftOutlined />} onClick={onBack}>
        {t('action.back')}
      </LinkButtonStyled>
      <AllWrapperStyled>
        <MediaGrid media={media} isLoading={isPending} />
        {hasNextPage && (
          <ButtonLoadMoreStyled loading={isFetchingNextPage} onClick={() => fetchNextPage()}>
            {t('action.load')}
          </ButtonLoadMoreStyled>
        )}
      </AllWrapperStyled>
    </Flex>
  );
};

export default DiscussionFilesAllMedia;
