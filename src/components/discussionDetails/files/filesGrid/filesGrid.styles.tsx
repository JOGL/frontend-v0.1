import styled from '@emotion/styled';

export const FilesGridStyled = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: ${({ theme }) => theme.space[4]};
  grid-row-gap: ${({ theme }) => theme.space[4]};
`;
