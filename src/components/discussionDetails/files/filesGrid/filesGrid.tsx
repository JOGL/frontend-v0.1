import File from '~/src/components/file/file';
import { DocumentModel } from '~/__generated__/types';
import { Empty } from 'antd';
import { FilesGridStyled } from './filesGrid.styles';
import useTranslation from 'next-translate/useTranslation';

interface Props {
  files?: DocumentModel[];
  isLoading: boolean;
}

const FilesGrid = ({ files, isLoading }: Props) => {
  const { t } = useTranslation('common');
  if (!files?.length && !isLoading) {
    return <Empty description={t('noFiles')} />;
  }

  return (
    <FilesGridStyled>
      {files?.map((fileItem) => {
        return <File key={fileItem.id} file={fileItem} />;
      })}
    </FilesGridStyled>
  );
};

export default FilesGrid;
