import MediaImage from '~/src/components/media/mediaImage/mediaImage';
import MediaVideo from '~/src/components/media/mediaVideo/mediaVideo';
import MediaAudio from '~/src/components/media/mediaAudio/mediaAudio';
import { isAudioFileType, isImageFileType, isVideoFileType } from '~/src/utils/utils';
import { DocumentModel } from '~/__generated__/types';
import { Empty } from 'antd';
import { MediaGridStyled } from './mediaGrid.styles';
import useTranslation from 'next-translate/useTranslation';

interface Props {
  media?: DocumentModel[];
  isLoading: boolean;
}

const MediaGrid = ({ media, isLoading }: Props) => {
  const { t } = useTranslation('common');
  if (!media?.length && !isLoading) {
    return <Empty description={t('noMedia')} />;
  }

  return (
    <MediaGridStyled>
      {media?.map((mediaItem) => {
        if (isImageFileType(mediaItem.file_type)) {
          return <MediaImage media={mediaItem} />;
        }
        if (isVideoFileType(mediaItem.file_type)) {
          return <MediaVideo media={mediaItem} />;
        }
        if (isAudioFileType(mediaItem.file_type)) {
          return <MediaAudio media={mediaItem} />;
        }
      })}
    </MediaGridStyled>
  );
};

export default MediaGrid;
