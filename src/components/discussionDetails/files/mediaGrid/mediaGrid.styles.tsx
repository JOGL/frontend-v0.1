import styled from '@emotion/styled';

export const MediaGridStyled = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, 130px);
  align-items: center;
  grid-column-gap: ${({ theme }) => theme.space[8]};
  grid-row-gap: ${({ theme }) => theme.space[4]};
`;
