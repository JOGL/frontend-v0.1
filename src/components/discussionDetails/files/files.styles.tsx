import styled from '@emotion/styled';
import { Button, Flex, Input } from 'antd';

export const ButtonLoadMoreStyled = styled(Button)`
  position: absolute;
  bottom: 0px;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const AllWrapperStyled = styled.div`
  position: relative;
`;

export const LinkButtonStyled = styled(Button)`
  align-self: flex-start;
  padding: 0;
`;

export const ChatWrapperStyled = styled(Flex)`
  height: 100%;
`;

export const FeedChatListWrapperStyled = styled.div`
  overflow-y: auto;
`;

export const SearchStyled = styled(Input.Search)`
  max-width: ${({ theme }) => theme.sm};
`;
