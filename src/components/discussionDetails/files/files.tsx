import { useState } from 'react';
import { Flex, Input } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { ChannelDetailModel } from '~/__generated__/types';
import useDebounce from '~/src/hooks/useDebounce';
import DiscussionFilesAllMedia from './allMedia/allMedia';
import DiscussionFileAllFiles from './allFiles/allFiles';
import DiscussionFilesMedia from './media/media';
import DiscussionFilesFiles from './files/files';
import { ScrollbarFullWidthStyled } from '~/src/assets/styles/Global.styled';
import { ChatWrapperStyled, FeedChatListWrapperStyled, SearchStyled } from './files.styles';

interface Props {
  discussion: ChannelDetailModel;
}

const DiscussionFiles = ({ discussion }: Props) => {
  const { t } = useTranslation('common');
  const [search, setSearch] = useState('');
  const debouncedSearch = useDebounce(search, 300);
  const [selectedView, setSelectedView] = useState<'main' | 'allMedia' | 'allFiles'>('main');

  return (
    <ScrollbarFullWidthStyled>
      <ChatWrapperStyled vertical gap="large">
        <SearchStyled
          placeholder={t('action.search')}
          onChange={(e) => {
            setSearch(e.target.value);
          }}
          onSearch={(value) => {
            setSearch(value);
          }}
        />
        <FeedChatListWrapperStyled>
          {selectedView === 'main' && (
            <Flex vertical gap="large">
              <DiscussionFilesMedia
                discussionId={discussion.id}
                search={debouncedSearch}
                onSeeAll={() => setSelectedView('allMedia')}
              />
              <DiscussionFilesFiles
                discussionId={discussion.id}
                search={debouncedSearch}
                onSeeAll={() => setSelectedView('allFiles')}
              />
            </Flex>
          )}
          {selectedView === 'allMedia' && (
            <DiscussionFilesAllMedia
              discussionId={discussion.id}
              search={search}
              onBack={() => setSelectedView('main')}
            />
          )}
          {selectedView === 'allFiles' && (
            <DiscussionFileAllFiles
              discussionId={discussion.id}
              search={search}
              onBack={() => setSelectedView('main')}
            />
          )}
        </FeedChatListWrapperStyled>
      </ChatWrapperStyled>
    </ScrollbarFullWidthStyled>
  );
};

export default DiscussionFiles;
