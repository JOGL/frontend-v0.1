import { Typography, Flex, Spin } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { DocumentFilter, SortKey } from '~/__generated__/types';
import { useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { RightOutlined } from '@ant-design/icons';
import MediaGrid from '../mediaGrid/mediaGrid';
import { LinkButtonStyled } from '../files.styles';

const PAGE_SIZE = 8;

interface Props {
  discussionId: string;
  search: string;
  onSeeAll: () => void;
}

const DiscussionFilesMedia = ({ discussionId, search, onSeeAll }: Props) => {
  const { t } = useTranslation('common');

  const { data: media, isPending } = useQuery({
    queryKey: [
      QUERY_KEYS.channelDocumentsList,
      discussionId,
      { type: DocumentFilter.Media, search, PageSize: PAGE_SIZE },
    ],
    queryFn: async () => {
      const response = await api.channels.documentsDetail(discussionId, {
        type: DocumentFilter.Media,
        Search: search,
        PageSize: PAGE_SIZE,
        SortKey: SortKey.Date,
        SortAscending: false,
      });
      return response.data;
    },
  });

  if (isPending && !media && !search) return <Spin />;

  return (
    <Flex vertical gap="large">
      <Flex justify="space-between" gap="large">
        <Typography.Text strong>{t('media')}</Typography.Text>
        {Number(media?.total) > PAGE_SIZE && (
          <LinkButtonStyled type="link" icon={<RightOutlined />} iconPosition="end" onClick={onSeeAll}>
            {t('action.seeAll')}
          </LinkButtonStyled>
        )}
      </Flex>
      <MediaGrid media={media?.items} isLoading={isPending} />
    </Flex>
  );
};

export default DiscussionFilesMedia;
