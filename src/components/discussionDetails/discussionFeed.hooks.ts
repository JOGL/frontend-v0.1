import { useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { Permission } from '~/__generated__/types';
import { useMemo } from 'react';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import useUser from '~/src/hooks/useUser';

export function useFeedPermissions(feedId: string) {
  const { user } = useUser();

  const { data: feedPermissions } = useQuery({
    queryKey: [QUERY_KEYS.feedPermissions, feedId],
    queryFn: async () => {
      const response = await api.feed.permissionsDetail(feedId);
      return response.data;
    },
    enabled: !!feedId && !!user,
  });

  const { canPost, canDeletePosts, canComment, canDeleteComments, canMentionEveryone } = useMemo(
    () => ({
      canPost: !!feedPermissions?.includes(Permission.Postcontententity),
      canDeletePosts: !!feedPermissions?.includes(Permission.Deletecontententity),
      canComment: !!feedPermissions?.includes(Permission.Postcomment),
      canDeleteComments: !!feedPermissions?.includes(Permission.Deletecomment),
      canMentionEveryone: !!feedPermissions?.includes(Permission.Mentioneveryone),
    }),
    [feedPermissions]
  );

  return {
    canPost,
    canDeletePosts,
    canComment,
    canDeleteComments,
    canMentionEveryone,
  };
}
