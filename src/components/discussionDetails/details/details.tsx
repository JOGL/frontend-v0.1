import { Space, Typography, Divider } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { ChannelExtendedModel, ChannelModel } from '~/__generated__/types';
import Trans from 'next-translate/Trans';
import DiscussionDetailsItem from './detailsItem';

interface Props {
  discussion: ChannelExtendedModel | ChannelModel;
}

const DiscussionDetails = ({ discussion }: Props) => {
  const { t } = useTranslation('common');

  return (
    <div>
      {discussion.description && (
        <>
          <Space direction="vertical">
            <Typography.Text type="secondary">{t('description')}</Typography.Text>
            <Typography.Text>{discussion.description}</Typography.Text>
          </Space>
          <Divider />
        </>
      )}
      <Space direction="vertical">
        <Typography.Text type="secondary">{t('privacySettings')}</Typography.Text>
        <DiscussionDetailsItem>
          <Typography.Text>
            <Trans
              i18nKey={`common:discussionPrivacy.${discussion.visibility}`}
              components={[<b key={discussion.id} />]}
            />
          </Typography.Text>
        </DiscussionDetailsItem>
        {discussion.visibility === 'open' && (
          <DiscussionDetailsItem>
            <Typography.Text>
              <Trans i18nKey={`common:autoJoin.${discussion.auto_join}`} components={[<b key={discussion.id} />]} />
            </Typography.Text>
          </DiscussionDetailsItem>
        )}
      </Space>
      <Divider />
      <Space direction="vertical">
        <Typography.Text type="secondary">{t('permissions')}</Typography.Text>
        <DiscussionDetailsItem>
          <Typography.Text>
            <Trans
              i18nKey={`common:discussionPermission.content_entities_created_by_any_member.${discussion.management.includes(
                'content_entities_created_by_any_member'
              )}`}
              components={[<b key={discussion.id} />]}
            />
          </Typography.Text>
        </DiscussionDetailsItem>
        <DiscussionDetailsItem>
          <Typography.Text>
            <Trans
              i18nKey={`common:discussionPermission.comments_created_by_any_member.${discussion.management.includes(
                'comments_created_by_any_member'
              )}`}
              components={[<b key={discussion.id} />]}
            />
          </Typography.Text>
        </DiscussionDetailsItem>
        <DiscussionDetailsItem>
          <Typography.Text>
            <Trans
              i18nKey={`common:discussionPermission.mention_everyone_by_any_member.${discussion.management.includes(
                'mention_everyone_by_any_member'
              )}`}
              components={[<b key={discussion.id} />]}
            />
          </Typography.Text>
        </DiscussionDetailsItem>
        <DiscussionDetailsItem>
          <Typography.Text>
            <Trans
              i18nKey={`common:discussionPermission.members_invited_by_any_member.${discussion.management.includes(
                'members_invited_by_any_member'
              )}`}
              components={[<b key={discussion.id} />]}
            />
          </Typography.Text>
        </DiscussionDetailsItem>
      </Space>
    </div>
  );
};

export default DiscussionDetails;
