import { Flex } from 'antd';
import { CheckOutlined } from '@ant-design/icons';
import { ReactNode } from 'react';

interface Props {
  children: ReactNode;
}

const DiscussionDetailsItem = ({ children }: Props) => {
  return (
    <Flex gap="small">
      <CheckOutlined />
      {children}
    </Flex>
  );
};

export default DiscussionDetailsItem;
