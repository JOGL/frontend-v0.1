import { message, Modal, Form, Select } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useMutation } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import { FeedIntegrationType } from '~/__generated__/types';
import { useState } from 'react';
import { AxiosError } from 'axios';

const CREATE_APP_FORM = 'create-app-form';

export interface AppFormFields {
  type: FeedIntegrationType;
}

interface Props {
  discussionId: string;
  onClose: () => void;
  onAddApp: () => void;
}

const DiscussionAddAIApp = ({ discussionId, onClose, onAddApp }: Props) => {
  const { t } = useTranslation('common');
  const [buttonDisabled, setButtonDisabled] = useState(true);
  const [form] = Form.useForm<AppFormFields>();
  const options = [{ label: 'Publication-aware LLM', value: FeedIntegrationType.Joglagentpublication }];

  // const requiredRule = {
  //   required: true,
  //   message: t('error.requiredField'),
  // };

  const addApp = useMutation({
    mutationKey: [MUTATION_KEYS.feedIntegrationAdd, discussionId],
    mutationFn: async (data: AppFormFields) => {
      const response = await api.feed.integrationsCreate(discussionId, {
        type: data.type,
        source_id: 'Agent',
      });
      return response.data;
    },
    onSuccess: () => {
      message.success(t('appWasAddedToTheDiscussion'));
      onAddApp();
      onClose();
    },
    onError: (error: AxiosError) => {
      switch (error.response?.status) {
        case 409:
          message.error(t('error.feedIntegrationAlreadyExists'));
          break;
        default:
          message.error(t('error.somethingWentWrong'));
          break;
      }
    },
  });

  return (
    <Modal
      open
      title={t('action.addApp')}
      okButtonProps={{ htmlType: 'submit', form: CREATE_APP_FORM, disabled: buttonDisabled }}
      okText={t('action.create')}
      onCancel={onClose}
    >
      <Form
        form={form}
        layout="vertical"
        id={CREATE_APP_FORM}
        onFinish={(data) => addApp.mutate(data)}
        onFieldsChange={() => {
          setButtonDisabled(form.getFieldError('type').length !== 0);
        }}
      >
        <Form.Item name="type" label={t('type')} required>
          <Select
            showSearch={true}
            options={options}
            filterOption={(inputValue, option) => option?.label.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
          />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default DiscussionAddAIApp;
