import { Typography, Flex, Avatar, Button, Modal, message } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { FeedIntegrationModel, FeedIntegrationType } from '~/__generated__/types';
import { useMutation } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import { DeleteOutlined } from '@ant-design/icons';
import { displayObjectRelativeDate } from '~/src/utils/utils';
import { useRouter } from 'next/router';

interface Props {
  app: FeedIntegrationModel;
  canEdit: boolean;
  updateAppsList: () => void;
}

const getAppIcon = (app: FeedIntegrationModel) => {
  switch (app.type) {
    case FeedIntegrationType.Github:
      return '/images/discussionApps/github-mark.svg';
    case FeedIntegrationType.Huggingface:
      return '/images/discussionApps/hf-logo.svg';
    case FeedIntegrationType.Arxiv:
      return '/images/discussionApps/arxiv-logomark-small.svg';
    case FeedIntegrationType.Pubmed:
      return '/images/discussionApps/US-NLM-PubMed-Logo.svg';
    case FeedIntegrationType.Joglagentpublication:
      return '/images/discussionApps/ai-logo.svg';
    default:
      return '';
  }
};

const DiscussionApp = ({ app, updateAppsList }: Props) => {
  const { t } = useTranslation('common');
  const { locale } = useRouter();
  const [modal, contextHolder] = Modal.useModal();

  const removeApp = useMutation({
    mutationKey: [MUTATION_KEYS.feedIntegrationDelete],
    mutationFn: async (appId: string) => {
      await api.feed.integrationsDeleteDetail(appId);
    },
    onSuccess: () => {
      message.success(t('appWasRemovedFromDiscussion'));
      updateAppsList();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const showRemoveWarning = () => {
    modal.warning({
      title: t('areYouSure'),
      content: t('appRemoveWarning'),
      onOk: () => removeApp.mutate(app.id),
      okButtonProps: { loading: removeApp.isPending },
      closable: true,
    });
  };

  return (
    <Flex align="center" justify="space-between" gap="small">
      <Flex gap={8} align="center" flex={'auto 1 auto'}>
        <Avatar src={getAppIcon(app)} />
        <Flex vertical>
          <Typography.Text strong>{`${app.source_id}`}</Typography.Text>
        </Flex>
      </Flex>
      <Flex gap={60} align="center" flex={'auto 1 auto'}>
        <Typography.Text type="secondary">
          {t(`lastActivity`)} {displayObjectRelativeDate(app.last_activity, locale ?? 'en', false, false)}
        </Typography.Text>
        <Button icon={<DeleteOutlined />} onClick={showRemoveWarning} />
      </Flex>
      {contextHolder}
    </Flex>
  );
};

export default DiscussionApp;
