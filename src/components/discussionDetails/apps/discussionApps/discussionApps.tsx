import { useState } from 'react';
import { Typography, Flex, Input, Spin, Modal } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { ChannelExtendedModel, ChannelModel, FeedIntegrationType, Permission } from '~/__generated__/types';
import { useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import useDebounce from '~/src/hooks/useDebounce';
import AppMenu from '../discussionAppMenu/discussionAppMenu';
import DiscussionAddPRApp from '../discussionAddPRApp/discussionAddPRApp';
import DiscussionAddPaperApp from '../discussionAddPaperApp/discussionAddPaperApp';
import DiscussionApp from '../discussionApp/discussionApp';
import AddAIApp from '../discussionAddAIApp/discussionAddAIApp';

interface Props {
  discussion: ChannelModel | ChannelExtendedModel;
}

const DiscussionApps = ({ discussion }: Props) => {
  const { t } = useTranslation('common');
  const [showAddPRAppsModal, setShowAddPRAppsModal] = useState(false);
  const [showAddPubmedAppsModal, setShowAddPubmedAppsModal] = useState(false);
  const [showAddArxivAppsModal, setShowAddArxivAppsModal] = useState(false);
  const [showAddAIAppsModal, setShowAddAIAppsModal] = useState(false);
  const [search, setSearch] = useState('');
  const debouncedSearch = useDebounce(search, 300);
  const { data, isPending, refetch } = useQuery({
    queryKey: [QUERY_KEYS.feedIntegrations, discussion.id, { search: debouncedSearch }],
    queryFn: async () => {
      const response = await api.feed.integrationsDetail(discussion.id, { Search: debouncedSearch });
      return response.data;
    },
  });

  const canManageApps = !!discussion?.permissions.includes(Permission.Manage);

  return (
    <Flex vertical gap="large">
      <Flex justify="space-between" align="center" gap="medium">
        <Typography.Text type="secondary">{t('appsCount', { count: data?.length || 0 })}</Typography.Text>
        {canManageApps && (
          <AppMenu
            onAddPR={() => {
              setShowAddPRAppsModal(true);
            }}
            onAddArxiv={() => {
              setShowAddArxivAppsModal(true);
            }}
            onAddPubmed={() => {
              setShowAddPubmedAppsModal(true);
            }}
            onAddAI={() => {
              setShowAddAIAppsModal(true);
            }}
          />
        )}
      </Flex>
      <Input
        placeholder={t('searchApps')}
        onChange={(e) => {
          setSearch(e.target.value);
        }}
      />
      <Spin spinning={isPending}>
        <Flex vertical gap="small">
          {data?.map((app) => {
            return (
              <DiscussionApp key={app.id} discussionId={discussion.id} app={app} updateAppsList={() => refetch()} />
            );
          })}
        </Flex>
      </Spin>

      {showAddPRAppsModal && (
        <Modal open title={t('addApp')} onCancel={() => setShowAddPRAppsModal(false)} footer={null}>
          <DiscussionAddPRApp
            discussionId={discussion.id}
            onClose={() => setShowAddPRAppsModal(false)}
            onAddApp={() => refetch()}
          />
        </Modal>
      )}

      {showAddArxivAppsModal && (
        <Modal open title={t('addApp')} onCancel={() => setShowAddArxivAppsModal(false)} footer={null}>
          <DiscussionAddPaperApp
            discussionId={discussion.id}
            type={FeedIntegrationType.Arxiv}
            source_url="https://arxiv.org/category_taxonomy"
            onClose={() => setShowAddArxivAppsModal(false)}
            onAddApp={() => refetch()}
          />
        </Modal>
      )}

      {showAddPubmedAppsModal && (
        <Modal open title={t('addApp')} onCancel={() => setShowAddPubmedAppsModal(false)} footer={null}>
          <DiscussionAddPaperApp
            discussionId={discussion.id}
            type={FeedIntegrationType.Pubmed}
            source_url="https://www.nlm.nih.gov/mesh/meshhome.html"
            onClose={() => setShowAddPubmedAppsModal(false)}
            onAddApp={() => refetch()}
          />
        </Modal>
      )}

      {showAddAIAppsModal && (
        <Modal open title={t('addAI')} onCancel={() => setShowAddAIAppsModal(false)} footer={null}>
          <AddAIApp
            discussionId={discussion.id}
            onClose={() => setShowAddAIAppsModal(false)}
            onAddApp={() => refetch()}
          />
        </Modal>
      )}
    </Flex>
  );
};

export default DiscussionApps;
