import { AutoComplete, message, Modal, Form } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useMutation, useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import { FeedIntegrationType } from '~/__generated__/types';
import { useState } from 'react';
import { RuleObject } from 'antd/es/form';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';

const CREATE_APP_FORM = 'create-app-form';

export interface AppFormFields {
  option: string;
}

interface Props {
  discussionId: string;
  type: FeedIntegrationType;
  source_url: string;
  onClose: () => void;
  onAddApp: () => void;
}
const DiscussionAddPaperApp = ({ discussionId, type, source_url, onClose, onAddApp }: Props) => {
  const { t } = useTranslation('common');
  const [buttonDisabled, setButtonDisabled] = useState(true);
  const [form] = Form.useForm<AppFormFields>();
  const { data: options } = useQuery({
    queryKey: [QUERY_KEYS.feedIntegrationOptions],
    queryFn: async () => {
      const response = await api.feed.integrationsOptionsDetailDetail(type);
      return response.data;
    },
  });

  const requiredRule = {
    required: true,
    message: t('error.requiredField'),
  };

  const addApp = useMutation({
    mutationKey: [MUTATION_KEYS.feedIntegrationAdd, discussionId],
    mutationFn: async (data: AppFormFields) => {
      const response = await api.feed.integrationsCreate(discussionId, {
        source_id: data.option,
        source_url: source_url,
        type: type,
      });
      return response.data;
    },
    onSuccess: () => {
      message.success(t('appWasAddedToTheDiscussion'));
      onAddApp();
      onClose();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  return (
    <Modal
      open
      title={t('action.addApp')}
      okButtonProps={{ htmlType: 'submit', form: CREATE_APP_FORM, disabled: buttonDisabled }}
      okText={t('action.create')}
      onCancel={onClose}
    >
      <Form
        form={form}
        layout="vertical"
        id={CREATE_APP_FORM}
        onFinish={(data) => addApp.mutate(data)}
        onFieldsChange={() => {
          setButtonDisabled(form.getFieldError('option').length !== 0);
        }}
      >
        <Form.Item
          name="option"
          label={t('category')}
          required
          rules={[
            requiredRule,
            {
              validator: (_: RuleObject, option: string): Promise<void | string> => {
                if (!options?.find((o) => o === option)) return Promise.reject();

                return Promise.resolve();
              },
            },
          ]}
        >
          <AutoComplete
            options={options?.map((o) => ({ value: o }))}
            filterOption={(inputValue, option) => option?.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
          />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default DiscussionAddPaperApp;
