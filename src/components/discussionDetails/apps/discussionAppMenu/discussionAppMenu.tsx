import { Button, Dropdown, Space } from 'antd';
import useTranslation from 'next-translate/useTranslation';

import { BulbOutlined, PullRequestOutlined, ReadOutlined } from '@ant-design/icons';

interface Props {
  onAddPR: () => void;
  onAddArxiv: () => void;
  onAddPubmed: () => void;
  onAddAI: () => void;
}
const AppMenu = ({ onAddPR, onAddArxiv, onAddPubmed, onAddAI }: Props) => {
  const { t } = useTranslation('common');

  const menuItems = [
    {
      label: (
        <Space>
          <PullRequestOutlined />
          {t('pullRequests')}
        </Space>
      ),
      key: 'pr',
      onClick: onAddPR,
    },
    {
      label: (
        <Space>
          <ReadOutlined />
          ARXIV
        </Space>
      ),
      key: 'arxiv',
      onClick: onAddArxiv,
    },
    {
      label: (
        <Space>
          <ReadOutlined />
          PubMed
        </Space>
      ),
      key: 'pubmed',
      onClick: onAddPubmed,
    },
    {
      label: (
        <Space>
          <BulbOutlined />
          {t('ai')}
        </Space>
      ),
      key: 'ai',
      onClick: onAddAI,
    },
  ];

  return (
    <Dropdown
      placement="bottomRight"
      trigger={['click']}
      menu={{
        items: menuItems,
      }}
    >
      <Button>{t('action.add')}</Button>
    </Dropdown>
  );
};

export default AppMenu;
