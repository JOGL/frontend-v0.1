import { Input, message, Modal, Form, Button, Flex, Alert } from 'antd';
import { LinkOutlined, KeyOutlined } from '@ant-design/icons';
import useTranslation from 'next-translate/useTranslation';
import { useMutation } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import { RuleObject } from 'antd/es/form';
import { FeedIntegrationType } from '~/__generated__/types';
import { useState } from 'react';
import { GitHubLogin } from 'react-github-login';
import { AxiosError } from 'axios';
import { useValidateIntegrationUrl } from '~/src/hooks/useValidateIntegrationUrl';

const CREATE_APP_FORM = 'create-app-form';

export interface AppFormFields {
  url: string;
}

interface Props {
  discussionId: string;
  onClose: () => void;
  onAddApp: () => void;
}

interface GitHubLoginResponse {
  code: string;
}

const DiscussionAddPRApp = ({ discussionId, onClose, onAddApp }: Props) => {
  const { t } = useTranslation('common');
  const [buttonDisabled, setButtonDisabled] = useState(true);
  const [errorMessage, setErrorMessage] = useState<string>();
  const [showGithubButton, setShowGithubButton] = useState(false);
  const [accessToken, setAccessToken] = useState<string>();
  const [form] = Form.useForm<AppFormFields>();
  const { parseFeedIntegration } = useValidateIntegrationUrl();

  const requiredRule = {
    required: true,
    message: t('error.requiredField'),
  };

  const onGithubSuccess = (response: GitHubLoginResponse) => {
    setAccessToken(response.code);
  };

  const onGithubFailure = () => {
    message.error(t('error.somethingWentWrong'));
  };

  const addApp = useMutation({
    mutationKey: [MUTATION_KEYS.feedIntegrationAdd, discussionId],
    mutationFn: async (data: AppFormFields) => {
      const integrationData = parseFeedIntegration(data.url);
      if (!integrationData) return;

      const response = await api.feed.integrationsCreate(discussionId, {
        ...integrationData,
        access_token: accessToken,
      });
      return response.data;
    },
    onSuccess: () => {
      message.success(t('appWasAddedToTheDiscussion'));
      onAddApp();
      onClose();
    },
    onError: (error: AxiosError, data) => {
      switch (error.response?.status) {
        case 404:
          const feedIntegration = parseFeedIntegration(data.url);
          switch (feedIntegration?.type) {
            case FeedIntegrationType.Github:
              if (!accessToken) {
                setErrorMessage(t('error.feedIntegrationValidationFailedAuthorizable'));
                setShowGithubButton(true);
              } else {
                setErrorMessage(t('error.feedIntegrationValidationFailed'));
              }
              break;
            default:
              setErrorMessage(t('error.feedIntegrationValidationFailed'));
              break;
          }
          break;
        case 409:
          message.error(t('error.feedIntegrationAlreadyExists'));
          break;
        default:
          message.error(t('error.somethingWentWrong'));
          break;
      }
    },
  });

  return (
    <Modal
      open
      title={t('action.addApp')}
      okButtonProps={{ htmlType: 'submit', form: CREATE_APP_FORM, disabled: buttonDisabled }}
      okText={t('action.create')}
      onCancel={onClose}
    >
      <Form
        form={form}
        layout="vertical"
        id={CREATE_APP_FORM}
        onFinish={(data) => addApp.mutate(data)}
        onFieldsChange={() => {
          setButtonDisabled(form.getFieldError('url').length !== 0);
        }}
      >
        <Form.Item
          name="url"
          label={t('url')}
          required
          rules={[
            requiredRule,
            {
              validator: (_: RuleObject, url: string): Promise<void | string> => {
                if (!url) return Promise.resolve();
                if (!parseFeedIntegration(url)) return Promise.reject(t('error.invalidAppURL'));

                return Promise.resolve();
              },
            },
          ]}
        >
          <Input
            onChange={() => errorMessage && setErrorMessage('')}
            addonBefore={<LinkOutlined />}
            placeholder={t('urlPlaceholder')}
          />
        </Form.Item>
      </Form>
      <Flex vertical align="flex-start" justify="flex-start" gap="large">
        <>
          {errorMessage && <Alert type="error" message={errorMessage} />}
          {showGithubButton === true && (
            <GitHubLogin
              clientId={process.env.GITHUB_CLIENT_ID}
              redirectUri={process.env.GITHUB_REDIRECT_URL}
              scope="repo"
              onSuccess={onGithubSuccess}
              onFailure={onGithubFailure}
            >
              <Button icon={<KeyOutlined />}>{t('authorize')}</Button>
            </GitHubLogin>
          )}
        </>
      </Flex>
    </Modal>
  );
};

export default DiscussionAddPRApp;
