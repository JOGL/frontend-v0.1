import { ChannelDetailModel } from '~/__generated__/types';
import { DiscussionFeed } from './discussionFeed';
import { useMemo } from 'react';
import { ContainerStyled } from './discussionDetails.styles';

interface Props {
  discussion: ChannelDetailModel;
}

const DiscussionDetails = ({ discussion }: Props) => {
  const { unreadThreads, unreadMentions } = useMemo(() => {
    return {
      unreadThreads: discussion.unread_threads,
      unreadMentions: discussion.unread_mentions,
    };
  }, [discussion]);

  return (
    <ContainerStyled>
      <DiscussionFeed discussion={discussion} unreadThreads={unreadThreads} unreadMentions={unreadMentions} />
    </ContainerStyled>
  );
};

export default DiscussionDetails;
