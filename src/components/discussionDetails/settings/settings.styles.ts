import styled from '@emotion/styled';
import { Tabs } from 'antd';

export const TabsStyled = styled(Tabs)`
  height: 100%;
  overflow: hidden auto;

  .ant-tabs-content-holder {
    height: 100%;
  }

  .ant-tabs-content {
    height: 100%;
  }

  .ant-tabs-tabpane {
    height: 100%;
  }
`;
