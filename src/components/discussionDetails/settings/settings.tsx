import useTranslation from 'next-translate/useTranslation';
import type { TabsProps } from 'antd';
import DiscussionDetailsGeneral from '../general/general';
import DiscussionMembers from '../members/members';
import { ChannelDetailModel } from '~/__generated__/types';
import DiscussionApps from '../apps/discussionApps/discussionApps';
import { TabsStyled } from './settings.styles';

interface Props {
  discussion: ChannelDetailModel;
  onClose: () => void;
}

const Settings = ({ discussion, onClose }: Props) => {
  const { t } = useTranslation('common');

  const tabs: TabsProps['items'] = [
    {
      key: 'general',
      label: t('general'),

      children: <DiscussionDetailsGeneral discussion={discussion} onClose={onClose} />,
    },
    {
      key: 'members',
      label: t('member.other'),
      children: <DiscussionMembers discussion={discussion} />,
    },
    { key: 'apps', label: t('app.other'), children: <DiscussionApps discussion={discussion} /> },
  ];

  return <TabsStyled defaultActiveKey="1" items={tabs} />;
};

export default Settings;
