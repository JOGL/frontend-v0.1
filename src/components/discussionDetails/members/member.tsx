import { Typography, Flex, Avatar, Button, Modal, Dropdown, message } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { AccessLevel, ChannelMemberUpsertModel, MemberModel } from '~/__generated__/types';
import { useMutation } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { MoreOutlined } from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import axios from 'axios';

interface Props {
  discussionId: string;
  member: MemberModel;
  canEdit: boolean;
  updateMembersList: () => void;
}

const DiscussionMember = ({ discussionId, member, canEdit, updateMembersList }: Props) => {
  const { t } = useTranslation('common');
  const isAdminOrOwner = member.access_level === 'admin' || member.access_level === 'owner';
  const [modal, contextHolder] = Modal.useModal();

  const removeMember = useMutation({
    mutationKey: [MUTATION_KEYS.channelMemberRemove],
    mutationFn: async (memberId: string) => {
      await api.channels.membersDelete(discussionId, [memberId]);
    },
    onSuccess: () => {
      message.success(t('memberWasRemovedFromDiscussion'));
      updateMembersList();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  const updateRole = useMutation({
    mutationKey: [MUTATION_KEYS.channelMemberRemove],
    mutationFn: async (data: ChannelMemberUpsertModel) => {
      await api.channels.membersUpdate(discussionId, [data]);
    },
    onSuccess: () => {
      message.success(t('memberRoleWasUpdated'));
      updateMembersList();
    },
    onError: (error) => {
      if (axios.isAxiosError(error) && error.response?.status === 424) {
        message.error(t('lastDiscussionAdminError'));
        return;
      }
      message.error(t('error.somethingWentWrong'));
    },
  });

  const shoRemoveWarning = () => {
    modal.warning({
      title: t('areYouSure'),
      content: t('userRemoveWarning'),
      onOk: () => removeMember.mutate(member.id),
      okButtonProps: { loading: removeMember.isPending },
      closable: true,
    });
  };

  const menuItems: MenuProps['items'] = [
    ...(isAdminOrOwner
      ? [{ label: t('removeAdminRole'), key: 'remove-admin-role' }]
      : [{ label: t('makeAdmin'), key: 'make-admin' }]),
    {
      label: t('removeFromDiscussion'),
      key: 'remove',
      danger: true,
    },
  ];

  const handleMenuClick: MenuProps['onClick'] = (e) => {
    if (e.key === 'remove-admin-role') {
      updateRole.mutate({ user_id: member.id, access_level: AccessLevel.Member });
    }
    if (e.key === 'make-admin') {
      updateRole.mutate({ user_id: member.id, access_level: AccessLevel.Admin });
    }
    if (e.key === 'remove') {
      shoRemoveWarning();
    }
  };

  const menuProps = {
    items: menuItems,
    onClick: handleMenuClick,
  };

  return (
    <Flex align="center" justify="space-between" gap="small">
      <Flex gap={8} align="center" flex={'auto 1 auto'}>
        <Avatar src={member.logo_url} />
        <Flex vertical>
          <Typography.Text strong>{`${member.first_name} ${member.last_name}`}</Typography.Text>
          <Typography.Text type="secondary">{`@${member.username}`}</Typography.Text>
        </Flex>
      </Flex>
      <Flex gap="middle" align="center">
        {isAdminOrOwner && (
          <Typography.Text type="secondary">{t(`legacy.role.${member.access_level}`)}</Typography.Text>
        )}
        {canEdit && (
          <Dropdown menu={menuProps}>
            <Button icon={<MoreOutlined />} />
          </Dropdown>
        )}
      </Flex>
      {contextHolder}
    </Flex>
  );
};

export default DiscussionMember;
