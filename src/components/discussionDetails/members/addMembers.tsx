import { useState } from 'react';
import { Typography, Row, Col, Flex, Avatar, Input, Spin, Empty, Checkbox, Button, message } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import useTranslation from 'next-translate/useTranslation';
import { useMutation, useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { MUTATION_KEYS } from '~/src/utils/api/mutationKeys';
import useDebounce from '~/src/hooks/useDebounce';
import type { CheckboxProps } from 'antd';
import { MembersStyled } from './addMembers.styles';
import { AccessLevel } from '~/__generated__/types';

interface Props {
  discussionId: string;
  onClose: () => void;
  onAddMembers: () => void;
}

const DiscussionAddMembers = ({ discussionId, onClose, onAddMembers }: Props) => {
  const { t } = useTranslation('common');
  const [selectedMembers, setSelectedMembers] = useState<string[]>([]);
  const [search, setSearch] = useState('');
  const debouncedSearch = useDebounce(search, 300);

  const { data, isPending } = useQuery({
    queryKey: [QUERY_KEYS.channelUsersList, discussionId, debouncedSearch],
    queryFn: async () => {
      const response = await api.channels.usersDetail(discussionId, { Search: debouncedSearch });
      return response.data;
    },
  });

  const addMembers = useMutation({
    mutationKey: [MUTATION_KEYS.channelMembersAdd, discussionId],
    mutationFn: async (data: string[]) => {
      const usersToAdd = data.map((id) => ({ user_id: id, access_level: AccessLevel.Member }));
      const response = await api.channels.membersCreate(discussionId, usersToAdd);
      return response.data;
    },
    onSuccess: () => {
      message.success(t('membersWereAddedToTheDiscussion'));
      onAddMembers();
      onClose();
    },
    onError: () => {
      message.error(t('error.somethingWentWrong'));
    },
  });

  if (!isPending && !data) return <Empty />;

  const onCheckAllChange: CheckboxProps['onChange'] = (e) => {
    setSelectedMembers(e.target.checked && data ? data.map((member) => member.id) : []);
  };

  return (
    <Flex vertical gap="large">
      <Input
        placeholder={t('searchMembers')}
        onChange={(e) => {
          setSearch(e.target.value);
        }}
      />
      <Spin spinning={isPending}>
        {data?.length ? (
          <Flex vertical gap="small">
            <Checkbox
              indeterminate={selectedMembers.length > 0 && selectedMembers.length < (data?.length || 0)}
              checked={selectedMembers.length === data?.length}
              onChange={onCheckAllChange}
            >
              {t('selectAll')}
            </Checkbox>
            <MembersStyled vertical gap="small">
              {data?.map((member) => {
                const isChecked = !!selectedMembers.includes(member.id);
                return (
                  <Row key={member.id}>
                    <Col span={18}>
                      <Checkbox
                        checked={isChecked}
                        onChange={(e) => {
                          if (e.target.checked) {
                            setSelectedMembers((prev) => [...prev, member.id]);
                          } else {
                            setSelectedMembers((prev) => [...prev.filter((id) => id !== member.id)]);
                          }
                        }}
                      >
                        {
                          <Flex gap="small" align="center" flex={'auto 1 auto'}>
                            <Avatar icon={<UserOutlined />} src={member.logo_url} />
                            <Flex vertical>
                              <Typography.Text strong>{member.first_name}</Typography.Text>
                              <Typography.Text type="secondary">{`@${member.username}`}</Typography.Text>
                            </Flex>
                          </Flex>
                        }
                      </Checkbox>
                    </Col>
                  </Row>
                );
              })}
            </MembersStyled>
          </Flex>
        ) : (
          <Empty description={t('noMembersFound')} />
        )}
      </Spin>
      <Flex align="center" justify="end" gap="small">
        <Button onClick={() => onClose()}>{t('action.cancel')}</Button>
        <Button
          type="primary"
          loading={addMembers.isPending}
          onClick={() => addMembers.mutate(selectedMembers)}
          disabled={!selectedMembers.length}
        >
          {t('action.add')}
        </Button>
      </Flex>
    </Flex>
  );
};

export default DiscussionAddMembers;
