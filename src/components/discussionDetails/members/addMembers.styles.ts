import styled from '@emotion/styled';
import { Flex } from 'antd';

export const MembersStyled = styled(Flex)`
  max-height: 300px;
  overflow-y: auto;
`;
