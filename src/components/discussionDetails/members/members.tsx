import { useState } from 'react';
import { Typography, Flex, Input, Spin, Button, Modal } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { ChannelDetailModel, Permission } from '~/__generated__/types';
import { useQuery } from '@tanstack/react-query';
import api from '~/src/utils/api/api';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import useDebounce from '~/src/hooks/useDebounce';
import { UsergroupAddOutlined } from '@ant-design/icons';
import DiscussionAddMembers from './addMembers';
import DiscussionMember from './member';

interface Props {
  discussion: ChannelDetailModel;
}

const DiscussionMembers = ({ discussion }: Props) => {
  const { t } = useTranslation('common');
  const [showAddMembersModal, setShowAddMembersModal] = useState(false);
  const [search, setSearch] = useState('');
  const debouncedSearch = useDebounce(search, 300);
  const { data, isPending, refetch } = useQuery({
    queryKey: [QUERY_KEYS.channelMembersList, discussion.id, { search: debouncedSearch }],
    queryFn: async () => {
      const response = await api.channels.membersDetail(discussion.id, { Search: debouncedSearch });
      return response.data;
    },
  });
  const isAdminOrOwner = !!discussion?.permissions.includes(Permission.Manage);
  const canAddMember = !!discussion?.permissions.includes(Permission.Invitemembers);

  return (
    <Flex vertical gap="large">
      <Flex justify="space-between" align="center" gap="medium">
        <Typography.Text type="secondary">{t('membersCount', { count: data?.length || 0 })}</Typography.Text>
        {canAddMember && (
          <Button icon={<UsergroupAddOutlined />} onClick={() => setShowAddMembersModal(true)}>
            {t('addMembers')}
          </Button>
        )}
      </Flex>
      <Input
        placeholder={t('searchMembers')}
        onChange={(e) => {
          setSearch(e.target.value);
        }}
      />
      <Spin spinning={isPending}>
        <Flex vertical gap="small">
          {data?.map((member) => {
            return (
              <DiscussionMember
                key={member.id}
                discussionId={discussion.id}
                member={member}
                canEdit={isAdminOrOwner}
                updateMembersList={() => refetch()}
              />
            );
          })}
        </Flex>
      </Spin>
      {showAddMembersModal && (
        <Modal open title={t('addMembers')} onCancel={() => setShowAddMembersModal(false)} footer={null}>
          <DiscussionAddMembers
            discussionId={discussion.id}
            onClose={() => setShowAddMembersModal(false)}
            onAddMembers={() => refetch()}
          />
        </Modal>
      )}
    </Flex>
  );
};

export default DiscussionMembers;
