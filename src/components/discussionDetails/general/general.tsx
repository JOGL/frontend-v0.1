import { ChannelDetailModel, Permission } from '~/__generated__/types';
import DiscussionEditView from '../edit/edit';
import DiscussionDetails from '../details/details';

interface Props {
  discussion: ChannelDetailModel;
  onClose: () => void;
}

const DiscussionDetailsGeneral = ({ discussion, onClose }: Props) => {
  const isAdminOrOwner = !!discussion?.permissions.includes(Permission.Manage);

  if (isAdminOrOwner) return <DiscussionEditView discussion={discussion} key={discussion.id} onClose={onClose} />;
  return <DiscussionDetails discussion={discussion} />;
};

export default DiscussionDetailsGeneral;
