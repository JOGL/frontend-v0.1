import styled from '@emotion/styled';
import { Avatar, Badge, Button, Flex, Tabs, Typography } from 'antd';

export const TabsStyled = styled(Tabs)`
  height: 100%;
  width: 100%;
  overflow: hidden;

  .ant-tabs-content-holder {
    height: 100%;
  }

  .ant-tabs-content {
    height: 100%;
  }

  .ant-tabs-tabpane {
    height: 100%;
  }

  .ProseMirror {
    font-size: 16px !important; /* Prevents iOS zoom */
    touch-action: manipulation;
    -webkit-tap-highlight-color: transparent;
    -webkit-text-size-adjust: none;
  }

  .tiptap {
    touch-action: manipulation;
    -webkit-tap-highlight-color: transparent;
  }

  .tiptap-editor-wrapper,
  .tiptap-content {
    touch-action: manipulation;
    -webkit-text-size-adjust: none;
  }

  .tiptap-menu-bar,
  .tiptap-floating-menu,
  .tiptap-bubble-menu {
    touch-action: manipulation;
    -webkit-tap-highlight-color: transparent;
  }
`;

export const BadgeStyled = styled(Badge)`
  .ant-badge-count {
    border-radius: ${({ theme }) => theme.token.borderRadiusSM}px;
    font-size: ${({ theme }) => theme.fontSizes['2xs']};
    font-weight: ${({ theme }) => theme.token.fontWeightStrong};
  }
`;

export const DiscussionEditorWrapper = styled.div`
  padding: ${({ theme }) => theme.token.paddingXXS}px;
  border: 1px solid ${({ theme }) => theme.token.colorBorder};
  border-radius: ${({ theme }) => theme.token.borderRadiusLG}px;
  box-shadow: ${({ theme }) => theme.token.boxShadowSecondary};
  max-height: 250px;
`;

export const DiscussionItemWrapper = styled(Flex)<{ isNew?: boolean }>`
  width: 100%;
  overflow-x: clip;
  overflow-y: visible;
  border-bottom: 1px solid ${({ theme }) => theme.token.colorSplit};
  padding-block: ${({ theme }) => theme.token.paddingContentVerticalLG}px;
  padding-inline: ${({ theme }) => theme.token.paddingXXS}px;
  background: ${({ isNew = false, theme }) => (isNew ? theme.token['purple-1'] : 'unset')};
`;

export const AvatarStyled = styled(Avatar)`
  flex-shrink: 0;

  img {
    width: 36px;
    height: 36px;
  }
`;

export const TextStyled = styled(Typography.Text)`
  font-size: ${({ theme }) => theme.token.fontSizeSM}px;
`;

export const DiscussionItemHeaderWrapper = styled(Flex)`
  width: 100%;
`;

export const DiscussionItemEditorWrapper = styled.div`
  width: 100%;
`;

export const AuthorButtonStyled = styled(Button)`
  padding: 0;
  height: auto;

  :hover {
    span {
      color: ${({ theme }) => theme.token.neutral8};
    }
  }
`;
export const TagButtonStyled = styled(Button)`
  font-size: ${({ theme }) => theme.fontSizes['2xs']};
  font-weight: ${({ theme }) => theme.token.fontWeightStrong};
`;
export const FlexStyled = styled(Flex)`
  overflow-x: hidden;
`;
