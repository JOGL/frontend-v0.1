import React, { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';
import { useApi } from '~/src/contexts/apiContext';
import { confAlert } from '~/src/utils/utils';

interface Props {
  id: string;
  score: number;
  max_score: number;
  callBack: any;
}

const ProposalScoreModal: FC<Props> = ({ id, score: scoreProp, max_score = 5, callBack }) => {
  const { t } = useTranslation('common');
  const api = useApi();
  const [score, setScore] = useState(scoreProp);
  const [sending, setSending] = useState(false);

  const handleChange = (e) => {
    let newScore = e.target.value === '' ? '' : parseFloat(e.target.value);
    newScore = newScore > max_score ? '' : newScore;
    setScore(newScore);
  };

  const handleSubmit = () => {
    setSending(true);
    api
      .post(`/proposals/${id}/score`, score)
      .then(() => {
        setSending(false);
        confAlert.fire({ icon: 'success', title: 'Score was given!' });
        callBack();
      })
      .catch(() => setSending(false));
  };

  return (
    <>
      <div className="content">
        <p>{t('chooseScoreThreshold', { score_threshold: max_score })}</p>
        <div tw="flex items-center w-full">
          <span tw="font-bold mr-2">{t('score')}</span>
          <input
            type="number"
            value={score}
            onChange={handleChange}
            className="form-control"
            tw="w-1/4 px-3 py-[0.4rem] rounded-md border-gray-300 focus:(ring-primary border-primary)"
            min={0}
            max={max_score}
          />
        </div>
      </div>
      <Button disabled={sending} onClick={handleSubmit} tw="mt-5 mb-2">
        {sending && <SpinLoader />}
        {t('action.save')}
      </Button>
    </>
  );
};

export default ProposalScoreModal;
