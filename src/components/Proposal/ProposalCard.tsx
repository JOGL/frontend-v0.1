import Link from 'next/link';
import React, { FC } from 'react';
import ObjectCard from '~/src/components/Cards/ObjectCard';
import H2 from '~/src/components/primitives/H2';
import Title from '~/src/components/primitives/Title';
import { Proposal, Cfp } from '~/src/types';
import useTranslation from 'next-translate/useTranslation';
import { displayObjectDate, showColoredStatus, proposalStatus } from '~/src/utils/utils';
import Loading from '../Tools/Loading';
import { TextChip } from '~/src/types/chip';
import tw from 'twin.macro';
import { EntityMiniModel } from '~/__generated__/types';

interface Props {
  proposal: Proposal;
  cfp?: Cfp;
  workspace?: EntityMiniModel;
  cardFormat?: string;
  textChips?: TextChip[];
  fromPage?: 'cfp';
}
const ProposalCard: FC<Props> = ({
  proposal,
  cfp = proposal.call_for_proposal,
  workspace = proposal.project,
  cardFormat,
  textChips,
  fromPage,
}) => {
  const { t } = useTranslation('common');
  const proposalUrl = `/proposal/${proposal?.id}`;

  return (
    <ObjectCard
      imgUrl={workspace?.banner_url || workspace?.logo_url || '/images/default/default-workspace.png'}
      href={proposalUrl}
      textChips={textChips}
      cardFormat={cardFormat}
    >
      {proposal && cfp ? (
        <>
          <div tw="inline-flex items-center">
            <Link href={proposalUrl} passHref legacyBehavior>
              <Title>
                <H2 tw="[word-break:break-word] text-[16px] items-center line-clamp-1">
                  {proposal?.title || t('myProposal')}
                </H2>
              </Title>
            </Link>
          </div>
          <Hr tw="mt-2 pt-1" />
          <div tw="h-28">
            <div tw="inline-flex flex-wrap text-sm gap-3">
              <div>
                {t('fromWorkspace')}:&nbsp;
                <Link
                  href={`/workspace/${workspace?.id}`}
                  passHref
                  tw="text-black font-medium underline hover:(text-black opacity-90) line-clamp-1"
                  title={workspace?.title || t('untitled')}
                >
                  {workspace?.title || t('untitled')}
                </Link>
              </div>

              {fromPage !== 'cfp' && (
                <div>
                  {t('forCallForProposals')}:&nbsp;
                  <span tw="font-bold">
                    <Link
                      href={`/cfp/${cfp?.id}`}
                      passHref
                      tw="text-black font-medium underline hover:(text-black opacity-80) line-clamp-1"
                      title={cfp?.title || t('untitled')}
                    >
                      {cfp?.title || t('untitled')}
                    </Link>
                  </span>
                </div>
              )}
            </div>
            <div tw="inline-flex flex-wrap text-sm">
              <span tw="font-bold mt-2">
                {showColoredStatus(
                  proposalStatus(proposal?.status, { start: cfp?.submissions_from, deadline: cfp?.submissions_to }),
                  t
                )}
              </span>
            </div>
          </div>

          {/* Stats */}
          {cardFormat !== 'inPosts' && (
            <>
              <Hr tw="pt-2 mt-5" />
              <div tw="items-center justify-around space-x-2 flex" css={cardFormat !== 'inPosts' ? tw`h-5` : ''}>
                {(proposal?.score ?? 0) > 0 && (
                  <CardData
                    value={cfp?.max_score ? `${proposal.score}/${cfp?.max_score}` : proposal.score}
                    title="Score"
                    cardFormat={cardFormat}
                  />
                )}
                {proposal.submitted_at && (
                  <CardData
                    value={displayObjectDate(proposal?.submitted_at, 'L', true)}
                    title={t('submissionDate')}
                    cardFormat={cardFormat}
                  />
                )}
              </div>
            </>
          )}
        </>
      ) : (
        <Loading />
      )}
    </ObjectCard>
  );
};

const CardData = ({ value, title, cardFormat }) => (
  <div tw="flex flex-col justify-center items-center text-[13px]">
    <div tw="font-bold line-clamp-1" css={cardFormat !== 'inPosts' ? tw`-mb-1` : ''}>
      {value}
    </div>
    <div tw="text-gray-500 font-medium line-clamp-1">{title}</div>
  </div>
);
const Hr = (props) => <div tw="-mx-4 border-0 border-t border-solid border-color[rgba(0, 0, 0, 0.07)]" {...props} />;

export default ProposalCard;
