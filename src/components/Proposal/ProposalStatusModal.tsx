import React, { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';
import { useApi } from '~/src/contexts/apiContext';
import { confAlert } from '~/src/utils/utils';
import FormDropdownComponent from '../Tools/Forms/FormDropdownComponent';

interface Props {
  id: string;
  status: string;
  callBack: any;
}

const ProposalStatusModal: FC<Props> = ({ id, status: statusProp, callBack }) => {
  const { t } = useTranslation('common');
  const api = useApi();
  const [sending, setSending] = useState(false);
  const [status, setStatus] = useState(statusProp);

  const handleChange = (key, content) => {
    setStatus(content);
  };

  const changeStatus = () => {
    api.post(`/proposals/${id}/status`, `"${status}"`).then(() => {
      confAlert.fire({ icon: 'success', title: `Successfully ${status} proposal` });
      callBack();
    });
  };

  return (
    <>
      <div className="content">
        <FormDropdownComponent
          id="status"
          type="proposalStatus"
          content={status}
          title={t('status')}
          options={['submitted', 'accepted', 'rejected']}
          warningMsg={t('draftWarningMessage')}
          onChange={handleChange}
        />
      </div>
      <Button disabled={sending} onClick={changeStatus} tw="mt-5 mb-2">
        {sending && <SpinLoader />}
        {t('action.save')}
      </Button>
    </>
  );
};

export default ProposalStatusModal;
