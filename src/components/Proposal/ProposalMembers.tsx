import Loading from '~/src/components/Tools/Loading';
import MemberDisplayMinimal from '../Members/MemberDisplayMinimal';

const ProposalMembers = ({ users }) => {
  return (
    <>
      {users.length !== 0 ? (
        <div tw="flex flex-wrap">
          {users.map((user) => (
            <MemberDisplayMinimal key={user.id} user={user} />
          ))}
        </div>
      ) : (
        <Loading />
      )}
    </>
  );
};
export default ProposalMembers;
