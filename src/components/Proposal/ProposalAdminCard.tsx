import React, { FC, ReactNode, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Proposal } from '~/src/types';
import { displayObjectDate, showColoredStatus, proposalStatus } from '~/src/utils/utils';
import ProposalScoreModal from './ProposalScoreModal';
import { useModal } from '~/src/contexts/modalContext';
import Alert from '~/src/components/Tools/Alert/Alert';
import { useApi } from '~/src/contexts/apiContext';
import ReactTooltip from 'react-tooltip';
import { Menu, MenuItem, MenuButton, MenuList } from '@reach/menu-button';
import Icon from '../primitives/Icon';
import Link from 'next/link';
import ProposalStatusModal from './ProposalStatusModal';
import SendEmailToJoglUserModal from '../Tools/SendEmailToJoglUserModal';

interface Props {
  proposal: Proposal;
  max_score: number;
  cfpDates: any;
  cfpId: string;
  callBack: () => void;
  hasScore: boolean;
}

const ProposalAdminCard: FC<Props> = ({ proposal, max_score, cfpDates, cfpId, hasScore, callBack }) => {
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const [sending, setSending] = useState<'remove' | 'accepted' | undefined>(undefined);
  const [error, setError] = useState<string | ReactNode>(undefined);
  const api = useApi();
  // const api = useApi();

  // const route = `/api/cfps/${cfpId}/proposals/${proposal.id}`;

  // const onSuccess = () => {
  //   setSending(undefined);
  //   callBack();
  // };

  // const onError = (err) => {
  //   console.error(err);
  //   setSending(undefined);
  //   setError(t('error.generic'));
  // };

  // const removeProposal = () => {
  //   setSending('remove');
  //   api
  //     .delete(route)
  //     .then(() => onSuccess())
  //     .catch((err) => onError(err));
  // };

  const revalidateAndCloseModal = () => {
    callBack();
    closeModal();
  };

  const sendMessageToProposalAdmins = (users) => {
    showModal({
      children: (
        <SendEmailToJoglUserModal
          containerType={'cfps'}
          containerId={cfpId}
          recipients={users}
          closeModal={closeModal}
        />
      ),

      title: t('action.sendEmail'),
    });
  };

  if (proposal) {
    return (
      <tr tw="border border-gray-500 md:border-none block md:table-row" key={proposal.id}>
        {/* Title */}
        <td tw="p-2 md:border md:border-gray-500 text-left block md:table-cell">
          <span tw="inline-block w-1/3 md:hidden font-bold">{t('title')}</span>{' '}
          {proposal.status !== 'draft' ? (
            <Link href={`/proposal/${proposal.id}`}>
              {proposal.title || t('untitledItem', { item: t('proposal.one') })}
            </Link>
          ) : (
            proposal.title || t('untitledItem', { item: t('proposal.one') })
          )}
        </td>
        {/* Status */}
        <td tw="p-2 md:border md:border-gray-500 text-left block md:table-cell">
          <span tw="inline-block w-1/3 md:hidden font-bold">{t('status')}: </span>
          {showColoredStatus(proposalStatus(proposal.status, cfpDates), t)}
        </td>
        {/* Submitted date */}
        <td tw="p-2 md:border md:border-gray-500 text-left block md:table-cell">
          <span tw="inline-block w-1/3 md:hidden font-bold">{t('submissionDate')}</span>
          {proposal.submitted_at ? displayObjectDate(proposal.submitted_at, 'L', true) : '-'}
        </td>
        {/* Funding */}
        {/* {hasGrant && (
          <td tw="p-2 md:border md:border-gray-500 text-left block md:table-cell">
            <span tw="inline-block w-1/3 md:hidden font-bold">{t('fundingRequested')}</span>
            {proposal.funding}€
          </td>
        )} */}
        {/* Score */}
        {hasScore && (
          <td tw="p-2 md:border md:border-gray-500 text-left block md:table-cell">
            <span tw="inline-block w-1/3 md:hidden font-bold">{t('score')}</span>
            {proposal?.score || '-'}/{max_score}
          </td>
        )}
        {/* Actions */}
        <td tw="p-2 md:border md:border-gray-500 text-left block md:table-cell">
          <Menu
            // show/hide tooltip on element focus/blur
            onFocus={(e) => ReactTooltip.show(e.target)}
            onBlur={(e) => ReactTooltip.hide(e.target)}
            tabIndex="0"
            data-tip={t('moreActions')}
            data-for="more_actions"
          >
            <MenuButton tw="text-[.8rem] h-[fit-content] text-gray-700 px-1 hover:bg-gray-300 rounded-sm">
              •••
            </MenuButton>
            <MenuList className="post-manage-dropdown" tw="divide-gray-300 divide-y">
              {/* Show button to give score only when proposal has been submitted, and we are after the deadline, and the peer review is not closed */}
              {proposal?.status !== 'draft' && hasScore && (
                // (cfpDates.stop
                //   ? !hasCfpDatePassed(cfpDates.stop)
                //   : hasCfpDatePassed(cfpDates.deadline)) && (
                <MenuItem
                  onSelect={() => {
                    showModal({
                      children: (
                        <ProposalScoreModal
                          max_score={max_score}
                          id={proposal?.id}
                          score={proposal?.score}
                          callBack={revalidateAndCloseModal}
                        />
                      ),
                      title: !proposal?.score ? t('giveScore') : t('changeScore'),
                    });
                  }}
                >
                  <Icon icon="mi:favorite" tw="w-5 h-5" />
                  {!proposal?.score ? t('giveScore') : t('changeScore')}
                </MenuItem>
              )}
              {proposal?.status !== 'draft' && new Date() > new Date(cfpDates.deadline) && (
                // <>
                //   <MenuItem onSelect={() => approveOrRejectProposal('accepted')}>
                //     <Icon icon="mdi:thumb-up-outline" tw="w-5 h-5" />
                //     Approve proposal
                //   </MenuItem>
                //   <MenuItem onSelect={() => approveOrRejectProposal('rejected')}>
                //     <Icon icon="mdi:thumb-down-outline" tw="w-5 h-5" />
                //     Reject proposal
                //   </MenuItem>
                // </>
                <MenuItem
                  onSelect={() => {
                    showModal({
                      children: (
                        <ProposalStatusModal
                          id={proposal?.id}
                          status={proposal?.status.toLowerCase()}
                          callBack={revalidateAndCloseModal}
                        />
                      ),
                      title: 'Manage status',
                    });
                  }}
                >
                  <Icon icon="mdi-light:thumbs-up-down" tw="w-5 h-5" />
                  {t('action.manageStatus')}
                </MenuItem>
              )}
              <MenuItem onSelect={() => sendMessageToProposalAdmins(proposal.users)}>
                <Icon icon="fa6-solid:envelope" tw="w-5 h-5" />
                {t('action.sendEmail')}
              </MenuItem>
              {/* {proposal?.status !== 'Submitted' && (
              <Button btnType="danger" disabled={sending === 'remove'} onClick={removeProposal}>
                {sending === 'remove' && <SpinLoader />}
                {t('action.remove')}
              </Button>
            )} */}
            </MenuList>
          </Menu>
          <ReactTooltip id="more_actions" delayHide={300} effect="solid" role="tooltip" />

          {error && <Alert type="danger" message={error} />}
        </td>
      </tr>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default ProposalAdminCard;
