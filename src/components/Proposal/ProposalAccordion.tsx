import React, { useState, useRef, useEffect } from 'react';

const ProposalAccordion = ({ allactive, title, children }) => {
  const [active, setActive] = useState(false);
  const contentRef = useRef(null);

  useEffect(() => {
    contentRef.current.style.maxHeight = active ? `50000px` : '0px';
  }, [contentRef, active]);

  useEffect(() => {
    setActive(allactive);
  }, [allactive]);

  const toogleActive = () => {
    setActive(!active);
  };

  return (
    <div className="accordion-section">
      <button className="accordion-title" tw="py-2 text-left" onClick={toogleActive}>
        <h4 tw="font-bold mb-0 text-[1.2rem]">{title}</h4>
        <span className={active ? 'accordion-icon rotate' : 'accordion-icon'}>
          <svg
            viewBox="0 0 16 16"
            height="25"
            width="25"
            focusable="false"
            role="img"
            fill="currentColor"
            xmlns="http://www.w3.org/2000/svg"
            className="css-1eamic5 ex0cdmw0"
          >
            <title>ChevronRight icon</title>
            <path
              fillRule="evenodd"
              d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"
            />
          </svg>
        </span>
      </button>

      <div ref={contentRef} className="accordion-content">
        {/* tw="md:max-w-[1000px]" */}
        {active && children}
      </div>
    </div>
  );
};

export default ProposalAccordion;
