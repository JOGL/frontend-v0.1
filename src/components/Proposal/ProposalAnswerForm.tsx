import { FC, useRef, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import FormWysiwygComponent from '../Tools/Forms/FormWysiwygComponent';
import BtnUploadFile from '../Tools/BtnUploadFile';
import { confAlert } from '~/src/utils/utils';
import Icon from '../primitives/Icon';
import DocViewer, { DocViewerRenderers } from '@cyntler/react-doc-viewer';
import { useModal } from '~/src/contexts/modalContext';
import mimetype2fa from '../Tools/createDocument/mimetypes';
import useGet from '~/src/hooks/useGet';

interface Props {
  answer: any;
  question: number;
  proposalId: string;
  onAnswerChange: (e, v, k) => void;
}

const ProposalAnswerForm: FC<Props> = ({ answer, question, proposalId, onAnswerChange }) => {
  const { t } = useTranslation('common');
  const [newAnswer, setNewAnswer] = useState(answer?.answer || []);
  const [uploadedDocumentId, setUploadedDocumentId] = useState(answer?.answer_document?.id);
  const { showModal } = useModal();
  const inputRef = useRef(null);

  const handleChange = (key, content) => {
    let newAnswerArr;

    if (question.type === 'paragraph' || question.type === 'single_choice') {
      newAnswerArr = content === '<p><br></p>' ? [] : [content];
    } else if (question.type === 'file_upload') {
      newAnswerArr = [''];
    } else {
      if (newAnswer.includes(content)) {
        newAnswerArr = newAnswer.filter((c) => c !== content);
      } else {
        newAnswerArr = [...newAnswer, content];
      }
    }

    setNewAnswer(newAnswerArr);
    onAnswerChange(
      answer ? answer?.question_key : question.key, // key of answer depending on if there was an existing answer
      newAnswerArr, // answer content (for the `answer` field)
      question.type === 'file_upload' ? content : '' // uploaded document id (for the `answer_document` field ; if not file upload, it's just empty)
    );
  };

  const DocumentViewer = () => {
    const { data: document } = useGet(
      (answer?.answer_document?.id || uploadedDocumentId) && `/proposals/${proposalId}/documents/${uploadedDocumentId}`
    );
    return (
      document && (
        <div tw="flex p-4 bg-gray-100 rounded-md border border-gray-200 w-full justify-center mt-4 items-center gap-4">
          <div tw="inline-flex text-[14px]">
            {mimetype2fa(document.file_type, true)}
            {document.title}
          </div>
          <Icon
            icon="ion:eye"
            tw="w-5 h-5 text-gray-600 hover:text-gray-900 cursor-pointer"
            onClick={() => {
              showModal({
                children: (
                  <DocViewer
                    documents={[{ uri: document.data, fileName: document.title }]}
                    pluginRenderers={DocViewerRenderers}
                  />
                ),
                title: 'Document',
                maxWidth: '30rem',
              });
            }}
          />
          <a href={document.data} download={document.title} target="_blank" rel="noopener noreferrer">
            <Icon icon="material-symbols:download" tw="w-5 h-5 text-gray-600 hover:text-gray-900 cursor-pointer" />
          </a>
        </div>
      )
    );
  };

  return (
    <form className="proposalForm">
      {question?.type === 'paragraph' ? (
        <>
          <FormWysiwygComponent
            id="content"
            title=""
            placeholder={t('writeAnswerHere')}
            content={newAnswer[0]}
            onChange={handleChange}
            maxChar={question.max_length}
          />
        </>
      ) : question?.type === 'single_choice' || question?.type === 'multiple_choice' ? (
        <div tw="space-y-1">
          {question?.choices.map((choice, i) => (
            <div key={i} tw="flex items-center">
              <input
                id={choice}
                name="choice"
                type={question?.type === 'single_choice' ? 'radio' : 'checkbox'}
                checked={newAnswer?.includes(choice)}
                onChange={(e) => handleChange('content', e.target.id)}
              />
              <label htmlFor={choice} tw="block text-sm font-medium leading-6 text-gray-900 cursor-pointer">
                {choice}
              </label>
            </div>
          ))}
        </div>
      ) : (
        <div tw="flex flex-col items-center justify-center w-full">
          <BtnUploadFile
            itemId={proposalId}
            itemType="proposals"
            singleFileOnly
            ref={inputRef}
            customCss={{ display: 'none' }}
            text={t('addOneOrMultipleFiles')}
            type="documents"
            uploadNow
            refresh={() => confAlert.fire({ icon: 'success', title: 'Successfully uploaded the document!' })}
            onFileSelected={(files) => inputRef?.current?.startUpload(files)}
            onChange={(result) => {
              if (result.error) confAlert.fire({ icon: 'error', title: 'Unknown error occured!' });
              handleChange('content', result.img);
              setUploadedDocumentId(result.img);
            }}
          />
          <div
            tw="flex flex-row gap-1 cursor-pointer py-7 px-4 bg-gray-100 rounded-md border border-gray-200 w-full justify-center items-center"
            onClick={(e) => {
              e.preventDefault();
              inputRef?.current?.click();
            }}
          >
            <Icon icon="material-symbols:upload" tw="w-5 h-5" />
            <span tw="underline text-[17px]">Upload a document</span>
          </div>
          <DocumentViewer />
        </div>
      )}
    </form>
  );
};

export default ProposalAnswerForm;
