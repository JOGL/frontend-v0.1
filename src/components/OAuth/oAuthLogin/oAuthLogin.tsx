import React, { FC, useState } from 'react';
import { GithubOutlined } from '@ant-design/icons';
import { ButtonStyled, OAuthContainerStyled } from './oAuthLogin.styles';
import api from '~/src/utils/api/api';
import { FeedIntegrationType } from '~/__generated__/types';
import useTranslation from 'next-translate/useTranslation';
import Loader from '../../Common/loader/loader';
import { FlexFullHeightStyled } from '../../Common/common.styles';
import { Avatar, Image } from 'antd';
import { QUERY_KEYS } from '~/src/utils/api/queryKeys';
import { useQuery } from '@tanstack/react-query';

interface PopupWindowOptions {
  width?: number;
  height?: number;
  left?: number;
  top?: number;
  [key: string]: number | undefined;
}

interface OAuthUser {
  login: string;
  name: string | null;
  avatar_url: string;
  picture: string;
}

const toQuery = (params: Record<string, string | number | undefined>, delimiter = '&'): string => {
  return Object.keys(params)
    .map((key) => {
      const value = params[key];
      if (value === undefined) {
        return '';
      }
      return `${key}=${encodeURIComponent(value.toString())}`;
    })
    .filter(Boolean)
    .join(delimiter);
};

class PopupWindow {
  private readonly id: string;
  private readonly url: string;
  private readonly options: PopupWindowOptions;
  private window: Window | null;
  private promise: Promise<Record<string, string>>;
  private _iid: number | null;

  public constructor(id: string, url: string, options: PopupWindowOptions = {}) {
    this.id = id;
    this.url = url;
    this.options = options;
    this.window = null;
    this._iid = null;
    this.promise = new Promise(() => {});
  }

  public open(): void {
    const { url, id, options } = this;
    this.window = window.open(url, id, toQuery(options, ','));
  }

  public close(): void {
    this.cancel();
    if (this.window) {
      this.window.close();
    }
  }

  public poll(): void {
    this.promise = new Promise((resolve, reject) => {
      this._iid = window.setInterval(() => {
        try {
          const popup = this.window;
          if (!popup || popup.closed !== false) {
            // Check localStorage when popup closes
            const code = localStorage.getItem('oauth_code');
            // Clean up localStorage
            localStorage.removeItem('oauth_code');
  
            if (code) {
              resolve({ code });
              this.close();
            }
            
            return;
          }
        } catch (error) {
          // Ignore cross-origin errors
        }
      }, 500);
    });
  }

  private cancel(): void {
    if (this._iid) {
      window.clearInterval(this._iid);
      this._iid = null;
    }
  }

  public then<TResult1 = Record<string, string>, TResult2 = never>(
    onfulfilled?: ((value: Record<string, string>) => TResult1 | PromiseLike<TResult1>) | null,
    onrejected?: ((reason: unknown) => TResult2 | PromiseLike<TResult2>) | null
  ): Promise<TResult1 | TResult2> {
    return this.promise.then(onfulfilled, onrejected);
  }

  public catch<TResult = never>(
    onrejected?: ((reason: unknown) => TResult | PromiseLike<TResult>) | null
  ): Promise<Record<string, string> | TResult> {
    return this.promise.catch(onrejected);
  }

  public static open(id: string, url: string, options: PopupWindowOptions): PopupWindow {
    const popup = new this(id, url, options);
    popup.open();
    popup.poll();
    return popup;
  }
}

const OAuthLogin: FC<{
  provider: FeedIntegrationType;
  accessToken?: string;
  onClick?: () => void;
  onLogin?: (accessToken: string) => void;
}> = (props) => {
  const [accessToken, setAccessToken] = useState(props.accessToken);
  const [loading, setLoading] = useState<boolean>(false);
  const { t } = useTranslation('common');

  const getProviderConfiguration = (provider: FeedIntegrationType) => {
    switch (provider) {
      case FeedIntegrationType.Github:
        return {
          clientId: process.env.GITHUB_CLIENT_ID,
          redirectUrl: process.env.GITHUB_REDIRECT_URL,
          scope: 'repo',
          authorizeUrl: 'https://github.com/login/oauth/authorize',
          userInfoUrl: 'https://api.github.com/user',
          prompt: t('github.connect'),
          icon: <GithubOutlined />,
        };
      case FeedIntegrationType.Huggingface:
        return {
          clientId: process.env.HUGGINGFACE_CLIENT_ID,
          redirectUrl: process.env.HUGGINGFACE_REDIRECT_URL,
          scope: 'read-repos',
          authorizeUrl: 'https://huggingface.co/oauth/authorize',
          userInfoUrl: 'https://huggingface.co/oauth/userinfo',
          prompt: t('huggingface.connect'),
          icon: (
            <Image preview={false} width={24} src="https://huggingface.co/front/assets/huggingface_logo-noborder.svg" />
          ),
        };
      default:
        return null;
    }
  };

  const config = getProviderConfiguration(props.provider);
  const login = async (): Promise<void> => {
    if (!config) {
      console.error(`oAuthLogin not configured for ${props.provider}`);
      return;
    }

    if (!config.clientId || !config.redirectUrl || !config.authorizeUrl) {
      console.error('Missing environment variables');
      return;
    }

    setLoading(true);
    try {
      const options: PopupWindowOptions = {
        width: 600,
        height: 700,
        left: window.screenX + (window.outerWidth - 600) / 2,
        top: window.screenY + (window.outerHeight - 700) / 2,
      };

      const url = `${config.authorizeUrl}?client_id=${config.clientId}&redirect_uri=${config.redirectUrl}&scope=${config.scope}&response_type=code&state=jogl`;
      const popup = PopupWindow.open(`${props.provider}-oauth`, url, options);
      const { code } = await popup;
      if (code) {
        // Exchange the code for an access token
        const response = await api.feed.integrationsTokenCreate({
          type: props.provider,
          authorization_code: code,
        });

        const token = await response.data;
        if (!token) {
          console.error('OAuth token exchange failed');
          return;
        }

        setAccessToken(token);
        props.onLogin(token);
      }
    } catch (error) {
      console.error('Authentication error:', error);
    } finally {
      setLoading(false);
    }
  };

  const { data: userData } = useQuery({
    queryKey: [QUERY_KEYS.usersSearchAutocompleteList, { provider: props.provider, token: accessToken }],
    queryFn: async () => {
      const config = getProviderConfiguration(props.provider);
      if (!config) return null;

      const userResponse = await fetch(config.userInfoUrl, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });

      return (await userResponse.json()) as OAuthUser;
    },
    enabled: !!accessToken,
  });

  if (!config) return <Loader />;

  return (
    <OAuthContainerStyled>
      {userData ? (
        <ButtonStyled icon={config.icon} onClick={props.onClick} loading={loading} size="large">
          <FlexFullHeightStyled gap="small" align="center">
            {userData.name ?? userData.login}
            <Avatar size={32} src={userData.avatar_url ?? userData.picture} alt="avatar" />
          </FlexFullHeightStyled>
        </ButtonStyled>
      ) : (
        <ButtonStyled type="primary" icon={config.icon} onClick={login} loading={loading} size="large">
          {loading ? t('loading') : config.prompt}
        </ButtonStyled>
      )}
    </OAuthContainerStyled>
  );
};

export default OAuthLogin;
