import styled from 'styled-components';
import { SpaceFullWidthStyled } from '~/src/components/Common/common.styles';
import { Button } from 'antd';

export const OAuthContainerStyled = styled(SpaceFullWidthStyled)`
  @media screen and (max-width: 480px) {
    width: unset;
  }
`;

export const ButtonStyled = styled(Button)`
  width: 280px;
`;
