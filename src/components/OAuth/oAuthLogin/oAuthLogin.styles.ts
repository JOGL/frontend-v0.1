import styled from "styled-components";
import { SpaceFullWidthStyled } from "../../Common/common.styles";
import { Button } from "antd";


export const OAuthContainerStyled = styled(SpaceFullWidthStyled)`
`;

export const ButtonStyled = styled(Button)`
  width:300px;
`;