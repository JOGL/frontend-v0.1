const Grid = (props) => (
  <div tw="grid grid-template-columns[repeat(auto-fill, 240px)] gap-[.8rem] justify-center" {...props} />
);

export default Grid;
