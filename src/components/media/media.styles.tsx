import styled from '@emotion/styled';
import { Skeleton } from 'antd';
import Image from 'next/image';

export const MediaWrapperStyled = styled.div`
  background-color: ${({ theme }) => theme.token.neutral5};
  width: 130px;
  height: 130px;
  border-radius: 16px;
  overflow: hidden;
  position: relative;
  cursor: pointer;
  * img {
    object-fit: cover;
  }

  * video {
    object-fit: cover;
  }
`;

export const SkeletonImageStyled = styled(Skeleton.Image)`
  width: 130px !important;
  height: 130px !important;
`;

export const ImageStyled = styled(Image)<{ objectFit: string }>`
  object-fit: ${({ objectFit }) => objectFit};
`;
