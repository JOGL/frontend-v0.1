import dayjs from 'dayjs';
import { ImageStyled, MediaWrapperStyled } from '../media.styles';
import { Flex, Typography } from 'antd';
import { DocumentModel } from '~/__generated__/types';
import { useState } from 'react';
import MediaPreview from '../mediaPreview/mediaPreview';

interface Props {
  media: DocumentModel;
  withPreview?: boolean;
}

const MediaImage = ({ media, withPreview = true }: Props) => {
  const [showPreview, setShowPreview] = useState(false);
  const userName = `${media.created_by?.first_name} ${media.created_by?.last_name}`;
  return (
    <>
      <Flex vertical gap="small" onClick={() => setShowPreview(true)}>
        <MediaWrapperStyled>
          <ImageStyled src={media.document_url} alt={media.file_name} width={130} height={130} objectFit="cover" />
        </MediaWrapperStyled>
        <Flex vertical>
          <Typography.Text strong ellipsis={{ tooltip: userName }}>
            {userName}
          </Typography.Text>
          <Typography.Text type="secondary">{dayjs(media.created).format('MMM Do')}</Typography.Text>
        </Flex>
      </Flex>
      {withPreview && showPreview && <MediaPreview media={media} onClose={() => setShowPreview(false)} />}
    </>
  );
};

export default MediaImage;
