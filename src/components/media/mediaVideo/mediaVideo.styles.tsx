import styled from '@emotion/styled';
import { CaretRightOutlined } from '@ant-design/icons';

export const CaretRightOutlinedStyled = styled(CaretRightOutlined)`
  position: absolute;
  font-size: 56px;
  color: ${({ theme }) => theme.token.colorBgContainer};
  opacity: 0.5;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;
