import { DocumentModel } from '~/__generated__/types';
import { useCallback, useState } from 'react';
import { Button, message } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useAuth } from '~/auth/auth';
import FilePreviewWrapper from '../../filePreviewWrapper/filePreviewWrapper';
import ReactPlayer from 'react-player';
import { isAudioFileType, isImageFileType, isVideoFileType } from '~/src/utils/utils';
import {
  ControlsContainer,
  ImageContainerStyled,
  ImageWrapperStyled,
  NavigationContainer,
} from './mediaPreview.styles';
import { ImageStyled } from '../media.styles';
import { LeftOutlined, RightOutlined, ZoomInOutlined, ZoomOutOutlined } from '@ant-design/icons';
import { DisplayableAttachment } from '../../Feed/types';
import { Grid } from 'antd';
const { useBreakpoint } = Grid;

interface Props {
  media: DocumentModel | DisplayableAttachment;
  onClose: () => void;
  onPrevious?: () => void;
  onNext?: () => void;
}

const MediaPreview = ({ media, onClose, onNext, onPrevious }: Props) => {
  const [isDownloading, setIsDownloading] = useState(false);
  const { accessToken } = useAuth();
  const { t } = useTranslation('common');
  const screens = useBreakpoint();
  const [scale, setScale] = useState(1);

  const onFileDownload = useCallback(
    (media: DocumentModel | DisplayableAttachment) => {
      if (!media?.document_url) {
        return null;
      }

      const download = async () => {
        try {
          setIsDownloading(true);
          let downloadUrl = '';
          if (accessToken) {
            const image = await fetch(`${media.document_url}`, {
              method: 'GET',
              headers: {
                'Content-Type': media.file_type,
                Authorization: accessToken,
              },
            });
            const imageBlog = await image.blob();
            downloadUrl = URL.createObjectURL(imageBlog);
          }

          const link = document?.createElement('a');
          link.href = downloadUrl;
          link.download = media.title;
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        } catch (error) {
          message.error(t('someUnknownErrorOccurred'));
        } finally {
          setIsDownloading(false);
        }
      };

      download();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [accessToken]
  );

  const zoomIn = () => setScale((s) => Math.min(3, s + 0.5));
  const zoomOut = () => setScale((s) => Math.max(0.5, s - 0.5));

  return (
    <FilePreviewWrapper
      title={media.file_name || media.title}
      isDownloading={isDownloading}
      onDownload={() => onFileDownload(media)}
      onClose={onClose}
      isScrollable={false}
    >
      {isImageFileType(media.file_type) && (
        <ControlsContainer>
          <Button disabled={scale === 1} onClick={zoomOut} icon={<ZoomOutOutlined />} />
          <Button onClick={zoomIn} icon={<ZoomInOutlined />} />
        </ControlsContainer>
      )}

      <NavigationContainer justify="center" align="center">
        {onPrevious && (
          <Button
            shape="circle"
            ghost
            icon={<LeftOutlined />}
            onClick={() => {
              setScale(1);
              onPrevious();
            }}
            style={{
              position: 'absolute',
              left: 0,
              top: '50%',
              transform: 'translateY(-50%)',
              zIndex: 1,
            }}
          />
        )}

        {(isAudioFileType(media.file_type) || isVideoFileType(media.file_type)) && (
          <ReactPlayer
            url={media.document_url}
            controls
            width={screens.lg ? 990 : 300}
            height={screens.lg ? 557 : undefined}
          />
        )}

        {isImageFileType(media.file_type) && (
          <ImageWrapperStyled scale={scale}>
            <ImageContainerStyled scale={scale}>
              <ImageStyled
                src={media.document_url}
                alt={media.file_name || media.title}
                layout="responsive"
                width={100}
                height={100}
                objectFit="contain"
              />
            </ImageContainerStyled>
          </ImageWrapperStyled>
        )}
        {onNext && (
          <Button
            shape="circle"
            ghost
            icon={<RightOutlined />}
            onClick={() => {
              setScale(1);
              onNext();
            }}
            style={{
              position: 'absolute',
              right: 0,
              top: '50%',
              transform: 'translateY(-50%)',
              zIndex: 1,
            }}
          />
        )}
      </NavigationContainer>
    </FilePreviewWrapper>
  );
};

export default MediaPreview;
