import styled from '@emotion/styled';
import { Button, Flex } from 'antd';

export const ImageWrapperStyled = styled.div<{ scale: number }>`
  position: relative;
  width: calc(100vw - 160px);
  height: 80vh;
  margin: 0 auto;
  overflow: auto;
  display: flex;
  justify-content: center;
  align-items: center;
  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    width: calc(100vw - 100px);
  }
`;

export const ImageContainerStyled = styled.div<{ scale: number }>`
  position: relative;
  width: ${({ scale }) => `${scale * 100}%`};
  height: ${({ scale }) => `${scale * 100}%`};
  min-width: 100%;
  min-height: 100%;
  img {
    transform: scale(${({ scale }) => scale});
    transform-origin: top left;
    transition: transform 0.2s ease-in-out;
  }

  @media screen and (max-width: ${({ theme }) => theme.token.screenSM}px) {
    height: auto !important;
    min-height: auto !important;
  }
`;
export const NavigationContainer = styled(Flex)`
  width: 100%;
  padding: 0 0;
  position: relative;
`;

export const ControlsContainer = styled(Flex)`
  position: absolute;
  top: 50px;
  left: 50%;
  transform: translateX(-50%);
  z-index: 2;
  gap: 8px;
  button {
    background: #ffffff !important;
  }
`;
