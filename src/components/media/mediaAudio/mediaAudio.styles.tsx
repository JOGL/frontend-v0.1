import styled from '@emotion/styled';
import { CustomerServiceOutlined } from '@ant-design/icons';

export const CustomerServiceOutlinedStyled = styled(CustomerServiceOutlined)`
  position: absolute;
  font-size: 56px;
  color: ${({ theme }) => theme.token.neutral9};
  opacity: 0.5;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;
