import dayjs from 'dayjs';
import { MediaWrapperStyled } from '../media.styles';
import { Flex, Typography } from 'antd';
import { DocumentModel } from '~/__generated__/types';
import { CustomerServiceOutlinedStyled } from './mediaAudio.styles';
import { useState } from 'react';
import MediaPreview from '../mediaPreview/mediaPreview';

interface Props {
  media: DocumentModel;
  withPreview?: boolean;
}
const MediaAudio = ({ media, withPreview = true }: Props) => {
  const [showPreview, setShowPreview] = useState(false);
  const userName = `${media.created_by?.first_name} ${media.created_by?.last_name}`;

  return (
    <>
      <Flex vertical gap="small" onClick={() => setShowPreview(true)}>
        <MediaWrapperStyled>
          <CustomerServiceOutlinedStyled />
        </MediaWrapperStyled>
        <Flex vertical>
          <Typography.Text strong ellipsis={{ tooltip: userName }}>
            {userName}
          </Typography.Text>
          <Typography.Text type="secondary">{dayjs(media.created).format('MMM Do')}</Typography.Text>
        </Flex>
      </Flex>
      {withPreview && showPreview && <MediaPreview media={media} onClose={() => setShowPreview(false)} />}
    </>
  );
};

export default MediaAudio;
