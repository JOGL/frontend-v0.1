import React, { useState, FC, useEffect } from 'react';
import useTranslation from 'next-translate/useTranslation';
import useGet from '~/src/hooks/useGet';
import Button from '../primitives/Button';
import Loading from '../Tools/Loading';
import SpinLoader from '../Tools/SpinLoader';
import Select from 'react-select';
import { useApi } from '~/src/contexts/apiContext';
import { useRouter } from 'next/router';
import { entityItem } from '~/src/utils/utils';
import { usePushNotificationStore } from '~/src/store/push-notifications/PushNotificationsStoreProvider';

interface PropsModal {
  userId: string;
  userLogoUrl?: string;
  defaultValue?: any;
  closeModal?: () => void;
}

export const CfpAttachToWorkspace: FC<PropsModal> = ({ userId, userLogoUrl, defaultValue, closeModal }) => {
  const { data: dataWorkspacesMine, isLoading } = useGet(`/users/${userId}/workspaces?permission=manage`);
  const { t } = useTranslation('common');
  const api = useApi();
  const [selectedWorkspace, setSelectedWorkspace] = useState(null);
  const [sending, setSending] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const router = useRouter();
  const { showPushNotificationModal } = usePushNotificationStore((s) => ({
    showPushNotificationModal: s.showPushNotificationModal,
  }));

  const onSubmit = (e) => {
    e.preventDefault();
    setSending(true);
    if (selectedWorkspace?.value) {
      api
        .post('/cfps', {
          title: '',
          short_description: '',
          short_title: '',
          status: 'draft',
          community_id: selectedWorkspace?.value,
          proposal_participiation: 'private',
          proposal_privacy: 'admin',
          discussion_participation: 'adminonly',
        })
        .then((res) => {
          showPushNotificationModal(router, `/cfp/${res.data}/edit`);
          setTimeout(() => {
            closeModal && closeModal();
          }, 3000);
          setIsButtonDisabled(true);
        })
        .catch((err) => setSending(false));
    }
  };

  // select the default value if there is one
  useEffect(() => {
    let defaultWorkspace = dataWorkspacesMine?.find((c) => c.id === defaultValue?.id && c.title !== '');
    if (defaultWorkspace) {
      setSelectedWorkspace({
        value: defaultWorkspace.id,
        label: defaultWorkspace.title,
        type: defaultWorkspace.type,
        userAccessLevel: defaultWorkspace?.user_access_level,
        imageUrl: defaultWorkspace?.logo_url_sm ?? defaultWorkspace?.banner_url_sm,
      });
      setIsButtonDisabled(false);
    }
  }, [dataWorkspacesMine]);

  const onWorkspaceSelect = (workspace) => {
    setSelectedWorkspace(optionsList.find((item) => item.value === workspace.value));
    setIsButtonDisabled(false);
  };

  // filter workspaces with empty title
  const optionsList = dataWorkspacesMine
    ?.filter((x) => x.title !== '')
    .map((option) => ({
      value: option.id,
      label: option.title,
      type: option.type,
      userAccessLevel: option?.user_access_level,
      imageUrl: option?.logo_url_sm ?? option?.banner_url_sm,
    }));

  const customClassNames: Record<string, () => string> = {
    menuList: () => 'show-scrollbar',
  };

  const customStyles = {
    control: (provided) => ({
      ...provided,
    }),
    valueContainer: (provided) => ({
      ...provided,
      height: 50,
    }),
    singleValue: (provided) => ({
      ...provided,
      width: '-webkit-fill-available',
    }),
    menu: (provided) => ({
      ...provided,
      zIndex: 0,
      position: 'unset',
    }),
    menuList: (provided) => ({
      ...provided,
      maxHeight: 175,
    }),
  };

  const CustomOption = ({ value, label, imageUrl, userAccessLevel }) => {
    return (
      <div tw="flex gap-2 cursor-pointer">
        <div
          style={{
            backgroundImage: `url(${imageUrl || entityItem.workspace.default_banner})`,
          }}
          tw="bg-cover bg-center h-9 w-9 rounded-full border border-solid border-[#ced4da]"
        />
        <div tw="flex flex-col flex-auto self-center">
          <span tw="text-[14px] font-bold">{label || t('untitled')}</span>
        </div>

        <div tw="flex flex-col w-14 items-center">
          <img
            src={userLogoUrl ?? '/images/default/default-user.png'}
            alt={label}
            tw="mx-auto w-5 h-5 self-center rounded-full shadow-custom"
          />{' '}
          <span tw="text-[14px] italic">{userAccessLevel}</span>
        </div>
      </div>
    );
  };

  return (
    <div>
      {!dataWorkspacesMine || isLoading ? (
        <Loading />
      ) : dataWorkspacesMine?.length > 0 && (dataWorkspacesMine as any)?.length > 0 ? (
        <div tw="flex flex-col gap-5">
          <div>
            <span tw="text-[14px] font-bold mt-2">{t('selectWorkspaceToCreateTheCallForProposalsIn')}</span>
            <Select
              options={optionsList}
              menuShouldScrollIntoView={true} // force scroll into view
              value={selectedWorkspace}
              noOptionsMessage={() => null}
              onChange={onWorkspaceSelect}
              formatOptionLabel={CustomOption}
              styles={customStyles}
              classNames={customClassNames}
              placeholder={t('typeToSearchItem', { item: t('workspace.other') })}
              autoFocus
            />
          </div>
          <div className="btnZone" tw="flex flex-row gap-2 self-center">
            <Button type="submit" disabled={isButtonDisabled || sending} onClick={onSubmit}>
              <>
                {sending && <SpinLoader />}
                {t('action.create')}
              </>
            </Button>
          </div>
        </div>
      ) : (
        <div style={{ textAlign: 'center' }}>{t('noWorkspaceToAdd')}</div>
      )}
    </div>
  );
};
