import React, { Fragment, useEffect, useState, useRef, Dispatch, SetStateAction } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Button from '~/src/components/primitives/Button';
import H2 from '~/src/components/primitives/H2';
import Card from '~/src/components/Cards/Card';
import FormDefaultComponent from '~/src/components/Tools/Forms/FormDefaultComponent';
import Icon from '../primitives/Icon';
import FormWysiwygComponent from '../Tools/Forms/FormWysiwygComponent';
import FormDropdownComponent from '../Tools/Forms/FormDropdownComponent';
import { CfpTemplate, CfpTemplateQuestion } from '~/src/types';
import { ReactSortable } from 'react-sortablejs';

interface CfpManageTemplateProps {
  template: CfpTemplate;
  setHasChanged: Dispatch<SetStateAction<boolean>>;
  handleChange: (key: any, content: any) => void;
}

export default function CfpManageTemplate({ template, setHasChanged, handleChange }: CfpManageTemplateProps) {
  const { t } = useTranslation('common');
  const [questions, setQuestions] = useState<CfpTemplateQuestion[]>(template.questions);
  const [hasUpdates, setHasUpdates] = useState<boolean>(false);
  const editedTemplate = useRef(template);

  const addQuestion = () => {
    setHasUpdates(true);

    const newQuestion = {
      key: (Date.now() + Math.floor(Math.random() * 100)).toString(),
      title: '',
      description: '',
      type: 'paragraph',
      choices: [''],
      max_length: 0,
      order: questions.length + 1,
    };

    setQuestions([...questions, newQuestion]);

    //update ref
    editedTemplate.current.questions.push(newQuestion);
  };

  const editQuestion = (questionKey: string, property: string | number, value: string | number | string[]) => {
    setHasUpdates(true);
    setQuestions((prevQuestions) =>
      prevQuestions.map((prevQ) => {
        return prevQ.key !== questionKey ? prevQ : { ...prevQ, [property]: value };
      })
    );

    editedTemplate.current.questions.find((q) => q.key === questionKey)[property] = value;
  };

  const deleteQuestion = (questionKey: string) => {
    //Reset error state because deleting a question may solve them
    setHasUpdates(true);
    setQuestions((prevQuestions) => prevQuestions.filter((question) => question.key !== questionKey));

    //update ref
    const index = editedTemplate.current.questions.findIndex((question) => question.key === questionKey);
    if (index > -1) {
      editedTemplate.current.questions.splice(index, 1);
    }
  };

  useEffect(() => {
    if (hasUpdates) {
      // passe handleChange to parent + also update every order fields
      handleChange('template', { questions: questions.map((o, i) => ({ ...o, order: i + 1 })) });
    }
  }, [questions]);

  return (
    <div tw="space-y-3 mt-10">
      <H2>{t('question.other')}</H2>
      {questions.length > 0 ? (
        <ReactSortable
          list={questions}
          setList={setQuestions}
          tw="flex flex-col pt-2 gap-5"
          onChange={() => {
            setHasChanged(true);
            setHasUpdates(true);
          }}
        >
          {questions.map((questionItem) => (
            <Item
              key={questionItem.key}
              questionItem={questionItem}
              editQuestion={editQuestion}
              deleteQuestion={deleteQuestion}
            />
          ))}
        </ReactSortable>
      ) : (
        <div>{t('noItems', { item: t('question.other') })}</div>
      )}
      <Button tw="mt-4 cursor-pointer" btnType="secondary" onClick={addQuestion}>
        {t('action.addItem', { item: t('question.one') })}
      </Button>
    </div>
  );
}

type ItemProps = {
  questionItem: CfpTemplateQuestion;
  editQuestion: (questionKey: string, property: string | number, value: string | number | string[]) => void;
  deleteQuestion: (questionKey: string) => void;
};

const Item = ({ questionItem, editQuestion, deleteQuestion }: ItemProps) => {
  const { t } = useTranslation('common');
  const [question, setQuestion] = useState<CfpTemplateQuestion>(questionItem);
  const [maxLength, setMaxLength] = useState(question.max_length);

  const handleQuestionChange = (property: string | number, value: string | number | string[]) => {
    setQuestion({ ...question, [property]: value });
    editQuestion(question.key, property, value);
  };

  const handleChangeMaxLength = (e) => {
    const newMaxLength = e.target.value === '' ? '' : parseFloat(e.target.value);
    setMaxLength(newMaxLength);
    handleQuestionChange('max_length', newMaxLength || 0);
  };

  return (
    <Fragment>
      <Card tw="cursor-grab active:cursor-grabbing">
        <form tw="flex flex-col justify-between md:flex-row">
          <div tw="flex flex-col w-full pr-0">
            <div tw="justify-between flex w-full">
              <span />
              <Icon icon="akar-icons:drag-horizontal" tw="w-5 h-5 hover:text-gray-600" />
              <Button btnType="secondary" onClick={() => deleteQuestion(question.key)}>
                <Icon icon="ic:round-delete" tw="w-5 h-5 cursor-pointer -mr-[.5px] hover:text-gray-600" />
              </Button>
            </div>
            <div tw="flex gap-3">
              <FormDefaultComponent
                id="title"
                content={question.title}
                title={t('question.one')}
                onChange={handleQuestionChange}
              />
              <FormDropdownComponent
                id="type"
                type="cfpTemplateType"
                content={question.type}
                title={t('type')}
                options={['paragraph', 'single_choice', 'multiple_choice', 'file_upload']}
                onChange={handleQuestionChange}
              />
            </div>
            {question.type === 'paragraph' && (
              <div tw="flex items-center w-full">
                <span tw="font-bold text-gray-700 mr-1">{t('maxNumberOfCharacters')}</span>
                <span tw="text-gray-700 mr-2">({t('leaveEmptyForNoLimit')})</span>
                <input
                  type="number"
                  value={maxLength === 0 ? '' : maxLength}
                  onChange={handleChangeMaxLength}
                  className="form-control"
                  tw="w-[100px] px-3 py-[0.4rem] rounded-md border-gray-300 focus:(ring-primary border-primary)"
                  min={0}
                />
              </div>
            )}
            <FormWysiwygComponent
              id="description"
              title={`${t('additionalDetails')}:`}
              content={question.description}
              onChange={handleQuestionChange}
              specialFormat="short"
            />
            {(question.type === 'single_choice' || question.type === 'multiple_choice') && (
              <>
                {question.choices.map((choice, i) => (
                  <div tw="flex gap-2 items-center" key={i}>
                    <input
                      id={choice}
                      name="choices"
                      type={question?.type === 'single_choice' ? 'radio' : 'checkbox'}
                      disabled
                    />
                    <FormDefaultComponent
                      id="choices"
                      content={choice}
                      title={`${t('option.one')} ${i + 1}`}
                      onChange={(p, v) =>
                        handleQuestionChange(p, [...question.choices.slice(0, i), v, ...question.choices.slice(i + 1)])
                      }
                    />
                    <div
                      onClick={() =>
                        handleQuestionChange('choices', [...question.choices.filter((_, index) => i !== index)])
                      }
                    >
                      <Icon icon="ic:baseline-close" tw="w-5 h-5 cursor-pointer hover:text-gray-600" />
                    </div>
                  </div>
                ))}
                {
                  <Button
                    onClick={() => handleQuestionChange('choices', [...question.choices, ''])}
                    btnType="secondary"
                  >
                    {t('action.addItem', { item: t('option.one') })}
                  </Button>
                }
              </>
            )}
          </div>
        </form>
      </Card>
    </Fragment>
  );
};
