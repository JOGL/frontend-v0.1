import { FC } from 'react';
import Grid from '../Grid';
import useGet from '~/src/hooks/useGet';
import { Cfp, Proposal } from '~/src/types';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import ProposalCard from '../Proposal/ProposalCard';
import useUser from '~/src/hooks/useUser';

interface Props {
  cfp: Cfp;
}

const CfpProposals: FC<Props> = ({ cfp }) => {
  const { data: proposals, isLoading, error } = useGet<Proposal[]>(`/cfps/${cfp.id}/proposals`);
  const { user } = useUser();
  return (
    <Grid>
      {isLoading && !error ? (
        <Loading />
      ) : proposals?.length === 0 || error ? (
        <NoResults type="proposal" />
      ) : (
        proposals.map((proposal, index) => (
          <ProposalCard
            key={index}
            proposal={proposal}
            cfp={cfp}
            fromPage="cfp"
            textChips={proposal.users.find((u) => u.id === user?.id) ? [{ name: "I'm applicant" }] : []}
          />
        ))
      )}
    </Grid>
  );
};

export default CfpProposals;
