import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Cfp } from '~/src/types';
import FormToggleComponent from '../Tools/Forms/FormToggleComponent';
import useGet from '~/src/hooks/useGet';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import ProposalAdminCard from '../Proposal/ProposalAdminCard';
import Icon from '../primitives/Icon';
import tw from 'twin.macro';
import MessageBox from '../Tools/MessageBox';

interface Props {
  cfp: Cfp;
  handleChange: (key: any, content: any) => void;
  handleSubmit: () => void;
}

const CfpProposalsAdmin: FC<Props> = ({ cfp, handleChange, handleSubmit }) => {
  const { data: proposals, mutate: proposalsRevalidate } = useGet(`/cfps/${cfp.id}/proposals`);
  const { t } = useTranslation('common');
  // const { showModal, closeModal } = useModal();
  const handleChangeScoring = (key, content) => {
    handleChange('scoring', content);
    handleSubmit({ scoring: content });
  };
  const handleChangeScore = (e) => {
    let max_score = e.target.value === '' ? '' : parseFloat(e.target.value);
    handleChange('max_score', max_score);
    handleSubmit({ max_score });
  };
  // const [showCloseBtn, setShowCloseBtn] = useState(
  //   cfp.stop === null &&
  //     hasCfpStarted(cfp.proposals.length) &&
  //     hasCfpDatePassed(cfp.deadline)
  // );

  const cantChangeScore = proposals?.filter((x) => x.score !== 0).length !== 0;
  return (
    <>
      <p tw="italic mb-8">{t('trackProgressAndScoreProposals')}</p>
      {cantChangeScore && (
        <MessageBox type="info">
          You can't change those parameters because at least one proposal have received a score already.
        </MessageBox>
      )}
      <div
        tw="flex flex-col px-6 py-3 border border-gray-300 border-solid rounded-md text-gray-800 items-center space-x-6 mb-4"
        css={cantChangeScore && tw`cursor-not-allowed pointer-events-none opacity-40`}
      >
        <div tw="inline-flex">
          <Icon icon="mdi:funnel" tw="mr-4 text-[#CBCFD6]" />
          {t('enableScoringToRateCallForProposalsParticipants')}.
          <FormToggleComponent
            id="scoring"
            choice1={t('no')}
            choice2={t('yes')}
            isChecked={cfp?.scoring}
            onChange={handleChangeScoring}
          />
        </div>
        <div tw="flex items-center w-full" css={!cfp?.scoring && tw`opacity-50 cursor-not-allowed`}>
          <span tw="font-bold mr-2">{t('maxScore')}</span>
          <input
            type="number"
            value={cfp?.max_score}
            onChange={handleChangeScore}
            className="form-control"
            tw="w-1/4 px-3 py-[0.4rem] rounded-md border-gray-300 focus:(ring-primary border-primary)"
            min={0}
            disabled={!cfp?.scoring}
          />
        </div>
      </div>
      {/* {showCloseBtn && (
        <div tw="mb-4">
          <div
            {...(!allProposalsGotScore && {
              'data-tip': t('allSubmittedProposalsNeedScore'),
              'data-for': 'cantCloseExplanation',
            })}
          >
            <Button
              disabled={!allProposalsGotScore}
              onClick={() => {
                Swal.fire({
                  title: t('closeCallForProposalsConfirmation'),
                  text: t('ensureYouHaveAssignedAllProposalsScore'),
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: t('yes'),
                }).then(({ isConfirmed }) => {
                  if (isConfirmed) {
                    api
                      .patch(`/api/cfps/${cfp.id}`, {
                        cfp: { stop: new Date(Date.now()) },
                      })
                      .then(() => {
                        Swal.fire({
                          icon: 'success',
                          showConfirmButton: false,
                          text: t('congratulationsFinishedCallForProposals'),
                          // timer: 2500,
                        });
                        setShowCloseBtn(false);
                      });
                  }
                });
              }}
            >
              {t('action.closeCallForProposals')}
            </Button>
          </div>
          <ReactTooltip
            id="cantCloseExplanation"
            effect="solid"
            role="tooltip"
            type="dark"
            className="forceTooltipBg"
          />
          <p tw="italic text-gray-600">{t('scoreProposalsButtonDisabled')}</p>
        </div>
      )} */}
      {!proposals ? (
        <Loading />
      ) : proposals?.length === 0 ? (
        <NoResults type="proposal" />
      ) : (
        <table tw="min-w-full border-collapse block md:table mb-8">
          <thead tw="block md:table-header-group">
            <tr tw="border border-gray-500 md:border-none block md:table-row absolute -top-full md:top-auto -left-full md:left-auto  md:relative ">
              <th tw="bg-gray-200 p-2 font-bold md:border md:border-gray-500 text-left block md:table-cell">
                {t('title')}
              </th>
              <th tw="bg-gray-200 p-2 font-bold md:border md:border-gray-500 text-left block md:table-cell">
                {t('status')}
                {': '}
              </th>
              <th tw="bg-gray-200 p-2 font-bold md:border md:border-gray-500 text-left block md:table-cell">
                {t('submissionDate')}
              </th>
              {cfp.scoring && (
                <th tw="bg-gray-200 p-2 font-bold md:border md:border-gray-500 text-left block md:table-cell">
                  {t('score')}
                </th>
              )}
              <th tw="bg-gray-200 p-2 font-bold md:border md:border-gray-500 text-left block md:table-cell">
                {t('action', { count: 2 })}
              </th>
            </tr>
          </thead>
          <tbody tw="block md:table-row-group">
            {proposals.map((proposal, i) => (
              <ProposalAdminCard
                proposal={proposal}
                key={i}
                max_score={cfp.max_score}
                cfpId={cfp.id}
                cfpDates={{
                  start: cfp.submissions_from,
                  deadline: cfp.submissions_to,
                }}
                hasScore={cfp.scoring}
                callBack={proposalsRevalidate}
              />
            ))}
          </tbody>
        </table>
      )}
    </>
  );
};
export default CfpProposalsAdmin;
