import React, { FC } from 'react';
import { Cfp } from '~/src/types';
import InfoStats from '~/src/components/Tools/InfoStats';
import useTranslation from 'next-translate/useTranslation';
import useGet from '~/src/hooks/useGet';
import {
  cfpStatus,
  displayObjectRelativeDate,
  displayObjectUTCDate,
  renderShortObjectCard,
  showColoredStatus,
} from '~/src/utils/utils';
import A from '../primitives/A';
import { useRouter } from 'next/router';

interface Props {
  cfp: Cfp;
  limitedVisibility: boolean;
}

const CfpInfo: FC<Props> = ({ cfp, limitedVisibility }) => {
  const { t } = useTranslation('common');
  const { locale } = useRouter();
  const { data: cfpWorkspace } = useGet(`/workspaces/${cfp.community_id}`);
  const cfpHardStatus = cfpStatus(cfp?.status, {
    start: cfp?.submissions_from,
    deadline: cfp?.submissions_to,
  })[0];

  return (
    <div tw="flex-shrink-0 md:p-4 md:w-80 md:pt-6">
      <div tw="flex flex-wrap mb-2 justify-around md:mt-0">
        <InfoStats
          object="cfp"
          type="proposal"
          tab="proposals"
          count={cfp.stats.submitted_proposals_count}
          {...(limitedVisibility && { noLink: true })}
        />
        {cfp?.submissions_from && cfp?.submissions_to && (
          <A href={`/cfp/${cfp?.id}?tab=rules`} shallow noStyle scroll={false}>
            <div tw="flex items-center flex-col justify-between py-1 text-center hover:opacity-80 cursor-pointer">
              <div
                tw="text-lg font-bold"
                title={displayObjectUTCDate(
                  cfpHardStatus === 'startingSoon' ? cfp?.submissions_from : cfp?.submissions_to
                )}
              >
                {displayObjectRelativeDate(
                  cfpHardStatus === 'startingSoon' ? cfp?.submissions_from : cfp?.submissions_to,
                  locale
                )}
              </div>
              {cfpHardStatus === 'startingSoon' ? t('openForSubmissions') : t('submissionDeadline')}
            </div>
          </A>
        )}
      </div>
      <hr tw="mb-4" />
      <div tw="flex items-center justify-between flex-wrap mb-3">
        <div>{t('status')}: </div>
        {showColoredStatus(
          cfpStatus(cfp?.status, {
            start: cfp?.submissions_from,
            deadline: cfp?.submissions_to,
          }),
          t
        )}
      </div>
      {t('organizedBy')}:{renderShortObjectCard('workspace', cfpWorkspace)}
    </div>
  );
};

export default CfpInfo;
