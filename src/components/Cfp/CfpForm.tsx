import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import FormDefaultComponent from '~/src/components/Tools/Forms/FormDefaultComponent';
import FormImgComponent from '~/src/components/Tools/Forms/FormImgComponent';
import { Cfp } from '~/src/types';
import FormSkillsComponent from '../Tools/Forms/FormSkillsComponent';
import { generateSlug } from '~/src/utils/utils';
import isMandatoryField from '~/src/utils/isMandatoryField';
import FormTextAreaComponent from '../Tools/Forms/FormTextAreaComponent';
import FormMissingFieldsList from '../Tools/Forms/FormMissingFieldsList';

interface Props {
  cfp: Cfp;
  stateValidation: {};
  showErrorList: boolean;
  handleChange: (key: any, content: any) => void;
}

const CfpForm: FC<Props> = ({ cfp, stateValidation, showErrorList, handleChange }) => {
  const { t } = useTranslation('common');

  const handleChangeCfp = (key, content) => {
    if (key === 'title') {
      // generate a shortname when typing a title, and update short_name field with the value
      handleChange('short_title', generateSlug(content));
    }
    handleChange(key, content);
  };

  const { valid_title, valid_short_description } = stateValidation || {};

  return (
    <form className="CfpForm">
      {showErrorList && <FormMissingFieldsList stateValidation={stateValidation} />}

      {/* Basic fields */}
      <div tw="flex-1">
        <FormDefaultComponent
          id="title"
          content={cfp.title}
          title={t('title')}
          placeholder={t('anAwesomeCallForProposals')}
          onChange={handleChangeCfp}
          mandatory={isMandatoryField('cfp', 'title')}
          errorCodeMessage={valid_title ? valid_title.message : ''}
          isValid={valid_title ? !valid_title.isInvalid : undefined}
          maxChar={120}
        />
      </div>

      <FormImgComponent
        id="banner_id"
        type="banner"
        title={t('callForProposalsBanner')}
        itemType="cfps"
        itemId={cfp.id}
        imageUrl={cfp.banner_url}
        showAddButton={cfp?.banner_url === null}
        defaultImg="/images/default/default-cfp.png"
        onChange={handleChangeCfp}
      />

      <FormTextAreaComponent
        content={cfp.short_description}
        id="short_description"
        maxChar={500}
        onChange={handleChange}
        rows={3}
        title={t('shortDescription')}
        placeholder={t('callForProposalsBrieflyExplained')}
        errorCodeMessage={valid_short_description ? valid_short_description.message : ''}
        isValid={valid_short_description ? !valid_short_description.isInvalid : undefined}
        mandatory={isMandatoryField('cfp', 'short_description')}
      />

      <FormSkillsComponent
        id="keywords"
        type="keywords"
        placeholder={t('skillsPlaceholder')}
        title={t('keyword.other')}
        showHelp={false}
        content={cfp?.keywords}
        mandatory={false}
        onChange={handleChangeCfp}
      />
    </form>
  );
};
export default CfpForm;
