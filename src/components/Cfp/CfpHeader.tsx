/* eslint-disable camelcase */
import Icon from '~/src/components/primitives/Icon';
import useTranslation from 'next-translate/useTranslation';
import React, { FC, useState } from 'react';
import H1 from '~/src/components/primitives/H1';
import Button from '../primitives/Button';
import { Cfp } from '~/src/types';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import CfpInfo from './CfpInfo';
import BtnJoin from '../Tools/BtnJoin';
import { useRouter } from 'next/router';
import { useModal } from '~/src/contexts/modalContext';
import { hasCfpDatePassed, isAdmin, isMember, isOwner, linkify } from '~/src/utils/utils';
import useGet from '~/src/hooks/useGet';
import { useApi } from '~/src/contexts/apiContext';
import { Menu, MenuItem, MenuButton, MenuList } from '@reach/menu-button';
import ShowOnboarding from '../Onboarding/ShowOnboarding';
import { WorkspaceLinkToCfp } from '../workspace/workspaceLinkToCfp/workspaceLinkToCfp';
import useUser from '~/src/hooks/useUser';
import BtnAdd from '../Tools/BtnAdd';
import { securityOptionsCfp } from '~/src/utils/security_options';
import Link from 'next/link';
import InfoDefaultComponent from '../Tools/Info/InfoDefaultComponent';
import { ContainerPrivacyAndSecurityInfo } from '../Tools/ContainerPrivacyAndSecurityInfo';

interface Props {
  cfp: Cfp;
  limitedVisibility: boolean;
  refresh?: () => void;
}

const CfpHeader: FC<Props> = ({ cfp, limitedVisibility, refresh }) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const { showModal, closeModal } = useModal();
  const { user } = useUser();
  const [hasAcceptedOrRejectedInvite, setHasAcceptedOrRejectedInvite] = useState(false);
  const api = useApi();

  const { data: pending_invite, mutate: refreshInvites } = useGet(
    cfp.user_joining_restriction_level === 'visitor' || cfp.user_access_level === 'pending'
      ? `/cfps/${cfp.id}/invitation`
      : ''
  );

  const leaveObject = () => {
    api.post(`/cfps/${cfp.id}/leave`).then((res) => {
      res.status === 200 && location.reload();
    });
  };

  const acceptOrRejectInvite = (e, type) => {
    // if (itemId && itemType && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
    if ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      const route = `/cfps/${cfp.id}/invites/${pending_invite?.invitation_id}/${type}`;
      api
        .post(route)
        .then((res) => {
          refreshInvites();
          setHasAcceptedOrRejectedInvite(true);
          // refresh page if accepting
          if (type === 'accept') {
            const channelId = cfp?.home_channel_id as string;
            router.push(`/cfp/${cfp.id}?channelId=${channelId}`).then(() => {
              router.reload();
            });
          }
          // send event to google analytics
        })
        .catch((err) => {
          // console.error(`Couldn't PUT ${itemType} with itemId=${itemId} and action=${action}`, err);
        });
    }
  };

  const { id, title, short_description } = cfp;

  const displayExtraActionButtons = () => (
    <div tw="gap-2 md:gap-1 flex">
      <Icon
        icon="majesticons:scroll-text-line"
        tw="w-8 h-8 border-gray-200 border p-1 rounded-full cursor-pointer hover:bg-gray-200"
        onClick={() => {
          showModal({
            children: (
              <ContainerPrivacyAndSecurityInfo
                object={cfp}
                limitedVisibility={limitedVisibility}
                securityOptions={securityOptionsCfp}
              />
            ),
            title: t('privacyAndSecurity'),
            maxWidth: '60rem',
          });
        }}
      />
      <Menu>
        <MenuButton tw="text-[.8rem] text-gray-700 hover:bg-gray-200 rounded-full border-gray-200 border w-8 h-8 mx-auto">
          •••
        </MenuButton>
        <MenuList className="post-manage-dropdown">
          <ShareBtns type="cfp" specialObjId={id} isActionMenu />
          {cfp.user_access_level !== 'visitor' && cfp.user_access_level !== 'pending' && !isOwner(cfp) && (
            <MenuItem
              onSelect={() => {
                showModal({
                  children: (
                    <div tw="inline-flex space-x-3">
                      <Button btnType="danger" onClick={leaveObject}>
                        {t('yes')}
                      </Button>
                      <Button onClick={closeModal}>{t('no')}</Button>
                    </div>
                  ),
                  title: t('areYouSureYouWantToLeaveThisContainer', { container: t('callForProposals.one') }),
                  maxWidth: '30rem',
                });
              }}
            >
              <Icon icon="pepicons-pop:leave" />
              {t('action.leave')}
            </MenuItem>
          )}
        </MenuList>
      </Menu>
    </div>
  );

  return (
    <header tw="relative w-full md:border-b border-gray-200">
      <div tw="flex flex-wrap md:divide-x divide-gray-200 md:flex-nowrap">
        <div tw="max-md:hidden">
          <CfpInfo cfp={cfp} limitedVisibility={limitedVisibility} />
        </div>
        <div tw="px-4 py-4 md:px-8 md:pt-6 w-full">
          <div tw="inline-flex items-center mb-1 justify-between w-full gap-3">
            <div tw="inline-flex items-center">
              <H1 tw="text-[1.7rem] md:text-[2rem]">{title}</H1>
            </div>
            <span tw="max-md:hidden">{displayExtraActionButtons()}</span>
          </div>

          <div tw="text-gray-600 mt-4">
            <InfoDefaultComponent content={linkify(short_description)} containsHtml />
          </div>

          <div tw="flex items-center gap-2 flex-wrap">
            {isMember(cfp) && <BtnAdd container={cfp} type="cfp" refresh={refresh} />}
            {user &&
              hasCfpDatePassed(cfp.submissions_from) &&
              !hasCfpDatePassed(cfp.submissions_to) &&
              cfp?.user_access?.permissions?.includes('createproposals') &&
              cfp?.template.questions.length !== 0 && (
                <Button
                  onClick={() => {
                    showModal({
                      children: <WorkspaceLinkToCfp userId={user.id} cfpId={cfp.id} />,
                      title: t('action.startApplication'),
                      maxWidth: '50rem',
                    });
                  }}
                >
                  {t('action.startApplication')}
                </Button>
              )}
            {isAdmin(cfp) && (
              <Link href={`/cfp/${cfp.id}/edit`}>
                <Button btnType="secondary">
                  <Icon icon="bxs:edit" tw="w-5 h-5" />
                  {t('action.manage')}
                </Button>
              </Link>
            )}
            <span tw="md:hidden">{displayExtraActionButtons()}</span>
          </div>

          <div tw="flex flex-wrap gap-3 mb-2">
            {/* Join/request btn, or onboarding modal, or accept/reject buttons */}
            {!hasAcceptedOrRejectedInvite && pending_invite && pending_invite.invitation_type !== 'request' ? (
              <div tw="flex px-6 py-3 border border-gray-300 border-solid rounded-md text-gray-500 items-center">
                {t('youHaveBeenInvitedToJoinThisContainer', { container: t('callForProposals.one') })}
                <div tw="space-x-2 pl-5">
                  <Button
                    tw="text-green-500 border-green-500 bg-white hover:(text-white bg-green-500)"
                    onClick={(e) => acceptOrRejectInvite(e, 'accept')}
                  >
                    {t('action.accept')}
                  </Button>
                  <Button
                    tw="text-danger border-danger bg-white hover:(text-white bg-danger)"
                    onClick={(e) => acceptOrRejectInvite(e, 'reject')}
                  >
                    {t('action.reject')}
                  </Button>
                </div>
              </div>
            ) : // if onboarding questionnaire is enabled, and multiple other conditions, show button to open onboarding questionnaire modal
            cfp.onboarding?.enabled &&
              cfp.user_access_level === 'visitor' &&
              cfp.user_access_level !== 'pending' &&
              cfp.user_joining_restriction_level !== 'forbidden' &&
              cfp.joining_restriction === 'request' &&
              cfp.onboarding?.questionnaire.enabled &&
              cfp.onboarding?.questionnaire.items.length > 0 ? (
              <Button
                onClick={() => {
                  showModal({
                    children: <ShowOnboarding object={cfp} itemType="cfps" type="first_onboarding" />,
                    title: t('answeringQuestions'),
                    maxWidth: '40rem',
                  });
                }}
              >
                {t('action.requestToJoin')}
              </Button>
            ) : (
              // else show the basic "join" button (can be open or request to join)
              (cfp.user_access_level === 'visitor' || cfp.user_access_level === 'pending') &&
              cfp.joining_restriction !== 'invite' &&
              cfp.user_joining_restriction_level !== null && (
                <BtnJoin
                  joinType={cfp.joining_restriction}
                  // isPrivate={cfp.content_privacy === 'private'}
                  itemType="cfps"
                  itemId={cfp.id}
                  textJoin={cfp.joining_restriction === 'open' ? t('action.join') : t('action.requestToJoin')}
                  count={cfp.stats.members_count}
                  pending_invite={pending_invite}
                  showMembersModal={() =>
                    router.push(`/cfp/${router.query.id}?tab=members`, undefined, { shallow: true })
                  }
                  hasNoStat
                />
              )
            )}
          </div>
        </div>
      </div>
    </header>
  );
};

export default CfpHeader;
