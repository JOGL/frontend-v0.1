import { FC, forwardRef, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import TitleInfo from '~/src/components/Tools/TitleInfo';
import { Cfp } from '~/src/types';
import Button from '../primitives/Button';
import FormWysiwygComponent from '../Tools/Forms/FormWysiwygComponent';
import FormDefaultComponent from '../Tools/Forms/FormDefaultComponent';
import DatePicker from 'react-datepicker';
import tw from 'twin.macro';
import Alert from '../Tools/Alert/Alert';
import MessageBox from '../Tools/MessageBox';

interface Props {
  cfp: Cfp;
  handleChange: (key: any, content: any) => void;
}

const CfpRulesForm: FC<Props> = ({ cfp, handleChange }) => {
  const { t } = useTranslation('common');

  const [hasUpdated, setHasUpdated] = useState<boolean>(false);
  const [to, setTo] = useState(cfp.submissions_to);
  const [from, setFrom] = useState(cfp.submissions_from);

  //Only set any date in the parent cfp object if both are valid
  useEffect(() => {
    if (hasUpdated) {
      handleChange('submissions_to', to);
      handleChange('submissions_from', from);
    }
  }, [to, from]);

  const CustomDateInput = forwardRef(({ value, onClick, fieldTitle }, ref) => (
    <div onClick={onClick} ref={ref}>
      <FormDefaultComponent content={value} title={fieldTitle} />
    </div>
  ));

  return (
    <form className="cfpForm">
      <FormWysiwygComponent
        id="rules"
        title={t('rule.other')}
        placeholder={t('describeRulesInDetail')}
        content={cfp.rules}
        onChange={handleChange}
      />
      <div tw="w-full mb-14">
        <TitleInfo title={t('timeline')} />
        <div className="content">
          <p>{t('manageDatesAndTimesForCallForProposals')}</p>
          {cfp.template.questions.length === 0 && (
            <MessageBox type="info">{t('needToDefineQuestionsBeforeSettingStartAndEndDate')}.</MessageBox>
          )}
          <div
            tw="flex gap-4 flex-wrap sm:flex-nowrap"
            css={cfp.template.questions.length === 0 && tw`cursor-not-allowed pointer-events-none opacity-40`}
          >
            <div tw="flex-col flex">
              <DatePicker
                id="submissions_from"
                selected={from && new Date(from)}
                maxDate={to && new Date(to)}
                onChange={(e) => {
                  setHasUpdated(true);
                  setFrom(e);
                }}
                customInput={<CustomDateInput fieldTitle={t('openForSubmissions')} />}
                timeInputLabel={`${t('time')}:`}
                dateFormat="MM/dd/yyyy h:mm aa"
                showTimeInput
              />
              <Button
                onClick={() => {
                  setHasUpdated(true);
                  setFrom(new Date());
                }}
              >
                {t('openForSubmissionsNow')}
              </Button>
            </div>
            <div tw="flex-col flex">
              <DatePicker
                id="submissions_to"
                selected={to && new Date(to)}
                minDate={from && new Date(from)}
                onChange={(e) => {
                  setHasUpdated(true);
                  setTo(e);
                }}
                customInput={<CustomDateInput fieldTitle={t('submissionDeadlineStartReview')} />}
                timeInputLabel={`${t('time')}:`}
                dateFormat="MM/dd/yyyy h:mm aa"
                showTimeInput
              />
              {to && (
                <Button
                  disabled={new Date(from) > new Date()}
                  onClick={() => {
                    setHasUpdated(true);
                    setTo(new Date());
                  }}
                >
                  {new Date(to) > new Date() ? t('closeSubmissionsNow') : t('reopenSubmissions')}
                </Button>
              )}
            </div>
          </div>
        </div>
      </div>
    </form>
  );
};
export default CfpRulesForm;
