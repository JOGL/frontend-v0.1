/* eslint-disable camelcase */
import Link from 'next/link';
import React from 'react';
import ObjectCard from '~/src/components/Cards/ObjectCard';
import H2 from '~/src/components/primitives/H2';
import Title from '~/src/components/primitives/Title';
import { TextWithPlural } from '~/src/utils/managePlurals';
import useTranslation from 'next-translate/useTranslation';
import { displayObjectRelativeDate, cfpStatus, showColoredStatus, displayObjectUTCDate } from '~/src/utils/utils';
import { ImageChip, TextChip } from '~/src/types/chip';
import tw from 'twin.macro';
import { useRouter } from 'next/router';

interface Props {
  id: string;
  title: string;
  short_title: string;
  short_description?: string;
  membersCount?: number;
  proposalsCount?: number;
  has_saved?: boolean;
  banner_url: string;
  width?: string;
  cardFormat?: string;
  status?: string;
  chip?: TextChip;
  textChips?: TextChip[];
  imageChips?: ImageChip[];
  last_activity?: string;
  submissions_from: string;
  submissions_to: string;
}
const CfpCard = ({
  id,
  title,
  short_title,
  short_description,
  membersCount,
  proposalsCount,
  banner_url,
  width,
  cardFormat,
  chip,
  textChips = [],
  imageChips = [],
  status,
  last_activity,
  submissions_from,
  submissions_to,
}: Props) => {
  const cfpUrl = `/cfp/${id}/${short_title}`;
  const { t } = useTranslation('common');
  const { locale } = useRouter();
  const cfpHardStatus = cfpStatus(status, {
    start: submissions_from,
    deadline: submissions_to,
  })[0];
  return (
    <ObjectCard
      imgUrl={banner_url || '/images/default/default-cfp.png'}
      href={cfpUrl}
      width={width}
      chip={chip}
      textChips={status === 'draft' ? [{ name: 'draft' }, ...textChips] : textChips}
      imageChips={imageChips}
      cardFormat={cardFormat}
    >
      {/* Title */}
      <div tw="inline-flex items-center" css={cardFormat !== 'inPosts' && tw`md:h-8`}>
        <Link href={cfpUrl} passHref legacyBehavior>
          <Title>
            <H2
              tw="[word-break:break-word] text-[16px] items-center"
              css={cardFormat !== 'inPosts' ? tw`md:line-clamp-2` : tw`md:line-clamp-1`}
              title={title || t('untitledItem', { item: t('callForProposals.one') })}
            >
              {title || t('untitledItem', { item: t('callForProposals.one') })}
            </H2>
          </Title>
        </Link>
      </div>

      {cardFormat !== 'inPosts' ? (
        <>
          <Hr tw="mt-2 pb-2" />
          <div tw="h-[7.5rem] text-sm">
            {/* Status */}
            {showColoredStatus(
              cfpStatus(status, {
                start: submissions_from,
                deadline: submissions_to,
              }),
              t
            )}
            <div tw="line-clamp-3 text-[#343a40] text-sm pt-2">{short_description}</div>
          </div>
        </>
      ) : (
        <div tw="line-clamp-2 text-[#343a40] text-sm pt-2">{short_description}</div>
      )}
      {/* Stats */}
      {cardFormat !== 'inPosts' && (
        <>
          <Hr tw="pt-2 mt-5" />
          <div tw="items-center justify-around space-x-2 flex">
            <CardData value={proposalsCount} title={<TextWithPlural type="proposal" count={proposalsCount} />} />
            {submissions_from && submissions_to && (
              <div tw="flex flex-col justify-center items-center text-[13px]">
                <div
                  tw="font-bold line-clamp-1"
                  title={displayObjectUTCDate(cfpHardStatus === 'startingSoon' ? submissions_from : submissions_to)}
                >
                  {displayObjectRelativeDate(
                    cfpHardStatus === 'startingSoon' ? submissions_from : submissions_to,
                    locale
                  ) || '--'}
                </div>
                <div tw="text-gray-500 font-medium">
                  {cfpHardStatus === 'startingSoon' ? t('openForSubmissions') : t('submissionDeadline')}
                </div>
              </div>
            )}
          </div>
        </>
      )}
    </ObjectCard>
  );
};

const CardData = ({ value, title }) => (
  <div tw="flex flex-col justify-center items-center text-[13px]">
    <div tw="font-bold line-clamp-1">{value || '--'}</div>
    <div tw="text-gray-500 font-medium">{title}</div>
  </div>
);
const Hr = (props) => <div tw="-mx-4 border-0 border-t border-solid border-color[rgba(0, 0, 0, 0.07)]" {...props} />;

export default CfpCard;
