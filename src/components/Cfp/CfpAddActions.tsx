import useTranslation from 'next-translate/useTranslation';
import { useModal } from '~/src/contexts/modalContext';
import { Cfp, ContainerPermissions, MenuActions } from '~/src/types';
import MembersAddModal from '../Members/MembersAddModal';
import ResourceCreate from '../Resources/ResourceCreate';
import { membersAction, resourceAction } from '~/src/utils/getContainerInfo';

const CfpAddActions = (cfp: Cfp, permissions: ContainerPermissions, refresh: () => void): MenuActions => {
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();

  const actions = {
    members: {
      ...membersAction,
      disabled: !permissions?.canAddMembers ?? true,
      handleSelect: () => {
        showModal({
          children: <MembersAddModal itemType="cfps" itemId={cfp?.id} />,
          title: t('inviteMembers'),
          maxWidth: '40rem',
        });
      },
    },
    content: [
      {
        ...resourceAction,
        disabled: !permissions?.canCreateResources ?? true,
        handleSelect: () => {
          showModal({
            children: (
              <ResourceCreate entityId={cfp?.id} entityType="cfps" closeModal={closeModal} refetchResult={refresh} />
            ),
            showCloseButton: false,
            title: t('action.addItem', { item: t('resource.one') }),
            maxWidth: '50rem',
          });
        },
      },
    ],
  };

  return actions;
};

export default CfpAddActions;
