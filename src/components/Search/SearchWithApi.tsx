import React, {
  FC,
  useReducer,
  useEffect,
  useState,
  useMemo,
  useRef,
  forwardRef,
  useImperativeHandle,
  CSSProperties,
} from 'react';
import Icon from '~/src/components/primitives/Icon';
import 'intro.js/introjs.css';
import Grid from '../Grid';
import { useApi } from '~/src/contexts/apiContext';
import { debounce, entityItem } from '~/src/utils/utils';
import { AxiosResponse } from 'axios';
import tw from 'twin.macro';
import { Menu, MenuItem, MenuButton, MenuList } from '@reach/menu-button';
import useTranslation from 'next-translate/useTranslation';

interface ExtraSearchParams {
  aggregateOption?: string | string[];
}

export interface SearchFilter {
  label: string;
  value: string;
  children?: { label: string; value: any }[];
}

interface SearchProps {
  placeHolder?: string;
  searchQuery: (searchTxt: string, pageNo: string, pageSize: string, extra?: ExtraSearchParams) => string;
  multiQueries?: (searchTxt: string, pageNo: string, pageSize: string) => Promise<AxiosResponse<any>>[];
  setQueriesResult?: (data: any[]) => void;
  renderer: (item: any, index: number) => JSX.Element;
  normalize?: (data: any) => any;
  refresh?: () => void;
  forceRefresh?: boolean;
  ref?: any;
  updateSearchParent?: () => void;
  showRemoveTextBtn?: boolean;
  renderInRowContent?: () => JSX.Element;
  showSearchComponent?: boolean;
  showPageNoFilter?: boolean;
  showResultComponent?: boolean;
  getSearchData?: (data: any[]) => void;
  aggregateOptions?: SearchFilter[];
  allowMultipleFilters?: boolean;
  customCss?: CSSProperties;
  defaultFilters?: string[];
}

interface SearchState {
  Search: string;
  Page: string;
  PageSize: string;
  queryTime: number;
  TotalPageNo: number;
}

export interface SearchRef {
  refetchResult: () => Promise<void>;
  searchComponent: () => JSX.Element;
  filterComponent: () => JSX.Element;
  getSearchData: () => any[];
  resultComponent: (wrapGrid?: boolean) => JSX.Element;
}

const nBOfHitsPerPage = ['72'];

const searchStateToURL = (searchState: any, currentHref: string, searchParamsQuery: any) => {
  // Prevent the trailing ? when searchState is empty.
  const isSearchStateEmpty = Object.keys(searchState).length === 0;
  if (isSearchStateEmpty) {
    return currentHref;
  }
  const hasQuery = currentHref.includes('?');
  const searchParams = new URLSearchParams(searchParamsQuery ?? '');
  for (const key in searchState) {
    searchParams.set(key, searchState[key]);
  }

  return hasQuery ? `${currentHref}&${searchParams.toString()}` : `${currentHref}?${searchParams.toString()}`;
};

const SearchWithApi: FC<SearchProps> = forwardRef<SearchRef, SearchProps>(
  (
    {
      placeHolder,
      searchQuery,
      setQueriesResult,
      multiQueries,
      renderer,
      normalize,
      refresh,
      updateSearchParent,
      getSearchData,
      renderInRowContent,
      showRemoveTextBtn = false,
      showSearchComponent = true,
      showPageNoFilter = true,
      forceRefresh,
      showResultComponent = true,
      aggregateOptions = [],
      allowMultipleFilters = false,
      customCss,
      defaultFilters,
    },
    ref
  ) => {
    if (!normalize) {
      normalize = (data) => data;
    }
    // change nb of hits per page depending of index (grid)
    const [searchState, setSearchState] = useReducer(
      (state: SearchState, updatedState: Partial<SearchState>) => ({
        ...state,
        ...updatedState,
        queryTime: state.queryTime + 1,
      }),
      { Search: '', Page: '1', TotalPageNo: 0, PageSize: nBOfHitsPerPage[0], queryTime: 0 }
    );

    const [searchData, setSearchData] = useState([]);

    const [aggregate, setAggregate] = useState('All');
    const [multipleAggregate, setMultipleAggregate] = useState<string[]>(['All']);

    const initialSearchParams = useRef(null);

    const api = useApi();
    const { t } = useTranslation('common');

    const fetchSearchData = async (url: string, searchState?: SearchState) => {
      try {
        const promises = [api.get(url)];
        if (multiQueries) {
          promises.push(...multiQueries(searchState.Search, searchState.Page, searchState.PageSize));
        }
        const fetchedData = await Promise.all(promises);
        const data = fetchedData.shift().data;
        setQueriesResult && setQueriesResult(fetchedData);
        setSearchData(data);
        getSearchData && normalize && getSearchData(normalize(data));
        updateSearchParent && updateSearchParent();
      } catch (e) {
        console.warn('Err While Searching With API:: ', e);
      }
    };
    const searchApi = useMemo(() => debounce(fetchSearchData, 600), []);

    useImperativeHandle(
      ref,
      () => ({
        refetchResult: () => {
          return forceSearch();
        },
        searchComponent: () => {
          return searchComponent();
        },
        getSearchData: () => {
          return searchData;
        },
        filterComponent: () => {
          return filterComponent();
        },
        resultComponent: (wrapGrid) => {
          return resultComponent(wrapGrid);
        },
      }),
      [searchState.queryTime, searchData]
    );

    const forceSearch = async () => {
      const queryURL = searchQuery(searchState.Search, searchState.Page, searchState.PageSize, {
        aggregateOption: allowMultipleFilters ? multipleAggregate : aggregate,
      });
      fetchSearchData(queryURL, searchState);
    };

    const replaceURL = () => {
      const updatedSearchState = Object.assign({}, searchState);
      delete updatedSearchState.queryTime;
      delete updatedSearchState.TotalPageNo;
      if (initialSearchParams.current !== location.search && location.search) {
        initialSearchParams.current = location.search;
      }
      const currentHref = location.pathname;
      const updatedHref = searchStateToURL(updatedSearchState, currentHref, initialSearchParams.current);
      if (currentHref === updatedHref) {
        return;
      }
      history.replaceState(null, '', updatedHref);
    };

    const searchComponent = (): JSX.Element => {
      return (
        <div className="ais-SearchBox" tw="relative" css={renderInRowContent && tw`flex-1 `}>
          <form
            noValidate={true}
            className="ais-SearchBox-form"
            action=""
            role="search"
            onSubmit={(e) => {
              e.preventDefault();
              forceSearch();
            }}
          >
            <input
              type="search"
              placeholder={placeHolder ?? t('action.searchHere')}
              autoComplete="off"
              autoCorrect="off"
              autoCapitalize="off"
              spellCheck="false"
              required={true}
              maxLength={512}
              onChange={(e) => {
                e.preventDefault();
                setSearchState({ Search: e.target.value });
              }}
              className="ais-SearchBox-input"
              tw="rounded-[20px] pl-9 pr-4 border-[#D1D1D1] placeholder:text-[#868686] bg-white"
            />
            <button
              type="submit"
              title="Submit your search query."
              className="ais-SearchBox-submit"
              tw="absolute left-3 top-[0.35rem]"
            >
              <Icon icon="ion:search-outline" tw="text-gray-500 w-5 h-5 mt-1 font-bold" />
            </button>
            {showRemoveTextBtn && (
              <button
                type="reset"
                title="Clear the search query."
                className="ais-SearchBox-reset"
                hidden={true}
                tw="absolute right-3 top-[0.31rem] hidden"
              >
                <Icon icon="basil:cross-outline" tw="text-gray-500 w-8 h-8" />
              </button>
            )}
          </form>
        </div>
      );
    };

    const filterComponent = (): JSX.Element => {
      return '';
    };

    const resultComponent = (wrapGrid: boolean = true): JSX.Element => {
      if (aggregateOptions.length > 0) {
        return <Grid tw="mt-4">{normalize(searchData as unknown[])?.map(renderer)}</Grid>;
      }
      if (!wrapGrid) {
        return <>{normalize(searchData as unknown[])?.map(renderer)}</>;
      }
      return <Grid tw="mt-2">{normalize(searchData as unknown[])?.map(renderer)}</Grid>;
    };

    useEffect(() => {
      const queryURL = searchQuery(searchState.Search, searchState.Page, searchState.PageSize, {
        aggregateOption: allowMultipleFilters ? multipleAggregate : aggregate,
      });
      replaceURL();
      if (searchState.queryTime === 0) {
        fetchSearchData(queryURL, searchState);
        return;
      }
      searchApi(queryURL, searchState);
      return () => {};
    }, [searchState, forceRefresh, aggregate, multipleAggregate]);

    const handleSelect = (filter: string) => {
      if (allowMultipleFilters) {
        setMultipleAggregate((prevFilters) => {
          if (filter === 'All') {
            return ['All'];
          }

          if (prevFilters.includes(filter)) {
            // If the filter is already in the array, remove it. If empty, select All by default.
            const updatedFilters = prevFilters?.filter((f) => f !== filter);
            return updatedFilters?.length > 0 ? updatedFilters : ['All'];
          } else {
            const updatedFilters = prevFilters?.filter((f) => f !== 'All');
            // If the filter is not in the array, add it
            return [...updatedFilters, filter];
          }
        });
      } else {
        setAggregate(filter);
      }
    };

    useEffect(() => {
      // set default filters
      defaultFilters?.map((f) => handleSelect(f));
    }, []);

    const isFilterSelected = (filter: SearchFilter) => {
      if (allowMultipleFilters) {
        if (filter?.children) {
          return filter?.children.some((c) =>
            multipleAggregate.includes(typeof c.value === 'object' && c.value !== null ? c?.value?.id : c?.value)
          );
        }

        return multipleAggregate?.includes(
          typeof filter?.value === 'object' && filter?.value !== null ? filter?.value?.id : filter?.value
        );
      } else {
        if (filter?.children) {
          return (
            filter?.children.findIndex(
              (c) => (typeof c.value === 'object' && c.value !== null ? c?.value?.id : c?.value) === aggregate
            ) !== -1
          );
        }

        return (
          aggregate ===
          (typeof filter?.value === 'object' && filter?.value !== null ? filter?.value?.id : filter?.value)
        );
      }
    };

    return (
      <>
        <div style={customCss}>
          <div
            className={`${showSearchComponent && 'container-options container-header '}`}
            id="search-header"
            tw="mb-5"
            css={renderInRowContent && tw`flex gap-4`}
          >
            {showSearchComponent &&
              ((searchData.length > 0 && searchState.Search === '') || searchState.Search) &&
              searchComponent()}

            {showPageNoFilter && filterComponent()}
            {renderInRowContent && renderInRowContent()}
          </div>
          {aggregateOptions.length > 0 && (
            <div tw="flex flex-wrap gap-2">
              <div
                onClick={(e) => {
                  e.preventDefault();
                  handleSelect('All');
                }}
                tw="cursor-pointer px-2 py-1 rounded-full hocus:bg-[#EBF1FA] min-w-[max-content] border border-solid border-gray-200 text-sm"
                css={isFilterSelected({ label: t('all'), value: 'All' }) ? tw`bg-[#EBF1FA]` : tw`bg-white `}
              >
                {t('all')}
              </div>
              {aggregateOptions.map((item, i) => {
                if (item?.children) {
                  return (
                    <div key={item?.value}>
                      <Menu>
                        <MenuButton
                          tw="cursor-pointer px-2 py-1 rounded-full hocus:bg-[#EBF1FA] min-w-[max-content] border border-solid border-gray-200 text-sm"
                          css={isFilterSelected(item) ? tw`bg-[#EBF1FA]` : tw`bg-white`}
                          key={`${item?.value}-${isFilterSelected(item)}`} //Trick to get the background to change instantly
                        >
                          {item?.label}
                          <Icon icon="tabler:chevron-down" tw="w-4 h-4 mr-0" />
                        </MenuButton>
                        <MenuList
                          portal={false}
                          className="post-manage-dropdown"
                          tw="absolute rounded-lg divide-y divide-gray-300 text-center text-sm bg-[#FAFAFA] mt-2"
                        >
                          {item?.children.map((child) => {
                            // check if child value is an object, and render a special input with image and name (for containers filter)
                            return typeof child.value === 'object' && child.value !== null ? (
                              <MenuItem
                                onSelect={() => handleSelect(child?.value?.id)}
                                tw="flex items-center italic text-[13px]"
                                key={child?.label}
                              >
                                <input
                                  key={child?.label}
                                  tw="size-3 text-[#454545]"
                                  type="radio"
                                  checked={isFilterSelected(child)}
                                  onChange={() => handleSelect(child?.value?.id)}
                                />
                                <img
                                  tw="size-6 rounded border object-cover object-center mr-1"
                                  src={child?.value.banner_url_sm || entityItem[child?.value.type].default_banner}
                                />
                                {t(entityItem[child?.value.type].translationId)}&nbsp;-&nbsp;
                                <span title={child?.value?.title} tw="font-bold overflow-hidden text-ellipsis">
                                  {child?.value.title}
                                </span>
                              </MenuItem>
                            ) : (
                              // else just return the basic input option (for location dropdown for ex)
                              <MenuItem
                                onSelect={() => handleSelect(child?.value)}
                                tw="flex items-center"
                                key={child?.label}
                              >
                                <input
                                  key={child?.label}
                                  tw="size-3 text-[#454545]"
                                  type="radio"
                                  checked={isFilterSelected(child)}
                                  onChange={() => handleSelect(child?.value)}
                                />
                                {child?.label}
                              </MenuItem>
                            );
                          })}
                        </MenuList>
                      </Menu>
                    </div>
                  );
                }

                return (
                  <div
                    key={item?.value}
                    onClick={(e) => {
                      e.preventDefault();
                      handleSelect(item.value);
                    }}
                    tw="cursor-pointer px-2 py-1 rounded-full hocus:(bg-[#EBF1FA]) min-w-[max-content] border border-solid border-gray-200 text-sm"
                    css={isFilterSelected(item) ? tw`bg-[#EBF1FA]` : tw`bg-white `}
                  >
                    {item.label}
                  </div>
                );
              })}
            </div>
          )}
          {showResultComponent && resultComponent()}
        </div>
      </>
    );
  }
);

export default SearchWithApi;
