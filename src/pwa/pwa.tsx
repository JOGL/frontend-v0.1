import { ReactNode, createContext, useContext, useEffect, useState } from 'react';

export interface PwaContext {
  showPwaAlert: boolean;
  handleInstallPwa: () => void;
  showIosPwaAlert: boolean;
  manualInstallInfo: boolean;
}

const Pwa = createContext<PwaContext>({
  showPwaAlert: false,
  handleInstallPwa: () => {},
  showIosPwaAlert: false,
  manualInstallInfo: false,
});

interface BeforeInstallPromptEvent extends Event {
  readonly platforms: string[];
  readonly userChoice: Promise<{
    outcome: 'accepted' | 'dismissed';
    platform: string;
  }>;
  prompt(): Promise<void>;
}

interface PwaProviderProps {
  children: ReactNode;
}

export const PwaProvider = ({ children }: PwaProviderProps) => {
  const [prompt, setPrompt] = useState<BeforeInstallPromptEvent | null>(null);
  const [showPwaAlert, setShowPwaAlert] = useState(false);

  const isIos = typeof window !== 'undefined' && /iphone|ipad|ipod/.test(window.navigator.userAgent.toLowerCase());
  const isInStandaloneMode =
    typeof window !== 'undefined' &&
    (('standalone' in window.navigator && window.navigator.standalone) ||
      window.matchMedia('(display-mode: standalone)').matches);

  const isBrowserSupportingPwa =
    typeof window !== 'undefined' &&
    'serviceWorker' in navigator &&
    (window.location.protocol === 'https:' || window.location.hostname === 'localhost');

  const showIosPwaAlert = isBrowserSupportingPwa && isIos && !isInStandaloneMode && !showPwaAlert;
  const manualInstallInfo = isBrowserSupportingPwa && !isInStandaloneMode && !showIosPwaAlert;

  useEffect(() => {
    const handleBeforeInstallPrompt = (event: BeforeInstallPromptEvent) => {
      event.preventDefault();
      setPrompt(event);
      if (!window.matchMedia('(display-mode: standalone)').matches) {
        setShowPwaAlert(true);
      }
    };
    window.addEventListener('beforeinstallprompt', handleBeforeInstallPrompt);

    return () => {
      window.removeEventListener('beforeinstallprompt', handleBeforeInstallPrompt);
    };
  }, []);

  const handleInstallPwa = () => {
    if (prompt) {
      prompt.prompt();
      prompt.userChoice.then((choiceResult) => {
        if (choiceResult.outcome === 'accepted') {
          setShowPwaAlert(false);
        }
        setPrompt(null);
      });
    }
  };

  return (
    <Pwa.Provider value={{ showPwaAlert, handleInstallPwa, showIosPwaAlert, manualInstallInfo }}>
      {children}
    </Pwa.Provider>
  );
};

export function usePwa() {
  return useContext(Pwa);
}
