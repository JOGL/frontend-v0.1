import { create } from 'zustand';

interface EditorFocusStore {
  isEditorFocused: boolean;
  setEditorFocused: (focused: boolean) => void;
}

export const useEditorFocusStore = create<EditorFocusStore>((set) => ({
  isEditorFocused: false,
  setEditorFocused: (focused) => set({ isEditorFocused: focused }),
}));
