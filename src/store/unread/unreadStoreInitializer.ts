import { useEffect } from 'react';
import { useHubDiscussionsStore } from '../hub-discussions/hubDiscussionsStoreProvider';
import { useUnreadStore } from './unreadStore';
import { objType } from './unreadStore.types';

export const UnreadStoreInitializer = () => {
  const { selectedHubDiscussion: selectedHub, selectedMenuItem } = useHubDiscussionsStore((store) => ({
    selectedHubDiscussion: store?.selectedHubDiscussion,
    selectedMenuItem: store?.selectedMenuItem,
  }));

  const { reloadPrimaryMenu, reloadSecondaryMenu} = useUnreadStore((store) => ({
    reloadPrimaryMenu: store?.reloadPrimaryMenu,
    reloadSecondaryMenu: store?.reloadSecondaryMenu,
  }));

  useEffect(() => {
    if (selectedHub) {
      reloadPrimaryMenu(selectedHub);
    }
  }, [selectedHub?.id, selectedMenuItem]); 

  useEffect(() => {
    if (selectedHub && selectedMenuItem) {
      reloadSecondaryMenu(selectedHub, selectedMenuItem as objType);
    }
  }, [selectedHub?.id, selectedMenuItem]);

  return null;
};