import { create } from 'zustand';
import { persist } from 'zustand/middleware';
import { objType, UnreadStore } from './unreadStore.types';
import  { getDocumentViewFilter } from '~/src/components/documents/useDocuments';
import { getPaperViewFilter } from '~/src/components/papers/usePapers';
import  { getNeedViewFilter } from '~/src/components/Need/useNeeds';
import api from '~/src/utils/api/api';
import { menuItems } from '~/src/components/Layout/menuItems/menuItems';
import { NodeFeedDataModel } from '~/__generated__/types';
import { getEventViewFilter } from '~/src/components/Event/useEvents';

const getKeyForHub = (object: string, selectedHubId: string | undefined, filterKey: string): string | null => {
  if (!object || !selectedHubId) return null;
  return `${object}-${selectedHubId}-${filterKey}`;
};

const getKeyForEntity = (object: string,  entityId: string): string | null => {
  if (!object) return null;
  return `${object}-${entityId}`;
};

const getHubHasNew = async (
  nodeId: string,
  object: objType,
  key: string
): Promise<boolean> => {
  switch (object) {
    case 'documents':
      const documentFilter = getDocumentViewFilter(key);
      const documentRes = await api.nodes.documentsAggregateNewDetail(nodeId, {
        filter: documentFilter.filter,
      });
      return documentRes.data;
    case 'papers':
      const paperFilter = getPaperViewFilter(key);
      const paperRes = await api.nodes.papersAggregateNewDetail(nodeId, {
        filter: paperFilter.filter,
      });
      return paperRes.data;
    case 'needs':
      const needFilter = getNeedViewFilter(key);
      const needRes = await api.nodes.needsAggregateNewDetail(nodeId, {
        filter: needFilter.filter,
      });
      return needRes.data;
    case 'events':
      const eventFilter = getEventViewFilter(key);
      const eventRes = await api.nodes.eventsAggregateNewDetail(nodeId, {
        filter: eventFilter.filter,
      });
      return eventRes.data;
    case 'channels':
        const contentRes = await api.nodes.channelAggregateNewDetail(nodeId);
        return contentRes.data;
    case 'inbox':
        const inboxRes = await api.nodes.feedAggregateNewDetail(nodeId);
        return inboxRes.data;
    default:
      return false;
  }
};

const getEntityHasNew = async (entityId: string, object: objType): Promise<boolean> => {
  switch (object) {
    case 'documents':
      const documentRes = await api.documents.documentsAllNewDetail(entityId);
      return documentRes.data;
    case 'papers':
      const paperRes = await api.papers.papersNewDetail(entityId);
      return paperRes.data;
    case 'needs':
      const needRes = await api.needs.needsNewDetail(entityId);
      return needRes.data;
    case 'events':
      const eventRes = await api.events.eventsNewDetail(entityId);
      return eventRes.data;
    case 'channels':
      const contentRes = await api.feed.getFeed(entityId);
      return contentRes.data;
    default:
      return false;
  }
};

export const useUnreadStore = create<UnreadStore>()(
  persist(
    (set, get) => {
      const loadForHub = async (object: objType, node: NodeFeedDataModel, key = 'all') => {
        const entryKey = getKeyForHub(object, node.id, key);
        if (!entryKey) return;
        const isNew = await getHubHasNew(node.id, object, key);
        get().setMenuKeyNew(entryKey, isNew);
      };

      const loadForEntity = async (object: objType, id: string) => {
        const entryKey = getKeyForEntity(object, id);
        if (!entryKey) return;
        const isNew = await getEntityHasNew(id, object);
        get().setMenuKeyNew(entryKey, isNew);
      };

      return {
        newMenuKeys: new Map<string, boolean>(),
        loading: false,

        setMenuKeyNew: (key, isNew) => {
          set((state) => ({
            ...state,
            newMenuKeys:
              state.newMenuKeys instanceof Map
                ? new Map(state.newMenuKeys).set(key, isNew)
                : new Map<string, boolean>().set(key, isNew),
          }));
        },

        hasNewItemsEntity: (object: string, entityId: string): boolean => {
          const state = get();
          const key = getKeyForEntity(object, entityId);
          if (!key) return false;
          if (!(state.newMenuKeys instanceof Map)) return false;
          return state.newMenuKeys.get(key) ?? false;
        },

        hasNewItemsHub: (object: string, hubId: string | undefined, filterKey: string = 'all'): boolean => {
          if (!hubId) return false;
          const state = get();
          const key = getKeyForHub(object, hubId, filterKey);
          if (!key) return false;
          if (!(state.newMenuKeys instanceof Map)) return false;

          const keyArray = Array.from(state.newMenuKeys.keys()).filter((k) => k.startsWith(key));

          return keyArray.some((itemKey) => state.newMenuKeys.get(itemKey));
        },

        reloadSecondaryMenu: async (node, object: objType) => {
          if (!menuItems[object]) return;

          menuItems[object].secondaryMenuItems &&
            Object.keys(menuItems[object].secondaryMenuItems).forEach(async (key) => {
              if (key === 'recent' || key === 'created' || key === 'calendar') return;
              loadForHub(object, node, key);
            });

          switch (object) {
            case 'channels':
              node?.entities &&
                node.entities.forEach(async (e) => {
                  e.channels.forEach(async (c) => {
                    loadForEntity(object, c.id);
                  });
                });
              break;
            default:
              node?.entities &&
                node.entities.forEach(async (e) => {
                  loadForEntity(object, e.id);
                });
          }
        },

        reloadPrimaryMenu: async (node) => {
          loadForHub('documents', node);
          loadForHub('papers', node);
          loadForHub('needs', node);
          loadForHub('events', node);
          loadForHub('channels', node);
          loadForHub('inbox', node);
        },
      };
    },
    {
      name: 'unread-store',
      partialize: (state) => ({
      //   newMenuKeys: state.newMenuKeys,
      }),
    }
  )
);