import { NodeFeedDataModel } from '~/__generated__/types';

export type objType = 'documents' | 'papers' | 'needs' | 'events' | 'channels' | 'inbox';

export interface UnreadStoreState {
  newMenuKeys: Map<string, boolean>;
  loading: boolean;
}

export interface UnreadStore extends UnreadStoreState {
  setMenuKeyNew: (key: string, isNew: boolean) => void;
  hasNewItemsEntity: (object: string, entityId: string) => boolean;
  hasNewItemsHub: (object: string, hubId: string | undefined, filterKey?) => boolean;
  reloadSecondaryMenu: (node: NodeFeedDataModel, object: objType) => Promise<void>;
  reloadPrimaryMenu: (node: NodeFeedDataModel) => Promise<void>;
}