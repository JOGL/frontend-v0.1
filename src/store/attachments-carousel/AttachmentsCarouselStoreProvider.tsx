import { ReactNode, createContext, useRef, useContext } from 'react';
import { StoreApi, useStore } from 'zustand';

import { AttachmentsCarouselStore, createAttachmentsCarouselStore } from './attachmentsCarouselStore';

export const AttachmentsCarouselStoreContext = createContext<StoreApi<AttachmentsCarouselStore> | null>(null);

export interface AttachmentsCarouselStoreProviderProps {
  children: ReactNode;
}

export const AttachmentsCarouselStoreProvider = ({ children }: AttachmentsCarouselStoreProviderProps) => {
  const storeRef = useRef<StoreApi<AttachmentsCarouselStore>>();
  if (!storeRef.current) {
    storeRef.current = createAttachmentsCarouselStore();
  }

  return (
    <AttachmentsCarouselStoreContext.Provider value={storeRef.current}>
      {children}
    </AttachmentsCarouselStoreContext.Provider>
  );
};

export const useAttachmentsCarouselStore = <T,>(selector: (store: AttachmentsCarouselStore) => T): T => {
  const attachmentsCarouselStoreContext = useContext(AttachmentsCarouselStoreContext);

  if (!AttachmentsCarouselStoreContext) {
    throw new Error(`useAttachmentsCarouselStore must be use within AttachmentsCarouselStoreProvider`);
  }

  return useStore(attachmentsCarouselStoreContext, selector);
};
