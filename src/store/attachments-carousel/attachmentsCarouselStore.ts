import { createStore } from 'zustand';

interface AttachmentsCarouselState {
  cachedFiles: Record<string, string>;
}

interface AttachmentsCarouselActions {
  addFile: (key: string, file: string) => void;
  removeFile: (key: string) => void;
}
export type AttachmentsCarouselStore = AttachmentsCarouselState & AttachmentsCarouselActions;

const defaultInitState: AttachmentsCarouselState = { cachedFiles: {} };

export const createAttachmentsCarouselStore = (initState: AttachmentsCarouselState = defaultInitState) => {
  return createStore<AttachmentsCarouselStore>()((set, get) => ({
    ...initState,
    addFile: (key, file) => {
      set({ cachedFiles: { ...get().cachedFiles, [key]: file } });
    },
    removeFile: (key) => {
      delete get().cachedFiles[key];
    },
  }));
};
