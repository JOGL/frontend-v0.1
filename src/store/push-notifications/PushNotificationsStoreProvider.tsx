import { ReactNode, createContext, useRef, useContext } from 'react';
import { StoreApi, useStore } from 'zustand';

import { PushNotificationStore, createPushNotificationsStore } from './pushNotificationsStore';

export const PushNotificationStoreContext = createContext<StoreApi<PushNotificationStore> | null>(null);

export interface PushNotificationStoreProviderProps {
  children: ReactNode;
}

export const PushNotificationStoreProvider = ({ children }: PushNotificationStoreProviderProps) => {
  const storeRef = useRef<StoreApi<PushNotificationStore>>();
  if (!storeRef.current) {
    storeRef.current = createPushNotificationsStore();
  }

  return (
    <PushNotificationStoreContext.Provider value={storeRef.current}>{children}</PushNotificationStoreContext.Provider>
  );
};

export const usePushNotificationStore = <T,>(selector: (store: PushNotificationStore) => T): T => {
  const pushNotificationStoreContext = useContext(PushNotificationStoreContext);

  if (!PushNotificationStoreContext) {
    throw new Error(`usePushNotificationStore must be use within PushNotificationStoreProvider`);
  }

  return useStore(pushNotificationStoreContext, selector);
};
