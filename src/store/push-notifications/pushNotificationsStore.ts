import { FirebaseOptions, initializeApp } from 'firebase/app';
import { Messaging, getMessaging, isSupported } from 'firebase/messaging';
import { NextRouter } from 'next/router';
import { createStore } from 'zustand';
import { PUSH_NOTIFICATIONS_QUERY_PARAM } from '~/src/components/Notifications/PushNotificationsHandler';

interface PushNotificationsState {
  config: FirebaseOptions;
  isFirebaseSupported: boolean;
  messaging?: Messaging;
  serviceWorkerRegistration?: ServiceWorkerRegistration;
}

interface PushNotificationsActions {
  initialize: (serviceWorkerRegistration: ServiceWorkerRegistration) => Promise<void>;
  showPushNotificationModal: (router: NextRouter, redirectPath?: string) => Promise<void>;
}
export type PushNotificationStore = PushNotificationsState & PushNotificationsActions;

const defaultInitState: PushNotificationsState = {
  config: {
    apiKey: 'AIzaSyD0AXh20T7S8lo8Ut3o0FmSbBeP_sDGQe0',
    authDomain: 'jogl-389521.firebaseapp.com',
    projectId: 'jogl-389521',
    storageBucket: 'jogl-389521.appspot.com',
    messagingSenderId: '703224592857',
    appId: '1:703224592857:web:707e7fa1473123801081c8',
    measurementId: 'G-G77VY04D3P',
  },
  isFirebaseSupported: false,
};

export const isPushNotificationsStoreSupported = () => 'Notification' in window && 'PushManager' in window;

export const createPushNotificationsStore = (initState: PushNotificationsState = defaultInitState) => {
  return createStore<PushNotificationStore>()((set, get) => ({
    ...initState,
    initialize: async (serviceWorkerRegistration) => {
      const isFirebaseSupported = await isSupported();
      if (!isFirebaseSupported) {
        set({ isFirebaseSupported: false });
      }

      try {
        const app = initializeApp(get().config);
        const messaging = getMessaging(app);

        set({ isFirebaseSupported: true, messaging, serviceWorkerRegistration });
      } catch (error) {
        console.error('Failed to register service worker', error);
      }
    },
    showPushNotificationModal: ({ push, replace, query }, redirectPath) => {
      if (redirectPath) {
        return push(`${redirectPath}${redirectPath.includes('?') ? '&' : '?'}${PUSH_NOTIFICATIONS_QUERY_PARAM}=1`);
      } else {
        return replace({ query: { ...query, [PUSH_NOTIFICATIONS_QUERY_PARAM]: 1 } }, undefined, { shallow: true });
      }
    },
  }));
};
