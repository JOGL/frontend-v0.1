import { CommunityEntityChannelModel, NodeFeedDataModel } from '~/__generated__/types';
import { ReactNode } from 'react';

export interface HubDiscussionsStoreState {
  loading: boolean;
  hubDiscussions?: NodeFeedDataModel[];
  selectedHubDiscussion?: NodeFeedDataModel;
  selectedMenuItem?: string;
  selectedEntity?: CommunityEntityChannelModel;
  isOpen: boolean;
  isJOGLGlobal: boolean;
  openMenuKeys: string[];
}

export interface HubDiscussionsStoreActions {
  initialize: () => Promise<void>;
  setSelectedHubDiscussion: (hubDiscussion: NodeFeedDataModel) => void;
  setSelectedHubDiscussionWithId: (id: string) => void;
  setSelectedMenuItem: (menuItem: string) => void;
  setSelectedEntity: (entity: CommunityEntityChannelModel) => void;
  setIsOpen: (isOpen: boolean) => void;
  setOpenMenuKeys: (keys: string[]) => void;
}

export type HubDiscussionsStore = HubDiscussionsStoreState & HubDiscussionsStoreActions;

export interface HubDiscussionsStoreProviderProps {
  children: ReactNode;
}
