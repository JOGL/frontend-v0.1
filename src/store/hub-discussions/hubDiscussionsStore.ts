import { createStore } from 'zustand';
import { persist } from 'zustand/middleware';
import { CommunityEntityChannelModel } from '~/__generated__/types';
import { HubDiscussionsStore, HubDiscussionsStoreState } from '~/src/store/hub-discussions/hubDiscussionStore.types';
import api from '~/src/utils/api/api';
import cookie from 'js-cookie';

const defaultInitState: HubDiscussionsStoreState = {
  hubDiscussions: [],
  loading: false,
  isOpen: false,
  isJOGLGlobal: true,
  openMenuKeys: [],
  selectedMenuItem: 'inbox'
};

export const createHubDiscussionsStore = (initState: HubDiscussionsStoreState = defaultInitState) => {
  return createStore<HubDiscussionsStore>()(
    persist(
      (set) => ({
        ...initState,
        openMenuKeys: [],
        loading: false,
        initialize: async () => {
          if (!cookie.get('authorization')) return;

          set({ loading: true });
          try {
            const nodeResponse = await api.feed.nodesList();
            const hubs = nodeResponse.data;

            set((state) => {
              // Keep track of current selections before updating
              const currentSelectedHubId = state.selectedHubDiscussion?.id;
              const currentSelectedEntityId = state.selectedEntity?.id;

              // Find the same hub in the new data
              const updatedSelectedHub = currentSelectedHubId
                ? hubs.find((hub) => hub.id === currentSelectedHubId)
                : hubs[0];

              // Find the same entity in the updated hub
              const updatedSelectedEntity =
                currentSelectedEntityId && updatedSelectedHub
                  ? updatedSelectedHub.entities?.find(
                      (entity: CommunityEntityChannelModel) => entity?.id === currentSelectedEntityId
                    )
                  : undefined;

              return {
                ...state,
                hubDiscussions: hubs,
                selectedHubDiscussion: updatedSelectedHub,
                selectedEntity: updatedSelectedEntity,
                isJOGLGlobal: false,
              };
            });
          } finally {
            set({ loading: false });
          }
        },
        setOpenMenuKeys: (keys) => set({ openMenuKeys: keys }),
        setSelectedHubDiscussion: (hubDiscussion) => {
          set({ selectedHubDiscussion: hubDiscussion, isJOGLGlobal: false });
        },
        setSelectedHubDiscussionWithId: (id) => {
          set((state) => ({
            ...state,
            selectedHubDiscussion: state.hubDiscussions?.find((hub) => hub.id === id),
          }));
        },
        setSelectedMenuItem: (menuItem) =>
          set({
            selectedMenuItem: menuItem,
          }),
        setSelectedEntity: (entity) =>
          set({
            selectedEntity: entity,
          }),
        setIsOpen: (isOpen) => {
          set({ isOpen: isOpen });
        },
      }),
      {
        partialize: (state) => ({
          selectedHubDiscussion: state.selectedHubDiscussion,
          selectedMenuItem: state.selectedMenuItem,
          isOpen: state.isOpen,
          isJOGLGlobal: state.isJOGLGlobal,
          openMenuKeys: state.openMenuKeys,
        }),
        name: 'hub-store',
      }
    )
  );
};
