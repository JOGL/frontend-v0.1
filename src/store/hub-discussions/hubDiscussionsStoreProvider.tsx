import { createContext, useRef, useContext } from 'react';
import { StoreApi, useStore } from 'zustand';
import {
  HubDiscussionsStore,
  HubDiscussionsStoreProviderProps,
} from '~/src/store/hub-discussions/hubDiscussionStore.types';
import { createHubDiscussionsStore } from './hubDiscussionsStore';

export const HubDiscussionsStoreContext = createContext<StoreApi<HubDiscussionsStore> | null>(null);

export const HubDiscussionsStoreProvider = ({ children }: HubDiscussionsStoreProviderProps) => {
  const storeRef = useRef<StoreApi<HubDiscussionsStore>>();
  if (!storeRef.current) {
    storeRef.current = createHubDiscussionsStore();
    storeRef.current.getState().initialize();
  }

  return <HubDiscussionsStoreContext.Provider value={storeRef.current}>{children}</HubDiscussionsStoreContext.Provider>;
};

export const useHubDiscussionsStore = <T,>(selector: (store: HubDiscussionsStore) => T): T => {
  const hubDiscussionsStoreContext = useContext(HubDiscussionsStoreContext);

  if (!hubDiscussionsStoreContext) {
    throw new Error(`hubDiscussionsStore must be used within HubDiscussionsStoreProvider`);
  }

  return useStore(hubDiscussionsStoreContext, selector);
};
