import { AxiosInstance } from 'axios';
import React, { useContext, useMemo } from 'react';
import getApi from '~/src/utils/getApi';
import useTranslation from 'next-translate/useTranslation';
import { useAuth } from '~/auth/auth';

type ApiContextType = AxiosInstance;
export const ApiContext = React.createContext<ApiContextType | undefined>(
  undefined // default value
);
// The credentials cookies come from the next-cookies in _app.js

export const ApiProvider = ({ children }) => {
  const { accessToken } = useAuth();
  const { t } = useTranslation('common');
  const api = useMemo(() => getApi({ authorization: accessToken, t }), [accessToken]);

  return <ApiContext.Provider value={api}>{children}</ApiContext.Provider>;
};
// HOC to use it in legacy class component code.
export function withApi(Component) {
  return function ApiComponent(props) {
    return <ApiContext.Consumer>{(api) => <Component {...props} api={api} />}</ApiContext.Consumer>;
  };
}

// Custom hook to use it fast and clean.
// This is the preferred way of using it.
export const useApi = () => {
  const api = useContext(ApiContext);

  if (!api) {
    throw new Error('Api instance is not available!');
  }

  return api;
};
