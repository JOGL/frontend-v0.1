import cookie from 'js-cookie';
import Router from 'next/router';
import React, { createContext, FC, ReactNode, useCallback, useEffect, useMemo, useState } from 'react';
import Swal from 'sweetalert2';
import { useApi } from '~/src/contexts/apiContext';
import { User } from '~/src/types';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import { usePushNotificationStore } from '~/src/store/push-notifications/PushNotificationsStoreProvider';
import CustomLoader from '~/src/components/Tools/CustomLoader';
import { isPushNotificationsStoreSupported } from '~/src/store/push-notifications/pushNotificationsStore';
import { useAuth } from '~/auth/auth';
import { AuthResultExtendedModel, Auth } from '~/__generated__/types';
import { useQuery } from '@tanstack/react-query';
import api from '../utils/api/api';
import { QUERY_KEYS } from '../utils/api/queryKeys';

//TODO remove all any types

// Context types
interface IUserContext {
  frontLogout: (redirectPath?: string, modalOnly?: boolean) => void;
  signUp: (user: any) => Promise<AuthResultExtendedModel>;
  logout: (redirectPath?: any) => Promise<void>;
  authOrcid: (authorization_code: string, screen: string) => Promise<AuthResultExtendedModel>;
  authGoogle: (access_token: string) => Promise<AuthResultExtendedModel>;
  authLinkedIn: (authorization_code: string) => Promise<AuthResultExtendedModel>;
  user: User | undefined;
  userLoading: false;
}

// Context, with default value set as undefined
export const UserContext = createContext<IUserContext | undefined>(undefined);
// associated useContext is written in each component

export const frontLogout = (redirectPath = '/', modalOnly = false) => {
  // Delete all the auth cookies
  cookie.remove('authorization');
  cookie.remove('userId');
  // to support logging out from all windows
  // This will work with NextJS since it's call in the browser only.
  typeof window !== 'undefined' && window.localStorage.setItem('logout', Date.now().toString());
  // Router.push(redirectPath);
};

// Provider types
interface ProviderProps {
  children: ReactNode;
}

// Provider
const UserProvider: FC<ProviderProps> = ({ children }) => {
  const oldApi = useApi();
  const { accessToken, setAccessToken, userId, setUserId, onLoginUser } = useAuth();

  const { data: user, isFetching } = useQuery({
    queryKey: [QUERY_KEYS.userData, userId],
    queryFn: async () => {
      if (!userId) return;
      const response = await api.users.usersDetailDetail(userId);
      return response.data;
    },
    enabled: !!userId,
    refetchOnMount: false,
    refetchOnWindowFocus: false,
  });
  const initialize = usePushNotificationStore((s) => s.initialize);
  const [registeredServiceWorkerFor, setRegisteredServiceWorkerFor] = useState<string>('');

  useEffect(() => {
    if (accessToken !== registeredServiceWorkerFor) {
      // This code needs to be executed before the first render, so that the service worker
      // for downloads has immediate access to the authorization header.
      if ('serviceWorker' in navigator) {
        navigator.serviceWorker
          .register(`/firebase-messaging-sw.js?authorization=${accessToken}`)
          // An unregistered SW will still work until the page is reloaded.
          // But if we don't unregister here, this component may get stuck on page reload,
          // because the SW is still fetching the video/audio and not done yet.
          .then(async (registration) => {
            if (isPushNotificationsStoreSupported()) {
              initialize(registration);
            }
            await registration.update();
            // TODO: turns out we can't unregister the service worker because the firebase-messaging logic stops working.
            // If users report pages hanging when refreshing the app where a big file/video is present we need to find a solution.
            // return registration.unregister();
          })
          .catch((error) => console.error('Failed to register service worker', error))
          .finally(() => setRegisteredServiceWorkerFor(accessToken));
      } else {
        setRegisteredServiceWorkerFor(accessToken);
      }
    }
  }, [registeredServiceWorkerFor, accessToken, initialize]);

  const logout = useCallback(
    (redirectPath = '/signin') => {
      typeof window !== 'undefined' && window.localStorage.clear(); // clear local storage
      frontLogout(redirectPath);
      setAccessToken('');
      setUserId('');
      //TODO implement "you have been logged out"  here
      Router.push('/signin');
    },
    [oldApi]
  );

  const signUp = useCallback(
    async (user) =>
      new Promise((resolve, reject) => {
        oldApi
          .post('/users', user)
          .then((res) => {
            resolve(res.data);
          })
          .catch((error) => {
            reject(error);
          });
      }),
    [oldApi]
  );

  const authOrcid = useCallback(
    async (authorization_code: string, screen: string): Promise<Auth.OrcidCreate.ResponseBody> => {
      return new Promise((resolve, reject) => {
        oldApi
          .post('/auth/orcid', { authorization_code, screen })
          .then((res) => {
            onLoginUser(res.data);
            resolve(res.data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    [oldApi, onLoginUser]
  );

  const authGoogle = useCallback(
    async (access_token: string): Promise<Auth.GoogleCreate.ResponseBody> => {
      return new Promise((resolve, reject) => {
        oldApi
          .post('/auth/google', { access_token })
          .then((res) => {
            onLoginUser(res.data);
            resolve(res.data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    [oldApi, onLoginUser]
  );

  const authLinkedIn = useCallback(
    async (authorization_code: string): Promise<Auth.LinkedinCreate.ResponseBody> => {
      return new Promise((resolve, reject) => {
        oldApi
          .post('/auth/linkedin', { authorization_code })
          .then((res) => {
            onLoginUser(res.data);
            resolve(res.data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    [oldApi, onLoginUser]
  );

  const providerValue = useMemo(
    () => ({
      frontLogout,
      signUp,
      logout,
      authOrcid,
      authGoogle,
      authLinkedIn,
      user,
      userLoading: isFetching,
    }),
    [logout, user]
  );
  // Values stored here passed on to all children components of the app, wrapped inside the present context in _app.tsx

  if (registeredServiceWorkerFor !== accessToken) return <CustomLoader />;

  return <UserContext.Provider value={providerValue}>{children}</UserContext.Provider>;
};

export default UserProvider;
