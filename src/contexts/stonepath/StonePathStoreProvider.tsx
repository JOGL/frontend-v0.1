import { createContext, useContext, useState, useCallback, useMemo } from 'react';
import { StonePathStore, StonePathStoreProviderProps } from './stonePathStore.types';
import { EntityMiniModel } from '~/__generated__/types';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';

const StonePathStoreContext = createContext<StonePathStore | undefined>(undefined);

export const StonePathStoreProvider = ({ children }: StonePathStoreProviderProps) => {
  const [path, setPath] = useState<EntityMiniModel[]>([]);
  const { hubDiscussions, setSelectedHubDiscussion } = useHubDiscussionsStore((store) => ({
    hubDiscussions: store.hubDiscussions,
    setSelectedHubDiscussion: store.setSelectedHubDiscussion,
  }));

  const setStonePath = useCallback(
    (path: EntityMiniModel[] = []) => {
      setPath(path);
      if (path[0]?.entity_type === 'node' && path[0]?.id) {
        const hub = hubDiscussions?.find((h) => h.id === path[0].id);
        if (hub) {
          setSelectedHubDiscussion(hub);
        }
      }
    },
    [hubDiscussions, setSelectedHubDiscussion]
  );

  const value = useMemo(() => ({ stonePath: path, setStonePath }), [path, setStonePath]);

  return <StonePathStoreContext.Provider value={value}>{children}</StonePathStoreContext.Provider>;
};

export const useStonePathStore = () => {
  const context = useContext(StonePathStoreContext);

  if (!context) {
    throw new Error('useStonePathStore must be used within a StonePathStoreProvider');
  }

  return context;
};
