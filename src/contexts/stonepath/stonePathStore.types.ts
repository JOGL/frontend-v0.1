import { ReactNode } from 'react';
import { EntityMiniModel } from '~/__generated__/types';

export interface StonePathStore {
  stonePath?: EntityMiniModel[];
  setStonePath: (path: EntityMiniModel[]) => void;
}

export interface StonePathStoreProviderProps {
  children: ReactNode;
}
