import React, { createContext, Dispatch, SetStateAction, useCallback, useContext, useMemo, useState } from 'react';
import Modal from 'react-modal';
import P from '~/src/components/primitives/P';
import { NotLoggedInModal } from '~/src/utils/getApi';
import styles from './modalContext.module.scss';
import Icon from '~/src/components/primitives/Icon';

interface ShowModal {
  title: string;
  maxWidth?: string;
  showTitle?: boolean;
  showCloseButton?: boolean;
}

export interface INotLoggedInModalContext {
  setIsOpen: Dispatch<SetStateAction<boolean>>;
  closeModal: (value?: boolean) => void;
  showModal: ({ maxWidth, title, showCloseButton, showTitle }: ShowModal) => void;
  isOpen: boolean;
}

// Required as mentioned in the docs.
Modal.setAppElement('#__next');
const NotLoggedInModalContext = createContext<INotLoggedInModalContext | undefined>(undefined);

export function NotLoggedInModalProvider({ children }) {
  const [isOpen, setIsOpen] = useState(false);
  const [contentLabel, setContentLabel] = useState(undefined);
  const [showTitle, setShowTitle] = useState<boolean>(true);
  const [modalTitle, setModalTitle] = useState<undefined | string>(undefined);
  const [showCloseButton, setShowCloseButton] = useState<boolean>(true);
  const [maxWidth, setMaxWith] = useState<string>('30rem');

  const showModal = useCallback(({ maxWidth, title, showCloseButton = true, showTitle = true }: ShowModal) => {
    setIsOpen(true);
    setModalTitle(title);
    setShowTitle(showTitle);
    setShowCloseButton(showCloseButton);
    setContentLabel(title);
    setMaxWith(maxWidth);
  }, []);
  const closeModal = (value: boolean = true) => setIsOpen(!value);
  // Memoize value so it doesn't re-render the tree
  const value: INotLoggedInModalContext = useMemo(() => ({ showModal, isOpen, closeModal }), [
    closeModal,
    showModal,
    isOpen,
  ]);

  return (
    <NotLoggedInModalContext.Provider value={value}>
      <Modal
        isOpen={isOpen}
        contentLabel={contentLabel}
        onRequestClose={closeModal}
        className={styles.modal}
        overlayClassName={styles.overlay}
      >
        <div tw="flex flex-col w-[80vw]" css={{ maxWidth: `${maxWidth}` }}>
          {(showTitle || showCloseButton) && (
            <div tw="flex justify-between items-center p-4 pr-1 border-b border-[lightgrey]">
              <P tw="flex-grow color[#212529] font-medium fontSize[1.3rem] mb-0">{showTitle && modalTitle}</P>
              {showCloseButton && (
                <button tw="flex flex-col pr-1" onClick={closeModal}>
                  <Icon icon="material-symbols:close" tw="text-[#868686] hover:text-[#212529]" />
                </button>
              )}
            </div>
          )}
          <div tw="flex flex-col p-4">
            <NotLoggedInModal hideModal={closeModal} />
          </div>
        </div>
      </Modal>
      {children}
    </NotLoggedInModalContext.Provider>
  );
}

export const useNotLoggedInModal = () => useContext(NotLoggedInModalContext);
export default NotLoggedInModalContext;
