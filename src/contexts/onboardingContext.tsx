import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useAuth } from '~/auth/auth';
import api from '~/src/utils/api/api';

export default function OnboardingContext({ children }) {
  const router = useRouter();
  const { isLoggedIn } = useAuth(); 
  const storageKey = 'onboardingCompleted';
  
  useEffect(() => {
    const checkOnboardingStatus = async () => {
      const onboardingCompleted = localStorage.getItem(storageKey);
      if (!onboardingCompleted) {
        const response = await api.users.onboardingStatusList();
        if (response.data) {
          localStorage.setItem(storageKey, 'true');
        } else {
          router.replace({ pathname: `/onboarding`, query: router.query });
        }
      }
    };

    if (!isLoggedIn || router.pathname === '/onboarding') {
      return;
    }

    checkOnboardingStatus();
  }, [router, isLoggedIn]);

  return children;
}