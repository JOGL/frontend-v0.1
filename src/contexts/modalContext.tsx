import { Router } from 'next/router';
import React, {
  createContext,
  Dispatch,
  ReactNode,
  SetStateAction,
  useCallback,
  useContext,
  useMemo,
  useState,
} from 'react';
import Modal from 'react-modal';
import P from '~/src/components/primitives/P';
import styled from '~/src/utils/styled';
import styles from './modalContext.module.scss';
import Icon from '~/src/components/primitives/Icon';

interface ShowModal {
  children: ReactNode;
  title?: string;
  maxWidth?: string;
  showTitle?: boolean;
  showCloseButton?: boolean;
  modalClassName?: string;
  shouldCloseOnEsc?: boolean;
  shouldCloseOnOverlayClick?: boolean;
}

export interface IModalContext {
  setModalChildren: Dispatch<SetStateAction<ReactNode>>;
  setIsOpen: Dispatch<SetStateAction<boolean>>;
  closeModal: (value?: boolean) => void;
  showModal: ({ children, maxWidth, title, showCloseButton, showTitle, modalClassName }: ShowModal) => void;
  isOpen: boolean;
}

// Required as mentioned in the docs.
Modal.setAppElement('#__next');
const ModalContext = createContext<IModalContext | undefined>(undefined);

export function ModalProvider({ children }) {
  const [isOpen, setIsOpen] = useState(false);
  const [modalChildren, setModalChildren] = useState<ReactNode>(
    <div>You haven\&apos;t provided any modalChildren</div>
  );
  const [contentLabel, setContentLabel] = useState(undefined);
  const [showTitle, setShowTitle] = useState<boolean>(true);
  const [modalTitle, setModalTitle] = useState<undefined | string>(undefined);
  const [showCloseButton, setShowCloseButton] = useState<boolean>(true);
  const [maxWidth, setMaxWith] = useState<string>('30rem');
  const [modalClassName, setModalClassName] = useState<string>('');
  const [shouldCloseOnEsc, setShouldCloseOnEsc] = useState<boolean>(true);
  const [shouldCloseOnOverlayClick, setShouldCloseOnOverlayClick] = useState<boolean>(true);

  const showModal = useCallback(
    ({
      children,
      maxWidth = '30rem',
      title,
      showCloseButton = true,
      showTitle = true,
      modalClassName,
      shouldCloseOnEsc,
      shouldCloseOnOverlayClick,
    }: ShowModal) => {
      setModalChildren(children);
      setIsOpen(true);
      setModalTitle(title);
      setShowTitle(showTitle);
      setShowCloseButton(showCloseButton);
      setContentLabel(title);
      setMaxWith(maxWidth);
      setModalClassName(modalClassName);
      setShouldCloseOnEsc(shouldCloseOnEsc);
      setShouldCloseOnOverlayClick(shouldCloseOnOverlayClick);
    },
    []
  );
  const closeModal = useCallback((value: boolean = true) => setIsOpen(!value), []);
  // Memoize value so it doesn't re-render the tree
  const value: IModalContext = useMemo(() => ({ showModal, setModalChildren, isOpen, closeModal }), [
    closeModal,
    showModal,
    setModalChildren,
    isOpen,
  ]);
  // force closing modal when soft changing route
  Router.events.on('routeChangeStart', closeModal);

  return (
    <ModalContext.Provider value={value}>
      <Modal
        isOpen={isOpen}
        contentLabel={contentLabel}
        onRequestClose={closeModal}
        className={styles.modal}
        overlayClassName={styles.overlay}
        shouldCloseOnEsc={shouldCloseOnEsc}
        shouldCloseOnOverlayClick={shouldCloseOnOverlayClick}
      >
        <div
          tw="flex flex-col w-[90vw] sm:w-[80vw]"
          css={{ maxWidth: `${maxWidth}` }}
          className={modalClassName}
          onClick={(e) => e.stopPropagation()}
        >
          {(showTitle || showCloseButton) && (
            <div tw="flex justify-between items-center p-4 pr-2 border-b border-[lightgrey] relative bg-primary rounded-t-lg">
              <P
                tw="flex-grow text-white font-medium text-center mb-0"
                style={{ fontSize: modalClassName === 'onBoardingModal' ? '2.3rem' : '1.3rem' }}
              >
                {showTitle && modalTitle}
              </P>
              {showCloseButton && (
                <button tw="flex flex-col" onClick={closeModal}>
                  <Icon icon="material-symbols:close" tw="text-white hover:text-gray-400" />
                </button>
              )}
            </div>
          )}
          <ModalFrame className="show-scrollbar">{modalChildren}</ModalFrame>
        </div>
      </Modal>
      {children}
    </ModalContext.Provider>
  );
}

const ModalFrame = styled.div`
  display: flex;
  flex-direction: column;
  padding: 2rem;
  overflow: auto;
  max-height: calc(95vh - 10rem);
`;

export const useModal = () => useContext(ModalContext);
export default ModalContext;
