import React, { createContext, useRef, useEffect, useCallback, useContext } from 'react';
import { WebSocketContextValue, MessageType, MessageHandler, WebSocketMessage } from './websocket.types';

const WebSocketContext = createContext<WebSocketContextValue | null>(null);

export const WebSocketProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const ws = useRef<WebSocket | null>(null);
  const listenersRef = useRef<Map<string, Set<{ types: MessageType[]; handler: MessageHandler }>>>(new Map());

  const connect = useCallback(() => {
    if (!process.env.ADDRESS_WSS) return;

    ws.current = new WebSocket(process.env.ADDRESS_WSS);

    ws.current.onopen = () => {
      console.log('## WebSocket open');

      // Resubscribe to all topics (if there are any pending)
      listenersRef.current.forEach((_, topicId) => {
        ws.current && ws.current.send(JSON.stringify({ type: 'subscribe', topic_id: topicId }));
      });
    };

    ws.current.onclose = () => {
      console.log('## WebSocket closed');
    };

    ws.current.onerror = (error) => {
      console.error('## WebSocket error:', error);
    };

    ws.current.onmessage = (event) => {
      const message: WebSocketMessage = JSON.parse(event.data);
      const topicListeners = listenersRef.current.get(message.topic_id);
      if (topicListeners) {
        topicListeners.forEach(({ types, handler }) => {
          //Warn the listeners that care about the message type
          if (types.includes(message.type)) {
            handler(message);
          }
        });
      }
    };
  }, []);

  useEffect(() => {
    connect();

    return () => {
      ws.current && ws.current.close();
    };
  }, [connect]);

  const subscribe = useCallback((topicId: string, messageTypes: MessageType[], messageHandler: MessageHandler) => {
    //If no one is listening to this topic, create the entry for it
    if (!listenersRef.current.has(topicId)) {
      listenersRef.current.set(topicId, new Set());
    }

    //Add the listener to the topic
    const listenerObject = { types: messageTypes, handler: messageHandler };
    listenersRef.current.get(topicId)!.add(listenerObject);

    //Subscribe to the topic
    if (ws.current?.readyState === WebSocket.OPEN) {
      ws.current.send(JSON.stringify({ type: 'subscribe', topic_id: topicId }));
    }

    return () => {
      const topicListeners = listenersRef.current.get(topicId);
      if (topicListeners) {
        //Delete the listener when that specific component is unmounted
        topicListeners.delete(listenerObject);

        //If there are no more listeners for this topic, unsubscribe from said topics
        if (topicListeners.size === 0) {
          listenersRef.current.delete(topicId);
          if (ws.current?.readyState === WebSocket.OPEN) {
            ws.current.send(JSON.stringify({ type: 'unsubscribe', topic_id: topicId }));
          }
        }
      }
    };
  }, []);

  return <WebSocketContext.Provider value={{ subscribe }}>{children}</WebSocketContext.Provider>;
};

export const useWebSocket = () => {
  const context = useContext(WebSocketContext);
  if (!context) {
    throw new Error('## useWebSocket must be used within a WebSocketProvider');
  }
  return context;
};
