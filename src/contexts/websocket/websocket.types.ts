export type MessageType = 'Notification' | 'Mention' | 'PostInFeed' | 'CommentInPost' | 'FeedActivity';

export interface WebSocketMessage {
  type: MessageType;
  topic_id: string;
  subject_id?: string;
}

export type MessageHandler = (message: WebSocketMessage) => void;

export interface WebSocketContextValue {
  subscribe: (topicId: string, messageTypes: MessageType[], messageHandler: MessageHandler) => () => void;
}
