import styled from '@emotion/styled';

export const ScrollbarStyled = styled.div`
  overflow: auto;
  height: 100%;

  /* Custom scrollbar styles */
  ::-webkit-scrollbar {
    width: 8px;
    height: 8px;
  }

  ::-webkit-scrollbar-thumb {
    background: ${({ theme }) => theme.token.neutral6};
    border-radius: 4px;
  }

  ::-webkit-scrollbar-thumb:hover {
    background: ${({ theme }) => theme.token.neutral4};
  }

  ::-webkit-scrollbar-track {
    background: ${({ theme }) => theme.token.neutral1};
    border-radius: 4px;
  }

  /* Scrollbar styles for Firefox */
  * {
    scrollbar-width: thin;
    scrollbar-color: ${({ theme }) => `${theme.token.neutral6} ${theme.token.neutral1}`};
  }
`;

export const ScrollbarFullWidthStyled = styled(ScrollbarStyled)`
  width: 100%;
`;
