import Icon from '@ant-design/icons';
import { CustomIconComponentProps } from '@ant-design/icons/lib/components/Icon';
import React from 'react';

const SoundOutlinedIconSvg = () => (
  <svg viewBox="0 0 18 18" width="1em" height="1em" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g clip-path="url(#clip0_6498_365)">
      <path
        d="M17.02 0.474473C16.9152 0.488891 11.3951 3.31987 11.3951 3.31987H6.96774C6.50755 3.31339 6.08434 3.72242 6.08434 4.17348C6.08434 4.62458 6.50759 5.03357 6.96774 5.0271H10.7419V9.57991H4.05532C2.81176 9.57991 1.74188 8.53048 1.74188 7.30358C1.74188 6.08082 2.8209 5.02725 4.06451 5.02725C4.52469 5.03372 4.9479 4.62469 4.9479 4.17363C4.9479 3.72253 4.52465 3.31354 4.06451 3.32002C1.844 3.31987 0 5.12272 0 7.30335C0 9.47991 1.83481 11.2871 4.05567 11.2871H4.7724L6.72298 16.96C6.83648 17.2981 7.18539 17.5461 7.54857 17.5469H11.6311C12.183 17.5469 12.6349 16.9295 12.4567 16.4177L10.6967 11.2871H11.3953L16.7208 14.0347C16.9851 14.1721 17.3226 14.163 17.5787 14.0116C17.8348 13.8602 18.0006 13.572 18 13.2789V1.32853C18.0037 0.843757 17.5108 0.413872 17.02 0.474473ZM16.2579 2.7418V11.8647L12.4837 9.91741V7.30316V4.68891L16.2579 2.7418ZM6.61364 11.2866H8.85459L10.4241 15.8394H8.17418L6.61364 11.2866Z"
        fill="currentColor"
      />
      <rect x="4.125" y="3.42969" width="4.6667" height="1.49609" fill="currentColor" />
    </g>
    <defs>
      <clipPath id="clip0_6498_365">
        <rect width="18" height="18" fill="white" transform="translate(0 0.0078125)" />
      </clipPath>
    </defs>
  </svg>
);

export const SoundOutlinedIcon = (props: Partial<CustomIconComponentProps>) => (
  <Icon component={SoundOutlinedIconSvg} {...props} />
);
