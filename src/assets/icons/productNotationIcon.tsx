export const ProductNotationIcon = () => {
  return (
    <svg width="7" height="8" viewBox="0 0 7 8" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M6.81463 0.727273V8H5.29474V1.97727H2.3544V8H0.834517V0.727273H6.81463Z" fill="#5F5D5D" />
    </svg>
  );
};
