export const SummationIcon = () => {
  return (
    <svg width="7" height="10" viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M6.72585 8.13139V9.32102H1.34233V8.13139H6.72585ZM6.57315 0.727273V1.92756H1.46662V0.727273H6.57315ZM5.19176 4.81818V5.15199L1.91406 9.32102H0.774148V8.58239L3.69318 4.98864L0.774148 1.46946V0.727273H1.91406L5.19176 4.81818Z"
        fill="#5F5D5D"
      />
    </svg>
  );
};
