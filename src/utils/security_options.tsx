export const securityOptionsWorkspace = [
  {
    header: 'findabilitySettings',
    key: 'listing_privacy',
    info: 'workspaceCanBeFoundByUsersOnJogl',
    options: [
      {
        header: 'public',
        key: 'public',
        icon: 'mdi:search',
        info: 'workspaceCanBeFoundByAnyone',
      },
      {
        header: 'ecosystemOnly',
        key: 'ecosystem',
        icon: 'mdi:search-web',
        info: 'workspaceCanBeFoundByMembersOfTheWorkspaceEcosystem',
      },
      {
        header: 'membersOnly',
        key: 'private',
        icon: 'ic:twotone-search-off',
        info: 'workspaceCanBeFoundByItsMembers',
      },
    ],
  },
  {
    header: 'visibilitySettings',
    key: 'content_privacy',
    info: 'whoCanViewTheWorkspace',
    options: [
      {
        header: 'public',
        key: 'public',
        icon: 'mdi:eye-outline',
        info: 'anyoneCanViewItsContent',
      },
      {
        header: 'ecosystemOnly',
        key: 'ecosystem',
        icon: 'mdi:database-eye-outline',
        info: 'onlyMembersOfTheWorkspaceEcosystemCanView',
      },
      {
        header: 'membersOnly',
        key: 'private',
        icon: 'mdi:eye-plus-outline',
        info: 'onlyMembersOfTheWorkspaceCanView',
      },
    ],
  },
  {
    header: 'membershipSettings',
    key: 'joining_restriction',
    info: 'howUsersCanBecomeMembersOfWorkspace',
    options: [
      {
        header: 'open',
        key: 'open',
        icon: 'mdi:person-plus-outline',
        info: 'anyJOGLUserCanBecomeMember',
      },
      {
        header: 'onRequest',
        key: 'request',
        icon: 'mdi:user-clock-outline',
        info: 'canRequestToBecomeMembers',
      },
      {
        header: 'inviteOnly',
        key: 'invite',
        icon: 'mdi:email-send-outline',
        info: 'canBecomeWorkspaceMembersThroughInvite',
      },
    ],
  },
  {
    header: 'discussionManagement',
    key: 'management',
    info: 'whoCanPostAndReply',
    options: [
      {
        header: 'adminsOnly',
        key: 'content_entities_created_by_any_member',
        isChecked: (options) => !options?.includes('content_entities_created_by_any_member'),
        icon: 'mdi:shield-user',
      },
      {
        header: 'allMembers',
        key: 'content_entities_created_by_any_member',
        isChecked: (options) => options?.includes('content_entities_created_by_any_member'),
        icon: 'mdi:user-group',
      },
    ],
  },
  {
    header: 'taggingEveryone',
    key: 'management',
    info: 'whoCanTagEveryone',
    isDisabled: (options) => !options?.includes('content_entities_created_by_any_member'),
    options: [
      {
        header: 'adminsOnly',
        key: 'mention_everyone_by_any_member',
        isChecked: (options) => !options?.includes('mention_everyone_by_any_member'),
        icon: 'mdi:shield-user',
      },
      {
        header: 'allMembers',
        key: 'mention_everyone_by_any_member',
        isChecked: (options) => options?.includes('mention_everyone_by_any_member'),
        icon: 'mdi:user-group',
      },
    ],
  },
  {
    header: 'documentsManagement',
    key: 'management',
    info: 'whoCanCreateAndDeleteDocuments',
    options: [
      {
        header: 'adminsOnly',
        key: 'documents_created_by_any_member',
        isChecked: (options) => !options?.includes('documents_created_by_any_member'),
        icon: 'mdi:shield-user',
      },
      {
        header: 'allMembers',
        key: 'documents_created_by_any_member',
        isChecked: (options) => options?.includes('documents_created_by_any_member'),
        icon: 'mdi:user-group',
      },
    ],
  },
  {
    header: 'libraryManagement',
    key: 'management',
    info: 'whoCanAddPapersAndSave',
    options: [
      {
        header: 'adminsOnly',
        key: 'library_managed_by_any_member',
        isChecked: (options) => !options?.includes('library_managed_by_any_member'),
        icon: 'mdi:shield-user',
      },
      {
        header: 'allMembers',
        key: 'library_managed_by_any_member',
        isChecked: (options) => options?.includes('library_managed_by_any_member'),
        icon: 'mdi:user-group',
      },
    ],
  },
  {
    header: 'needsManagement',
    key: 'management',
    info: 'whoCanCreateAndDeleteNeeds',
    options: [
      {
        header: 'adminsOnly',
        key: 'needs_created_by_any_member',
        isChecked: (options) => !options?.includes('needs_created_by_any_member'),
        icon: 'mdi:shield-user',
      },
      {
        header: 'allMembers',
        key: 'needs_created_by_any_member',
        isChecked: (options) => options?.includes('needs_created_by_any_member'),
        icon: 'mdi:user-group',
      },
    ],
  },
  {
    header: 'eventCreation',
    key: 'management',
    info: 'whoCanCreateAndDeleteEvents',
    options: [
      {
        header: 'adminsOnly',
        key: 'events_created_by_any_member',
        isChecked: (options) => !options?.includes('events_created_by_any_member'),
        icon: 'mdi:shield-user',
      },
      {
        header: 'allMembers',
        key: 'events_created_by_any_member',
        isChecked: (options) => options?.includes('events_created_by_any_member'),
        icon: 'mdi:user-group',
      },
    ],
  },
];

export const securityOptionsHub = [
  {
    header: 'findabilitySettings',
    key: 'listing_privacy',
    info: 'howTheHubCanBeFoundByUsers',
    options: [
      {
        header: 'public',
        key: 'public',
        icon: 'mdi:search',
        info: 'hubCanBeFoundByAnyone',
      },
      {
        header: 'ecosystemOnly',
        key: 'ecosystem',
        icon: 'mdi:search-web',
        info: 'hubCanBeFoundByMembersOfTheHubEcosystem',
      },
      {
        header: 'membersOnly',
        key: 'private',
        icon: 'ic:twotone-search-off',
        info: 'hubCanOnlyBeFoundByItsMembers',
      },
    ],
  },
  {
    header: 'visibilitySettings',
    key: 'content_privacy',
    info: 'whoCanViewTheHub',
    options: [
      {
        header: 'public',
        key: 'public',
        icon: 'mdi:eye-outline',
        info: 'anyoneCanViewTheHubContent',
      },
      {
        header: 'membersOnly',
        key: 'private',
        icon: 'mdi:eye-plus-outline',
        info: 'onlyTheMembersOfTheHubCanView',
      },
    ],
  },
  {
    header: 'membershipSettings',
    key: 'joining_restriction',
    info: 'howUsersCanBecomeMembersOfTheHub',
    options: [
      {
        header: 'open',
        key: 'open',
        icon: 'mdi:person-plus-outline',
        info: 'anyJOGLUserCanBecomeMember',
      },
      {
        header: 'onRequest',
        key: 'request',
        icon: 'mdi:user-clock-outline',
        info: 'canRequestToBecomeMembers',
      },
      {
        header: 'inviteOnly',
        key: 'invite',
        icon: 'mdi:email-send-outline',
        info: 'canBecomeHubMembersThroughAnInvite',
      },
    ],
  },
  {
    header: 'discussionManagement',
    key: 'management',
    info: 'whoCanPostAndReply',
    options: [
      {
        header: 'adminsOnly',
        key: 'content_entities_created_by_any_member',
        isChecked: (options) => !options?.includes('content_entities_created_by_any_member'),
        icon: 'mdi:shield-user',
      },
      {
        header: 'allMembers',
        key: 'content_entities_created_by_any_member',
        isChecked: (options) => options?.includes('content_entities_created_by_any_member'),
        icon: 'mdi:user-group',
      },
    ],
  },
  {
    header: 'taggingEveryone',
    key: 'management',
    info: 'whoCanTagEveryone',
    isDisabled: (options) => !options?.includes('content_entities_created_by_any_member'),
    options: [
      {
        header: 'adminsOnly',
        key: 'mention_everyone_by_any_member',
        isChecked: (options) => !options?.includes('mention_everyone_by_any_member'),
        icon: 'mdi:shield-user',
      },
      {
        header: 'allMembers',
        key: 'mention_everyone_by_any_member',
        isChecked: (options) => options?.includes('mention_everyone_by_any_member'),
        icon: 'mdi:user-group',
      },
    ],
  },
  {
    header: 'documentsManagement',
    key: 'management',
    info: 'whoCanCreateAndDeleteDocuments',
    options: [
      {
        header: 'adminsOnly',
        key: 'documents_created_by_any_member',
        isChecked: (options) => !options?.includes('documents_created_by_any_member'),
        icon: 'mdi:shield-user',
      },
      {
        header: 'allMembers',
        key: 'documents_created_by_any_member',
        isChecked: (options) => options?.includes('documents_created_by_any_member'),
        icon: 'mdi:user-group',
      },
    ],
  },
  {
    header: 'libraryManagement',
    key: 'management',
    info: 'whoCanAddPapersAndSave',
    options: [
      {
        header: 'adminsOnly',
        key: 'library_managed_by_any_member',
        isChecked: (options) => !options?.includes('library_managed_by_any_member'),
        icon: 'mdi:shield-user',
      },
      {
        header: 'allMembers',
        key: 'library_managed_by_any_member',
        isChecked: (options) => options?.includes('library_managed_by_any_member'),
        icon: 'mdi:user-group',
      },
    ],
  },
  {
    header: 'needsManagement',
    key: 'management',
    info: 'whoCanCreateAndDeleteNeeds',
    options: [
      {
        header: 'adminsOnly',
        key: 'needs_created_by_any_member',
        isChecked: (options) => !options?.includes('needs_created_by_any_member'),
        icon: 'mdi:shield-user',
      },
      {
        header: 'allMembers',
        key: 'needs_created_by_any_member',
        isChecked: (options) => options?.includes('needs_created_by_any_member'),
        icon: 'mdi:user-group',
      },
    ],
  },
  {
    header: 'workspaceCreation',
    key: 'management',
    info: 'whoCanCreateOrAddWorkspace',
    options: [
      {
        header: 'adminsOnly',
        key: 'workspace_created_by_any_member',
        isChecked: (options) => !options?.includes('workspace_created_by_any_member'),
        icon: 'mdi:shield-user',
      },
      {
        header: 'allMembers',
        key: 'workspace_created_by_any_member',
        isChecked: (options) => options?.includes('workspace_created_by_any_member'),
        icon: 'mdi:user-group',
      },
    ],
  },
  {
    header: 'eventCreation',
    key: 'management',
    info: 'whoCanCreateAndDeleteEvents',
    options: [
      {
        header: 'adminsOnly',
        key: 'events_created_by_any_member',
        isChecked: (options) => !options?.includes('events_created_by_any_member'),
        icon: 'mdi:shield-user',
      },
      {
        header: 'allMembers',
        key: 'events_created_by_any_member',
        isChecked: (options) => options?.includes('events_created_by_any_member'),
        icon: 'mdi:user-group',
      },
    ],
  },
];

export const securityOptionsOrganization = [
  {
    header: 'membershipSettings',
    key: 'joining_restriction',
    info: 'howUsersCanBecomeMembersOfTheOrganization',
    options: [
      {
        header: 'open',
        key: 'open',
        icon: 'mdi:person-plus-outline',
        info: 'anyJOGLUserCanBecomeMember',
      },
      {
        header: 'onRequest',
        key: 'request',
        icon: 'mdi:user-clock-outline',
        info: 'canRequestToBecomeMembers',
      },
      {
        header: 'inviteOnly',
        key: 'invite',
        icon: 'mdi:email-send-outline',
        info: 'canBecomeOrganizationMembersThroughAnInvite',
      },
    ],
  },
];

export const securityOptionsCfp = [
  {
    header: 'applicationCriteria',
    key: 'proposal_participiation',
    info: 'applicationRightsForTheCallForProposals',
    options: [
      {
        header: 'everyone',
        key: 'public',
        icon: 'mdi:person-plus-outline',
        info: 'anyJoglUser',
      },
      {
        header: 'workspaceEcosystemMembersOnly',
        key: 'ecosystem',
        icon: 'mdi:account-details-outline',
        info: 'anyMemberOfTheWorkspaceEcosystem',
      },
      {
        header: 'workspaceMembersOnly',
        key: 'private',
        icon: 'mdi:user-group',
        info: 'anyMemberOfTheWorkspace',
      },
    ],
  },
  {
    header: 'proposalsVisibilitySettings',
    key: 'proposal_privacy',
    info: 'whoCanSeeTheSubmittedProposals',
    options: [
      {
        header: 'everyone',
        key: 'public',
        icon: 'mdi:eye-outline',
        info: 'anyJoglUser',
      },
      {
        header: 'workspaceEcosystemOnly',
        key: 'ecosystem',
        icon: 'mdi:database-eye-outline',
        info: 'onlyWorkspaceEcosystemMembers',
      },
      {
        header: 'workspaceMembersOnly',
        key: 'private',
        icon: 'mdi:eye-plus-outline',
        info: 'onlyWorkspaceMembers',
      },
      {
        header: 'workspaceAdminsAndReviewersOnly',
        key: 'adminandreviewers',
        icon: 'mdi:eye-lock-open-outline',
        info: 'onlyWorkspaceAdminsAndReviewers',
      },
      {
        header: 'workspaceAdminsOnly',
        key: 'admin',
        icon: 'mdi:eye-lock-outline',
        info: 'onlyWorkspaceAdmins',
      },
    ],
  },
  {
    header: 'discussionManagement',
    key: 'discussion_participation',
    info: 'whoCanPostInTheDiscussion',
    options: [
      {
        header: 'everyone',
        key: 'public',
        icon: 'mdi:person-plus-outline',
        info: 'anyJoglUser',
      },
      {
        header: 'workspaceEcosystemOnly',
        key: 'ecosystem',
        icon: 'mdi:account-details-outline',
        info: 'anyMemberOfTheWorkspaceEcosystem',
      },
      {
        header: 'workspaceMembersOnly',
        key: 'private',
        icon: 'mdi-users-outline',
        info: 'anyMemberOfTheWorkspace',
      },
      {
        header: 'applicantsAndAdmins',
        key: 'participants',
        icon: 'mdi:account-lock-open-outline',
        info: 'callForProposalApplicantsAndAdmins',
      },
      {
        header: 'adminsOnly',
        key: 'adminonly',
        icon: 'mdi:account-lock-outline',
        info: 'adminsOnly',
      },
    ],
  },
  // {
  //   header: 'taggingEveryone',
  //   key: 'management',
  //   info: 'whoCanTagEveryone',
  //   isDisabled: (options) => !options?.includes('content_entities_created_by_any_member'),
  //   options: [
  //     {
  //       header: 'adminsOnly',
  //       key: 'mention_everyone_by_any_member',
  //       isChecked: (options) => !options?.includes('mention_everyone_by_any_member'),
  //       icon: 'mdi:shield-user',
  //     },
  //     {
  //       header: 'allMembers',
  //       key: 'mention_everyone_by_any_member',
  //       isChecked: (options) => options?.includes('mention_everyone_by_any_member'),
  //       icon: 'mdi:user-group',
  //     },
  //   ],
  // },
];
