import { KnownUrl } from '~/src/types/knownUrl';

export const isKnownUrl = (url: string): KnownUrl => {
  const patterns: [RegExp, KnownUrl][] = [
    [
      /^(https?:\/\/)?(www\.)?github\.com\b/,
      { name: 'github', icon: '/images/icons/links-github.png', image: '/images/icons/links-github-large.png' },
    ],
    [
      /^(https?:\/\/)?(www\.)?onedrive\.live\.com\b/,
      { name: 'one drive', icon: '/images/icons/links-onedrive.png', image: '/images/icons/links-onedrive-large.png' },
    ],
    [
      /^(https?:\/\/)?(www\.)?drive\.google\.com\b/,
      {
        name: 'google drive',
        icon: '/images/icons/links-googledrive.png',
        image: '/images/icons/links-googledrive-large.png',
      },
    ],
    [
      /^(https?:\/\/)?(www\.)?docs\.google\.com\/document\b/,
      {
        name: 'google docs',
        icon: '/images/icons/links-googledocs.png',
        image: '/images/icons/links-googledocs-large.svg',
      },
    ],
    [
      /^(https?:\/\/)?(www\.)?docs\.google\.com\/presentation\b/,
      {
        name: 'google slides',
        icon: '/images/icons/links-googleslides.png',
        image: '/images/icons/links-googleslides-large.svg',
      },
    ],
    [
      /^(https?:\/\/)?(www\.)?docs\.google\.com\/spreadsheets\b/,
      {
        name: 'google sheets',
        icon: '/images/icons/links-googlesheets.png',
        image: '/images/icons/links-googlesheets-large.svg',
      },
    ],
    [
      /https:\/\/meet\.google\.com\/\S+/i,
      {
        name: 'google meet',
        icon: '/images/icons/links-googlemeet.png',
        image: '/images/icons/links-googlemeet.png',
      },
    ],
    [
      /^(https?:\/\/)?(www\.)?notion\.so\b/,
      { name: 'notion', icon: '/images/icons/links-notion.png', image: '/images/icons/links-notion-large.svg' },
    ],
    [
      /^https:\/\/[a-z0-9-]+\.notion\.site\b/,
      { name: 'notion', icon: '/images/icons/links-notion.png', image: '/images/icons/links-notion-large.svg' },
    ],
    [
      /^(https?:\/\/)?(www\.)?[a-zA-Z0-9-]+\.bigbluebutton\.[a-zA-Z]+(\.[a-zA-Z]+)?\/?/i,
      {
        name: 'BigBlueButton',
        icon: '/images/icons/links-bigbluebutton.png',
        image: '/images/icons/links-bigbluebutton.png',
      },
    ],
    [
      /https:\/\/bluejeans\.com\/\d+/i,
      {
        name: 'BlueJeans',
        icon: '/images/icons/links-bluejeans.png',
        image: '/images/icons/links-bluejeans.png',
      },
    ],
    [
      /https:\/\/meet\.jit\.si\/\S+/i,
      {
        name: 'Jitsi Meet',
        icon: '/images/icons/links-jitsi.png',
        image: '/images/icons/links-jitsi.png',
      },
    ],
    [
      /https:\/\/app\.livestorm\.co\/[a-z]\/[a-zA-Z0-9-]+/i,
      {
        name: 'Livestorm',
        icon: '/images/icons/links-livestorm.png',
        image: '/images/icons/links-livestorm.png',
      },
    ],
    [
      /https:\/\/.*nextcloud\.com\S*/i,
      {
        name: 'Nextcloud',
        icon: '/images/icons/links-nextcloud.png',
        image: '/images/icons/links-nextcloud.png',
      },
    ],
    [
      /https:\/\/.*skype\.com/i,
      {
        name: 'Skype',
        icon: '/images/icons/links-skype.png',
        image: '/images/icons/links-skype.png',
      },
    ],
    [
      /https:\/\/teams\.microsoft\.com\/[^\s]+/i,
      {
        name: 'Teams',
        icon: '/images/icons/links-teams.png',
        image: '/images/icons/links-teams.png',
      },
    ],
    [
      /https:\/\/.*webex\.com/i,
      {
        name: 'Webex',
        icon: '/images/icons/links-webex.png',
        image: '/images/icons/links-webex.png',
      },
    ],
    [
      /https:\/\/wa\.me\/\d+/i,
      {
        name: 'Whatsapp',
        icon: '/images/icons/links-whatsapp.png',
        image: '/images/icons/links-whatsapp.png',
      },
    ],
    [
      /https:\/\/.*zoom\.us.*/i,
      {
        name: 'Zoom',
        icon: '/images/icons/links-zoom.svg',
        image: '/images/icons/links-zoom.svg',
      },
    ],
  ];

  for (const [pattern, serviceInfo] of patterns) {
    if (pattern.test(url)) {
      return serviceInfo;
    }
  }

  return undefined;
};
