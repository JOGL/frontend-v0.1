import { useState } from 'react';

import { QueryClient } from '@tanstack/react-query';

export const useQueryClient = () => {
  const [queryClient] = useState<QueryClient>(
    () =>
      new QueryClient({
        defaultOptions: {
          queries: {
            retryDelay: 1000,
            // Add this to prevent retrying on 401 errors
            retry: (failureCount, error: any) => {
              if (error?.response?.status === 401) {
                return false;
              }
              return failureCount < 1;
            },
          },
        },
      })
  );

  return queryClient;
};
