import { Api } from '~/__generated__/types';
import cookie from 'js-cookie';
import Router from 'next/router';
import { message } from 'antd';

const api = new Api({
  baseURL: process.env.ADDRESS_BACK,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});

// Add a flag to track if logout message was shown
let isShowingLogoutMessage = false;

api.instance.interceptors.request.use(
  (config) => {
    const authorization = cookie.get('authorization');
    if (authorization) {
      const common = { Authorization: authorization };
      return { ...config, headers: { ...config.headers, common: { ...config.headers.common, ...common } } };
    }
    return config;
  },
  (error) => Promise.reject(error)
);

api.instance.interceptors.response.use(
  (response) => response,
  (error) => {
    const status = error?.response?.status;
    const loggedInWithOldCredentials = error?.response?.data?.error === 'Signature verification raised';
    const hasExpiredSession = error?.response?.data?.error === 'Signature has expired';

    if (status === 401) {
      if (typeof window !== 'undefined' && !window?.location?.pathname.includes('/signin')) {
        if (loggedInWithOldCredentials || hasExpiredSession) {
          cookie.remove('authorization');
          cookie.remove('userId');
          typeof window !== 'undefined' && window.localStorage.setItem('logout', Date.now().toString());
          Router.push('/signin').then(() => Router.reload());
        } else if (!window?.location?.pathname.includes('/sign') && !isShowingLogoutMessage) {
          isShowingLogoutMessage = true;
          message.error('You were logged out', 2, () => {
            isShowingLogoutMessage = false;
          });

          // Add small delay to ensure message is shown
          setTimeout(() => {
            window.location.replace('/signout');
          }, 1000); // 1 second delay
        }
      }
    }
    return Promise.reject(error);
  }
);

export default api;
