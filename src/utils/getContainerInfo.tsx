import { Cfp, Organization, User } from '~/src/types';
import { ContainerPermissions, MenuActions } from '~/src/types/containerAddMenu';
import OrganizationAddActions from '~/src/components/Organization/OrganizationAddActions';
import CfpAddActions from '~/src/components/Cfp/CfpAddActions';
import UserAddActions from '~/src/components/User/UserAddAction';
import useUser from '~/src/hooks/useUser';
import EventAddActions from '~/src/components/Event/EventAddActions';
import { Event } from '~/src/types/event';

//Default actions
export const membersAction = {
  title: 'member.other',
  icon: 'fluent:people-community-add-20-regular',
  privacy: 'adminsOnly',
  disabled: true,
  handleSelect: undefined,
};

export const cfpAction = {
  title: 'callForProposals.one',
  icon: 'bx:medal',
  privacy: 'adminsOnly',
  disabled: true,
  handleSelect: undefined,
};

export const needAction = (object: any) => {
  return {
    title: 'need.one',
    icon: 'material-symbols:extension-outline',
    privacy: object?.management?.includes('needs_created_by_any_member') ? 'allMembers' : 'adminsOnly',
    disabled: true,
    handleSelect: undefined,
  };
};

export const paperAction = (object: any) => {
  return {
    title: 'paper.one',
    icon: 'clarity:library-line',
    privacy: object?.management?.includes('library_managed_by_any_member') ? 'allMembers' : 'adminsOnly',
    disabled: true,
    handleSelect: undefined,
  };
};

export const resourceAction = {
  title: 'resource.one',
  icon: 'carbon:software-resource-cluster',
  privacy: 'adminsOnly',
  disabled: true,
  handleSelect: undefined,
};

export const documentAction = (object: any) => {
  return {
    title: 'document.one',
    icon: 'ph:file-doc',
    privacy: object?.management?.includes('documents_created_by_any_member') ? 'allMembers' : 'adminsOnly',
    disabled: true,
    handleSelect: undefined,
    goToTab: 'documents',
  };
};

export const hubAction = {
  title: 'hub.one',
  icon: 'carbon:edge-node-alt',
  privacy: 'adminsOnly',
  disabled: true,
  handleSelect: undefined,
};

export const organizationAction = {
  title: 'organization.one',
  icon: 'carbon:location-company',
  privacy: 'adminsOnly',
  disabled: true,
  handleSelect: undefined,
};

export const eventAction = (object: any) => {
  return {
    title: 'event.one',
    icon: 'material-symbols:event-note-outline',
    privacy: object?.management?.includes('events_created_by_any_member') ? 'allMembers' : 'adminsOnly',
    disabled: true,
    handleSelect: undefined,
  };
};

const getPermissions = (container: Organization | Cfp | Event): ContainerPermissions => {
  //check if it is an Event
  const permissions =
    'attendee_count' in container
      ? (container as Event)?.permissions
      : (container as Organization | Cfp)?.user_access?.permissions;

  return {
    isAdmin: permissions?.includes('manage'),
    canAddMembers: permissions?.includes('manage'),
    canCreateNeed: permissions?.includes('postneed'),
    canCreateJOGLDoc: permissions?.includes('managedocuments'),
    canCreatePapers: permissions?.includes('managelibrary'),
    canCreateDocuments: permissions?.includes('managedocuments'),
    canCreateResources: permissions?.includes('postresources'),
    canCreateCfp: permissions?.includes('manage'),
    canCreateHub: permissions?.includes('manage'),
    canCreateOrganization: permissions?.includes('manage'),
    canCreateEvent: permissions?.includes('createevents'),
  };
};

export const getContainerInfo = (container: any, type: string, refresh: () => void): MenuActions => {
  const { user } = useUser();
  const permissions = getPermissions(container);

  switch (type) {
    case 'cfp':
      return CfpAddActions(container as Cfp, permissions, refresh);
    case 'organization':
      return OrganizationAddActions(user, container as Organization, permissions, refresh);
    case 'user':
      return UserAddActions(container as User, refresh);
    case 'event':
      return EventAddActions(container as Event, permissions, refresh);
    default:
      return {};
  }
};
