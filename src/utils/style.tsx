import { keyframes, css } from '@emotion/react';
import styled from '~/src/utils/styled';
import Icon from '~/src/components/primitives/Icon';
import tw from 'twin.macro';

// export const globalStyles = (
//   <Global
//     styles={css`
//       html,
//       body {
//         padding: 3rem 1rem;
//         margin: 0;
//         background: papayawhip;
//         min-height: 100%;
//         font-family: Helvetica, Arial, sans-serif;
//         font-size: 24px;
//       }
//     `}
//   />
// );

export const basicStyles = css`
  background-color: white;
  color: cornflowerblue;
  border: 1px solid lightgreen;
  border-right: none;
  border-bottom: none;
  box-shadow: 5px 5px 0 0 lightgreen, 10px 10px 0 0 lightyellow;
  transition: all 0.1s linear;
  margin: 3rem 0;
  padding: 1rem 0.5rem;
`;

export const hoverStyles = css`
  &:hover {
    color: white;
    background-color: lightgray;
    border-color: aqua;
    box-shadow: -15px -15px 0 0 aqua, -30px -30px 0 0 cornflowerblue;
  }
`;
export const bounce = keyframes`
  from {
    transform: scale(1.01);
  }
  to {
    transform: scale(0.99);
  }
`;

export const Basic = styled('div')`
  ${basicStyles};
`;

export const Combined = styled('div')`
  ${basicStyles};
  ${hoverStyles};
  & code {
    background-color: linen;
  }
`;
export const Animated = styled('div')`
  ${basicStyles};
  ${hoverStyles};
  & code {
    background-color: linen;
  }
  animation: ${(props) => props.animation} 0.2s infinite ease-in-out alternate;
`;

export const Tab = styled.button`
  display: inline-block;
  border-bottom: 3px solid transparent;
  // border-bottom-color: #737272;
  // font-weight: ${(p) => p.selected && 500};
  color: ${(p) => (p.selected ? 'black' : '#737272')};
  border-bottom-color:${(p) => p.selected && '#737272'};
  text-align: left;
  cursor: pointer;
  &:hover {
    // color: black;
    border-bottom-color:#737272;
    text-decoration: none;
  }
  &:focus {
    outline: none;
  }
  @media (max-width: 48em) {
    white-space: nowrap;
  }
  @media (min-width: 48em) {
    background: ${(p) => p.selected && '#ededed'};
    border-bottom: 1px solid #0000000D;
    &:hover {
      border-bottom: 1px solid #0000000D;
      // color: black;
      background:#ededed;
      // font-weight: 700;
    }
    &:focus {
      border-bottom: 1px solid #0000000D;
      // color: black;
      background:#ededed;
      // font-weight: 700;
    }
  }
`;

interface TabPanelProps {
  defaultPadding?: boolean;
}

export const TabPanel = styled.div<TabPanelProps>`
  width: 100%;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  padding: 24px 32px;
  .infoHtml {
    width: 100% !important;
  }
`;

export const NavContainer = styled.div`
  position: relative;

  @media (max-width: 48em) {
    border-bottom: 2px solid #dce3e9;
  }

  /* To make element sticky on top when reach top of page */
  @media (min-width: 48em) {
    position: sticky;
    position: -webkit-sticky;
    top: 64px;
  }

  @media (min-width: 48em) {
    width: 100%;
    justify-content: flex-end;
    // margin-right: 10px;
  }
`;

export const Nav = styled.div`
  overflow-x: scroll;

  &::-webkit-scrollbar {
    display: none;
  }
  @media (max-width: 47.95em) {
    padding-left: 1.2rem;
    padding-right: 3rem;
    button {
      padding: 0 3px 5px;
    }
  }
`;

export const OverflowGradient = styled.div`
  height: 100%;
  position: absolute;
  ${(p) =>
    p.gradientPosition === 'right' &&
    'right:0;background: linear-gradient(269.82deg, white 50.95%, rgba(241, 244, 248, 0) 120.37%);width: 3rem;'};
  ${(p) =>
    p.gradientPosition === 'left' &&
    'left:0;background: linear-gradient(90.82deg, white 50.95%, rgba(241, 244, 248, 0) 120.37%);width: 2rem;'};
`;

export const ContactButton = <Icon icon="fa6-solid:envelope" tw="cursor-pointer text-gray-700 hover:text-action" />;

export const StickyHeading = styled.div`
  display: flex;
  flex-direction: column;
  background-color: white;
  justify-content: center;
  position: fixed;
  height: 70px;
  top: ${(p) => (p.isSticky ? '64px' : '-1000px')};
  width: 100%;
  z-index: 9;
  border-top: 1px solid grey;
  left: 0;
  transition: top 333ms;
  box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.15), 0 2px 3px rgba(0, 0, 0, 0.2);
  overflow: hidden;
  @media (max-width: 48em) {
    height: 60px;
    .actions {
      display: none;
    }
    img {
      width: 40px;
      height: 40px;
    }
  }
  > div {
    max-width: 1280px;
    margin: 0 auto;
    width: 100%;
  }
`;
