const colors = {
  carouselBg: '#F5F3FE',
  panelListBG: '#EFECFA',
  colorListLink: '#2987CD',
  new: '#EFECFA',
  badge: '#5e3ebf',
};

export default colors;
