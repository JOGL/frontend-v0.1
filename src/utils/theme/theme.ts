import space from './space';
import colors from './colors';
import { fontSizes, fontWeight } from './typography';

type ListWithAliases<A> = string[] & Record<keyof A, string>;

/**
 * Helper function to create an array with additional properties from a specific type
 * @param list
 * @param keys
 */
const makeListWithAliases = <A>(aliases: Record<keyof A, string>, ...keys: Array<keyof A>): ListWithAliases<A> => {
  const list = keys.reduce<string[]>((prev, curr) => [...prev, aliases[curr]], []);
  return Object.assign(list, aliases);
};

//
// Fonts
//

const fontsAliases = { primary: "'Inter', sans-serif", secondary: "'Bebas Neue', sans-serif" };

const fonts = makeListWithAliases(fontsAliases, 'primary', 'secondary');

//
// Radii
//

// Radii is for border-radius
const radiiAliases = {
  /* 0 => 0px or none    */ none: '0',
  /* 1 => 2px or sm      */ sm: '0.125rem',
  /* 2 => 4px or default */ default: '0.25rem',
  /* 3 => 6px or md      */ md: '0.375rem',
  /* 4 => 8px or lg      */ lg: '0.5rem',
  /* 5 => 16px or xl     */ xl: '1rem',
  /* 6 => 9999px or full */ full: '9999px',
};

const radii = makeListWithAliases(radiiAliases, 'none', 'sm', 'default', 'md', 'lg', 'xl', 'full');

//
// Breakpoints
//

const breakpointsAliases = {
  sm: '40em' /* 1 => '> 640px' or sm  */,
  md: '48em' /* 2 => '> 768px' or md  */,
  lg: '64em' /* 3 => '> 1024px' or lg */,
  xl: '80em' /* 4 => '> 1280px' or xl */,
};

const breakpoints = makeListWithAliases(breakpointsAliases, 'sm', 'md', 'lg', 'xl');

//
// Colors
//

// some scales are generated with this => https://gka.github.io/palettes/#/9|s|ffc107|ffffe0,ff005e,93003a|0|0
const greysAliases = {
  /* 0 => null, don't use it */ '0': 'white',
  /* 1 => 100 */ '100': '#F7F8F9',
  /* 2 => 200 */ '200': '#DCE3E9',
  /* 3 => 300 */ '300': '#CFD8E1',
  /* 4 => 400 */ '400': '#C5CED5',
  /* 5 => 500 */ '500': '#A4B6C5',
  /* 6 => 600 */ '600': '#8694A7',
  /* 7 => 700 */ '700': '#626E7E',
  /* 8 => 800 */ '800': '#38424F',
  /* 9 => 900 */ '900': '#1E242D ',
  /* 10 => 1000 */ '1000': '#5c5d5d ',
  /* 11 => 1100 */ '1100': '#7f8fa4 ',
  /* 12 => 1200 */ '1200': '#d3d3d3 ',
};

const greysList = makeListWithAliases(greysAliases, '0', '100', '200', '300', '400', '500', '600', '700', '800', '900');

const primariesAliases = {
  /* 0 => null, don't use it */ '0': 'white',
  /* 1 => 100 */ '100': '#5e3ebf',
  /* 2 => 200 */ '200': '#4a3292',
};

const primariesList = makeListWithAliases(primariesAliases, '0', '100', '200');

const secondaryAliases = {
  /* 0 => null, don't use it */ '0': 'white',
  /* 1 => 100 */ '100': '#e5e7eb',
  /* 2 => 200 */ '200': '#4a3292',
  /* 3 => 300 */ '300': '#5F5D5D',
  /* 4 => 400 */ '400': '#006db0',
  /* 5 => 500 */ '500': '#2987cd',
  /* 6 => 600 */ '600': '#51a2eb',
  /* 7 => 700 */ '700': '#73bfff',
  /* 8 => 800 */ '800': '#93dcff',
  /* 9 => 900 */ '900': '#b3faff',
};

const secondaryList = makeListWithAliases(
  secondaryAliases,
  '0',
  '100',
  '200',
  '300',
  '400',
  '500',
  '600',
  '700',
  '800',
  '900'
);

// Colors are for color, background-color, border-color
const legacyColors = {
  primary: primariesList[3],
  primaries: primariesList,
  secondaries: secondaryList,
  grey: greysList[4],
  greys: greysList,
  dark: 'rgba(0, 0, 0, 0.7)',
};

//
// Shadows
//

const shadowsAliases = {
  /* 0 => xs      */ xs: '0 0 0 1px rgba(0, 0, 0, 0.05)',
  /* 1 => sm      */ sm: '0 1px 2px 0 rgba(0, 0, 0, 0.05)',
  /* 2 => default */ default: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
  /* 3 => md      */ md: '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
  /* 4 => lg      */ lg: '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
  /* 5 => xl      */ xl: '0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)',
  /* 6 => '2xl'   */ '2xl': '0 25px 50px -12px rgba(0, 0, 0, 0.25)',
};

const shadows = Object.assign(makeListWithAliases(shadowsAliases, 'xs', 'sm', 'default', 'md', 'lg', 'xl', '2xl'), {
  // used
  inner: 'inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)',
  outline: '0 0 0 3px rgba(66, 153, 225, 0.5)',
  none: 'none',
  light: '1px 2px 8px rgba(0,0,0,0.1)',
  buttonShadow: '0 0 #0000, 0 0 #0000, 0px 4px 30px rgba(0, 0, 0, 0.09)',
});

//
// Theme
//
//

const { sm, md, lg, xl } = breakpoints;

const theme = {
  fonts,
  fontSizes,
  fontWeight,
  space,
  radii,
  shadows,
  colors: { ...legacyColors, ...colors },
  ...breakpoints,
};

export default theme;

export type Theme = typeof theme;
