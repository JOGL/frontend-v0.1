import { AppThemeConfig } from './index.type';

const theme: AppThemeConfig = {
  token: {
    colorPrimary: '#5e3ebf',
    colorLink: '#5e3ebf',
    fontFamily: 'Inter, sans-serif',
    // https://ant.design/docs/spec/colors#neutral-color-palette
    neutral1: '#ffffff',
    neutral2: '#fafafa',
    neutral3: '#f5f5f5',
    neutral4: '#f0f0f0',
    neutral5: '#d9d9d9',
    neutral6: '#bfbfbf',
    neutral7: '#8c8c8c',
    neutral8: '#595959',
    neutral9: '#434343',
    neutral10: '#262626',
    neutral11: '#1f1f1f',
    neutral12: '#141414',
    neutral13: '#000000',
  },
};

export default theme;
