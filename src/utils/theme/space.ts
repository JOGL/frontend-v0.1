const space = [
  // margin and padding
  /* 0  => 0px   */ '0rem',
  /* 1  => 4px   */ '0.25rem',
  /* 2  => 8px   */ '0.5rem',
  /* 3  => 12px  */ '0.75rem',
  /* 4  => 16px  */ '1rem',
  /* 5  => 20px  */ '1.25rem',
  /* 6  => 24px  */ '1.5rem',
  /* 7  => 32px  */ '2rem',
  /* 8  => 40px  */ '2.5rem',
  /* 9  => 48px  */ '3rem',
  /* 10 => 64px  */ '4rem',
  /* 11 => 80px  */ '5rem',
  /* 12 => 96px  */ '6rem',
  /* 13 => 128px */ '8rem',
  /* 14 => 160px */ '10rem',
  /* 15 => 192px */ '12rem',
  /* 16 => 224px */ '14rem',
  /* 17 => 256px */ '16rem',
];

export default space;
