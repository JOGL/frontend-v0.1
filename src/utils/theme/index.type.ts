import type { ThemeConfig } from 'antd';
import { AliasToken } from 'antd/es/theme/interface/alias';

export interface AppAliasToken extends AliasToken {
  neutral1: string;
  neutral2: string;
  neutral3: string;
  neutral4: string;
  neutral5: string;
  neutral6: string;
  neutral7: string;
  neutral8: string;
  neutral9: string;
  neutral10: string;
  neutral11: string;
  neutral12: string;
  neutral13: string;
}

export interface AppThemeConfig extends ThemeConfig {
  token: Partial<AppAliasToken>;
}
