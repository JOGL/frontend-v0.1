import React, { Fragment, useRef, useEffect, useState } from 'react';
import Link from 'next/link';
import A from '~/src/components/primitives/A';
// Formatting dates with day.js
import day from 'dayjs';
import localizedFormat from 'dayjs/plugin/localizedFormat';
import relativeTime from 'dayjs/plugin/relativeTime';
import { useRouter } from 'next/router';
import Swal from 'sweetalert2';
import { theme } from 'twin.macro';
import Grid from '~/src/components/Grid';
import UserCard from '~/src/components/User/UserCard';
import Button from '~/src/components/primitives/Button';
import ReactTooltip from 'react-tooltip';
import { toAlphaNum } from '~/src/components/Tools/Nickname';
import FormValidator from '~/src/components/Tools/Forms/FormValidator';
import { Cfp, Hub, Organization } from '~/src/types';
import { ImageChip, TextChip } from '~/src/types/chip';
import { allTimezones, useTimezoneSelect } from 'react-timezone-select';
import { Translate } from 'next-translate';
import { EventTimezone } from '~/src/types/event';
import { Typography, message } from 'antd';
import {
  NodeModel,
  CommunityEntityMiniModel,
  WorkspaceModel,
  ContentEntityModel,
  FeedType,
  EntityMiniModel,
} from '~/__generated__/types';
import { BookOutlined, CarryOutOutlined, FileOutlined, LaptopOutlined } from '@ant-design/icons';
import { menuItems } from '../components/Layout/menuItems/menuItems';

export function linkify(content) {
  // detect links in a text and englobe them with a <a> tag
  const urlRegex = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/gm;
  return content?.replace(urlRegex, (url) => {
    const addHttp = url.substr(0, 4) !== 'http' ? 'http://' : '';
    return `<a href="${addHttp}${url}" target="_blank" rel="noopener">${url}</a>`;
  });
}

export const addQueryParams = (url: string, params: Record<string, string>): URL => {
  const urlObj = new URL(url);
  const searchParams = new URLSearchParams(urlObj.search);
  for (const [key, value] of Object.entries(params)) {
    searchParams.set(key, value);
  }
  urlObj.search = searchParams.toString();
  return urlObj;
};

export const isValidHttpUrl = (string: string) => {
  let url;

  try {
    url = new URL(string);
  } catch (_) {
    return false;
  }

  return url?.protocol === 'http:' || url?.protocol === 'https:';
};

export const debounce = (func: (...args: any[]) => void, timeout = 300) => {
  let timer: any;
  return (...args: any[]) => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      func.apply(this, args);
    }, timeout);
  };
};

export function checkPropsEqual(previousProps, nextProps) {
  return JSON.stringify(previousProps, getCircularReplacer()) === JSON.stringify(nextProps, getCircularReplacer());
}

export const getCircularReplacer = () => {
  //form a closure and use this
  //weakset to monitor object reference.
  const seen = new WeakSet();

  //return the replacer function
  return (key, value) => {
    if (typeof value === 'object' && value !== null) {
      if (seen.has(value)) {
        return;
      }
      seen.add(value);
    }
    return value;
  };
};

export function formatNumber(value) {
  return Number(value).toLocaleString();
}

export function capitalize(word: string) {
  return word.charAt(0).toUpperCase() + word.slice(1);
}

export function displayObjectDate(date, format = 'L', noRelativeTime = false) {
  // https://day.js.org/docs/en/display/format#list-of-localized-formats
  day.extend(localizedFormat);
  day.extend(relativeTime);
  const objectCreatedDate = new Date(date);
  const { locale } = useRouter();
  const dateDiff = Math.abs(objectCreatedDate - new Date()) / 1000; // get date difference
  // else, if date diff less than 1 day (3600sec * 24h), AND noRelativeTime param is false, display related date (eg: a few seconds ago, 4 days ago...)
  if (dateDiff < 86400 && !noRelativeTime) return day(objectCreatedDate).locale(locale).fromNow();
  // else display date with day, month & year
  return day(objectCreatedDate).locale(locale).format(format);
}

export function displayObjectUTCDate(date) {
  day.extend(localizedFormat);
  const objectCreatedDate = new Date(date);
  const { locale } = useRouter();
  return day(objectCreatedDate).locale(locale).format('lll') + '(UTC)';
}

export function displayObjectRelativeDate(date, locale: string, withMonthlyLimit = true, withoutSuffix = false) {
  const objectCreatedDate = new Date(date);
  day.extend(localizedFormat);
  day.extend(relativeTime);

  if (withMonthlyLimit) {
    const dateDiff = Math.abs(objectCreatedDate?.getTime() - new Date()?.getTime()) / 1000; // get date difference
    if (dateDiff > 3974400) return day(objectCreatedDate).format('DD/MM/YYYY'); // if date is approx older than 1 month, show full date
  }

  return day(objectCreatedDate).locale(locale).fromNow(withoutSuffix); // else show relative date
}

export function displayPartsOdDateDate(originalDate, part) {
  const date = new Date(originalDate);
  day.extend(localizedFormat);
  day.extend(relativeTime);
  return day(date).format(part); // if date is approx older than 1 month, show full date
}

export function isDateUrgent(date) {
  const objectCreatedDate = new Date(date);
  const dateDiff = Math.abs(objectCreatedDate - new Date()) / 1000; // get date difference
  // else, if date diff less than 2 days (3600sec * 24h * 2), display related date without time update
  if (dateDiff < 86400) return true;
}

// return first link of a post (the one we want the metadata from)
export function returnFirstLink(content) {
  if (content) {
    const regexLinks = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/gm; // match all links (http/https only!)
    const match = content.match(regexLinks);
    if (match) {
      const firstLink = match[0];
      return firstLink;
    }
  }
}

// default settings/params for a toast alert
export const confAlert = Swal.mixin({
  toast: true,
  position: 'bottom-start',
  showConfirmButton: false,
  timer: 4000,
});

// default settings/params for the "changes were saved" modal
export function changesSavedConfAlert(t) {
  return Swal.mixin({
    icon: 'success',
    title: t('saveChangesSuccess'),
    showCancelButton: true,
    confirmButtonColor: theme`colors.primary`,
    confirmButtonText: t('action.seePage'),
    cancelButtonText: t('action.continueEditing'),
  });
}

export function copyPostLink(objectId, objectType, commentId, t) {
  // function to copy post link to user clipboard, we use a tweak because the browser don't like we force copy to clipboard
  const el = document.createElement('textarea'); // create textarea element
  const link =
    objectType !== 'comment'
      ? `${window.location.origin}/${objectType}/${objectId}` // if objectType is not comment, make its value be the object url
      : objectType === 'comment' && `${window.location.origin}/post/${objectId}#comment-${commentId}`; // if it' a comment, make its value be the comment single url
  el.value = link;
  el.setAttribute('readonly', '');
  el.style.position = 'absolute'; // put some style that make it not visible
  el.style.left = '-9999px';
  document.body.appendChild(el); // add element to html (body)
  el.select(); // select the textarea text
  document.execCommand('copy'); // copy content to user clipboard
  document.body.removeChild(el); // remove element
  // show conf alert
  message.success(
    objectType === 'post'
      ? t('copyPostLinkSuccess')
      : objectType === 'comment'
      ? t('copyReplyLinkSuccess')
      : t('copyLinkSuccess')
  );
}
export function copyLink(url, t) {
  navigator.clipboard.writeText(`${window.location.origin}${url}`);
  confAlert.fire({
    icon: 'success',
    title: t('copyLinkSuccess'),
  });
}

export function reportContent(contentType, postId, commentId, api, t) {
  // function to send mail with the reported post to a JOGL admin
  const reportLink =
    contentType === 'post'
      ? `${window.location.origin}/post/${postId}`
      : `${window.location.origin}/post/${postId}#comment-${commentId}`;
  const param = {
    object: `${contentType} report`, // mail subject
    content: `The user reported the following ${contentType}: ${reportLink}`, // mail content, with reported post/comment link
  };
  api
    .post('/api/users/2/send_email', param) // send it to user 2, which is JOGL admin user
    .then(() => {
      // show conf alert
      confAlert.fire({
        icon: 'success',
        title: contentType === 'post' ? t('action.reportPost.conf') : t('action.reportComment.conf'),
      });
    });
}

export function useWhyDidYouUpdate(name, props) {
  // Get a mutable ref object where we can store props ...
  // ... for comparison next time this hook runs.
  const previousProps = useRef();

  useEffect(() => {
    if (previousProps.current) {
      // Get all keys from previous and current props
      const allKeys = Object.keys({ ...previousProps.current, ...props });
      // Use this object to keep track of changed props
      const changesObj = {};
      // Iterate through keys
      allKeys.forEach((key) => {
        // If previous is different from current
        if (previousProps.current[key] !== props[key]) {
          // Add to changesObj
          changesObj[key] = {
            from: previousProps.current[key],
            to: props[key],
          };
        }
      });
    }

    // Finally update previousProps with current props for next hook call
    previousProps.current = props;
  });
}

export function defaultSdgsInterests(translate) {
  return [
    { value: '1', label: translate('legacy.sdg.title.1'), color: '#EC1B30' },
    { value: '2', label: translate('legacy.sdg.title.2'), color: '#D49F2B' },
    { value: '3', label: translate('legacy.sdg.title.3'), color: '#2DA354' },
    { value: '4', label: translate('legacy.sdg.title.4'), color: '#CB233B' },
    { value: '5', label: translate('legacy.sdg.title.5'), color: '#ED4129' },
    { value: '6', label: translate('legacy.sdg.title.6'), color: '#02B7DF' },
    { value: '7', label: translate('legacy.sdg.title.7'), color: '#FEBF12' },
    { value: '8', label: translate('legacy.sdg.title.8'), color: '#981A41' },
    { value: '9', label: translate('legacy.sdg.title.9'), color: '#F37634' },
    { value: '10', label: translate('legacy.sdg.title.10'), color: '#DE1768' },
    { value: '11', label: translate('legacy.sdg.title.11'), color: '#FA9D26' },
    { value: '12', label: translate('legacy.sdg.title.12'), color: '#C69932' },
    { value: '13', label: translate('legacy.sdg.title.13'), color: '#498A50' },
    { value: '14', label: translate('legacy.sdg.title.14'), color: '#347DB7' },
    { value: '15', label: translate('legacy.sdg.title.15'), color: '#40B04A' },
    { value: '16', label: translate('legacy.sdg.title.16'), color: '#02558B' },
    { value: '17', label: translate('legacy.sdg.title.17'), color: '#1B366B' },
  ];
}

export function kFormatter(num) {
  return Math.abs(num) > 999
    ? Math.sign(num) * (Math.abs(num) / 1000).toFixed(1) + 'k'
    : Math.sign(num) * Math.abs(num);
}

export function formatNumberToShowCommas(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}

export function openShareConnectionsModal(id, api, t, showModal) {
  // get all user's mutual connections, then show modal displaying them
  api.get(`/users/${id}/mutual`).then((res) => {
    showModal({
      children: (
        <>
          <Grid tw="py-0 sm:py-2 md:py-4">
            {res.data.map((member, i) => (
              <UserCard key={i} user={member} isCompact />
            ))}
          </Grid>
          <div tw="self-center mt-5">
            <Link href={`/user${id}`} passHref legacyBehavior>
              <Button>{t('action.seeProfile')}</Button>
            </Link>
          </div>
        </>
      ),
      maxWidth: '61rem',
      title: t('mutualConnection', { count: 2 }),
    });
  });
}

export function proposalStatus(status, dates) {
  // function that returns proposal status (string), depending on submission state (status) and dates
  const currentDate = new Date();
  const afterStart = currentDate > new Date(dates.start);
  const afterDeadline = currentDate > new Date(dates.deadline);
  return status !== 'draft'
    ? status === 'accepted'
      ? ['legacy.proposalStatus.positively_reviewed', '#1CB5AC']
      : status === 'rejected'
      ? ['legacy.proposalStatus.reviewed', '#643CC6']
      : afterDeadline
      ? ['legacy.proposalStatus.under_review', '#F2A611']
      : afterStart && ['legacy.proposalStatus.isSubmitted', '#0a36d2']
    : ['legacy.proposalStatus.not_submitted', '#A4B6C5'];
}

export function cfpStatus(status, dates) {
  // function that returns cfp status (string), depending on dates and visibility
  const currentDate = new Date();
  const afterStart = dates.start && currentDate > new Date(dates.start);
  const afterDeadline = dates.deadline && currentDate > new Date(dates.deadline);
  const afterStop = status === 'archived';
  return status !== 'draft'
    ? afterStop
      ? ['closed', '#643CC6']
      : afterDeadline
      ? ['reviewOngoing', '#F2A611']
      : afterStart
      ? ['openForSubmissions', '#1CB5AC']
      : ['startingSoon', '#ff88de']
    : ['draft', '#A4B6C5'];
}

export function showColoredStatus(status, t) {
  return (
    <div tw="inline-flex space-x-2 items-center">
      <div tw="rounded-full h-2 w-2" style={{ backgroundColor: status !== '?' ? status[1] : 'grey' }} />
      <div
        style={{ color: status !== '?' ? status[1] : 'grey' }}
        tw="flex w-[fit-content] rounded-md items-center"
        data-tip={t('status')}
        data-for="challengeCard_status"
      >
        <div tw="font-bold">{t(status[0])}</div>
      </div>
      <ReactTooltip id="challengeCard_status" effect="solid" />
    </div>
  );
}

export function hasCfpDatePassed(prDate) {
  const currentDate = new Date();
  const stopOrDeadlineDate = new Date(prDate);

  return currentDate > stopOrDeadlineDate;
}

export function hasCfpStarted(proposalsNb) {
  return proposalsNb !== 0;
}

export const addressFront = process.env.ADDRESS_FRONT;

export function isGroupNotChallenge(createdDate, customType) {
  const dateLimit = new Date('2022 04 20');
  const chalCreatedDate = new Date(createdDate);
  // returns true if group(challenge) was created after Apr 20th 2022, or if customType is "Group"
  return chalCreatedDate > dateLimit || customType === 'Group';
}

export function isRole(checkedRole, userRole) {
  return checkedRole === userRole;
}

export function isMember(object) {
  return (
    object.user_access_level !== undefined &&
    object.user_access_level !== 'visitor' &&
    object.user_access_level !== 'pending'
  );
}
export function isAdmin(object) {
  return (
    object.user_access_level === 'admin' ||
    object.user_access_level === 'owner' ||
    object?.permissions?.includes('manage')
  );
}
export function isOwner(object) {
  return object.user_access_level === 'owner';
}

export function convertBase64(file: Blob): Promise<string> {
  return new Promise((resolve, reject) => {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);

    fileReader.onload = () => {
      if (typeof fileReader.result === 'string') {
        resolve(fileReader.result);
      } else if (fileReader.result instanceof ArrayBuffer) {
        // Convert ArrayBuffer to string
        const uint8Array = new Uint8Array(fileReader.result);
        const binaryString = uint8Array.reduce((acc, byte) => acc + String.fromCharCode(byte), '');
        const base64String = btoa(binaryString);
        resolve(`data:${file.type};base64,${base64String}`);
      }
    };

    fileReader.onerror = (error) => {
      reject(error);
    };
  });
}

export const getBase64FromUrl = async (url) => {
  const data = await fetch(url);
  const blob = await data.blob();
  return new Promise((resolve) => {
    const reader = new FileReader();
    reader.readAsDataURL(blob);
    reader.onloadend = () => {
      const base64data = reader.result;
      resolve(base64data);
    };
  });
};

export const generateSlug = (title) => {
  let proposalShortName = title.trim();
  proposalShortName = toAlphaNum(proposalShortName);
  return proposalShortName;
};

export const entityItem = {
  organization: {
    translationId: 'organization.one',
    front_path: 'organization',
    back_path: 'organizations',
    icon: 'carbon:location-company',
    color: '#FFBFDE',
    default_banner: '/images/default/default-organization.png',
    default_logo: '/images/default/default-organization-logo.png',
  },
  organizations: {
    translationId: 'organization.one',
    front_path: 'organization',
    back_path: 'organizations',
    icon: 'carbon:location-company',
    color: '#FFBFDE',
    default_banner: '/images/default/default-organization.png',
  },
  hub: {
    translationId: 'hub.one',
    front_path: 'hub',
    back_path: 'nodes',
    icon: 'carbon:edge-node-alt',
    color: '#9FFFDD',
    default_banner: '/images/default/default-hub.png',
  },
  node: {
    translationId: 'hub.one',
    front_path: 'hub',
    back_path: 'nodes',
    icon: 'carbon:edge-node-alt',
    color: '#9FFFDD',
    default_banner: '/images/default/default-hub.png',
  },
  nodes: {
    translationId: 'hub.one',
    front_path: 'hub',
    back_path: 'nodes',
    icon: 'carbon:edge-node-alt',
    color: '#9FFFDD',
    default_banner: '/images/default/default-hub.png',
  },
  need: {
    translationId: 'need.one',
    name_plural: 'need.other',
    front_path: 'need',
    back_path: 'needs',
    icon: 'material-symbols:extension-outline',
    color: '#A0CDFB',
    default_banner: '/images/default/default-workspace.png',
  },
  callforproposal: {
    translationId: 'callForProposals.one',
    front_path: 'cfp',
    back_path: 'cfps',
    icon: 'bx:medal',
    color: '#F2A611',
    default_banner: '/images/default/default-cfp.png',
  },
  cfp: {
    translationId: 'callForProposals.one',
    front_path: 'cfp',
    back_path: 'cfps',
    icon: 'bx:medal',
    color: '#F2A611',
    default_banner: '/images/default/default-cfp.png',
  },
  cfps: {
    translationId: 'callForProposals.one',
    front_path: 'cfp',
    back_path: 'cfps',
    icon: 'bx:medal',
    color: '#F2A611',
    default_banner: '/images/default/default-cfp.png',
  },
  paper: {
    translationId: 'paper.one',
    name_plural: 'library',
    front_path: 'paper',
    back_path: 'papers',
    icon: 'ri:article-line',
    color: '#A0CDFB',
    default_banner: '/images/default/default-paper.png',
  },
  jogldoc: {
    translationId: 'joglDoc',
    front_path: 'doc',
    back_path: 'documents',
    icon: 'mdi:file-document-edit-outline',
    color: '#A0CDFB',
    default_banner: '/images/default/default-jogldoc.png',
  },
  document: {
    translationId: 'joglDoc',
    name_plural: 'document.other',
    front_path: 'doc',
    back_path: 'feed/contentEntities',
    icon: 'mdi:file-document-edit-outline',
    color: '#A0CDFB',
    default_banner: '/images/default/default-jogldoc.png',
  },
  documents: {
    translationId: 'joglDoc',
    name_plural: 'document.other',
    front_path: 'doc',
    back_path: 'feed/contentEntities',
    icon: 'mdi:file-document-edit-outline',
    color: '#A0CDFB',
    default_banner: '/images/default/default-jogldoc.png',
  },
  doc: {
    translationId: 'joglDoc',
    front_path: 'doc',
    back_path: 'feed/contentEntities',
    icon: 'mdi:file-document-edit-outline',
    color: '#A0CDFB',
    default_banner: '/images/default/default-jogldoc.png',
  },
  announcement: {
    translationId: 'post.one',
    front_path: 'post',
    back_path: 'post',
    icon: 'material-symbols:chat-outline',
    color: '#A0CDFB',
    default_banner: '/images/default/default-banner.png',
  },
  post: {
    translationId: 'post.one',
    front_path: 'post',
    back_path: 'post',
    icon: 'material-symbols:chat-outline',
    color: '#A0CDFB',
    default_banner: '/images/default/default-banner.png',
  },
  proposal: {
    translationId: 'proposal.one',
    front_path: 'proposal',
    back_path: 'proposals',
    icon: 'material-symbols:chat-outline',
    color: '#A0CDFB',
    default_banner: '/images/default/default-banner.png',
  },
  user: {
    translationId: 'user.one',
    front_path: 'user',
    back_path: 'users',
    icon: 'mdi:people-check',
    color: '#FBE1B0',
    default_banner: '/images/default/default-user-banner.svg',
  },
  users: {
    translationId: 'user.one',
    front_path: 'user',
    back_path: 'users',
    icon: 'mdi:people-check',
    color: '#FBE1B0',
    default_banner: '/images/default/default-user-banner.svg',
  },
  event: {
    translationId: 'event.one',
    name_plural: 'event.other',
    front_path: 'event',
    back_path: 'events',
    icon: 'material-symbols:event-note-outline',
    color: '#BDA0DA',
    default_banner: '/images/default/default-event.png',
  },
  events: {
    translationId: 'event.one',
    front_path: 'event',
    back_path: 'events',
    icon: 'material-symbols:event-note-outline',
    color: '#BDA0DA',
    default_banner: '/images/default/default-event.png',
  },
  workspace: {
    translationId: 'workspace.one',
    front_path: 'workspace',
    back_path: 'workspaces',
    icon: 'material-symbols:groups-outline',
    color: '#F9E452',
    default_banner: '/images/default/default-workspace.png',
    default_logo: '/images/default/default-workspace-logo.png',
  },
  workspaces: {
    translationId: 'workspace.one',
    front_path: 'workspace',
    back_path: 'workspaces',
    icon: 'material-symbols:groups-outline',
    color: '#F9E452',
    default_banner: '/images/default/default-workspace.png',
    default_logo: '/images/default/default-workspace-logo.png',
  },
  channel: {
    translationId: 'discussion.one',
    front_path: 'channel',
    back_path: 'channels',
    icon: 'material-symbols:chat-outline',
    color: '#BDA0DA',
    default_banner: '/images/default/default-channel.png',
    default_logo: '/images/default/default-channel-logo.png',
  },
};

export const renderShortObjectCard = (type, object) => (
  <div tw="shadow-custom2 pl-2 pr-4 py-3 items-center flex gap-2 rounded-md max-w-[18rem]">
    <div
      style={{
        backgroundImage: `url(${object?.banner_url_sm || entityItem[type].default_banner})`,
      }}
      tw="bg-cover bg-center h-[40px] w-[40px] rounded-full border border-solid border-[#ced4da] flex-none"
    />
    <div>
      <Link tw="font-semibold text-[15px] line-clamp-1" href={`/${entityItem[type].front_path}/${object?.id}`}>
        {object?.title}
      </Link>
      {/* <div tw="line-clamp-2 text-[#343a40] pt-2 text-sm">{object?.short_description}</div> */}
    </div>
  </div>
);

export const renderMediumObjectCard = (type, object) => (
  <div tw="shadow-custom2 items-center flex gap-2 rounded-md w-[18rem]">
    <div
      style={{
        backgroundImage: `url(${object?.banner_url_sm || entityItem[type].default_banner})`,
      }}
      tw="bg-cover bg-center h-[90px] w-[50px] flex-none"
    />
    <div tw="p-2">
      <Link
        tw="font-bold text-[15px] line-clamp-1"
        href={`/${entityItem[type].front_path}/${object?.id}`}
        target="_blank"
        rel="noopener noreferrer"
      >
        {object?.title}
      </Link>
      <div tw="line-clamp-2 text-[#343a40] pt-2 text-sm">{object?.short_description}</div>
    </div>
  </div>
);

export const needUrl = (needId) => {
  return `/need/${needId}`;
};

export const getFullValidationObject = (validator: FormValidator, object: any) => {
  let formattedValidation = {};
  const validation = validator.validate(object);

  Object.keys(validation).map((key: any) => {
    if (key !== 'isValid') {
      formattedValidation = { ...formattedValidation, [`valid_${key}`]: validation[key] };
    }
  });

  return formattedValidation;
};

export const isValidEmail = (email: string) => {
  const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
  return emailRegex.test(email);
};

export const toInitials = (text: string) =>
  text
    ?.split(' ')
    .map((n) => n[0])
    .join('')
    .substr(0, 3);

export const getPaperAuthorChipOptions = (paper: any, t: Translate) => {
  if (paper?.tags?.includes('authoredbyme')) {
    return [{ name: t('authoredByMe').toLocaleLowerCase() }];
  }

  if (paper?.tags?.includes('authorof')) {
    return [{ name: t('authorOf').toLocaleLowerCase() }];
  }

  return undefined;
};

//Only use to test if a string starts and ends with {} basically. Sometimes this might not be enough

/*export const containerListingOrigins = {
  ecosystemmember: {
    name: 'Ecosystem member',
    tooltip: 'Only visible to ecosystem members.',
  },
  directmemember: {
    name: 'Member',
    tooltip: 'Only visible to members.',
  },
  public: {
    name: 'Public',
    tooltip: 'Visible to every user.',
  },
};*/

export const getContainerAccessLevelChip = (
  container: Hub | Organization | Cfp | NodeModel | CommunityEntityMiniModel | WorkspaceModel
): TextChip => {
  return {
    name: container?.user_access_level,
    // tooltip: containerAccessLevels[container?.user_access_level]?.tooltip, ## no tooltip for now
  };
};

export const containerAccessLevels = {
  owner: {
    tooltip: 'You can view this content as an owner.',
  },
  admin: {
    tooltip: 'You can view this content as an admin.',
  },
  member: {
    tooltip: 'You can view this content as a member.',
  },
};

export const containerContentPrivacies = {
  ecosystem: {
    tooltip: 'Content visibility is set to ecosystem members.',
  },
  private: {
    tooltip: 'Visibility is set to members only.',
  },
  public: {
    tooltip: 'Visibility is set to public.',
  },
  custom: {
    tooltip: 'Custom visibility settings.',
  },
};

export const containerContentOrigins = {
  ecosystemmember: {
    tooltip: 'You can view this content as an ecosystem member.',
  },
  directmemember: {
    tooltip: 'You can view this content as a member.',
  },
  public: {
    tooltip: 'You can view this content.',
  },
};

export const containerJoiningRestrictions = {
  request: {
    icon: 'mdi:user-clock-outline',
    tooltip: 'Membership on request.',
  },
  invite: {
    icon: 'mdi:email-send-outline',
    tooltip: 'Membership is invite only.',
  },
  custom: {
    icon: 'entypo:brush',
    tooltip: 'Custom membership settings.',
  },
  open: {
    icon: 'mdi:person-plus-outline',
    tooltip: 'Membership is open.',
  },
};

export const getContainerVisibilityChip = (
  container: Hub | Organization | Cfp | NodeModel | CommunityEntityMiniModel | WorkspaceModel
): ImageChip => {
  let chip = { icon: 'mdi:eye-outline', tooltip: '' };

  if (container?.user_access?.permissions?.includes('read')) {
    if (container?.user_access_level) {
      chip.tooltip =
        containerAccessLevels[container?.user_access_level]?.tooltip +
        ' ' +
        containerContentPrivacies[container?.content_privacy]?.tooltip;
    } else {
      chip.tooltip =
        containerContentOrigins[container?.content_origin?.type]?.tooltip +
        ' ' +
        containerContentPrivacies[container?.content_privacy]?.tooltip;
    }
  } else {
    chip.icon = 'mdi:eye-off-outline';
    chip.tooltip = "You can't view this content. " + containerContentPrivacies[container?.content_privacy]?.tooltip;
  }
  return chip;
};

export const getContainerJoiningRestrictionChip = (
  container: Hub | Organization | Cfp | NodeModel | CommunityEntityMiniModel | WorkspaceModel
): ImageChip => {
  return {
    icon: containerJoiningRestrictions[container?.joining_restriction]?.icon,
    tooltip: containerJoiningRestrictions[container?.joining_restriction]?.tooltip,
  };
};

export const getUserDefaultTimezone = () => {
  const { parseTimezone } = useTimezoneSelect({ labelStyle: 'original', timezones: allTimezones });
  let tz = parseTimezone(Intl.DateTimeFormat().resolvedOptions().timeZone);

  return {
    value: tz?.value,
    label: tz?.label,
  };
};

export const formatEventDate = (date: Date, tz: EventTimezone, format: string = 'llll') => {
  day.extend(day);

  const { parseTimezone } = useTimezoneSelect({ labelStyle: 'original', timezones: allTimezones });
  //Make sure it is a correct Date object - shouldn't be needed, but sometimes javascript complains...
  const originalDate = new Date(date);

  const eventTimezone = parseTimezone(tz?.value);
  const userTimezone = parseTimezone(Intl.DateTimeFormat().resolvedOptions().timeZone);

  const timeDifference = (userTimezone?.offset - eventTimezone?.offset) * 60;

  // Create a new date object with the time difference applied
  const localTime = new Date(originalDate.getTime() + timeDifference * 60000);

  return day(localTime).format(format);
};

export const formatEventDateInListView = (date: Date, tz: EventTimezone) => {
  day.extend(day);

  const { parseTimezone } = useTimezoneSelect({ labelStyle: 'original', timezones: allTimezones });
  //Make sure it is a correct Date object - shouldn't be needed, but sometimes javascript complains...
  const originalDate = new Date(date);

  const eventTimezone = parseTimezone(tz?.value);
  const userTimezone = parseTimezone(Intl.DateTimeFormat().resolvedOptions().timeZone);

  const timeDifference = (userTimezone?.offset - eventTimezone?.offset) * 60;

  // Create a new date object with the time difference applied
  const localTime = new Date(originalDate.getTime() + timeDifference * 60000);

  return day(localTime).format('LT');
};

export const areDatesInSameDay = (date1: string, date2: string): boolean => {
  const d1 = new Date(date1);
  const d2 = new Date(date2);

  return (
    d1.getUTCFullYear() === d2.getUTCFullYear() &&
    d1.getUTCMonth() === d2.getUTCMonth() &&
    d1.getUTCDate() === d2.getUTCDate()
  );
};

export const getTextWidth = (text: string, fontStyle: string) => {
  const canvas = document.createElement('canvas');
  const context = canvas.getContext('2d');
  context.font = fontStyle;
  return context.measureText(text).width;
};

export function isStringEnum<T extends string, TEnumValue extends string>(
  value: unknown,
  testEnum: { [key in T]: TEnumValue }
): value is TEnumValue {
  return typeof value === 'string' && Object.values(testEnum).includes(value);
}

export const isImageFileType = (type: string) => type?.startsWith('image/');

export const isAudioFileType = (type: string) => type?.startsWith('audio/');

export const isVideoFileType = (type: string) => type?.startsWith('video/');

export const isPdfFileType = (type: string) => type === 'application/pdf';

export const isImageAudioOrVideoFileType = (type: string) =>
  isImageFileType(type) || isAudioFileType(type) || isVideoFileType(type);

export const isHeifOrHeicFileType = (name: string, type: string) => {
  if (type === 'image/heif' || type === 'image/heic') {
    return true;
  }

  const inferredType = name.split('.')[1];
  return inferredType.toLowerCase() === 'heic' || inferredType.toLowerCase() === 'heif';
};

export const isOfficeSuiteFileType = (name: string, type: string) => {
  if (
    // .xls or .xlsx
    type === 'application/vnd.ms-excel' ||
    type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
    // .doc or .docx
    type === 'application/msword' ||
    type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
    // .ppt or .pptx
    type === 'application/vnd.ms-powerpoint' ||
    type === 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
  ) {
    return true;
  }

  const inferredType = name.split('.')[1];
  return inferredType && ['.xls', '.xlsx', '.doc', '.docx', '.ppt', '.pptx'].includes(inferredType.toLowerCase());
};

// https://emailregex.com/
export const EMAIL_REGEX = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/; // eslint-disable-line no-control-regex

export const deepEqual = (obj1: any, obj2: any): boolean => {
  if (obj1 === obj2) return true;

  if (typeof obj1 !== 'object' || obj1 === null || typeof obj2 !== 'object' || obj2 === null) {
    return obj1 === obj2;
  }

  const keys1 = Object.keys(obj1);
  const keys2 = Object.keys(obj2);

  if (keys1.length !== keys2.length) return false;

  for (const key of keys1) {
    if (!keys2.includes(key)) return false;
    if (!deepEqual(obj1[key], obj2[key])) return false;
  }

  return true;
};

export const getEntityIcon = (type: FeedType) => {
  switch (type) {
    case FeedType.Event:
      return menuItems.events.icon;
    case FeedType.Channel:
      return <Typography.Text>#</Typography.Text>;
    case FeedType.Need:
      return menuItems.needs.icon;
    case FeedType.Document:
      return menuItems.documents.icon;
    case FeedType.Paper:
      return menuItems.papers.icon;
    default:
      return;
  }
};
