import useTranslation from 'next-translate/useTranslation';
import { ItemType } from '~/src/types';

export const TextWithPlural = ({ type, count = 0 }: { type: ItemType; count: number }) => {
  const { t } = useTranslation('common');
  return <>{t(type, { count })}</>;
};

export function pluralText(type: ItemType, count = 0, router, t) {
  const { locale } = router;
  const translatedCount = count === 0 ? (locale === 'en' ? 2 : 1) : count;
  return t(type, { count: translatedCount });
}
