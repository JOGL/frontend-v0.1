import userProfileFormRules from '~/src/components/User/userProfileFormRules.json';
import hubFormRules from '~/src/components/Hub/HubFormRules.json';
import cfpFormRules from '~/src/components/Cfp/CfpFormRules.json';
import organizationFormRules from '~/src/components/Organization/OrganizationFormRules.json';
import proposalFormRules from '~/src/components/Proposal/proposalFormRules.json';
import needFormRules from '~/src/components/Need/needFormRules.json';
import jogldocFormRules from '~/src/components/JOGLDoc/jogldocFormRules.json';
import eventFormRules from '~/src/components/Event/EventFormRules.json';

export default function isMandatoryField(context: string, field: string) {
  switch (context) {
    case 'user':
      return checkUserRules(field);
    case 'hub':
      return checkHubRules(field);
    case 'cfp':
      return checkCfpRules(field);
    case 'organization':
      return checkOrganizationRules(field);
    case 'proposal':
      return checkProposalRules(field);
    case 'need':
      return checkNeedRules(field);
    case 'jogldoc':
      return checkJOGLDocRules(field);
    case 'event':
      return checkEventRules(field);
    default:
      return false;
  }
}

const checkUserRules = (field: string) => {
  if (field === 'skills' || field === 'interests') {
    return userProfileFormRules.findIndex((rule) => rule.field === field && rule.method === 'isLength') !== -1;
  }

  return userProfileFormRules.findIndex((rule) => rule.field === field && rule.method === 'isEmpty') !== -1;
};

const checkHubRules = (field: string) => {
  return hubFormRules.findIndex((rule) => rule.field === field && rule.method === 'isEmpty') !== -1;
};

const checkCfpRules = (field: string) => {
  return cfpFormRules.findIndex((rule) => rule.field === field && rule.method === 'isEmpty') !== -1;
};

const checkOrganizationRules = (field: string) => {
  if (field === 'keywords') {
    return organizationFormRules.findIndex((rule) => rule.field === field && rule.method === 'isLength') !== -1;
  }

  return organizationFormRules.findIndex((rule) => rule.field === field && rule.method === 'isEmpty') !== -1;
};

const checkProposalRules = (field: string) => {
  if (field === 'description') {
    return proposalFormRules.findIndex((rule) => rule.field === field && rule.method === 'minLength') !== -1;
  }

  return proposalFormRules.findIndex((rule) => rule.field === field && rule.method === 'isEmpty') !== -1;
};

const checkNeedRules = (field: string) => {
  return needFormRules.findIndex((rule) => rule.field === field && rule.method === 'isEmpty') !== -1;
};

const checkJOGLDocRules = (field: string) => {
  switch (field) {
    case 'user_ids':
      return jogldocFormRules.findIndex((rule) => rule.field === field && rule.method === 'isLength') !== -1;
    case 'description':
      return jogldocFormRules.findIndex((rule) => rule.field === field && rule.method === 'minLength') !== -1;
    default:
      return jogldocFormRules.findIndex((rule) => rule.field === field && rule.method === 'isEmpty') !== -1;
  }
};

const checkEventRules = (field: string) => {
  if (field === 'organizers') {
    return eventFormRules.findIndex((rule) => rule.field === field && rule.method === 'isLength') !== -1;
  }

  return eventFormRules.findIndex((rule) => rule.field === field && rule.method === 'isEmpty') !== -1;
};
