import { CommunityEntityMiniModel, EntityMiniModel } from '~/__generated__/types';
import { Owner, UsersSm } from './common';

export type NeedDisplayMode = 'default' | 'hub_discussions';

export interface Need {
  id: string;
  title: string;
  content: string;
  owner: Owner;
  description?: string;
  status: string;
  feed_id: number;
  users_sm: UsersSm[];
  skills: string[];
  ressources: string[];
  entity: CommunityEntityMiniModel;
  followers_count: number;
  members_count: number;
  created_by_user_id?: string;
  claps_count: number;
  saves_count: number;
  posts_count: number;
  created?: Date;
  updated: Date;
  end_date?: Date;
  is_urgent: boolean;
  is_owner: boolean;
  is_admin: boolean;
  is_member: boolean;
  user_follows: boolean;
  has_saved: boolean;
  feed_stats?: {
    post_count: number;
    new_post_count: number;
    comment_count: number;
    new_mention_count: number;
    new_thread_activity_count: number;
  };
  is_new?: boolean;
  type: string;
  permissions?: string[];
  path?: EntityMiniModel[];
}
