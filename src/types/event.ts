import { EntityMiniModel, WorkspaceModel } from '~/__generated__/types';
import { Cfp } from './cfp';
import { BaseContainerPrivacyOption, SelectContainerOption } from './common';
import { Hub } from './hub';
import { Organization } from './organization';
import { User } from './user';

export enum EventCreationStep {
  PickContainer = 'pick_container',
  PickPrivacy = 'pick_privacy',
}

export enum EventPrivacySetting {
  Visibility = 'visibility',
}

export type EventDisplayMode = 'default' | 'hub_discussions';
export type EventVisibility = 'public' | 'private' | 'container';

export interface Event {
  id: string;
  created_by_user_id?: string;
  title: string;
  description: string;
  generate_meet_link: boolean;
  generated_meeting_url: string;
  meeting_url: string;
  banner_id: string;
  banner_url: string;
  banner_url_sm: string;
  visibility: EventVisibility;
  start: Date;
  end: Date;
  timezone: EventTimezone;
  keywords: string[];
  location?: EventLocation;
  permissions?: string[];
  feed_entity?: {
    id: string;
    title: string;
    created: string;
    entity_type: 'workspace' | 'node' | 'user';
    short_description?: string;
    banner_url?: string;
    banner_url_sm?: string;
  };
  attendee_count?: number;
  invitee_count?: number;
  user_attendance?: EventAttendance;
  attendances_to_upsert?: EventInvite[];
  feed_stats?: {
    post_count: number;
    new_post_count: number;
    comment_count: number;
    new_mention_count: number;
    new_thread_activity_count: number;
  };
  is_new: boolean;
  path?: EntityMiniModel[];
}

export interface EventLocation {
  name?: string;
  url?: string;
}

export interface EventTimezone {
  value?: string;
  label?: string;
}

export interface EventAttendance {
  id?: string;
  event_id?: string;
  user?: User;
  user_email?: string;
  community_entity?: WorkspaceModel | Cfp | Hub | Organization;
  origin_community_entity?: WorkspaceModel | Cfp | Hub | Organization;
  labels?: string[];
  status?: 'pending' | 'yes' | 'no';
  access_level: 'admin' | 'member';
}

export interface EventInvite {
  user_id?: string;
  user_email?: string;
  community_entity_id?: string;
  origin_community_entity_id?: string;
  labels?: string[];
  access_level: 'admin' | 'member';
}

export interface EventSelectContainerOption extends SelectContainerOption {
  content_privacy: string;
  members_count: number;
}

export interface EventPrivacyOption extends BaseContainerPrivacyOption {
  key: EventPrivacySetting;
  options: EventPrivacyOptionChild[];
}

export interface EventPrivacyOptionChild extends BaseContainerPrivacyOption {
  key: string;
}

export const EVENT_PRIVACY_SETTINGS: EventPrivacyOption[] = [
  {
    key: EventPrivacySetting.Visibility,
    title: 'visibilitySettings',
    info: '',
    options: [
      {
        key: 'private',
        title: 'privateEvent',
        info: 'onlyInvitedUsersCanSeeTheEventInTheirCalendar',
      },
      {
        key: 'container',
        title: '',
        info: 'allMembersOfContainerCanSeeTheEventInContainerCalendar',
      },
      {
        key: 'public',
        title: 'publicEvent',
        info: 'allJOGLUsersCanSeeTheEventAndAddItToTheirCalendar',
      },
    ],
  },
];
