export type PublicationDisplayMode = 'default' | 'hub_discussions';

export interface Publication {
  id?: string;
  title?: string;
  abstract?: string;
  journal?: string;
  publication_date?: string;
  authors?: string;
  external_id?: string;
  open_access_pdf?: string;
  is_new?: boolean;
  permissions?: string[];
  feed_stats?: {
    post_count: number;
    new_post_count: number;
    comment_count: number;
    new_mention_count: number;
    new_thread_activity_count: number;
  };
  feed_count?: number;
}

/*
  Easy to customize result object for each source later without breaking
  
  interface SemanticScholarPublication extends Publication {
  
  }
*/
