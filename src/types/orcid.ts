export interface OrcidUser {
  educations?: {
    school?: string;
    department_name?: string;
    degree_name?: string;
    dateFrom?: string;
    dateTo?: string;
  }[];
  employments?: {
    company?: string;
    department_name?: string;
    position?: string;
    date_from?: string;
    date_to?: string;
  }[];
}
