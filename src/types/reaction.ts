import { UserMini } from './user';

type ReactionType = 'clap';

export interface Reaction {
  created: Date;
  created_by_user_id?: string;
  updated?: Date;
  last_activity?: Date;
  id?: string;
  type: ReactionType;
  user_id?: string;
  created_by: UserMini;
}

export interface ReactionCount {
  type: ReactionType;
  count: number;
}
