export interface MenuActions {
  members?: ActionRow;
  content?: ActionRow[];
  containers?: ActionRow[];
}

export interface ActionRow {
  title: string;
  icon: string;
  privacy: string;
  disabled: boolean;
  handleSelect: () => void;
}

export interface ContainerPermissions {
  isAdmin: boolean;
  canAddMembers?: boolean;
  canCreateNeed?: boolean;
  canCreateJOGLDoc?: boolean;
  canCreatePapers?: boolean;
  canCreateDocuments?: boolean;
  canCreateResources?: boolean;
  canCreateCfp?: boolean;
  canCreateHub?: boolean;
  canCreateOrganization?: boolean;
  canCreateEvent?: boolean;
}
