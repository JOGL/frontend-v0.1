export interface Comment {
  id: string;
  created: Date;
  created_by_user_id?: string;
  updated?: Date;
  last_activity?: Date;
  text?: string;
  reply_to_id?: string;
  user_id?: string;
  created_by: any; // TODO[andre]: after https://gitlab.com/JOGL/frontend-v0.1/-/merge_requests/643 is merged use `UserMini` type here
  user_mentions: number;
}
