import { ItemType } from './itemType';

export interface EntityMini {
  created?: Date;
  created_by_user_id?: string;
  updated?: Date;
  last_activity?: Date;
  id?: string;
  full_name?: string;
  short_title?: string;
  title?: string;
  short_description?: string;
  banner_url?: string;
  banner_url_sm?: string;
  logo_url?: string;
  logo_url_sm?: string;
  entity_type: FeedType;
}

type FeedType =
  | 'workspace'
  | 'node'
  | 'need'
  | 'user'
  | 'organization'
  | 'paper'
  | 'document'
  | 'callforproposal'
  | 'event';

export interface CommunityEntityStats {
  cfp_count?: number;
  workspaces_count?: number;
  content_entity_count?: number;
  documents_count?: number;
  documents_count_aggregate?: number;
  hubs_count?: number;
  members_count?: number;
  needs_count?: number;
  needs_count_aggregate?: number;
  organization_count?: number;
  papers_count?: number;
  papers_count_aggregate?: number;
  participant_count?: number;
  proposal_count?: number;
  resources_count?: number;
  resources_count_aggregate?: number;
}

export interface CommunityEntityMini {
  created?: Date;
  created_by_user_id?: string;
  updated?: Date;
  last_activity?: Date;
  id?: string;
  full_name?: string;
  short_title?: string;
  title?: string;
  short_description?: string;
  banner_url?: string;
  banner_url_sm?: string;
  logo_url?: string;
  logo_url_sm?: string;
  type: ItemType;
  user_access_level?: string;
  content_privacy?: string;
  stats?: CommunityEntityStats;
}
