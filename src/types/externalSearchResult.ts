export interface ExternalSearchResult<T> {
  total?: number;
  items?: T[];
}
