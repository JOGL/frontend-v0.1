import { EntityMiniModel } from '~/__generated__/types';
import { Cfp } from './cfp';
import { User } from './user';

export interface Proposal {
  id: string;
  project: EntityMiniModel;
  call_for_proposal: Cfp;
  community: EntityMiniModel;
  title: string;
  description: string;
  status: string;
  users: User[];
  answers: {
    question_key: string;
    answer: string;
  }[];
  created: string;
  created_by_user_id: string;
  updated: string;
  last_activity?: string;
  submitted_at: Date;
  score?: number;
}
