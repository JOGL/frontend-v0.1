export enum DocType {
  'link' = 1,
  'upload' = 2,
  'jogl_doc' = 3,
}
