export interface Resource {
  id: number;
  title: string;
  content: string;
  position: number;
}
