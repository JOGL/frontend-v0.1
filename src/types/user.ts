import { EntityMini } from './entity';

interface Badge {
  id: number;
  name: string;
  description: string;
  custom_fields?: any;
}

export interface UserMini {
  created: Date;
  created_by_user_id?: string;
  updated?: Date;
  last_activity?: Date;
  id?: string;
  first_name?: string;
  last_name?: string;
  username?: string;
  short_bio?: string;
  banner_url?: string;
  banner_url_sm?: string;
  logo_url?: string;
  logo_url_sm?: string;
  status?: string;
  country?: string;
  city?: string;
  follower_count: number;
  user_follows: boolean;
  stats: CommunityEntityStat;
  organizations: EntityMini[];
}

interface UserOrganization {
  created: string;
  created_by_user_id: string;
  updated: string;
  last_activity: string;
  id: string;
  full_name: string;
  short_title: string;
  title: string;
  short_description: string;
  banner_url: string;
  banner_url_sm: string;
  logo_url: string;
  logo_url_sm: string;
  entity_type: string;
}

export interface User {
  id: string;
  first_name: string;
  last_name: string;
  username: string;
  affiliation: string;
  category: string;
  country: string;
  city: string;
  bio?: string;
  short_bio: string;
  orcid_id?: string;
  email?: string;
  external_auth?: {
    is_google_user?: boolean;
    is_orcid_user?: boolean;
  };
  contact_me: boolean;
  status: string;
  created: Date;
  interests: number[];
  skills: string[];
  ressources: string[];
  badges: Badge[];
  feed_id: string;
  logo_url: string;
  logo_url_sm: string;
  banner_url: string;
  banner_url_sm: string;
  mail_weekly: boolean;
  mail_newsletter: boolean;
  is_admin: boolean;
  has_clapped: boolean;
  experience?: UserExperience[];
  education?: UserEducation[];
  user_follows: boolean;
  has_saved: boolean;
  geoloc: {
    lat: number;
    lng: number;
  };
  stats: {
    claps_count: number;
    saves_count: number;
    followers_count: number;
    following_count: number;
    mutual_count: number;
    needs_count: number;
    workspaces_count: number;
  };
  notification_settings?: UserNotificationSettings | null;
  language?: string | null;
  organizations?: UserOrganization[];
}

interface CommunityEntityStat {
  members_count: number;
  workspaces_count: number;
  cfp_count: number;
  organization_count: number;
  hubs_count: number;
  needs_count: number;
  content_entity_count: number;
  participant_count: number;
}

export interface UserExperience {
  company?: string;
  position?: string;
  description?: string;
  date_from?: string;
  date_to?: string;
}

export interface UserEducation {
  school?: string;
  description?: string;
  dateFrom?: string;
  dateTo?: string;
}

export interface UserNotificationSettings {
  mentionJogl: boolean;
  mentionEmail: boolean;
  postMemberContainerJogl: boolean;
  postMemberContainerEmail: boolean;
  threadActivityJogl: boolean;
  threadActivityEmail: boolean;
  postAuthoredObjectJogl: boolean;
  postAuthoredObjectEmail: boolean;
  documentMemberContainerJogl: boolean;
  documentMemberContainerEmail: boolean;
  needMemberContainerJogl: boolean;
  needMemberContainerEmail: boolean;
  paperMemberContainerJogl: boolean;
  paperMemberContainerEmail: boolean;
  containerInvitationJogl: boolean;
  containerInvitationEmail: boolean;
  eventInvitationJogl: boolean;
  eventMemberContainerJogl: boolean;
  eventMemberContainerEmail: boolean;
  postAttendingEventJogl: boolean;
  postAttendingEventEmail: boolean;
  postAuthoredEventJogl: boolean;
  postAuthoredEventEmail: boolean;
}