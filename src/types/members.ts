import { Geoloc } from './common';

export interface Members {
  users: {
    id: string;
    invitation_id: string;
    first_name: string;
    last_name: string;
    owner: boolean;
    admin: boolean;
    member: boolean;
    user_follows: boolean;
    has_clapped: boolean;
    logo_url: string;
    logo_url_sm: string;
    short_bio: string;
    geoloc: Geoloc;
    contact_me: boolean;
    bio: string;
    stats: {
      mutual_count: number;
      workspaces_count: number;
    };
  }[];
}
