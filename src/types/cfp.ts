import { EntityMiniModel } from '~/__generated__/types';

export interface Cfp {
  id: string;
  title: string;
  short_title: string;
  banner_url: string;
  banner_url_sm: string;
  short_description: string;
  status: string;
  skills: string[];
  ressources: string[];
  interests: number[];
  country?: string;
  city?: string;
  feed_id: number;
  created: Date;
  updated: Date;
  user_follows: boolean;
  has_saved: boolean;
  description: string;
  scoring?: boolean;
  max_score?: string;
  template: CfpTemplate;
  user_access_level: string;
  user_access: { permissions: string[] };
  listing_origin?: { type: string; ecosystem_memberships?: any[] };
  content_origin?: { type: string; ecosystem_memberships?: any[] };
  joining_restriction: string;
  content_privacy: string;
  type?: string;
  stats?: {
    workspaces_count?: number;
    content_entity_count?: number;
    hubs_count?: number;
    members_count?: number;
    needs_count?: number;
    organization_count?: number;
    participant_count?: number;
    submitted_proposals_count?: number;
  };
  submissions_from?: Date;
  submissions_to?: Date;
  path?: EntityMiniModel[];
  user_joining_restriction_level?: string;
  listing_privacy: string;
  management: string[];
  proposal_participation?: string;
  proposal_privacy?: string;
  discussion_participation?: string;
  home_channel_id?: string | null;
}

export interface CfpTemplate {
  questions: CfpTemplateQuestion[];
}

export interface CfpTemplateQuestion {
  key: string;
  type: string;
  title: string;
  description: string;
  choices: string[];
}
