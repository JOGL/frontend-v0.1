export interface TextChip {
  name: string;
  tooltip?: string;
}

export interface ImageChip {
  icon: string;
  tooltip?: string;
}
