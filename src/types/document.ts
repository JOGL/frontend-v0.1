import { ContentEntityVisibility } from './contentEntity';
import { EntityMini } from './entity';
import { FeedStat } from './feed';
import { UserMini } from './user';

type DocumentType = 'document' | 'link' | 'jogldoc';

export interface Document {
  created: Date;
  created_by_user_id?: string;
  updated?: Date;
  last_activity?: Date;
  id?: string;
  title?: string;
  file_name?: string;
  file_type?: string;
  file_size_bytes: number;
  url?: string;
  type: DocumentType;
  description?: string;
  data?: string;
  image_url?: string;
  document_url?: string;
  image_url_sm?: string;
  created_by: UserMini;
  feed_entity: EntityMini;
  users: UserMini[];
  feed_stats: FeedStat;
  status: 'active' | 'draft';
  visibility: ContentEntityVisibility;
  image_id?: string;
  folder_id?: string;
  is_folder: boolean;
  is_new: boolean;
  permissions: Permission[];
  path?: EntityMini[];
}

export type Permission =
  | 'read'
  | 'manage'
  | 'manageowners'
  | 'delete'
  | 'postneed'
  | 'postcontententity'
  | 'postcomment'
  | 'managelibrary'
  | 'managedocuments'
  | 'postresources'
  | 'deletecontententity'
  | 'deletecomment'
  | 'createworkspaces'
  | 'createproposals'
  | 'listproposals'
  | 'scoreproposals'
  | 'mentioneveryone';
