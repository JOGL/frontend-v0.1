export interface KnownUrl {
  name: string;
  icon: string;
  image: string;
}
