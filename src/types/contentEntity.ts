import { Document } from './document';
import { EntityMini } from './entity';
import { FeedStat } from './feed';
import { Reaction, ReactionCount } from './reaction';
import { UserMini } from './user';

type ContentEntityType = 'announcement' | 'jogldoc' | 'preprint' | 'article' | 'protocol' | 'need' | 'paper';

export type ContentEntityVisibility = 'public' | 'entity' | 'event' | 'ecosystem' | 'authors';

export interface ContentEntity {
  created: Date;
  created_by_user_id?: string;
  updated?: Date;
  last_activity?: Date;
  id?: string;
  type: ContentEntityType;
  title?: string;
  users: UserMini[];
  summary?: string;
  text?: string;
  status: 'active' | 'draft';
  visibility: ContentEntityVisibility;
  labels: string[];
  date_time_utc: Date;
  linked_entity_id?: string;
  created_by: UserMini;
  feed_entity: EntityMini;
  documents: Document[];
  reaction_count: ReactionCount[];
  comment_count: number;
  new_comment_count: number;
  user_reactions: Reaction[];
  user_mentions: number;
  user_mentions_in_comments: number;
  image_url?: string;
  image_url_sm?: string;
  feed_stats: FeedStat;
  is_new: boolean;
}
