import { Owner } from './common';

interface Mention {
  obj_type: string;
  obj_id: number;
  obj_match: string;
}

interface From {
  object_type: string;
  object_id: number;
  object_name: string;
  object_image?: any;
  object_need_proj_id?: number;
}

interface Clapper {
  id?: any;
  user_id: number;
}

export interface Post {
  id: string;
  content: string;
  type: 'announcement' | 'jogldoc' | 'protocol' | 'need' | 'update' | 'meeting' | 'poll' | 'paper';
  media?: any;
  owner: Owner;
  mentions: Mention[];
  from: From;
  comments: any[];
  title?: string;
  text?: string;
  feed_entity?: {
    id: string;
    title: string;
  };
  created_by?: {
    id: string;
    first_name: string;
    last_name: string;
  };
  created: Date;
  documents: any[];
  claps_count: number;
  saves_count: number;
  clappers: Clapper[];
  has_clapped: boolean;
  has_saved: boolean;
  comment_count: number;
  new_comment_count: number;
  user_mentions: number;
  user_mentions_in_comments: number;
}
