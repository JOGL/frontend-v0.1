export interface SearchModel {
  page: number;
  pageSize: number;
}
