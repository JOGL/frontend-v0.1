import { Publication } from './publication';

export interface Author {
  name: string;
  papers: Publication[];
}
