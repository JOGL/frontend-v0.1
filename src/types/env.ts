export enum NEXT_PUBLIC_ENVIRONMENT {
  develop = 'develop',
  staging = 'staging',
  production = 'production',
}
