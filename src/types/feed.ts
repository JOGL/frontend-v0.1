import { Post } from './post';

export interface FeedData {
  total: number;
  items: Post[];
  unread_posts: number;
  unread_mentions: number;
  unread_threads: number;
}

export interface FeedStat {
  post_count: number;
  new_post_count: number;
  new_mention_count: number;
  new_thread_activity_count: number;
}
