# License
This project is licensed under the GNU Affero General Public License v3.0 (AGPL-3.0). For the full license text, please see the LICENSE  file in the root directory of this repository.

# Contribute

JOGL is **100% open source**, and we fully welcome contributions! You can help us by **contributing to the code**, or translating the platform in your language.

We use the following technologies for frontend: [ReactJS](https://reactjs.org/), [NextJS](https://github.com/vercel/next.js/), Typescript, Algolia, Sass, AmazonS3, GraphQL, Elastic Search... If you have experience in one or multiple of them, your help will be much appreciated!

Feel free to browse through the issues and see if you can help us in one or multiple of them:

- **List view** - View issues in list: [frontend-only issues](https://gitlab.com/JOGL/JOGL/-/issues?label_name[]=frontend) or [all issues](https://gitlab.com/JOGL/JOGL/-/issues).
- **Board view** - View a Trello-like view of the issues listed by priority and state (to do, doing, ready/review): [frontend-only issues](https://gitlab.com/JOGL/JOGL/-/boards/1990876?&label_name[]=frontend) or [all issues](https://gitlab.com/JOGL/JOGL/-/boards/885598).

You can also contact us at support[at]jogl.io for any question.

### The philosophy:

1. API first, head to our [API documentation](https://documenter.getpostman.com/view/8688524/SWE84xMg?version=latest) for a current view of the API.
2. If what you need is missing, then you'll have to add it to our backend: https://gitlab.com/JOGL/backend-v0.1
3. If you want to add code, fork from the branch `develop` and merge request to the branch `develop`! Any request to `master` will be automatically rejected.
4. DRY!
5. Object Oriented thinking, if you think you should make a new component for this, then you should indeed! Modular code is the way forward!

For more detailed information on how to contribute, please read our [Contributing guidelines](https://gitlab.com/JOGL/JOGL/-/blob/master/CONTRIBUTING.md).

### How to report a bug or request a feature.

Please have a quick check through our [open issues](https://gitlab.com/JOGL/JOGL/-/issues) to see if your issue/problem/request has already been reported.

If so, you can click on :thumbsup: to indicate that you have the same issue, and you can add a comment to the issue to give more detail. If not, go ahead and **create your new issue** using [this link](https://gitlab.com/JOGL/JOGL/-/issues/new).

---

# Getting started

We highly recommend you setup a SSH key with your GitLab account (https://docs.gitlab.com/ee/ssh/), but this is up to you.

Clone the repository with SSH `git clone git@gitlab.com:JOGL/frontend-v0.1.git` or HTTPS `git clone https://gitlab.com/JOGL/frontend-v0.1.git`

Navigate to the repository

`cd frontend-v0.1/app`

Make sure you are onto the develop branch

`git checkout develop`

Create a new branch for your feature or bug correction

`git checkout -b MyNewBranch`

---

# Understanding our front-end architecture

After cloning our repository and consulting our existing work, you might feel the need to receive an introduction on our code architecture, which relies mainly on [NextJS](https://nextjs.org/docs/getting-started). <br>
To that effect, please consult our [front-end architecture walk through on Wiki](https://gitlab.com/JOGL/frontend-v0.1/-/wikis/home)!

---

# Running the stack

To get an introduction on our complete front-end stack, please consult our [technologies documentation on Wiki](https://gitlab.com/JOGL/frontend-v0.1/-/wikis/Technologies).

You will need npm, follow the instalation guide for your computer (https://www.npmjs.com/get-npm)

At the root first `npm install` to install all required libraries followed by `npm run dev` to start the app locally.

Before launching the app, make sure to duplicate the `.env.exemple` file (containing the necessary variables to run the stack), to create a `.env.local` file. The app will use the variables in this file on development and build mode.

Note: If you work on **both** the backend and the frontend, we recommend you setup your own Algolia account to be able to run your tests productively. See the section about [deployment](https://nextjs.org/docs/deployment#nodejs-server) for more information.

## Launching the app

### `npm run dev`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload as you make edits.<br>

### `npm run build` then `npm run start`

Builds the app for production.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.
Then you can host it in a Node.JS server and start it with `npm run start`.

## Other available scripts

In the project directory, you can also run:

### `npm run test`

We are using Jest to test the app. Jest is configured with projects, meaning it will run multiple tests by default:

- Normal jest tests.
- ESLint tests

To only run normal tests, press `P` and de-select `eslint` by pressing the `space` key.

### `npm run analyze`

Visualize size of webpack output files with an interactive zoomable treemap. Will output the client and server results.

---

## Some technical aspect:

1. If you need an API to test things you can point to: https://jogl-backend-dev.herokuapp.com
2. If you prefer to work locally, test our dockerstack: https://gitlab.com/JOGL/JOGL

---

## Using translations within the app:

When launching the app, you will be able to browse it with your default browser language. You can change it with the select dropdown languages and you will see that the app is available in **English**, **French**, **Spanish** and **German**.

### Adding translations:

If you notice a missing translation or if you would like to improve some:

1. Feel free to make it on **each** JSON located under `locales`>`language code name`>`common.json`.
2. Carefully take a look to write the translated words and sentences with the same key on the four JSON and at the same position within the JSON object.
3. You can now use your translated words within the page or component of your choice. Take a look a [next-translate](https://github.com/vinissimus/next-translate) project in order to make sure you use it as it is asking to.

---

## Data fetching withing the app

We use a technology called [SWR](https://swr.vercel.app/) (`stale-while-revalidate`) and developed by the Nextjs team through the application. With SWR, components get a stream of data updates constantly and automatically. And the UI is always fast and reactive.

To use correctly and widely this technology over the front-end, we created our own custom hooks ([see here the front-end architecture](https://gitlab.com/JOGL/frontend-v0.1/-/wikis/home)), so we have all the options, props and types correctly integrated.

---

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
