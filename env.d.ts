declare namespace NodeJS {
  export interface ProcessEnv {
    NEXT_PUBLIC_RECAPTCHA_KEY: string;
  }
}
