importScripts('https://www.gstatic.com/firebasejs/9.6.4/firebase-app-compat.js');
importScripts('https://www.gstatic.com/firebasejs/9.6.4/firebase-messaging-compat.js');

const firebaseConfig = {
    apiKey: 'AIzaSyD0AXh20T7S8lo8Ut3o0FmSbBeP_sDGQe0',
    authDomain: 'jogl-389521.firebaseapp.com',
    projectId: 'jogl-389521',
    storageBucket: 'jogl-389521.appspot.com',
    messagingSenderId: '703224592857',
    appId: '1:703224592857:web:707e7fa1473123801081c8',
    measurementId: 'G-G77VY04D3P',
};

firebase.initializeApp(firebaseConfig);
// We can't rely on firebase.messaging.isSupported() check because it's broken inside Safari service workers, so we just try running it, and catch errors.
try {
    var messaging = firebase.messaging();
    /* eslint-disable-next-line */
    self.addEventListener('push', async (event) => {
        var payload = event.data.json();
        const clients = await self.clients.matchAll({
          type: 'window',
        });
        if (clients.some((client) => client.focused)) {
          clients.forEach((client) => {
            client.postMessage({
              type: 'PUSH_RECEIVED',
              notification: payload.notification,
              data: payload.data,
            });
          });
        }
        else {
          var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
          if (isSafari) {
            var notificationOptions = {
                body: payload.notification.body,
                icon: payload.notification.icon,
                data: {
                    CTA_URL: payload.data ? payload.data.CTA_URL : undefined
                },
                actions: [{
                    action: 'view',
                    title: 'View',
                }, ],
            };

            event.waitUntil(self.registration.showNotification(payload.notification.title, notificationOptions));
          }
        }
    });
    // Safari doesn't allow silent notifications (either foreground or background), after 3 silent notifications permissions are revoked
} catch (error) {
    console.error(error);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////// Fetch interceptor for downloads //////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// TODO: mixing file download logic with the firebase-messaging logic isn't the cleanest but is need because:
// - we can only have 1 service worker per scope (and they are both global scoped)
// - firebase expects a file named like this one
// The correct solution is likely having an importScript() call and on that script have the code below

// Taken from https://github.com/sampotts/plyr/issues/1312
// This service worker is a workaround to play media (videos + audio) from endpoints that require authorization.
// The service worker is registered whenever a user logs in (check UserProvider.tsx) and it adds a listener that
// intercepts requests made to `/documents/{documentId}/download` and adds authorization headers to them.

var authorization = ''; // Receive from Component

self.addEventListener('install', () => {
    self.skipWaiting();
    const params = new URL(location);
    authorization = params.searchParams.get('authorization');
});

self.addEventListener('fetch', (event) => {
    if (event.request.url.endsWith('/download')) {
        event.respondWith(customHeaderRequestFetch(event));
    }
});

function customHeaderRequestFetch(event) {
    const headers = {
        Authorization: authorization ? authorization : undefined
    };
    if (event.request.headers.get('range') != undefined) {
        headers.Range = event.request.headers.get('range');
    }

    const newRequest = new Request(event.request, {
        mode: 'cors',
        credentials: 'omit',
        headers,
    });
    return fetch(newRequest);
}