import React from 'react';
import Accordion from '../src/components/Accordion';
export default { title: 'Accordion' };

export const index = () => (
  <Accordion
    values={[
      { title: 'Title 1', content: 'Content 1' },
      { title: 'Title 2', content: 'Content 2' },
      { title: 'Title 3', content: 'Content 3' },
      { title: 'Title 4', content: 'Content 4' },
    ]}
  />
);
