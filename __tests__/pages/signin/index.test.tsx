// ? Could add a test to see if when logged in it redirects
// ? Could add for redirects when successfully logged in.
import user from '@testing-library/user-event';
import { render } from '~/test/testUtils';
import Signin from '~/pages/signin';
// This page isn't about testing the layout so we mock it.
jest.mock('~/src/components/Layout', () => {
  const FakeLayout = jest.fn(({ children }) => children);
  return FakeLayout;
});

// When the Sign In button will be pressed it will call the signIn function coming
// from the useUser React custom hook, we don't want to make an api call, so we are
// going to mock the signIn promise but check if the arguments passed are correct.
const mockSignIn = jest.fn(() => Promise.resolve());

jest.mock('~/src/hooks/useUser', () => () => ({
  // mockSignIn is declared outside so we can use it in an `expect` after.
  signIn: mockSignIn,
}));

// Mocking the entire 'next/router' module
jest.mock('next/router', () => ({
  // ! Could not verify if this method was called with the correct params.
  push: jest.fn(),
  // Creates a mocked method that return the query params from the URL
  useRouter: jest.fn(() => ({ query: { callbackPath: 'test', callbackAsPath: 'test' } })),
}));

afterEach(() => {
  jest.clearAllMocks();
});

describe('Sign-in page', () => {
  // Renders the page and creates a snapshot of the rendered dom
  // Every time the rendered dom changes, check if the diffs are correct
  // Then update the snapshot
  it('matches snapshot', () => {
    const { asFragment } = render(<Signin />, {});
    expect(asFragment()).toMatchSnapshot();
  });

  it('signs in ', async () => {
    // Verify that it renders
    const { findByLabelText, findByText } = render(<Signin />, {});
    const fakeUser = {
      email: 'email@email.com',
      password: 'password',
    };
    // Simulated an user that enters his email and password.
    user.type(await findByLabelText(/email/i), fakeUser.email);
    user.type(await findByLabelText(/password/i), fakeUser.password);
    // Submits the form
    user.click(await findByText(/sign in/i, { selector: 'button' }));
    // Now we verify that the mocked signIn function has been called with the correct params.
    expect(mockSignIn).toHaveBeenCalledWith(fakeUser.email, fakeUser.password);
  });
});
