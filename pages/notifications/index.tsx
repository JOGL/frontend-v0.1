import { useEffect, useState } from 'react';
import Layout from '~/src/components/Layout/layout/layout';
import { useApi } from '~/src/contexts/apiContext';
import Loading from '~/src/components/Tools/Loading';
import { TabPanels, TabPanel as SpecialTabPanel, Tabs } from '@reach/tabs';
import { NavTab, TabListStyleNoSticky } from '~/src/components/Tabs/TabsStyles';
import NotificationCard from '~/src/components/Header/NotificationCard';
import tw from 'twin.macro';
import { filterNCleanNotification } from '~/src/components/Header/NotificationUtils';
import useGet from '~/src/hooks/useGet';
import Button from '~/src/components/primitives/Button';
import SpinLoader from '~/src/components/Tools/SpinLoader';
import useInfiniteLoading from '~/src/hooks/useInfiniteLoading';
import useTranslation from 'next-translate/useTranslation';

const NotificationPage = () => {
  const [tabIndex, setTabIndex] = useState(0);
  const { t } = useTranslation('common');
  const itemsPerQuery = 20; // number of users per query calls
  const api = useApi();

  const { data: rawNotifications, size, setSize, isLoading: isLoadingNotifications } = useInfiniteLoading(
    (index) => `/users/notifications?pageSize=${itemsPerQuery}&page=${index + 1}&loadDetails=true`
  );
  const { data: lastRead, isLoading: loadingAction } = useGet<any>('/users/notifications/lastRead');

  const unfilteredNotifs = rawNotifications ? [].concat(...rawNotifications) : [];
  const isLoadingMore =
    isLoadingNotifications || (size > 0 && rawNotifications && typeof rawNotifications[size - 1] === 'undefined');
  const isEmpty = rawNotifications?.[0]?.length === 0;
  const isReachingEnd =
    isEmpty || (rawNotifications && rawNotifications[rawNotifications.length - 1]?.length < itemsPerQuery);

  const notifications = filterNCleanNotification(unfilteredNotifs ?? [], lastRead ?? '', t);
  const isLoading = isLoadingNotifications || loadingAction;

  const readNotification = async () => {
    try {
      await api.post(`/users/notifications/lastRead`);
    } catch (e) {}
  };

  useEffect(() => {
    setTimeout(readNotification, 1000);
  }, []);

  return (
    <Layout title="Notifications | JOGL" noIndex>
      <Loading active={isLoading}>
        <div tw="flex flex-col">
          <Tabs onChange={(index) => setTabIndex(index)}>
            <TabListStyleNoSticky tw="border-none">
              <NavTab>
                <div tw="inline-flex items-center justify-center" css={tabIndex === 0 && tw`(font-bold text-black)!`}>
                  {t('personal')}
                  {notifications.personalCount > 0 && (
                    <span tw="bg-[#E2E2E2] rounded-full w-7 h-7 ml-2">
                      <span tw="flex justify-center">{notifications.personalCount}</span>
                    </span>
                  )}
                </div>
              </NavTab>
              <NavTab>
                <div tw="inline-flex items-center justify-center" css={tabIndex === 1 && tw`(font-bold text-black)!`}>
                  {t('legacy.role.admin')}
                  {notifications.adminCount > 0 && (
                    <span tw="bg-[#E2E2E2] rounded-full w-7 h-7 ml-2">
                      <span tw="flex justify-center">{notifications.adminCount}</span>
                    </span>
                  )}
                </div>
              </NavTab>
            </TabListStyleNoSticky>
            <TabPanels tw="justify-center">
              {/* Personal Notification */}
              <SpecialTabPanel>
                <div tw="flex justify-center flex-col px-[5vw] md:px-[15vw]">
                  {notifications?.personal?.notRead?.map((item, i) => (
                    <NotificationCard notification={item} isRead={false} key={i + 'personal-unread'} />
                  ))}
                  <div tw="flex flex-row items-center py-5" />
                  {notifications?.personal?.read?.map((item, i) => (
                    <NotificationCard notification={item} isRead={true} key={i + 'personal-read'} />
                  ))}
                </div>
              </SpecialTabPanel>
              {/* Admin Notification */}
              <SpecialTabPanel>
                <div tw="flex justify-center flex-col px-[5vw] md:px-[15vw]">
                  {notifications?.admin?.notRead?.map((item, i) => (
                    <NotificationCard notification={item} isRead={false} key={i + 'admin-unread'} />
                  ))}
                  <div tw="flex flex-row items-center py-5" />
                  {notifications?.admin?.read?.map((item, i) => (
                    <NotificationCard notification={item} isRead={true} key={i + 'admin-read'} />
                  ))}
                </div>
              </SpecialTabPanel>
            </TabPanels>
          </Tabs>
        </div>
        {!isReachingEnd && (
          <Button
            btnType="button"
            tw="flex m-auto justify-center items-center mt-6"
            onClick={() => setSize(size + 1)}
            disabled={isLoadingMore || isReachingEnd}
          >
            {isLoadingMore && <SpinLoader />}
            {isLoadingMore ? t('loadingWithEllipsis') : !isReachingEnd ? t('action.load') : t('noMoreResults')}
          </Button>
        )}
      </Loading>
    </Layout>
  );
};

export default NotificationPage;
