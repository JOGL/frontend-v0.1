import { NextPage } from 'next';
import ChangePwd from '~/src/components/Pages/auth/ChangePwd/ChangePwd';

const ChangePwdPage: NextPage = () => <ChangePwd />;
export default ChangePwdPage;
