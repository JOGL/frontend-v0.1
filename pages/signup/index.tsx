import { GetServerSidePropsContext, NextPage } from 'next';
import nextCookie from 'next-cookies';
import { SignUp } from '~/src/components/Pages/SignInUp/SignUp/SignUp';

const SignUpPage: NextPage = () => {
  return <SignUp />;
};

export const getServerSideProps = async (ctx: GetServerSidePropsContext) => {
  const { userId } = nextCookie(ctx);

  //Logged in users are redirected to their profile
  if (userId) {
    return {
      redirect: { destination: `/user/${userId}`, permanent: false },
    };
  }

  return {};
};

export default SignUpPage;
