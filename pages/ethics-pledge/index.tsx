import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Layout from '~/src/components/Layout/layout/layout';

const EthicsPledge: NextPage = () => {
  const { t } = useTranslation('ethics-pledge');
  const title = `${t('awarenessAndEthicsPledge.title')} | JOGL`;
  return (
    <Layout title={title}>
      <div className="legal" tw="px-4 xl:px-0">
        <h1>{t('awarenessAndEthicsPledge.title')}</h1>
        <h2>{t('awarenessAndEthicsPledge.awareness.title')}</h2>
        <h4>{t('awarenessAndEthicsPledge.awareness.freedomToExperiment.title')}</h4>
        <p>{t('awarenessAndEthicsPledge.awareness.freedomToExperiment.description')}</p>
        <h4>{t('awarenessAndEthicsPledge.awareness.collectiveIntelligence.title')}</h4>
        <p>{t('awarenessAndEthicsPledge.awareness.collectiveIntelligence.description')}</p>
        <h4>{t('awarenessAndEthicsPledge.awareness.freeAccess.title')}</h4>
        <p>{t('awarenessAndEthicsPledge.awareness.freeAccess.description')}</p>
        <h4>{t('awarenessAndEthicsPledge.awareness.inclusiveness.title')}</h4>
        <p>{t('awarenessAndEthicsPledge.awareness.inclusiveness.description')}</p>
        <h4>{t('awarenessAndEthicsPledge.awareness.independentButCommitted.title')}</h4>
        <p>{t('awarenessAndEthicsPledge.awareness.independentButCommitted.description')}</p>
        <h2>{t('awarenessAndEthicsPledge.ethics.title')}</h2>
        <p>{t('awarenessAndEthicsPledge.ethics.subTitle')}</p>
        <h5>{t('awarenessAndEthicsPledge.ethics.respect.title')}</h5>
        <p>{t('awarenessAndEthicsPledge.ethics.respect.description')}</p>
        <h5>{t('awarenessAndEthicsPledge.ethics.safety.title')}</h5>
        <p>{t('awarenessAndEthicsPledge.ethics.safety.description')}</p>
        <h5>{t('awarenessAndEthicsPledge.ethics.modesty.title')}</h5>
        <p>{t('awarenessAndEthicsPledge.ethics.modesty.description')}</p>
        <h5>{t('awarenessAndEthicsPledge.ethics.transparency.title')}</h5>
        <p>{t('awarenessAndEthicsPledge.ethics.transparency.description')}</p>
        <h5>{t('awarenessAndEthicsPledge.ethics.integrity.title')}</h5>
        <p>{t('awarenessAndEthicsPledge.ethics.integrity.description')}</p>
        <h5>{t('awarenessAndEthicsPledge.ethics.pacifism.title')}</h5>
        <p>{t('awarenessAndEthicsPledge.ethics.pacifism.description')}</p>
        <h5>{t('awarenessAndEthicsPledge.ethics.responsibility.title')}</h5>
        <p>{t('awarenessAndEthicsPledge.ethics.responsibility.description')}</p>
        <h5>{t('awarenessAndEthicsPledge.ethics.collaboration.title')}</h5>
        <p>{t('awarenessAndEthicsPledge.ethics.collaboration.description')}</p>
        <h5>{t('awarenessAndEthicsPledge.ethics.education.title')}</h5>
        <p>{t('awarenessAndEthicsPledge.ethics.education.description')}</p>
      </div>
    </Layout>
  );
};

export default EthicsPledge;
