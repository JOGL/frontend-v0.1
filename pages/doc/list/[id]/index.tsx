import { NextPage } from 'next';
import { getApiFromCtx } from '~/src/utils/getApi';
import { Permission } from '~/__generated__/types';
import EntityDocumentList from '~/src/components/documents/entityDocumentList/entityDocumentList';
import Layout from '~/src/components/Layout/layout/layout';

const DocumentListPage: NextPage = ({ id, permissions }: { id: string; permissions: Permission[] }) => {
  return (
    <Layout title="Documents">
      <EntityDocumentList entityId={id} canManageDocuments={permissions.includes(Permission.Managedocuments)} />
    </Layout>
  );
};

export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/entities/${ctx.query.id}/permissions`).catch((err) => console.error(err));
  return { props: { id: ctx.query.id, permissions: res?.data ?? [res], key: ctx.query.id } };
};

export default DocumentListPage;
