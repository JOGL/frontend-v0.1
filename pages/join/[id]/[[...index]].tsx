import { NextPage } from 'next';
import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useAuth } from '~/auth/auth';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import api from '~/src/utils/api/api';
import useDashboard from '~/pages/dashboard/useDashboard';

const InvitePage: NextPage = () => {
  const router = useRouter();
  const [goHome] = useDashboard();
  const { id, key } = router.query;
  const { setSelectedHub, initializeHubs } = useHubDiscussionsStore((store) => ({
    setSelectedHub: store.setSelectedHubDiscussion,
    initializeHubs: store.initialize,
  }));

  const { isLoggedIn } = useAuth();
  
  useEffect(() => {
    const handleInvite = async () => {
      if (!(id && key)) return;
  
      if (isLoggedIn !== true) {
        router.push(`/signin?redirectUrl=/join/${id}?key=${key}`);
        return;
      }
  
      const res = await api.communityEntities.joinKeyCreateDetail(id as string, key as string);
      if (!res || !res.data) {
        router.push('/search');
        return;
      }

      switch (res.data.type) {
        case 'node':
          const nodeResponse = await api.feed.nodesDetailDetail(id as string);
          setSelectedHub(nodeResponse.data);
          break;
      }
  
      await initializeHubs();
      goHome(id as string);
    };
  
    handleInvite();
   // eslint-disable-next-line react-hooks/exhaustive-deps
   }, []);

  return <> </>;
};

export default InvitePage;
