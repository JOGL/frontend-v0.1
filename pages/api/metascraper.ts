import { VercelRequest, VercelResponse } from '@vercel/node';
import createMetascraper from 'metascraper';
import metascraperDescription from 'metascraper-description';
import metascraperImage from 'metascraper-image';
import metascraperTitle from 'metascraper-title';
import metascraperLogo from 'metascraper-logo';
import metascraperLogoFavicon from 'metascraper-logo-favicon';
import metascraperAuthor from 'metascraper-author';
import metascraperDate from 'metascraper-date';
import metascraperPublisher from 'metascraper-publisher';
import metascraperUrl from 'metascraper-url';
import metascraperAmazon from 'metascraper-amazon';
import metascraperClearbit from 'metascraper-clearbit';
import metascraperInstagram from 'metascraper-instagram';
import metascraperManifest from 'metascraper-manifest';
import metascraperSoundcloud from 'metascraper-soundcloud';
import metascraperTelegram from 'metascraper-telegram';
import metascraperUol from 'metascraper-uol';
import metascraperSpotify from 'metascraper-spotify';
import metascraperX from 'metascraper-x';
import createBrowserless from 'browserless';
import getHTML from 'html-get';

const metascraper = createMetascraper([
  metascraperAuthor(),
  metascraperDate(),
  metascraperDescription(),
  metascraperImage(),
  metascraperLogo(),
  metascraperClearbit(),
  metascraperPublisher(),
  metascraperTitle(),
  metascraperLogoFavicon(),
  metascraperUrl(),
  metascraperAmazon(),
  metascraperClearbit(),
  metascraperInstagram(),
  metascraperManifest(),
  metascraperSoundcloud(),
  metascraperTelegram(),
  metascraperUol(),
  metascraperSpotify(),
  metascraperX(),
]);

// Spawn Chromium process once
const browserlessFactory = createBrowserless();

// Kill the process when Node.js exit
process.on('exit', () => {
  console.log('closing resources!');
  browserlessFactory.close();
});

export default async (req: VercelRequest, res: VercelResponse): Promise<void> => {
  try {
    // const { targetUrl } = req.query; // Cannot use this option because nextjs is automatically decoding the url passed in the query param.
    // For example, a request to '/api/metascraper?targetUrl=https://en.wikipedia.org/wiki/Hundred_Years%27_War'
    // has a `req.query.targetUrl` of 'https://en.wikipedia.org/wiki/Hundred_Years'_War', which is invalid because it's already escaped
    const targetUrl = req.url.split('api/metascraper?targetUrl=')[1];

    // create a browser context inside Chromium process
    const browserContext = browserlessFactory.createContext();
    const getBrowserless = () => browserContext;
    const { html, url } = await getHTML(targetUrl, { getBrowserless });

    // close the browser context after it's used
    await getBrowserless((browser) => browser.destroyContext());

    const metadata = await metascraper({ html, url });

    res.json(metadata);
  } catch (error) {
    console.log(error);
    res.status(400).json({ error: 'Failed to scrap metadata.' });
  }
};
