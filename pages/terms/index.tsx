/* eslint-disable react/no-unescaped-entities */
import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import Layout from '~/src/components/Layout/layout/layout';

const Terms: NextPage = () => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const { locale } = router;
  // if website or browser is in french
  if (locale === 'fr') {
    return (
      <Layout title={`${t('termsAndConditions')} | JOGL`}>
        <div className="legal" tw="px-4 xl:px-0">
          <h1>Termes et conditions</h1>

          <h2>Préambule</h2>
          <p>
            Just One Giant Lab (ci-après dénommé "JOGL") est une plateforme collaborative et ouverte de recherche et
            d'innovation dédiée à l'apprentissage collectif et à la résolution des problèmes liés aux 17 objectifs de
            développement durable des Nations Unies.
          </p>
          <p>Les présentes règles sont obligatoires pour s'inscrire (ci-après dénommées "Règles") sur la plateforme.</p>
          <p>
            JOGL est la seule entité responsable de toutes les questions relatives à l'organisation des programmes et
            des défis, ainsi que du développement, de l'hébergement et de la maintenance des technologies supportant la
            plateforme...
          </p>

          <h2>Article 1. Définitions</h2>
          <p>
            <strong>Challenges</strong> : fait référence aux défis hébergés sur la plateforme JOGL.
          </p>
          <p>
            <strong>Programmes</strong> : désigne les programmes hébergés et organisés par JOGL.
          </p>
          <p>
            <strong>Contenu</strong> : désigne l'ensemble des informations fournies par l'Utilisateur sur la Plateforme.
          </p>
          <p>
            <strong>CLUF</strong> : définit les contrats spécifiques relatifs aux logiciels et matériels fournis par les
            Partenaires de JOGL aux Utilisateurs.
          </p>
          <p>
            <strong>Jury</strong> : désigne le comité réuni pour différents programmes, comme le comité d'éthique, des
            sciences et de l'impact. Les règles exactes régissant chaque jury sont définies par le règlement et les
            conditions du programme.
          </p>
          <p>
            <strong>Utilisateur</strong> : désigne toute personne physique ou morale ayant créé un compte sur la
            plateforme JOGL et ayant accepté les CGU.
          </p>
          <p>
            <strong>Leader</strong> : Dans le cadre d'un projet, désigne tout Utilisateur qui a créé ou s'est vu
            attribuer des droits d'administrateur sur ledit projet.
          </p>
          <p>
            <strong>Contributeur</strong> : Dans le cadre d'un projet, désigne tout Utilisateur à qui un Leader du
            projet susmentionné a donné le droit de contribuer aux objectifs du projet susmentionné.
          </p>
          <p>
            <strong>Participant</strong> : désigne tout Utilisateur qui a accepté les Règles d'un Programme ou d'un
            Challenge et dont la demande a été acceptée par ce Programme ou ce Challenge.
          </p>
          <p>
            <strong>Compte utilisateur</strong> : désigne le compte créé par l'Utilisateur.
          </p>
          <p>
            <strong>Équipe</strong> : désigne un groupe formalisé par plusieurs Utilisateurs, avec au moins un Leader.
          </p>
          <p>
            <strong>Plateforme</strong> : désigne le site Internet disponible sur{' '}
            <a href="https://app.jogl.io" target="_blank" rel="noopener noreferrer">
              app.jogl.io
            </a>{' '}
            ainsi que ses services annexes intégrés au site Internet et fournis aux Utilisateurs pendant les différents
            Programmes.
          </p>
          <p>
            <strong>Facilitateur</strong> : désigne une organisation qui fournit des resources à JOGL et à sa
            communauté.
          </p>
          <p>
            <strong>Projet</strong> : désigne l'ensemble du contenu généré par un Utilisateur ou une Equipe
            d'Utilisateurs à travers la fonctionnalité projet de la Plateforme. Les projets peuvent être attachés par
            les utilisateurs à différents Challenges afin de participer dans le cadre des règles et conditions de chaque
            challenge.
          </p>
          <p>
            <strong>Groupe</strong> : désigne l'ensemble du contenu généré par une multitude d'Utilisateurs à travers la
            fonctionnalité de groupe de la Plateforme.
          </p>
          <p>
            <strong>Règles</strong> : se réfère à ce cadre très contractuel.
          </p>

          <h2>Article 2. Accès à la Plateforme</h2>

          <h3>2.1 Modalités d' inscription</h3>
          <p>
            L'inscription et la participation à JOGL, aux projets, groupes, programmes et challenges est ouverte et
            gratuite à toute personne ayant atteint l'âge légal de la majorité dans l'état de l'Utilisateur, ou à toute
            personne en deçà de cet âge légal, avec un consentement explicite et écrit du/de ses représentant(s)
            légal/aux. Chaque inscription concerne une personne privée. Une personne morale est également autorisée à
            participer lorsqu'elle a une personne physique agissant en son nom. L'inscription et la participation sont
            ouvertes à toutes les nationalités sans qu'il soit nécessaire d'avoir un diplôme d'études secondaires.
            <br />
            La participation à la Plateforme est considérée comme une acceptation de l'intégralité des Règles par les
            Utilisateurs sans réserve.
          </p>

          <h3>2.2 Cadre contractuel</h3>
          <p>
            L'Utilisateur doit accepter les Règles et les différents CLUF afin d'accéder/utiliser la Plateforme. Ces
            contrats constituent l'intégralité de l'accord entre l'Utilisateur et JOGL.
            <br />
            En outre, l'Utilisateur doit se conformer aux licences open data fournies par le producteur de données.
          </p>

          <h3>2.3 Création d'un Compte Utilisateur</h3>
          <p>
            L'acceptation du cadre contractuel précité entraîne la création d'un Compte Utilisateur dédié fourni par la
            Plateforme. Ce Compte Utilisateur est une condition obligatoire pour participer à toutes les activités sur
            la Plateforme. La création d’un compte nécessitera l’enregistrement de certaines données à caractère
            personnel (pour plus d'informations, veuillez vous reporter à l'article 10 - Vie privée ci-dessous). Parmi
            ces informations, un mot de passe d'une complexité minimale sera nécessaire pour éviter toute utilisation
            abusive du Compte Utilisateur par un tiers.
          </p>
          <p>
            Ces informations seront stockées par JOGL et seront disponibles pour l'Utilisateur sur la page du profil. En
            tant que souscripteur aux Règles, l'Utilisateur s'engage à :
          </p>
          <ul>
            <li>fournir des informations vraies, exactes et complètes</li>
            <li>à maintenir l'exhaustivité de l'information et à la mettre à jour</li>
          </ul>
          <p>
            Le Compte Utilisateur est strictement personnel. Il ne peut être utilisé qu'individuellement et ne peut être
            partagé ou cédé à un tiers. L'Utilisateur est tenu de garder secret son mot de passe. Par conséquent, toute
            action contraire à l'article 5 ci-dessous commise sous le compte de l'Utilisateur sera réputée avoir été
            commise par l'Utilisateur.
          </p>

          <h2>Article 3. Création et gestion des équipes</h2>
          <p>
            Les utilisateurs sont fortement encouragés à se regrouper en équipes afin de participer à la plateforme.
          </p>
          <p>
            Tout utilisateur peut choisir de participer seul ou en équipe. L'équipe existe lorsque plusieurs
            utilisateurs sont réunis dans un projet soumis par un utilisateur. Au sein d'un Groupe, les Utilisateurs
            ayant le droit de propriété ont la possibilité de déterminer si un Utilisateur est bien accueilli dans
            l'Équipe qu'il/elle a créée.
          </p>

          <h2>Article 4. Programmes et challenges</h2>
          <h3>4.1 Définition des programmes</h3>
          <p>
            Un programme peut être créé et coordonné par JOGL autour d'une thématique d'action spécifique, avec la
            participation potentielle des Facilitateurs. Les facilitateurs fournissent les resources nécessaires à la
            réalisation dudit programme. Un programme contient à son tour un ensemble d'un ou plusieurs défis qui
            définissent la portée de la participation. Les Facilitateurs acceptent de collaborer pleinement avec
            l'équipe JOGL pour l'organisation dudit programme.
          </p>
          <h3>4.2 Définition des challenges</h3>
          <p>
            Un Challenge est créé par tout Utilisateur sur la plateforme, dans la mesure où il a expressément demandé le
            droit de créer un Challenge par email à <a href="mailto:hello@jogl.io">hello[at]jogl.io</a>. JOGL se réserve
            le droit de refuser la création d'un Challenge. Les défis sont régis par un ensemble spécifique de règles de
            participation. Les utilisateurs peuvent soumettre leurs projets à n'importe quel Challenge, mais l'équipe
            organisatrice du Challenge se réserve le droit d'accepter ou de rejeter tout projet.
          </p>
          <h3>4.3 Règles régissant la participation aux programmes et aux challenges</h3>
          <p>
            Chaque Programme ou Challenge inscrit sur la plateforme contiendra un ensemble spécifique de règles et
            conditions de participation qui peuvent être trouvées sur leurs pages respectives. Les participants et les
            équipes doivent se référer à ces règles et s'y conformer pour que leur participation soit prise en compte,
            sinon ils risquent d'être retirés du programme. Les règles peuvent inclure, sans s'y limiter, une date de
            soumission, une date de fin, des termes et conditions, des règles éthiques et des règles de bonne conduite.
          </p>

          <h2>Article 5. Modération et interdiction</h2>
          <h3>5.1 Règles d’interdiction</h3>
          <p>
            L'Utilisateur est entièrement responsable de tout Contenu (i) qu'il/elle fournit à un tiers, (ii) qu'il/elle
            envoie par le biais de messages privés fournis par la Plateforme, ou (iii) qu'il stocke dans ses espaces
            personnels sur la Plateforme ou (iv) les espaces disponibles à un tiers, (v) qu'il télécharge depuis la
            Plateforme, ou (vi) qu'il envoie par tout moyen par l'interface fournie par la Plateforme.
          </p>
          <p>
            L'Utilisateur est également conscient que tout Contenu mis en ligne par un autre Utilisateur est publié sous
            sa seule responsabilité. JOGL ne peut être tenus responsable d'aucun contrôle préalable sur ces contenus, ni
            d'aucune obligation générale de surveiller le Contenu stocké par l'Utilisateur.
          </p>
          <p>
            <br />
            Toutefois, JOGL se réserve le droit de supprimer tout Contenu en conflit avec les Règles sans préavis
            formel. L'Utilisateur accepte expressément qu'il lui soit explicitement interdit d'utiliser la Plateforme
            pour participer, directement ou indirectement, de quelque manière que ce soit, à :
          </p>
          <ul>
            <li>
              la provocation, la promotion ou l'encouragement à commettre des crimes ou des délits, et en particulier
              des crimes contre l'humanité,
            </li>
            <li>la promotion ou l'encouragement de la haine raciale,</li>
            <li>activité ou Contenu à caractère raciste, xénophobe ou négationniste,</li>
            <li>
              une activité ou un Contenu à caractère pédophile, ou susceptible de constituer ou d'être associé,
              directement ou indirectement, à la pornographie enfantine, ou à la banalisation de tels actes,
            </li>
            <li>
              la promotion ou l'encouragement de la violence, du suicide, de l'utilisation, de la production ou de la
              distribution de substances illégales ou d'actes de terrorisme,
            </li>
            <li>
              insultes, calomnie, violation ou atteinte au droit à la vie privée, à l'image, à l'honneur et/ou à la
              réputation, violation des droits sur la personne d'un tiers ou de JOGL,
            </li>
            <li>
              toute attaque ou piratage du système informatique d'un tiers, ou la collecte, le traitement ou la
              transmission illégale de données,
            </li>
            <li>
              toute propagation de contaminants informatiques, ou toute activité de piratage informatique de quelque
              nature que ce soit, qu'il s'agisse de la Plateforme, ou du système, ou de tout autre service, ou système
              lié à Internet, quels que soient la technologie et la méthode utilisées (par exemple, l'utilisation d'un
              programme automatisé).
            </li>
          </ul>
          <p>
            En particulier, conformément à la législation locale applicable, l'Utilisateur déclare et garantit, mais
            sans s'y limiter, que l'utilisation de la Plateforme, de ses différentes pages et de tout le Contenu qu'il
            met à disposition :
          </p>
          <ul>
            <li>
              respecte l'ensemble des lois et législations locales, notamment françaises, ainsi que les droits des
              tiers, et que l'Utilisateur poursuivra exclusivement un but légal dans son utilisation
            </li>
            <li>
              ne participera, directement ou indirectement, à aucune pratique préjudiciable, déviante, abusive et/ou
              illégale, telle que l'utilisation ou la tentative d'utilisation de données violant les droits de tiers
              et/ou les lois ou législations, et/ou les activités telles que le phishing, le spamming, le hacking ou
              toute tentative de piratage informatique, ou toute infraction visée aux articles 323-1 à 323-7 du Code
              pénal français.
            </li>
          </ul>
          <h3>5.2. Code de conduite</h3>
          <p>
            Le présent code de conduite est aussi appliqué dans le cadre des challenges. Tous les Utilisateurs sont
            tenus de le respecter.
          </p>
          <h4>5.2.1. L'engagement de JOGL</h4>
          <p>
            Dans le but de favoriser un environnement ouvert et accueillant, JOGL s'engage à faire de la participation
            aux défis une expérience sans harcèlement pour tous les utilisateurs, quels que soient leur âge, leur
            taille, leur handicap, leur origine ethnique, leur identité et leur expression sexuelles, leur niveau
            d'expérience, leur nationalité, leur apparence personnelle, leur race, leur religion, leur identité sexuelle
            et leur orientation.
          </p>
          <h4>5.2.2. Nos normes</h4>
          <p>Voici des exemples de comportements qui contribuent à créer un environnement positif :</p>
          <ul>
            <li>Utiliser un langage accueillant et inclusif</li>
            <li>Respecter les points de vue et les expériences divergentes</li>
            <li>Accepter élégamment les critiques constructives</li>
            <li>Se concentrer sur ce qui est le mieux pour la communauté</li>
            <li>Faire preuve d'empathie envers les autres membres de la communauté</li>
          </ul>
          <p>Exemples de comportements inacceptables de la part de l'Utilisateur :</p>
          <ul>
            <li>
              L'utilisation d'un langage ou d'images sexualisés et l'attention ou les avances sexuelles importunes
            </li>
            <li>Trolling, commentaires insultants/dérogatoires et attaques personnelles ou politiques</li>
            <li>Harcèlement public ou privé</li>
            <li>La publication d'informations personne</li>
          </ul>
          <p>
            Tout autre comportement qui pourrait raisonnablement être considéré comme inapproprié dans un cadre
            professionnel
          </p>
          <h4>5.2.3. Les responsabilités de JOGL</h4>
          <p>
            JOGL est responsable de clarifier les normes de comportement acceptable et doit prendre des mesures
            correctives appropriées et équitables en réponse à tout cas de comportement inacceptable.
          </p>
          <p>
            JOGL a le droit et la responsabilité de supprimer, de modifier ou de rejeter les commentaires, les
            engagements, le code, les modifications wiki, les problèmes et autres contributions qui ne sont pas
            conformes à ce code de conduite, ou de bannir temporairement ou définitivement les collaborateurs pour tout
            comportement que JOGL juge inapproprié, menaçant, choquant ou nocif.
          </p>
          <h4>5.2.4. Exécution</h4>
          <p>
            Des cas de comportement abusif, de harcèlement ou d'autres comportements inacceptables peuvent être signalés
            en contactant JOGL à <a href="mailto:report@jogl.io">report[at]jogl.io</a>. Toutes les plaintes feront
            l'objet d'un examen et d'une enquête et donneront lieu à une réponse jugée nécessaire et appropriée selon
            les circonstances. JOGL a l'obligation de respecter la confidentialité à l'égard du déclarant d'un incident.
            De plus amples détails sur les politiques d'application spécifiques peuvent être affichés séparément.
          </p>
          <p>
            Les utilisateurs qui ne respectent pas ou n'appliquent pas le code de conduite en toute bonne foi peuvent
            s'exposer à des répercussions temporaires ou permanentes, telles que déterminées par JOGL.
          </p>
          <p>
            Ce code de conduite est adapté de la version 1.4 du Pacte du contributeur, disponible sur{' '}
            <a href="https://www.contributor-covenant.org" target="_blank" rel="noopener noreferrer">
              contributor-covenant.org
            </a>
            .
          </p>
          <h3>5.3. Signalement des infractions</h3>
          <p>
            En cas de Contenu stocké sur la Plateforme présentant un trouble manifestement illicite, il pourra être
            signalé à JOGL par l'envoi d'un email à <a href="mailto:report@jogl.io">report[at]jogl.io</a>. La
            notification permettra à JOGL d'identifier le Contenu allégué illicite et son chemin d'accès audit Contenu.
            La notification doit contenir les informations suivantes :
          </p>
          <ul>
            <li>nom, prénom et domicile du plaignant ;</li>
            <li>une copie du Contenu et l'adresse URL de la page du site Web ;</li>
            <li>la raison pour laquelle le Contenu allégué illicite doit être retiré</li>
          </ul>
          <p>
            Toute notification injustifiée d'un Contenu licite sera sanctionnée par des sanctions judiciaires ou
            déclenchera la stipulation prévue au point 6.3. ci-dessous.
          </p>
          <h3>5.4. Sanctions</h3>
          <p>
            Lorsqu'un ou plusieurs des actes susmentionnés sont perpétrés, JOGL demandera à l'Utilisateur contrevenant
            de mettre fin à ses actes lorsque ces actes ne sont pas pertinents. Toutefois, en cas de récidive ou de
            violation de la loi, et nonobstant l'expulsion de l'Utilisateur de la Plateforme, JOGL communiquera
            l'infraction à la police. En considération de l'article 5, JOGL sous sa propre et seule appréciation peut
            exclure l'Utilisateur transgresseur.
          </p>

          <h2>Article 6. Responsabilités</h2>
          <h3>6.1.</h3>
          <p>
            JOGL ne saurait être tenu responsable de tout dysfonctionnement du réseau ou des serveurs de l'hébergeur de
            son site web, ou de tout événement qui pourrait échapper à son contrôle raisonnable, qui pourrait empêcher
            l'Utilisateur d'accéder à la Plateforme.
          </p>
          <h3>6.2.</h3>
          <p>
            JOGL se réserve le droit d'interrompre, de suspendre momentanément ou de modifier tout ou partie de l'accès
            à la Plateforme, afin de maintenir la Plateforme, ou pour toute autre raison, sans aucune forme de
            responsabilité ni aucune compensation.
          </p>
          <h3>6.3.</h3>
          <p>
            JOGL mettra tout en œuvre pour assurer un accès de qualité à la Plateforme. JOGL ne saurait être tenu pour
            responsable de la continuité de la Plateforme.
          </p>
          <h3>6.4.</h3>
          <p>
            JOGL garantit détenir tous les droits de propriété intellectuelle sur le contenu fourni par JOGL aux
            Utilisateurs. Sur cette base, JOGL garantit que tous les services mis à la disposition de l'Utilisateur ne
            sauraient constituer une contrefaçon d'une œuvre antérieure, quelle qu'en soit la nature.
          </p>
          <h3>6.5.</h3>
          <p>
            Dans ces conditions, JOGL s'engage à protéger l'Utilisateur contre toute action en contrefaçon de propriété
            intellectuelle qui pourrait être activée par tout acteur potentiel sur la base de toute fonctionnalité
            fournie par la Plateforme ou sur la base de logiciels tiers hébergés et disponibles pour l'Utilisateur dans
            les conditions décrites par les Règles.
          </p>
          <h3>6.6.</h3>
          <p>JOGL est tenue à une obligation de moyens à l'égard de l'engagement pris dans les Règles.</p>
          <h3>6.7.</h3>
          <p>
            L'Utilisateur reconnaît et accepte expressément que JOGL ne peut être tenu responsable des interruptions de
            service ni des dommages consécutifs liés à :
          </p>
          <ul>
            <li>un cas de force majeure ou une décision administrative/judiciaire ;</li>
            <li>à une interruption de l'électricité, quelle qu'elle soit fournie par un opérateur public ou privé ;</li>
            <li>
              à une utilisation anormale ou frauduleuse de la Plateforme par l'Utilisateur ou par toute tierce partie
              nécessaire pour arrêter le service pour des raisons de sécurité ;
            </li>
            <li>
              à un dysfonctionnement du matériel ou des logiciels hébergés ou à un accès à Internet par le Participant
              ou en raison d'une mauvaise utilisation du Logiciel par l'Utilisateur ;
            </li>
            <li>
              à toute attaque ou piratage par un tiers hébergé dans le système ou à l'extraction illicite de données,
              malgré la mise en place de protocoles de sécurité à la pointe de la technologie, JOGL n'est tenu que d'une
              obligation de moyens ;
            </li>
            <li>
              de retarder le transfert d'informations et de données, lorsque JOGL n'est pas à l'origine dudit retard ;
            </li>
            <li>au fonctionnement de tout moyen de communication qui est maintenu par JOGL ;</li>
            <li>
              de chercher à désanonymiser les données personnelles qui ont été rendues anonymes, et d'informer
              immédiatement JOGL en cas de personnalisation fortuite des données personnelles
            </li>
          </ul>
          <h3>6.8.</h3>
          <p>
            JOGL ne pourra être tenu pour responsable de tout dommage accessoire ou de tout dommage directement et
            exclusivement lié à une défaillance de la Plateforme.
          </p>
          <h3>6.9.</h3>
          <p>
            JOGL se réserve le droit de procéder à des interruptions de service afin de maintenir la Plateforme. JOGL
            s'engage, dans la mesure de ses moyens, à ne procéder à ces interruptions que sur de courtes périodes.
          </p>

          <h2>Article 7. Force majeure</h2>
          <p>
            En cas de force majeure telle que définie par la jurisprudence de la Cour de Cassation, la responsabilité de
            JOGL ne saurait être engagée. Ainsi, les modalités des contestations seront modifiées, raccourcies ou
            annulées. JOGL se réserve le droit de communiquer toute date annoncée. En cas de force majeure, JOGL se
            réserve le droit de signaler, raccourcir, prolonger, modifier ou annuler les contestations sans être tenue
            responsable de cette action. Les utilisateurs ne pourront intenter une action en justice pour toute
            réclamation ou indemnisation à ce titre.
          </p>

          <h2>Article 8. Convention de preuve</h2>
          <p>
            JOGL se réserve le droit de modifier les Règles à tout moment par une annonce publique faite par tout moyen
            électronique ou par une annonce publique sur le site de la Plateforme{' '}
            <a href="https://app.jogl.io" target="_blank" rel="noopener noreferrer">
              app.jogl.io
            </a>
            .
          </p>
          <p>
            L'utilisateur reconnaît que, sauf en cas d'erreur manifeste, JOGL pourra utiliser comme preuve tout acte,
            fait ou omission, logiciel, données, fichiers d'enregistrement, opérations et autres éléments (tels que les
            rapports de suivi) de tout type de formulaire ou support informatique, établi, reçu ou conservé directement
            ou indirectement par JOGL, incluant les systèmes informatiques des facilitateurs. JOGL respectera les
            dispositions de l'article 10 (Vie privée) ci-dessous.
          </p>
          <p>
            L'utilisateur s'engage à ne pas contester la recevabilité, la validité ou la force probante des éléments
            susvisés, sur quelque base juridique que ce soit et qui précise que certains documents doivent être écrits
            ou signés par les parties afin de faire foi.
          </p>

          <h2>Article 9. Propriété intellectuelle</h2>
          <h3>9.1. Reproduction du Contenu</h3>
          <p>
            L'Utilisateur accepte expressément que les Contenus créés et partagés sur la Plateforme soient affichés sous
            une licence open source telle que définie par les critères établis par l'Open Source Initiative{' '}
            <a href="http://opensource.org/osr" target="_blank" rel="noopener noreferrer">
              opensource.org/osr
            </a>
            ).
          </p>
          <p>
            L'édition d'un Contenu sur la Plateforme implique que l'Utilisateur consent à JOGL une licence mondiale,
            libre de droits, non exclusive et irrévocable pour toute reproduction et représentation publique du Contenu
            sur la Plateforme, étant entendu que JOGL a le droit d'organizer et de mettre à jour le Contenu par des
            techniques de publication mises en œuvre par la Plateforme. De plus, l'Utilisateur accepte expressément que
            JOGL soit autorisé à reproduire et représenter librement le Contenu dans un contexte de recherche et
            développement. Toutefois, JOGL s'engage à toujours mentionner la qualité d'auteur de l'Utilisateur du
            Contenu lorsqu'il est affiché ou représenté en dehors de JOGL.
          </p>
          <p>
            En raison d'une obligation de modération, le Contenu de l'Utilisateur pourra être modifié ou supprimé par
            JOGL à la demande d'un tiers. Cependant une copie en cache sera conservée jusqu'à la prescription de la loi
            pénale française.
          </p>
          <p>
            En ce qui concerne le Contenu original, c'est-à-dire le Contenu créé uniquement par l'Utilisateur,
            l'Utilisateur s'engage à accepter que le Contenu original fasse l'objet d'une licence Open Source dans les
            conditions précitées.
          </p>
          <h3>9.2. Content created as a Project</h3>
          <p>
            Chaque Utilisateur est partie prenante de la propriété intellectuelle de sa contribution au Projet.
            Toutefois, chaque Utilisateur fournit une garantie à JOGL et à l'Equipe contre toute réclamation fondée sur
            une violation des droits d'auteur d'un tiers.
          </p>
          <p>
            Les utilisateurs seront libres de placer leur contribution sous une licence libre ou open source telle que
            définie par l'Open Source Initiative (disponible{' '}
            <a href="http://opensource.org/osd" target="_blank" rel="noopener noreferrer">
              opensource.org/osd
            </a>
            ) ou par la{' '}
            <a href="https://www.fsf.org" target="_blank" rel="noopener noreferrer">
              Free Software Foundation
            </a>
          </p>
          <h3>9.3. Liens hypertextes vers le contenu</h3>
          <p>
            JOGL autorise, sans accord préalable, la mise en place de liens hypertextes vers sa Plateforme. Les sites
            web qui choisissent de pointer vers{' '}
            <a href="https://app.jogl.io" target="_blank" rel="noopener noreferrer">
              app.jogl.io
            </a>{' '}
            sont seuls responsables du contenu affiché.
          </p>
          <p>
            JOGL accepte d'héberger des liens hypertextes vers des sites web édités ou publiés par des tiers. JOGL
            n'exerce aucun contrôle préalable sur le contenu des liens, les utilisateurs reconnaissent explicitement que
            JOGL ne sera pas responsable de l'approvisionnement de ces resources et les utilisateurs ne peuvent tenir
            JOGL responsable de l'affichage de ce contenu.
          </p>

          <h2>Article 10. Protection de la vie privée</h2>
          <h3>10.1.</h3>
          <p>
            Dans une logique de respect de la vie privée des Utilisateurs, JOGL s'engage à ce que la collecte et le
            traitement des données personnelles, effectués dans le cadre de l'utilisation de la Plateforme se fassent
            conformément à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés,
            dite Loi " Informatique et Libertés ". Le but de cette collecte et de ce traitement de données personnelles
            est de personnaliser l'expérience de l'Utilisateur, d'améliorer le site web et d'améliorer le service de
            l'Utilisateur.
          </p>
          <h3>10.2.</h3>
          <p>
            Conformément à l'article 34 de la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et
            aux libertés, dite Loi " Informatique et Libertés ", JOGL s'engage à fournir aux Utilisateurs un droit
            d'opposition, d'accès et de rectification aux données personnelles qui les concernent. Les utilisateurs
            peuvent exercer ce droit en écrivant à <a href="mailto:hello@jogl.io">hello[at]jogl.io</a> ou à
            l'Association JOGL (CRI) - 10 Rue Charles V, 75004 Paris, France.
          </p>
          <h3>10.3.</h3>
          <p>
            Cette autorisation n'est accordée qu'à des fins non commerciales. Toute fin commerciale fera l'objet d'un
            contrat spécifique conclu entre le ou les Utilisateurs concernés et JOGL.
          </p>
          <h3>10.4.</h3>
          <p>
            Les utilisateurs qui ne souhaitent pas que leur image soit utilisée publiquement par JOGL dans le cadre de
            ses missions telles que la valorisation des actions des Utilisateurs doivent adresser une demande écrite à{' '}
            <a href="mailto:hello@jogl.io">hello[at]jogl.io</a> ou à l'Association JOGL (CRI) - 10 Rue Charles V, 75004
            Paris, France.
          </p>
          <h3>10.5.</h3>
          <p>
            L'utilisateur est informé et accepte que ses données personnelles soient traitées automatiquement. Les
            données personnelles ne sont utilisées que dans le cadre de la Plateforme afin de permettre à l'Utilisateur
            d'accéder aux services fournis par la Plateforme et de l'améliorer. Les données personnelles et les
            métadonnées pourraient également être collectées aux fins décrites ci-dessous :
          </p>
          <ul>
            <li>Toutes les données personnelles recueillies lors de l'inscription ;</li>
            <li>Toutes les actions impliquant un appel API ;</li>
            <li>Toutes les actions impliquant un service auxiliaire fourni par la Plateforme.</li>
          </ul>
          <p>
            JOGL peut également mettre en œuvre une technique d'anonymisation pour toutes les données personnelles
            collectées à des fins d'analyse scientifique et de publication.
          </p>
          <h3>10.6.</h3>
          <p>
            JOGL n'est responsable que du traitement des données personnelles au sein de la Plateforme. Les données
            personnelles collectées par les Partenaires sont gérées par leur propre politique de confidentialité. Les
            données personnelles seront conservées jusqu'à ce que l'Utilisateur les supprime activement à l'aide de
            l'outil fourni sur la Plateforme.
          </p>
          <h3>10.7.</h3>
          <p>
            Tous les serveurs hébergeant des données personnelles sont situés chez un prestataire de services dédié
            situé en France. Les serveurs sont situés dans les locaux européens de Heroku. Ces serveurs enregistrent
            automatiquement les informations en fonction de l'adresse IP de l'Utilisateur liée à la connexion sur la
            Plateforme. Ces informations n'identifient pas l'Utilisateur en tant que personne mais identifient le
            terminal. Ces informations sont utilisées pour déterminer d'où le site Web est vu et quels sont les
            habitudes de navigation de l'utilisateur. Ces informations sont utilisées afin de fournir la meilleure
            couverture et d'améliorer la plateforme.
          </p>
          <h3>10.8.</h3>
          <p>
            Lors de la visite de l'Utilisateur sur la Plateforme ou sur ses services annexes, de petits fichiers texte
            appelés cookies peuvent être installés sur le terminal de l'Utilisateur.
          </p>
          <p>
            Un cookie est un petit fichier installé sur le terminal lors de la visite d'un site. Ces cookies ont pour
            but de collecter des informations relatives à la navigation sur la Plateforme. Les informations stockées sur
            le terminal de l'Utilisateur qui maintient l'opérabilité avec la Plateforme. Ces cookies ne menacent pas la
            vie privée de l'Utilisateur ni la sécurité de son terminal car ils stockent un code unique attribué de
            manière aléatoire. Les cookies peuvent être gérés par le navigateur Internet de l'Utilisateur.
          </p>
          <p>
            Les cookies d'analyse sont utilisés pour améliorer les performances de la Plateforme en collectant des
            informations relatives au nombre de visiteurs ou à l'utilisation du site Web.
          </p>
          <p>La Plateforme utilise les cookies suivants :</p>
          <ul>
            <li>
              Cookies d'analyse. Nous utilisons Google Analytics pour surveiller le trafic sur notre site. Lorsque vous
              visitez notre site, ces programmes installent certains cookies qui enregistrent des informations
              statistiques sur vos visites sur le site. Nous mesurons le nombre de visites, le nombre de pages vues
              ainsi que l'activité des visiteurs sur le site et leur fréquence de retour. Cela nous aide à améliorer
              notre site Web et ses fonctionnalités, par exemple, en nous assurant que les utilisateurs peuvent y
              naviguer intuitivement. Ces cookies ne stockent aucune information personnelle qui pourrait être utilisée
              pour vous identifier.
            </li>
          </ul>

          <div tw="overflow-x-auto shadow sm:rounded-lg">
            <div tw="align-middle inline-block min-w-full">
              <div tw="overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table tw="min-w-full divide-y divide-gray-200 md:max-w-[700px]">
                  <thead tw="bg-gray-50">
                    <tr>
                      <th scope="col" tw="px-6 py-3 font-bold text-gray-700">
                        Cookie
                      </th>
                      <th scope="col" tw="px-6 py-3 font-bold text-gray-700">
                        Description
                      </th>
                      <th scope="col" tw="px-6 py-3 font-bold text-gray-700">
                        Source
                      </th>
                      <th scope="col" tw="px-6 py-3 font-bold text-gray-700">
                        Durée
                      </th>
                    </tr>
                  </thead>
                  <tbody tw="bg-white divide-y divide-gray-200 divide-solid divide-x-0 border-0">
                    <tr>
                      <td tw="px-4 py-3">_gid</td>
                      <td tw="px-4 py-3">Utilisé pour distinguer les utilisateurs.</td>
                      <td tw="px-4 py-3">Google Analytics</td>
                      <td tw="px-4 py-3">24 heures</td>
                    </tr>
                    <tr>
                      <td tw="px-4 py-3">_gat</td>
                      <td tw="px-4 py-3">Utilisé pour réduire le débit de demande.</td>
                      <td tw="px-4 py-3">Google Analytics</td>
                      <td tw="px-4 py-3">1 minute</td>
                    </tr>
                    <tr>
                      <td tw="px-4 py-3">_ga</td>
                      <td tw="px-4 py-3">Utilisé pour distinguer les utilisateurs</td>
                      <td tw="px-4 py-3">Google Analytics</td>
                      <td tw="px-4 py-3">13 mois</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <h3>10.9. Chaque navigateur fournit un tutoriel à l'adresse suivante :</h3>
          <ul>
            <li>
              pour{' '}
              <a
                href="http://windows.microsoft.com/fr-FR/windows-vista/Block-or-allow-cookies"
                target="_blank"
                rel="noopener noreferrer"
              >
                Internet Explorer
              </a>
            </li>
            <li>
              pour{' '}
              <a
                href="http://docs.info.apple.com/article.html?path=Safari/3.0/fr/9277.html"
                target="_blank"
                rel="noopener noreferrer"
              >
                Safari
              </a>
            </li>
            <li>
              pour{' '}
              <a
                href="http://support.google.com/chrome/bin/answer.py?hl=fr&amp;hlrm=en&amp;answer=95647"
                target="_blank"
                rel="noopener noreferrer"
              >
                Chrome
              </a>
            </li>
            <li>
              pour{' '}
              <a
                href="http://support.mozilla.org/fr/kb/Activer%20et%20d%C3%A9sactiver%20les%20cookies"
                target="_blank"
                rel="noopener noreferrer"
              >
                Firefox
              </a>
            </li>
            <li>
              pour{' '}
              <a href="http://help.opera.com/Windows/10.20/fr/cookies.html" target="_blank" rel="noopener noreferrer">
                Opera
              </a>
            </li>
          </ul>

          <h2>Article 11. Litiges</h2>
          <p>
            Dans l'hypothèse d'une violation alléguée ou prouvée des Règles lors des contestations, par l'Utilisateur,
            JOGL peut convoquer une commission ad hoc qui entendra l'Utilisateur. Compte tenu de l'importance de
            l'infraction, cette commission ad hoc jugera entre la suspension temporaire ou permanente de l'Utilisateur.
          </p>
          <p>
            Toute contestation relative à l'exécution des obligations découlant de l'organisation, de l'exécution ou de
            l'interprétation des Règles ou toute contestation relative à la procédure indiquée ci-dessus, sera soumise à
            une procédure de conciliation amiable. Cette procédure sera déclenchée par la réception de la lettre
            d'inscription à l'Association JOGL - Centre de Recherche Interdisciplinaire (CRI) - 10 Rue Charles V, 75004
            Paris, France. En cas d'impossibilité de parvenir à un accord par un arbitre unique, chaque partie désignera
            son propre arbitre, ces deux arbitres en choisiront un troisième.
          </p>
          <p>
            S'il existe toujours un litige, le litige sera soumis à la compétence exclusive du tribunal de Paris dont la
            compétence est expressément attribuée, nonobstant pluralité de défendeurs.
          </p>

          <h2>Mentions légales</h2>
          <p>
            Just One Giant Lab
            <br />
            MVAC, 23 Rue Greneta, 75002, Paris, France
            <br />
            <a href="mailto:hello@jogl.io">hello[at]jogl.io</a>
          </p>
          <p>
            <strong>Hébergement :</strong> GANDI SAS
            <br />
            63-65 boulevard Masséna, 75013, Paris, FRANCE
            <br />
            Siren 423 093 459 RCS PARIS
            <br />
            n. de TVA FR81423093459
            <br />
            Tel +33 (0) 1 70.37.76.61
          </p>
        </div>
      </Layout>
    );
  }

  // else, english
  return (
    <Layout title={`${t('termsAndConditions')} | JOGL`}>
      <div className="legal" tw="px-4 xl:px-0">
        <h1>Terms and conditions</h1>

        <h2>Preamble</h2>
        <p>
          Just One Giant Lab (hereinafter referred jointly to as «JOGL») is a collaborative and open research and
          innovation platform dedicated to collective learning and solving of problems associated with the 17
          sustainable development goals of the United Nations.
        </p>
        <p>The present rules are mandatory to register (hereinafter referred to as «Rules») to the platform.</p>
        <p>
          JOGL is the sole entity liable for all questions related to the organization of Programs and Challenges, and
          the development, hosting, and maintenance of the technologies supporting the Platform..{' '}
        </p>

        <h2>Article 1. Definitions</h2>
        <p>
          <strong>Challenges</strong>: refers to the challenges hosted on the JOGL platform.
        </p>
        <p>
          <strong>Programs</strong>: refers to the programs hosted and organized by JOGL
        </p>
        <p>
          <strong>Content</strong>: refers to all information provided by the User on the Platform.
        </p>
        <p>
          <strong>EULA</strong>: defines the specific contracts related to the software and hardware provided by JOGL’s
          Partners to Users.
        </p>
        <p>
          <strong>Jury</strong>: refers to the committee gathered for different programs, such as the Ethics, Science
          and Impact committee. The exact rules governing each jury are defined by the programs rules and conditions.{' '}
        </p>
        <p>
          <strong>User</strong>: refers to any physical or moral person who created an account on the JOGL platform and
          accepted the CGU.{' '}
        </p>
        <p>
          <strong>Leader</strong>: In the context of a project, refers to any Users who has created or have been given
          admin rights to the aforementioned project.
        </p>
        <p>
          <strong>Contributor</strong>: In the context of a project, refers to any Users who is been given the rights by
          a Leader of the aforementioned project to contribute to the goals of the aforementioned project.
        </p>
        <p>
          <strong>Participant</strong>: refers to any Users who has accepted the Rules of a Program or a Challenge and
          whose application was accepted by such Program or Challenge.
        </p>
        <p>
          <strong>User account</strong>: refers to the account created by the User.
        </p>
        <p>
          <strong>Team</strong>: refers to a group formalized by several Users, with at least one Leader.
        </p>
        <p>
          <strong>Enabler</strong>: refers to an organisation providing resources to JOGL and its community.{' '}
        </p>
        <p>
          <strong>Platform</strong>: refers to the website available on{' '}
          <a href="https://app.jogl.io" target="_blank" rel="noopener noreferrer">
            app.jogl.io
          </a>{' '}
          and also to its ancillary services embedded in the website and provided to the Users during the different
          Programs.
        </p>
        <p>
          <strong>Project</strong>: refers to the entirety of the content generated by a User or a Team of Users through
          the project feature of the Platform. Projects can be attached by users to different Challenges in order to
          participate within the rules and conditions of each challenge.{' '}
        </p>
        <p>
          <strong>Group</strong>: refers to the entirety of the content generated by a multitude of Users through the
          group feature of the Platform.{' '}
        </p>
        <p>
          <strong>Rules</strong>: refers to this very contractual framework.
        </p>

        <h2>Article 2. Access to the platform</h2>

        <h3>2.1 Modalities of registration</h3>
        <p>
          The registration and participation to JOGL, projects, groups, programs and challenges is open and free to any
          person over the legal age of majority in the state of the User, or to any person under the legal age of
          majority in the state of the User with an explicit and written consent of his/her/their legal guardian(s).
          Each registration is for one private person. A moral person is also allowed to participate when it has one
          individual acting on its behalf. Registration and participation is open to every nationality without any
          college graduation requirement.
          <br />
          The participation to the Platform shall be considered as an acceptance of the integrality of the Rules by the
          Users without any reservation.
        </p>

        <h3>2.2 Contractual framework</h3>
        <p>
          The User has to agree to the Rules and to the different EULAs in order to access/use the Platform. Those
          contracts constitute the entire agreement between the User and JOGL.
          <br />
          Furthermore the User has to comply with the Open data licenses provided by the data producer.
        </p>

        <h3>2.3 Creation of a User Account</h3>
        <p>
          The acceptance of the aforementioned contractual framework leads to the creation of a dedicated User Account
          provided by the Platform. This User Account is a mandatory requirement to participate in any activities on the
          Platform. A registration will necessitate some personal data (for further information please refer to Article
          9 – Privacy below). Among those information, a password with a minimal complexity will be required to prevent
          any misuse of the User Account by any third party.
        </p>
        <p>
          Those information will be stored by JOGL and be available to the User on the profile page. As a subscriber to
          the Rules, the User commits:
        </p>
        <ul>
          <li>to provide true, exact and complete information</li>
          <li>to maintain the completeness of the information and to update it.</li>
        </ul>
        <p>
          The User Account is strictly personal. It could only be used individually and could not be shared or be
          assigned to any third party. The User is liable to keep secret her/his password. Therefore, all action
          contrary to article 4 below committed under the User account shall be deemed as done by the User.
        </p>

        <h2>Article 3. Creation and management of the teams</h2>
        <p>Users are strongly encouraged to gather into Teams in order to participate to the platform.</p>
        <p>
          Any User can choose to participate alone or with a Team. The Team exists when several Users are gathered in a
          project submitted by a User. Within a Group, Users with the ownership right have the ability to determine
          whether a User is welcomed in the Team (s)he/they created.{' '}
        </p>

        <h2>Article 4. Programs and challenges</h2>
        <h3>4.1 Definition of programs</h3>
        <p>
          A program is created and coordinated by JOGL around a specific thematic of action, with the potential
          involvement of Enablers. Enablers provide resources to allow for the realization of said program. A program in
          turn contains a set of one or more Challenges which define the scope of the participation. Enablers agree to
          collaborate fully with the JOGL team for the organization of said program.{' '}
        </p>
        <h3>4.2 Definition of challenges </h3>
        <p>
          A Challenge is created by any User on the platform, given that they have expressly requested the right to
          create a Challenge by email to{' '}
          <a href="mailto:hello@jogl.io" target="_blank">
            hello[at]jogl.io
          </a>
          . JOGL reserve the right to refuse the creation of a Challenge. Challenges are governed by a set of rules of
          participation. Users can submit their Projects to any Challenge they choose to, but the organizing Team of the
          Challenge reserve the right to accept or reject any Project.{' '}
        </p>
        <h3>4.3 Rules governing the participation to programs and challenges</h3>
        <p>
          Each Program or Challenge registered on the platform will contain a set of participation rules and conditions
          that can be found on their respective pages. Participants and Teams must refer to and comply with those rules
          for their participation to be considered, or risk being removed from the program. Rules can include but are
          not limited to, a submission date, an end date, terms and conditions, ethics rules, and good behavior rules.{' '}
        </p>

        <h2>Article 5. Moderation and prohibitions</h2>
        <h3>5.1 Rules of prohibitions</h3>
        <p>
          The User is entirely liable of every Content that (i) (s)he/they provides to any third party, (ii) that
          (s)he/they sends through private messages provided by the Platform, or (iii) (s)he/they stores in
          her/his/their personal spaces on the Platform or (iv) the spaces available to third party, (v) that (s)he/they
          downloads from the Platform, or (vi) (s)he/they sends through by any way through the interface provided by the
          Platform.
        </p>
        <p>
          The User is also aware that every Content put online by another User is published under her/his/their sole
          liability. JOGL can not be held liable to any prior control on those contents, neither to any general
          obligation to monitor the Content stocked by the User.
        </p>
        <p>
          <br />
          However, JOGL reserves the right to delete any Content in conflict with the Rules without issuing any formal
          notice. User explicitly agrees that (s)he/they is explicitly forbidden from using the Platform in order to
          participate, directly or indirectly, in any way whatsoever, in:
        </p>
        <ul>
          <li>
            provocation, advocacy or encouragement to commit crimes or offenses, and particularly crimes against
            humanity,
          </li>
          <li>advocacy or encouragement of racial hatred,</li>
          <li>activity or Content of racist, xenophobic, or negationist character,</li>
          <li>
            activity or Content of pedophile character, or that is liable to constitute or be associated with, either
            directly or indirectly, child pornography, or the trivialization of such acts,
          </li>
          <li>
            advocacy or encouragement of violence, suicide, or the use, production or distribution of illegal
            substances, or acts of terrorism,
          </li>
          <li>
            insults, slander, violation or injury to the right of personal privacy, image, honor and/or reputation,
            violation of the personality rights of a third party or of JOGL,
          </li>
          <li>
            any attack or hacking of a third party’s computer system, or the illegal collection, processing, or
            transmission of data,
          </li>
          <li>
            any propagation of computer contaminants, or hacking activity of any sort whatsoever, whether it concerns
            the Platform, or system, or any other service, or system linked to the Internet, regardless of the
            technology and method used (for example, use of an automated program).
          </li>
        </ul>
        <p>
          Specifically, in accordance with the local applicable law, the User declares and guarantees, but not in a
          limiting way, that the use of the Platform, its various pages, and all Content that the User makes available:
        </p>
        <ul>
          <li>
            respects all local laws and legislation, in particular those of France, as well as the rights of third
            parties, and that the User will exclusively pursue legal purposes in its use,
          </li>
          <li>
            will not participate, directly or indirectly, in any prejudicial, deviant, abusive and/or illegal practices,
            such as the use or attempted use of data that violates the rights of third parties and/or laws or
            legislation, and/or activities such as phishing, spamming, hacking or any attempt to hack a computer system,
            or any infringement mentioned in Articles 323-1 to 323-7 of the French Penal Code.
          </li>
        </ul>
        <h3>5.2. Code of conduct</h3>
        <p>
          The present code of conduct is also enforced within the challenges. All the users are obliged to respect it.
        </p>
        <h4>5.2.1. JOGL’s Pledge</h4>
        <p>
          In the interest of fostering an open and welcoming environment, JOGL pledges to make participation in
          challenges a harassment-free experience for all the Users, regardless of age, body size, disability,
          ethnicity, gender identity and expression, level of experience, nationality, personal appearance, race,
          religion, or sexual identity and orientation.
        </p>
        <h4>5.2.2. Our Standards</h4>
        <p>Examples of behavior that contribute to creating a positive environment include:</p>
        <ul>
          <li>Using welcoming and inclusive language</li>
          <li>Being respectful of differing viewpoints and experiences</li>
          <li>Gracefully accepting constructive criticism</li>
          <li>Focusing on what is best for the community</li>
          <li>Showing empathy towards other community members</li>
        </ul>
        <p>Examples of unacceptable behavior by the User include:</p>
        <ul>
          <li>The use of sexualized language or imagery and unwelcome sexual attention or advances</li>
          <li>Trolling, insulting/derogatory comments, and personal or political attacks</li>
          <li>Public or private harassment</li>
          <li>
            Publishing others’ private information, such as a physical or electronic address, without explicit
            permission
          </li>
        </ul>
        <p>Other conduct which could reasonably be considered inappropriate in a professional setting</p>
        <h4>5.2.3. JOGL’s responsibilities</h4>
        <p>
          JOGL is responsible for clarifying the standards of acceptable behavior and is expected to take appropriate
          and fair corrective action in response to any instances of unacceptable behavior.
        </p>
        <p>
          JOGL has the right and the responsibility to remove, edit, or reject comments, commits, code, wiki edits,
          issues, and other contributions that are not aligned to this code of conduct, or to ban temporarily or
          permanently any contributor for other behaviors that JOGL deems inappropriate, threatening, offensive, or
          harmful.
        </p>
        <h4>5.2.4. Enforcement</h4>
        <p>
          Instances of abusive, harassing, or otherwise unacceptable behavior may be reported by contacting JOGL at{' '}
          <a href="mailto:report@jogl.io">report[at]jogl.io</a>. All complaints will be reviewed and investigated and
          will result in a response that is deemed necessary and appropriate to the circumstances. JOGL is obligated to
          maintain confidentiality with regard to the reporter of an incident. Further details of specific enforcement
          policies may be posted separately.
        </p>
        <p>
          Users who do not follow or enforce the code of conduct in good faith may face temporary or permanent
          repercussions as determined by JOGL.
        </p>
        <p>
          This code of conduct is adapted from the Contributor Covenant, version 1.4, available at{' '}
          <a href="https://www.contributor-covenant.org" target="_blank" rel="noopener noreferrer">
            contributor-covenant.org
          </a>
          .
        </p>
        <h3>5.3. Reporting of infringement</h3>
        <p>
          In case of a Content stocked on the Platform presenting a manifestly unlawful disturbance, it could be noticed
          to JOGL by sending an email to <a href="mailto:report@jogl.io">report[at]jogl.io</a>. The notification shall
          allow JOGL to identify the alleged unlawful Content and its path to the said Content. The notification shall
          contain the following information:
        </p>
        <ul>
          <li>name, first name and residence of the plaintive ;</li>
          <li>a copy of the Content and the URL address of the website page ;</li>
          <li>the reason of why the alleged unlawful Content shall be withdrawn.</li>
        </ul>
        <p>
          All wrongful notification of a lawful Content shall be punished by judicial sanctions or shall trigger the
          stipulation stated in 6.3. below.
        </p>
        <h3>5.4. Sanctions</h3>
        <p>
          When one or several aforementioned acts are perpetrated, JOGL will demand to the infringer User to stop
          her/his/their acts when those acts are irrelevant. However, in the case of repetition or in the case of the
          breach of law, and notwithstanding the expulsion of the User from the Platform, JOGL will communicate the
          infringement to the police. In consideration of article 5, JOGL under its own and only appreciation can
          exclude User infringer.
        </p>

        <h2>Article 6. Liabilities</h2>
        <h3>6.1.</h3>
        <p>
          JOGL won’t be held liable of all network malfunction or servers of its website host, or any kind of event
          which could escape its reasonable control, which may prevent the User from accessing to the Platform.
        </p>
        <h3>6.2.</h3>
        <p>
          JOGL keeps the right to interrupt, to suspend momentarily or to modify part or the whole access to the
          Platform, in order to maintain the Platform, or for any other kind of reason, without any kind of liability
          nor any kind of compensation.
        </p>
        <h3>6.3.</h3>
        <p>
          JOGL shall employ any means to ensure a quality access to the Platform. JOGL could not be held liable to any
          continuity of the Platform.
        </p>
        <h3>6.4.</h3>
        <p>
          JOGL warrants to own every intellectual property right of content provided by JOGL to Users. On this basis,
          JOGL warrants that all the services available to the User could not constitute a counterfeit of a previous
          work, whichever nature the work is.
        </p>
        <h3>6.5.</h3>
        <p>
          In those conditions, JOGL warrants to keep safe and secure the User from any intellectual property
          infringement action which may be activated by any potential stakeholder on the basis of any functionality
          provided by the Platform or on the basis of third party softwares hosted and available to the User in the
          conditions described by the Rules
        </p>
        <h3>6.6.</h3>
        <p>JOGL is held to a best efforts obligation to the commitment held in the Rules.</p>
        <h3>6.7.</h3>
        <p>
          The User explicitly acknowledges and agrees that JOGL shall not be held liable to services interruptions nor
          to subsequent damages linked to:
        </p>
        <ul>
          <li>a force majeure or an administrative/judicial decision ;</li>
          <li>to an interruption of electricity whichever is provided by a public or private operator ;</li>
          <li>
            to an abnormal or fraudulent use of the Platform by the User or any third party necessary to stop the
            service for security reasons ;
          </li>
          <li>
            to a dysfunctioning of hardware or software hosted or to an access to Internet by the Participant or due to
            a wrong use of the Software by the User ;
          </li>
          <li>
            to any attack or hacking by a third party hosted into the system or to the illicit extraction of data,
            despite the implementation of security protocols in the state of art, JOGL is only held to a best effort
            obligation ;
          </li>
          <li>to delay in the transfer of information and data, when JOGL is not at the origin of the said delay ;</li>
          <li>to the functioning of any means of communication which are upheld by JOGL ;</li>
          <li>
            to search to deanonymize personal data which were anonymized, and to inform immediately JOGL in the case of
            a fortuit personalization of the personal data.
          </li>
        </ul>
        <h3>6.8.</h3>
        <p>
          JOGL could not be held for any incidental damages or for damages which are directly and exclusively linked to
          a failure of the Platform.
        </p>
        <h3>6.9.</h3>
        <p>
          JOGL keeps the right to proceed to service interruptions in order to uphold and maintain the Platform. JOGL
          commits, in the width of its best efforts, to proceed to those interruptions only in short periods.
        </p>

        <h2>Article 7. Force majeure</h2>
        <p>
          In case of a force majeure as defined by la Cour de Cassation’s case law, JOGL’s liability shall not be
          engaged. Thus the modalities of challenges shall be modified, shortened or cancelled. JOGL reserves the right
          to report any announced date. In the case of the presence of a force majeure, JOGL reserves the right to
          report, shorten, extend, modify or cancel challenges without being held liable for this action. Users could
          not sue for any claim or any compensation for this subject.
        </p>

        <h2>Article 8. Convention of proof</h2>
        <p>
          JOGL reserves the right to modify the Rules at any time by a public announcement done by any electronic means
          or by a public announcement on the platform website{' '}
          <a href="https://app.jogl.io" target="_blank" rel="noopener noreferrer">
            app.jogl.io
          </a>
          .
        </p>
        <p>
          Users acknowledge that, except in an obvious mistake case, JOGL could use as proof any act, fact or omission,
          software, data, registration files, operations and other elements (such as follow-up reports) of any kind of
          form or computer supports, established, received or conserved directly or undirectly by JOGL, including the
          information systems of the enablers. JOGL shall respect the stipulation of article 10 (Privacy) below.
        </p>
        <p>
          Users commit to not contest the admissibility, the validity or the probative force of the elements as
          aforementioned, on whichever legal basis and which specified that certain documents shall be written or signed
          by the parties in order to become a proof.
        </p>

        <h2>Article 9. Intellectual Property</h2>
        <h3>9.1. Reproduction of the Content</h3>
        <p>
          Users explicitly agree that the Contents created and shared on the Platform will be displayed under an open
          source license as defined by the criteria settled by the Open Source Initiative (available on{' '}
          <a href="http://opensource.org/osr" target="_blank" rel="noopener noreferrer">
            opensource.org/osr
          </a>
          ).
        </p>
        <p>
          The editing of a Content on the Platform implies that User agrees to JOGL a worldwide, royalty-free, non
          exclusive, irrevocable licence for any public reproduction and representation of the Content on the Platform,
          understood that JOGL is entitled to organize and upgrade the Content through publication techniques
          implemented by the Platform. Furthermore, the User explicitly agrees that JOGL is authorized to reproduce and
          represent the Content freely in a context of research and development. However, JOGL commits to always mention
          the authorship of the User of the Content when displayed or represent outside of JOGL.
        </p>
        <p>
          Due to a moderation obligation, User’s Content could be modified or deleted by JOGL upon any request of any
          third party. However a cached copy will be kept until the prescription of french criminal law.
        </p>
        <p>
          Considering the original Content, i.e. the Content solely created by the User, the User agrees to accept that
          the original Content shall be under an Open source licence in the conditions aforementioned.
        </p>
        <h3>9.2. Content created as a Project</h3>
        <p>
          Each User is a stakeholder of the intellectual property of her/his/their contribution within the Project.
          However, each User provides a warranty to JOGL and to the Team against any claim based upon a copyright
          infringement from any third party.
        </p>
        <p>
          Users will be free to put their contribution under a free or an open source license as defined by the Open
          Source Initiative (available on{' '}
          <a href="http://opensource.org/osd" target="_blank" rel="noopener noreferrer">
            opensource.org/osd
          </a>
          ) or by the{' '}
          <a href="https://www.fsf.org" target="_blank" rel="noopener noreferrer">
            Free Software Foundation
          </a>
        </p>
        <h3>9.3. Hypertext links to the Contents</h3>
        <p>
          JOGL authorizes, without prior grant, to set up hypertexts links toward its Platform. The websites which
          choose to point toward{' '}
          <a href="https://app.jogl.io" target="_blank" rel="noopener noreferrer">
            app.jogl.io
          </a>{' '}
          are solely liable to the content displayed.
        </p>
        <p>
          JOGL accepts to host hypertext links toward websites edited or published by third parties. JOGL does not
          exercise any prior control on the content linked, Users explicitly acknowledge that JOGL shall not be liable
          for the provisioning of those resources and Users cannot hold JOGL liable for the display of this content.
        </p>

        <h2>Article 10. Privacy</h2>
        <h3>10.1.</h3>
        <p>
          In a logic of privacy of Users, JOGL commits that the data collection and treatment of personal data, done
          through the use of the Platform shall be done in accordance to the law n°78-17 du 6 janvier 1978 relative à
          l’informatique, aux fichiers et aux libertés, dite Loi « Informatique et Libertés ». The purpose of this
          collect and of this process of personal data is in order to personalize User’s experience, to improve the
          website and to improve User’ service.
        </p>
        <h3>10.2.</h3>
        <p>
          Accordingly to the article 34 of the « law n°78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers
          et aux libertés, dite Loi « Informatique et Libertés », JOGL commits to provide Users the right to oppose,
          access and modify the personal data related to them. Users could use the right by writing to{' '}
          <a href="mailto:hello@jogl.io">hello[at]jogl.io</a> or to Association JOGL (CRI) - 10 Rue Charles V, 75004
          Paris, France.
        </p>
        <h3>10.3.</h3>
        <p>
          This authorization is granted only for a non commercial purpose. All commercial purpose shall be the object of
          a specific contract concluded between the concerned User(s) and with JOGL.{' '}
        </p>
        <h3>10.4.</h3>
        <p>
          Users who do not wish their image to be used publicly by JOGL in the context of its missions such as
          valorizing the actions of the users must send a written request to{' '}
          <a href="mailto:hello@jogl.io">hello[at]jogl.io</a> or to Association JOGL (CRI) - 10 Rue Charles V, 75004
          Paris, France.
        </p>
        <h3>10.5.</h3>
        <p>
          Users are informed and agree that their personal data are automatically processed. The personal data are used
          only in the framework of the Platform in order to allow the User access to the services provided by the
          Platform and to upgrade it. Personal data and metadata could also be collected for the purposes developed
          below:
        </p>
        <ul>
          <li>All personal data collected during the registration;</li>
          <li>All actions implying an API call;</li>
          <li>All actions implying an ancillary service provided by the Platform.</li>
        </ul>
        <p>
          JOGL may also implement anonymization technique for all personal data collected in view of scientific analysis
          and publications.
        </p>
        <h3>10.6.</h3>
        <p>
          JOGL is only liable for the process of the personal data within the Platform. Personal data collected by
          Partners are managed by their own privacy policy. Personal data will be kept until the User actively deletes
          them using the tool provided on the Platform.
        </p>
        <h3>10.7.</h3>
        <p>
          All servers hosting personal data are situated in dedicated services provider situated in France. The servers
          are situated within Heroku’s european premises. Those servers are automatically registering information based
          upon the IP address of the User related to the connection on the Platform. Those information doesn’t identify
          the User as a person but are identifying the terminal. Those information are used to determine from where the
          website is seen and what are the browsing patterns of the User. Those informations are used in order to
          provide the best coverage and to enhance the Platform.
        </p>
        <h3>10.8.</h3>
        <p>
          During the visit of the User on the Platform or on its ancillary services, small text files called cookies may
          be installed on the User’s terminal.
        </p>
        <p>
          A cookie is a small file installed on the terminal during the visit of a site. The purpose of such cookies is
          to collect information related to the browsing of the Platform. Those information stocked on the User’s
          terminal which maintains the operability with the Platform. Those cookies are not threatening the privacy of
          the User neither the security of its terminal because their are stocking a single code randomly attributed.
          The cookies can be managed by the internet browser of the User.
        </p>
        <p>
          The analysis cookies are used to enhance the performance of the Platform by collecting information related to
          the number of visitors or the use of the website.
        </p>
        <p>The Platform uses the following cookies:</p>
        <ul>
          <li>
            Analytics cookies. We use Google Analytics to monitor traffic on our site. When you visit our site, these
            programs set certain cookies that record statistical information about your visits to the site. We measure
            the number of visits, the number of pages viewed as well as visitors' activity on the site and their
            frequency of return. This helps us improve our website and its functionality, for example, by ensuring that
            users can navigate it intuitively. These cookies do not store any personal information that could be used to
            identify you.
          </li>
        </ul>

        <div tw="overflow-x-auto shadow sm:rounded-lg">
          <div tw="align-middle inline-block min-w-full">
            <div tw="overflow-hidden border-b border-gray-200 sm:rounded-lg">
              <table tw="min-w-full divide-y divide-gray-200 md:max-w-[700px]">
                <thead tw="bg-gray-50">
                  <tr>
                    <th scope="col" tw="px-6 py-3 font-bold text-gray-700">
                      Cookie
                    </th>
                    <th scope="col" tw="px-6 py-3 font-bold text-gray-700">
                      Description
                    </th>
                    <th scope="col" tw="px-6 py-3 font-bold text-gray-700">
                      Source
                    </th>
                    <th scope="col" tw="px-6 py-3 font-bold text-gray-700">
                      Duration
                    </th>
                  </tr>
                </thead>
                <tbody tw="bg-white divide-y divide-gray-200 divide-solid divide-x-0 border-0">
                  <tr>
                    <td tw="px-4 py-3">
                      <strong>Google analytics</strong>
                    </td>
                    <td />
                    <td />
                    <td />
                  </tr>
                  <tr>
                    <td tw="px-4 py-3">_gid</td>
                    <td tw="px-4 py-3">Used to distinguish users.</td>
                    <td tw="px-4 py-3">Google Analytics</td>
                    <td tw="px-4 py-3">24 hours</td>
                  </tr>
                  <tr>
                    <td tw="px-4 py-3">_gat</td>
                    <td tw="px-4 py-3">Used to throttle request rate.</td>
                    <td tw="px-4 py-3">Google Analytics</td>
                    <td tw="px-4 py-3">1 minute</td>
                  </tr>
                  <tr>
                    <td tw="px-4 py-3">_ga</td>
                    <td tw="px-4 py-3">Used to distinguish users.</td>
                    <td tw="px-4 py-3">Google Analytics</td>
                    <td tw="px-4 py-3">13 months</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <h3>10.9. Each browser provides a tutorial at the following address:</h3>
        <ul>
          <li>
            for{' '}
            <a
              href="http://windows.microsoft.com/fr-FR/windows-vista/Block-or-allow-cookies"
              target="_blank"
              rel="noopener noreferrer"
            >
              Internet Explorer
            </a>
          </li>
          <li>
            for{' '}
            <a
              href="http://docs.info.apple.com/article.html?path=Safari/3.0/fr/9277.html"
              target="_blank"
              rel="noopener noreferrer"
            >
              Safari
            </a>
          </li>
          <li>
            for{' '}
            <a
              href="http://support.google.com/chrome/bin/answer.py?hl=fr&amp;hlrm=en&amp;answer=95647"
              target="_blank"
              rel="noopener noreferrer"
            >
              Chrome
            </a>
          </li>
          <li>
            for{' '}
            <a
              href="http://support.mozilla.org/fr/kb/Activer%20et%20d%C3%A9sactiver%20les%20cookies"
              target="_blank"
              rel="noopener noreferrer"
            >
              Firefox
            </a>
          </li>
          <li>
            For{' '}
            <a href="http://help.opera.com/Windows/10.20/fr/cookies.html" target="_blank" rel="noopener noreferrer">
              Opera
            </a>
          </li>
        </ul>

        <h2>Article 11. Litigation</h2>
        <p>
          In the hypothesis of an alleged or proved infringement of the Rules during the challenges, by the User, JOGL
          may summon an ad hoc committee which will hear the User. In consideration of the importance of the
          infringement, this ad hoc committee will judge between the temporary or permanent suspension of the User.
        </p>
        <p>
          Any contestation related to the execution of the obligations coming from the organization, the execution or
          the interpretation of Rules or any contestation related to the procedure stated above, will be submitted to a
          procedure of amiable composition. This procedure will be triggered by the reception of registration letter in
          Association JOGL – Centre de Recherche Interdisciplinaire (CRI) - 10 Rue Charles V, 75004 Paris, France,
          before one (1) month following the end of the challenges. In case of the impossibility to reach an agreement
          by a sole arbitrator, each Party will appoint its own arbitrator, those two arbitrators will choose a third
          one.
        </p>
        <p>
          If there still is a litigation, the dispute shall be referred to the exclusive jurisdiction to the Paris court
          which power is expressly allocated, notwithstanding multiple defendants.
        </p>
        <h2>Legal Notice</h2>
        <p>
          Just One Giant Lab
          <br />
          MVAC, 23 Rue Greneta, 75002, Paris, France
          <br />
          <a href="mailto:hello@jogl.io">hello[at]jogl.io</a>
        </p>
        <p>
          <strong>Hosting:</strong> GANDI SAS
          <br />
          63-65 boulevard Masséna, 75013, Paris, France
          <br />
          Siren 423 093 459 RCS PARIS
          <br />
          n. de TVA FR81423093459
          <br />
          Tel +33 (0) 1 70.37.76.61
        </p>
      </div>
    </Layout>
  );
};
export default Terms;
