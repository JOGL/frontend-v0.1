import { NextPage, GetServerSidePropsContext } from 'next';
import nextCookie from 'next-cookies';
import SignIn from '~/src/components/Pages/SignInUp/SignIn/SignIn';

const SigninPage: NextPage = () => {
  return <SignIn />;
};

export const getServerSideProps = async (ctx: GetServerSidePropsContext) => {
  const { userId } = nextCookie(ctx);

  //Logged in users are redirected to their profile
  if (userId) {
    return {
      redirect: { destination: `/user/${userId}`, permanent: false },
    };
  }

  return {};
};

export default SigninPage;
