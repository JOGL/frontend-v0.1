import Icon from '~/src/components/primitives/Icon';
import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import A from '~/src/components/primitives/A';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import Layout from '~/src/components/Layout/layout/layout';
import Button from '~/src/components/primitives/Button';
import ProposalAnswerForm from '~/src/components/Proposal/ProposalAnswerForm';
import { useApi } from '~/src/contexts/apiContext';
import { Proposal } from '~/src/types';
import { getApiFromCtx } from '~/src/utils/getApi';
import useGet from '~/src/hooks/useGet';
import nextCookie from 'next-cookies';
import SpinLoader from '~/src/components/Tools/SpinLoader';
import { confAlert } from '~/src/utils/utils';
import { securityOptionsCfp } from '~/src/utils/security_options';
import MessageBox from '~/src/components/Tools/MessageBox';
import H2 from '~/src/components/primitives/H2';
import Card from '~/src/components/Cards/Card';
import FormDefaultComponent from '~/src/components/Tools/Forms/FormDefaultComponent';
import { useModal } from '~/src/contexts/modalContext';
import FormValidator from '~/src/components/Tools/Forms/FormValidator';
import proposalFormRules from '~/src/components/Proposal/proposalFormRules.json';
import isMandatoryField from '~/src/utils/isMandatoryField';
import tw from 'twin.macro';
import Trans from 'next-translate/Trans';

interface Props {
  proposal: Proposal;
}

const ProposalEdit: NextPage<Props> = ({ proposal: proposalProp }) => {
  const { t } = useTranslation('common');
  const validator = new FormValidator(proposalFormRules, t);
  const [proposal, setProposal] = useState(proposalProp);
  const router = useRouter();
  const [sending, setSending] = useState(false);
  const [updatedProposal, setUpdatedProposal] = useState(undefined);
  const [newAnswers, setNewAnswers] = useState(proposal.answers);
  const [hasChanged, setHasChanged] = useState(false);
  const [stateValidation, setStateValidation] = useState<any>({});
  const [hasErrors, setHasErrors] = useState<boolean>(false);
  const { showModal, closeModal } = useModal();
  const urlBack = `/proposal/${router.query.id}`;
  const api = useApi();
  const { data: cfp } = useGet(`/cfps/${proposal.call_for_proposal?.id}`);

  const handleChange = (key, content) => {
    setHasChanged(true);

    setProposal((prevProposal) => ({ ...prevProposal, [key]: content })); // update fields as user changes them
    // TODO: have only one place where we manage need content
    setUpdatedProposal((prevUpdatedProposal) => ({ ...prevUpdatedProposal, [key]: content })); // set an object containing only the fields/inputs that are updated by user

    /* Validators start */
    const validation = validator.validate({ [key]: content });

    if (validation[key] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_${key}`] = validation[key];
      setStateValidation(newStateValidation);
    }
    /* Validators end */
  };

  useEffect(() => {
    if (hasChanged) {
      const validation = validator.validate(proposal);
      if (validation.isValid) {
        setHasErrors(false);
      } else {
        setHasErrors(true);
      }
    }
  }, [proposal]);

  const refetchProposal = async () => {
    return await api
      .get(`/proposals/${proposal.id}`)
      .then((res) => res.data)
      .catch(() => console.warn('Error fetching the proposal before saving.'));
  };

  const validateAnswer = () => {
    const invalidAnswerLength = newAnswers.find(
      (a) =>
        cfp?.template.questions.find((question) => question.key === a.question_key)?.max_length > 0 &&
        a.answer[0]?.replace(/(<([^>]+)>)/gi, '').length >
          cfp?.template.questions.find((question) => question.key === a.question_key)?.max_length
    );
    return {
      validAnswer: !invalidAnswerLength,
      error: invalidAnswerLength ? t('error.exceededMaxNumberOfCharacters') : t('error.one'),
    };
  };

  const saveChanges = async () => {
    const currentProposal = await refetchProposal();

    const { validAnswer, error } = validateAnswer();
    if (currentProposal.updated === proposal.updated) {
      validAnswer && updatedProposal ? handleSubmit() : confAlert.fire({ icon: 'error', title: error });
    } else {
      showModal({
        children: (
          <>
            <p tw="text-base">{t('someoneUpdatedTheProposalWarning')}</p>
            <div tw="inline-flex space-x-3">
              <Button
                btnType="danger"
                onClick={() => {
                  validAnswer && updatedProposal ? handleSubmit() : confAlert.fire({ icon: 'error', title: error });
                  closeModal();
                }}
              >
                {t('action.overwrite')}
              </Button>
              <Button
                onClick={() => {
                  closeModal();
                  setProposal(currentProposal);
                  setNewAnswers(currentProposal.answers);
                  setUpdatedProposal(undefined);
                  setStateValidation({}); // reset validations
                  setHasChanged(false); //reset changed status
                }}
              >
                {t('action.reload')}
              </Button>
            </div>
          </>
        ),
        title: t('overwritingChanges'),
        maxWidth: '30rem',
      });
    }
  };

  const handleSubmit = () => {
    setSending(true);
    api
      .patch(`/proposals/${proposal.id}`, updatedProposal)
      .then(async () => {
        setSending(false);
        setUpdatedProposal(undefined); // reset updated program component
        setStateValidation({}); // reset validations
        setHasChanged(false); //reset changed status

        //Update the proposal state object, otherwise there's no way to keep comparing update dates after the first save
        const currentProposal = await refetchProposal();
        setProposal(currentProposal);
        confAlert.fire({ icon: 'success', title: `${t('successfullyUpdated')!}` });
      })
      .catch(() => setSending(false));
  };

  const { valid_title } = stateValidation || {};

  return (
    <Layout title={`Edit ${proposal?.title || 'proposal'} | JOGL`} noIndex>
      <div tw="sm:shadow-custom2">
        <div tw="flex flex-col gap-2 py-6 px-4 md:px-10 lg:w-3/4 mx-auto">
          <div tw="flex flex-col justify-start pb-8 mb-4">
            <div tw="flex flex-row justify-between items-center mb-2">
              <A href={urlBack}>
                <Icon icon="formkit:arrowleft" tw="w-5 h-5" />
                {t('action.backToTheProposal')}
              </A>
            </div>
            <MessageBox type="info">
              {t('whoCanViewTheProposalsOfThisCallForProposals')}:&nbsp;
              {securityOptionsCfp
                .filter((option) => option.key === 'proposal_privacy')
                .map((option, i) => (
                  <li tw="inline-flex items-center" key={i}>
                    {option.options
                      .filter((option) => option.key === cfp?.proposal_privacy)
                      .map((option, i) => {
                        return (
                          <Trans key={i} i18nKey={`common:${option.info}`} components={[<b key={option.key} />]} />
                        );
                      })}
                  </li>
                ))}
            </MessageBox>
            <FormDefaultComponent
              content={proposal.title}
              id="title"
              errorCodeMessage={valid_title ? valid_title.message : ''}
              isValid={valid_title ? !valid_title.isInvalid : undefined}
              mandatory={isMandatoryField('proposal', 'title')}
              onChange={handleChange}
              placeholder={t('greatProposal')}
              title={t('title')}
              maxChar={120}
            />
            <hr />
            <H2 tw="my-2">{t('application.one')}</H2>
            <MessageBox type="info">{t('answerQuestionsSetByCallForProposals')}</MessageBox>
            <div tw="mb-10 flex flex-col gap-4">
              {proposal?.answers &&
                cfp?.template?.questions?.map((question, i) => (
                  <Card key={i}>
                    <h5 tw="font-bold">{question.title}</h5>
                    {(question.description !== '‎' || question.description !== '‎<p><br></p>') && (
                      <p tw="mb-0" dangerouslySetInnerHTML={{ __html: question.description }} />
                    )}
                    <ProposalAnswerForm
                      key={proposal.updated} //Unlike FormDefaultComponent, this does not re-render on proposal change thus reloading co-editing changes doesn't work without this
                      proposalId={proposal?.id}
                      answer={proposal?.answers?.find((answer) => answer.question_key === question.key)}
                      question={question}
                      onAnswerChange={(key, answer, answer_document_id) => {
                        // find index of answer user is updating
                        const editedAnswerIndex = newAnswers.findIndex((answer) => answer.question_key === key);
                        // then update value if it exists, or push it to array if not
                        if (editedAnswerIndex === -1)
                          newAnswers.push({ question_key: key, answer, answer_document_id });
                        else {
                          newAnswers[editedAnswerIndex].answer = answer;
                          newAnswers[editedAnswerIndex].answer_document_id = answer_document_id;
                        }
                        handleChange('answers', newAnswers);
                      }}
                    />
                  </Card>
                ))}
            </div>
            <div
              tw="mt-10 mb-3 space-x-4 text-center"
              css={
                hasChanged &&
                tw`sticky bottom-0 inline-flex flex-col items-center justify-center py-4 mx-auto border-t backdrop-blur-md bg-white/30 w-[calc(100%+32px)] -ml-4`
              }
            >
              <Button disabled={sending || !hasChanged || hasErrors} onClick={saveChanges}>
                {sending && <SpinLoader />}
                {t('action.save')}
              </Button>
              {hasChanged && <p tw="mb-0">{t('dontForgetToSaveYourChanges')}</p>}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  const { userId } = nextCookie(ctx);
  const res = await api
    .get(`/proposals/${query.id}`)
    .catch((err) => console.error(`Couldn't fetch proposal with id=${query.id}`, err));
  // return data/page if user is admin & submitted_at is not set
  // (meaning proposal has not been sent for review), else redirect to proposal show page
  if (res?.data?.status === 'draft') return { props: { proposal: res.data } };
  return { redirect: { destination: `/proposal/${query.id}`, permanent: false } };
}

export default ProposalEdit;
