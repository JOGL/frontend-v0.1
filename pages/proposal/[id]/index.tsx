import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Layout from '~/src/components/Layout/layout/layout';
import { Proposal, Cfp } from '~/src/types';
import { getApiFromCtx } from '~/src/utils/getApi';
import useGet from '~/src/hooks/useGet';
import Button from '~/src/components/primitives/Button';
import nextCookie from 'next-cookies';
import Icon from '~/src/components/primitives/Icon';
import Link from 'next/link';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import ShareBtns from '~/src/components/Tools/ShareBtns/ShareBtns';
import InfoHtmlComponent from '~/src/components/Tools/Info/InfoHtmlComponent';
import { Menu, MenuItem, MenuButton, MenuList } from '@reach/menu-button';
import ReactTooltip from 'react-tooltip';
import useUser from '~/src/hooks/useUser';
import { useApi } from '~/src/contexts/apiContext';
import Swal from 'sweetalert2';
import { hasCfpDatePassed, renderShortObjectCard } from '~/src/utils/utils';
import ProposalAccordion from '~/src/components/Proposal/ProposalAccordion';
import { displayObjectDate, showColoredStatus, proposalStatus } from '~/src/utils/utils';
import ProposalMembers from '~/src/components/Proposal/ProposalMembers';
import { useModal } from '~/src/contexts/modalContext';
import { useRouter } from 'next/router';
import P from '~/src/components/primitives/P';
import mimetype2fa from '~/src/components/Tools/createDocument/mimetypes';
import DocViewer, { DocViewerRenderers } from '@cyntler/react-doc-viewer';

const ProposalShow: NextPage<{ proposal: Proposal; userId: string }> = ({ proposal, userId }) => {
  const { t } = useTranslation('common');
  const { data: cfp } = useGet<Cfp>(`/cfps/${proposal.call_for_proposal?.id}`);
  const title = proposal.title || `Proposal for "${proposal?.project?.title}"`;
  const [active, setActive] = useState(false);
  const toogleActive = () => setActive(!active);
  const [showSubmitBtn, setShowSubmitBtn] = useState(true);
  const { user } = useUser();
  const discussionRef = useRef();
  const api = useApi();
  const { showModal, closeModal } = useModal();
  const router = useRouter();
  const isAdmin = proposal.users.findIndex((u) => u.id === userId) !== -1;
  const cfpDates = { start: cfp?.submissions_from, deadline: cfp?.submissions_to };
  // filter out answers that are not in the cfp template questions (because they might have been deleted, reordered..)
  const filteredProposals = proposal?.answers?.filter((a) =>
    cfp?.template.questions.map((x) => x.key).includes(a.question_key)
  );
  const hasAnsweredAllQuestions = filteredProposals?.length === cfp?.template?.questions?.length;

  const goToDiscussionSection = () => {
    const navContainerTopPosition = discussionRef.current.getBoundingClientRect().top + window.scrollY;
    const yAdjustment = window.innerWidth < 768 ? 40 : 150; // change yAdustement depending on if it's mobile/tablet or desktop
    window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top (remove a little to be really on top)
  };

  useEffect(() => {
    // check if user scrolled to feed component and mark feed as read if yes
    // improve with https://medium.com/@satyam-dua/lazy-loading-components-in-next-js-with-intersection-observer-and-typescript-9e96f69fa171
    // or https://www.freecodecamp.org/news/how-to-implement-infinite-scroll-in-next-js/
    // BETTER to use document?.querySelector('.feed .post:first-child')
    if (!discussionRef?.current) return;
    const observer = new IntersectionObserver(([entry]) => {
      if (entry.isIntersecting && user) {
        api.post(`/feed/${proposal.id}/seen`);
        observer.unobserve(entry.target);
      }
    });
    observer.observe(discussionRef.current);
  }, []);

  const deleteProposal = () => {
    api
      .delete(`/proposals/${proposal.id}`)
      .then(() => {
        closeModal(); // close modal
        router.push('/');
      })
      .catch((error) => console.warn(error));
  };

  const AnswerContent = useCallback(
    ({ answer, active }) => {
      const { data: document } = useGet(
        answer?.answer_document?.id && `/proposals/${proposal.id}/documents/${answer?.answer_document?.id}`
      );
      return (
        <ProposalAccordion
          title={cfp?.template.questions.find((question) => question.key === answer.question_key)?.title}
          allactive={active}
        >
          <span className="accordion-text" tw="text-[16px]!">
            <InfoHtmlComponent content={answer.answer.join(', ')} />
            {document && (
              <div tw="flex p-4 bg-gray-100 rounded-md border border-gray-200 w-full justify-center mt-4 items-center gap-4">
                <div tw="inline-flex text-[14px]">
                  {mimetype2fa(document.file_type, true)}
                  {document.title}
                </div>
                <Icon
                  icon="ion:eye"
                  tw="w-5 h-5 text-gray-600 hover:text-gray-900 cursor-pointer"
                  onClick={() => {
                    showModal({
                      children: (
                        <DocViewer
                          documents={[{ uri: document.data, fileName: document.title }]}
                          pluginRenderers={DocViewerRenderers}
                        />
                      ),
                      title: 'Document',
                      maxWidth: '30rem',
                    });
                  }}
                />
                <a href={document.data} download={document.title} target="_blank" rel="noopener noreferrer">
                  <Icon
                    icon="material-symbols:download"
                    tw="w-5 h-5 text-gray-600 hover:text-gray-900 cursor-pointer"
                  />
                </a>
              </div>
            )}
          </span>
        </ProposalAccordion>
      );
    },
    [cfp]
  );

  return (
    <Layout noIndex={proposal.status === 'draft'} title={title && `${title} | JOGL`}>
      <div tw="sm:shadow-custom2">
        <div tw="flex flex-col gap-2 py-6 mx-14 px-4 md:px-10 mx-auto">
          <div tw="flex flex-col justify-start pb-8 mb-4">
            <div tw="flex flex-row justify-between items-center mb-2">
              <div tw="flex self-start items-center">
                <span tw="px-4 py-2 background[rgba(209.31, 209.31, 209.31, 0.5)] rounded-lg text-sm">Proposal</span>
              </div>
              <div tw="flex justify-center items-center gap-2">
                {proposal.status !== 'draft' && <ShareBtns type="proposal" specialObjId={proposal?.id} />}
                {proposal.status === 'draft' && isAdmin && !hasCfpDatePassed(cfpDates.deadline) && showSubmitBtn && (
                  <div>
                    <div
                      {...(!hasAnsweredAllQuestions && {
                        'data-tip': t('cantSubmitExplanationError'),
                        'data-for': 'cantSubmitExplanation',
                      })}
                    >
                      <Button
                        onClick={() => {
                          Swal.fire({
                            title: t('areYouSure'),
                            text: t('notAbleToEditAgainWarning'),
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#5E3EBF',
                            cancelButtonColor: '#d33',
                            confirmButtonText: t('action.submitApplication'),
                          }).then(({ isConfirmed }) => {
                            if (isConfirmed) {
                              api.post(`/proposals/${proposal?.id}/status`, `"submitted"`).then(() => {
                                Swal.fire(t('sent'), t('proposalHasBeenSentForReview'), 'success').then(
                                  ({ isConfirmed }) => {
                                    setShowSubmitBtn(false);
                                    location.reload(); // force page refresh
                                  }
                                );
                              });
                            }
                          });
                        }}
                        disabled={!hasAnsweredAllQuestions}
                      >
                        {t('action.submitApplication')}
                      </Button>
                    </div>
                    <ReactTooltip
                      id="cantSubmitExplanation"
                      effect="solid"
                      role="tooltip"
                      type="dark"
                      className="forceTooltipBg"
                    />
                  </div>
                )}
                {/* {proposal?.created_by_user_id === user?.id && ( */}
                {isAdmin && proposal.status === 'draft' && (
                  <Menu>
                    <MenuButton tw="text-[.8rem] text-gray-700 hover:bg-gray-200 rounded-full border-gray-200 border w-8 h-8 mx-auto">
                      •••
                    </MenuButton>
                    <MenuList className="post-manage-dropdown">
                      <MenuItem>
                        {isAdmin && proposal.status === 'draft' && (
                          <Link href={`/proposal/${proposal?.id}/edit`} legacyBehavior>
                            <div>
                              <Icon icon="bxs:edit" />
                              {t('action.manage')}
                            </div>
                          </Link>
                        )}
                      </MenuItem>
                      {proposal?.status === 'draft' && (
                        <MenuItem
                          onSelect={() => {
                            showModal({
                              children: (
                                <>
                                  <P tw="text-base">{t('deleteItemConfirmation', { item: proposal.title })}</P>
                                  <div tw="inline-flex space-x-3">
                                    <Button btnType="danger" onClick={deleteProposal}>
                                      {t('yes')}
                                    </Button>
                                    <Button onClick={closeModal}>{t('no')}</Button>
                                  </div>
                                </>
                              ),
                              title: t('action.delete'),
                              maxWidth: '30rem',
                            });
                          }}
                        >
                          <Icon icon="ic:round-delete" />
                          {t('action.delete')}
                        </MenuItem>
                      )}
                    </MenuList>
                  </Menu>
                )}
              </div>
            </div>
            {title && <h1 tw="pb-2 font-bold text-2xl md:text-3xl">{title}</h1>}
            <div tw="flex items-center pb-2">
              <div>{t('status')}:&nbsp;</div>
              <span>{showColoredStatus(proposalStatus(proposal.status, cfpDates), t)}</span>
            </div>
            <div tw="flex flex-col lg:(justify-between flex-row) mb-4 gap-6">
              <div>
                <span tw="font-bold font-size[15px]">{t('proposedBy')}</span>
                <ProposalMembers users={proposal.users} />
              </div>
              <div tw="flex flex-col h-[fit-content] lg:items-end w-[300px] text-[15px] gap-3">
                {proposal.submitted_at && (
                  <div tw="flex items-center">
                    <p tw="mb-0">{t('submissionDate')}:&nbsp;</p>
                    <div tw="font-semibold">{displayObjectDate(proposal.submitted_at, 'L', true)}</div>
                  </div>
                )}
                {proposal?.score !== 0 && (
                  <div tw="flex items-center">
                    <p tw="mb-0">{t('score')}:&nbsp;</p>
                    <div tw="font-semibold">{`${proposal.score}/${cfp?.max_score}`}</div>
                  </div>
                )}
                {/* <div onClick={goToDiscussionSection} tw="flex flex-row cursor-pointer p-2 hocus:opacity-80">
                  <Icon icon="la:comment-solid" tw="color[#5F5D5D]" />
                  <span tw="flex flex-row underline font-semibold color[#5F5D5D]">
                    {proposal.comment_count ?? 0}
                    <TextWithPlural type="comment" count={proposal.comment_count ?? 0} />
                  </span>
                </div> */}
              </div>
            </div>
            <div tw="inline-flex flex-wrap gap-4 md:gap-6">
              <div>
                <div>{t('fromWorkspace')}:&nbsp;</div>
                {renderShortObjectCard('workspace', proposal.project)}
              </div>
              <div>
                <div>{t('forCallForProposals')}:&nbsp;</div>
                {renderShortObjectCard('cfp', proposal.call_for_proposal)}
              </div>
            </div>
            {filteredProposals?.length !== 0 && (
              <div tw="border-t mt-4 border-solid border-gray-200 max-w-[100ch]">
                <Button tw="mt-5 mb-3" onClick={toogleActive}>
                  {active ? 'Collapse all' : 'Expand all'}
                </Button>
                {filteredProposals?.map((answer, i) => (
                  <AnswerContent answer={answer} active={active} key={i} />
                ))}
              </div>
            )}
          </div>
          {/* <div ref={discussionRef}>
            <div tw="flex flex-row gap-1 mb-2 items-center">
              <Icon icon="uil:chat" tw="w-6 h-6 text-gray-500" />
              <span tw="color[rgb(114, 114, 114)] font-bold text-xl">Discussions</span>
            </div>
            <Feed
              entityType="proposal"
              feedId={proposal.id}
              allowPosting
              adminsCanDelete={proposal?.created_by_user_id === user?.id}
              isAdmin={proposal?.created_by_user_id === user?.id}
              noFilter
            />
          </div> */}
        </div>
      </div>
    </Layout>
  );
};

export async function getServerSideProps(ctx) {
  const api = getApiFromCtx(ctx);
  const { userId } = nextCookie(ctx);
  const res = await api.get(`/proposals/${ctx.query.id}`).catch((err) => console.error(err));
  // don't show page to non members if proposal has not been submitted
  if (res?.data) return { props: { proposal: res.data, userId } };
  return { redirect: { destination: '/', permanent: false } };
}

export default ProposalShow;
