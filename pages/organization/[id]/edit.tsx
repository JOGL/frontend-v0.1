import Icon from '~/src/components/primitives/Icon';
import { NextPage } from 'next';
import ManageFaq from '~/src/components/Tools/ManageFaq';
import ManageExternalLink from '~/src/components/Tools/ManageExternalLink';
import Layout from '~/src/components/Layout/layout/layout';
import Button from '~/src/components/primitives/Button';
import H2 from '~/src/components/primitives/H2';
import OrganizationForm from '~/src/components/Organization/OrganizationForm';
import { useApi } from '~/src/contexts/apiContext';
import useGet from '~/src/hooks/useGet';
import { Hub, Organization } from '~/src/types';
import useTranslation from 'next-translate/useTranslation';
import { getApiFromCtx } from '~/src/utils/getApi';
import { Fragment, useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/router';
import A from '~/src/components/primitives/A';
import useUser from '~/src/hooks/useUser';
import SpinLoader from '~/src/components/Tools/SpinLoader';
import { Disclosure, DisclosureButton, DisclosurePanel } from '@reach/disclosure';
import Link from 'next/link';
import { Tab, TabPanel, NavContainer, Nav, OverflowGradient } from '~/src/utils/style';
import tw from 'twin.macro';
import { confAlert, entityItem, getFullValidationObject, isAdmin, isOwner } from '~/src/utils/utils';
import PrivacySecurity from '~/src/components/PrivacySecurity/PrivacySecurity';
import FormToggleComponent from '~/src/components/Tools/Forms/FormToggleComponent';
import FormWysiwygComponent from '~/src/components/Tools/Forms/FormWysiwygComponent';
import MembersManage from '~/src/components/Members/MembersManage';
import HubManageAffiliation from '~/src/components/Hub/HubManageAffiliation';
import { MoreTabForContainerOrProfileArchiveOrDelete } from '~/src/components/Tools/MoreTabForContainerOrProfileArchiveOrDelete';
import FormValidator from '~/src/components/Tools/Forms/FormValidator';
import organizationFormRules from '~/src/components/Organization/OrganizationFormRules.json';
import WorkspaceManageAffiliation from '~/src/components/workspace/workspaceManageAffiliation/workspaceManageAffiliation';
import { CommunityEntityInvitationModelSource } from '~/__generated__/types';

interface Props {
  organization: Organization;
}

const DEFAULT_TAB = 'general';

const OrganizationEdit: NextPage<Props> = ({ organization: organizationProp }) => {
  const { t } = useTranslation('common');
  const validator = new FormValidator(organizationFormRules, t);
  const [organization, setOrganization] = useState(organizationProp);
  const [updatedOrganization, setUpdatedOrganization] = useState(undefined);
  const [sending, setSending] = useState(false);
  const [hasChanged, setHasChanged] = useState(false);
  const [selectedTab, setSelectedTab] = useState(DEFAULT_TAB);
  const [savedOrPublished, setSavedOrPublished] = useState('saved');
  const savedOrPublishedRef = useRef('saved');
  const [stateValidation, setStateValidation] = useState<any>({});
  const [hasErrors, setHasErrors] = useState<boolean>(false);
  const [showErrorList, setShowErrorList] = useState<boolean>(false);
  const urlBack = `/organization/${organization.id}/${organization.short_title}`;
  const api = useApi();
  const { user } = useUser();
  const router = useRouter();
  const navRef = useRef();
  savedOrPublishedRef.current = savedOrPublished;

  const { data: incomingObjInvites, mutate: incomingObjRevalidate } = useGet<CommunityEntityInvitationModelSource[]>(
    `/organizations/${organization.id}/communityEntityInvites/incoming`
  );
  const { data: outgoingObjInvites, mutate: outgoingObjRevalidate } = useGet<CommunityEntityInvitationModelSource[]>(
    `/organizations/${organization.id}/communityEntityInvites/outgoing`
  );
  const { data: hubs, mutate: hubsRevalidate } = useGet<Hub[]>(`/organizations/${organization.id}/nodes`);

  const handleChange: (key: any, content: any) => void = (key, content) => {
    setHasChanged(true);
    if (key === 'banner_id') {
      handleChange('banner_url', `${process.env.ADDRESS_BACK}/images/${content?.id}/full`);
      handleChange('banner_url_sm', `${process.env.ADDRESS_BACK}/images/${content?.id}/tn`);
      setUpdatedOrganization((prevUpdatedOrganization) => ({
        ...prevUpdatedOrganization,
        banner_id: content?.id ?? null,
      }));
    } else {
      setOrganization((prevOrganization) => ({ ...prevOrganization, [key]: content })); // update fields as user changes them
      // TODO: have only one place where we manage organization content
      setUpdatedOrganization((prevUpdatedOrganization) => ({ ...prevUpdatedOrganization, [key]: content })); // set an object containing only the fields/inputs that are updated by user
    }

    /* Validators start */
    const validation = validator.validate({ [key]: content });

    if (validation[key] !== undefined) {
      setStateValidation((prevState) => ({ ...prevState, [`valid_${key}`]: validation[key] }));
    }
    /* Validators end */
  };

  useEffect(() => {
    if (hasChanged) {
      const validation = validator.validate(organization);
      if (validation.isValid) {
        setHasErrors(false);
      } else {
        setHasErrors(true);
      }
    }
  }, [organization]);

  /*const handleChangeShowTabs: (key: any, content: any) => void = (key, content) => {
    const selected_tabs = key.split('.')[1];
    setOrganization((prevOrganization) => ({
      ...prevOrganization,
      selected_tabs: { ...prevOrganization.selected_tabs, [selected_tabs]: content },
    })); // update fields as user changes them
    // TODO: have only one place where we manage organization content
    setUpdatedOrganization((prevUpdatedOrganization) => ({
      ...prevUpdatedOrganization,
      selected_tabs: { ...organization.selected_tabs, [selected_tabs]: content },
    })); // set an object containing only the fields/inputs that are updated by user
  };*/

  const handleSubmit = async (data: any = null) => {
    if (!hasErrors) {
      setShowErrorList(false);

      const verifiedData = data && !data.clientX ? data : null;
      setSending(true);
      const res = await api
        .patch(`/organizations/${organization.id}`, verifiedData ?? updatedOrganization)
        .catch((err) => {
          console.error(`Couldn't patch organization with id=${organization.id}`, err);
          setSending(false);
        });
      if (res) {
        confAlert.fire({ icon: 'success', title: `Successfully ${savedOrPublishedRef.current}!` });
        setSending(false);
        setUpdatedOrganization(undefined); // reset updated organization component
        setStateValidation({}); // reset validations
        setHasChanged(false); //reset changed status
      }
    } else {
      /*
       * Validate entire object so we get entire list of missing fields, even for fields that haven't changed.
       * This is important for displaying the correct info on the first edit, otherwise mandatory fields that haven't been touched yet will not show up on the list of missing fields.
       */
      const fullValidationObject = getFullValidationObject(validator, organization);
      setStateValidation(fullValidationObject);

      setShowErrorList(true);
      window?.scrollTo({ top: 0, behavior: 'smooth' }); // scroll to element to show error
    }
  };

  const publish = () => {
    setSavedOrPublished('published');
    handleChange('status', 'active');
    handleSubmit({ ...updatedOrganization, status: 'active' });
  };

  const handleChangeManagementSettings = (id) => {
    const newArr = organization.management.includes(id)
      ? organization.management.filter((x) => x !== id)
      : [...organization.management, id];
    handleChange('management', newArr);
    handleSubmit({ management: newArr });
  };

  const tabs = [
    { value: 'general', translationId: 'general', icon: 'fluent:text-description-20-filled' },
    { value: 'about', translationId: 'about', icon: 'ic:outline-info' },
    {
      value: 'privacy_security',
      translationId: 'privacySecurity',
      icon: 'material-symbols:privacy-tip-outline',
    },
    {
      value: 'ecosystem',
      translationId: 'ecosystem',
      icon: 'material-symbols:groups-outline',
      children: [
        { value: 'members', translationId: 'person.other', icon: 'mdi:people-check' },
        { value: 'workspaces', translationId: 'workspace.other', icon: 'material-symbols:groups-outline' },
        { value: 'hubs', translationId: 'hub.other', icon: 'carbon:edge-node-alt' },
      ],
    },
    // { value: 'resources', translationId: 'resource.other', icon: 'carbon:software-resource-cluster' },
    // { value: 'faq', translationId: 'faqAcronym', icon: 'octicon:question-24' },
    { value: 'more', translationId: 'more', icon: 'mdi:more-circle-outline' },
  ];

  const basicTabs = tabs.map(({ value }) => value);
  const subTabs =
    tabs
      .filter((val) => val.children?.length > 0)
      .map((child) => child.children.map(({ value }) => value))
      .flat() || [];
  const tabValues = [...basicTabs, ...subTabs];

  useEffect(() => {
    // on first load, check if url has a tab param, and if it does, select this tab
    if (router.query.tab) {
      setSelectedTab(router.query.tab as string);
      const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      const yAdjustment = window.innerWidth < 768 ? 40 : 150; // change yAdustement depending on if it's mobile/tablet or desktop
      window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
    }
  }, []);

  // function when changing tab (or when router change in general)
  useEffect(() => {
    if (tabValues.includes(router.query.tab as string) && navRef?.current) {
      if (router.query.tab === 'general') {
        router.push(`/organization/${router.query.id}/edit`, undefined, {
          shallow: true,
        });
      }
      const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      const yAdjustment = window.innerWidth < 768 ? 40 : 150; // change yAdustement depending on if it's mobile/tablet or desktop
      window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
      setSelectedTab(router.query.tab as string);
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);
  }, [router]);

  const showEditContext = () => (
    <div tw="inline-flex flex-col w-full pl-[10px] items-start justify-start md:(pl-[0px] items-start justify-start) max-md:(pt-4 pb-2)">
      <A href={urlBack}>
        <div tw="px-4 py-1 mb-1 w-full border-gray-200 border rounded-full text-primary! hover:bg-gray-100 md:hidden">
          <Icon icon="formkit:arrowleft" tw="w-5 h-5" />
          {t('action.back')}
        </div>
      </A>
      <h1 tw="font-extrabold text-[36px]">{t('action.editTheOrganization')}</h1>
      <span tw="text-black px-[6px] py-[3px] text-[15px] mb-2" style={{ background: entityItem.organization.color }}>
        {t('organization', { count: 1 })} <span tw="font-bold">{organization?.title}</span>
      </span>
      {false && <span tw="italic text-[15px] font-[400]">In this tab you can edit name, banner etc.</span>}
    </div>
  );

  const isSaveHidden = () => selectedTab !== 'general' && selectedTab !== 'about' && selectedTab !== 'privacy_security';

  return (
    <Layout title={`${organization.title} | JOGL`}>
      <div tw="mx-auto sm:shadow-custom2">
        <div
          tw="mb-6 grid grid-cols-1 md:(bg-white grid-cols-[14rem calc(100% - 14rem)]) bg-white border-t border-solid border-gray-200"
          ref={navRef}
        >
          {/* Header and nav (left col on desktop, top col on mobile */}
          <div tw="md:hidden">{showEditContext()}</div>
          <div tw="flex flex-col border-r-2 border-solid border-[#F7F7F9] bg-white sticky top-[64px] z-[8] md:(items-center)">
            <NavContainer>
              <OverflowGradient tw="md:hidden" gradientPosition="left" />
              <OverflowGradient tw="md:hidden" gradientPosition="right" />
              <Nav as="nav" tw="flex gap-3 pt-2.5 w-full md:(flex-col gap-0 pt-0)">
                <div tw="flex flex-col w-full">
                  <div tw="relative hidden md:(flex justify-center flex-col items-center)">
                    <img
                      tw="object-cover h-[100px] w-full relative border-b border-gray-200"
                      src={organization?.banner_url ?? '/images/default/default-organization.png'}
                      alt="banner url"
                    />
                    {organization?.status === 'draft' && (
                      <div tw="absolute top-2 left-2 text-sm font-medium text-gray-600 rounded bg-white bg-opacity-80 px-2 py-1 [box-shadow:0px 4px 15px rgb(0 0 0 / 9%)] first-letter:capitalize">
                        {t('draft')}
                      </div>
                    )}
                    <A href={urlBack}>
                      <div tw="px-4 ml-auto my-2 py-1 w-full border-gray-200 border rounded-full text-primary! hover:bg-gray-100">
                        <Icon icon="formkit:arrowleft" tw="w-5 h-5" />
                        {t('action.back')}
                      </div>
                    </A>
                  </div>
                </div>
                {tabs.map((item, index) => (
                  <>
                    {!item.children ? (
                      <Link
                        shallow
                        scroll={false}
                        key={index}
                        href={`/organization/${router.query.id}/edit${
                          item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`
                        }`}
                        legacyBehavior
                      >
                        <Tab
                          selected={item.value === selectedTab}
                          tw="pl-4 pr-1 py-3"
                          as="button"
                          id={`${item.value}-tab`}
                          css={[item.value === 'info' ? tw`md:hidden` : tw`block`]}
                        >
                          <Icon icon={item.icon} tw="mr-2" />
                          {t(item.translationId)}
                        </Tab>
                      </Link>
                    ) : (
                      <Disclosure as="div" defaultOpen={true}>
                        <DisclosureButton tw="pt-0 px-[3px] pb-[5px] md:(pl-4 pr-1 py-3 text-left)" as={Tab}>
                          <Icon icon={item.icon} tw="mr-2" />
                          {t(item.translationId)}
                        </DisclosureButton>
                        <DisclosurePanel
                          as="div"
                          tw="list-none max-md:data-[state=open]:flex max-md:[border: 1px solid lightgray] max-md:-mt-[5px] max-md:mb-[5px] gap-x-2"
                        >
                          {item.children.map((subItem) => (
                            <Fragment key={subItem.value}>
                              <Link
                                shallow
                                scroll={false}
                                key={index}
                                href={`/organization/${router.query.id}/edit?tab=${subItem.value}`}
                                legacyBehavior
                              >
                                <Tab
                                  selected={subItem.value === selectedTab}
                                  tw="pl-7 pr-1 pt-0 pb-0! md:py-3! block w-full"
                                  as="button"
                                  id={`${subItem.value}-tab`}
                                >
                                  <Icon icon={subItem.icon} tw="mr-2" />
                                  {t(subItem.translationId)}
                                </Tab>
                              </Link>
                            </Fragment>
                          ))}
                        </DisclosurePanel>
                      </Disclosure>
                    )}
                  </>
                ))}
              </Nav>
            </NavContainer>
          </div>

          {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
          <div tw="flex flex-col px-0 pb-10 relative mt-4 md:px-4 md:(pl-4) xl:(pr-4)">
            <div tw="max-md:hidden">{showEditContext()}</div>
            {/* General infos tab */}
            {selectedTab === 'general' && (
              <TabPanel>
                <OrganizationForm
                  organization={organization}
                  stateValidation={stateValidation}
                  showErrorList={showErrorList}
                  handleChange={handleChange}
                />
              </TabPanel>
            )}

            {/* About tab */}
            {selectedTab === 'about' && (
              <TabPanel>
                <FormWysiwygComponent
                  id="description"
                  content={organization.description}
                  title={t('description')}
                  placeholder={t('describeYourOrganizationInDetail')}
                  onChange={handleChange}
                />
                <ManageExternalLink links={organization.links} handleChange={handleChange} />
              </TabPanel>
            )}

            {/* Feed / discussions */}
            {selectedTab === 'feed' && (
              <TabPanel>
                <p>
                  In this tab, you can manage the resources of your organization. Members can benefit from the resources
                  made available only by participating in calls and then being validated by the admins.
                </p>

                <H2>Wall management</H2>
                <h5>Posts on the feed</h5>
                <FormToggleComponent
                  id="content_entities_created_by_any_member"
                  title="Allow members to create posts"
                  choice1={t('no')}
                  choice2={t('yes')}
                  isChecked={organization.management.includes('content_entities_created_by_any_member')}
                  onChange={handleChangeManagementSettings}
                />
                <FormToggleComponent
                  id="content_entities_deleted_by_admins"
                  title="Allow admins to delete posts/comments"
                  choice1={t('no')}
                  choice2={t('yes')}
                  isChecked={organization.management.includes('content_entities_deleted_by_admins')}
                  onChange={handleChangeManagementSettings}
                />
              </TabPanel>
            )}

            {/* Privacy/security tab */}
            {selectedTab === 'privacy_security' && (
              <TabPanel>
                <PrivacySecurity object={organization} type="organization" handleChange={handleChange} />
              </TabPanel>
            )}

            {/* Members tab */}
            {selectedTab === 'members' && (
              <TabPanel>
                <MembersManage itemId={organization.id} itemType="organizations" isOwner={isOwner(organization)} />
              </TabPanel>
            )}

            {/* Workspaces */}
            {selectedTab === 'workspaces' && (
              <TabPanel>
                <WorkspaceManageAffiliation
                  object={organization}
                  objectType="organizations"
                  handleSubmit={handleSubmit}
                />
              </TabPanel>
            )}

            {/* Hubs tab */}
            {selectedTab === 'hubs' && (
              <TabPanel>
                <HubManageAffiliation
                  hubs={hubs}
                  childType="organizations"
                  childId={organization.id}
                  incomingObjInvites={incomingObjInvites}
                  incomingObjRevalidate={incomingObjRevalidate}
                  hubsRevalidate={hubsRevalidate}
                  outgoingObjInvites={outgoingObjInvites}
                  outgoingObjRevalidate={outgoingObjRevalidate}
                />
              </TabPanel>
            )}

            {/* FAQs tab */}
            {selectedTab === 'faq' && (
              <TabPanel>
                <ManageFaq faqs={organization.faq} handleChange={handleChange} />
              </TabPanel>
            )}

            {/* Partners tab */}
            {/* <TabPanel>
              <OrganizationForm
                mode="edit_partners"
                organization={organization}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                hasUpdated={hasUpdated}
                sending={sending}
              />
            </TabPanel> */}

            {/* Resources tab */}
            {selectedTab === 'resources' && (
              <TabPanel>
                Resources - Work in progress
                {/* <H2 tw="pb-3">{t('document', { count: 2 })}</H2> */}
                {/* <div tw="flex flex-col pb-5" /> */}
              </TabPanel>
            )}

            {/* {selectedTab === 'publications' && <TabPanel id="publications">Publications - Work in progress</TabPanel>} */}
            {/* {selectedTab === 'documentation' && (
              <TabPanel id="documentation">
                <ManageDocumentationSettings
                  object={organization}
                  handleChange={handleChange}
                  handleSubmit={handleSubmit}
                />
              </TabPanel>
            )} */}
            {selectedTab === 'callForProposal' && (
              <TabPanel id="callForProposal">Call for proposals - Work in progress</TabPanel>
            )}
            {selectedTab === 'events' && <TabPanel id="events">Events - Work in progress</TabPanel>}

            {/* Advanced tab */}
            {selectedTab === 'more' && (
              <TabPanel>
                <MoreTabForContainerOrProfileArchiveOrDelete
                  containerOrProfile={organization}
                  setContainerOrProfile={setOrganization}
                  handleSubmit={handleSubmit}
                  user={user}
                  apiURL="/organizations"
                  containerType="organization"
                  translateText="organization.one"
                />
              </TabPanel>
            )}

            {/* Bottom action buttons */}
            {!isSaveHidden() && (
              <div
                tw="mt-10 mb-3 space-x-4 text-center inline-flex items-center self-center"
                css={
                  hasChanged &&
                  tw`sticky bottom-0 inline-flex items-center justify-center py-4 mx-auto border-t backdrop-blur-md bg-white/30 w-[calc(100%+32px)] -ml-4`
                }
              >
                <A href={urlBack}>
                  <Button btnType="secondary">{t('action.cancel')}</Button>
                </A>
                <Button
                  onClick={handleSubmit}
                  disabled={sending || !hasChanged}
                  css={
                    organization?.status === 'draft' &&
                    tw`bg-[#C8BCE9] border-[#C8BCE9] text-black hover:text-white hover:bg-primary`
                  }
                >
                  {sending && savedOrPublishedRef.current === 'saved' && <SpinLoader />}
                  {organization?.status === 'draft' ? t('action.saveAsDraft') : t('action.save')}
                </Button>
                {organization?.status === 'draft' && <Button onClick={publish}>{t('action.publish')}</Button>}
                {/* {hasChanged && <p tw="mb-0">{t('dontForgetToSaveYourChanges')}</p>} */}
              </div>
            )}
          </div>
        </div>
      </div>
    </Layout>
  );
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  const res = await api
    .get(`/organizations/${query.id}`)
    .catch((err) => console.error(`Couldn't fetch organization with id=${query.id}`, err));
  // Check if it got the organization and if the user is admin
  if (res?.data && isAdmin(res?.data)) return { props: { organization: { ...res?.data }, key: query.id } };
  return { redirect: { destination: `/organization/${query.id}`, permanent: false } };
}

export default OrganizationEdit;
