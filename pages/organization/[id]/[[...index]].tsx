import { NextPage } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { Fragment, useEffect, useRef, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Layout from '~/src/components/Layout/layout/layout';
import OrganizationAbout from '~/src/components/Organization/OrganizationAbout';
import OrganizationHeader from '~/src/components/Organization/OrganizationHeader';
import { Organization } from '~/src/types';
import { getApiFromCtx } from '~/src/utils/getApi';
import tw from 'twin.macro';
import 'intro.js/introjs.css';
import OrganizationInfo from '~/src/components/Organization/OrganizationInfo';
import { Tab, TabPanel, NavContainer, Nav, OverflowGradient } from '~/src/utils/style';
import { entityItem, isMember } from '~/src/utils/utils';
import Icon from '~/src/components/primitives/Icon';
import { Disclosure, DisclosureButton, DisclosurePanel } from '@reach/disclosure';
import ShowHubs from '~/src/components/Tools/ShowHubs';
import ShowMembers from '~/src/components/Tools/ShowMembers';
import ShowSkillSdg from '~/src/components/Tools/ShowSkillSdg';
import ShowLinks from '~/src/components/Tools/ShowLinks';
import { useStonePathStore } from '~/src/contexts/stonepath/StonePathStoreProvider';
import EntityEventList from '~/src/components/Event/entityEventList/entityEventList';
import { Permission } from '~/__generated__/types';

const DEFAULT_TAB = 'about';

const tabs = [
  { value: 'info', translationId: 'info', icon: 'ic:outline-info' },
  { value: 'about', translationId: 'about', icon: 'ic:outline-info' },
  {
    value: 'ecosystem',
    translationId: 'ecosystem',
    icon: 'material-symbols:groups-outline',
    childrens: [
      { value: 'members', translationId: 'person.other', icon: 'mdi:people-check' },
      { value: 'hubs', translationId: 'hub.other', icon: 'carbon:edge-node-alt' },
    ],
  },
  { value: 'events', translationId: 'event.other', icon: 'material-symbols:event-note-outline' },
];

const OrganizationDetails: NextPage<{ organization: Organization }> = ({ organization }) => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const [selectedTab, setSelectedTab] = useState(DEFAULT_TAB);
  const headerRef = useRef<HTMLDivElement>();
  const limitedVisibility = organization.content_privacy === 'private' && !isMember(organization);
  const { setStonePath } = useStonePathStore();

  const [shouldRefreshHubs, setShouldRefreshHubs] = useState<boolean>(false);

  useEffect(() => {
    setStonePath(organization?.path ?? []);
  }, [organization, setStonePath]);

  const basicTabs = tabs.map(({ value }) => value);
  const subTabs =
    tabs
      .filter((val) => val.childrens?.length > 0)
      .map((x) => x.childrens.map(({ value }) => value))
      .flat() || [];
  const tabValues = [...basicTabs, ...subTabs];
  useEffect(() => {
    // on first load, check if url has a tab param, and if it does, select this tab
    if (router.query.tab) {
      setSelectedTab(router.query.tab as string);
    }
  }, []);

  // function when changing tab (or when router change in general)
  useEffect(() => {
    if (tabValues.includes(router.query.tab as string)) {
      if (router.query.tab === DEFAULT_TAB) {
        router.replace(`/organization/${router.query.id}`, undefined, {
          shallow: true,
        });
      }
      // const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      // const yAdjustment = window.innerWidth < 768 ? 40 : 150; // change yAdustement depending on if it's mobile/tablet or desktop
      // window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
      setSelectedTab(router.query.tab as string);
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);
  }, [router]);

  const refreshContent = () => {
    switch (selectedTab) {
      case 'hubs':
        setShouldRefreshHubs(!shouldRefreshHubs);
        break;
      default:
        return;
    }
  };

  const displayTagsAndLinks = () => (
    <div tw="max-md:mt-7">
      {(organization.interests?.length > 0 || organization.keywords?.length > 0) && (
        <ShowSkillSdg interests={organization.interests} skills={organization.keywords} />
      )}
      {organization?.links && organization?.links.length !== 0 && <ShowLinks links={organization.links} />}
    </div>
  );

  return (
    <Layout
      title={organization?.title && `${organization.title} | JOGL`}
      desc={organization?.short_description}
      img={organization?.banner_url || '/images/default/default-organization.png'}
      noIndex={organization?.status === 'draft'}
    >
      <div tw="sm:shadow-custom2">
        <div
          tw="w-full px-3 py-1 flex justify-between md:text-[18px]"
          ref={headerRef}
          style={{ background: entityItem.organization.color }}
        >
          <span tw="font-bold">{t('organization', { count: 1 })}</span>
          <span>{organization.user_access_level}</span>
        </div>

        <OrganizationHeader
          organization={organization}
          limitedVisibility={limitedVisibility}
          refresh={refreshContent}
        />
        <span tw="max-md:hidden">{displayTagsAndLinks()}</span>

        {/* ---- Page grid starts here ---- */}
        {!limitedVisibility && (
          <div tw="mb-6 grid grid-cols-1 md:(bg-white grid-cols-[14rem calc(100% - 14rem)]) bg-white border-t border-solid border-gray-200">
            {/* Header and nav (left col on desktop, top col on mobile */}
            <div tw="flex flex-col border-r-2 border-solid border-[#F7F7F9] bg-white sticky top-[60px] z-[7] md:(items-center)">
              <NavContainer>
                <OverflowGradient tw="md:hidden" gradientPosition="left" />
                <OverflowGradient tw="md:hidden" gradientPosition="right" />
                <Nav as="nav" tw="flex gap-3 pt-2.5 w-full md:(flex-col gap-0 pt-0)">
                  {tabs.map((item, index) => (
                    <>
                      {!item.childrens ? (
                        <Link
                          shallow
                          scroll={false}
                          key={index}
                          href={`/organization/${router.query.id}${
                            item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`
                          }`}
                          legacyBehavior
                          replace
                        >
                          <Tab
                            selected={item.value === selectedTab}
                            tw="pl-4 pr-1 py-3"
                            as="button"
                            id={`${item.value}-tab`}
                            css={[item.value === 'info' ? tw`md:hidden` : tw`block`]}
                          >
                            <Icon icon={item.icon} tw="mr-2" />
                            {t(item.translationId)}
                          </Tab>
                        </Link>
                      ) : (
                        <Disclosure as="div" defaultOpen={true}>
                          <DisclosureButton tw="pt-0 px-[3px] pb-[5px] md:(pl-4 pr-1 py-3 text-left)" as={Tab}>
                            <Icon icon={item.icon} tw="mr-2" />
                            {t(item.translationId)}
                          </DisclosureButton>
                          <DisclosurePanel
                            as="div"
                            tw="list-none max-md:data-[state=open]:flex max-md:[border: 1px solid lightgray] max-md:-mt-[5px] max-md:mb-[5px] gap-x-2"
                          >
                            {item.childrens.map((subItem) => (
                              <Fragment key={subItem.value}>
                                <Link
                                  shallow
                                  scroll={false}
                                  key={index}
                                  href={`/organization/${router.query.id}?tab=${subItem.value}`}
                                  legacyBehavior
                                  replace
                                >
                                  <Tab
                                    selected={subItem.value === selectedTab}
                                    tw="pl-7 pr-1 pt-0 pb-0! md:py-3! block w-full"
                                    as="button"
                                    id={`${subItem.value}-tab`}
                                  >
                                    <Icon icon={subItem.icon} tw="mr-2" />
                                    {t(subItem.translationId)}
                                  </Tab>
                                </Link>
                              </Fragment>
                            ))}
                          </DisclosurePanel>
                        </Disclosure>
                      )}
                    </>
                  ))}
                </Nav>
              </NavContainer>
            </div>

            {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
            <div tw="flex flex-col px-0 pb-10 relative md:px-4 md:(pl-4) xl:(pr-4)">
              {/* Main infos */}
              {selectedTab === 'info' && (
                <TabPanel id="info" tw="md:hidden">
                  <OrganizationInfo organization={organization} limitedVisibility={limitedVisibility} />
                  <span tw="md:hidden">{displayTagsAndLinks()}</span>
                </TabPanel>
              )}

              {/* About */}
              {selectedTab === 'about' && (
                <TabPanel id="about">
                  <div tw="max-w-3xl mx-auto">
                    <OrganizationAbout description={organization.description} />
                  </div>
                </TabPanel>
              )}

              {/* Hubs */}
              {selectedTab === 'hubs' && (
                <TabPanel id="hubs">
                  <ShowHubs
                    objectId={organization.id}
                    objectType="organizations"
                    isMember={isMember(organization)}
                    forceRefresh={shouldRefreshHubs}
                  />
                </TabPanel>
              )}

              {/* Members */}
              {selectedTab === 'members' && (
                <TabPanel id="members" tw="relative ">
                  <ShowMembers
                    parentId={organization.id}
                    parentType="organizations"
                    canCreate={organization.user_access?.permissions?.includes('manage')}
                  />
                </TabPanel>
              )}

              {/* Events */}
              {selectedTab === 'events' && (
                <TabPanel id="events">
                  <EntityEventList
                    entityId={organization.id}
                    canManageEvents={organization.user_access.permissions?.includes(Permission.Createevents)}
                  />
                </TabPanel>
              )}

              {/* Resources */}
              {selectedTab === 'resources' && <TabPanel id="resources">Resources - Work in progress</TabPanel>}
            </div>
          </div>
        )}
      </div>
    </Layout>
  );
};

export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/organizations/${ctx.query.id}/detail`).catch((err) => console.error(err));
  if (res?.data) return { props: { organization: { ...res.data }, key: ctx.query.id } };
  return { redirect: { destination: '/search?tab=organizations', permanent: false } };
};

export default OrganizationDetails;
