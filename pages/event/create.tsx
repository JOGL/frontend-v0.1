import Icon from '~/src/components/primitives/Icon';
import { NextPage } from 'next';
import Layout from '~/src/components/Layout/layout/layout';
import Button from '~/src/components/primitives/Button';
import { useApi } from '~/src/contexts/apiContext';
import { Hub, User } from '~/src/types';
import useTranslation from 'next-translate/useTranslation';
import { getApiFromCtx } from '~/src/utils/getApi';
import { useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/router';
import A from '~/src/components/primitives/A';
import SpinLoader from '~/src/components/Tools/SpinLoader';
import tw from 'twin.macro';
import { confAlert, entityItem, getFullValidationObject, getUserDefaultTimezone } from '~/src/utils/utils';
import FormValidator from '~/src/components/Tools/Forms/FormValidator';
import eventFormRules from '~/src/components/Event/EventFormRules.json';
import EventForm from '~/src/components/Event/EventForm';
import { useModal } from '~/src/contexts/modalContext';
import nextCookie from 'next-cookies';
import useUser from '~/src/hooks/useUser';
import dayjs from 'dayjs';
import { Event, EventInvite, EventVisibility } from '~/src/types/event';
import { WorkspaceModel } from '~/__generated__/types';

interface Props {
  event?: Event;
  parent: WorkspaceModel | Hub;
  parentType: string;
  parentMembers: User[];
  batchInviteMembers: boolean;
  chosenVisibility: EventVisibility;
}

const now = new Date();

const defaultStartDate = () => {
  const roundedMinutes = Math.ceil(now.getMinutes() / 15) * 15;
  now.setMinutes(roundedMinutes);
  now.setSeconds(0, 0);
  return now;
};

const defaultEndDate = () => {
  const roundedMinutes = Math.ceil(now.getMinutes() / 15) * 15;
  now.setMinutes(roundedMinutes);
  now.setSeconds(0, 0);
  return new Date(now.getTime() + 60 * 60 * 1000);
};

const defaultEvent: Event = {
  id: null,
  title: null,
  description: null,
  generate_meet_link: false,
  generated_meeting_url: null,
  meeting_url: null,
  banner_id: null,
  banner_url: null,
  banner_url_sm: null,
  visibility: 'private',
  start: defaultStartDate(),
  end: defaultEndDate(),
  timezone: null,
  keywords: [],
  location: null,
  is_new: false,
};

const EventCreate: NextPage<Props> = ({
  event: eventProp,
  parent,
  parentType,
  parentMembers,
  batchInviteMembers,
  chosenVisibility,
}) => {
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const { user } = useUser();
  const validator = new FormValidator(eventFormRules, t);
  const existingEvent = eventProp?.id ? true : false;

  //Needs to be done here so that if the user creates another event while inside the EventCreate page,
  //react hooks stay in the same order and no errors are thrown
  const defaultTimezone = getUserDefaultTimezone();

  //Initialize attendances_to_upsert here to prevent using data from old array references.
  const [event, setEvent] = useState<Event>(
    eventProp ?? {
      ...defaultEvent,
      visibility: chosenVisibility,
      attendances_to_upsert: [],
      timezone: defaultTimezone,
    }
  );

  const [initialTitle, setInitialTitle] = useState<string>(eventProp?.title ?? '');
  const [initialStartDate, setInitialStartDate] = useState<Date>(new Date(eventProp?.start) ?? undefined);
  const [initialEndDate, setInitialEndDate] = useState<Date>(new Date(eventProp?.end) ?? undefined);

  const [sending, setSending] = useState(false);
  const [hasChanged, setHasChanged] = useState(false);
  const [stateValidation, setStateValidation] = useState<any>({});
  const [hasErrors, setHasErrors] = useState<boolean>(false);
  const [showErrorList, setShowErrorList] = useState<boolean>(false);

  const urlBack = event?.id ? `/event/${event?.id}` : `/${entityItem[parentType]?.front_path}/${parent?.id}`;
  const api = useApi();
  const router = useRouter();
  const navRef = useRef<any>();

  useEffect(() => {
    if (!existingEvent && user) {
      if (batchInviteMembers) {
        (parentMembers?.filter((member: User) => member.id !== user.id) ?? []).map((member: User) => {
          event.attendances_to_upsert.push({
            user_id: member?.id,
            user_email: member?.email,
            community_entity_id: null,
            origin_community_entity_id: null,
            labels: [],
            access_level: 'member',
          });
        });
      }

      //Always add the user that is creating the event at the beginning
      event.attendances_to_upsert.unshift({
        user_id: user?.id,
        access_level: 'admin',
        labels: [],
        community_entity_id: null,
        origin_community_entity_id: null,
        user_email: user?.email,
      });
    }
  }, [batchInviteMembers, existingEvent, parentMembers, user]);

  const handleChange = (key, content) => {
    setHasChanged(true);
    if (key === 'banner_id') {
      handleChange('banner_url', `${process.env.ADDRESS_BACK}/images/${content?.id}/full`);
      handleChange('banner_url_sm', `${process.env.ADDRESS_BACK}/images/${content?.id}/tn`);
      setEvent((prev: Event) => ({
        ...prev,
        banner_id: content?.id ?? null,
      }));
    } else {
      setEvent((prev: Event) => ({ ...prev, [key]: content }));
    }

    runValidations(key, content);
  };

  const handleAttendancesToUpsertChange = (newArray: EventInvite[]) => {
    setEvent((prev: Event) => ({
      ...prev,
      attendances_to_upsert: newArray,
    }));

    setHasChanged(true);
  };

  const runValidations = (key, content) => {
    /* Validators start */
    const validation = validator.validate({ [key]: content });

    if (validation[key] !== undefined) {
      setStateValidation((prevState) => ({ ...prevState, [`valid_${key}`]: validation[key] }));
    }
    /* Validators end */
  };

  useEffect(() => {
    if (hasChanged) {
      const validation = validator.validate(event);
      if (validation.isValid) {
        setHasErrors(false);
      } else {
        setHasErrors(true);
      }
    }
  }, [event]);

  const handleSubmitClick = () => {
    const hasTitleChanged = event?.title !== initialTitle;
    const hasDateChanged =
      new Date(event?.start)?.getTime() !== new Date(initialStartDate)?.getTime() ||
      new Date(event?.end)?.getTime() !== new Date(initialEndDate)?.getTime();

    //Only show modal when event is not being created
    if (event?.id && (hasTitleChanged || hasDateChanged)) {
      const text =
        hasTitleChanged && hasDateChanged
          ? t('becauseUpdatedEventTitleAndDate')
          : hasTitleChanged
          ? t('becauseUpdatedEventTitle')
          : t('becauseUpdatedEventDate');

      showModal({
        children: (
          <div tw="flex flex-col gap-4 items-center">
            <span tw="text-sm">{text}.</span>
            <div tw="flex gap-2 items-center">
              <Button
                btnType="secondary"
                type="submit"
                onClick={() => {
                  closeModal();
                }}
              >
                {t('action.cancel')}
              </Button>

              <Button
                type="submit"
                onClick={() => {
                  handleSubmit();
                  closeModal();
                }}
              >
                {t('action.confirm')}
              </Button>
            </div>
          </div>
        ),
        title: t('confirmChanges'),
        maxWidth: '30rem',
      });
    } else {
      handleSubmit();
    }
  };

  const handleSubmit = async () => {
    if (!hasErrors) {
      setShowErrorList(false);
      setSending(true);

      //Hack to prevent Axios from converting date objects to UTC, resulting in email invites being sent with the wrong time
      const eventWithStringDates = {
        ...event,
        start: dayjs(event.start).format('YYYY-MM-DDTHH:mm:ss'),
        end: dayjs(event.end).format('YYYY-MM-DDTHH:mm:ss'),
      };

      let res;
      //If id exists, update the event, else create it.
      if (event?.id) {
        res = await api.put(`/events/${event.id}`, eventWithStringDates).catch((err) => {
          console.error(`Couldn't put event with id=${event.id}`, err);
          setSending(false);
        });
      } else {
        res = await api
          .post(`${entityItem[parentType]?.back_path}/${parent?.id}/events`, eventWithStringDates)
          .catch((err) => {
            console.error(`Couldn't post event with id=${event.id}`, err);
            setSending(false);
          });
      }

      if (res && res?.data) {
        confAlert.fire({ icon: 'success', title: event?.id ? 'Successfully updated!' : 'Successfully created' });

        //If event was just created, redirect to the edit page, else just update the state
        if (!event?.id) {
          router.push({
            pathname: '/event/create',
            query: {
              id: res.data,
              parentId: parent?.id,
              parentType: parentType,
            },
          });
        } else {
          //If the toggle is set to true, but the generated url is not there, set it so it displays in the form without needing to refresh
          if (event?.generate_meet_link && !event?.generated_meeting_url) {
            setEvent({ ...event, generated_meeting_url: res?.data?.generated_meeting_url });
          }

          setSending(false);
          setStateValidation({}); // reset validations
          setHasChanged(false); //reset changed status

          //reset initialValues
          setInitialTitle(event?.title);
          setInitialStartDate(new Date(event?.start));
          setInitialEndDate(new Date(event?.end));
        }
      }
    } else {
      /*
       * Validate entire object so we get entire list of missing fields, even for fields that haven't changed.
       * This is important for displaying the correct info on the first edit, otherwise mandatory fields that haven't been touched yet will not show up on the list of missing fields.
       */
      const fullValidationObject = getFullValidationObject(validator, event);
      setStateValidation(fullValidationObject);

      setShowErrorList(true);
      window?.scrollTo({ top: 0, behavior: 'smooth' }); // scroll to element to show error
    }
  };

  const handleDeleteClick = () => {
    showModal({
      children: (
        <div tw="flex flex-col gap-2 items-center">
          {t('deleteItemConfirmation', { item: t('event', { count: 1 }) })}
          <Button
            type="submit"
            onClick={() => {
              handleDelete();
              closeModal();
            }}
          >
            {t('action.confirm')}
          </Button>
        </div>
      ),
      title: t('action.deleteItem', { item: t('event', { count: 1 }) }),
      maxWidth: '30rem',
    });
  };

  const handleDelete = async () => {
    try {
      await api.delete(`/events/${event?.id}`);
      confAlert.fire({ icon: 'success', title: 'Deleted event' });
      router.push(`/${entityItem[parentType]?.front_path}/${parent?.id}`);
    } catch (err) {
      console.warn(err);
      confAlert.fire({ icon: 'error', title: 'Error while deleting event' });
    }
  };

  const showEditContext = () => (
    <div tw="inline-flex flex-col w-full pl-[10px] items-start justify-start md:(pl-[0px] items-start justify-start) max-md:(pt-4 pb-2)">
      <div tw="md:hidden pb-2">
        {/* Back button going to the parent page */}
        <A href={urlBack}>
          <div tw="px-4 ml-auto mr-auto my-2 py-1 border-gray-200 border rounded-full text-primary! hover:bg-gray-100 self-center">
            <Icon icon="formkit:arrowleft" tw="size-5" />
            {t('action.back')}
          </div>
        </A>
      </div>
      <h1 tw="font-extrabold text-[36px]">{t('action.editTheEvent')}</h1>
      <div tw="flex w-full justify-between items-center">
        <span tw="color[black] px-[6px] py-[3px] text-[15px] mb-2" style={{ background: entityItem.event.color }}>
          {t('event', { count: 1 })} <span tw="font-bold">{event?.title}</span>
        </span>
        {event?.id && event?.permissions?.includes('delete') && (
          <Icon icon="bi:trash-fill" tw="size-5 cursor-pointer hover:text-red-500" onClick={handleDeleteClick} />
        )}
      </div>
      {false && <span tw="italic text-[15px] font-[400]">In this tab you can edit title, banner etc.</span>}
    </div>
  );

  return (
    <Layout title={`${event?.title ?? 'Event'} | JOGL`}>
      <div tw="mx-auto sm:shadow-custom2">
        <div
          tw="mb-6 grid grid-cols-1 md:(bg-white grid-cols-[14rem calc(100% - 14rem)]) bg-white border-t border-solid border-gray-200"
          ref={navRef}
        >
          {/* Header and nav (left col on desktop, top col on mobile */}
          <div tw="md:hidden">{showEditContext()}</div>
          <div tw="flex flex-col w-full">
            <div tw="relative hidden md:(flex justify-center flex-col items-center)">
              <img
                tw="object-cover h-[100px] w-full relative border-b border-gray-200"
                src={event?.banner_url ?? '/images/default/default-event.png'}
                alt="banner url"
              />
              <A href={urlBack}>
                <div tw="flex items-center px-4 ml-auto mr-auto my-2 py-1 border-gray-200 border rounded-full text-primary! hover:bg-gray-100 self-center">
                  <Icon icon="formkit:arrowleft" tw="size-5" />
                  {t('action.back')}
                </div>
              </A>
            </div>
          </div>

          {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
          {event && (
            <div tw="flex flex-col relative mt-4 px-4">
              <div tw="max-md:hidden">{showEditContext()}</div>
              {/* General info tab */}

              <EventForm
                event={event}
                parent={{
                  ...parent,
                  type: parentType,
                }}
                parentMembers={parentMembers}
                existingEvent={existingEvent}
                stateValidation={stateValidation}
                showErrorList={showErrorList}
                handleChange={handleChange}
                handleAttendancesToUpsertChange={handleAttendancesToUpsertChange}
              />

              {/*Save/Cancel Buttons */}
              <div
                tw="mt-10 mb-3 space-x-4 text-center"
                css={
                  hasChanged &&
                  tw`sticky bottom-0 inline-flex flex-col items-center justify-center py-4 mx-auto border-t backdrop-blur-md bg-white/30 w-[calc(100%+32px)] -ml-4`
                }
              >
                <div tw="flex gap-4 justify-center">
                  <Button btnType="secondary" disabled={sending} onClick={() => router.push(urlBack)}>
                    {t('action.cancel')}
                  </Button>
                  <Button disabled={sending || !hasChanged} onClick={handleSubmitClick}>
                    {sending && <SpinLoader />}
                    {t('action.publish')}
                  </Button>
                </div>
                {hasChanged && <p tw="mb-0">{t('dontForgetToSaveYourChanges')}</p>}
              </div>
            </div>
          )}
        </div>
      </div>
    </Layout>
  );
};

export async function getServerSideProps(ctx) {
  const api = getApiFromCtx(ctx);
  const id = ctx.query.id;
  const parentId = ctx.query.parentId;
  const parentType = entityItem[ctx.query.parentType].back_path ?? null;
  const privacy = ctx.query?.privacy ?? 'private';
  const batchInviteMembers = ctx.query?.batchInviteMembers === 'true' ?? false;
  const { userId } = nextCookie(ctx);
  const currentUser = await api.get(`/users/${userId}`).catch((err) => null);

  let event: Event = null;
  let parent = null;

  //Someone not logged in is trying to access this page
  if (!currentUser?.data) {
    return { redirect: { destination: '/', permanent: false } };
  }

  let parentMembers: User[] = [currentUser.data];

  //Someone not logged in is trying to access this page
  if (!currentUser?.data) {
    return { redirect: { destination: '/', permanent: false } };
  }

  if (parentId) {
    parent = await api
      .get(`/${parentType ?? 'workspaces'}/${parentId}`)
      .then((res) => res.data)
      .catch((err) => console.error(err));
  }

  if (id) {
    event = await api
      .get(`/events/${id}`)
      .then((res) => res.data)
      .catch((err) => console.error(err));

    //Means that someone is trying to access this route without permission
    if (!event || event === null || !event?.permissions?.includes('manage')) {
      return { redirect: { destination: '/', permanent: false } };
    }
  } else {
    if (batchInviteMembers) {
      parentMembers = await api
        .get(`/${parentType ?? 'workspaces'}/${parentId}/members`)
        .then((res) => res.data)
        .catch((err) => console.error(err));
    }

    //Means that someone is trying to access this route without permission
    if (!parent || parent === null || !parent?.user_access?.permissions?.includes('createevents')) {
      return { redirect: { destination: '/', permanent: false } };
    }
  }

  return {
    props: {
      key: `${userId}-${id}-${parentId}`,
      event,
      parent,
      parentType,
      parentMembers,
      batchInviteMembers,
      chosenVisibility: privacy,
    },
  };
}

export default EventCreate;
