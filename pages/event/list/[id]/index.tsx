import { NextPage } from 'next';
import { getApiFromCtx } from '~/src/utils/getApi';
import { Permission } from '~/__generated__/types';
import Layout from '~/src/components/Layout/layout/layout';
import EntityEventList from '~/src/components/Event/entityEventList/entityEventList';

const EventListPage: NextPage = ({ id, permissions }: { id: string; permissions: Permission[] }) => {
  return (
    <Layout title="Events">
      <EntityEventList entityId={id} canManageEvents={permissions.includes(Permission.Createevents)} />
    </Layout>
  );
};

export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/entities/${ctx.query.id}/permissions`).catch((err) => console.error(err));
  return { props: { id: ctx.query.id, permissions: res?.data ?? [res], key: ctx.query.id } };
};

export default EventListPage;
