import React from 'react';
import { Button, Result } from 'antd';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import Layout from '~/src/components/Layout/layout/layout';
import styled from 'styled-components';
import { LockFilled } from '@ant-design/icons';

export default function Page403() {
  const { t } = useTranslation('common');
  const router = useRouter();

  const goBack = () => {
    router.back();
  };

  const goHome = () => {
    router.push('/');
  };


 const Container = styled.div`
  min-height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0 1rem;
`;

 const Title = styled.h1`
  font-size: 2.25rem;
  font-weight: bold;
`;

 const MessageContainer = styled.div`
  margin-top: 1rem;
`;

 const Message = styled.p`
  margin-bottom: 0.5rem;
`;

 const HelpText = styled(Message)`
  font-size: 0.875rem;
`;

 const ButtonContainer = styled.div`
  margin-top: 2rem;
  > button + button {
    margin-left: 1rem;
  }
`;

  return (
    <Layout noIndex>
      <Container>
        <Result
          icon={<LockFilled size={64} />}
          title={<Title>{t('403.title')}</Title>}
          subTitle={
            <MessageContainer>
              <Message>{t('403.message')}</Message>
              <HelpText>{t('403.help')}</HelpText>
            </MessageContainer>
          }
          extra={
            <ButtonContainer>
              <Button type="primary" onClick={goHome}>
                {t('home')}
              </Button>
              <Button onClick={goBack}>{t('back')}</Button>
            </ButtonContainer>
          }
        />
      </Container>
    </Layout>
  );
}
