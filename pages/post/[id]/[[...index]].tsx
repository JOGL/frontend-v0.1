import { NextPage } from 'next';
import { getApiFromCtx } from '~/src/utils/getApi';

const PostPage: NextPage = () => {
  return <> </>;
};

export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/feed/contentEntities/${ctx.query.id}/feed`).catch((err) => console.error(err));
  if (!res || !res.data) {
    return { redirect: { destination: '/search', permanent: false } };
  }
  return {
    redirect: {
      destination: `/discussion/${res.data.id}?postId=${ctx.query.id}`,
      permanent: false,
    },
  };
};

export default PostPage;
