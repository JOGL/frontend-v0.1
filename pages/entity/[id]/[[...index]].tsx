import { NextPage } from 'next';
import { getApiFromCtx } from '~/src/utils/getApi';

const EntityPage: NextPage = () => {
  return <> </>;
};

export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/communityEntities/${ctx.query.id}`).catch((err) => console.error(err));
  if (!res || !res.data) {
    return { redirect: { destination: '/search', permanent: false } };
  }
  switch (res.data.type) {
    case 'node':
      return {
        redirect: {
          destination: `/hub/${res.data.id}`,
          permanent: false,
        },
      };
    case 'workspace':
      return {
        redirect: {
          destination: `/workspace/${res.data.id}`,
          permanent: false,
        },
      };
  }

  return { redirect: { destination: '/search', permanent: false } };
};

export default EntityPage;
