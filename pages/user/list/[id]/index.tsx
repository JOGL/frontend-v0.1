import { NextPage } from 'next';
import { getApiFromCtx } from '~/src/utils/getApi';
import { Permission } from '~/__generated__/types';
import Layout from '~/src/components/Layout/layout/layout';
import EntityUserList from '~/src/components/users/entityUserList/entityUserList';

const UserListPage: NextPage = ({ id, permissions }: { id: string; permissions: Permission[] }) => {
  return (
    <Layout title="Papers">
      <EntityUserList entityId={id} canManageUsers={permissions.includes(Permission.Manage)} />
    </Layout>
  );
};

export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/entities/${ctx.query.id}/permissions`).catch((err) => console.error(err));
  return { props: { id: ctx.query.id, permissions: res?.data ?? [], key: ctx.query.id } };
};

export default UserListPage;
