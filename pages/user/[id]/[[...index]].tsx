import { NextPage } from 'next';
import { UserModel } from '~/__generated__/types';
import User from '~/src/components/Pages/user/user';

interface Props {
  myId: string | null;
  user: UserModel;
}

const UserProfilePage: NextPage = ({ user }: Props) => {
  return <User />;
};

export default UserProfilePage;
