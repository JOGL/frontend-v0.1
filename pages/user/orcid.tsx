import nextCookie from 'next-cookies';
import { NextPage } from 'next';
import Layout from '~/src/components/Layout/layout/layout';
import UserOrcid from '~/src/components/User/UserOrcid';
import { getApiFromCtx } from '~/src/utils/getApi';

const Orcid: NextPage = ({ user, code }) => {
  return (
    <Layout title="Orcid | JOGL">
      <UserOrcid user={user} code={code} />
    </Layout>
  );
};

export async function getServerSideProps({ query, ...ctx }) {
  const { authorization, userId } = nextCookie(ctx);
  const api = getApiFromCtx(ctx);

  const res = await api
    .get(`/users/${userId}`)
    .catch((err) => console.error(`Could not fetch user with id=${query.id}`, err));
  // Check if it got the user and if user is the connected user, else redirect to user page

  if (!authorization || !userId || !query.code) {
    return { redirect: { destination: `/`, permanent: false } };
  }

  if (userId) return { props: { user: res?.data, code: query.code } };

  return {};
}

export default Orcid;
