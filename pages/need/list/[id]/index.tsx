import { NextPage } from 'next';
import { getApiFromCtx } from '~/src/utils/getApi';
import { Permission } from '~/__generated__/types';
import EntityNeedList from '~/src/components/Need/entityNeedList/entityNeedList';
import Layout from '~/src/components/Layout/layout/layout';

const NeedListPage: NextPage = ({ id, permissions }: { id: string; permissions: Permission[] }) => {
  return (
    <Layout title="Needs">
      <EntityNeedList entityId={id} canManageNeeds={permissions.includes(Permission.Postneed)} />
    </Layout>
  );
};

export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/entities/${ctx.query.id}/permissions`).catch((err) => console.error(err));
  return { props: { id: ctx.query.id, permissions: res?.data ?? [], key: ctx.query.id } };
};

export default NeedListPage;
