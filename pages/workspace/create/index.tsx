import { NextPage, GetServerSidePropsContext } from 'next';
import { Permission, WorkspaceModel } from '~/__generated__/types';
import CreateWorkspace from '~/src/components/workspace/createWorkspace/createWorkspace';
import { getApiFromCtx } from '~/src/utils/getApi';

const CreateWorkspacePage: NextPage<{ workspaces: WorkspaceModel[] }> = ({ workspaces }) => {
  return <CreateWorkspace workspaces={workspaces} />;
};

export const getServerSideProps = async (ctx: GetServerSidePropsContext) => {
  const api = getApiFromCtx(ctx);
  const res = await api
    .get(`/entities/communityEntities?permission=${Permission.Createworkspaces}`)
    .catch((err) => console.error(err));

  if (res && res?.data) {
    return { props: { workspaces: res?.data } };
  }
  return { redirect: { destination: '/', permanent: false } };
};

export default CreateWorkspacePage;
