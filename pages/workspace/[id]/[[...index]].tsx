import { NextPage } from 'next';
import { WorkspaceDetailModel } from '~/__generated__/types';
import { getApiFromCtx } from '~/src/utils/getApi';
import Workspace from '~/src/components/Pages/workspace/workspace';

const WorkspacePage: NextPage = ({ workspace }: { workspace: WorkspaceDetailModel }) => {
  return <Workspace workspace={workspace} />;
};

export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/workspaces/${ctx.query.id}/detail`).catch((err) => console.error(err));
  if (res && res?.data) return { props: { workspace: { ...res.data }, key: ctx.query.id } };
  return { redirect: { destination: '/search?tab=workspaces', permanent: false } };
};

export default WorkspacePage;
