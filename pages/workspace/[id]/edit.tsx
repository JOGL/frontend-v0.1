import Icon from '~/src/components/primitives/Icon';
import { NextPage } from 'next';
import ManageFaq from '~/src/components/Tools/ManageFaq';
import ManageExternalLink from '~/src/components/Tools/ManageExternalLink';
import Layout from '~/src/components/Layout/layout/layout';
import Button from '~/src/components/primitives/Button';
import { Button as AntdButton, Space, Tag } from 'antd';
import { useApi } from '~/src/contexts/apiContext';
import useGet from '~/src/hooks/useGet';
import { Hub, Organization } from '~/src/types';
import useTranslation from 'next-translate/useTranslation';
import { getApiFromCtx } from '~/src/utils/getApi';
import { Fragment, useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/router';
import A from '~/src/components/primitives/A';
import useUser from '~/src/hooks/useUser';
import SpinLoader from '~/src/components/Tools/SpinLoader';
import { Disclosure, DisclosureButton, DisclosurePanel } from '@reach/disclosure';
import Link from 'next/link';
import { Tab, TabPanel, NavContainer, Nav, OverflowGradient } from '~/src/utils/style';
import tw from 'twin.macro';
import { confAlert, entityItem, getFullValidationObject, isAdmin, isOwner } from '~/src/utils/utils';
import ManageOnboarding from '~/src/components/Onboarding/ManageOnboarding';
import FormWysiwygComponent from '~/src/components/Tools/Forms/FormWysiwygComponent';
import HubManageAffiliation from '~/src/components/Hub/HubManageAffiliation';
import MembersManage from '~/src/components/Members/MembersManage';
import OrganizationManageAffiliation from '~/src/components/Organization/OrganizationManageAffiliation';
import { MoreTabForContainerOrProfileArchiveOrDelete } from '~/src/components/Tools/MoreTabForContainerOrProfileArchiveOrDelete';
import FormValidator from '~/src/components/Tools/Forms/FormValidator';
import workspaceFormRules from '~/src/components/workspace/workspaceFormRules/workspaceFormRules.json';
import { LeftOutlined } from '@ant-design/icons';
import { CommunityEntityInvitationModelSource, FeedType, WorkspaceDetailModel } from '~/__generated__/types';
import WorkspaceEditForm from '~/src/components/workspace/workspaceEditForm/workspaceEditForm';
import { PrivacySettingsWorkspace } from '~/src/components/workspace/privacySettingsWorkspace/privacySettingsWorkspace';
import WorkspaceManageAffiliation from '~/src/components/workspace/workspaceManageAffiliation/workspaceManageAffiliation';

interface Props {
  workspace: WorkspaceDetailModel;
}

const DEFAULT_TAB = 'general';

const WorkspaceEdit: NextPage<Props> = ({ workspace: workspaceProp }) => {
  const { t } = useTranslation('common');
  const validator = new FormValidator(workspaceFormRules, t);
  const [workspace, setWorkspace] = useState(workspaceProp);
  const [updatedWorkspace, setUpdatedWorkspace] = useState(undefined);
  const [sending, setSending] = useState(false);
  const [hasChanged, setHasChanged] = useState(false);
  const [hasErrors, setHasErrors] = useState<boolean>(false);
  const [showErrorList, setShowErrorList] = useState<boolean>(false);
  const [stateValidation, setStateValidation] = useState<any>({});
  const [selectedTab, setSelectedTab] = useState(DEFAULT_TAB);
  const [savedOrPublished, setSavedOrPublished] = useState('saved');
  const savedOrPublishedRef = useRef('saved');
  const urlBack = `/workspace/${workspace.id}`;
  const legacyApi = useApi();
  const { user } = useUser();
  const router = useRouter();
  const navRef = useRef();
  const isSubworkspace = workspace.path.some(({ entity_type }) => entity_type === FeedType.Workspace);

  savedOrPublishedRef.current = savedOrPublished;

  const { data: incomingObjInvites, mutate: incomingObjRevalidate } = useGet<CommunityEntityInvitationModelSource>(
    `/workspaces/${workspace.id}/communityEntityInvites/incoming`
  );
  const { data: outgoingObjInvites, mutate: outgoingObjRevalidate } = useGet<CommunityEntityInvitationModelSource>(
    `/workspaces/${workspace.id}/communityEntityInvites/outgoing`
  );
  const { data: hubs, mutate: hubsRevalidate } = useGet<Hub[]>(`/workspaces/${workspace.id}/nodes`);
  const { data: organizations, mutate: organizationsRevalidate } = useGet<Organization[]>(
    `/workspaces/${workspace.id}/organizations`
  );

  const handleChange: (key: any, content: any) => void = (key, content) => {
    setHasChanged(true);
    if (key === 'banner_id') {
      handleChange('banner_url', `${process.env.ADDRESS_BACK}/images/${content?.id}/full`);
      handleChange('banner_url_sm', `${process.env.ADDRESS_BACK}/images/${content?.id}/tn`);
      setUpdatedWorkspace((prevUpdatedWorkspace) => ({ ...prevUpdatedWorkspace, banner_id: content?.id ?? null }));
    } else {
      setWorkspace((prevWorkspace) => ({ ...prevWorkspace, [key]: content })); // update fields as user changes them
      // TODO: have only one place where we manage workspace content
      setUpdatedWorkspace((prevUpdatedWorkspace) => ({ ...prevUpdatedWorkspace, [key]: content })); // set an object containing only the fields/inputs that are updated by user
    }

    /* Validators start */
    const validation = validator.validate({ [key]: content });

    if (validation[key] !== undefined) {
      setStateValidation((prevState) => ({ ...prevState, [`valid_${key}`]: validation[key] }));
    }
    /* Validators end */
  };

  useEffect(() => {
    if (hasChanged) {
      const validation = validator.validate(workspace);
      if (validation.isValid) {
        setHasErrors(false);
      } else {
        setHasErrors(true);
      }
    }
  }, [workspace]);

  const handleSubmit = async (data: any = null) => {
    if (!hasErrors) {
      setShowErrorList(false);

      const verifiedData = data && !data.clientX ? data : null;
      setSending(true);
      const res = await legacyApi
        .patch(`/workspaces/${workspace.id}`, verifiedData ?? updatedWorkspace)
        .catch((err) => {
          console.error(`Couldn't patch workspace with id=${workspace.id}`, err);
          setSending(false);
        });
      if (res) {
        confAlert.fire({ icon: 'success', title: `Successfully ${savedOrPublishedRef.current}!` });
        // on success
        setSending(false);
        setUpdatedWorkspace(undefined); // reset updated workspace component
        setStateValidation({}); // reset validations
        setHasChanged(false); //reset changed status
      }
    } else {
      /*
       * Validate entire object so we get entire list of missing fields, even for fields that haven't changed.
       * This is important for displaying the correct info on the first edit, otherwise mandatory fields that haven't been touched yet will not show up on the list of missing fields.
       */
      const fullValidationObject = getFullValidationObject(validator, workspace);
      setStateValidation(fullValidationObject);

      setShowErrorList(true);
      window?.scrollTo({ top: 0, behavior: 'smooth' }); // scroll to element to show error
    }
  };

  const publish = () => {
    setSavedOrPublished('published');
    handleChange('status', 'active');
    handleSubmit({ ...updatedWorkspace, status: 'active' });
  };

  const tabs = [
    { value: 'general', translationId: 'general', icon: 'fluent:text-description-20-filled' },
    { value: 'about', translationId: 'about', icon: 'ic:outline-info' },
    {
      value: 'privacy_security',
      translationId: 'privacySecurity',
      icon: 'material-symbols:privacy-tip-outline',
    },
    { value: 'onboarding', translationId: 'onboarding', icon: 'mdi:funnel' },
    {
      value: 'ecosystem',
      translationId: 'ecosystem',
      icon: 'material-symbols:groups-outline',
      children: [
        { value: 'members', translationId: 'person.other', icon: 'mdi:people-check' },
        ...(!isSubworkspace
          ? [{ value: 'workspaces', translationId: 'workspace.other', icon: 'material-symbols:science-outline' }]
          : []),
        { value: 'hubs', translationId: 'hub.other', icon: 'carbon:edge-node-alt' },
        { value: 'organizations', translationId: 'organization.other', icon: 'carbon:location-company' },
      ],
    },
    { value: 'faq', translationId: 'faqAcronym', icon: 'octicon:question-24' },
    { value: 'more', translationId: 'more', icon: 'mdi:more-circle-outline' },
  ];

  const basicTabs = tabs.map(({ value }) => value);
  const subTabs =
    tabs
      .filter((val) => val.children?.length > 0)
      .map((child) => child.children.map(({ value }) => value))
      .flat() || [];
  const tabValues = [...basicTabs, ...subTabs];

  useEffect(() => {
    if (router.query.tab && router.query.tab !== selectedTab) {
      setSelectedTab(router.query.tab as string);
    }
  }, [router.query.tab]);

  // function when changing tab (or when router change in general)
  useEffect(() => {
    if (tabValues.includes(router.query.tab as string) && navRef?.current) {
      if (router.query.tab === 'general') {
        router.push(`/workspace/${router.query.id}/edit`, undefined, {
          shallow: true,
        });
      }
      const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      const yAdjustment = window.innerWidth < 768 ? 40 : 150; // change yAdustement depending on if it's mobile/tablet or desktop
      window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
      setSelectedTab(router.query.tab as string);
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);
  }, [router]);

  const showEditContext = () => (
    <div tw="inline-flex flex-col w-full pl-[10px] items-start justify-start md:(pl-[0px] items-start justify-start) max-md:(pt-4 pb-2)">
      <div tw="py-1 mb-1 md:hidden">
        <AntdButton type="primary" onClick={() => router.push(urlBack)} icon={<LeftOutlined />}>
          {t('action.back')}
        </AntdButton>
      </div>
      <h1 tw="font-extrabold text-[36px]">{t('action.editItem', { item: t('workspace.one') })}</h1>
      <Space>
        <span tw="text-black px-[6px] py-[3px] text-[15px] mb-2" style={{ background: entityItem.workspace.color }}>
          {t('workspace', { count: 1 })} <span tw="font-bold">{workspace?.title}</span>
        </span>
        {workspace?.status === 'draft' && <Tag>{t('draft')}</Tag>}
      </Space>
      {false && <span tw="italic text-[15px] font-[400]">{t('inThisTabYouCanEditNameBanner')}.</span>}
    </div>
  );

  const isSaveHidden = () => {
    return (
      selectedTab !== 'general' && selectedTab !== 'about' && selectedTab !== 'onboarding' && selectedTab !== 'faq'
    );
  };

  useEffect(() => {
    setWorkspace(workspaceProp);
  }, [workspaceProp]);

  return (
    <Layout title={`${workspace.title} | JOGL`} noIndex>
      <div tw="mx-auto sm:shadow-custom2">
        <div tw="mb-6 grid grid-cols-1 md:(bg-white grid-cols-[14rem calc(100% - 14rem)]) bg-white" ref={navRef}>
          {/* Header and nav (left col on desktop, top col on mobile */}
          <div tw="md:hidden">{showEditContext()}</div>
          <div tw="flex flex-col border-r-2 border-solid border-[#F7F7F9] bg-white sticky top-[64px] z-[8] md:(items-center)">
            <NavContainer>
              <OverflowGradient tw="md:hidden" gradientPosition="left" />
              <OverflowGradient tw="md:hidden" gradientPosition="right" />
              <Nav as="nav" tw="flex gap-3 pt-2.5 w-full md:(flex-col gap-0 pt-0)">
                <div tw="hidden md:(flex justify-start flex-col items-start)">
                  <div tw="p-4">
                    <AntdButton type="primary" onClick={() => router.push(urlBack)} icon={<LeftOutlined />}>
                      {t('action.back')}
                    </AntdButton>
                  </div>
                </div>
                {tabs.map((item, index) => (
                  <>
                    {!item.children ? (
                      <Link
                        shallow
                        scroll={false}
                        key={index}
                        href={`/workspace/${router.query.id}/edit${
                          item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`
                        }`}
                        legacyBehavior
                      >
                        <Tab
                          selected={item.value === selectedTab}
                          tw="pl-4 pr-1 py-3"
                          as="button"
                          id={`${item.value}-tab`}
                          css={[item.value === 'info' ? tw`md:hidden` : tw`block`]}
                        >
                          <Icon icon={item.icon} tw="mr-2" />
                          {t(item.translationId)}
                        </Tab>
                      </Link>
                    ) : (
                      <Disclosure as="div" defaultOpen={true} key={index}>
                        <DisclosureButton tw="pt-0 px-[3px] pb-[5px] md:(pl-4 pr-1 py-3 text-left)" as={Tab}>
                          <Icon icon={item.icon} tw="mr-2" />
                          {t(item.translationId)}
                        </DisclosureButton>
                        <DisclosurePanel
                          as="div"
                          tw="list-none max-md:data-[state=open]:flex max-md:[border: 1px solid lightgray] max-md:-mt-[5px] max-md:mb-[5px] gap-x-2"
                        >
                          {item.children.map((subItem) => (
                            <Fragment key={subItem.value}>
                              <Link
                                shallow
                                scroll={false}
                                key={index}
                                href={`/workspace/${router.query.id}/edit?tab=${subItem.value}`}
                                legacyBehavior
                              >
                                <Tab
                                  selected={subItem.value === selectedTab}
                                  tw="pl-7 pr-1 pt-0 pb-0! md:py-3! block w-full"
                                  as="button"
                                  id={`${subItem.value}-tab`}
                                >
                                  <Icon icon={subItem.icon} tw="mr-2" />
                                  {t(subItem.translationId)}
                                </Tab>
                              </Link>
                            </Fragment>
                          ))}
                        </DisclosurePanel>
                      </Disclosure>
                    )}
                  </>
                ))}
              </Nav>
            </NavContainer>
          </div>

          {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
          <div tw="flex flex-col px-0 pb-10 relative mt-4 md:px-4 md:pl-4 xl:pr-4">
            <div tw="max-md:hidden">{showEditContext()}</div>
            {/* General infos tab */}
            {selectedTab === 'general' && (
              <TabPanel>
                <WorkspaceEditForm
                  workspace={workspace}
                  stateValidation={stateValidation}
                  showErrorList={showErrorList}
                  handleChange={handleChange}
                />
              </TabPanel>
            )}

            {/* About tab */}
            {selectedTab === 'about' && (
              <TabPanel>
                <FormWysiwygComponent
                  id="description"
                  content={workspace.description}
                  title={t('description')}
                  placeholder={t('describeTheWorkspaceInDetail')}
                  onChange={handleChange}
                />
                <ManageExternalLink links={workspace.links} handleChange={handleChange} />
              </TabPanel>
            )}

            {/* Privacy security */}
            {selectedTab === 'privacy_security' && (
              <TabPanel>
                <PrivacySettingsWorkspace workspace={workspace} />
              </TabPanel>
            )}

            {/* Onboarding */}
            {selectedTab === 'onboarding' && (
              <TabPanel>
                {workspace.onboarding !== null && (
                  <ManageOnboarding object={workspace} objectType="workspaces" handleChange={handleChange} />
                )}
              </TabPanel>
            )}

            {/* Members tab */}
            {selectedTab === 'members' && (
              <TabPanel>
                <MembersManage
                  itemId={workspace.id}
                  itemType="workspaces"
                  isOnboardingActive={workspace.onboarding?.enabled}
                  isOwner={isOwner(workspace)}
                />
              </TabPanel>
            )}

            {/* Workspaces tab */}
            {selectedTab === 'workspaces' && (
              <TabPanel>
                <WorkspaceManageAffiliation object={workspace} objectType="workspaces" handleSubmit={handleSubmit} />
              </TabPanel>
            )}

            {/* Hubs tab */}
            {selectedTab === 'hubs' && (
              <TabPanel>
                <HubManageAffiliation
                  hubs={hubs}
                  childType="workspaces"
                  childId={workspace.id}
                  incomingObjInvites={incomingObjInvites}
                  incomingObjRevalidate={incomingObjRevalidate}
                  hubsRevalidate={hubsRevalidate}
                  outgoingObjInvites={outgoingObjInvites}
                  outgoingObjRevalidate={outgoingObjRevalidate}
                />
              </TabPanel>
            )}

            {/* Organization tab */}
            {selectedTab === 'organizations' && (
              <TabPanel>
                <OrganizationManageAffiliation
                  organizations={organizations}
                  childType="workspaces"
                  childId={workspace.id}
                  incomingObjInvites={incomingObjInvites}
                  incomingObjRevalidate={incomingObjRevalidate}
                  organizationsRevalidate={organizationsRevalidate}
                  outgoingObjInvites={outgoingObjInvites}
                  outgoingObjRevalidate={outgoingObjRevalidate}
                />
              </TabPanel>
            )}

            {/* FAQs tab */}
            {selectedTab === 'faq' && (
              <TabPanel>
                <ManageFaq faqs={workspace.faq} handleChange={handleChange} />
              </TabPanel>
            )}

            {/* Advanced tab */}
            {selectedTab === 'more' && (
              <TabPanel>
                <MoreTabForContainerOrProfileArchiveOrDelete
                  containerOrProfile={workspace}
                  setContainerOrProfile={setWorkspace}
                  handleSubmit={handleSubmit}
                  user={user}
                  apiURL="/workspaces"
                  containerType="workspace"
                  translateText="workspace.one"
                />
              </TabPanel>
            )}

            {/* Bottom action buttons */}
            {!isSaveHidden() && (
              <div
                tw="mt-10 mb-3 space-x-4 text-center inline-flex items-center self-center"
                css={
                  hasChanged &&
                  tw`sticky bottom-0 inline-flex items-center justify-center py-4 mx-auto border-t backdrop-blur-md bg-white/30 w-[calc(100%+32px)] -ml-4`
                }
              >
                <A href={urlBack}>
                  <Button btnType="secondary">{t('action.cancel')}</Button>
                </A>
                <Button
                  onClick={handleSubmit}
                  disabled={sending || !hasChanged}
                  css={
                    workspace?.status === 'draft' &&
                    tw`bg-[#C8BCE9] border-[#C8BCE9] text-black hover:text-white hover:bg-primary`
                  }
                >
                  {sending && savedOrPublishedRef.current === 'saved' && <SpinLoader />}
                  {workspace?.status === 'draft' ? t('action.saveAsDraft') : t('action.save')}
                </Button>
                {workspace?.status === 'draft' && <Button onClick={publish}>{t('action.publish')}</Button>}
                {/* {hasChanged && <p tw="mb-0">{t('dontForgetToSaveYourChanges')}</p>} */}
              </div>
            )}
          </div>
        </div>
      </div>
    </Layout>
  );
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  const res = await api
    .get(`/workspaces/${query.id}/detail`)
    .catch((err) => console.error(`Couldn't fetch workspace with id=${query.id}`, err));
  // Check if it got the workspace and if the user is admin
  if (res?.data && isAdmin(res?.data)) return { props: { workspace: { ...res?.data }, key: query.id } };
  return { redirect: { destination: `/workspace/${query.id}`, permanent: false } };
}

export default WorkspaceEdit;
