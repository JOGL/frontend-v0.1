import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import Router from 'next/router';
import React from 'react';
import Button from '~/src/components/primitives/Button';
import Head from 'next/head';
function ErrorPage({ statusCode }) {
  const { t } = useTranslation('common');

  return (
    <>
      <Head>
        <meta http-equiv="refresh" content="1" />
        {/* router.refresh() */}
      </Head>
      <div tw="bg-gray-100 flex items-center px-2 md:px-5 py-16 overflow-hidden relative justify-center h-screen lg:p-20">
        <div tw="flex-1 rounded-3xl bg-white shadow-xl p-20 text-gray-800 relative items-center text-center max-w-4xl md:(flex text-left)">
          <div tw="w-full">
            <div tw="mb-10 text-gray-600 font-light md:mb-14">
              <h1 tw="font-black uppercase text-3xl text-primary mb-10 lg:text-5xl">Error</h1>
              <p tw="italic">
                {statusCode ? `An error ${statusCode} occurred on the server.` : 'An error occurred on client.'}
              </p>
              <p>
                {t('error.500')} <a href="mailto:support@jogl.io">support[at]jogl.io</a>.
              </p>
            </div>
            <div tw="mb-20 md:mb-0 flex flex gap-4">
              <Button onClick={() => location.reload()}>Refresh page</Button>
              <Link href="/">
                <Button btnType="secondary">{t('action.backToHomePage')}</Button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

ErrorPage.getInitialProps = async ({ res, err, asPath }) => {
  // Capture 404 of pages with traling slash and redirect them
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  // If the page has a trailing slash like "/about/" it will remove the trailing slash
  // so => "/about" while keeping the URL params. This is because nextJS doesn't allow
  // trailing slashes
  // TODO: might have to change it when rewrites and redirects will be available in next.
  if (statusCode && statusCode === 404) {
    const [path, query = ''] = asPath.split('?');
    if (path.match(/\/$/)) {
      const withoutTrailingSlash = path.substr(0, path.length - 1);
      if (res) {
        res.writeHead(302, {
          Location: `${withoutTrailingSlash}${query ? `?${query}` : ''}`,
        });
        res.end();
      } else {
        Router.push(`${withoutTrailingSlash}${query ? `?${query}` : ''}`);
      }
    }
  }
  return { statusCode };
};
export default ErrorPage;
