import Icon from '~/src/components/primitives/Icon';
import { NextPage } from 'next';
import ManageFaq from '~/src/components/Tools/ManageFaq';
import ManageExternalLink from '~/src/components/Tools/ManageExternalLink';
import Layout from '~/src/components/Layout/layout/layout';
import Button from '~/src/components/primitives/Button';
import { Button as AntdButton, Space, Tag } from 'antd';
import H2 from '~/src/components/primitives/H2';
import HubForm from '~/src/components/Hub/HubForm';
import { useApi } from '~/src/contexts/apiContext';
import useGet from '~/src/hooks/useGet';
import { Organization } from '~/src/types';
import useTranslation from 'next-translate/useTranslation';
import { getApiFromCtx } from '~/src/utils/getApi';
import { Fragment, useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/router';
import A from '~/src/components/primitives/A';
import useUser from '~/src/hooks/useUser';
import SpinLoader from '~/src/components/Tools/SpinLoader';
import { Disclosure, DisclosureButton, DisclosurePanel } from '@reach/disclosure';
import Link from 'next/link';
import { Tab, TabPanel, NavContainer, Nav, OverflowGradient } from '~/src/utils/style';
import tw from 'twin.macro';
import { confAlert, entityItem, getFullValidationObject, isAdmin, isOwner } from '~/src/utils/utils';
import ManageOnboarding from '~/src/components/Onboarding/ManageOnboarding';
import FormToggleComponent from '~/src/components/Tools/Forms/FormToggleComponent';
import FormWysiwygComponent from '~/src/components/Tools/Forms/FormWysiwygComponent';
import MembersManage from '~/src/components/Members/MembersManage';
import OrganizationManageAffiliation from '~/src/components/Organization/OrganizationManageAffiliation';
import { useHeaderStore } from '~/src/components/Header/HeaderStore';
import { MoreTabForContainerOrProfileArchiveOrDelete } from '~/src/components/Tools/MoreTabForContainerOrProfileArchiveOrDelete';
import FormValidator from '~/src/components/Tools/Forms/FormValidator';
import hubFormRules from '~/src/components/Hub/HubFormRules.json';
import { LeftOutlined } from '@ant-design/icons';
import { CommunityEntityInvitationModelSource, NodeModel, Permission } from '~/__generated__/types';
import { PrivacySettingsHub } from '~/src/components/privacySettingsHub/privacySettingsHub';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';
import WorkspaceManageAffiliation from '~/src/components/workspace/workspaceManageAffiliation/workspaceManageAffiliation';

interface Props {
  hub: NodeModel;
}

const DEFAULT_TAB = 'general';

const HubEdit: NextPage<Props> = ({ hub: hubProp }) => {
  const { t } = useTranslation('common');
  const validator = new FormValidator(hubFormRules, t);
  const [hub, setHub] = useState(hubProp);
  const [updatedHub, setUpdatedHub] = useState(undefined);
  const [sending, setSending] = useState(false);
  const [hasChanged, setHasChanged] = useState(false);
  const [hasErrors, setHasErrors] = useState<boolean>(false);
  const [showErrorList, setShowErrorList] = useState<boolean>(false);
  const [stateValidation, setStateValidation] = useState<any>({});
  const [selectedTab, setSelectedTab] = useState(DEFAULT_TAB);
  const [savedOrPublished, setSavedOrPublished] = useState('saved');
  const savedOrPublishedRef = useRef('saved');
  const urlBack = `/hub/${hub.id}/${hub.short_title}`;
  const legacyApi = useApi();
  const { user } = useUser();
  const router = useRouter();
  const navRef = useRef();
  const { setHeaderState, containerId } = useHeaderStore();
  const { selectedHubDiscussion, setSelectedHubDiscussion } = useHubDiscussionsStore((store) => ({
    selectedHubDiscussion: store?.selectedHubDiscussion,
    setSelectedHubDiscussion: store.setSelectedHubDiscussion,
  }));
  savedOrPublishedRef.current = savedOrPublished;

  if (containerId !== hub?.id) {
    setHeaderState({
      containerId: hub?.id,
      containerTitle: hub?.title,
      containerType: 'nodes',
      containerLogo: hub?.logo_url_sm ?? hub.banner_url_sm ?? '/images/logo_single.svg',
      canCreateNeed: hub?.user_access?.permissions?.includes(Permission.Postneed),
      canCreateJOGLDoc: hub?.user_access?.permissions?.includes(Permission.Postcontententity),
      canCreateCfp: hub?.user_access?.permissions?.includes(Permission.Manage),
      canCreateEvents: hub?.user_access?.permissions?.includes(Permission.Createevents),
    });
  }

  const { data: incomingObjInvites, mutate: incomingObjRevalidate } = useGet<CommunityEntityInvitationModelSource[]>(
    `/nodes/${hub.id}/communityEntityInvites/incoming`
  );
  const { data: outgoingObjInvites, mutate: outgoingObjRevalidate } = useGet<CommunityEntityInvitationModelSource[]>(
    `/nodes/${hub.id}/communityEntityInvites/outgoing`
  );
  const { data: organizations, mutate: organizationsRevalidate } = useGet<Organization[]>(
    `/nodes/${hub.id}/organizations`
  );

  const handleChange: (key: any, content: any) => void = (key, content) => {
    setHasChanged(true);
    if (key === 'banner_id') {
      handleChange('banner_url', `${process.env.ADDRESS_BACK}/images/${content?.id}/full`);
      handleChange('banner_url_sm', `${process.env.ADDRESS_BACK}/images/${content?.id}/tn`);
      setUpdatedHub((prevUpdatedHub) => ({ ...prevUpdatedHub, banner_id: content?.id ?? null }));
    } else if (key === 'logo_id') {
      handleChange('logo_url', `${process.env.ADDRESS_BACK}/images/${content?.id}/full`);
      handleChange('logo_url_sm', `${process.env.ADDRESS_BACK}/images/${content?.id}/tn`);
      setUpdatedHub((prevUpdatedHub) => ({ ...prevUpdatedHub, logo_id: content?.id ?? null }));
    } else {
      setHub((prevHub) => ({ ...prevHub, [key]: content })); // update fields as user changes them
      // TODO: have only one place where we manage hub content
      setUpdatedHub((prevUpdatedHub) => ({ ...prevUpdatedHub, [key]: content })); // set an object containing only the fields/inputs that are updated by user
    }

    /* Validators start */
    const validation = validator.validate({ [key]: content });

    if (validation[key] !== undefined) {
      setStateValidation((prevState) => ({ ...prevState, [`valid_${key}`]: validation[key] }));
    }
    /* Validators end */
  };

  useEffect(() => {
    if (hasChanged) {
      const validation = validator.validate(hub);
      if (validation.isValid) {
        setHasErrors(false);
      } else {
        setHasErrors(true);
      }
    }
  }, [hub]);

  useEffect(() => {
    // if we just created the hub, set it globally
    if (router.query.setNewHub) setSelectedHubDiscussion(hub);
  }, []);

  const handleSubmit = async (data: any = null) => {
    if (!hasErrors) {
      setShowErrorList(false);
      const verifiedData = data && !data.clientX ? data : null;
      setSending(true);
      const res = await legacyApi.patch(`/nodes/${hub.id}`, verifiedData ?? updatedHub).catch((err) => {
        console.error(`Couldn't patch hub with id=${hub.id}`, err);
        setSending(false);
      });
      if (res) {
        confAlert.fire({ icon: 'success', title: `Successfully ${savedOrPublishedRef.current}!` });
        // on success
        setSending(false);
        setUpdatedHub(undefined); // reset updated hub component
        setStateValidation({}); // reset validations
        setHasChanged(false); //reset changed status
        // update global hub with updated hub values if we're editing the current selected global hub
        if (selectedHubDiscussion?.id === hub.id) {
          setSelectedHubDiscussion({ ...selectedHubDiscussion, ...updatedHub });
        }
      }
    } else {
      /*
       * Validate entire object so we get entire list of missing fields, even for fields that haven't changed.
       * This is important for displaying the correct info on the first edit, otherwise mandatory fields that haven't been touched yet will not show up on the list of missing fields.
       */
      const fullValidationObject = getFullValidationObject(validator, hub);
      setStateValidation(fullValidationObject);

      setShowErrorList(true);
      window?.scrollTo({ top: 0, behavior: 'smooth' }); // scroll to element to show error
    }
  };

  const publish = () => {
    setSavedOrPublished('published');
    handleChange('status', 'active');
    handleSubmit({ ...updatedHub, status: 'active' });
  };

  const handleChangeManagementSettings = (id) => {
    const newArr = hub.management.includes(id) ? hub.management.filter((x) => x !== id) : [...hub.management, id];
    handleChange('management', newArr);
    handleSubmit({ management: newArr });
  };

  const tabs = [
    { value: 'general', translationId: 'general', icon: 'fluent:text-description-20-filled' },
    { value: 'about', translationId: 'about', icon: 'ic:outline-info' },
    {
      value: 'privacy_security',
      translationId: 'privacySecurity',
      icon: 'material-symbols:privacy-tip-outline',
    },
    { value: 'onboarding', translationId: 'onboarding', icon: 'mdi:funnel' },
    {
      value: 'ecosystem',
      translationId: 'ecosystem',
      icon: 'material-symbols:groups-outline',
      children: [
        { value: 'members', translationId: 'person.other', icon: 'mdi:people-check' },
        { value: 'workspaces', translationId: 'workspace.other', icon: 'material-symbols:groups-outline' },
        { value: 'organizations', translationId: 'organization.other', icon: 'carbon:location-company' },
      ],
    },
    // { value: 'publications', translationId: 'paper.other', icon: 'ri:article-line' },
    // { value: 'resources', translationId: 'resource.other', icon: 'carbon:software-resource-cluster' },
    // { value: 'callForProposal', translationId: 'callForProposals.one', icon: 'bxs:medal' },
    { value: 'faq', translationId: 'faqAcronym', icon: 'octicon:question-24' },
    { value: 'more', translationId: 'more', icon: 'mdi:more-circle-outline' },
  ];

  const basicTabs = tabs.map(({ value }) => value);
  const subTabs =
    tabs
      .filter((val) => val.children?.length > 0)
      .map((child) => child.children.map(({ value }) => value))
      .flat() || [];
  const tabValues = [...basicTabs, ...subTabs];

  useEffect(() => {
    // on first load, check if url has a tab param, and if it does, select this tab
    if (router.query.tab) {
      setSelectedTab(router.query.tab as string);
      const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      const yAdjustment = window.innerWidth < 768 ? 40 : 150; // change yAdustement depending on if it's mobile/tablet or desktop
      window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
    }
  }, []);

  // function when changing tab (or when router change in general)
  useEffect(() => {
    if (tabValues.includes(router.query.tab as string) && navRef?.current) {
      if (router.query.tab === 'general') {
        router.push(`/hub/${router.query.id}/edit`, undefined, {
          shallow: true,
        });
      }
      const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      const yAdjustment = window.innerWidth < 768 ? 40 : 150; // change yAdustement depending on if it's mobile/tablet or desktop
      window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
      setSelectedTab(router.query.tab as string);
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);
  }, [router]);

  const showEditContext = () => (
    <div tw="inline-flex flex-col w-full pl-[10px] items-start justify-start md:(pl-[0px] items-start justify-start) max-md:(pt-4 pb-2)">
      <div tw="py-1 mb-1 md:hidden">
        <AntdButton type="primary" onClick={() => router.push(urlBack)} icon={<LeftOutlined />}>
          {t('action.back')}
        </AntdButton>
      </div>
      <h1 tw="font-extrabold text-[36px]">{t('action.editTheHub')}</h1>
      <Space>
        <span tw="text-black px-[6px] py-[3px] text-[15px] mb-2" style={{ background: entityItem.node.color }}>
          {t('hub', { count: 1 })} <span tw="font-bold">{hub?.title}</span>
        </span>
        {hub?.status === 'draft' && <Tag>Draft</Tag>}
      </Space>
      {false && <span tw="italic text-[15px] font-[400]">In this tab you can edit name, banner etc.</span>}
    </div>
  );

  const isSaveHidden = () => {
    return (
      selectedTab !== 'general' && selectedTab !== 'about' && selectedTab !== 'onboarding' && selectedTab !== 'faq'
    );
  };

  useEffect(() => {
    setHub(hubProp);
  }, [hubProp]);

  return (
    <Layout title={`${hub.title} | JOGL`} noIndex>
      <div tw="mx-auto sm:shadow-custom2">
        <div tw="mb-6 grid grid-cols-1 md:(bg-white grid-cols-[14rem calc(100% - 14rem)]) bg-white" ref={navRef}>
          {/* Header and nav (left col on desktop, top col on mobile */}
          <div tw="md:hidden">{showEditContext()}</div>
          <div tw="flex flex-col border-r-2 border-solid border-[#F7F7F9] bg-white sticky top-[64px] z-[8] md:(items-center)">
            <NavContainer>
              <OverflowGradient tw="md:hidden" gradientPosition="left" />
              <OverflowGradient tw="md:hidden" gradientPosition="right" />
              <Nav as="nav" tw="flex gap-3 pt-2.5 w-full md:(flex-col gap-0 pt-0)">
                <div tw="hidden md:(flex justify-start flex-col items-start)">
                  <div tw="p-4">
                    <AntdButton type="primary" onClick={() => router.push(urlBack)} icon={<LeftOutlined />}>
                      {t('action.back')}
                    </AntdButton>
                  </div>
                </div>
                {tabs.map((item, index) => (
                  <>
                    {!item.children ? (
                      <Link
                        shallow
                        scroll={false}
                        key={index}
                        href={`/hub/${router.query.id}/edit${item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`}`}
                        legacyBehavior
                      >
                        <Tab
                          selected={item.value === selectedTab}
                          tw="pl-4 pr-1 py-3"
                          as="button"
                          id={`${item.value}-tab`}
                          css={[item.value === 'info' ? tw`md:hidden` : tw`block`]}
                        >
                          <Icon icon={item.icon} tw="mr-2" />
                          {t(item.translationId)}
                        </Tab>
                      </Link>
                    ) : (
                      <Disclosure as="div" defaultOpen={true}>
                        <DisclosureButton tw="pt-0 px-[3px] pb-[5px] md:(pl-4 pr-1 py-3 text-left)" as={Tab}>
                          <Icon icon={item.icon} tw="mr-2" />
                          {t(item.translationId)}
                        </DisclosureButton>
                        <DisclosurePanel
                          as="div"
                          tw="list-none max-md:data-[state=open]:flex max-md:[border: 1px solid lightgray] max-md:-mt-[5px] max-md:mb-[5px] gap-x-2"
                        >
                          {item.children.map((subItem) => (
                            <Fragment key={subItem.value}>
                              <Link
                                shallow
                                scroll={false}
                                key={index}
                                href={`/hub/${router.query.id}/edit?tab=${subItem.value}`}
                                legacyBehavior
                              >
                                <Tab
                                  selected={subItem.value === selectedTab}
                                  tw="pl-7 pr-1 pt-0 pb-0! md:py-3! block w-full"
                                  as="button"
                                  id={`${subItem.value}-tab`}
                                >
                                  <Icon icon={subItem.icon} tw="mr-2" />
                                  {t(subItem.translationId)}
                                </Tab>
                              </Link>
                            </Fragment>
                          ))}
                        </DisclosurePanel>
                      </Disclosure>
                    )}
                  </>
                ))}
              </Nav>
            </NavContainer>
          </div>

          {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
          <div tw="flex flex-col px-0 pb-10 mt-4 relative md:px-4 md:(pl-4) xl:(pr-4)">
            <div tw="max-md:hidden">{showEditContext()}</div>
            {/* General infos tab */}
            {selectedTab === 'general' && (
              <TabPanel>
                <HubForm
                  hub={hub}
                  handleChange={handleChange}
                  stateValidation={stateValidation}
                  showErrorList={showErrorList}
                />
              </TabPanel>
            )}

            {/* About tab */}
            {selectedTab === 'about' && (
              <TabPanel>
                <FormWysiwygComponent
                  id="description"
                  content={hub.description}
                  title={t('descriptionShownOnHubExplorePage')}
                  placeholder={t('describeYourHubInDetail')}
                  onChange={handleChange}
                />
                <ManageExternalLink links={hub.links} handleChange={handleChange} />
              </TabPanel>
            )}

            {/* Feed/discussions */}
            {selectedTab === 'feed' && (
              <TabPanel>
                <p>
                  In this tab, you can manage the resources of your hub. Members can benefit from the resources made
                  available only by participating in calls and then being validated by the admins.
                </p>

                <H2>Wall management</H2>
                <h5>Posts on the feed</h5>
                <FormToggleComponent
                  id="content_entities_created_by_any_member"
                  title="Allow members to create posts"
                  choice1={t('no')}
                  choice2={t('yes')}
                  isChecked={hub.management.includes('content_entities_created_by_any_member')}
                  onChange={handleChangeManagementSettings}
                />
                <FormToggleComponent
                  id="content_entities_deleted_by_admins"
                  title="Allow admins to delete posts/comments"
                  choice1={t('no')}
                  choice2={t('yes')}
                  isChecked={hub.management.includes('content_entities_deleted_by_admins')}
                  onChange={handleChangeManagementSettings}
                />
              </TabPanel>
            )}

            {/* Privacy security */}
            {selectedTab === 'privacy_security' && (
              <TabPanel>
                <PrivacySettingsHub hub={hub} />
              </TabPanel>
            )}

            {selectedTab === 'onboarding' && (
              <TabPanel>
                {hub.onboarding !== null && (
                  <ManageOnboarding object={hub} objectType="nodes" handleChange={handleChange} />
                )}
              </TabPanel>
            )}

            {/* Members tab */}
            {selectedTab === 'members' && (
              <TabPanel>
                <MembersManage
                  itemId={hub.id}
                  itemType="nodes"
                  isOnboardingActive={hub.onboarding?.enabled}
                  isOwner={isOwner(hub)}
                />
              </TabPanel>
            )}

            {/* Workspaces */}
            {selectedTab === 'workspaces' && (
              <TabPanel>
                <WorkspaceManageAffiliation object={hub} objectType="nodes" handleSubmit={handleSubmit} />
              </TabPanel>
            )}

            {/* Organization tab */}
            {selectedTab === 'organizations' && (
              <TabPanel>
                <OrganizationManageAffiliation
                  organizations={organizations}
                  childType="nodes"
                  childId={hub.id}
                  incomingObjInvites={incomingObjInvites}
                  incomingObjRevalidate={incomingObjRevalidate}
                  organizationsRevalidate={organizationsRevalidate}
                  outgoingObjInvites={outgoingObjInvites}
                  outgoingObjRevalidate={outgoingObjRevalidate}
                />
              </TabPanel>
            )}

            {/* FAQs tab */}
            {selectedTab === 'faq' && (
              <TabPanel>
                <ManageFaq faqs={hub.faq} handleChange={handleChange} />
              </TabPanel>
            )}

            {/* More tab */}
            {selectedTab === 'more' && (
              <TabPanel>
                <MoreTabForContainerOrProfileArchiveOrDelete
                  containerOrProfile={hub}
                  setContainerOrProfile={setHub}
                  handleSubmit={handleSubmit}
                  user={user}
                  apiURL="/nodes"
                  containerType="hub"
                  translateText="hub.one"
                />
              </TabPanel>
            )}

            {/* Bottom action buttons */}
            {!isSaveHidden() && (
              <div
                tw="mt-10 mb-3 space-x-4 text-center inline-flex items-center self-center"
                css={
                  hasChanged &&
                  tw`sticky bottom-0 inline-flex items-center justify-center py-4 mx-auto border-t backdrop-blur-md bg-white/30 w-[calc(100%+32px)] -ml-4`
                }
              >
                <A href={urlBack}>
                  <Button btnType="secondary">{t('action.cancel')}</Button>
                </A>
                <Button
                  onClick={handleSubmit}
                  disabled={sending || !hasChanged}
                  css={
                    hub?.status === 'draft' &&
                    tw`bg-[#C8BCE9] border-[#C8BCE9] text-black hover:text-white hover:bg-primary`
                  }
                >
                  {sending && savedOrPublishedRef.current === 'saved' && <SpinLoader />}
                  {hub?.status === 'draft' ? t('action.saveAsDraft') : t('action.save')}
                </Button>
                {hub?.status === 'draft' && <Button onClick={publish}>Publish</Button>}
                {/* {hasChanged && <p tw="mb-0">{t('dontForgetToSaveYourChanges')}</p>} */}
              </div>
            )}
          </div>
        </div>
      </div>
    </Layout>
  );
};

export async function getServerSideProps(ctx) {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/nodes/${ctx.query.id}`).catch((err) => {
    // console.error(`Couldn't fetch hub with id=${query.id}`, err)
    return null;
  });
  // Check if it got the hub and if the user is admin
  if (res?.data && isAdmin(res?.data)) return { props: { hub: { ...res?.data }, key: ctx.query.id } };
  return { redirect: { destination: `/hub/${ctx.query.id}`, permanent: false } };
}

export default HubEdit;
