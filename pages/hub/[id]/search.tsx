import { NextPage } from 'next';
import { NodeDetailModel } from '~/__generated__/types';
import HubSearch from '~/src/components/Pages/hubSearch/hubSearch';
import { getApiFromCtx } from '~/src/utils/getApi';

interface Props {
  hub: NodeDetailModel;
  initial: string;
}
const HubSearchPage: NextPage = ({ hub, initial }: Props) => {
  return <HubSearch hub={hub} initial={initial} />;
};

export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/nodes/${ctx.query.id}/detail`).catch((err) => console.error(err));
  if (res && res?.data) return { props: { hub: { ...res.data }, initial: ctx.query.initial, key: ctx.query.id } };
  return { redirect: { destination: '/search?tab=hubs', permanent: false } };
};

export default HubSearchPage;
