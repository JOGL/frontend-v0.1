import { NextPage } from 'next';
import { useRouter } from 'next/router';
import React, { useEffect, useMemo, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Layout from '~/src/components/Layout/layout/layout';
import HubHeader from '~/src/components/hubHeader/hubHeader';
import { getApiFromCtx } from '~/src/utils/getApi';
import 'intro.js/introjs.css';
import { TabPanel } from '~/src/utils/style';
import { isMember } from '~/src/utils/utils';
import ShowCfps from '~/src/components/Tools/ShowCfps';
import ResourceList from '~/src/components/Resources/ResourceList';
import { useStonePathStore } from '~/src/contexts/stonepath/StonePathStoreProvider';
import { ShowHubWorkspaces } from '~/src/components/Hub/ShowHubWorkspaces';
import { useModal } from '~/src/contexts/modalContext';
import useUser from '~/src/hooks/useUser';
import { HorizontalTabs } from '~/src/components/Tools/spaceTabs/horizontalTabs/horizontalTabs';
import { DiscussionsListHub } from '~/src/components/discussions/hub/hub';
import ShowMembers from '~/src/components/Tools/ShowMembers';
import { AppstoreAddOutlined, CompassOutlined, FileTextOutlined, ReadOutlined } from '@ant-design/icons';
import { VerticalTabs } from '~/src/components/Tools/spaceTabs/verticalTabs/verticalTabs';
import { SpaceTab } from '~/src/components/Tools/spaceTabs/spaceTabs';
import { Grid } from 'antd';
import { NodeDetailModel, Permission } from '~/__generated__/types';
import { PageWrapperStyled } from '~/src/components/Hub/hub.styles';
import EntityDocumentList from '~/src/components/documents/entityDocumentList/entityDocumentList';
import EntityNeedList from '~/src/components/Need/entityNeedList/entityNeedList';
import EntityPaperList from '~/src/components/papers/entityPaperList/entityPaperList';
import EntityEventList from '~/src/components/Event/entityEventList/entityEventList';

const { useBreakpoint } = Grid;

const DEFAULT_TAB = 'feed';
const verticalTabsList = ['documents', 'library', 'needs', 'resources'];

const HubDetails: NextPage<{ hub: NodeDetailModel }> = ({ hub }) => {
  const { t } = useTranslation('common');
  const { user } = useUser();
  const router = useRouter();
  const { showModal } = useModal();

  const screens = useBreakpoint();

  const limitedVisibility = hub.content_privacy === 'private' && !isMember(hub);
  const { setStonePath } = useStonePathStore();
  const isAdmin = hub.user_access?.permissions?.includes(Permission.Manage);
  const hasPapers = hub.stats?.papers_count_aggregate && hub.stats.papers_count_aggregate > 0;
  const hasNeeds = hub.stats?.needs_count_aggregate && hub.stats.needs_count_aggregate > 0;
  const hasResources = hub.stats?.resources_count && hub.stats.resources_count > 0;

  const showLibraryTab = !!(hub.management?.includes('library_managed_by_any_member') || hasPapers || isAdmin);
  const showDocumentsTab = hub.tabs?.includes('documents') && hub?.user_access?.permissions.includes(Permission.Read);
  const showNeedsTab = !!(hub.management?.includes('needs_created_by_any_member') || hasNeeds || isAdmin);
  const showResourcesTab = !!(hub.management?.includes('resources_created_by_any_member') || hasResources || isAdmin);

  const showResourcesTabDisclaimerToAdmin = isAdmin && !hasResources;

  const horizontalTabs: SpaceTab[] = useMemo(() => {
    return [
      { value: 'feed', translationId: 'discussion.one', icon: 'material-symbols:chat-outline' },
      { value: 'workspaces', translationId: 'workspace.other', icon: 'gravity-ui:nodes-down' },
      { value: 'events', translationId: 'calendar', icon: 'material-symbols:event-note-outline' },
      ...(hub.stats?.cfp_count > 0
        ? [{ value: 'cfps', translationId: 'callForProposals.other', icon: 'bx:medal' }]
        : []),
      {
        value: 'members',
        translationId: 'person.other',
        icon: 'mdi:people-check',
        extraLabel: `${hub.stats?.members_count}`,
      },
    ];
  }, [hub]);

  const verticalTabs: SpaceTab[] = useMemo(() => {
    return [
      ...(showDocumentsTab
        ? [{ value: 'documents', translationId: 'document.other', icon: <FileTextOutlined /> }]
        : []),
      ...(showLibraryTab ? [{ value: 'library', translationId: 'library', icon: <ReadOutlined /> }] : []),
      ...(showNeedsTab ? [{ value: 'needs', translationId: 'need.other', icon: <AppstoreAddOutlined /> }] : []),
      ...(showResourcesTab ? [{ value: 'resources', translationId: 'resource.other', icon: <CompassOutlined /> }] : []),
    ];
  }, [showDocumentsTab, showLibraryTab, showNeedsTab, showResourcesTab]);

  const allTabs = [...horizontalTabs, ...verticalTabs];
  const isAllowedToViewSelectedTab = allTabs.some((tab) => tab.value === router.query.tab);
  const [selectedTab, setSelectedTab] = useState(
    isAllowedToViewSelectedTab ? (router.query.tab as string) : DEFAULT_TAB
  );

  useEffect(() => {
    setStonePath(hub.path);
  }, [hub, setStonePath]);

  // function when changing tab (or when router change in general)
  useEffect(() => {
    if (allTabs.map((tab: SpaceTab) => tab.value).includes(router.query.tab as string)) {
      if (router.query.tab === DEFAULT_TAB) {
        router.replace(`/hub/${router.query.id}`, undefined, {
          shallow: true,
        });
      }
      setSelectedTab(router.query.tab as string);
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router]);

  const showVerticalTab = verticalTabsList.includes(selectedTab);

  return (
    <Layout
      title={hub?.title && `${hub.title} | JOGL`}
      desc={hub?.short_description}
      img={hub?.banner_url || '/images/default/default-hub.png'}
      noIndex={hub?.status === 'draft' || hub?.listing_privacy !== 'public'}
    >
      <div>
        <HubHeader hub={hub} limitedVisibility={limitedVisibility} />

        {!limitedVisibility && (
          <div>
            {
              <HorizontalTabs
                containerType="hub"
                tabs={horizontalTabs}
                verticalTabs={verticalTabs}
                defaultTab={DEFAULT_TAB}
                selectedTab={selectedTab}
                verticalTabSelected={showVerticalTab}
              />
            }

            <PageWrapperStyled gap="small">
              {screens.md && showVerticalTab && (
                <VerticalTabs
                  containerType="hub"
                  tabs={verticalTabs}
                  defaultTab={DEFAULT_TAB}
                  selectedTab={selectedTab}
                />
              )}
              {selectedTab === 'feed' && (
                <TabPanel id="feed">
                  <DiscussionsListHub
                    hubId={hub.id}
                    canCreate={hub?.user_access?.permissions?.includes(Permission.Createchannels)}
                  />
                </TabPanel>
              )}

              {selectedTab === 'workspaces' && (
                <TabPanel id="workspaces">
                  <ShowHubWorkspaces hub={hub} />
                </TabPanel>
              )}

              {selectedTab === 'events' && (
                <TabPanel id="events">
                  <EntityEventList
                    entityId={hub.id}
                    canManageEvents={hub.user_access?.permissions?.includes(Permission.Createevents)}
                  />
                </TabPanel>
              )}

              {selectedTab === 'cfps' && (
                <TabPanel id="cfps">
                  <ShowCfps parentId={hub.id} parentType="nodes" isAdmin={isAdmin} />
                </TabPanel>
              )}

              {selectedTab === 'members' && (
                <TabPanel id="members">
                  <ShowMembers parentType="nodes" parentId={hub.id} canCreate={isAdmin} />
                </TabPanel>
              )}

              {selectedTab === 'documents' && (
                <TabPanel id="documents">
                  <EntityDocumentList
                    entityId={hub.id}
                    canManageDocuments={hub.user_access.permissions.includes(Permission.Managedocuments)}
                  />
                </TabPanel>
              )}

              {selectedTab === 'library' && (
                <TabPanel id="library">
                  <EntityPaperList
                    entityId={hub.id}
                    canManagePapers={hub.user_access.permissions.includes(Permission.Managelibrary)}
                  />
                </TabPanel>
              )}

              {selectedTab === 'needs' && (
                <TabPanel id="needs">
                  <EntityNeedList
                    entityId={hub.id}
                    canManageNeeds={hub.user_access.permissions.includes(Permission.Postneed)}
                  />
                </TabPanel>
              )}

              {selectedTab === 'resources' && (
                <TabPanel id="resources">
                  <ResourceList
                    itemType="nodes"
                    entity={hub}
                    isAdmin={isAdmin}
                    showResourcesTabDisclaimerToAdmin={showResourcesTabDisclaimerToAdmin}
                  />
                </TabPanel>
              )}
            </PageWrapperStyled>
          </div>
        )}
      </div>
    </Layout>
  );
};

export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/nodes/${ctx.query.id}/detail`).catch((err) => console.error(err));
  if (res && res?.data) return { props: { hub: { ...res.data }, key: ctx.query.id } };
  return { redirect: { destination: '/search?tab=hubs', permanent: false } };
};

export default HubDetails;
