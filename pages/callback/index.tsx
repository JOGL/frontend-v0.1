import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import Loader from '~/src/components/Common/loader/loader';

const CallbackPage: NextPage = () => {
  const router = useRouter();

  useEffect(() => {
   const code = router.query.code; 
    if (code) {
      localStorage.setItem('oauth_code', code as string);
      window.close();
    }
  }, [router.query.code]);
  
  return <Loader/>;
};
export default CallbackPage
