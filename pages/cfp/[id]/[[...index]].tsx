import { NextPage } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { Fragment, useEffect, useRef, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Layout from '~/src/components/Layout/layout/layout';
import CfpAbout from '~/src/components/Cfp/CfpAbout';
import CfpHeader from '~/src/components/Cfp/CfpHeader';
import { Cfp } from '~/src/types';
import { getApiFromCtx } from '~/src/utils/getApi';
import tw from 'twin.macro';
import 'intro.js/introjs.css';
import CfpInfo from '~/src/components/Cfp/CfpInfo';
import { Tab, TabPanel, NavContainer, Nav, OverflowGradient } from '~/src/utils/style';
import { displayObjectUTCDate, entityItem, isAdmin, isMember } from '~/src/utils/utils';
import Icon from '~/src/components/primitives/Icon';
import { Disclosure, DisclosureButton, DisclosurePanel } from '@reach/disclosure';
import ShowMembers from '~/src/components/Tools/ShowMembers';
import InfoHtmlComponent from '~/src/components/Tools/Info/InfoHtmlComponent';
import H2 from '~/src/components/primitives/H2';
import CfpProposals from '~/src/components/Cfp/CfpProposals';
import ResourceList from '~/src/components/Resources/ResourceList';
import ShowSkillSdg from '~/src/components/Tools/ShowSkillSdg';
import ShowLinks from '~/src/components/Tools/ShowLinks';
import { useStonePathStore } from '~/src/contexts/stonepath/StonePathStoreProvider';
import Feed from '~/src/components/discussionFeed/feed/feed';
import EntityDocumentList from '~/src/components/documents/entityDocumentList/entityDocumentList';
import { Permission } from '~/__generated__/types';

const CfpDetails: NextPage<{ cfp: Cfp }> = ({ cfp }) => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const DEFAULT_TAB = isMember(cfp) ? 'feed' : 'about';
  const [selectedTab, setSelectedTab] = useState(DEFAULT_TAB);
  const headerRef = useRef<HTMLDivElement>();
  const limitedVisibility = cfp.content_privacy === 'private' && !isMember(cfp);
  const [shouldRefreshResources, setShouldRefreshResources] = useState<boolean>(false);

  const { setStonePath } = useStonePathStore();

  const tabs = [
    { value: 'info', translationId: 'info', icon: 'ic:outline-info' },
    { value: 'feed', translationId: 'discussion.one', icon: 'material-symbols:chat-outline' },
    { value: 'about', translationId: 'about', icon: 'ic:outline-info' },
    { value: 'rules', translationId: 'rule.other', icon: 'carbon:rule' },
    { value: 'resources', translationId: 'resource.other', icon: 'carbon:software-resource-cluster' },
    { value: 'members', translationId: 'person.other', icon: 'mdi:people-check' },
    { value: 'proposals', translationId: 'proposal.other', icon: 'bx:medal' },
    ...(cfp?.faq?.length !== 0 ? [{ value: 'faq', translationId: 'faqAcronym', icon: 'octicon:question-24' }] : []),
  ];

  useEffect(() => {
    setStonePath(cfp?.path ?? []);
  }, [cfp, setStonePath]);

  const basicTabs = tabs.map(({ value }) => value);
  const subTabs =
    tabs
      .filter((val) => val.childrens?.length > 0)
      .map((x) => x.childrens.map(({ value }) => value))
      .flat() || [];
  const tabValues = [...basicTabs, ...subTabs];

  useEffect(() => {
    // on first load, check if url has a tab param, and if it does, select this tab
    if (router.query.tab) {
      setSelectedTab(router.query.tab as string);
    }
  }, []);

  // function when changing tab (or when router change in general)
  useEffect(() => {
    if (tabValues.includes(router.query.tab as string)) {
      if (router.query.tab === DEFAULT_TAB) {
        router.replace(`/cfp/${router.query.id}`, undefined, {
          shallow: true,
        });
      }
      setSelectedTab(router.query.tab as string);
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);
  }, [router]);

  const refreshContent = () => {
    switch (selectedTab) {
      case 'resources':
        setShouldRefreshResources(!shouldRefreshResources);
        break;
      default:
        return;
    }
  };

  const displayTagsAndLinks = () => (
    <div tw="max-md:mt-7">
      {(cfp.interests?.length > 0 || cfp.keywords?.length > 0) && (
        <ShowSkillSdg interests={cfp.interests} skills={cfp.keywords} />
      )}
      {cfp?.links && cfp?.links.length !== 0 && <ShowLinks links={cfp.links} />}
    </div>
  );

  return (
    <Layout
      title={cfp?.title && `${cfp.title} | JOGL`}
      desc={cfp?.short_description}
      img={cfp?.banner_url || '/images/default/default-cfp.png'}
      noIndex={cfp?.status === 'draft'}
    >
      <div tw="sm:shadow-custom2">
        <div
          tw="w-full px-3 py-1 flex justify-between md:text-[18px]"
          ref={headerRef}
          style={{ background: entityItem.cfp.color }}
        >
          <span tw="font-bold">{t('callForProposals', { count: 1 })}</span>
          <span>{t(`legacy.role.${cfp.user_access_level}`)}</span>
        </div>

        <CfpHeader cfp={cfp} limitedVisibility={limitedVisibility} refresh={refreshContent} />
        <span tw="max-md:hidden">{displayTagsAndLinks()}</span>

        {/* ---- Page grid starts here ---- */}
        {!limitedVisibility && (
          <div tw="mb-6 grid grid-cols-1 md:(bg-white grid-cols-[14rem calc(100% - 14rem)]) bg-white border-t border-solid border-gray-200">
            {/* Header and nav (left col on desktop, top col on mobile */}
            <div tw="flex flex-col border-r-2 border-solid border-[#F7F7F9] bg-white sticky top-[60px] z-[7] md:(items-center)">
              <NavContainer>
                <OverflowGradient tw="md:hidden" gradientPosition="left" />
                <OverflowGradient tw="md:hidden" gradientPosition="right" />
                <Nav as="nav" tw="flex gap-3 pt-2.5 w-full md:(flex-col gap-0 pt-0)">
                  {tabs.map((item, index) => (
                    <>
                      {!item.childrens ? (
                        <Link
                          shallow
                          scroll={false}
                          key={index}
                          href={`/cfp/${router.query.id}${item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`}`}
                          legacyBehavior
                          replace
                        >
                          <Tab
                            selected={item.value === selectedTab}
                            tw="pl-4 pr-1 py-3"
                            as="button"
                            id={`${item.value}-tab`}
                            css={[item.value === 'info' ? tw`md:hidden` : tw`block`]}
                          >
                            <Icon icon={item.icon} tw="mr-2" />
                            {t(item.translationId)}
                          </Tab>
                        </Link>
                      ) : (
                        <Disclosure as="div" defaultOpen={true}>
                          <DisclosureButton tw="pt-0 px-[3px] pb-[5px] md:(pl-4 pr-1 py-3 text-left)" as={Tab}>
                            <Icon icon={item.icon} tw="mr-2" />
                            {t(item.translationId)}
                          </DisclosureButton>
                          <DisclosurePanel
                            as="div"
                            tw="list-none max-md:data-[state=open]:flex max-md:[border: 1px solid lightgray] max-md:-mt-[5px] max-md:mb-[5px] gap-x-2"
                          >
                            {item.childrens.map((subItem) => (
                              <Fragment key={subItem.value}>
                                <Link
                                  shallow
                                  scroll={false}
                                  key={index}
                                  href={`/cfp/${router.query.id}?tab=${subItem.value}`}
                                  legacyBehavior
                                  replace
                                >
                                  <Tab
                                    selected={subItem.value === selectedTab}
                                    tw="pl-7 pr-1 pt-0 pb-0! md:py-3! block w-full"
                                    as="button"
                                    id={`${subItem.value}-tab`}
                                  >
                                    <Icon icon={subItem.icon} tw="mr-2" />
                                    {t(subItem.translationId)}
                                  </Tab>
                                </Link>
                              </Fragment>
                            ))}
                          </DisclosurePanel>
                        </Disclosure>
                      )}
                    </>
                  ))}
                </Nav>
              </NavContainer>
            </div>

            {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
            <div tw="flex flex-col px-0 pb-10 relative md:px-4 md:(pl-4) xl:(pr-4)">
              {selectedTab === 'info' && (
                <TabPanel id="info" tw="md:hidden">
                  <CfpInfo cfp={cfp} limitedVisibility={limitedVisibility} />
                  <span tw="md:hidden">{displayTagsAndLinks()}</span>
                </TabPanel>
              )}

              {selectedTab === 'feed' && (
                <TabPanel id="feed" tw="mx-auto w-full px-0 flex justify-center items-center">
                  {/* Show feed, and pass DisplayCreate to admins */}
                  {cfp.feed_id && <Feed feedId={cfp.id} />}
                </TabPanel>
              )}

              {selectedTab === 'about' && (
                <TabPanel id="about">
                  <div tw="max-w-3xl mx-auto">
                    <CfpAbout description={cfp.description} cfpId={cfp.id} enablers={cfp?.enablers} />
                  </div>
                </TabPanel>
              )}

              {selectedTab === 'members' && (
                <TabPanel id="members" tw="relative ">
                  <ShowMembers
                    parentId={cfp.id}
                    parentType="cfps"
                    canCreate={cfp.user_access?.permissions?.includes('manage')}
                  />
                </TabPanel>
              )}

              {selectedTab === 'rules' && (
                <TabPanel id="rules">
                  <div tw="flex items-center flex-wrap">
                    <div>{t('openForSubmissions')}:&nbsp;</div>
                    <span tw="font-bold text-sm">
                      {cfp?.submissions_from ? displayObjectUTCDate(cfp?.submissions_from) : t('toBeDefined')}
                    </span>
                  </div>
                  <div tw="flex items-center flex-wrap mb-4">
                    <div>{t('submissionDeadline')}:&nbsp;</div>
                    <span tw="font-bold text-sm">
                      {cfp?.submissions_to ? displayObjectUTCDate(cfp?.submissions_to) : t('toBeDefined')}
                    </span>
                  </div>
                  {cfp.rules && (
                    <>
                      <H2>{t('rule.other')}</H2>
                      <InfoHtmlComponent content={cfp.rules} />
                      <InfoHtmlComponent content={'po'} />
                    </>
                  )}
                </TabPanel>
              )}

              {selectedTab === 'resources' && (
                <TabPanel id="resources">
                  <ResourceList
                    itemType="cfps"
                    entity={cfp}
                    isAdmin={isAdmin(cfp)}
                    shouldForceRefresh={shouldRefreshResources}
                  />
                </TabPanel>
              )}

              {selectedTab === 'documents' && (
                <TabPanel id="documents">
                  <EntityDocumentList
                    entityId={cfp.id}
                    canManageDocuments={cfp.user_access?.permissions?.includes(Permission.Managedocuments)}
                  />
                </TabPanel>
              )}

              {selectedTab === 'proposals' && (
                <TabPanel id="proposals">
                  <CfpProposals cfp={cfp} />
                </TabPanel>
              )}

              {selectedTab === 'faq' && <TabPanel id="faq">{/* <ShowFaq faqList={cfp.faq} /> */}</TabPanel>}
            </div>
          </div>
        )}
      </div>
    </Layout>
  );
};

export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/cfps/${ctx.query.id}/detail`).catch((err) => console.error(err));
  if (res?.data) return { props: { cfp: { ...res.data }, key: ctx.query.id } };
  return { redirect: { destination: '/search?tab=cfps', permanent: false } };
};

export default CfpDetails;
