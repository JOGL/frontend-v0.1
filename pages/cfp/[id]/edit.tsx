import Icon from '~/src/components/primitives/Icon';
import { NextPage } from 'next';
import { useModal } from '~/src/contexts/modalContext';
import ManageFaq from '~/src/components/Tools/ManageFaq';
import ManageExternalLink from '~/src/components/Tools/ManageExternalLink';
import Layout from '~/src/components/Layout/layout/layout';
import Button from '~/src/components/primitives/Button';
import CfpForm from '~/src/components/Cfp/CfpForm';
import { useApi } from '~/src/contexts/apiContext';
import { Cfp } from '~/src/types';
import useTranslation from 'next-translate/useTranslation';
import { getApiFromCtx } from '~/src/utils/getApi';
import { Fragment, useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/router';
import A from '~/src/components/primitives/A';
import useUser from '~/src/hooks/useUser';
import P from '~/src/components/primitives/P';
import SpinLoader from '~/src/components/Tools/SpinLoader';
import { Disclosure, DisclosureButton, DisclosurePanel } from '@reach/disclosure';
import Link from 'next/link';
import { Tab, TabPanel, NavContainer, Nav, OverflowGradient } from '~/src/utils/style';
import tw from 'twin.macro';
import { confAlert, entityItem, getFullValidationObject, isAdmin, isOwner } from '~/src/utils/utils';
import MembersManage from '~/src/components/Members/MembersManage';
import CfpRulesForm from '~/src/components/Cfp/CfpRulesForm';
import CfpProposalsAdmin from '~/src/components/Cfp/CfpProposalsAdmin';
import CfpManageTemplate from '~/src/components/Cfp/CfpManageTemplate';
import FormValidator from '~/src/components/Tools/Forms/FormValidator';
import cfpFormRules from '~/src/components/Cfp/CfpFormRules.json';
import PrivacySecurity from '~/src/components/PrivacySecurity/PrivacySecurity';
import MessageBox from '~/src/components/Tools/MessageBox';
import Swal from 'sweetalert2';

interface Props {
  cfp: Cfp;
}

const DEFAULT_TAB = 'general';

const CfpEdit: NextPage<Props> = ({ cfp: cfpProp }) => {
  const { t } = useTranslation('common');
  const validator = new FormValidator(cfpFormRules, t);
  const [cfp, setCfp] = useState(cfpProp);
  const [updatedCfp, setUpdatedCfp] = useState<Cfp>(undefined);
  const [sending, setSending] = useState(false);
  const [hasChanged, setHasChanged] = useState(false);
  const [hasErrors, setHasErrors] = useState<boolean>(false);
  const [showErrorList, setShowErrorList] = useState<boolean>(false);
  const [stateValidation, setStateValidation] = useState<any>({});
  const [savedOrPublished, setSavedOrPublished] = useState('saved');
  const savedOrPublishedRef = useRef('saved');
  const [selectedTab, setSelectedTab] = useState(DEFAULT_TAB);
  const urlBack = `/cfp/${cfp.id}/${cfp.short_title}`;
  const api = useApi();
  const { showModal, closeModal } = useModal();
  const { user } = useUser();
  const router = useRouter();
  const navRef = useRef();

  const submittedProposalsCount = cfp.stats.submitted_proposals_count;
  const draftProposalsCount = cfp.stats.proposals_count - submittedProposalsCount;
  savedOrPublishedRef.current = savedOrPublished;

  const templateChangeWarnMsg = t('ifYouEditTheTemplateSubmittedProposalsWillBeRolledBackToDrafts', {
    draftCount: draftProposalsCount,
    placeholderOne: t('proposal', { count: draftProposalsCount }),
    submittedCount: submittedProposalsCount,
    placeholderTwo: t('proposal', { count: submittedProposalsCount }),
  });

  const handleChange: (key: any, content: any) => void = (key, content) => {
    setHasChanged(true);
    if (key === 'banner_id') {
      handleChange('banner_url', `${process.env.ADDRESS_BACK}/images/${content?.id}/full`);
      handleChange('banner_url_sm', `${process.env.ADDRESS_BACK}/images/${content?.id}/tn`);
      setUpdatedCfp((prevUpdatedCfp) => ({ ...prevUpdatedCfp, banner_id: content?.id ?? null }));
    } else {
      setCfp((prevCfp) => ({ ...prevCfp, [key]: content })); // update fields as user changes them
      // TODO: have only one place where we manage cfp content
      setUpdatedCfp((prevUpdatedCfp) => ({ ...prevUpdatedCfp, [key]: content })); // set an object containing only the fields/inputs that are updated by user
    }

    /* Validators start */
    const validation = validator.validate({ [key]: content });

    if (validation[key] !== undefined) {
      setStateValidation((prevState) => ({ ...prevState, [`valid_${key}`]: validation[key] }));
    }
    /* Validators end */
  };

  useEffect(() => {
    if (hasChanged) {
      const validation = validator.validate(cfp);
      if (validation.isValid) {
        setHasErrors(false);
      } else {
        setHasErrors(true);
      }
    }
  }, [cfp]);

  const validateTemplateAndRules = () => {
    const canSubmit = (cfp.submissions_from && cfp.submissions_to) || (!cfp.submissions_from && !cfp.submissions_to);

    if (!canSubmit) {
      return { valid: false, error: t('submissionDatesError') };
    }

    return validateQuestions();
  };

  const validateQuestions = () => {
    const invalidTitle = cfp.template.questions.find((q) => !q.title || q.title.trim() === '');
    if (invalidTitle) {
      return { valid: false, error: t('questionsTitleError') };
    }

    const invalidChoices = cfp.template.questions.find(
      (q) => (q.type === 'single_choice' || q.type === 'multiple_choice') && q.choices.length < 2
    );
    if (invalidChoices) {
      return { valid: false, error: t('multipleOrSingleChoiceQuestionsError') };
    }

    return { valid: true, error: '' };
  };

  const handleSubmit = async (data: any = null) => {
    if (!hasErrors) {
      setShowErrorList(false);

      const { valid, error } = validateTemplateAndRules();

      if (valid) {
        const verifiedData = data && !data.clientX ? data : null;
        setSending(true);
        const res = await api.patch(`/cfps/${cfp.id}`, verifiedData ?? updatedCfp).catch((err) => {
          console.error(`Couldn't patch cfp with id=${cfp.id}`, err);
          setSending(false);
        });
        if (res) {
          confAlert.fire({ icon: 'success', title: `Successfully ${savedOrPublishedRef.current}!` });
          // on success
          setSending(false);
          setUpdatedCfp(undefined); // reset updated cfp component
          setStateValidation({}); // reset validations
          setHasChanged(false); //reset changed status
        }
      } else {
        confAlert.fire({ icon: 'error', title: error });
      }
    } else {
      /*
       * Validate entire object so we get entire list of missing fields, even for fields that haven't changed.
       * This is important for displaying the correct info on the first edit, otherwise mandatory fields that haven't been touched yet will not show up on the list of missing fields.
       */
      const fullValidationObject = getFullValidationObject(validator, cfp);
      setStateValidation(fullValidationObject);

      setShowErrorList(true);
      window?.scrollTo({ top: 0, behavior: 'smooth' }); // scroll to element to show error
    }
  };

  const publish = () => {
    setSavedOrPublished('published');
    handleChange('status', 'active');
    handleSubmit({ ...updatedCfp, status: 'active' });
  };

  const tabs = [
    { value: 'general', translationId: 'general', icon: 'fluent:text-description-20-filled' },
    { value: 'about', translationId: 'about', icon: 'ic:outline-info' },
    {
      value: 'privacy_security',
      translationId: 'privacySecurity',
      icon: 'material-symbols:privacy-tip-outline',
    },
    { value: 'templates', translationId: 'template.one', icon: 'tdesign:template' },
    { value: 'rules', translationId: 'rule.other', icon: 'carbon:rule' },
    { value: 'members', translationId: 'person.other', icon: 'mdi:people-check' },
    { value: 'proposals', translationId: 'proposal.other', icon: 'bx:medal' },
    { value: 'faq', translationId: 'faqAcronym', icon: 'octicon:question-24' },
    { value: 'more', translationId: 'more', icon: 'mdi:more-circle-outline' },
  ];

  const basicTabs = tabs.map(({ value }) => value);
  const subTabs =
    tabs
      .filter((val) => val.children?.length > 0)
      .map((child) => child.children.map(({ value }) => value))
      .flat() || [];
  const tabValues = [...basicTabs, ...subTabs];

  useEffect(() => {
    if (router.query.tab && router.query.tab !== selectedTab) {
      setSelectedTab(router.query.tab as string);
    }
  }, [router.query.tab]);

  // function when changing tab (or when router change in general)
  useEffect(() => {
    if (tabValues.includes(router.query.tab as string) && navRef?.current) {
      if (router.query.tab === 'general') {
        router.push(`/cfp/${router.query.id}/edit`, undefined, {
          shallow: true,
        });
      }
      const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      const yAdjustment = window.innerWidth < 768 ? 40 : 150; // change yAdustement depending on if it's mobile/tablet or desktop
      window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
      setSelectedTab(router.query.tab as string);
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);
  }, [router]);

  const deleteCfp = () => {
    api
      .delete(`cfps/${cfp.id}`)
      .then(() => {
        closeModal(); // close modal
        router.push('/');
      })
      .catch((error) => {
        // setErrors(error.toString());
      });
  };

  const delBtnTitleId = cfp?.members_count > 1 ? 'action.archive' : 'action.delete';
  const delBtnTextId = cfp?.members_count > 1 ? 'areYouSure' : 'deleteConfirmation';

  // const handleSubmitMemo = useMemo(() => debounce(handleSubmit, 800), []);

  const handleArchive = () => {
    const status = cfp.status === 'archived' ? 'active' : 'archived';
    setCfp((prevState) => ({ ...prevState, status }));
    handleSubmit({ status });
  };

  const showEditContext = () => (
    <div tw="inline-flex flex-col w-full pl-[10px] items-start justify-start md:(pl-[0px] items-start justify-start) max-md:(pt-4 pb-2)">
      <A href={urlBack}>
        <div tw="px-4 py-1 mb-1 w-full border-gray-200 border rounded-full text-primary! hover:bg-gray-100 md:hidden">
          <Icon icon="formkit:arrowleft" tw="w-5 h-5" />
          {t('action.back')}
        </div>
      </A>
      <h1 tw="font-extrabold text-[36px]">{t('action.editItem', { item: t('callForProposals', { count: 1 }) })}</h1>
      <span tw="text-black px-[6px] py-[3px] text-[15px] mb-2" style={{ background: entityItem.cfp.color }}>
        {t('callForProposals', { count: 1 })} <span tw="font-bold">{cfp?.title}</span>
      </span>
      {false && <span tw="italic text-[15px] font-[400]">In this tab you can edit name, banner etc.</span>}
    </div>
  );

  const isSaveHidden = () => {
    return (
      selectedTab !== 'general' &&
      selectedTab !== 'about' &&
      selectedTab !== 'privacy_security' &&
      selectedTab !== 'rules' &&
      selectedTab !== 'templates' &&
      selectedTab !== 'faq'
    );
  };

  return (
    <Layout title={`${cfp.title} | JOGL`}>
      <div tw="mx-auto sm:shadow-custom2">
        <div
          tw="mb-6 grid grid-cols-1 md:(bg-white grid-cols-[14rem calc(100% - 14rem)]) bg-white border-t border-solid "
          ref={navRef}
        >
          {/* Header and nav (left col on desktop, top col on mobile */}
          <div tw="md:hidden">{showEditContext()}</div>
          <div tw="flex flex-col border-r-2 border-solid border-[#F7F7F9] bg-white sticky top-[64px] z-[8] md:(items-center)">
            <NavContainer>
              <OverflowGradient tw="md:hidden" gradientPosition="left" />
              <OverflowGradient tw="md:hidden" gradientPosition="right" />
              <Nav as="nav" tw="flex gap-3 pt-2.5 w-full md:(flex-col gap-0 pt-0)">
                <div tw="flex flex-col w-full">
                  <div tw="relative hidden md:(flex justify-center flex-col items-center)">
                    <img
                      tw="object-cover h-[100px] w-full relative border-b border-gray-200"
                      src={cfp?.banner_url ?? '/images/default/default-cfp.png'}
                      alt="banner url"
                    />
                    {cfp?.status === 'draft' && (
                      <div tw="absolute top-2 left-2 text-sm font-medium text-gray-600 rounded bg-white bg-opacity-80 px-2 py-1 [box-shadow:0px 4px 15px rgb(0 0 0 / 9%)] first-letter:capitalize">
                        Draft
                      </div>
                    )}
                    <A href={urlBack}>
                      <div tw="px-4 ml-auto my-2 py-1 w-full border-gray-200 border rounded-full text-primary! hover:bg-gray-100">
                        <Icon icon="formkit:arrowleft" tw="w-5 h-5" />
                        {t('action.back')}
                      </div>
                    </A>
                  </div>
                </div>
                {tabs.map((item, index) => (
                  <div key={`${item.value}-${index}`}>
                    {!item.children ? (
                      <Link
                        shallow
                        scroll={false}
                        key={`${item.value}-${index}`}
                        href={`/cfp/${router.query.id}/edit${item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`}`}
                        legacyBehavior
                      >
                        <Tab
                          selected={item.value === selectedTab}
                          tw="pl-4 pr-1 py-3"
                          as="button"
                          id={`${item.value}-tab`}
                          css={[item.value === 'info' ? tw`w-full md:hidden` : tw`block w-full`]}
                        >
                          <Icon icon={item.icon} tw="mr-2" />
                          {t(item.translationId)}
                        </Tab>
                      </Link>
                    ) : (
                      <Disclosure as="div" defaultOpen={true} key={`${item.value}-${index}`}>
                        <DisclosureButton tw="pt-0 px-[3px] pb-[5px] md:(pl-4 pr-1 py-3 text-left)" as={Tab}>
                          <Icon icon={item.icon} tw="mr-2" />
                          {t(item.translationId)}
                        </DisclosureButton>
                        <DisclosurePanel
                          as="div"
                          tw="list-none max-md:data-[state=open]:flex max-md:[border: 1px solid lightgray] max-md:-mt-[5px] max-md:mb-[5px] gap-x-2"
                        >
                          {item.children.map((subItem) => (
                            <Fragment key={subItem.value}>
                              <Link
                                shallow
                                scroll={false}
                                key={`${subItem.value}-${index}`}
                                href={`/cfp/${router.query.id}/edit?tab=${subItem.value}`}
                                legacyBehavior
                              >
                                <Tab
                                  selected={subItem.value === selectedTab}
                                  tw="pl-7 pr-1 pt-0 pb-0! md:py-3! block w-full"
                                  as="button"
                                  id={`${subItem.value}-tab`}
                                >
                                  <Icon icon={subItem.icon} tw="mr-2" />
                                  {t(subItem.translationId)}
                                </Tab>
                              </Link>
                            </Fragment>
                          ))}
                        </DisclosurePanel>
                      </Disclosure>
                    )}
                  </div>
                ))}
              </Nav>
            </NavContainer>
          </div>

          {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
          <div tw="flex flex-col px-0 pb-10 relative mt-4 md:px-4 md:(pl-4) xl:(pr-4)">
            <div tw="max-md:hidden">{showEditContext()}</div>
            {/* General infos tab */}
            {selectedTab === 'general' && (
              <TabPanel>
                <CfpForm
                  cfp={cfp}
                  handleChange={handleChange}
                  stateValidation={stateValidation}
                  showErrorList={showErrorList}
                />
              </TabPanel>
            )}

            {/* About tab */}
            {selectedTab === 'about' && (
              <TabPanel>
                {/* <FormWysiwygComponent
                  id="description"
                  content={cfp.description}
                  title={t('description')}
                  placeholder={t('describeCallForProposalsInDetail')}
                  onChange={handleChange}
                /> */}
                <ManageExternalLink links={cfp.links} handleChange={handleChange} />
              </TabPanel>
            )}

            {/* Privacy security */}
            {selectedTab === 'privacy_security' && (
              <TabPanel>
                <PrivacySecurity object={cfp} type="cfp" handleChange={handleChange} />
              </TabPanel>
            )}

            {/* Members tab */}
            {selectedTab === 'members' && (
              <TabPanel>
                <MembersManage
                  itemId={cfp.id}
                  itemType="cfps"
                  isOnboardingActive={cfp.onboarding?.enabled}
                  isOwner={isOwner(cfp)}
                />
              </TabPanel>
            )}

            {/* Rules tab */}
            {selectedTab === 'rules' && (
              <TabPanel>
                <CfpRulesForm cfp={cfp} handleChange={handleChange} />
              </TabPanel>
            )}

            {/* Templates tab */}
            {selectedTab === 'templates' && (
              <TabPanel>
                <p tw="italic">{t('templateQuestionsForProposals')}</p>
                {cfp.stats.proposals_count > 0 ? <MessageBox type="danger">{templateChangeWarnMsg}</MessageBox> : ''}
                <CfpManageTemplate template={cfp.template} setHasChanged={setHasChanged} handleChange={handleChange} />
              </TabPanel>
            )}

            {/* Proposals tab */}
            {selectedTab === 'proposals' && (
              <TabPanel>
                <CfpProposalsAdmin cfp={cfp} handleChange={handleChange} handleSubmit={handleSubmit} />
              </TabPanel>
            )}

            {/* FAQs tab */}
            {selectedTab === 'faq' && (
              <TabPanel>
                <ManageFaq faqs={cfp.faq} handleChange={handleChange} />
              </TabPanel>
            )}

            {/* Advanced tab */}
            {selectedTab === 'more' && (
              <TabPanel>
                <div className="deleteBtns">
                  {cfp && (
                    <button
                      tw="ml-3 bg-blue-500 hocus:bg-blue-600 text-white font-semibold py-2 px-4 rounded-lg border border-blue-600 mr-4"
                      onClick={handleArchive}
                    >
                      {cfp.status === 'archived' ? t('action.publish') : t('action.archive')}{' '}
                      {t('callForProposals', { count: 1 })}
                    </button>
                  )}
                  {cfp && cfp.status === 'archived' && (
                    <Button
                      onClick={() => {
                        showModal({
                          children: (
                            <>
                              {/* {errors && <Alert type="danger" message={errorMessage} />} */}
                              <P tw="text-base">{t(delBtnTextId)}</P>
                              <div tw="inline-flex space-x-3">
                                <Button btnType="danger" onClick={deleteCfp}>
                                  {t('yes')}
                                </Button>
                                <Button onClick={closeModal}>{t('no')}</Button>
                              </div>
                            </>
                          ),
                          title: t(delBtnTitleId),
                          maxWidth: '30rem',
                        });
                      }}
                      btnType="danger"
                      tw="bg-red-500 hover:bg-red-600 text-white font-semibold py-2 px-4 rounded-lg"
                    >
                      {t(delBtnTitleId)} {t('callForProposals', { count: 1 })}
                    </Button>
                  )}
                </div>
              </TabPanel>
            )}

            {/* Bottom action buttons */}
            {!isSaveHidden() && (
              <div
                tw="mt-10 mb-3 space-x-4 text-center inline-flex items-center self-center"
                css={
                  hasChanged &&
                  tw`sticky bottom-0 inline-flex items-center justify-center py-4 mx-auto border-t backdrop-blur-md bg-white/30 w-[calc(100%+32px)] -ml-4`
                }
              >
                <A href={urlBack}>
                  <Button btnType="secondary">{t('action.cancel')}</Button>
                </A>
                <Button
                  onClick={() => {
                    // If user changes the template and there are proposals, warn them with a modal, and submit only if they confirm.
                    // Otherwise, no modal and submit directly
                    if (selectedTab === 'templates' && cfp.stats.proposals_count > 0) {
                      Swal.fire({
                        title: t('changingTheTemplate'),
                        text: templateChangeWarnMsg,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#5E3EBF',
                        cancelButtonColor: '#d33',
                        confirmButtonText: t('action.save'),
                      }).then(({ isConfirmed }) => isConfirmed && handleSubmit());
                    } else handleSubmit();
                  }}
                  disabled={sending || !hasChanged}
                  css={
                    cfp?.status === 'draft' &&
                    tw`bg-[#C8BCE9] border-[#C8BCE9] text-black hover:text-white hover:bg-primary`
                  }
                >
                  {sending && savedOrPublishedRef.current === 'saved' && <SpinLoader />}
                  {cfp?.status === 'draft' ? t('action.saveAsDraft') : t('action.save')}
                </Button>
                {cfp?.status === 'draft' && <Button onClick={publish}>{t('action.publish')}</Button>}
                {/* {hasChanged && <p tw="mb-0">{t('dontForgetToSaveYourChanges')}</p>} */}
              </div>
            )}
          </div>
        </div>
      </div>
    </Layout>
  );
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  const res = await api
    .get(`/cfps/${query.id}`)
    .catch((err) => console.error(`Couldn't fetch cfp with id=${query.id}`, err));
  // Check if it got the cfp and if the user is admin
  if (res?.data && isAdmin(res?.data)) return { props: { cfp: { ...res?.data }, key: query.id } };
  return { redirect: { destination: `/cfp/${query.id}`, permanent: false } };
}

export default CfpEdit;
