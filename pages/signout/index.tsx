import { NextPage } from 'next';
import SignOut from '~/src/components/Pages/SignInUp/SignOut/SignOut';

const SignoutPage: NextPage = () => {
  return <SignOut />;
};

export default SignoutPage;
