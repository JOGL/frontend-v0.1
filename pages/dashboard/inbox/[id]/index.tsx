import { NextPage } from 'next';
import { getApiFromCtx } from '~/src/utils/getApi';
import { NodeDetailModel } from '~/__generated__/types';
import EmptyComponent from '~/src/components/Pages/empty/empty';

const InboxPage: NextPage = ({ node }: { node: NodeDetailModel }) => {
  return <EmptyComponent />;
};

export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/nodes/${ctx.query.id}/detail`).catch((err) => console.error(err));
  if (res && res?.data) return { props: { node: { ...res.data } } };
  return { redirect: { destination: '/search', permanent: false } };
};

export default InboxPage;
