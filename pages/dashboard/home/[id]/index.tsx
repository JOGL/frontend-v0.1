import { NextPage } from 'next';
import { getApiFromCtx } from '~/src/utils/getApi';
import { NodeDetailModel } from '~/__generated__/types';
import HubWorkspaces from '~/src/components/Pages/dashboard/hubWorkspaces/hubWorkspaces';

const HomePage: NextPage = ({ node, filterKey }: { node: NodeDetailModel; filterKey: string }) => {
  return <HubWorkspaces node={node} filterKey={filterKey} />;
};

export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/nodes/${ctx.query.id}/detail`).catch((err) => console.error(err));
  if (res && res?.data) return { props: { node: { ...res.data }, filterKey: ctx.query.key } };
  return { redirect: { destination: '/search', permanent: false } };
};

export default HomePage;
