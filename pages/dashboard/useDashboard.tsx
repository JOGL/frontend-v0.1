import { useRouter } from 'next/router';
import { menuItems } from '~/src/components/Layout/menuItems/menuItems';
import usePrimaryMenu from '~/src/components/Layout/primaryMenu/primaryMenu.hooks';
import { useHubDiscussionsStore } from '~/src/store/hub-discussions/hubDiscussionsStoreProvider';

const useDashboard = (): [(id?: string) => void] => {
  const router = useRouter();
  const [getPath] = usePrimaryMenu();

  const { selectedHub, setSelectedMenuItem } = useHubDiscussionsStore((store) => ({
    selectedHub: store?.selectedHubDiscussion,
    setSelectedMenuItem: store?.setSelectedMenuItem
  }));

  const goHome = (id?: string) => {
    if (!id) id = selectedHub?.id;
    setSelectedMenuItem('home');
    router.push(
      `/${getPath(menuItems.home.front_path_desktop, {
        id: id,
        home_channel_id: selectedHub?.home_channel_id,
      })}`
    );
  };

  return [goHome];
};

export default useDashboard;
