import { NextPage } from 'next';
import Layout from '~/src/components/Layout/layout/layout';
import SecondaryMenu from '~/src/components/Layout/secondaryMenu/secondaryMenu';

const MenuPage: NextPage = ({ menu }: { menu: string }) => {
  return (
    <Layout title="Menu" className={''}>
      <SecondaryMenu desiredMenuItem={menu} />
    </Layout>
  );
};

export const getServerSideProps = async (ctx) => {
  return { props: { menu: ctx.query.menu } };
};

export default MenuPage;
