import { NextPage } from 'next';
import Search from '~/src/components/Pages/search/search';

const SearchPage: NextPage = () => {
  return <Search />;
};

export default SearchPage;
