import { NextPage } from 'next';
import UserOnboarding from '~/src/components/Onboarding/userOnboarding/userOnboarding';
import { OnboardingStoreProvider } from '~/src/components/Onboarding/userOnboardingStore/userOnboardingStoreProvider';

const Onboarding: NextPage = () => {
  return (
    <OnboardingStoreProvider>
      <UserOnboarding />
    </OnboardingStoreProvider>
  );
};

export default Onboarding;
