import { NextPage } from 'next';
import { getApiFromCtx } from '~/src/utils/getApi';
import { Permission } from '~/__generated__/types';
import EntityPaperList from '~/src/components/papers/entityPaperList/entityPaperList';
import Layout from '~/src/components/Layout/layout/layout';

const PaperListPage: NextPage = ({ id, permissions }: { id: string; permissions: Permission[] }) => {
  return (
    <Layout title="Papers">
      <EntityPaperList entityId={id} canManagePapers={permissions.includes(Permission.Managelibrary)} />
    </Layout>
  );
};

export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/entities/${ctx.query.id}/permissions`).catch((err) => console.error(err));
  return { props: { id: ctx.query.id, permissions: res?.data ?? [], key: ctx.query.id } };
};

export default PaperListPage;
