import { useRouter } from 'next/router';
import { NextPage } from 'next';
import { useEffect, useState } from 'react';
import { useApi } from '~/src/contexts/apiContext';
import React from 'react';

const Confirm: NextPage = () => {
  const router = useRouter();
  const api = useApi();
  const [email_value] = useState<string>(`${router.query.email}` || '');
  const [verificationCode_value] = useState<string>(`${router.query.verification_code}` || '');
  const redirectUrl = router.query.redirectURL || '';

  useEffect(() => {
    if (email_value && verificationCode_value) {
      confirm();
    }
  }, [email_value]);

  const confirm = async () => {
    try {
      await api.post(`/auth/verification/confirm`, { email: email_value.toLowerCase(), code: verificationCode_value });
      router.push(
        `/signin?confirmed=true&email=${encodeURIComponent(email_value.toLowerCase())}&redirectUrl=${redirectUrl}`
      );
    } catch (e) {
      router.push(`/signin?email=${encodeURIComponent(email_value.toLowerCase())}&redirectUrl=${redirectUrl}`);
    }
  };

  return <></>;
};

export default Confirm;
