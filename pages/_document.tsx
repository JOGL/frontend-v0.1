import Document, { Head, Main, NextScript, DocumentContext, Html } from 'next/document';

export default class IntlDocument extends Document {
  static async getStaticProps(context: DocumentContext) {
    const initialProps = await Document.getStaticProps(context);
    const language = context.req.language;
    return { props: { ...initialProps, language } };
  }

  render() {
    return (
      <Html lang={this.props.language}>
        <Head>
          <link rel="manifest" href="/manifest.json" />
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link
            href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;700;900&display=swap"
            rel="stylesheet"
          />
        </Head>
        {/* assume that user is using mouse by default. We use this to remove style from elements on focus if user is using mouse */}
        <body className="using-mouse">
          <Main />
          <script
            src={`https://maps.googleapis.com/maps/api/js?key=${process.env.GOOGLE_MAPS_API_KEY}&loading=async&libraries=places`}
          />
          <NextScript />
        </body>
      </Html>
    );
  }
}
