/* eslint-disable global-require */
// General imports
import React, { useEffect } from 'react';
import { AppProps } from 'next/app';
import dynamic from 'next/dynamic';
import { clarity } from 'react-microsoft-clarity';
import CustomLoader from '~/src/components/Tools/CustomLoader';
import '@reach/menu-button/styles.css';
import '~/src/components/Auth.scss';
import '~/src/components/Feed/Comments/CommentCreate.scss';
import '~/src/components/Feed/Comments/CommentDisplay.scss';
import '~/src/components/Feed/PostsOld/PostDisplay.scss';
import '~/src/components/Need/need.scss';
import '~/src/components/Need/NeedContent.scss';
import '~/src/components/Need/NeedForm.scss';
import '~/src/components/Search/CustomSearch.scss';
import '~/src/components/Similar.scss';
import '~/src/components/Tools/BtnUploadFile.scss';
import '~/src/components/Tools/Forms/FormImgComponent.scss';
import '~/src/components/Tools/Forms/FormSkillsComponent.scss';
import '~/src/components/Tools/Forms/FormToggleComponent.scss';
import '~/src/components/Tools/Info/InfoDefaultComponent.scss';
import '~/src/components/Tools/Info/InfoHtmlComponent.scss';
import '~/src/components/Tools/ShareBtns/ShareBtns.scss';
import '~/src/components/User/UserHeader.scss';
import '~/src/components/User/UserProfileEdit.scss';
import 'react-datepicker/dist/react-datepicker.css';
import '~/src/components/Event/EventDatePicker.scss';
import 'pages/custom.scss';
import 'pages/global.scss';
import 'pages/nProgress.scss';
import 'intl-pluralrules';
// Formatting dates with day.js
import 'dayjs/locale/fr';
import 'dayjs/locale/de';
import 'dayjs/locale/es';
import 'katex/dist/katex.min.css';
import '~/src/dayjs';
import { PwaProvider } from '~/src/pwa/pwa';

const AppLayout = dynamic(() => import('../src/components/Layout/AppLayout'), {
  loading: () => <CustomLoader />,
  ssr: false,
});

export default function App({ Component, pageProps }: AppProps) {
  useEffect(() => {
    clarity.init('orqgpj1faj');
    clarity.consent();
  }, []);

  return (
    <PwaProvider>
      <AppLayout>
        <Component {...pageProps} />
      </AppLayout>
    </PwaProvider>
  );
}
