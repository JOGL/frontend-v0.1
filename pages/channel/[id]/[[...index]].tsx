import { NextPage } from 'next';
import { getApiFromCtx } from '~/src/utils/getApi';
import Channel from '~/src/components/Pages/channel/channel';
import { ChannelDetailModel } from '~/__generated__/types';

const ChannelPage: NextPage = ({ channel }: { channel: ChannelDetailModel }) => {
  return <Channel channel={channel} />;
};

export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/channels/${ctx.query.id}/detail`).catch((err) => console.error(err));
  if (res && res?.data) return { props: { channel: { ...res.data }, key: ctx.query.id } };
  return { redirect: { destination: '/search', permanent: false } };
};

export default ChannelPage;
