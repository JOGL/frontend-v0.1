import { NextPage } from 'next';
import EmptyComponent from '~/src/components/Pages/empty/empty';

const ChannelPage: NextPage = () => {
  return <EmptyComponent />;
};

export default ChannelPage;
