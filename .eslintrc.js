require('@rushstack/eslint-config/patch/modern-module-resolution');

module.exports = {
  env: {
    browser: true,
    es6: true,
    jest: true,
  },
  extends: ['@rushstack/eslint-config', '@rushstack/eslint-config/react', 'plugin:react-hooks/recommended'],
  parserOptions: {
    project: './tsconfig.strict.json',
    tsconfigRootDir: __dirname,
  },
  rules: {
    '@typescript-eslint/no-floating-promises': 'off',
    '@typescript-eslint/typedef': 'off',
    '@typescript-eslint/naming-convention': [
      'error',
      {
        selector: 'interface',
        format: ['PascalCase'],
      },
    ],
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint(react/jsx-no-bind)': 'off',
    'react/jsx-no-bind': 'off',
    '@rushstack/no-null': 'off',
    'no-unused-expressions': ['error', { allowShortCircuit: true }],
  },
  settings: {
    react: {
      version: '16.13',
    },
  },
};
