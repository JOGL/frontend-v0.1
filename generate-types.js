const { generateApi } = require('swagger-typescript-api');
const path = require('path');

generateApi({
  name: 'types.ts',
  output: path.resolve(process.cwd(), './__generated__'),
  url: process.env.ADDRESS_BACK + 'swagger/v1/swagger.json',
  generateClient: true,
  httpClientType: 'axios',
  generateRouteTypes: true,
  generateResponses: true,
  hooks: {
    onFormatRouteName: (routeInfo, templateRouteName) => {
      const hasPathDetails = /{[^/]+}$/.test(routeInfo.route);
      if (hasPathDetails) {
        // Resolve path name conflicts for route name
        // https://github.com/acacode/swagger-typescript-api/issues/211
        return templateRouteName + 'Detail';
      }
      return templateRouteName;
    },
  },
}).catch((e) => console.error('Error writing generated types file', e));
